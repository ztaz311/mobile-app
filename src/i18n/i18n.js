import I18n from 'react-native-i18n';

import en from './locales/en';
import vi from './locales/vi';
import ko from './locales/ko';

I18n.translations = {
    en,
    vi,
    ko
};

export default I18n;