import React, { Component } from 'react';
import { View, ActivityIndicator, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
export default class Loading extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, position: 'absolute', backgroundColor: 'rgba(0,0,0,0.1)', left: 0, top: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', zIndex: 999999 }}>
                <ActivityIndicator size="large" color="#000000" style={{bottom: 30}} />
            </View>
        );
    }
}
