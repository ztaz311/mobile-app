import React, { Component } from 'react';
import { Platform, View, SafeAreaView, StatusBar } from 'react-native';

export default class SafeView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            Platform.OS === 'android' ?
                <View
                    style={this.props.style}>
                    <StatusBar barStyle="light-content" />
                    {this.props.children}
                </View>
                :
                <SafeAreaView
                    style={this.props.style}>
                    <StatusBar barStyle="dark-content" />
                    {this.props.children}
                </SafeAreaView>
        )
    }
}
