import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

import Touchable from './Touchable'
import * as Colors from '../constants/Colors'

export default class Bar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index
        }
    }

    componentWillReceiveProps(props) {
        this.setState({ index: props.index })
    }

    setIndex = (index) => {
        this.setState({ index })
        if (this.props.onValueChanged) this.props.onValueChanged(index)
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', backgroundColor: Colors.BG }}>
                <BUTTON
                    image={this.state.index == 0 ? require('../assets/1_home_blu_.png') : require('../assets/1_home_blk_.png')}
                    title={'HOME'}
                    selected={this.state.index == 0}
                    onPress={() => this.setIndex(0)}
                />
                <BUTTON
                    image={this.state.index == 1 ? require('../assets/2_bookmark_blu_.png') : require('../assets/2_bookmark_blk_.png')}
                    title={'BOOKMARK'}
                    selected={this.state.index == 1}
                    onPress={() => this.setIndex(1)}
                />
                <BUTTON
                    image={this.state.index == 2 ? require('../assets/3_ticket_blu_.png') : require('../assets/3_ticket_blk_.png')}
                    title={'TICKET'}
                    selected={this.state.index == 2}
                    onPress={() => this.setIndex(2)}
                />
                <BUTTON
                    image={this.state.index == 3 ? require('../assets/4_news_blu_.png') : require('../assets/4_news_blk_.png')}
                    title={'NEWS'}
                    selected={this.state.index == 3}
                    onPress={() => this.setIndex(3)}
                />
                <BUTTON
                    image={this.state.index == 4 ? require('../assets/5_my_blu_.png') : require('../assets/5_my_blk_.png')}
                    title={'MY'}
                    selected={this.state.index == 4}
                    onPress={() => this.setIndex(4)}
                />
            </View>
        );
    }
}

class BUTTON extends Component {
    render() {
        return (
            <Touchable
                onPress={this.props.onPress}
                style={{
                    minHeight: 56,
                    flex: 1, alignSelf: 'stretch',
                    alignItems: 'center', justifyContent: 'center'
                }}>
                <Image source={this.props.image} style={{ alignSelf: 'center' }} />
                <Text style={{ alignSelf: 'center', marginTop: 4, fontSize: 12, color: this.props.selected ? Colors.PRIMARY : Colors.TEXT_P }}>{this.props.title}</Text>
            </Touchable>
        )
    }
}
