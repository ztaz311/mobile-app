import React, { Component } from 'react';
import {
    StyleSheet, Dimensions, Platform,
    Modal, View, TouchableOpacity, Picker,
    Text, Image
} from 'react-native';

import * as Colors from '../constants/Colors'

var PickerItemIOS = Picker.Item;
var SCREEN_WIDTH = Dimensions.get('window').width;

class PickerItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: this.props.options,
            color: Colors.PRIMARY,
            modalVisible: this.props.modalVisible ? this.props.modalVisible : false,
            selectedOption: (this.props.selectedOption != '') ? this.props.selectedOption : this.props.options[0].value,
            oldSelectedOption: '',
            isChange: this.props.isChange ? this.props.isChange : false,
            txtConfirm: this.props.txtConfirm ? this.props.txtConfirm : ('Confirm'),
            txtCancel: this.props.txtCancel ? this.props.txtCancel : ('Cancel'),
            boxDisplayStyles: this.props.boxDisplayStyles ? this.props.boxDisplayStyles : {},
            textDisplayStyle: this.props.textDisplayStyle ? this.props.textDisplayStyle : {},
            containerStyle: this.props.containerStyle ? this.props.containerStyle : {},
            iconStyle: this.props.iconStyle ? this.props.iconStyle : {},
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.options !== nextProps.options) {
            this.setState({
                options: nextProps.options
            });
        }
        if(this.props.selectedOption !== nextProps.selectedOption) {
            this.setState({
                selectedOption: nextProps.selectedOption
            });
            if(this.props.updateValue) {
                this.props.updateValue(nextProps.selectedOption)
            }
        }
    }

    onPressPicker() {
        var oldSelected = this.state.selectedOption;
        this.setState({ modalVisible: true, oldSelectedOption: oldSelected });
    }

    confirm() {
        if (this.props.onSubmit) {
            this.props.onSubmit(this.state.selectedOption);
        }
        this.setState({ modalVisible: false });
    }

    cancel() {
        if (!this.state.isChange) {
            var oldSelected = this.state.oldSelectedOption;
            this.setState({ modalVisible: false, selectedOption: oldSelected });
        } else {
            this.setState({ modalVisible: false });
        }
    }

    onChange(value) {
        if (this.state.isChange) {
            if (this.props.onSubmit) {
                this.props.onSubmit(value);
            }
            this.setState({ selectedOption: value, modalVisible: false });
        } else {
            this.setState({ selectedOption: value });
        }
    }

    onChangeAndroid(value) {
        if (this.props.onSubmit) {
            this.props.onSubmit(value);
        }
        this.setState({ selectedOption: value});
    }

    displaySelected() {
        var selectedOption = this.state.selectedOption;
        var item = new Array();
        item = this.state.options.filter(function (option) {
            return option.value == selectedOption;
        })
        return item.length > 0 ? item[0].name : '';
    }

    renderIOS = () => {
        return <TouchableOpacity
            style={[this.state.boxDisplayStyles, {justifyContent: 'center'}]}
            underlayColor={'transparent'}
            onPress={() => this.onPressPicker()}>
            <View style={[
                {
                    alignSelf: 'stretch',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                },
                this.state.containerStyle
            ]}>
                <Text style={[{ fontSize: 15, fontWeight: 'normal', color: Colors.TEXT_P, flex: 1 }, this.state.textDisplayStyle]} numberOfLines={1}>{this.displaySelected()}</Text>
                <Image source={require('../assets/down_arrow_active.png')} style={{ marginLeft: 0, width: 13, height: 11 }} />
            </View>
            <Modal
                visible={this.state.modalVisible}
                transparent={true}>
                <View style={styles.basicContainer}>
                    <View style={styles.modalContainer}>
                        <View style={styles.buttonView}>
                            <TouchableOpacity onPress={() => this.cancel()}>
                                <Text style={{ color: Colors.TEXT_P, fontSize: 16 }}>{this.state.txtCancel}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.confirm()}>
                                <Text style={{ color: this.state.color, fontSize: 16, fontWeight: 'normal' }}>{this.state.txtConfirm}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.mainBox}>
                            {/*Model body*/}
                            <Picker
                                ref={'picker'}
                                style={styles.bottomPicker}
                                selectedValue={this.state.selectedOption}
                                onValueChange={(value) => this.onChange(value)}
                            >
                                {this.state.options.map((option, i) => {
                                    return (
                                        <PickerItemIOS
                                            key={i}
                                            value={option.value}
                                            label={option.name}
                                        />
                                    )
                                })}
                            </Picker>
                        </View>
                    </View>
                </View>
            </Modal>
        </TouchableOpacity>
    }


    renderAndroid = () => {
        return <View
        style={[this.state.boxDisplayStyles, {flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}]}>
            <Picker
                style={[{ height: 28, backgroundColor: 'transparent', width: '100%', position: 'absolute', left: 0, top: 0, }, this.state.containerStyle]}
                mode='dropdown'
                selectedValue={this.state.selectedOption}
                onValueChange={(itemValue, itemIndex) => this.onChangeAndroid(itemValue)}>
                {this.state.options.map((option, i) => {
                    return (
                        <Picker.Item
                            key={i}
                            value={option.value}
                            label={option.name}
                        />
                    )
                })}
            </Picker>
            <Image source={require('../assets/down_arrow_active.png')} style={[{ marginRight: 5, width: 13, height: 11 }, this.state.iconStyle]} />
        </View>
    }

    render() {
        return Platform.OS === 'ios' ? this.renderIOS() : this.renderAndroid()
    }
}

var styles = StyleSheet.create({
    basicContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'rgba(78, 78, 78, 0.68)'
    },
    modalContainer: {
        width: SCREEN_WIDTH,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        backgroundColor: '#F5FCFF',
    },
    buttonView: {
        width: SCREEN_WIDTH,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        paddingTop: 10,
        borderTopWidth: 0.5,
        borderTopColor: 'lightgrey',
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.TEXT_S,
    },
    bottomPicker: {
        width: SCREEN_WIDTH,
    },
    mainBox: {
    }
});
export default PickerItem;


