import React, { Component } from 'react';
import { Platform, TouchableOpacity, TouchableHighlight, View } from 'react-native';

import * as Colors from '../constants/Colors'

export default class Touchable extends Component {
    render() {
        let children = this.props.children
        return (
            <TouchableOpacity
                disabled={this.props.disabled}
                style={this.props.style}
                activeOpacity={0.5}
                onPress={this.props.onPress}>
                <React.Fragment>
                    {children}
                </React.Fragment>
            </TouchableOpacity>

            // Platform.select({
            // android: <TouchableHighlight
            //     activeOpacity={this.props.activeOpacity}
            //     style={this.props.style}
            //     underlayColor={Colors.PRESS}
            //     onPress={this.props.onPress}>
            //     <View>
            //         {children}
            //     </View>
            // </TouchableHighlight>,
            // ios: <TouchableOpacity
            //     style={this.props.style}
            //     activeOpacity={0.5}
            //     onPress={this.props.onPress}>
            //     <View>
            //         {children}
            //     </View>
            // </TouchableOpacity>
            // })
        );
    }
}
