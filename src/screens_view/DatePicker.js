import React, { Component } from 'react';
import { View, Platform, DatePickerAndroid, DatePickerIOS } from 'react-native';

export default class DatePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            Platform.select({
                android: <DatePickerAndroid />,
                ios: <DatePickerIOS />
            })
        );
    }
}
