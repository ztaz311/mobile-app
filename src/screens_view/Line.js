import React, { Component } from 'react';
import { View } from 'react-native';

import * as Colors from '../constants/Colors'

export default class Line extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[
                {
                    width: '100%',
                    height: 1,
                    backgroundColor: Colors.TEXT_S,
                    opacity: 0.1
                },
                this.props.style
            ]} />
        );
    }
}
