import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

export default class Error extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF', justifyContent: 'center', alignItems: 'center', paddingBottom: 60 }}>
                <Image source={require('../assets/404.png')} style={{ width: 296, height: 153, marginBottom: 10 }} />
                <Text style={{ color: '#828282', fontSize: 18, textAlign: 'center', fontWeight: 'bold' }}>{'Oops! Page Not Found'}</Text>
                <Text style={{ color: '#828282', fontSize: 14, textAlign: 'center' }}>{'Sorry, an error has occured.'}</Text>
                <Text style={{ color: '#828282', fontSize: 14, textAlign: 'center' }}>{'Requested page not found!'}</Text>
                <TouchableOpacity onPress={() => this.props.onPress ? this.props.onPress() : this.props.navigation.goBack()} style={{ height: 36, borderRadius: 4, borderColor: '#00A9F4', borderWidth: 1, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', marginTop: 20, marginBottom: 20, paddingLeft: 10, paddingRight: 10 }}>
                    <Image source={require('../assets/back_icon_green.png')} style={{width: 15, height: 15, marginRight: 10}} />
                    <Text style={{ color: '#00A9F4', fontSize: 14 }}>Back to previous page</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
