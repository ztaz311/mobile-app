import React, { Component } from 'react';
import {
    Dimensions, Linking,
    View, Text, Image
} from 'react-native';
import HTML from 'react-native-render-html';
import Touchable from '../screens_view/Touchable'
import * as Colors from '../constants/Colors'


export default class ItemNews extends Component {
    render() {
        const { data, style } = this.props
        return <View style={[style]}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 15 }}>
                <Image source={{ uri: data.Team.Logo }} style={{ width: 60, height: 60, borderWidth: 1, borderRadius: 30, borderColor: Colors.TEXT_S }} />
                <View style={{ marginLeft: 20 }}>
                    <Text style={{ color: Colors.TEXT_P, fontSize: 18 }}>{data.Team.Name}</Text>
                    <Text style={{ color: Colors.TEXT_S, fontSize: 16 }}>{data.CreatedAt}</Text>
                </View>
            </View>

            {/* <Text style={{ color: Colors.TEXT_P, fontSize: 18, marginTop: 12 }}>{data.Content}</Text> */}
            <HTML html={data.Content}
                imagesMaxWidth={Dimensions.get('window').width - 40}
                baseFontStyle={{ fontSize: 16 }}
                onLinkPress={(evt, href) => { Linking.openURL(href) }} />

            {/* button opt*/}
            <View style={{ flexDirection: 'row', alignSelf: 'stretch', marginTop: 12 }}>
                <Touchable
                    style={{ minWidth: 40, minHeight: 40, justifyContent: 'center' }}>
                    <Image source={require('../assets/like_empty.png')} />
                </Touchable>
                <Touchable
                    style={{ minWidth: 40, minHeight: 40, justifyContent: 'center' }}>
                    <Image source={require('../assets/share_2.png')} />
                </Touchable>

                <Touchable
                    style={{ minWidth: 40, minHeight: 40, justifyContent: 'center', alignItems: 'flex-end', position: 'absolute', right: 0 }}>
                    <Image source={require('../assets/more_2_contents.png')} />
                </Touchable>
            </View>
        </View>
    }
}