import * as Types from '../constants/ActionType';
const initialState = {
    searchValue: '',
    tabView: 0,
    loading: false,
    loadingEvent: false,
    loadingTeam: false,
    loadingVenue: false,
    errors: {},
    dataSearch: {
        eventList: {
            is_empty: false,
            events: [],
            current_page: 1,
            scroll_id: ''
        },
        venueList: {
            is_empty: false,
            venues: [],
            current_page: 1,
            scroll_id: ''
        },
        teamList: {
            is_empty: false,
            teams: [],
            current_page: 1,
            scroll_id: ''
        },
        data: {
            events: { data: [] },
            teams: { data: [] },
            venues: { data: [] },
        }
    },
    loadMoreTeam: false,
    loadMoreEvent: false,
    loadingFollowers: false,
    teamIdAction: 0,//De biet team nao dang thuc hien action: follow, ...
};

var findIndex = (datas, id) => {
    var result = -1;
    datas.forEach((data, index) => {
        if (data.Id === id) {
            result = index;
            return result;
        }
    });
    return result;
}

const search = (state = initialState, action) => {
    var index = -1;
    switch (action.type) {
        case Types.SEARCH_DATA_REQUEST:
            state.loading = true
            return { ...state };
        case Types.SEARCH_DATA_SUCCESS:
            state.dataSearch.data = action.data
            state.dataSearch.eventList.events = action.data.events.data
            state.dataSearch.venueList.venues = action.data.venues.data
            state.dataSearch.teamList.teams = action.data.teams.data
            state.dataSearch.eventList.scroll_id = action.data.events.scroll_id
            state.dataSearch.venueList.scroll_id = action.data.venues.scroll_id
            state.dataSearch.teamList.scroll_id = action.data.teams.scroll_id
            state.dataSearch.eventList.current_page = 1
            state.dataSearch.venueList.current_page = 1
            state.dataSearch.teamList.current_page = 1
            state.loadMoreTeam = action.data.teams.data.length > 0
            state.loadMoreEvent = action.data.events.data.length > 0
            state.loading = false
            return { ...state };
        case Types.SEARCH_DATA_FAILURE:
            state.loading = false
            return { ...state };

        case Types.CLEAR_DATA_SEARCH:

            state.dataSearch = {
                eventList: {
                    is_empty: false,
                    events: [],
                    current_page: 1,
                    scroll_id: ''
                },
                venueList: {
                    is_empty: false,
                    venues: [],
                    current_page: 1,
                    scroll_id: ''
                },
                teamList: {
                    is_empty: false,
                    teams: [],
                    current_page: 1,
                    scroll_id: ''
                },
                data: {
                    events: { data: [] },
                    teams: { data: [] },
                    venues: { data: [] },
                }

            }
            return { ...state };
        case Types.CLEAR_DATA_SEARCH_TAB_VIEW:
            state.dataSearch.eventList = {
                is_empty: false,
                events: [],
                current_page: 1
            }
            state.dataSearch.venueList = {
                is_empty: false,
                venues: [],
                current_page: 1
            }
            state.dataSearch.teamList = {
                is_empty: false,
                teams: [],
                current_page: 1,

            }
            return { ...state };
        case Types.SEARCH_EVENT_REQUEST:
            // state.dataSearch.eventList.events = []
            state.dataSearch.eventList.current_page = 1
            state.dataSearch.eventList.is_empty = true
            state.loadingEvent = true
            return { ...state };
        case Types.SEARCH_VENUE_REQUEST:
            state.loadingVenue = true
            return { ...state };
        case Types.SEARCH_TEAM_REQUEST:
            state.dataSearch.teamList.current_page = 1
            state.loadingTeam = true
            return { ...state };
        case Types.LOAD_MORE_SEARCH_TEAM_REQUEST:
            state.dataSearch.teamList.current_page = action.page
            state.loadMoreTeam = true
            return { ...state };
        case Types.LOAD_MORE_SEARCH_TEAM_FAILURE:
            state.loadMoreTeam = false
            return { ...state };
        case Types.LOAD_MORE_SEARCH_EVENT_REQUEST:
            state.loadMoreEvent = true
            state.dataSearch.eventList.current_page = action.page
            return { ...state };
        case Types.LOAD_MORE_SEARCH_EVENT_FAILURE:
            state.loadMoreEvent = false
            return { ...state };


        case Types.LOAD_MORE_SEARCH_EVENT_SUCCESS:
            var eventList = action.data.events
            state.dataSearch.eventList.events = state.dataSearch.eventList.events.concat(eventList.data);
            state.dataSearch.eventList.is_empty = state.dataSearch.eventList.events.length === 0 ? true : false;
            state.loadingEvent = false
            state.loadMoreEvent = eventList.data.length > 0
            state.dataSearch.eventList.scroll_id = eventList.scroll_id
            return { ...state };

        case Types.LOAD_MORE_SEARCH_TEAM_SUCCESS:
            var teamList = action.data.teams
            state.dataSearch.teamList.teams = state.dataSearch.teamList.teams.concat(teamList.data);
            state.dataSearch.teamList.is_empty = state.dataSearch.teamList.teams.length === 0 ? true : false;
            state.loadingTeam = false
            state.loadMoreTeam = teamList.data.length > 0
            state.dataSearch.teamList.scroll_id = teamList.scroll_id
            return { ...state };

        case Types.SEARCH_EVENT_SUCCESS:
            var eventList = action.data.events
            state.dataSearch.eventList.current_page = + 1
            state.dataSearch.eventList.events = eventList.data;
            state.dataSearch.eventList.is_empty = state.dataSearch.eventList.events.length === 0 ? true : false;
            state.loadingEvent = false
            state.loadMoreEvent = eventList.data.length > 0
            state.dataSearch.eventList.scroll_id = eventList.scroll_id
            return { ...state };

        case Types.SEARCH_VENUE_SUCCESS:
            state.loadingVenue = false
            state.dataSearch.venueList.venues = action.data.venues
            return { ...state };
        case Types.SEARCH_TEAM_SUCCESS:

            var teamList = action.data.teams
            if (!teamList.data.length) {
                state.loadMoreTeam = false,
                    state.dataSearch.teamList.is_empty = true
            }
            if (action.page == 1) {
                state.dataSearch.teamList.teams = teamList.data
            }
            if (action.page > 1) {
                if (action.page !== state.dataSearch.teamList.current_page) {
                    state.dataSearch.teamList.current_page = teamList.current_page
                    var data_new = state.dataSearch.teamList.teams.concat(teamList.data)
                    state.dataSearch.teamList.teams = data_new
                }

            }
            if (teamList.current_page < teamList.last_page) { state.loadMoreTeam = true } else { state.loadMoreTeam = false }

            state.loadingTeam = false
            return { ...state };


        case Types.SEARCH_EVENT_FAILURE:
            state.loadingEvent = false
            return { ...state };
        case Types.SEARCH_VENUE_FAILURE:
            state.loadingVenue = false
            return { ...state };
        case Types.SEARCH_TEAM_FAILURE:
            state.loadingTeam = false
            return { ...state };

        case Types.UPDATE_KEYWORD_SEARCH:
            state.searchValue = action.searchValue
            return { ...state };
        case Types.UPDATE_TABVIEW_SEARCH:
            state.tabView = action.tabView
            return { ...state };

        case Types.TEAM_FOLLOW_REQUEST:
            state.loadingFollowers = true;
            state.teamIdAction = action.teamId;
            return { ...state };
        case Types.TEAM_FOLLOW_SUCCESS:
            state.loadingFollowers = false;
            state.teamIdAction = 0;
            var teams = state.dataSearch.teamList.teams;
            var newTeams = teams.map((team) => {
                if (team.Id == action.teamId) {
                    team.IsFollow = team.IsFollow ? false : true;
                }
                return team;
            })

            state.dataSearch.teamList.teams = newTeams;
            return { ...state };
        case Types.TEAM_FOLLOW_FAILURE:
            state.loadingFollowers = false;
            state.teamIdAction = 0;
            return { ...state };
        case Types.LIKE_EVENT_IN_SEARCH_REQUEST:
        case Types.LIKE_EVENT_IN_SEARCH_FAILURE:
            if(state.dataSearch.eventList.events.length > 0) {
                var newEvents = state.dataSearch.eventList.events.map((event) => {
                    if (event.Id == action.id) {
                        event.Like = event.Like ? false : true;
                    }
                    return event;
                })
                state.dataSearch.eventList.events = newEvents;
            }
            return { ...state };

        default: return { ...state };
    }
}

export default search;