import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    team: {
        Events: []
    },
    teams: [],
    errors: {},
    loadingReport: false,
    loadingDelete: false,
    loadingMembers: false,
    loadingLeave: false,
    loadingRole: false,
    loadingInvite: false,
    loadingFollowers: false,
    loadingTeamNews: false,
    loadingCreateTeamNews: false,
    loadingImagesIns: false,
    loadingSetIns: false,
    isFetching: false,
    team_news: [],
    loadMore: false,
    is_empty: false,
    loadingEditTeamNews: false,
    itemTeamNewsEditing: null,
    errorsPasswordDelete: false,
    teamIdAction: 0,//De biet team nao dang thuc hien action: follow, ...
    home_teams: [],
    banners: [],
    loadMoreEvent: false,
    loadingEvent: false,
    events: [],
    loadMoreSaleReport: false,
    saleReport: [],
    loadingTeamNewsDetail: false,
    teamNewsDetail: {},
    loadingHome: false,
    Members: []
};
var findIndex = (datas, id) => {
    var result = -1;
    datas.forEach((data, index) => {
        if (data.Id === id) {
            result = index;
            return result;
        }
    });
    return result;
}
const team = (state = initialState, action) => {
    var index = -1;
    switch (action.type) {
        case Types.TEAM_DETAIL_REQUEST:
            state.loading = true;
            state.team = {
                Events: []
            };
            return { ...state };
        case Types.TEAM_DETAIL_SUCCESS:
            state.loading = false;
            state.team = action.data;
            return { ...state };
        case Types.TEAM_DETAIL_FAILURE:
            state.loading = false;
            if (action.status_code) {
                state.team.status_code = action.status_code;
            }
            state.errors = action.errors;
            return { ...state };

        case Types.TEAM_REPORT_REQUEST:
            state.loadingReport = true;
            return { ...state };
        case Types.TEAM_REPORT_SUCCESS:
            state.loadingReport = false;
            return { ...state };
        case Types.TEAM_REPORT_FAILURE:
            state.loadingReport = false;
            return { ...state };

        case Types.TEAM_DELETE_REQUEST:
            state.loadingDelete = true;
            state.errors = {};
            return { ...state };
        case Types.TEAM_DELETE_SUCCESS:
            state.loadingDelete = false;
            state.team = {
                Events: []
            };
            return { ...state };
        case Types.TEAM_DELETE_FAILURE:
            state.loadingDelete = false;
            state.errors = action.errors;
            return { ...state };

        case Types.TEAM_MEMBERSLIST_REQUEST:
            state.loadingMembers = true;
            return { ...state };
        case Types.TEAM_MEMBERSLIST_SUCCESS:
            state.loadingMembers = false;
            state.Members = action.data;
            return { ...state };
        case Types.TEAM_MEMBERSLIST_FAILURE:
            state.loadingMembers = false;
            return { ...state };

        case Types.TEAM_LEAVE_REQUEST:
            state.loadingLeave = true;
            return { ...state };
        case Types.TEAM_LEAVE_SUCCESS:
            state.loadingLeave = false;
            var members = state.Members;
            var newMembers = members.filter((member) => {
                if (member.Id != action.memberId) {
                    return member;
                }
            })
            state.Members = newMembers;
            if (action.isAuth) {
                state.team.Role = 'Guest';
            }
            return { ...state };
        case Types.TEAM_LEAVE_FAILURE:
            state.loadingLeave = false;
            return { ...state };

        case Types.TEAM_ROLE_REQUEST:
            state.loadingRole = true;
            return { ...state };
        case Types.TEAM_ROLE_SUCCESS:
            state.loadingRole = false;
            var members = state.Members;
            var newMembers = members.map((member) => {
                if (member.Id == action.memberId) {
                    member.Role = action.role;
                }
                return member;
            })
            state.Members = newMembers;
            return { ...state };
        case Types.TEAM_INVITE_FAILURE:
            state.loadingInvite = false;
            return { ...state };
        case Types.TEAM_INVITE_SUCCESS:
            state.loadingInvite = false;
            var members = state.Members;
            console.log(members)
            var newMembers = members.filter((member) => {
                console.log(member)
                if (member.Id != action.memberId) {
                    return member;
                }
            })
            console.log(newMembers)
            state.Members = newMembers;
            var followers = state.team.Followers;
            var newFollowers = followers.map((member) => {
                if (member.Id == action.memberId) {
                    member.Role = 'Invited';
                }
                return member;
            })
            state.team.Followers = newFollowers;
            return { ...state };
        case Types.TEAM_INVITE_FAILURE:
            state.loadingInvite = false;
            return { ...state };

        case Types.TEAM_LIST_FOLLOW_REQUEST:
            state.loadingFollowers = true;
            return { ...state };
        case Types.TEAM_LIST_FOLLOW_SUCCESS:
            state.loadingFollowers = false;
            state.team.Followers = action.data;
            return { ...state };
        case Types.TEAM_LIST_FOLLOW_FAILURE:
            state.loadingFollowers = false;
            return { ...state };

        case Types.TEAM_NEWSLIST_REQUEST:
            state.loadingTeamNews = true;
            return { ...state };
        case Types.TEAM_NEWSLIST_SUCCESS:
            state.loadingTeamNews = false;
            state.team.TeamNews = action.data;
            return { ...state };
        case Types.TEAM_NEWSLIST_FAILURE:
            state.loadingTeamNews = false;
            return { ...state };

        case Types.CLEAR_TEAM_LIST:
            state.teams = [];
            return { ...state };
        case Types.TEAM_LIST_REQUEST:
            state.loadMore = true;
            state.loading = true;
            state.is_empty = false;
            if (action.page === 1) {
                state.teams = [];
            }
            return { ...state };
        case Types.TEAM_LIST_SUCCESS:
            state.loading = false;
            if (action.page == 1) {
                state.teams = action.data;
            } else {
                state.teams = state.teams.concat(action.data)
            }
            state.loadMore = state.teams.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.TEAM_LIST_FAILURE:
            state.loadMore = false;
            state.loading = false;
            return { ...state };

        case Types.HOME_TEAM_LIST_REQUEST:
            state.loadingHome = true;
            return { ...state };
        case Types.HOME_TEAM_LIST_SUCCESS:
            state.loadingHome = false;
            state.home_teams = action.data;
            return { ...state };
        case Types.HOME_TEAM_LIST_FAILURE:
            state.loadingHome = false;
            return { ...state };

        case Types.TEAM_FOLLOW_REQUEST:
            state.loadingFollowers = true;
            state.teamIdAction = action.teamId;
            return { ...state };
        case Types.TEAM_FOLLOW_SUCCESS:
            state.loadingFollowers = false;
            state.teamIdAction = 0;
            var teams = state.teams;
            var newTeams = teams.map((team) => {
                if (team.Id == action.teamId) {
                    team.IsFollow = team.IsFollow ? false : true;
                }
                return team;
            })
            state.teams = newTeams;
            var home_teams = state.home_teams;
            var newHomeTeams = home_teams.map((team) => {
                if (team.Id == action.teamId) {
                    team.IsFollow = team.IsFollow ? false : true;
                }
                return team;
            })
            state.home_teams = newHomeTeams;
            if (state.team.Id == action.teamId) {
                state.team.FollowerCount = state.team.IsFollow ? parseInt(state.team.FollowerCount) - 1 : parseInt(state.team.FollowerCount) + 1
                state.team.IsFollow = !state.team.IsFollow
            }
            return { ...state };
        case Types.TEAM_FOLLOW_FAILURE:
            state.loadingFollowers = false;
            state.teamIdAction = 0;
            return { ...state };

        case Types.TEAM_IMAGEINS_REQUEST:
            state.loadingImagesIns = true;
            return { ...state };
        case Types.TEAM_IMAGEINS_SUCCESS:
            state.loadingImagesIns = false;
            state.team.InstagramImages = action.data;
            return { ...state };
        case Types.TEAM_IMAGEINS_FAILURE:
            state.loadingImagesIns = false;
            return { ...state };

        case Types.TEAM_SET_INSTAGRAM_REQUEST:
            state.loadingSetIns = true;
            return { ...state };
        case Types.TEAM_SET_INSTAGRAM_SUCCESS:
            state.loadingSetIns = false;
            state.team.InstagramImages = action.data;
            state.team.IsInstagram = true;
            return { ...state };
        case Types.TEAM_SET_INSTAGRAM_FAILURE:
            state.loadingSetIns = false;
            return { ...state };

        case Types.TEAM_REMOVE_INSTAGRAM_REQUEST:
            state.loadingSetIns = true;
            return { ...state };
        case Types.TEAM_REMOVE_INSTAGRAM_SUCCESS:
            state.loadingSetIns = false;
            state.team.InstagramImages = [];
            state.team.IsInstagram = false;
            return { ...state };
        case Types.TEAM_REMOVE_INSTAGRAM_FAILURE:
            state.loadingSetIns = false;
            return { ...state };

        case Types.TEAM_NEWS_CREATE_REQUEST:
            state.loadingCreateTeamNews = true;
            return { ...state };
        case Types.TEAM_NEWS_CREATE_SUCCESS:
            state.loadingCreateTeamNews = false;
            var newTeamNews = state.team.TeamNews
            newTeamNews.unshift(action.data)
            state.team.TeamNews = newTeamNews;
            return { ...state };
        case Types.TEAM_NEWS_CREATE_FAILURE:
            state.loadingCreateTeamNews = false;
            return { ...state };
        case Types.LIKE_TEAM_NEWS_REQUEST:
        case Types.LIKE_TEAM_NEWS_FAILURE:
            var newData = state.team_news;
            newData.map((data, key) => {
                if (data.Id === action.id) {
                    data.IsLike = !data.IsLike;
                }
            })
            state.team_news = newData;
            if (typeof state.teamNewsDetail.IsLike !== 'undefined') {
                state.teamNewsDetail.IsLike = !state.teamNewsDetail.IsLike;
            }
            return { ...state };
        case Types.UPDATE_LIKE_TEAM_NEWS_REQUEST:
        case Types.UPDATE_LIKE_TEAM_NEWS_FAILURE:
            if (state.team.TeamNews) {
                var newData = state.team.TeamNews;
                newData.map((data, key) => {
                    if (data.Id === action.id) {
                        data.IsLike = !data.IsLike;
                    }
                })
                state.team.TeamNews = newData;
            }
            return { ...state };
        case Types.LOAD_DATA_TEAM_NEWS_REQUEST:
            state.loadMore = true;
            return { ...state };
        case Types.LOAD_DATA_TEAM_NEWS_SUCCESS:
            state.team_news = action.page === 1 ? action.data : state.team_news.concat(action.data);
            state.loadMore = state.team_news.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_TEAM_NEWS_FAILURE:
            state.loadMore = false;
            return { ...state };

        case Types.DELETE_TEAM_NEWS_SUCCESS:
            index = findIndex(state.team_news, action.id);
            if (index !== -1) {
                state.team_news.splice(index, 1);
            }
            if (state.team.TeamNews) {
                index = findIndex(state.team.TeamNews, action.id);
                if (index !== -1) {
                    state.team.TeamNews.splice(index, 1);
                }
            }
            return { ...state };
        case Types.EDIT_TEAM_NEWS_REQUEST:
            state.loadingEditTeamNews = true;
            return { ...state };
        case Types.EDIT_TEAM_NEWS_SUCCESS:
            state.loadingEditTeamNews = false;
            state.itemTeamNewsEditing = action.data;
            return { ...state };
        case Types.EDIT_TEAM_NEWS_FAILURE:
            state.loadingEditTeamNews = false;
            return { ...state };
        case Types.UPDATE_TEAM_NEWS_REQUEST:
            state.isFetching = true;
            return { ...state };
        case Types.UPDATE_TEAM_NEWS_FAILURE:
            state.isFetching = false;
            return { ...state };
        case Types.UPDATE_TEAM_NEWS_SUCCESS:
            state.isFetching = false;
            // state.event.EventNews.unshift(action.data);
            index = findIndex(state.team_news, action.id);
            if (index !== -1) {
                state.team_news[index].Content = action.data.Content;
                state.team_news[index].ContentShort = action.data.ContentShort;
                state.team_news[index].Images = action.data.Images;
                state.team_news[index].Link = action.data.Link;
            }
            return { ...state };
        case Types.UPDATE_TEAM_NEWS_LIST_SUCCESS:
            state.isFetching = false;
            if (state.team.TeamNews) {
                index = findIndex(state.team.TeamNews, action.id);
                if (index !== -1) {
                    state.team.TeamNews[index].Content = action.data.Content;
                    state.team.TeamNews[index].ContentShort = action.data.ContentShort;
                    state.team.TeamNews[index].Images = action.data.Images;
                    state.team.TeamNews[index].Link = action.data.Link;
                }
            }
            return { ...state };

        case Types.CREATE_TEAM_REQUEST:
            state.loading = true;
            return { ...state };
        case Types.CREATE_TEAM_SUCCESS:
            state.loading = false;
            if (state.teams.length > 0) {
                state.teams.unshift(action.data)
                state.teams.pop();
                state.teams = newTeams;
            }
            state.home_teams.unshift(action.data)
            return { ...state };
        case Types.CREATE_TEAM_FAILURE:
            state.loading = false;
            return { ...state };
        case Types.UPDATE_TEAM_SUCCESS:
            state.loading = false;
            state.team = action.data;
            var teams = state.teams;
            var newTeams = teams.map((team) => {
                if (team.Id == action.teamId) {
                    team = action.data;
                }
                return team;
            })
            state.teams = newTeams;
            var home_teams = state.home_teams;
            var newHomeTeams = home_teams.map((team) => {
                if (team.Id == action.teamId) {
                    team = action.data;
                }
                return team;
            })
            state.home_teams = newHomeTeams;
            return { ...state };
        case Types.LOAD_BANNER_TEAM_SUCCESS:
            state.banners = action.data;
            return { ...state };

        case Types.CREATE_ABOUT_TEAM_REQUEST:
            state.isFetching = true;
            return { ...state };
        case Types.CREATE_ABOUT_TEAM_FAILURE:
            state.isFetching = false;
            return { ...state };
        case Types.CREATE_ABOUT_TEAM_SUCCESS:
            state.isFetching = false;
            state.team.AboutTeam = action.data.AboutTeam;
            return { ...state };
        case Types.LOAD_DATA_EVENT_BY_TEAM_REQUEST:
            state.loadMoreEvent = true;
            state.is_empty = false;
            if (action.page === 1) {
                state.events = [];
            }
            return { ...state };
        case Types.LOAD_DATA_EVENT_BY_TEAM_FAILURE:
            state.loadMoreEvent = false;
            return { ...state };
        case Types.LOAD_DATA_EVENT_BY_TEAM_SUCCESS:
            state.events = action.page === 1 ? action.data : state.events.concat(action.data);
            state.loadMoreEvent = state.events.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LIKE_EVENT_IN_TEAM_REQUEST:
        case Types.LIKE_EVENT_IN_TEAM_FAILURE:
            index = findIndex(state.events, action.id);
            if (index !== -1) {
                state.events[index].Like = !state.events[index].Like
            }
            if (state.team.Events) {
                index = findIndex(state.team.Events, action.id);
                if (index !== -1) {
                    state.team.Events[index].Like = !state.team.Events[index].Like
                }
            }
            return { ...state };
        case Types.LOAD_DATA_SALE_REPORT_REQUEST:
            state.is_empty = true;
            state.loadMoreSaleReport = true;
            if (action.page === 1) {
                state.saleReport = [];
            }
            return { ...state };
        case Types.LOAD_DATA_SALE_REPORT_SUCCESS:
            if (action.page === 1) {
                state.saleReport = action.data;
            } else {
                state.saleReport.Events = state.saleReport.Events.concat(action.data.Events);
            }
            state.loadMoreSaleReport = state.saleReport.Events.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_SALE_REPORT_FAILURE:
            state.is_empty = false;
            state.loadMoreSaleReport = false;
            return { ...state };
        case Types.LOAD_DATA_TEAM_NEWS_DETAIL_REQUEST:
            state.teamNewsDetail = {};
            state.loadingTeamNewsDetail = true;
            return { ...state };
        case Types.LOAD_DATA_TEAM_NEWS_DETAIL_SUCCESS:
            state.loadingTeamNewsDetail = false;
            state.teamNewsDetail = action.data;
            return { ...state };
        case Types.LOAD_DATA_TEAM_NEWS_DETAIL_FAILURE:
            if (action.status_code) {
                state.teamNewsDetail.status_code = action.status_code;
            }
            state.loadingTeamNewsDetail = false;
            return { ...state };
        default: return { ...state };
    }
}

export default team;