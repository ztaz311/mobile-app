import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    banners: [],
    errors:{},
};

const banner = (state = initialState, action) => {
    switch(action.type) {
        case Types.BANNER_LIST_REQUEST:
            state.loading = true;
            return {...state};
        case Types.BANNER_LIST_SUCCESS:
            state.loading = false;
            if (action.page == 1) {
                var banners = action.data;
            } else {
                var banners = state.banners.concat(action.data)
            }
            state.banners = banners;
            return {...state};
        case Types.BANNER_LIST_FAILURE:
            state.loading = false;
            return {...state};
        default: return {...state};
    }
}

export default banner;