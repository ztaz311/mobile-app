import { combineReducers } from 'redux';
import user from "./userReducer";
import team from "./teamReducer";
import event from "./eventReducer";
import venue from "./venueReducer";
import banner from "./bannerReducer";
import city from "./cityReducer";
import global from "./globalReducer";
import category from "./categoryReducer";
import ticket from "./ticketReducer";
import payment from "./paymentReducer";
import language from "./languageReducer";
import like from "./likeReducer";
import search from "./searchReducer";
import news from "./newsReducer";
const appReducers = combineReducers({
  user,
  team,
  event,
  venue,
  banner,
  city,
  global,
  category,
  ticket,
  payment,
  language,
  like,
  search,
  news
});
const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    const language = state.language;
    const global = state.global
    state = {}
    state.language = language;
    state.global = global;
  }
  return appReducers(state, action)
}
export default rootReducer;