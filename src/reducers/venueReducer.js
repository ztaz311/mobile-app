import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    venue: {},
    venues: [],
    errors:{},
};

const venue = (state = initialState, action) => {
    switch(action.type) {
        case Types.VENUE_LIST_REQUEST:
            state.loading = true;
            return {...state};
        case Types.VENUE_LIST_SUCCESS:
            state.loading = false;
            if (action.page == 1) {
                var venues = action.data;
            } else {
                var venues = state.venues.concat(action.data)
            }
            state.venues = venues;
            return {...state};
        case Types.VENUE_LIST_FAILURE:
            state.loading = false;
            return {...state};
        default: return {...state};
    }
}

export default venue;