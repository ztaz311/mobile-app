import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    cities: [],
    countries: [],
    districts: [],
    event_countries: [],
};

const city = (state = initialState, action) => {
    switch(action.type) {
        case Types.SELECT_CITY_REQUEST:
            state.loading = true;
            return {...state};
        case Types.SELECT_CITY_SUCCESS:
            state.loading = false;
            state.cities = action.data;
            return {...state};
        case Types.SELECT_CITY_FAILURE:
            state.loading = false;
            return {...state};
        case Types.SELECT_COUNTRY_REQUEST:
            state.loading = true;
            return {...state};
        case Types.SELECT_COUNTRY_SUCCESS:
            state.loading = false;
            state.countries = action.data;
            return {...state};
        case Types.SELECT_COUNTRY_EVENT_SUCCESS:
            state.loading = false;
            state.event_countries = action.data;
            return {...state};
        case Types.SELECT_COUNTRY_FAILURE:
            state.loading = false;
            return {...state};
        case Types.GET_DISTRICT_BY_CITY_REQUEST:
            state.loading = true;
            return {...state};
        case Types.GET_DISTRICT_BY_CITY_SUCCESS:
            state.loading = false;
            state.districts = action.data;
            return {...state};
        case Types.GET_DISTRICT_BY_CITY_FAILURE:
            state.loading = false;
            return {...state};
        default: return {...state};
    }
}

export default city;