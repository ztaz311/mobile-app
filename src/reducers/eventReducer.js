import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    loadingEventDetail: false,
    event: {},
    events: [],
    errors: {},
    hostsInfo: [],
    locations: [],
    venues: [],
    categories: [],
    list_events: [],
    events_map: [],
    event_news: [],
    list_likes_event: {
        data: [],
        total: 0
    },
    list_event_import: {
        data: [],
    },
    collaborates: [],
    invitations: [],
    list_team_apply: [],
    is_empty: false,
    loadMore: false,
    isFetching: false,
    itemEditing: null,
    loadingReport: false,
    itemEventNewsEditing: null,
    banners: [],
    tickets: [],
    loadingTicket: false,
    loadMoreLiked: false,
    loadingGetTicket: false,
    purchases: {
        data: [],
        totalPrice: 0,
        unit: 'vnd'
    },
    loadingPurchase: false,
    hostsInfoByEvent: [],
    loadingMember: false,
    members: [],
    memberInvited: [],
    loadingTicketSales: false,
    SaleReport: {
        SaleReportMain: {},
        detailSaleReports: [],
        Event: {},
    },
    loadingWithdraw: false,
    refunds: [],
    loadMoreRefund: false,
    loadMoreImport: false,
    totalRefunds: 0,
    loadingImportWithdraw: false,
    importWithdraws: [],
    InviteMembers: [],
    InviteFollowers: [],
    InviteSearch: [],
    loadMoreSearch: false,
    loadMoreMembers: true,
    loadMoreFollowers: true,
    list_event_invited: [],
    loadingInvite: false,
    loadingRefundAll: false,
    loadingSetRole: false,
    loadingDataWithdraw: false,
    dataWithdraw: {
        import: [],
        withdraw: null,
    },
    eventNewsDetail: {},
    loadingEventNewsDetail: false,
    refreshTicket: false,
    loadingHome: false,
};

var findIndex = (events, id) => {
    var result = -1;
    if (events.length > 0) {
        events.forEach((event, i) => {
            if (event.Id === id) {
                result = i;
                return result;
            }
        });
    }
    return result;
}
var findIndexUser = (events, id) => {
    var result = -1;
    if (events.length > 0) {
        events.forEach((event, i) => {
            if (event.UserId === id) {
                result = i;
                return result;
            }
        });
    }
    return result;
}

var findRefundIndex = (refunds, id) => {
    var result = -1;
    if (refunds.length > 0) {
        refunds.forEach((refund, i) => {
            if (refund.idTicketDetail == id) {
                result = i;
                return result;
            }
        });
    }
    return result;
}

const event = (state = initialState, action) => {
    var index = -1;
    switch (action.type) {
        case Types.EVENT_LIST_REQUEST:
            state.loadingHome = true;
            return { ...state };
        case Types.LOAD_DATA_HOST_INFO_REQUEST:
        case Types.LOAD_DATA_LOCATION_REQUEST:
        case Types.LOAD_DATA_VENUE_REQUEST:
        // case Types.LOAD_DATA_CATEGORY_REQUEST:
        case Types.LOAD_DATA_EVENT_NEWS_REQUEST:
        case Types.LOAD_DATA_COLLABORATING_TEAM_REQUEST:
        case Types.EDIT_EVENT_NEWS_REQUEST:
            state.loadMore = true;
            state.loading = true;
            state.is_empty = false;
            return { ...state };
        case Types.LOAD_DATA_EVENT_IMPORT_REQUEST:
            state.loadMoreImport = true;
            state.is_empty = false;
        case Types.LOAD_DATA_TEAM_APPLY_REQUEST:
            state.loadMoreEventApply = true;
            state.is_empty = false;

        case Types.LOAD_DATA_LIKE_EVENT_REQUEST:
            state.loadMoreLiked = true;
            state.is_empty = false;
            if(action.page === 1) {
                state.list_likes_event = {
                    data: [],
                    total: 0
                }
            }
            return { ...state };
        case Types.EDIT_EVENT_REQUEST:
            state.loading = true;
            state.itemEditing = null;
            return { ...state };


        case Types.EVENT_KICK_STAFF_SUCCESS:

            index = findIndex(state.members, action.body.Id)
            if (index !== -1) {
                state.members.splice(index, 1)
            }
            return { ...state };

        case Types.EVENT_ROLE_REQUEST:
            state.loadingSetRole = true
            index = findIndex(state.members, action.body.UserId)
            if (index !== -1) {
                state.members[index].Role = action.body.Role.charAt(0).toUpperCase() + action.body.Role.slice(1)
            }
            return { ...state };
        case Types.EVENT_ROLE_SUCCESS:
            state.loadingSetRole = false
            return { ...state };
        case Types.EVENT_ROLE_FAILURE:
            state.loadingSetRole = false
            index = findIndex(state.members, action.body.UserId)
            if (index !== -1) {
                state.members[index].Role = action.body.oldRole
            }
            return { ...state };


        case Types.UNINVITE_USER_REQUEST:
        case Types.UNINVITE_USER_FAILURE:
            var { userId } = action
            index = findIndex(state.memberInvited, userId)
            if (index == -1) {
                state.memberInvited.push({ Id: userId })
            } else {
                state.memberInvited.splice(index, 1)
            }
            return { ...state };

        case Types.UNINVITE_USER_SUCCESS:
            // var { userId, inviting } = action

            // index = findIndex(state.list_event_invited, userId)
            // if (index == -1 && inviting) {
            //     state.memberInvited.push({ Id: userId })

            // } else if (!inviting && index !== -1) {
            //     state.memberInvited.splice(index, 1)
            // }

            return { ...state };



        case Types.LOAD_INVITE_TEAM_MEMBERS:

            action.page == 1 ? state.InviteMembers = [] : null
            state.loadMoreMembers = true
            state.is_empty = true;
            return { ...state };
        case Types.LOAD_INVITE_TEAM_FOLLOWERS:
            action.page == 1 ? state.InviteFollowers = [] : null
            state.loadMoreFollowers = true;
            state.is_empty = false;
            return { ...state };
        case Types.LOAD_INVITE_TEAM_SEARCH:
            action.page == 1 ? state.InviteSearch = [] : null
            state.loadMoreSearch = true;
            state.is_empty = false;
            return { ...state };

        case Types.LOAD_INVITE_TEAM_MEMBERS_FAIL:
            state.loadMoreMembers = false;
            state.is_empty = true;
            return { ...state };
        case Types.LOAD_INVITE_TEAM_FOLLOWERS_FAILURE:
            state.loadMoreFollowers = false;
            state.is_empty = true;
            return { ...state };
        case Types.LOAD_INVITE_TEAM_SEARCH_FAILURE:
            state.loadMoreSAearch = false;
            state.is_empty = true;
            return { ...state };


        case Types.LOAD_DATA_EVENT_DETAIL_REQUEST:
            state.event = {};
            state.loadMore = true;
            state.loadingEventDetail = true;
            state.is_empty = false;
            return { ...state };

        case Types.LOAD_DATA_LIST_EVENT_REQUEST:
            state.loadMore = true;
            state.loading = true;
            state.is_empty = false;
            if (action.page === 1) {
                state.list_events = [];
            }
            return { ...state };
        case Types.EVENT_LIST_SUCCESS:
            state.loadingHome = false;
            if (action.page == 1) {
                var events = action.data;
            } else {
                var events = state.events.concat(action.data)
            }
            state.events = events;
            return { ...state };
        case Types.EVENT_LIST_FAILURE:
            state.loadingHome = false;
            return { ...state };
        case Types.LOAD_DATA_HOST_INFO_FAILURE:
        case Types.LOAD_DATA_LOCATION_FAILURE:
        case Types.LOAD_DATA_VENUE_FAILURE:
        // case Types.LOAD_DATA_CATEGORY_FAILURE:
        case Types.LOAD_DATA_LIST_EVENT_FAILURE:
        case Types.LOAD_DATA_EVENT_NEWS_FAILURE:
        case Types.LOAD_DATA_LIKE_EVENT_FAILURE:
        case Types.LOAD_DATA_COLLABORATING_TEAM_FAILURE:
        case Types.EDIT_EVENT_FAILURE:
        case Types.EDIT_EVENT_NEWS_FAILURE:
        case Types.LOAD_DATA_HOST_INFO_BY_TEAM_FAILURE:
            state.loadMore = false;
            state.loading = false;
            state.loadMoreLiked = false;
            return { ...state };

        case Types.LOAD_DATA_TEAM_APPLY_FAILURE:
            state.loadMoreEventApply = false;
            return { ...state };
        case Types.LOAD_DATA_EVENT_IMPORT_FAILURE:
            state.loadMoreImport = false;
            return { ...state };

        case Types.LOAD_DATA_EVENT_DETAIL_FAILURE:
            if (action.status_code) {
                state.event.status_code = action.status_code;
            }
            state.loadMore = false;
            state.loadingEventDetail = false;
            return { ...state };
        //invite event
        case Types.LOAD_INVITE_TEAM_SEARCH_SUCCESS:
            state.InviteSearch = action.page === 1 ? action.data : state.InviteSearch.concat(action.data);
            state.loadMoreSearch = state.InviteSearch.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_INVITE_TEAM_MEMBERS_SUCCESS:
            state.InviteMembers = action.page === 1 ? action.data : state.InviteMembers.concat(action.data);
            state.loadMoreMembers = state.InviteMembers.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_INVITE_TEAM_FOLLOWERS_SUCCESS:
            state.InviteFollowers = action.page === 1 ? action.data : state.InviteFollowers.concat(action.data);
            state.loadMoreFollowers = state.InviteFollowers.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        //
        case Types.INVITE_USER_REQUEST:
            var { userId } = action
            console.log(state.InviteFollowers)
            index = findIndex(state.InviteMembers, userId)
            if (index !== -1) {
                state.InviteMembers[index].Invite = 'Invited'
            }
            var index2 = findIndex(state.InviteFollowers, userId)
            if (index2 !== -1) {
                state.InviteFollowers[index2].Role = 'Invited'
            }
            var index3 = findIndex(state.InviteSearch, userId)
            if (index3 !== -1) {
                state.InviteSearch[index3].Role = 'Invited'
            }
            return { ...state };
        case Types.INVITE_USER_SUCCESS:
            var { userId, inviting } = action

            index = findIndex(state.memberInvited, userId)
            if (index == -1 && inviting) {
                state.memberInvited.push(action.item)
            }

            return { ...state };
        case Types.INVITE_USER_FAILURE:
            var { userId } = action
            index = findIndex(state.InviteMembers, userId)
            if (index !== -1) {
                state.InviteMembers[index].Invite = 'Invite'
            }
            var index2 = findIndex(state.InviteFollowers, userId)
            console.log(state.InviteFollowers, index2)
            if (index2 !== -1) {
                state.InviteFollowers[index2].Role = 'Invite'
            }
            var index3 = findIndex(state.InviteSearch, userId)
            if (index3 !== -1) {
                state.InviteSearch[index3].Role = 'Invite'
            }
            return { ...state };

        case Types.LOAD_DATA_HOST_INFO_SUCCESS:
            state.loading = false;
            state.hostsInfo = action.data;
            return { ...state };

        case Types.LOAD_DATA_LOCATION_SUCCESS:
            state.loading = false;
            state.locations = action.data;
            return { ...state };

        case Types.LOAD_DATA_VENUE_SUCCESS:
            state.loading = false;
            state.venues = action.data;
            return { ...state };

        // case Types.LOAD_DATA_CATEGORY_SUCCESS:
        //     state.loading = false;
        //     state.categories = action.data;
        //     return {...state};
        case Types.LOAD_DATA_LIST_EVENT_SUCCESS:
            state.loading = false;
            state.list_events = action.page === 1 ? action.data : state.list_events.concat(action.data);
            // if(action.page === 1) {
            //     state.events_map = action.data;
            // }
            state.loadMore = state.list_events.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };

        case Types.LOAD_DATA_LIKE_EVENT_SUCCESS:
            state.loading = false;
            state.list_likes_event.data = action.page === 1 ? action.data : state.list_likes_event.data.concat(action.data);
            state.list_likes_event.total = action.total;
            state.loadMoreLiked = state.list_likes_event.data.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };

        case Types.LOAD_DATA_EVENT_DETAIL_SUCCESS:
            state.loadingEventDetail = false;
            state.event = action.data;
            return { ...state };

        case Types.LOAD_DATA_EVENT_NEWS_SUCCESS:
            state.loading = false;
            state.event_news = action.page === 1 ? action.data : state.event_news.concat(action.data);
            state.loadMore = state.event_news.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };

        case Types.LOAD_DATA_COLLABORATING_TEAM_SUCCESS:
            state.loading = false;
            state.collaborates = action.collaborates;
            state.invitations = action.invitations;
            return { ...state };

        case Types.LIKE_EVENT_REQUEST:
        case Types.LIKE_EVENT_FAILURE:
            if(state.list_events.length > 0) {
                index = findIndex(state.list_events, action.id);
                if (index !== -1) {
                    state.list_events[index].Like = !state.list_events[index].Like
                }
            }
            if(state.events.length > 0) {
                index = findIndex(state.events, action.id);
                if (index !== -1) {
                    state.events[index].Like = !state.events[index].Like
                }
            }
            if(state.events_map.length > 0) {
                index = findIndex(state.events_map, action.id);
                if (index !== -1) {
                    state.events_map[index].Like = !state.events_map[index].Like
                }
            }
            if (state.event.RelatedEvent) {
                index = findIndex(state.event.RelatedEvent, action.id);
                if (index !== -1) {
                    state.event.RelatedEvent[index].Like = !state.event.RelatedEvent[index].Like
                }
            }
            if (state.event.Id === action.id) {
                state.event.Like = !state.event.Like;
            }
            return { ...state };
        case Types.LIKE_EVENT_SUCCESS:
            if (state.event.Id && action.user) {
                if (state.event.Like) {
                    state.event.ListLikes.push(action.user)
                    state.list_likes_event.total = state.list_likes_event.total + 1
                    state.list_likes_event.data.push(action.user)
                } else {
                    var datas = state.event.ListLikes.filter(data => data.Id === action.user.Id);
                    if (datas.length > 0) {
                        var i = state.event.ListLikes.indexOf(datas[0]);
                        state.event.ListLikes.splice(i, 1)
                    }
                    var datas2 = state.list_likes_event.data.filter(data2 => data2.Id === action.user.Id);
                    if (datas2.length > 0) {
                        var j = state.list_likes_event.data.indexOf(datas2[0]);
                        state.list_likes_event.data.splice(j, 1)
                    }
                    state.list_likes_event.total = state.list_likes_event.total - 1
                }
            }
            return { ...state };
        case Types.CONFIRM_COLLABORATE_SUCCESS:
            index = findIndex(state.invitations, action.id);
            if (index !== -1) {
                state.event.CollaborationTeams = state.event.CollaborationTeams.concat(state.invitations[index]);
                state.collaborates = state.collaborates.concat(state.invitations[index]);
                state.invitations.splice(index, 1);
            }
            return { ...state };

        case Types.DISMISS_COLLABORATE_SUCCESS:
            index = findIndex(state.collaborates, action.id);
            index2 = findIndex(state.event.CollaborationTeams, action.id);
            if (index2 !== -1) {
                state.event.CollaborationTeams.splice(index2, 1);
            }
            if (index !== -1) {
                state.collaborates.splice(index, 1);
            }
            return { ...state };

        case Types.DELETE_COLLABORATE_SUCCESS:
            index = findIndex(state.invitations, action.id);
            if (index !== -1) {
                state.invitations.splice(index, 1);
            }
            return { ...state };

        case Types.LOAD_DATA_TEAM_APPLY_SUCCESS:
            state.loading = false;
            state.list_team_apply = action.data;
            state.loadMoreEventApply = state.list_team_apply.length < action.total ? true : false;
            return { ...state };

        case Types.SAVE_EVENT_REQUEST:
        case Types.APPLY_COLLABORATE_REQUEST:
        case Types.SAVE_EVENT_NEWS_REQUEST:
        case Types.UPDATE_EVENT_NEWS_REQUEST:
            state.isFetching = true;
            return { ...state };

        case Types.APPLY_COLLABORATE_SUCCESS:
            state.isFetching = false;
            index = findIndex(state.list_team_apply, action.id);
            if (index !== -1) {
                state.list_team_apply.splice(index, 1);
            }
            return { ...state };

        case Types.APPLY_COLLABORATE_FAILURE:
        case Types.SAVE_EVENT_FAILURE:
        case Types.SAVE_EVENT_NEWS_FAILURE:
        case Types.UPDATE_EVENT_NEWS_FAILURE:
            state.isFetching = false;
            return { ...state };

        case Types.SAVE_EVENT_SUCCESS:
            state.isFetching = false;
            return { ...state };

        case Types.EVENT_DETAIL_TEAM_FOLLOW_SUCCESS:
            if (state.event.Team && (state.event.Team.Id === action.teamId)) {
                state.event.Team.IsFollow = state.event.Team.IsFollow ? false : true;
            }
            if (state.event.CollaborationTeams) {
                var newTeams = state.event.CollaborationTeams.map((team) => {
                    if (team.Id == action.teamId) {
                        team.IsFollow = team.IsFollow ? false : true;
                    }
                    return team;
                })
                state.event.CollaborationTeams = newTeams;
            }
            return { ...state };

        case Types.LOAD_DATA_EVENT_IMPORT_SUCCESS:
            state.loading = false;
            state.list_event_import.data = action.page === 1 ? action.data : state.list_event_import.data.concat(action.data);
            state.list_event_import.total = action.total;
            state.loadMoreImport = state.list_event_import.data.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };

        case Types.ADD_TEAM_TO_IMPORT:
            state.hostsInfo.unshift(action.data);
            return { ...state };

        case Types.SAVE_EVENT_NEWS_SUCCESS:
            state.isFetching = false;
            state.event.EventNews.unshift(action.data);
            return { ...state };

        case Types.UPDATE_EVENT_NEWS_LIST_SUCCESS:
            state.isFetching = false;
            index = findIndex(state.event_news, action.id);
            if (index !== -1) {
                state.event_news[index].Contents = action.data.Contents;
                state.event_news[index].ContentShort = action.data.ContentShort;
                state.event_news[index].Images = action.data.Images;
                state.event_news[index].Link = action.data.Link;
            }
            return { ...state };
        case Types.UPDATE_EVENT_NEWS_SUCCESS:
            state.isFetching = false;
            if (state.event.EventNews) {
                index = findIndex(state.event.EventNews, action.id);
                if (index !== -1) {
                    state.event.EventNews[index].Contents = action.data.Contents;
                    state.event.EventNews[index].ContentShort = action.data.ContentShort;
                    state.event.EventNews[index].Images = action.data.Images;
                    state.event.EventNews[index].Link = action.data.Link;
                }
            }
            return { ...state };

        case Types.EDIT_EVENT_SUCCESS:
            state.loading = false;
            state.itemEditing = action.data;
            return { ...state };

        case Types.EDIT_EVENT_NEWS_SUCCESS:
            state.loading = false;
            state.itemEventNewsEditing = action.data;
            return { ...state };

        case Types.UPDATE_EVENT_SUCCESS:
            state.event = action.data;
            state.isFetching = false;
            return { ...state };

        case Types.EVENT_DELETE_REQUEST:
        case Types.EVENT_CANCEL_REQUEST:
            state.isFetching = true;
            return { ...state };
        case Types.EVENT_DELETE_SUCCESS:
            state.isFetching = false;
            state.event = {};
            return { ...state };
        case Types.EVENT_CANCEL_SUCCESS:
            state.isFetching = false;
            state.event.Status = 0;
            return { ...state };
        case Types.EVENT_DELETE_FAILURE:
        case Types.EVENT_CANCEL_FAILURE:
            state.isFetching = false;
            return { ...state };

        case Types.EVENT_REPORT_REQUEST:
            state.loadingReport = true;
            return { ...state };
        case Types.EVENT_REPORT_SUCCESS:
            state.loadingReport = false;
            return { ...state };
        case Types.EVENT_REPORT_FAILURE:
            state.loadingReport = false;
            return { ...state };

        case Types.LIKE_EVENT_NEWS_REQUEST:
        case Types.LIKE_EVENT_NEWS_FAILURE:
            index = findIndex(state.event_news, action.id);
            if (index !== -1) {
                state.event_news[index].IsLike = !state.event_news[index].IsLike
            }
            if (typeof state.eventNewsDetail.IsLike !== 'undefined') {
                state.eventNewsDetail.IsLike = !state.eventNewsDetail.IsLike;
            }
            return { ...state };
        case Types.UPDATE_LIKE_EVENT_NEWS_REQUEST:
        case Types.UPDATE_LIKE_EVENT_NEWS_FAILURE:
            if (state.event.EventNews) {
                index = findIndex(state.event.EventNews, action.id);
                if (index !== -1) {
                    state.event.EventNews[index].IsLike = !state.event.EventNews[index].IsLike
                }
            }
            return { ...state };

        case Types.DELETE_EVENT_NEWS_SUCCESS:
            index = findIndex(state.event_news, action.id);
            if (index !== -1) {
                state.event_news.splice(index, 1);
            }
            if (state.event.EventNews) {
                index = findIndex(state.event.EventNews, action.id);
                if (index !== -1) {
                    state.event.EventNews.splice(index, 1);
                }
            }
            return { ...state };

        case Types.LOAD_EVENT_BY_MAPS_REQUEST:
            state.isFetching = true;
            state.events_map = [];
            return { ...state };
        case Types.LOAD_EVENT_BY_MAPS_SUCCESS:
            state.isFetching = false;
            state.events_map = action.data;
            return { ...state };
        case Types.LOAD_EVENT_BY_MAPS_FAILURE:
            state.isFetching = false;
            return { ...state };
        case Types.LOAD_BANNER_EVENT_SUCCESS:
            state.banners = action.data;
            return { ...state };
        case Types.LOAD_DATA_EVENT_TICKETS_REQUEST:
            state.loadingTicket = true;
            state.refreshTicket = false;
            state.tickets = [];
            return { ...state };
        case Types.LOAD_DATA_EVENT_TICKETS_SUCCESS:
            state.loadingTicket = false;
            state.tickets = action.data;
            return { ...state };
        case Types.LOAD_DATA_EVENT_TICKETS_FAILURE:
            state.loadingTicket = false;
            return { ...state };
        case Types.GET_TICKET_REQUEST:
            state.loadingGetTicket = true;
            return { ...state };
        case Types.GET_TICKET_SUCCESS:
            state.loadingGetTicket = false;
            return { ...state };
        case Types.GET_TICKET_FAILURE:
            state.loadingGetTicket = false;
            state.refreshTicket = true;
            return { ...state };
        case Types.GET_TICKET_FAILURE_NOT_REFRESH:
            state.loadingGetTicket = false;
            return { ...state };
        case Types.LOAD_DATA_PURCHASE_REQUEST:
            state.loadingPurchase = true;
            return { ...state };
        case Types.LOAD_DATA_PURCHASE_SUCCESS:
            state.loadingPurchase = false;
            state.purchases = action.data;
            return { ...state };
        case Types.LOAD_DATA_PURCHASE_FAILURE:
            state.loadingPurchase = false;
            return { ...state };
        case Types.LOAD_DATA_HOST_INFO_BY_TEAM_REQUEST:
            state.loadMore = true;
            state.loading = true;
            state.is_empty = false;
            if (action.page === 1) {
                state.hostsInfoByEvent = [];
            }
            return { ...state };
        case Types.LOAD_DATA_HOST_INFO_BY_TEAM_SUCCESS:
            state.loading = false;
            state.hostsInfoByEvent = action.page === 1 ? action.data : state.hostsInfoByEvent.concat(action.data);
            state.loadMore = state.hostsInfoByEvent.length < action.total ? true : false;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_MEMBERS_REQUEST:
            state.loadingMember = true;
            return { ...state };
        case Types.LOAD_DATA_MEMBERS_SUCCESS:
            state.loadingMember = false;
            state.members = action.members;
            state.memberInvited = action.memberInvited;
            if(Object.keys(state.event).length === 0) {
                state.event = action.event
            }
            return { ...state };
        case Types.LOAD_DATA_MEMBERS_FAILURE:
            state.loadingMember = false;
            return { ...state };
        case Types.LOAD_DATA_TICKET_SALES_REQUEST:
            state.loadingTicketSales = true;
            return { ...state };
        case Types.LOAD_DATA_TICKET_SALES_SUCCESS:
            state.loadingTicketSales = false;
            state.SaleReport = action.data;
            return { ...state };
        case Types.LOAD_DATA_TICKET_SALES_FAILURE:
            state.loadingTicketSales = false;
            return { ...state };
        case Types.WITHDRAW_REQUEST:
            state.loadingWithdraw = true;
            return { ...state };
        case Types.WITHDRAW_SUCCESS:
            state.loadingWithdraw = false;
            state.SaleReport.Event.Withdraw.Have = 1;
            state.SaleReport.Event.Withdraw.Status = 0;
            return { ...state };
        case Types.WITHDRAW_FAILURE:
            state.loadingWithdraw = false;
            return { ...state };
        case Types.LOAD_DATA_REFUND_REQUEST:
            state.is_empty = true;
            state.loadMoreRefund = true;
            if (action.page === 1) {
                state.refunds = [];
            }
            return { ...state };
        case Types.LOAD_DATA_REFUND_SUCCESS:
            state.refunds = action.page === 1 ? action.data : state.refunds.concat(action.data);
            state.loadMoreRefund = state.refunds.length < action.total ? true : false;
            state.totalRefunds = action.total;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_REFUND_FAILURE:
            state.is_empty = false;
            state.loadMoreRefund = false;
            return { ...state };
        case Types.LOAD_DATA_WITHDRAW_BY_TEAM_REQUEST:
            state.loadingImportWithdraw = true;
            return { ...state };
        case Types.LOAD_DATA_WITHDRAW_BY_TEAM_SUCCESS:
            state.loadingImportWithdraw = false;
            state.importWithdraws = action.data;
            return { ...state };
        case Types.LOAD_DATA_WITHDRAW_BY_TEAM_FAILURE:
            state.loadingImportWithdraw = false;
            return { ...state };
        case Types.REFUND_ALL_REQUEST:
        case Types.REFUSE_ALL_REQUEST:
            state.loadingRefundAll = true;
            return { ...state };
        case Types.REFUND_ALL_SUCCESS:
            state.loadingRefundAll = false;
            for (var i = 0; i < state.refunds.length; i++) {
                if (state.refunds[i].status == 0) {
                    state.refunds[i].status = "1";
                }
            }
            return { ...state };
        case Types.REFUND_ALL_FAILURE:
        case Types.REFUSE_ALL_FAILURE:
            state.loadingRefundAll = false;
            return { ...state };
        case Types.REFUSE_ALL_SUCCESS:
            state.loadingRefundAll = false;
            for (var i = 0; i < state.refunds.length; i++) {
                if (state.refunds[i].status == 0) {
                    state.refunds[i].status = "-2";
                }
            }
            return { ...state };
        case Types.UPDATE_STATUS_REFUND_LIST_SUCCESS:
            index = findRefundIndex(state.refunds, action.TicketDetailId);
            if (index !== -1) {
                state.refunds[index].status = action.Status;
            }
        case Types.DELETE_WITHDRAW_IMPORT_SUCCESS:
            index = findIndex(state.dataWithdraw.import, action.id);
            if (index !== -1) {
                state.dataWithdraw.import.splice(index, 1);
            }
            return { ...state };
        case Types.LOAD_DATA_WITHDRAW_REQUEST:
            state.loadingDataWithdraw = true;
            state.dataWithdraw.withdraw = null;
            return { ...state };
        case Types.LOAD_DATA_WITHDRAW_SUCCESS:
            state.loadingDataWithdraw = false;
            state.dataWithdraw = action.data;
            return { ...state };
        case Types.LOAD_DATA_WITHDRAW_FAILURE:
            state.loadingDataWithdraw = false;
            return { ...state };
        case Types.CLEAR_EVENT_LIST:
            state.list_events = [];
            return { ...state };
        case Types.CLEAR_ITEM_EVENT:
            state.event = {};
            return { ...state };
        case Types.LOAD_DATA_EVENT_NEWS_DETAIL_REQUEST:
            state.eventNewsDetail = {};
            state.loadingEventNewsDetail = true;
            return { ...state };
        case Types.LOAD_DATA_EVENT_NEWS_DETAIL_SUCCESS:
            state.loadingEventNewsDetail = false;
            state.eventNewsDetail = action.data;
            return { ...state };
        case Types.LOAD_DATA_EVENT_NEWS_DETAIL_FAILURE:
            if (action.status_code) {
                state.eventNewsDetail.status_code = action.status_code;
            }
            state.loadingEventNewsDetail = false;
            return { ...state };
        case Types.CLEAR_INVITE_SEARCH:
            state.InviteSearch = [];
            state.loadMoreSearch = false;
            return { ...state };
        default: return { ...state };
    }
}

export default event;