import * as Types from '../constants/ActionType';
const initialState = {
    loadMore: false,
    is_empty: false,
    dataNews: {
        idFirst: 0,
        data: [],
        loadMore: false,
        is_empty: false
    },
    dataUnreadNews: {
        idFirst: 0,
        data: [],
        loadMore: false,
        is_empty: false
    },
    dataReadNews: {
        idFirst: 0,
        data: [],
        loadMore: false,
        is_empty: false
    },
};

var findIndex = (datas, id) => {
    var result = -1;
    datas.forEach((data, index) => {
        if (data.Id === id) {
            result = index;
            return result;
        }
    });
    return result;
}

const news = (state = initialState, action) => {
    var index = -1;
    switch (action.type) {
        // news 
        case Types.LOAD_DATA_NEWS_REQUEST:
            state.dataNews.loadMore = true;
            state.dataNews.is_empty = false;
            return { ...state };
        case Types.LOAD_DATA_NEWS_SUCCESS:
            var list = action.data
            if (list.current_page == 1) {
                state.dataNews.data = list.data;
            } else {
                state.dataNews.data = state.dataNews.data.concat(list.data);
            }
            state.dataNews.is_empty = list.total === 0 ? true : false;

            if (list.current_page < list.last_page) {
                state.dataNews.loadMore = true
            } else { state.dataNews.loadMore = false }
            return { ...state };
        case Types.LOAD_DATA_NEWS_FAILURE:
            state.dataNews.loadMore = false;
            state.dataNews.is_empty = false;
            return { ...state };
        case Types.LOAD_DATA_UNREAD_NEWS_REQUEST:
            state.dataUnreadNews.loadMore = true;
            state.dataUnreadNews.is_empty = false;
            return { ...state };
        case Types.LOAD_DATA_UNREAD_NEWS_SUCCESS:
            var list = action.data
            if (list.current_page == 1) {
                state.dataUnreadNews.data = list.data;
            } else {
                state.dataUnreadNews.data = state.dataUnreadNews.data.concat(list.data);
            }
            state.dataUnreadNews.is_empty = list.total === 0 ? true : false;

            if (list.current_page < list.last_page) {
                state.dataUnreadNews.loadMore = true
            } else { state.dataUnreadNews.loadMore = false }
            return { ...state };
        case Types.LOAD_DATA_UNREAD_NEWS_FAILURE:
            state.dataUnreadNews.loadMore = false;
            state.dataUnreadNews.is_empty = false;
            return { ...state };
        case Types.LOAD_DATA_READ_NEWS_REQUEST:
            state.dataReadNews.loadMore = true;
            state.dataReadNews.is_empty = false;
            return { ...state };
        case Types.LOAD_DATA_READ_NEWS_SUCCESS:
            var list = action.data
            if (list.current_page == 1) {
                state.dataReadNews.data = list.data;
            } else {
                state.dataReadNews.data = state.dataReadNews.data.concat(list.data);
            }
            state.dataReadNews.is_empty = list.total === 0 ? true : false;

            if (list.current_page < list.last_page) {
                state.dataReadNews.loadMore = true
            } else { state.dataReadNews.loadMore = false }
            return { ...state };
        case Types.LOAD_DATA_READ_NEWS_FAILURE:
            state.dataReadNews.loadMore = false;
            state.dataReadNews.is_empty = false;
            return { ...state };
        case Types.CLEAR_DATA_NOTIFICATION:
            state.dataNews.data = [];
            return { ...state };
        case Types.READ_NEWS:
            index = findIndex(state.dataNews.data, action.newsId);
            if (index !== -1) {
                state.dataNews.data[index].IsJustRead = true;
            }
            index = findIndex(state.dataUnreadNews.data, action.newsId);
            if (index !== -1) {
                state.dataUnreadNews.data[index].IsJustRead = true;
            }
            return { ...state };
        case Types.MARK_READ_ALL_SUCCESS:
            var dataNews = state.dataNews.data;
            var newDataNews = dataNews.map((news) => {
                news.IsRead = 1;
                return news;
            })
            state.dataNews.data = newDataNews;
            var dataUnreadNews = state.dataUnreadNews.data;
            var newDataUnreadNews = dataUnreadNews.map((news) => {
                news.IsRead = 1;
                return news;
            })
            state.dataUnreadNews.data = newDataUnreadNews;
            return { ...state };
        default: return { ...state };
    }
}

export default news;