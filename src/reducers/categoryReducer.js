import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    categories: [],
};

const category = (state = initialState, action) => {
    switch(action.type) {
        case Types.LOAD_DATA_CATEGORY_REQUEST:
            state.loading = true;
            return {...state};
        case Types.LOAD_DATA_CATEGORY_SUCCESS:
            state.loading = false;
            state.categories = action.data;
            return {...state};
        case Types.LOAD_DATA_CATEGORY_FAILURE:
            state.loading = false;
            return {...state};
        default: return {...state};
    }
}

export default category;