import * as Types from '../constants/ActionType';
const initialState = {
    language: 'en'
};

const language = (state = initialState, action) => {
    switch(action.type) {
        case Types.CHANGE_LANGUAGE:
            state.language = action.data;
            return {...state};
        default: return {...state};
    }
}

export default language;