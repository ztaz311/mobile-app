import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    mytickets: [],
    loadingPastTicket: false,
    myPastTickets: [],
    loadingTicketByEvent: false,
    listTicketByEvent: [],
    loadingUseTicket: false,
    isShowModalUseTicket: false,
    isUseSuccess: false,
    messageUseTicket: '',
    loadingTicket: false,
    ticket: {},
    isShowModalTicket: false,
    isShowModalScanTicket: false,
    errors: {
        CodeScan: ''
    },
    isLoadTicketSuccess: false,
    isScanTicketSuccess: false,
    loadingRefund: false,
    itemRefund: {
        user: {},
        refund: {},
        refundComments: [],
        ticket: {},
        ticketDetail: {},
        eventPoster: {},
        slugEvent: ''
    },
    loadingPostRefund: false,
    Comment: '',
    holders: [],
    loadMoreHolder: false,
    is_empty: false,
    totalHolders: 0,
    loadingCommentHolder: false,
    loadingCancelRefund: false,
    loadingHandleRefund: false,
    handleRefund: {
        clientUser: {},
        user: {},
        refund: {},
        refundComments: [],
        ticket: {},
        ticketDetail: {},
        eventPoster: {},
        slugEvent: ''
    },
    loadingRefuseRefund: false,
    loadingPostHandleRefund: false,
    modalRefundInfo: false,
    loadingCreateRefundInfo: false,
    loadingAcceptRefund: false,
};

var findIndex = (tickets, id) => {
    var result = -1;
    tickets.forEach((ticket, i) => {
        if (ticket.Id === id) {
            result = i;
            return result;
        }
    });
    return result;
}

var findHolder = (holders, id) => {
    var result = -1;
    holders.forEach((holder, i) => {
        if (holder.PaymentId === id) {
            result = i;
            return result;
        }
    });
    return result;
}

const ticket = (state = initialState, action) => {
    var index = -1;
    switch (action.type) {
        case Types.LOAD_DATA_MY_TICKET_REQUEST:
            state.loading = true;
            return { ...state };
        case Types.LOAD_DATA_MY_TICKET_SUCCESS:
            state.loading = false;
            state.mytickets = action.data;
            return { ...state };
        case Types.LOAD_DATA_MY_TICKET_FAILURE:
            state.loading = false;
            return { ...state };
        case Types.LOAD_DATA_MY_PAST_TICKET_REQUEST:
            state.loadingPastTicket = true;
            if (state.page === 1) {
                state.myPastTickets = [];
            }
            return { ...state };
        case Types.LOAD_DATA_MY_PAST_TICKET_SUCCESS:
            state.loadingPastTicket = false;
            state.myPastTickets = action.data;
            return { ...state };
        case Types.LOAD_DATA_MY_PAST_TICKET_FAILURE:
            state.loadingPastTicket = false;
            return { ...state };
        case Types.LOAD_DATA_TICKET_PURCHASED_REQUEST:
            state.loadingTicketByEvent = true;
            return { ...state };
        case Types.LOAD_DATA_TICKET_PURCHASED_SUCCESS:
            state.loadingTicketByEvent = false;
            state.listTicketByEvent = action.data;
            return { ...state };
        case Types.LOAD_DATA_TICKET_PURCHASED_FAILURE:
            state.loadingTicketByEvent = false;
            return { ...state };
        case Types.USE_E_TICKET_REQUEST:
            state.loadingUseTicket = true;
            state.isShowModalUseTicket = false;
            state.isUseSuccess = false;
            return { ...state };
        case Types.USE_E_TICKET_SUCCESS:
            state.loadingUseTicket = false;
            state.isShowModalUseTicket = true;
            if (action.data.status == 200) {
                state.isUseSuccess = true;
                state.messageUseTicket = action.data.messenger.messenger;
                var tickets = state.mytickets[action.indexTicket].tickets;
                index = findIndex(tickets, action.id);
                if (index !== -1) {
                    tickets[index].Status = 1;
                    tickets[index].UseDate = action.data.data.Time;
                }
            } else {
                state.isUseSuccess = false;
                state.messageUseTicket = action.data.errors.CodeScan;
            }
            return { ...state };
        case Types.USE_E_TICKET_FAILURE:
            state.loadingUseTicket = false;
            state.isShowModalUseTicket = true;
            state.isUseSuccess = false;
            state.messageUseTicket = action.content;
            return { ...state };
        case Types.CLOSE_MODAL_USE_TICKET:
            state.isShowModalUseTicket = false;
            return { ...state };
        case Types.LOAD_DATA_TICKET_REQUEST:
            state.loadingTicket = true;
            return { ...state };
        case Types.LOAD_DATA_TICKET_SUCCESS:
            state.loadingTicket = false;
            state.isShowModalTicket = true;
            state.isLoadTicketSuccess = true;
            state.ticket = action.data;
            return { ...state };
        case Types.LOAD_DATA_TICKET_FAILURE:
            state.isShowModalTicket = true;
            state.isLoadTicketSuccess = false;
            state.errors = action.errors;
            state.loadingTicket = false;
            return { ...state };
        case Types.CLOSE_MODAL_DATA_TICKET:
            state.isShowModalTicket = false;
            return { ...state };
        case Types.SCAN_TICKET_REQUEST:
            state.isShowModalTicket = false;
            state.loadingTicket = true;
            return { ...state };
        case Types.SCAN_TICKET_SUCCESS:
            state.loadingTicket = false;
            state.isShowModalScanTicket = true;
            state.isScanTicketSuccess = true;
            return { ...state };
        case Types.SCAN_TICKET_FAILURE:
            state.isShowModalScanTicket = true;
            state.isScanTicketSuccess = false;
            state.error_scan = action.error;
            state.loadingTicket = false;
            return { ...state };
        case Types.CLOSE_MODAL_SCAN_TICKET:
            state.isShowModalScanTicket = false;
            return { ...state };
        case Types.LOAD_DATA_TICKET_HISTORY_REFUND_REQUEST:
            state.Comment = '';
            state.loadingRefund = true;
            return { ...state };
        case Types.LOAD_DATA_TICKET_HISTORY_REFUND_SUCCESS:
            state.loadingRefund = false;
            state.itemRefund.user = action.data.user;
            state.itemRefund.refund = action.data.refund;
            state.itemRefund.refundComments = action.data.refundComments ? action.data.refundComments : [];
            state.itemRefund.ticket = action.data.ticket;
            state.itemRefund.ticketDetail = action.data.ticketDetail ? action.data.ticketDetail : {};
            state.itemRefund.eventPoster = action.data.eventPoster ? action.data.eventPoster : {};
            state.itemRefund.slugEvent = action.data.slugEvent ? action.data.slugEvent : {};
            // state.itemRefund = action.data;
            return { ...state };
        case Types.LOAD_DATA_TICKET_HISTORY_REFUND_FAILURE:
            state.loadingRefund = false;
            return { ...state };
        case Types.REFUND_REQUEST:
            state.loadingPostRefund = true;
            state.itemRefund.refund.Zipcode = action.body.Zipcode;
            state.itemRefund.refund.Phone = action.body.Phone;
            return { ...state };
        case Types.REFUND_SUCCESS:
            state.loadingPostRefund = false;
            state.itemRefund.refundComments.push(action.data);
            state.itemRefund.ticketDetail.Status = -1;
            if(action.ticketType) {
                if (action.ticketType == 'current') {
                    var newData = state.mytickets[action.indexTicket].tickets;
                    newData.map((data, key) => {
                        if (data.Id === state.itemRefund.ticketDetail.Id) {
                            data.Status = -1;
                        }
                    })
                    state.mytickets[action.indexTicket].tickets = newData;
                } else {
                    var newData = state.myPastTickets[action.indexTicket].tickets;
                    newData.map((data, key) => {
                        if (data.Id === state.itemRefund.ticketDetail.Id) {
                            data.Status = -1;
                        }
                    })
                    state.myPastTickets[action.indexTicket].tickets = newData;
                }
            }
            state.itemRefund.ticketDetail.CheckRefund = true;
            return { ...state };
        case Types.REFUND_FAILURE:
            state.loadingPostRefund = false;
            return { ...state };
        case Types.SEND_COMMENT_REFUND_REQUEST:
            state.loadingPostComment = true;
            return { ...state };
        case Types.SEND_COMMENT_REFUND_SUCCESS:
            state.Comment = '';
            state.loadingPostComment = false;
            if (action.from === 'host') {
                state.handleRefund.refundComments.push(action.data);
            } else {
                state.itemRefund.refundComments.push(action.data);
            }
            return { ...state };
        case Types.SEND_COMMENT_REFUND_FAILURE:
            state.loadingPostComment = false;
            return { ...state };
        case Types.CHANGE_COMMENT:
            state.Comment = action.Comment;
            return { ...state };
        case Types.LOAD_DATA_HOLDER_REQUEST:
            state.is_empty = true;
            state.loadMoreHolder = true;
            if (action.page === 1) {
                state.holders = [];
            }
            return { ...state };
        case Types.LOAD_DATA_HOLDER_SUCCESS:
            state.holders = action.page === 1 ? action.data : state.holders.concat(action.data);
            state.loadMoreHolder = state.holders.length < action.total ? true : false;
            state.totalHolders = action.total;
            state.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_HOLDER_FAILURE:
            state.is_empty = false;
            state.loadMoreHolder = false;
            return { ...state };
        case Types.COMMENT_HOLDER_REQUEST:
            state.loadingCommentHolder = true;
            return { ...state };
        case Types.COMMENT_HOLDER_SUCCESS:
            state.loadingCommentHolder = false;
            index = findHolder(state.holders, action.data.PaymentId);
            if (index !== -1) {
                state.holders[index].commentHolderList = action.data.CommentHolder;
            }
            return { ...state };
        case Types.COMMENT_HOLDER_FAILURE:
            state.loadingCommentHolder = false;
            return { ...state };
        case Types.CANCEL_REFUND_REQUEST:
            state.loadingCancelRefund = true;
            return { ...state };
        case Types.CANCEL_REFUND_SUCCESS:
            state.loadingCancelRefund = false;
            if(action.ticketType) {
                if (action.ticketType == 'current') {
                    index = findIndex(state.mytickets[action.indexTicket].tickets, action.Id);
                    if (index !== -1) {
                        state.mytickets[action.indexTicket].tickets[index].CheckRefund = true;
                        state.mytickets[action.indexTicket].tickets[index].Status = 0;
                    }
                } else {
                    index = findIndex(state.myPastTickets[action.indexTicket].tickets, action.Id);
                    if (index !== -1) {
                        state.mytickets[action.indexTicket].tickets[index].CheckRefund = true;
                    }
                }
            }
            return { ...state };
        case Types.CANCEL_REFUND_FAILURE:
            state.loadingCancelRefund = false;
            return { ...state };
        case Types.ACCEPT_REFUND_REQUEST:
            state.loadingAcceptRefund = true;
            return { ...state };
        case Types.ACCEPT_REFUND_SUCCESS:
            state.loadingAcceptRefund = false;
            state.itemRefund.ticketDetail.Status = -6;
            if(action.indexTicket) {
                if (action.ticketType == 'current') {
                    index = findIndex(state.mytickets[action.indexTicket].tickets, action.ticketDetailId);
                } else {
                    index = findIndex(state.myPastTickets[action.indexTicket].tickets, action.ticketDetailId);
                }
                if (index !== -1) {
                    state.mytickets[action.indexTicket].tickets[index].Status = -6;
                }
            }
            return { ...state };
        case Types.ACCEPT_REFUND_FAILURE:
            state.loadingAcceptRefund = false;
            return { ...state };
        case Types.LOAD_DATA_TICKET_HANDLE_REFUND_REQUEST:
            state.Comment = '';
            state.loadingHandleRefund = true;
            return { ...state };
        case Types.LOAD_DATA_TICKET_HANDLE_REFUND_SUCCESS:
            state.loadingHandleRefund = false;
            state.handleRefund.user = action.data.user;
            state.handleRefund.clientUser = action.data.clientUser;
            state.handleRefund.refund = action.data.refund;
            state.handleRefund.refundComments = action.data.refundComments ? action.data.refundComments : [];
            state.handleRefund.ticket = action.data.ticket;
            state.handleRefund.ticketDetail = action.data.ticketDetail ? action.data.ticketDetail : {};
            state.handleRefund.eventPoster = action.data.eventPoster ? action.data.eventPoster : {};
            state.handleRefund.slugEvent = action.data.slugEvent ? action.data.slugEvent : {};
            // state.itemRefund = action.data;
            return { ...state };
        case Types.LOAD_DATA_TICKET_HANDLE_REFUND_FAILURE:
            state.loadingHandleRefund = false;
            return { ...state };
        case Types.REFUSE_REFUND_REQUEST:
            state.loadingRefuseRefund = true;
            return { ...state };
        case Types.REFUSE_REFUND_SUCCESS:
            state.loadingRefuseRefund = false;
            // index = findIndex(state.listTicketByEvent, action.Id);
            // if (index !== -1) {
            //     state.listTicketByEvent[index].CheckRefund = true;
            //     state.listTicketByEvent[index].Status = 0;
            // }
            state.handleRefund.ticketDetail.Status = -3;
            state.handleRefund.refundComments.push(action.data);
            return { ...state };
        case Types.REFUSE_REFUND_FAILURE:
            state.loadingRefuseRefund = false;
            return { ...state };
        case Types.HANDLE_REFUND_REQUEST:
            state.loadingPostHandleRefund = true;
            return { ...state };
        case Types.HANDLE_REFUND_SUCCESS:
            state.loadingPostHandleRefund = false;
            state.Comment = '';
            // index = findIndex(state.listTicketByEvent, action.Id);
            // if (index !== -1) {
            //     state.listTicketByEvent[index].CheckRefund = true;
            //     state.listTicketByEvent[index].Status = 0;
            // }
            state.handleRefund.ticketDetail.Status = -2;
            state.handleRefund.refund.Rate = action.Rate;
            state.handleRefund.refund.PriceRefund = state.handleRefund.ticket.Price * action.Rate / 100;
            return { ...state };
        case Types.HANDLE_REFUND_FAILURE:
            state.loadingPostHandleRefund = false;
            return { ...state };
        case Types.OPEN_MODAL_REFUND_INFO:
            state.modalRefundInfo = true;
            return { ...state };
        case Types.CLOSE_MODAL_REFUND_INFO:
            state.modalRefundInfo = false;
            return { ...state };
        case Types.CREATE_BANK_INFO_REQUEST:
            state.loadingCreateRefundInfo = true;
            return { ...state };
        case Types.CREATE_BANK_INFO_SUCCESS:
            state.loadingCreateRefundInfo = false;
            state.itemRefund.refund = action.data;
            state.itemRefund.ticketDetail.Status = -6;
            state.modalRefundInfo = false;
            var newData = state.mytickets[action.indexTicket].tickets;
            newData.map((data, key) => {
                if (data.Id === state.itemRefund.ticketDetail.Id) {
                    data.Status = -6;
                }
            })
            state.mytickets[action.indexTicket].tickets = newData;
            return { ...state };
        case Types.CREATE_BANK_INFO_FAILURE:
            state.loadingCreateRefundInfo = false;
            return { ...state };
        default: return { ...state };
    }
}

export default ticket;