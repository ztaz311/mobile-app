import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    data: {},
    is_buy_cod: false,
    loading_buy_cod: false,
    loading_e_ticket_free: false,
};

const payment = (state = initialState, action) => {
    switch(action.type) {
        case Types.PAYMENT_REQUEST:
            state.loading = true;
            return {...state};
        case Types.PAYMENT_SUCCESS:
            state.loading = false;
            state.data = action.data ? action.data : {};
            return {...state};
        case Types.PAYMENT_FAILURE:
            state.loading = false;
            return {...state};
        case Types.LOAD_PURCHASE_DETAIL:
            state.is_buy_cod = false;
            return {...state};
        case Types.PAYMENT_COD_REQUEST:
            state.loading_buy_cod = true;
            return {...state};
        case Types.PAYMENT_COD_SUCCESS:
            state.loading_buy_cod = false;
            state.is_buy_cod = true;
            return {...state};
        case Types.PAYMENT_COD_FAILURE:
            state.loading_buy_cod = false;
            return {...state};
        case Types.PAYMENT_E_TICKET_FREE_REQUEST:
            state.loading_e_ticket_free = true;
            return {...state};
        case Types.PAYMENT_E_TICKET_FREE_SUCCESS:
            state.loading_e_ticket_free = false;
            return {...state};
        case Types.PAYMENT_E_TICKET_FREE_FAILURE:
            state.loading_e_ticket_free = false;
            return {...state};
        default: return {...state};
    }
}

export default payment;