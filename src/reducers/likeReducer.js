import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    // banners: [],
    errors: {},
    liked: {
        events: [],
        posts: [],
        is_empty: false,
        loadMore: false
    },
    events: [],
    past_events: [],
    loadMoreEvent: false,
    is_event_empty: false,
    is_past_event_empty: false,
    loadMorePastEvent: false
};

var findIndex = (datas, id, type) => {
    var result = -1;
    if (datas.length > 0) {
        datas.forEach((data, i) => {
            var isType = type === 'event' ? data.Event: data.Team;
            if (data.Id === id && isType) {
                result = i;
                return result;
            }
        });
    }
    return result;
}

const like = (state = initialState, action) => {
    var index = -1;
    switch (action.type) {
        case Types.LOAD_DATA_LIKE_REQUEST:
            state.liked.loadMore = true;
            state.is_empty = false;
            return { ...state };
        case Types.LOAD_DATA_LIKE_SUCCESS:
            if (action.page == 1) {
                state.liked.events = action.data.events;
                state.liked.posts = action.data.posts.data;
            } else {
                state.liked.posts = state.liked.posts.concat(action.data.posts.data);
            }
            state.liked.loadMore = state.liked.posts.length < action.total ? true : false;
            state.liked.is_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_LIKE_FAILURE:
            state.is_empty = false;
            state.liked.loadMore = false;
            return { ...state };
        case Types.LOAD_DATA_EVENT_LIKE_REQUEST:
            state.loadMoreEvent = true;
            state.is_event_empty = false;
            // state.events = {};
            return { ...state };
        case Types.LOAD_DATA_EVENT_LIKE_SUCCESS:
            if (action.page == 1) {
                state.events = action.data;
            } else {
                state.events = state.events.concat(action.data);
            }
            state.loadMoreEvent = state.events.length < action.total ? true : false;
            state.liked.is_event_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_EVENT_LIKE_FAILURE:
            state.is_event_empty = false;
            state.loadMoreEvent = false;
            return { ...state };
        case Types.LOAD_DATA_PAST_EVENT_LIKE_REQUEST:
            state.loadMorePastEvent = true;
            state.is_past_event_empty = false;
            return { ...state };
        case Types.LOAD_DATA_PAST_EVENT_LIKE_SUCCESS:
            if (action.page == 1) {
                state.past_events = action.data;
            } else {
                state.past_events = state.past_events.concat(action.data);
            }
            state.loadMorePastEvent = state.past_events.length < action.total ? true : false;
            state.liked.is_past_event_empty = action.total === 0 ? true : false;
            return { ...state };
        case Types.LOAD_DATA_PAST_EVENT_LIKE_FAILURE:
            state.is_past_event_empty = false;
            state.loadMorePastEvent = false;
            return { ...state };
        case Types.UPDATE_LIKE_TEAM_NEWS_REQUEST:
        case Types.UPDATE_LIKE_TEAM_NEWS_FAILURE:
            if (state.liked.posts) {
                var newData = state.liked.posts;
                newData.map((data, key) => {
                    if (data.Id === action.id && data.Team) {
                        data.IsLike = !data.IsLike;
                    }
                })
                state.liked.posts = newData;
            }
            return { ...state };
        case Types.LIKE_EVENT_NEWS_REQUEST:
        case Types.LIKE_EVENT_NEWS_FAILURE:
            if (state.liked.posts) {
                var newData = state.liked.posts;
                newData.map((data, key) => {
                    if (data.Id === action.id && data.Event) {
                        data.IsLike = !data.IsLike;
                    }
                })
                state.liked.posts = newData;
            }
            return { ...state };
        case Types.LIKE_EVENT_REQUEST:
        case Types.LIKE_EVENT_FAILURE:
            if (state.events) {
                var newData = state.events;
                newData.map((data, key) => {
                    if (data.Id === action.id) {
                        data.Like = !data.Like;
                    }
                })
                state.events = newData;
            }
            if (state.liked.events) {
                var newData_ = state.liked.events;
                newData_.map((data, key) => {
                    if (data.Id === action.id) {
                        data.Like = !data.Like;
                    }
                })
                state.liked.events = newData_;
            }
            if (state.past_events) {
                var newData_ = state.past_events;
                newData_.map((data, key) => {
                    if (data.Id === action.id) {
                        data.Like = !data.Like;
                    }
                })
                state.past_events = newData_;
            }
            return { ...state };
        case Types.UPDATE_EVENT_NEWS_LIST_SUCCESS:
            index = findIndex(state.liked.posts, action.id, 'event');
            if (index !== -1) {
                state.liked.posts[index].Contents = action.data.Contents;
                state.liked.posts[index].ContentShort = action.data.ContentShort;
                state.liked.posts[index].Images = action.data.Images;
                state.liked.posts[index].Link = action.data.Link;
            }
            return { ...state };
        case Types.DELETE_EVENT_NEWS_SUCCESS:
            index = findIndex(state.liked.posts, action.id, 'event');
            if (index !== -1) {
                state.liked.posts.splice(index, 1);
            }
            return { ...state };
        case Types.UPDATE_TEAM_NEWS_LIST_SUCCESS:
            index = findIndex(state.liked.posts, action.id, 'team');
            if(index !== -1) {
                state.liked.posts[index].Content = action.data.Content;
                state.liked.posts[index].ContentShort = action.data.ContentShort;
                state.liked.posts[index].Images = action.data.Images;
                state.liked.posts[index].Link = action.data.Link;
            }
            return {...state};
        case Types.DELETE_TEAM_NEWS_SUCCESS:
            index = findIndex(state.liked.posts, action.id, 'team');
            if(index !== -1) {
                state.liked.posts.splice(index, 1);
            }
            return {...state};
        default: return { ...state };
    }
}

export default like;