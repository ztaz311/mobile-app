import * as React from 'react';
import { NavigationContainer, useLinking } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import firebase from 'react-native-firebase';
import 'react-native-gesture-handler';
import Splash from '../screens/Splash';
import EditProfile from '../screens/EditProfile';
import WelcomeBack from '../screens/WelcomeBack';
import Login from '../containers/LoginContainer';
import LoginWithMail from '../screens/LoginWithMail';
import Signup from '../screens/Signup';
import ForgotPw from '../screens/ForgotPw';
import RegisterOpt from '../screens/RegisterOpt';
import RegisterTeam from '../screens/RegisterTeam';
import SelectTeams from '../screens/SelectTeams';
import DetailTeam from '../screens/DetailTeam';
import CreateIntroTeam from '../screens/CreateIntroTeam';
import TeamListNews from '../screens/TeamListNews';
import CreateANews from '../screens/CreateANews';
import TeamList from '../screens/TeamList';
import TeamReport from '../screens/TeamReport';
import SaleReport from '../screens/SaleReport';
import TeamDelete from '../screens/TeamDelete';
import TeamMember from '../screens/TeamMember';
import TeamInvite from '../screens/TeamInvite';
import TeamFollowers from '../screens/TeamFollowers';
import ModalZoomImage from '../components/team/ModalZoomImage';
import EventHostList from '../screens/events/EventHostList';
import CreateEvent from '../screens/events/CreateEvent';
import CreateEventStep1 from '../screens/events/CreateEventStep1';
import CalendarSelect from '../screens/events/CalendarSelect';
import LocationSetting from '../screens/events/LocationSetting';
import PreviousLocation from '../screens/events/PreviousLocation';
import SelectCity from '../screens/events/SelectCity';
import SelectVenue from '../screens/events/SelectVenue';
import SelectCategory from '../screens/events/SelectCategory';
import EventTicket from '../screens/events/EventTicket';
import EventDetailSetting from '../screens/events/EventDetailSetting';
import ImportHostInfo from '../screens/events/ImportHostInfo';
import ImportEventInfo from '../screens/events/ImportEventInfo';
import EventMaps from '../screens/events/EventMaps';
import EventList from '../screens/events/EventList';
import EventMemberLiked from '../screens/events/EventMemberLiked';
import EventApplyColla from '../screens/events/EventApplyColla';
import CollaboratePart from '../screens/events/CollaboratePart';
import CollaboratingTeam from '../screens/events/CollaboratingTeam';
import EventNews from '../screens/events/EventNews';
import MapViews from '../screens/events/MapViews';
import WriteEventNews from '../screens/events/WriteEventNews';
import EventDelete from '../screens/events/EventDelete';
import EventReport from '../screens/events/EventReport';
import PurchaseTicket from '../screens/tickets/PurchaseTicket';
import PurchaseTicketStep2 from '../screens/tickets/PurchaseTicketStep2';
import PaymentOnline from '../screens/tickets/PaymentOnline';
import MyTicket from '../screens/tickets/MyTicket';
import MyPastTicket from '../screens/tickets/MyPastTicket';
import TicketByEvent from '../screens/tickets/TicketByEvent';
import TicketDetail from '../screens/tickets/TicketDetail';
import EventByTeams from '../screens/EventByTeams';
import PaymentSuccess from '../screens/tickets/PaymentSuccess';
import PurchaseDetail from '../screens/tickets/PurchaseDetail';
import WebViewEditDetail from '../screens/events/WebViewEditDetail';
import EventScanTicket from '../screens/tickets/EventScanTicket';
import RequestRefund from '../screens/events/RequestRefund';
import CancelEvent from '../screens/events/CancelEvent';
import DeleteEvent from '../screens/events/DeleteEvent';
import EventStaff from '../screens/events/EventStaff';
import EventInviteStaff from '../screens/events/EventInviteStaff';
import WithdrawRequest from '../screens/events/WithdrawRequest';
import RefundList from '../screens/events/RefundList';
import EventSaleReport from '../screens/events/EventSaleReport';
import HolderList from '../screens/tickets/HolderList';
import HolderListComment from '../screens/tickets/HolderListComment';
import ImportWithdraw from '../screens/events/ImportWithdraw';
import HandleRefund from '../screens/events/HandleRefund';
import EventNewsReport from '../screens/events/EventNewsReport';
import TeamNewsReport from '../screens/TeamNewsReport';
import EventLiked from '../screens/liked/EventLiked';
import VenueLiked from '../screens/liked/VenueLiked';
import PastEventLiked from '../screens/liked/PastEventLiked';
import SearchResult from '../screens/search/SearchResult';
import ResultSearchTabView from '../screens/search/ResultSearchTabView';
import FollowingTeam from '../screens/profile/FollowingTeam';
import HostingEvent from '../screens/profile/HostingEvent';
import PastEvent from '../screens/profile/PastEvent';
import Main from './Main';
import EventDetail from '../screens/events/EventDetail';
import EventNewsDetail from '../screens/events/EventNewsDetail';
import TeamNewsDetail from '../screens/teams/TeamNewsDetail';
import Error404 from '../screens_view/404Error';
import Logout from '../screens/profile/Logout';
import PaymentReset from '../screens/tickets/PaymentReset';
import ResetToHome from '../screens/tickets/ResetToHome';
import ReceivedRequest from '../screens/profile/ReceivedRequest';
import { navigationRef } from './NavigationService';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Remote debugger']);

console.disableYellowBox = true;

const getActiveRouteName = state => {
  const route = state.routes[state.index];

  if (route.state) {
    // Dive into nested navigators
    return getActiveRouteName(route.state);
  }

  return route.name;
};

const Stack = createStackNavigator();

function App() {
  const routeNameRef = React.useRef();

  const { getInitialState } = useLinking(navigationRef, {
    prefixes: ['https://www.comeup.asia', 'comeup://'],
    config: {
      RequestRefund: 'payment/:eventSlug/purchased/:ticketDetailId/refund',
      Main: {
        screens: {
          ticket: 'payment/:eventSlug/purchased'
        }
      },
      HandleRefund: 'events/:eventSlug/refund-request/:ticketDetailId',
      EventStaff: 'event/:Slug/members',
      EventDetail: 'events/:Slug',
      EventList: 'events',
      EventNewsDetail: 'event-news',
      DetailTeam: 'teams/:id',
      ReceivedRequest: 'settings/received-request',
      TeamNewsDetail: 'team-news',

      Error404: '404',
    },
  });

  // const [isReady, setIsReady] = React.useState(false);
  const [initialState, setInitialState] = React.useState();

  React.useEffect(() => {
    const state = navigationRef.current.getRootState();

    // Save the initial route name
    routeNameRef.current = getActiveRouteName(state);
    Promise.race([
      getInitialState(),
      new Promise(resolve =>
        // Timeout in 150ms if `getInitialState` doesn't resolve
        // Workaround for https://github.com/facebook/react-native/issues/25675
        setTimeout(resolve, 150)
      ),
    ])
      .catch(e => {
        console.error(e);
      })
      .then(state => {
        if (state !== undefined) {
          setInitialState(state);
        }

        // setIsReady(true);
      });
  }, [getInitialState]);

  // if (!isReady) {
  //   return null;
  // }
  return (
    <NavigationContainer initialState={initialState} ref={navigationRef} onStateChange={state => {
      const previousRouteName = routeNameRef.current;
      const currentRouteName = getActiveRouteName(state);

      if (previousRouteName !== currentRouteName) {
        firebase.analytics().setCurrentScreen(currentRouteName, currentRouteName + ' | ComeUp Mobile App');
      }

      // Save the current route name for later comparision
      routeNameRef.current = currentRouteName;
    }}>
      <Stack.Navigator initialRouteName="Splash" headerMode='none'>
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="EditProfile" component={EditProfile} />
        <Stack.Screen name="WelcomeBack" component={WelcomeBack} />
        <Stack.Screen name="LoginWithMail" component={LoginWithMail} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Signup" component={Signup} />
        <Stack.Screen name="ForgotPw" component={ForgotPw} />
        <Stack.Screen name="RegisterOpt" component={RegisterOpt} />
        <Stack.Screen name="RegisterTeam" component={RegisterTeam} />
        <Stack.Screen name="SelectTeams" component={SelectTeams} />
        <Stack.Screen name="DetailTeam" component={DetailTeam} />
        <Stack.Screen name="CreateIntroTeam" component={CreateIntroTeam} />
        <Stack.Screen name="TeamListNews" component={TeamListNews} />
        <Stack.Screen name="CreateANews" component={CreateANews} />
        <Stack.Screen name="TeamList" component={TeamList} />
        <Stack.Screen name="TeamReport" component={TeamReport} />
        <Stack.Screen name="SaleReport" component={SaleReport} />
        <Stack.Screen name="TeamDelete" component={TeamDelete} />
        <Stack.Screen name="TeamMember" component={TeamMember} />
        <Stack.Screen name="TeamInvite" component={TeamInvite} />
        <Stack.Screen name="TeamFollowers" component={TeamFollowers} />
        <Stack.Screen name="ModalZoomImage" component={ModalZoomImage} />
        <Stack.Screen name="EventHostList" component={EventHostList} />
        <Stack.Screen name="CreateEvent" component={CreateEvent} />
        <Stack.Screen name="CreateEventStep1" component={CreateEventStep1} />
        <Stack.Screen name="CalendarSelect" component={CalendarSelect} />
        <Stack.Screen name="LocationSetting" component={LocationSetting} />
        <Stack.Screen name="PreviousLocation" component={PreviousLocation} />
        <Stack.Screen name="SelectCity" component={SelectCity} />
        <Stack.Screen name="SelectVenue" component={SelectVenue} />
        <Stack.Screen name="SelectCategory" component={SelectCategory} />
        <Stack.Screen name="EventTicket" component={EventTicket} />
        <Stack.Screen name="EventDetailSetting" component={EventDetailSetting} />
        <Stack.Screen name="ImportHostInfo" component={ImportHostInfo} />
        <Stack.Screen name="ImportEventInfo" component={ImportEventInfo} />
        <Stack.Screen name="EventMaps" component={EventMaps} />
        <Stack.Screen name="EventList" component={EventList} />
        <Stack.Screen name="EventMemberLiked" component={EventMemberLiked} />
        <Stack.Screen name="EventDetail" component={EventDetail} />
        <Stack.Screen name="EventApplyColla" component={EventApplyColla} />
        <Stack.Screen name="CollaboratePart" component={CollaboratePart} />
        <Stack.Screen name="CollaboratingTeam" component={CollaboratingTeam} />
        <Stack.Screen name="EventNews" component={EventNews} />
        <Stack.Screen name="MapViews" component={MapViews} />
        <Stack.Screen name="WriteEventNews" component={WriteEventNews} />
        <Stack.Screen name="EventDelete" component={EventDelete} />
        <Stack.Screen name="EventReport" component={EventReport} />
        <Stack.Screen name="PurchaseTicket" component={PurchaseTicket} />
        <Stack.Screen name="PurchaseTicketStep2" component={PurchaseTicketStep2} />
        <Stack.Screen name="PaymentOnline" component={PaymentOnline} />
        <Stack.Screen name="MyTicket" component={MyTicket} />
        <Stack.Screen name="MyPastTicket" component={MyPastTicket} />
        <Stack.Screen name="TicketByEvent" component={TicketByEvent} />
        <Stack.Screen name="TicketDetail" component={TicketDetail} />
        <Stack.Screen name="EventByTeams" component={EventByTeams} />
        <Stack.Screen name="PaymentSuccess" component={PaymentSuccess} />
        <Stack.Screen name="PurchaseDetail" component={PurchaseDetail} />
        <Stack.Screen name="WebViewEditDetail" component={WebViewEditDetail} />
        <Stack.Screen name="EventScanTicket" component={EventScanTicket} />
        <Stack.Screen name="RequestRefund" component={RequestRefund} />
        <Stack.Screen name="CancelEvent" component={CancelEvent} />
        <Stack.Screen name="DeleteEvent" component={DeleteEvent} />
        <Stack.Screen name="EventStaff" component={EventStaff} />
        <Stack.Screen name="EventInviteStaff" component={EventInviteStaff} />
        <Stack.Screen name="WithdrawRequest" component={WithdrawRequest} />
        <Stack.Screen name="RefundList" component={RefundList} />
        <Stack.Screen name="EventSaleReport" component={EventSaleReport} />
        <Stack.Screen name="HolderList" component={HolderList} />
        <Stack.Screen name="HolderListComment" component={HolderListComment} />
        <Stack.Screen name="ImportWithdraw" component={ImportWithdraw} />
        <Stack.Screen name="HandleRefund" component={HandleRefund} />
        <Stack.Screen name="EventNewsReport" component={EventNewsReport} />
        <Stack.Screen name="TeamNewsReport" component={TeamNewsReport} />
        <Stack.Screen name="EventLiked" component={EventLiked} />
        <Stack.Screen name="VenueLiked" component={VenueLiked} />
        <Stack.Screen name="PastEventLiked" component={PastEventLiked} />
        <Stack.Screen name="SearchResult" component={SearchResult} />
        <Stack.Screen name="ResultSearchTabView" component={ResultSearchTabView} />
        <Stack.Screen name="FollowingTeam" component={FollowingTeam} />
        <Stack.Screen name="HostingEvent" component={HostingEvent} />
        <Stack.Screen name="PastEvent" component={PastEvent} />
        <Stack.Screen name="EventNewsDetail" component={EventNewsDetail} />
        <Stack.Screen name="TeamNewsDetail" component={TeamNewsDetail} />
        <Stack.Screen name="Error404" component={Error404} />
        <Stack.Screen name="Logout" component={Logout} />
        <Stack.Screen name="PaymentReset" component={PaymentReset} />
        <Stack.Screen name="ReceivedRequest" component={ReceivedRequest} />
        <Stack.Screen name="ResetToHome" component={ResetToHome} />
        <Stack.Screen name="Main" component={Main} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;