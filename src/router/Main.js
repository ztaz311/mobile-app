import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabHome from './TabHome';
import TabLikePage from './TabLikePage';
import TabTicket from './TabTicket';
// import TabNews from './TabNews';
import TabMyPage from './TabMyPage';
const Tab = createBottomTabNavigator();
import * as Colors from '../constants/Colors';
import { convertLanguage } from '../services/Helper';
import { actShowModalLogin } from '../actions/global';
import { connect } from "react-redux";
import { SafeAreaConsumer } from 'react-native-safe-area-context';
const hiddenLists = [
    'EventDetail',
    'EventHostList',
    'PurchaseTicket',
    'PurchaseTicketStep2',
    'PurchaseDetail',
    'PaymentOnline',
    'PaymentSuccess',
    'CreateEvent',
    'WriteEventNews',
    'TicketDetail',
    'EventMaps',
    'RegisterTeam',
    'CreateANews',
    'CreateIntroTeam',
    'WriteEventNews',
    'CalendarSelect',
    'LocationSetting',
    'SelectCategory',
    'EventTicket',
    'EventDetailSetting',
    'ImportEventInfo',
    'EventScanTicket',
    'HolderListComment',
    'HandleRefund',
    'RequestRefund',
    'WebViewEditDetail',
    'UpdatePassword'
]
class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    MyTabBar({ state, descriptors, navigation }) {
        var { profile } = this.props.user;
        var { language } = this.props.language;
        var tabBarVisible = (state.routes[state.index].state && (hiddenLists.indexOf(state.routes[state.index].state.routes[state.routes[state.index].state.index].name) !== -1 || (state.routes[state.index].state.routes[state.routes[state.index].state.index].params && state.routes[state.index].state.routes[state.routes[state.index].state.index].params['visible']))) ? false : true;
        return (
            <>
                {
                    tabBarVisible &&
                    <SafeAreaConsumer>
                        {
                            insets => <View style={{ flexDirection: 'row', height: 56 + (insets ? insets.bottom : 0), backgroundColor: 'rgb(255,255,255)', borderTopColor: 'rgb(199,199,204)', borderTopWidth: 0.333333, elevation: 8, paddingBottom: insets ? insets.bottom : 0, zIndex: -1 }}>
                                {state.routes.map((route, index) => {
                                    const { options } = descriptors[route.key];
                                    const label =
                                        options.tabBarLabel !== undefined
                                            ? options.tabBarLabel
                                            : options.title !== undefined
                                                ? options.title
                                                : route.name;

                                    const isFocused = state.index === index;
                                    const onPress = () => {
                                        if ((route.name === 'liked' || route.name === 'ticket' || route.name === 'news' || route.name === 'my') && !profile.Id) {
                                            this.props.onShowModalLogin();
                                        } else {
                                            const event = navigation.emit({
                                                type: 'tabPress',
                                                target: route.key,
                                            });

                                            if (!isFocused && !event.defaultPrevented) {
                                                navigation.navigate(route.name);
                                            } else if (route.state && route.state.index === 0) {
                                                const navigationInRoute = route.state.routes[0];
                                                if (!!navigationInRoute && !!navigationInRoute.params && !!navigationInRoute.params.scrollToTop) {
                                                    navigationInRoute.params.scrollToTop();
                                                }
                                            }
                                        }
                                    };

                                    const onLongPress = () => {
                                        navigation.emit({
                                            type: 'tabLongPress',
                                            target: route.key,
                                        });
                                    };

                                    let iconName;

                                    if (route.name === 'home') {
                                        iconName = isFocused
                                            ? require('../assets/home_active.png')
                                            : require('../assets/home.png');
                                    } else if (route.name === 'liked') {
                                        iconName = isFocused
                                            ? require('../assets/like_active.png')
                                            : require('../assets/like.png');
                                    } else if (route.name === 'ticket') {
                                        iconName = isFocused
                                            ? require('../assets/ticket_active.png')
                                            : require('../assets/ticket.png');
                                    } else if (route.name === 'my') {
                                        iconName = isFocused
                                            ? require('../assets/profile_active.png')
                                            : require('../assets/profile.png');
                                    }

                                    return (
                                        <TouchableOpacity
                                            activeOpacity={1}
                                            accessibilityRole="button"
                                            accessibilityStates={isFocused ? ['selected'] : []}
                                            accessibilityLabel={options.tabBarAccessibilityLabel}
                                            testID={options.tabBarTestID}
                                            onPress={onPress}
                                            onLongPress={onLongPress}
                                            key={index}
                                            style={{ flex: 1, alignItems: 'center', backgroundColor: 'transparent', justifyContent: 'flex-end', flexDirection: 'column' }}
                                        >
                                            <View style={{ alignItems: 'center', justifyContent: 'center', width: 24, height: 24 }}>
                                                <Image source={iconName} style={{ width: 24, height: 24 }} />
                                            </View>
                                            <Text style={{ color: isFocused ? '#00A9F4' : '#828282', fontSize: 12, marginBottom: 9, fontWeight: '400' }}>
                                                {convertLanguage(language, label)}
                                            </Text>
                                        </TouchableOpacity>
                                    );
                                })}
                            </View>
                        }
                    </SafeAreaConsumer>
                }
            </>
        );
    }

    render() {
        return (
            // <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
            <Tab.Navigator tabBar={props => this.MyTabBar(props)}>
                <Tab.Screen name='home' component={TabHome} />
                <Tab.Screen name='liked' component={TabLikePage} />
                <Tab.Screen name='ticket' component={TabTicket} />
                <Tab.Screen name='my' component={TabMyPage} />
            </Tab.Navigator>
            // </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        language: state.language,
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onShowModalLogin: () => {
            dispatch(actShowModalLogin())
        }

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
