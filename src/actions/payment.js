import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
import Toast from 'react-native-simple-toast';
import NavigationService from '../router/NavigationService';
import firebase from 'react-native-firebase';

export const actPayment = (body, dataAnalytics) => {
    firebase.analytics().logEvent('begin_checkout', dataAnalytics);
    return dispatch => {
        dispatch({type: Types.PAYMENT_REQUEST});
        return HTTP.callApiWithHeader('payment', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.PAYMENT_SUCCESS, data: response.data.data});
                if(response.data.TypePayment == 'online') {
                    NavigationService.navigate('PaymentOnline', {CodePayment: response.data.CodePayment})
                } else {
                    var purchaseInfo = response.data.purchaseInfo;
                    purchaseInfo.value = parseInt(purchaseInfo.value);
                    firebase.analytics().logEvent('purchase', purchaseInfo);
                    NavigationService.navigate('PaymentSuccess')
                }
            } else {
                dispatch({type: Types.PAYMENT_FAILURE});
                dispatch({ type: Types.GET_TICKET_FAILURE });
                NavigationService.goBack();
                if(response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
                } else if(response.data.errors && response.data.errors.Error) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.LONG, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({type: Types.PAYMENT_FAILURE});
        });
    };
}

export const actPaymentFree = (body, dataAnalytics) => {
    firebase.analytics().logEvent('begin_checkout', dataAnalytics);
    return dispatch => {
        dispatch({type: Types.PAYMENT_REQUEST});
        return HTTP.callApiWithHeader('payment-free', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                var purchaseInfo = response.data.purchaseInfo;
                purchaseInfo.value = parseInt(purchaseInfo.value);
                firebase.analytics().logEvent('purchase', purchaseInfo);
                dispatch({type: Types.PAYMENT_SUCCESS});
                NavigationService.navigate('PaymentSuccess')
                // NavigationService.navigate('PaymentSuccess')
            } else {
                dispatch({type: Types.PAYMENT_FAILURE});
                dispatch({ type: Types.GET_TICKET_FAILURE });
                NavigationService.goBack();
                if(response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({type: Types.PAYMENT_FAILURE});
            dispatch({ type: Types.GET_TICKET_FAILURE });
            NavigationService.goBack();
        });
    };
}

export const actLoadPurchaseDetail = () => {
    return dispatch => {
        dispatch({type: Types.LOAD_PURCHASE_DETAIL});
    };
}

export const actPaymentCod = () => {
    return dispatch => {
        dispatch({type: Types.PAYMENT_COD_REQUEST});
        return HTTP.callApiWithHeader('payments/cod', 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.PAYMENT_COD_SUCCESS});
            } else {
                dispatch({type: Types.PAYMENT_COD_FAILURE});
                if(response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({type: Types.PAYMENT_COD_FAILURE});
        });
    };
}

export const actPaymentCodFree = () => {
    return dispatch => {
        dispatch({type: Types.PAYMENT_COD_REQUEST});
        return HTTP.callApiWithHeader('payment-free/cod', 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.PAYMENT_COD_SUCCESS});
            } else {
                dispatch({type: Types.PAYMENT_COD_FAILURE});
                if(response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({type: Types.PAYMENT_COD_FAILURE});
        });
    };
}

export const actPaymentETicketFree = () => {
    return dispatch => {
        dispatch({type: Types.PAYMENT_E_TICKET_FREE_REQUEST});
        return HTTP.callApiWithHeader('payment-free/e-ticket', 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.PAYMENT_E_TICKET_FREE_SUCCESS});
                NavigationService.navigate('PaymentSuccess')
            } else {
                dispatch({type: Types.PAYMENT_E_TICKET_FREE_FAILURE});
                if(response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.LONG, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({type: Types.PAYMENT_E_TICKET_FREE_FAILURE});
        });
    };
}





