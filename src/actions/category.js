import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
import Toast from 'react-native-simple-toast';


export const actLoadDataCategory = (type) => {
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_CATEGORY_REQUEST});
        return HTTP.callApiWithHeader('tag-categories?type='+type, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_CATEGORY_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.LOAD_DATA_CATEGORY_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_CATEGORY_FAILURE});
        });
    };
}



