import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';


export const actSearch = (searchValue) => {
    return dispatch => {
        dispatch({
            type: Types.UPDATE_KEYWORD_SEARCH,
            searchValue
        });
    };
}

export const actUpdateTabView = (tabView) => {
    return dispatch => {
        dispatch({
            type: Types.UPDATE_TABVIEW_SEARCH,
            tabView
        });
    };
}
export const actClearDataSearch = () => {
    return dispatch => {
        dispatch({
            type: Types.CLEAR_DATA_SEARCH,
        });
    };
}
export const actClearDataSearchTabview = () => {
    return dispatch => {
        dispatch({
            type: Types.CLEAR_DATA_SEARCH_TAB_VIEW,
        });
    };
}

export const actSearchData = (searchValue, Id, type) => {
    searchValue = searchValue.replace(/#/g, "%23")
    var url = '';
    if(type === 'city') {
        url = `search?keyword=${searchValue}&city=${Id}`;
    } else {
        url = `search?keyword=${searchValue}&iso_code=${Id}`;
    }
    return dispatch => {
        dispatch({ type: Types.SEARCH_DATA_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({
                    type: Types.SEARCH_DATA_SUCCESS,
                    data: response.data
                });
            } else {
                dispatch({ type: Types.SEARCH_DATA_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.SEARCH_DATA_FAILURE });
        });
    };
}

export const actSearchEvent = (searchValue, Id, page = 1, per_page = 10) => {
    searchValue = searchValue.replace(/#/g, "%23")
    return dispatch => {
        dispatch({ type: Types.SEARCH_EVENT_REQUEST });
        return HTTP.callApiWithHeader(`event/search?keyword=${searchValue}&city=${Id}&page=${page}&per_page=${per_page}`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({
                    type: Types.SEARCH_EVENT_SUCCESS,
                    data: response.data
                });
            } else {
                dispatch({ type: Types.SEARCH_EVENT_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.SEARCH_EVENT_FAILURE });
        });
    };
}

export const actLoadMoreEvent = (searchValue, Id, page = 1, scroll_id = '', per_page = 12) => {
    searchValue = searchValue.replace(/#/g, "%23")
    return dispatch => {
        dispatch({ type: Types.LOAD_MORE_SEARCH_EVENT_REQUEST, page });
        return HTTP.callApiWithHeader(`event/search?keyword=${searchValue}&city=${Id}&page=${page}&scroll_id=${scroll_id}&per_page=${per_page}`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({
                    type: Types.LOAD_MORE_SEARCH_EVENT_SUCCESS,
                    data: response.data
                });
            } else {
                dispatch({ type: Types.LOAD_MORE_SEARCH_EVENT_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_MORE_SEARCH_EVENT_FAILURE });
        });
    };
}


export const actSearchVenue = (searchValue, Id) => {
    searchValue = searchValue.replace(/#/g, "%23")
    return dispatch => {
        dispatch({ type: Types.SEARCH_VENUE_REQUEST });
        return HTTP.callApiWithHeader(`venue/search?keyword=${searchValue}&city=${Id}`, 'GET', null).then(response => {

            if (response && response.data.status == 200) {

                dispatch({
                    type: Types.SEARCH_VENUE_SUCCESS,
                    data: response.data
                });
            } else {
                dispatch({ type: Types.SEARCH_VENUE_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.SEARCH_VENUE_FAILURE });
        });
    };
}

export const actSearchTeam = (searchValue, Id, page = 1, per_page = 12) => {
    searchValue = searchValue.replace(/#/g, "%23")
    return dispatch => {
        page > 1 ? dispatch({ type: Types.LOAD_MORE_SEARCH_TEAM_REQUEST, page }) : dispatch({ type: Types.SEARCH_TEAM_REQUEST })

        return HTTP.callApiWithHeader(`team/search?keyword=${searchValue}&city=${Id}&page=${page}&per_page=${per_page}`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {

                dispatch({
                    type: Types.SEARCH_TEAM_SUCCESS,
                    data: response.data,
                    page
                });
            } else {
                dispatch({ type: Types.SEARCH_TEAM_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.SEARCH_TEAM_FAILURE });
        });
    };
}

export const actLoadMoreTeam = (searchValue, Id, page = 1, scroll_id = '', per_page = 12) => {
    searchValue = searchValue.replace(/#/g, "%23")
    return dispatch => {
        dispatch({ type: Types.LOAD_MORE_SEARCH_TEAM_REQUEST, page });
        return HTTP.callApiWithHeader(`team/search?keyword=${searchValue}&city=${Id}&page=${page}&per_page=${per_page}&scroll_id=${scroll_id}`, 'GET', null).then(response => {

            if (response && response.data.status == 200) {
                dispatch({
                    type: Types.LOAD_MORE_SEARCH_TEAM_SUCCESS,
                    data: response.data
                });
            } else {
                dispatch({ type: Types.LOAD_MORE_SEARCH_TEAM_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_MORE_SEARCH_TEAM_FAILURE });
        });
    };
}





