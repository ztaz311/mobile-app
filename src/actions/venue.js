import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';


export const actVenueLists = (id, type) => {
    var url = '';
    if(type === 'city') {
        url = `venues?page=1&per_page=12&city_id=${id}`;
    } else {
        url = `venues?page=1&per_page=12&iso_code=${id}`;
    }
    return dispatch => {
        dispatch({type: Types.VENUE_LIST_REQUEST});
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.VENUE_LIST_SUCCESS, data: response.data.data, page: 1});
            } else {
                dispatch({type: Types.VENUE_LIST_FAILURE});
                // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.VENUE_LIST_FAILURE});
        });
    };
}




