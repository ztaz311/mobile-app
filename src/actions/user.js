import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
import { Alert, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import Toast from 'react-native-simple-toast';
import * as Config from '../constants/Config';
import { convertLanguage } from '../services/Helper';
import { CommonActions } from '@react-navigation/native';
import NavigationService from '../router/NavigationService';
export const actLogin = (auKey, auValue, goToWelcome, navigation, type = '', register = false) => {
    return dispatch => {
        dispatch({ type: Types.LOGIN_REQUEST });
        let headers = {}
        headers[auKey] = auValue;
        return HTTP.callApi('login', 'post', null, headers).then(response => {
            response = response.data;
            if (response.status == 200) {
                var login_type = 'account login';
                switch (auKey) {
                    case 'Fbtoken':
                        login_type = 'facebook'
                        break;
                    case 'Ggtoken':
                        login_type = 'google'
                        break;
                    case 'Aptoken':
                        login_type = 'apple'
                        break;
                    default:
                        break;
                }
                firebase.analytics().logEvent('login', { username: response.data.UserId, login_type: login_type, device: 'Mobile App', fullname: response.data.Name, member_id: response.data.Id });
                let arr = []
                arr.push(['auKey', 'Authorization'])
                arr.push(['auValue', 'Bearer ' + response.data.token])
                AsyncStorage.multiSet(arr, () => {
                    dispatch({ type: Types.LOGIN_SUCCESS, data: response.data });
                    if (type === 'modal') {
                        if (response.data.newUser || !response.data.UserId) {
                            navigation.navigate('WelcomeBack', { type: 'modal' })
                        }
                        dispatch({ type: Types.HIDE_MODAL_LOGIN });
                    } else if (type === 'login_mail') {
                        if (response.data.newUser || !response.data.UserId) {
                            navigation.replace('WelcomeBack', { type: 'modal' })
                        } else {
                            navigation.goBack();
                        }
                    } else {
                        navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: response.data.newUser || !response.data.UserId ? 'WelcomeBack' : 'Main' },
                                ],
                            })
                        );
                    }
                })
                var language = response.data.Language ? response.data.Language : 'en';
                dispatch({ type: Types.CHANGE_LANGUAGE, data: language });
                AsyncStorage.setItem('lang', language);
            } else {
                dispatch({ type: Types.LOGIN_FAILURE, errors: response.errors });
                if (response.errors.message) {
                    Toast.showWithGravity(JSON.stringify(response.errors.message), Toast.LONG, Toast.BOTTOM)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOGIN_FAILURE, errors: {} });
        });
    };
}

export const actCheckLogin = (auKey, auValue, goToWelcome, navigation) => {
    return dispatch => {
        dispatch({ type: Types.CHECK_LOGIN_REQUEST });
        let headers = {}
        headers[auKey] = auValue;
        return HTTP.callApi('users/profile', 'get', null, headers).then(response => {
            response = response.data;
            if (response.status == 200) {
                let arr = []
                arr.push(['auKey', auKey])
                arr.push(['auValue', auValue])
                AsyncStorage.multiSet(arr, () => {
                    dispatch({ type: Types.CHECK_LOGIN_SUCCESS, data: response.data });
                    // navigation.replace(goToWelcome ? 'WelcomeBack' : 'Main', { data: response.data })
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 1,
                            routes: [
                                { name: goToWelcome ? 'WelcomeBack' : 'Main', params: { data: response.data } },
                                // {
                                //   name: goToWelcome ? 'WelcomeBack' : 'home',
                                //   params: { data: response.data,register },
                                // },
                            ],
                        })
                    );
                })
                if (response.data.Language) {
                    dispatch({ type: Types.CHANGE_LANGUAGE, data: response.data.Language });
                    AsyncStorage.setItem('lang', response.data.Language);
                }
            } else if (response.status == 401) {
                dispatch({ type: Types.CHECK_LOGIN_FAILURE });
                AsyncStorage.removeItem('auKey')
                AsyncStorage.removeItem('auValue')
                navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [
                            { name: 'Login' },
                        ],
                    })
                );
            } else {
                dispatch({ type: Types.CHECK_LOGIN_FAILURE, error_status: 500 });
            }
        }).catch(function (error) {
            dispatch({ type: Types.CHECK_LOGIN_FAILURE, error_status: 500 });
        });
    };
}


export const actSearchMember = (keyword, teamId) => {
    return dispatch => {
        dispatch({ type: Types.USER_SEARCH_REQUEST });
        let body = {};

        return HTTP.callApiWithHeader('search/users?search=' + keyword + '&teamid=' + teamId, 'get', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.USER_SEARCH_SUCCESS, data: response.data.data });
                // Toast.showWithGravity(response.data.messages, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.USER_SEARCH_FAILURE });
            } else {
                dispatch({ type: Types.USER_SEARCH_FAILURE });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            }
            dispatch({ type: Types.USER_SEARCH_FAILURE });
        }).catch(function (error) {
            dispatch({ type: Types.USER_SEARCH_FAILURE });
        });
    };
}

export const actUpdateAvatar = (body) => {
    return dispatch => {
        return HTTP.callApiWithHeader('users/update-avatar', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.UPDATE_AVATAR_SUCCESS, data: response.data.data });
                Toast.showWithGravity('Update success', Toast.LONG, Toast.TOP)
            } else {
                Toast.showWithGravity('Update error', Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            Toast.showWithGravity('Update error', Toast.LONG, Toast.TOP)
        });
    };
}

export const actCheckLoginSocial = (body, type) => {
    return dispatch => {
        dispatch({ type: Types.CHECK_LOGIN_SOCIAL_REQUEST });
        return HTTP.callApiWithHeader('check-login-social', 'POST', body).then(response => {
            console.log(response)
            if (response && response.data.status === 200) {
                dispatch({ type: Types.CHECK_LOGIN_SOCIAL_SUCCESS, login_type: type });
            } else {
                dispatch({ type: Types.CHECK_LOGIN_SOCIAL_FAILURE, errors: response.data.errors, login_type: type });
            }
        }).catch(function (error) {
            dispatch({ type: Types.CHECK_LOGIN_SOCIAL_FAILURE, errors: {}, login_type: type });
        });
    };
}

export const actRemoveConnectFacebook = (type, language) => {
    if (type == 'gg') {
        var url = 'google-remove-link';
    } else if (type == 'fb') {
        var url = 'facebook-remove-link';
    } else {
        var url = 'apple-remove-link';
    }
    return dispatch => {
        dispatch({ type: Types.REMOVE_CONNECT_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            console.log(response)
            if (response && response.data.status === 200) {
                Toast.showWithGravity(response.data.messages.messages, Toast.LONG, Toast.TOP)
                dispatch({ type: Types.REMOVE_CONNECT_SUCCESS, remove_type: type });
            } else if (response && response.data.status === 401) {
                dispatch({ type: Types.REMOVE_CONNECT_FAILURE });
                Alert.alert(
                    convertLanguage(language, 'no_password'),
                    convertLanguage(language, 'please_add_a_password_before_deleting_the_link'),
                    [
                        {
                            text: convertLanguage(language, 'cancel'),
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                        { text: convertLanguage(language, 'ok'), onPress: () => Linking.openURL(Config.MAIN_URL + '/forgot-password') },
                    ],
                    { cancelable: false },
                );
            } else {
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
                dispatch({ type: Types.REMOVE_CONNECT_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.REMOVE_CONNECT_FAILURE, errors: {} });
        });
    };
}

export const actUpdateProfile = (body) => {
    return dispatch => {
        dispatch({ type: Types.UPDATE_PROFILE_REQUEST });
        return HTTP.callApiWithHeader('users/update-profile', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.UPDATE_PROFILE_SUCCESS, data: response.data.data });
                var language = response.data.data.Language ? response.data.data.Language : 'en';
                dispatch({ type: Types.CHANGE_LANGUAGE, data: language });
                AsyncStorage.setItem('lang', language);
                Toast.showWithGravity(response.data.messages, Toast.LONG, Toast.TOP)
            } else {
                dispatch({ type: Types.UPDATE_PROFILE_FAILURE, errors: response.data.errors });
            }
        }).catch(function (error) {
            dispatch({ type: Types.UPDATE_PROFILE_FAILURE, errors: {} });
        });
    };
}

export const actLogout = (navigation) => {
    return dispatch => {
        dispatch({ type: Types.LOGOUT });
        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    { name: 'Login' },
                ],
            })
        );
    };
}


export const actFollowingTeamLists = (page = 1, per_page = 12) => {
    return dispatch => {
        dispatch({ type: Types.FOLLOWING_TEAM_LIST_REQUEST, page: page });
        return HTTP.callApiWithHeader('user/teams?page=' + page + '&per_page=' + per_page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.FOLLOWING_TEAM_LIST_SUCCESS, data: response.data.data, page: page, total: response.data.total, last_page: response.data.last_page });
            } else {
                dispatch({ type: Types.FOLLOWING_TEAM_LIST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.FOLLOWING_TEAM_LIST_FAILURE });
        });
    };
}

export const actClearTeamListFollowingTeam = () => {
    return dispatch => {
        dispatch({ type: Types.CLEAR_TEAM_FOLLOWING_TEAM });
    };
}


export const actClearEventListHostTeam = () => {
    return dispatch => {
        dispatch({ type: Types.CLEAR_EVENT_LIST_HOSTEAM });
    };
}

export const actClearEventPastEventHostEvent = () => {
    return dispatch => {
        dispatch({ type: Types.CLEAR_PAST_EVENT_LIST_HOST_EVENT });
    };
}




export const actHostingEvent = (page = 1, per_page = 12) => {
    return dispatch => {
        dispatch({ type: Types.HOSTEAM_LIST_REQUEST });
        return HTTP.callApiWithHeader('hosting-events?page=' + page + '&per_page=' + per_page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                if (response.data.total === 0) {
                    NavigationService.replace('PastEvent');
                }
                dispatch({ type: Types.HOSTEAM_LIST_SUCCESS, data: response.data });
            } else {
                dispatch({ type: Types.HOSTEAM_LIST_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.HOSTEAM_LIST_FAILURE });
        });
    };
}

export const actPastHostingEvent = (page = 1, per_page = 12) => {
    return dispatch => {
        dispatch({ type: Types.PAST_HOSTEAM_LIST_REQUEST, page });
        return HTTP.callApiWithHeader('past-events?page=' + page + '&per_page=' + per_page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.PAST_HOSTEAM_LIST_SUCCESS, data: response.data, page });
            } else {
                dispatch({ type: Types.PAST_HOSTEAM_LIST_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.PAST_HOSTEAM_LIST_FAILURE });
        });
    };
}

export const actLoadDataMyProfile = () => {

    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_MY_PROFILE_REQUEST });
        return HTTP.callApiWithHeader(`my-profile`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({
                    type: Types.LOAD_DATA_MY_PROFILE_SUCCESS,
                    data: response.data
                });
            } else {
                dispatch({ type: Types.LOAD_DATA_MY_PROFILE_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_MY_PROFILE_FAILURE });
        });
    };
}

export const actUpdateDeviceToken = (body) => {
    return dispatch => {
        return HTTP.callApiWithHeader('getTokenNotiPush', 'POST', body).then(response => {

        }).catch(function (error) {
            console.log(error)
        });
    };
}

export const actRemoveDeviceToken = (body) => {
    return dispatch => {
        return HTTP.callApiWithHeader('removeTokenNotiPush', 'POST', body).then(response => {

        }).catch(function (error) {
            console.log(error)
        });
    };
}

export const actUpdateNewNotification = () => {
    return dispatch => {
        dispatch({ type: Types.UPDATE_NEW_NOTIFICATION });
    };
}

export const actLoadDataTeamRequest = (page = 1, per_page = 12) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_TEAM_REQUEST_REQUEST, page: page });
        return HTTP.callApiWithHeader('settings/received-request-team?page=' + page + '&per_page=' + per_page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_TEAM_REQUEST_SUCCESS, data: response.data.data, page: page, total: response.data.total });
            } else {
                dispatch({ type: Types.LOAD_DATA_TEAM_REQUEST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_TEAM_REQUEST_FAILURE });
        });
    };
}

export const actLoadDataEventRequest = (page = 1, notification) => {
    var url = notification ? `settings/received-request-event?page=${page}&per_page=12&notification=${notification}` : `settings/received-request-event?page=${page}&per_page=12`;
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_REQUEST_REQUEST, page: page });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_REQUEST_SUCCESS, data: response.data.data, page: page, total: response.data.total });
            } else {
                dispatch({ type: Types.LOAD_DATA_EVENT_REQUEST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_REQUEST_FAILURE });
        });
    };
}

export const actLoadDataPurchaseHistory = (page = 1, iso_code) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_PURCHASE_HISTORY_REQUEST, page: page });
        return HTTP.callApiWithHeader('settings/purchase-history?page=' + page + '&per_page=12&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_PURCHASE_HISTORY_SUCCESS, data: response.data.data, page: page, total: response.data.total });
            } else {
                dispatch({ type: Types.LOAD_DATA_PURCHASE_HISTORY_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_PURCHASE_HISTORY_FAILURE });
        });
    };
}

export const actLoadDataUserSaleReportList = (Page, Time = '', TimeStart = '', TimeFinish = '') => {
    var url = Time == -1 ? `settings/sale-report?page=${Page}&time=${Time}&start=${TimeStart}&end=${TimeFinish}` : `settings/sale-report?page=${Page}&time=${Time}`;
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_USER_SALE_REPORT_REQUEST, page: Page });
        return HTTP.callApiWithHeader(url).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_USER_SALE_REPORT_SUCCESS, data: response.data.data, total: response.data.total, page: Page });
            } else {
                dispatch({ type: Types.LOAD_DATA_USER_SALE_REPORT_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_USER_SALE_REPORT_FAILURE });
        });
    };
}

export const actAcceptEvent = (Id) => {
    return dispatch => {
        dispatch({ type: Types.USER_ACCEPT_EVENT_REQUEST, Id });
        return HTTP.callApiWithHeader(`event/${Id}/accept-event`, 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.USER_ACCEPT_EVENT_SUCCESS });
            } else {
                dispatch({ type: Types.USER_ACCEPT_EVENT_FAILURE, Id });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.USER_ACCEPT_EVENT_FAILURE, Id });
        });
    };
}

export const actDenyEvent = (Id) => {
    return dispatch => {
        dispatch({ type: Types.USER_DENY_EVENT_REQUEST, Id });
        return HTTP.callApiWithHeader(`event/${Id}/deny-event`, 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.USER_DENY_EVENT_SUCCESS });
            } else {
                dispatch({ type: Types.USER_DENY_EVENT_FAILURE, Id });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.USER_DENY_EVENT_FAILURE, Id });
        });
    };
}

export const actAcceptTeam = (Id) => {
    return dispatch => {
        dispatch({ type: Types.USER_ACCEPT_TEAM_REQUEST, Id });
        return HTTP.callApiWithHeader(`team/${Id}/accept-team`, 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.USER_ACCEPT_TEAM_SUCCESS });
            } else {
                dispatch({ type: Types.USER_ACCEPT_TEAM_FAILURE, Id });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.USER_ACCEPT_TEAM_FAILURE, Id });
        });
    };
}

export const actDenyTeam = (Id) => {
    return dispatch => {
        dispatch({ type: Types.USER_DENY_TEAM_REQUEST, Id });
        return HTTP.callApiWithHeader(`team/${Id}/deny-team`, 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.USER_DENY_TEAM_SUCCESS });
            } else {
                dispatch({ type: Types.USER_DENY_TEAM_FAILURE, Id });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.USER_DENY_TEAM_FAILURE, Id });
        });
    };
}

export const actVerifyEmail = () => {
    return dispatch => {
        dispatch({ type: Types.VERIFY_EMAIL_REQUEST });
        return HTTP.callApiWithHeader('verify-email', 'POST', null).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.VERIFY_EMAIL_SUCCESS });
                Toast.showWithGravity(response.data.messages, Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.VERIFY_EMAIL_FAILURE, errors: response.data.errors });
            }
        }).catch(function (error) {
            dispatch({ type: Types.VERIFY_EMAIL_FAILURE, errors: {} });
        });
    };
}

export const actUpdateSetting = (body) => {
    return dispatch => {
        dispatch({ type: Types.UPDATE_SETTING_REQUEST, body });
        return HTTP.callApiWithHeader('general-setting', 'POST', body, true).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.UPDATE_SETTING_SUCCESS });
            }
        }).catch(function (error) {
            dispatch({ type: Types.UPDATE_SETTING_FAILURE, body });
        });
    };
}

export const changePasswordSuccess = () => {
    return dispatch => {
        dispatch({ type: Types.CHANGE_PASSWORD_SUCCESS });
    };
}

export const setCheckTeamKR = () => {
    return dispatch => {
        return HTTP.callApiWithHeader('check-team-kr', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.SHOW_MODAL_SETTING_TEAM, team: response.data.data });
            }
        }).catch(function (error) {
            // dispatch({ type: Types.UPDATE_SETTING_FAILURE});
        });
    };
}

export const hideModalSettingTeam = () => {
    return dispatch => {
        dispatch({ type: Types.HIDE_MODAL_SETTING_TEAM });
    };
}



