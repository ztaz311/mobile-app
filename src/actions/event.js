import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
// let HTTP = require('../services/HTTP');
import Toast from 'react-native-simple-toast';
import firebase from 'react-native-firebase';
import NavigationService from '../router/NavigationService';
import { Alert } from 'react-native';
import { convertLanguage } from '../services/Helper';
export const actEventLists = (id, type) => {
    var url = '';
    if (type === 'city') {
        url = `events?page=1&per_page=1&city_id=${id}`;
    } else {
        url = `events?page=1&per_page=1&iso_code=${id}`;
    }
    return dispatch => {
        dispatch({ type: Types.EVENT_LIST_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EVENT_LIST_SUCCESS, data: response.data.data, page: 1 });
            } else {
                dispatch({ type: Types.EVENT_LIST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EVENT_LIST_FAILURE });
        });
    };
}

export const actLoadDataHostInfo = () => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_HOST_INFO_REQUEST });
        return HTTP.callApiWithHeader('users/show/teams', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_HOST_INFO_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_DATA_HOST_INFO_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_HOST_INFO_FAILURE });
        });
    };
}

export const actLoadDataLocation = () => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_LOCATION_REQUEST });
        return HTTP.callApiWithHeader('search/location', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_LOCATION_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_DATA_LOCATION_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_LOCATION_FAILURE });
        });
    };
}

export const actLoadDataVenue = (name) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_VENUE_REQUEST });
        return HTTP.callApiWithHeader('search/venues?search=' + name, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_VENUE_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_DATA_VENUE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_VENUE_FAILURE });
        });
    };
}

export const actLoadDataListEvent = (page, area, time_event, tag, start = '', finish = '', type) => {
    var url = '';
    if (type === 'city') {
        url = 'collection/events?page=' + page + '&area=' + area + '&time_event=' + time_event + '&tag=' + tag + '&start=' + start + '&finish=' + finish;
    } else {
        url = 'collection/events?page=' + page + '&iso_code=' + area + '&time_event=' + time_event + '&tag=' + tag + '&start=' + start + '&finish=' + finish;
    }
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_LIST_EVENT_REQUEST, page });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_LIST_EVENT_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_LIST_EVENT_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_LIST_EVENT_FAILURE });
        });
    };
}



export const actLoadDataEventDetail = (slug, notification_id, iso_code) => {
    var url = notification_id ? `events/${slug}?notification=${notification_id}&iso_code=${iso_code}` : `events/${slug}?iso_code=${iso_code}`;
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_DETAIL_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_DETAIL_SUCCESS, data: response.data.data });
            } else {
                if (response && response.data.status == 404) {
                    dispatch({ type: Types.LOAD_DATA_EVENT_DETAIL_FAILURE, status_code: 404 });
                    // Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                } else {
                    dispatch({ type: Types.LOAD_DATA_EVENT_DETAIL_FAILURE, status_code: 500 });
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_DETAIL_FAILURE, status_code: 500 });
        });
    };
}

export const actLoadDataEventNews = (slug, page, iso_code) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/event-news?page=' + page + '&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_FAILURE });
        });
    };
}

export const actLikeEvent = (id, user = null) => {
    var body = {
        EventId: id
    }
    return dispatch => {
        dispatch({ type: Types.LIKE_EVENT_REQUEST, id });
        dispatch({ type: Types.LIKE_EVENT_IN_TEAM_REQUEST, id });
        dispatch({ type: Types.LIKE_EVENT_IN_SEARCH_REQUEST, id });
        return HTTP.callApiWithHeader('likes/event', 'POST', body, true).then(response => {
            if (response && response.data.status == 200) {
                console.log(response.data)
                if (response.data.like === 'liked') {
                    firebase.analytics().logEvent('like_event', { event_id: id });
                }
                dispatch({ type: Types.LIKE_EVENT_SUCCESS, id, user });
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types.LIKE_EVENT_FAILURE, id });
                dispatch({ type: Types.LIKE_EVENT_IN_TEAM_FAILURE, id });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
            } else {
                dispatch({ type: Types.LIKE_EVENT_FAILURE, id });
                dispatch({ type: Types.LIKE_EVENT_IN_TEAM_FAILURE, id });
                dispatch({ type: Types.LIKE_EVENT_IN_SEARCH_FAILURE, id });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LIKE_EVENT_FAILURE, id });
            dispatch({ type: Types.LIKE_EVENT_IN_TEAM_FAILURE, id });
            dispatch({ type: Types.LIKE_EVENT_IN_SEARCH_FAILURE, id });
        });
    };
}

export const actLoadDataLikeEvent = (slug, page) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_LIKE_EVENT_REQUEST, page });
        return HTTP.callApiWithHeader('events/' + slug + '/like-people?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.LOAD_DATA_LIKE_EVENT_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_LIKE_EVENT_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_LIKE_EVENT_FAILURE });
        });
    };
}

export const actLoadDataCollaboratingTeam = (slug) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_COLLABORATING_TEAM_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/collaborate', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                return HTTP.callApiWithHeader('events/' + slug + '/invite', 'GET', null).then(response2 => {
                    if (response2 && response2.data.status == 200) {
                        dispatch({ type: Types.LOAD_DATA_COLLABORATING_TEAM_SUCCESS, collaborates: response.data.data, invitations: response2.data.data });
                    }
                })
                    .catch(function (error) {
                        dispatch({ type: Types.LOAD_DATA_COLLABORATING_TEAM_FAILURE });
                    });
            } else {
                dispatch({ type: Types.LOAD_DATA_COLLABORATING_TEAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_COLLABORATING_TEAM_FAILURE });
        });
    };
}

export const actConfirmCollaborate = (slug, id) => {
    return dispatch => {
        // dispatch({type: Types.CONFIRM_COLLABORATE_REQUEST, id});
        return HTTP.callApiWithHeader('events/' + slug + '/teams/' + id + '/collaborate', 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.CONFIRM_COLLABORATE_SUCCESS, id });
            } else {
                // dispatch({type: Types.CONFIRM_COLLABORATE_FAILURE, id});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            // dispatch({type: Types.CONFIRM_COLLABORATE_FAILURE, id});
        });
    };
}

export const actDismissCollaborate = (slug, id) => {
    return dispatch => {
        return HTTP.callApiWithHeader('events/' + slug + '/teams/' + id + '/dismiss-collaborate', 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.DISMISS_COLLABORATE_SUCCESS, id });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
        });
    };
}

export const actDeleteCollaborate = (slug, id) => {
    var body = {
        EventId: slug,
        TeamId: id
    }
    return dispatch => {
        return HTTP.callApiWithHeader('invites/event/delete-invite', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.DELETE_COLLABORATE_SUCCESS, id });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
        });
    };
}

export const actLoadDataTeamApply = (slug, page) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_TEAM_APPLY_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/apply?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.LOAD_DATA_TEAM_APPLY_SUCCESS, data: response.data.data, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_TEAM_APPLY_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_TEAM_APPLY_FAILURE });
        });
    };
}


export const actApplyCollaborate = (body, navigation, language) => {
    return dispatch => {
        dispatch({ type: Types.APPLY_COLLABORATE_REQUEST });
        return HTTP.callApiWithHeader('invites/event', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.APPLY_COLLABORATE_SUCCESS, id: body.TeamId });
                Toast.showWithGravity(convertLanguage(language, 'successfully_sent_request_to_join_the_group_waiting_for_approval'), Toast.LONG, Toast.TOP)
                navigation.goBack()
            } else {
                dispatch({ type: Types.APPLY_COLLABORATE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.APPLY_COLLABORATE_FAILURE });
        });
    };
}

export const actSaveEvent = (Slug, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.SAVE_EVENT_REQUEST });
        return HTTP.callApiWithHeader('teams/' + Slug + '/events/create', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.SAVE_EVENT_SUCCESS });
                if (body.Draft) {
                    Toast.showWithGravity('Save draft success!', Toast.LONG, Toast.TOP)
                } else {
                    firebase.analytics().logEvent('create_event', { title: response.data.data.Title });
                    navigation.replace('EventDetail', { Slug: response.data.data.Slug });
                }
            } else {
                dispatch({ type: Types.SAVE_EVENT_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.SAVE_EVENT_FAILURE });
        });
    };
}

export const actLoadDataEventImport = (slug, page) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_IMPORT_REQUEST });
        return HTTP.callApiWithHeader('teams/' + slug + '/import-events?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_IMPORT_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_EVENT_IMPORT_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_IMPORT_FAILURE });
        });
    };
}

export const actSaveEventNews = (Slug, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.SAVE_EVENT_NEWS_REQUEST });
        return HTTP.callApiWithHeader('events/' + Slug + '/event-news', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.SAVE_EVENT_NEWS_SUCCESS, data: response.data.data });
                navigation.goBack();
            } else {
                dispatch({ type: Types.SAVE_EVENT_NEWS_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.SAVE_EVENT_NEWS_FAILURE });
        });
    };
}

export const actEditEvent = (slug) => {
    return dispatch => {
        dispatch({ type: Types.EDIT_EVENT_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/edit', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EDIT_EVENT_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.EDIT_EVENT_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EDIT_EVENT_FAILURE });
        });
    };
}

export const actUpdateEvent = (Slug, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.SAVE_EVENT_REQUEST });
        return HTTP.callApiWithHeader('events/' + Slug + '/update', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.UPDATE_EVENT_SUCCESS, data: response.data.data });
                navigation.goBack();
            } else {
                dispatch({ type: Types.SAVE_EVENT_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.SAVE_EVENT_FAILURE });
        });
    };
}

export const actCancelEvent = (id, body) => {
    return dispatch => {
        dispatch({ type: Types.EVENT_CANCEL_REQUEST });
        return HTTP.callApiWithHeader('events/' + id + '/delete', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EVENT_CANCEL_SUCCESS });
                Toast.showWithGravity('Cancel Success!', Toast.LONG, Toast.TOP)
                NavigationService.goBack();
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.EVENT_CANCEL_FAILURE });
                Toast.showWithGravity('Password is invalid!', Toast.LONG, Toast.TOP)
            } else {
                dispatch({ type: Types.EVENT_CANCEL_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EVENT_CANCEL_FAILURE });
        });
    };
}

export const actDeleteEvent = (id, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.EVENT_DELETE_REQUEST });
        return HTTP.callApiWithHeader('events/' + id + '/delete', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EVENT_DELETE_SUCCESS });
                navigation.navigate('ResetToHome')
                Toast.showWithGravity('Delete Success!', Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.EVENT_DELETE_FAILURE });
                Toast.showWithGravity('Password is invalid!', Toast.LONG, Toast.TOP)
                // Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.EVENT_DELETE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EVENT_DELETE_FAILURE });
        });
    };
}

export const actReportEvent = (Slug, content, navigation) => {
    return dispatch => {
        dispatch({ type: Types.EVENT_REPORT_REQUEST });
        let body = {
            EventId: Slug,
            Content: content
        };
        return HTTP.callApiWithHeader('events/' + Slug + '/report', 'post', body, '').then(response => {
            if (response && response.status == 200) {
                dispatch({ type: Types.EVENT_REPORT_SUCCESS });
                Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
                navigation.goBack();
            } else {
                dispatch({ type: Types.EVENT_REPORT_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EVENT_REPORT_FAILURE });
        });
    };
}

export const actLikeEventNews = (EventNewsId) => {
    var body = {
        EventNewsId
    }
    return dispatch => {
        dispatch({ type: Types.LIKE_EVENT_NEWS_REQUEST, id: EventNewsId });
        dispatch({ type: Types.UPDATE_LIKE_EVENT_NEWS_REQUEST, id: EventNewsId });
        return HTTP.callApiWithHeader('likes/event-news', 'POST', body, true).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LIKE_EVENT_NEWS_SUCCESS, id: EventNewsId });
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types.LIKE_EVENT_NEWS_FAILURE, id: EventNewsId });
                dispatch({ type: Types.UPDATE_LIKE_EVENT_NEWS_FAILURE, id: EventNewsId });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
            } else {
                dispatch({ type: Types.LIKE_EVENT_NEWS_FAILURE, id: EventNewsId });
                dispatch({ type: Types.UPDATE_LIKE_EVENT_NEWS_FAILURE, id: EventNewsId });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LIKE_EVENT_NEWS_FAILURE, id: EventNewsId });
            dispatch({ type: Types.UPDATE_LIKE_EVENT_NEWS_FAILURE, id: EventNewsId });
        });
    };
}

export const actEditEventNews = (Id) => {
    return dispatch => {
        dispatch({ type: Types.EDIT_EVENT_NEWS_REQUEST });
        return HTTP.callApiWithHeader('event-news/' + Id, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EDIT_EVENT_NEWS_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.EDIT_EVENT_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EDIT_EVENT_NEWS_FAILURE });
        });
    };
}

export const actUpdateEventNews = (Id, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.UPDATE_EVENT_NEWS_REQUEST });
        return HTTP.callApiWithHeader('event-news/' + Id + '/update', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.UPDATE_EVENT_NEWS_SUCCESS, data: response.data.data, id: Id });
                dispatch({ type: Types.UPDATE_EVENT_NEWS_LIST_SUCCESS, data: response.data.data, id: Id });
                navigation.goBack();
            } else {
                dispatch({ type: Types.UPDATE_EVENT_NEWS_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.UPDATE_EVENT_NEWS_FAILURE });
        });
    };
}

export const actDeleteEventNews = (Id) => {
    return dispatch => {
        dispatch({ type: Types.DELETE_EVENT_NEWS_REQUEST });
        return HTTP.callApiWithHeader('event-news/' + Id + '/delete', 'POST', null).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.DELETE_EVENT_NEWS_SUCCESS, id: Id });
            } else {
                dispatch({ type: Types.DELETE_EVENT_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.DELETE_EVENT_NEWS_FAILURE });
        });
    };
}

export const actLoadEventByMaps = (north = '', east = '', south = '', west = '', area = '', tag = '', time_event = '', start = '', finish = '', type) => {
    var url = '';
    if (type === 'city') {
        url = 'search/event/in-map?North=' + north + '&East=' + east + '&South=' + south + '&West=' + west + '&area=' + area + '&tag=' + tag + '&time_event=' + time_event + '&start=' + start + '&finish=' + finish;
    } else {
        url = 'search/event/in-map?North=' + north + '&East=' + east + '&South=' + south + '&West=' + west + '&iso_code=' + area + '&tag=' + tag + '&time_event=' + time_event + '&start=' + start + '&finish=' + finish;
    }
    return dispatch => {
        dispatch({ type: Types.LOAD_EVENT_BY_MAPS_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_EVENT_BY_MAPS_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_EVENT_BY_MAPS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_EVENT_BY_MAPS_FAILURE });
        });
    };
}

export const actLoadBannerEvent = () => {
    return dispatch => {
        dispatch({ type: Types.LOAD_BANNER_EVENT_REQUEST });
        return HTTP.callApiWithHeader('banners/event', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_BANNER_EVENT_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_BANNER_EVENT_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_BANNER_EVENT_FAILURE });
        });
    };
}

export const actLoadDataEventTickets = (slug) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_TICKETS_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/tickets', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_TICKETS_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_DATA_EVENT_TICKETS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_TICKETS_FAILURE });
        });
    };
}

export const actGetTicket = (slug, purchases, dataAnalytics) => {
    var body = {
        purchases
    }
    return dispatch => {
        dispatch({ type: Types.GET_TICKET_REQUEST });
        return HTTP.callApiWithHeader('event/' + slug + '/purchase', 'POST', body, true).then(response => {
            if (response && response.data.status == 200) {
                firebase.analytics().logEvent('add_to_cart', dataAnalytics);
                NavigationService.navigate('PurchaseTicketStep2', { dataAnalytics });
                dispatch({ type: Types.GET_TICKET_SUCCESS });
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types.GET_TICKET_FAILURE_NOT_REFRESH });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.GET_TICKET_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -2) {
                dispatch({ type: Types.GET_TICKET_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == 404) {
                dispatch({ type: Types.GET_TICKET_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == 500) {
                dispatch({ type: Types.GET_TICKET_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
            } else if (response.data.errors && response.data.errors.Error) {
                dispatch({ type: Types.GET_TICKET_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
            } else {
                dispatch({ type: Types.GET_TICKET_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.GET_TICKET_FAILURE });
        });
    };
}

export const actLoadDataPurchase = () => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_PURCHASE_REQUEST });
        return HTTP.callApiWithHeader('purchase', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                console.log(response.data.data)
                dispatch({ type: Types.LOAD_DATA_PURCHASE_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_DATA_PURCHASE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_PURCHASE_FAILURE });
        });
    };
}

export const actDataHostInfoByTeam = (slug, page) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_HOST_INFO_BY_TEAM_REQUEST, page });
        return HTTP.callApiWithHeader('team/' + slug + '/host-info?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_HOST_INFO_BY_TEAM_SUCCESS, data: response.data.data, total: response.data.data.length, page });
            } else if (response && response.data.status == 404) {
                dispatch({ type: Types.LOAD_DATA_HOST_INFO_BY_TEAM_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.LOAD_DATA_HOST_INFO_BY_TEAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_HOST_INFO_BY_TEAM_FAILURE });
        });
    };
}

export const actLoadDataMembers = (slug, notification_id) => {
    var url = notification_id ? `event/${slug}/members?notification=${notification_id}` : `event/${slug}/members`;
    console.log(url)
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_MEMBERS_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_MEMBERS_SUCCESS, members: response.data.members, memberInvited: response.data.memberInvited, event: response.data.event });
            } else {
                dispatch({ type: Types.LOAD_DATA_MEMBERS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_MEMBERS_FAILURE });
        });
    };
}

export const actLoadDataTicketSales = (slug) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_TICKET_SALES_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/ticket-sales', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_TICKET_SALES_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_DATA_TICKET_SALES_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_TICKET_SALES_FAILURE });
        });
    };
}

export const actWithdraw = (slug, body) => {
    return dispatch => {
        dispatch({ type: Types.WITHDRAW_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/withdraw', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.WITHDRAW_SUCCESS });
                Toast.showWithGravity(response.data.success, Toast.LONG, Toast.TOP)
                NavigationService.goBack();
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.WITHDRAW_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data.errors?.Error), Toast.LONG, Toast.TOP)
            } else {
                dispatch({ type: Types.WITHDRAW_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.WITHDRAW_FAILURE });
        });
    };
}

export const actLoadDataInviteTeamMembers = (id, page) => {

    return dispatch => {
        dispatch({ type: Types.LOAD_INVITE_TEAM_MEMBERS, page });
        return HTTP.callApiWithHeader(`event/${id}/member-host`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_INVITE_TEAM_MEMBERS_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_INVITE_TEAM_MEMBERS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            ispatch({ type: Types.LOAD_INVITE_TEAM_MEMBERS_FAILURE });
        });
    };
}
export const actLoadDataInviteFollowers = (slug, page) => {

    return dispatch => {
        dispatch({ type: Types.LOAD_INVITE_TEAM_FOLLOWERS, page });
        return HTTP.callApiWithHeader(`events/${slug}/like-people`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_INVITE_TEAM_FOLLOWERS_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_INVITE_TEAM_FOLLOWERS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            ispatch({ type: Types.LOAD_INVITE_TEAM_FOLLOWERS_FAILURE });
        });
    };
}

export const actLoadDataInviteSearch = (id, page, key) => {

    return dispatch => {
        dispatch({ type: Types.LOAD_INVITE_TEAM_SEARCH, page });
        return HTTP.callApiWithHeader(`search/users?search=${key}&page=${page}&eventid=${id}`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_INVITE_TEAM_SEARCH_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_INVITE_TEAM_SEARCH_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            ispatch({ type: Types.LOAD_INVITE_TEAM_SEARCH_FAILURE });
        });
    };
}

export const actUnInviteUser = (id, userId) => {

    return dispatch => {
        dispatch({ type: Types.UNINVITE_USER_REQUEST, userId });
        return HTTP.callApiWithHeader(`event/${id}/invites/${userId}`, 'POST', null, true).then(response => {
            if (response && response.data.status == 200) {
                let invite = response.data.invite ? response.data.invite : "invite";
                let inviting = invite == 'inviting' ? true : false
                dispatch({ type: Types.UNINVITE_USER_SUCCESS, userId, inviting });
            } else {
                dispatch({ type: Types.UNINVITE_USER_FAILURE, userId });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            ispatch({ type: Types.UNINVITE_USER_FAILURE, userId });
        });
    };
}

export const actInviteUser = (id, userId, item) => {
    console.log(`event/${id}/invites/${userId}`)
    return dispatch => {
        console.log(userId)
        dispatch({ type: Types.INVITE_USER_REQUEST, userId });
        return HTTP.callApiWithHeader(`event/${id}/invites/${userId}`, 'POST', null, true).then(response => {
            if (response && response.data.status == 200) {
                let invite = ''
                let inviting = false
                response.data.invite ? invite = response.data.invite : invite = "invite"
                invite == 'inviting' ? inviting = true : inviting = false
                dispatch({ type: Types.INVITE_USER_SUCCESS, userId, inviting, item });
            } else {
                dispatch({ type: Types.INVITE_USER_FAILURE, userId });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            ispatch({ type: Types.INVITE_USER_FAILURE, userId });
        });
    };
}

export const actLoadDataRefundList = (EventId, Keyword, Page, Type, Status) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_REFUND_REQUEST, page: Page });
        return HTTP.callApiWithHeader('search/users-request-refund?page=' + Page + '&search=' + Keyword + '&type=' + Type + '&eventId=' + EventId + '&refundStatus=' + Status).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_REFUND_SUCCESS, data: response.data.data, total: response.data.total, page: Page });
            } else {
                dispatch({ type: Types.LOAD_DATA_REFUND_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_REFUND_FAILURE });
        });
    };
}

export const actDataWithdrawByTeam = (TeamId) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_WITHDRAW_BY_TEAM_REQUEST });
        return HTTP.callApiWithHeader('teams/' + TeamId + '/import-withdraw', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_WITHDRAW_BY_TEAM_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_DATA_WITHDRAW_BY_TEAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_WITHDRAW_BY_TEAM_FAILURE });
        });
    };
}

export const actClearInviteEvent = () => {

    return dispatch => {
        dispatch({ type: Types.CLEAR_INVITE_EVENT });
    }
}

export const actRefundAll = (body) => {
    return dispatch => {
        dispatch({ type: Types.REFUND_ALL_REQUEST });
        return HTTP.callApiWithHeader('refund/confirm-refund-all', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                Toast.showWithGravity(JSON.stringify(response.data.messages.message), Toast.LONG, Toast.TOP)
                dispatch({ type: Types.REFUND_ALL_SUCCESS });
            } else {
                dispatch({ type: Types.REFUND_ALL_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.REFUND_ALL_FAILURE });
        });
    };
}

export const actRefuseAll = (body) => {
    return dispatch => {
        dispatch({ type: Types.REFUSE_ALL_REQUEST });
        return HTTP.callApiWithHeader('refund/refuse-refund-all', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                Toast.showWithGravity(JSON.stringify(response.data.messages.message), Toast.LONG, Toast.TOP)
                dispatch({ type: Types.REFUSE_ALL_SUCCESS });
            } else {
                dispatch({ type: Types.REFUSE_ALL_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.REFUSE_ALL_FAILURE });
        });
    };
}



export const actSetEventRole = (eventId, memberId, role, navigation, oldRole) => {
    return dispatch => {
        let body = {
            UserId: memberId,
            Role: role,
            oldRole
        };
        dispatch({ type: Types.EVENT_ROLE_REQUEST, body });
        return HTTP.callApiWithHeader('event/' + eventId + '/set-role', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                // var newRole = role.charAt(0).toUpperCase() + role.slice(1)
                dispatch({ type: Types.EVENT_ROLE_SUCCESS, body });
                // navigation.replace('EventStaff');
                // Toast.showWithGravity(response.data.messages, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.EVENT_ROLE_FAILURE, body });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.EVENT_ROLE_FAILURE, body });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EVENT_ROLE_FAILURE, body });
        });
    };
}


export const actLeaveEvent = (eventId, member) => {
    console.log(member)
    return dispatch => {
        let body = member
        dispatch({ type: Types.EVENT_KICK_STAFF_REQUEST, body });
        return HTTP.callApiWithHeader('event/' + eventId + '/kick-member', 'post', { MemberId: member.Id }).then(response => {
            if (response && response.data.status == 200) {
                // var newRole = role.charAt(0).toUpperCase() + role.slice(1)
                dispatch({ type: Types.EVENT_KICK_STAFF_SUCCESS, body });
                // navigation.replace('EventStaff');
                // Toast.showWithGravity(response.data.messages, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.EVENT_KICK_STAFF_FAILURE, body });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.EVENT_KICK_STAFF_FAILURE, body });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EVENT_KICK_STAFF_FAILURE, body });
        });
    };
}

export const actDeleteWithdrawImport = (Id) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`withdraw-import/${Id}/delete`, 'POST', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.DELETE_WITHDRAW_IMPORT_SUCCESS, id: Id });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
        });
    };
}

export const actReportEventNews = (Slug, content, navigation) => {
    return dispatch => {
        dispatch({ type: Types.EVENT_REPORT_REQUEST });
        let body = {
            EventId: Slug,
            Content: content
        };
        return HTTP.callApiWithHeader('event-news/' + Slug + '/report', 'post', body, null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EVENT_REPORT_SUCCESS });
                Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.CENTER)
                navigation.goBack();
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types.EVENT_REPORT_FAILURE });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
            } else {
                dispatch({ type: Types.EVENT_REPORT_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EVENT_REPORT_FAILURE });
        });
    };
}

export const actGetDataWithdraw = (slug, language) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_WITHDRAW_REQUEST });
        return HTTP.callApiWithHeader('events/' + slug + '/withdraw', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_WITHDRAW_SUCCESS, data: response.data.data });
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.LOAD_DATA_WITHDRAW_FAILURE });
                Alert.alert(
                    convertLanguage(language, 'warning'),
                    response.data.errors.Error,
                    [
                        { text: convertLanguage(language, 'ok'), onPress: () => null, style: 'cancel' },
                    ],
                    { cancelable: false },
                );
                NavigationService.goBack();
            } else {
                dispatch({ type: Types.LOAD_DATA_WITHDRAW_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_WITHDRAW_FAILURE });
        });
    };
}

export const actClearEventList = () => {
    return dispatch => {
        dispatch({ type: Types.CLEAR_EVENT_LIST });
    };
}

export const actClearItemEvent = () => {
    return dispatch => {
        dispatch({ type: Types.CLEAR_ITEM_EVENT });
    };
}

export const actLoadDataEventNewsDetail = (id, notification_id, iso_code) => {
    var url = notification_id ? `event-news/${id}?notification=${notification_id}&iso_code=${iso_code}` : `event-news/${id}?iso_code=${iso_code}`;
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_DETAIL_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_DETAIL_SUCCESS, data: response.data.data });
            } else {
                if (response && response.data.status == 404) {
                    dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_DETAIL_FAILURE, status_code: 404 });
                    // Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                } else {
                    dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_DETAIL_FAILURE });
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_NEWS_DETAIL_FAILURE });
        });
    };
}

export const actClearInviteSearch = () => {

    return dispatch => {
        dispatch({ type: Types.CLEAR_INVITE_SEARCH });
    }
}





