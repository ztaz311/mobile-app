import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
// let HTTP = require('../services/HTTP');
import Toast from 'react-native-simple-toast';
import firebase from 'react-native-firebase';

export const actTeamDetail = (id, notification_id, iso_code) => {
    var url = notification_id ? `teams/${id}?notification=${notification_id}&iso_code=${iso_code}` : `teams/${id}?iso_code=${iso_code}`;
    return dispatch => {
        dispatch({ type: Types.TEAM_DETAIL_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            response = response.data;
            if (response.status == 200) {
                dispatch({ type: Types.TEAM_DETAIL_SUCCESS, data: response.data });
            } else {
                if (response && response.data.status == 404) {
                    dispatch({ type: Types.TEAM_DETAIL_FAILURE, status_code: 404 });
                    // Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                } else {
                    dispatch({ type: Types.TEAM_DETAIL_FAILURE, status_code: 500 });
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_DETAIL_FAILURE, status_code: 500 });
        });
    };
}

export const actReportTeam = (id, content, navigation) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_REPORT_REQUEST });
        let body = {
            TeamId: id,
            Content: content
        };
        return HTTP.callApiWithHeader('teams/' + id + '/report', 'post', body, null).then(response => {
            if (response && response.status == 200) {
                dispatch({ type: Types.TEAM_REPORT_SUCCESS });
                navigation.goBack();
                Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.CENTER)
            } else {
                dispatch({ type: Types.TEAM_REPORT_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_REPORT_FAILURE });
        });
    };
}

export const actDeleteTeam = (id, password, navigation) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_DELETE_REQUEST });
        let body = {
            Password: password
        };
        return HTTP.callApiWithHeader('teams/' + id + '/delete', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_DELETE_SUCCESS });
                navigation.navigate('ResetToHome')
                Toast.showWithGravity('Delete Success!', Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.TEAM_DELETE_FAILURE, errors: response.data.errors });
                Toast.showWithGravity('Password is invalid!', Toast.SHORT, Toast.TOP)
                // Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.TEAM_DELETE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_DELETE_FAILURE });
        });
    };
}

export const actListMembers = (id) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_MEMBERSLIST_REQUEST });
        return HTTP.callApiWithHeader('teams/' + id + '/members', 'GET', null).then(response => {
            console.log(response.data)
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_MEMBERSLIST_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.TEAM_MEMBERSLIST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_MEMBERSLIST_FAILURE });
        });
    };
}

export const actLeaveTeam = (teamId, memberId, navigation, isAuth = false) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_LEAVE_REQUEST });
        let body = {
            MemberId: memberId
        };
        return HTTP.callApiWithHeader('teams/' + teamId + '/leave', 'post', body).then(response => {
            console.log(response.data)
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_LEAVE_SUCCESS, memberId: memberId, isAuth: isAuth });
                dispatch({ type: Types.TEAM_INVITE_SUCCESS, memberId: memberId });
                dispatch({ type: Types.USER_INVITE_TEAM_SUCCESS, memberId: memberId });
                if(isAuth) {
                    navigation.goBack();
                }
                Toast.showWithGravity(response.data.messages, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.TEAM_LEAVE_FAILURE });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.TEAM_LEAVE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_LEAVE_FAILURE });
        });
    };
}

export const actSetTeamRole = (teamId, memberId, role, navigation) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_ROLE_REQUEST });
        let body = {
            UserId: memberId,
            Role: role
        };
        return HTTP.callApiWithHeader('teams/' + teamId + '/set-role', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                var newRole = role.charAt(0).toUpperCase() + role.slice(1)
                dispatch({ type: Types.TEAM_ROLE_SUCCESS, role: newRole, memberId: memberId });
                Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.TEAM_ROLE_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.TEAM_ROLE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_ROLE_FAILURE });
        });
    };
}

export const actInvitedMember = (teamId, memberId, navigation) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_INVITE_REQUEST });
        let body = {};
        return HTTP.callApiWithHeader('teams/' + teamId + '/invites/' + memberId, 'post', body).then(response => {
            console.log(response)
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_INVITE_SUCCESS, memberId: memberId });
                dispatch({ type: Types.USER_INVITE_TEAM_SUCCESS, memberId: memberId });
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.TEAM_INVITE_FAILURE });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.TEAM_INVITE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_INVITE_FAILURE });
        });
    };
}

export const actListMemberFollowers = (id) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_LIST_FOLLOW_REQUEST });
        return HTTP.callApiWithHeader('teams/' + id + '/follows', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_LIST_FOLLOW_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.TEAM_LIST_FOLLOW_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_MEMBERSLIST_FAILURE });
        });
    };
}

export const actTeamNews = (id) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_NEWSLIST_REQUEST });
        return HTTP.callApiWithHeader('teams/' + id + '/team-news', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_NEWSLIST_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.TEAM_NEWSLIST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_NEWSLIST_FAILURE });
        });
    };
}

export const actTeamLists = (page = 1, per_page = 12, country = '', status_team = '', tag = '') => {
    return dispatch => {
        dispatch({ type: Types.TEAM_LIST_REQUEST, page: page });
        return HTTP.callApiWithHeader('teams?page=' + page + '&per_page=' + per_page + '&country=' + country + '&status_team=' + status_team + '&tag=' + tag, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_LIST_SUCCESS, data: response.data.data, page: page, total: response.data.total });
            } else {
                dispatch({ type: Types.TEAM_LIST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_LIST_FAILURE });
        });
    };
}

export const actHomeTeamLists = (id, type) => {
    var url = '';
    if (type === 'city') {
        url = `teams?page=1&per_page=12&city_id=${id}`;
    } else {
        url = `teams?page=1&per_page=12&iso_code=${id}`;
    }
    return dispatch => {
        dispatch({ type: Types.HOME_TEAM_LIST_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.HOME_TEAM_LIST_SUCCESS, data: response.data.data, page: 1 });
            } else {
                dispatch({ type: Types.HOME_TEAM_LIST_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.HOME_TEAM_LIST_FAILURE });
        });
    };
}


export const actTeamFollow = (teamId) => {
    return dispatch => {
        
        dispatch({ type: Types.TEAM_FOLLOW_SUCCESS, teamId: teamId });
        dispatch({ type: Types.EVENT_DETAIL_TEAM_FOLLOW_SUCCESS, teamId: teamId });
        let body = {
            TeamId: teamId
        };
        return HTTP.callApiWithHeader('follows', 'post', body, true).then(response => {
            if (response && response.data.status == 200) {
                if(response.data.total === 1) {
                    firebase.analytics().logEvent('follow_team', {team_id: teamId});
                }
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.TEAM_FOLLOW_SUCCESS, teamId: teamId });
                dispatch({ type: Types.EVENT_DETAIL_TEAM_FOLLOW_SUCCESS, teamId: teamId });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types.TEAM_FOLLOW_SUCCESS, teamId: teamId });
                dispatch({ type: Types.EVENT_DETAIL_TEAM_FOLLOW_SUCCESS, teamId: teamId });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
            } else {
                dispatch({ type: Types.TEAM_FOLLOW_SUCCESS, teamId: teamId });
                dispatch({ type: Types.EVENT_DETAIL_TEAM_FOLLOW_SUCCESS, teamId: teamId });
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_FOLLOW_SUCCESS, teamId: teamId });
            dispatch({ type: Types.EVENT_DETAIL_TEAM_FOLLOW_SUCCESS, teamId: teamId });
        });
    };
}

export const actImagesIns = (teamId) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_IMAGEINS_REQUEST });
        return HTTP.callApiWithHeader("teams/" + teamId + "/get-images-instagram", 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_IMAGEINS_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.TEAM_IMAGEINS_FAILURE });
                // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_IMAGEINS_FAILURE });
        });
    };
}

export const actSetTokenIns = (teamId, token) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_SET_INSTAGRAM_REQUEST });
        let body = {
            Token: token.access_token
        };
        return HTTP.callApiWithHeader('teams/' + teamId + '/set-instagram', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_SET_INSTAGRAM_SUCCESS, data: response.data.data, token: token });
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.TEAM_SET_INSTAGRAM_FAILURE });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.TEAM_SET_INSTAGRAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_SET_INSTAGRAM_FAILURE });
        });
    };
}

export const actRemoveTokenIns = (Code) => {
    var body = {
        code: Code
    }
    return dispatch => {
        dispatch({ type: Types.TEAM_REMOVE_INSTAGRAM_REQUEST });
        return HTTP.callApiWithHeader('teams/remove-instagram', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.TEAM_REMOVE_INSTAGRAM_SUCCESS });
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types.TEAM_REMOVE_INSTAGRAM_FAILURE });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.TEAM_REMOVE_INSTAGRAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_SET_INSTAGRAM_FAILURE });
        });
    };
}

export const actCreateTeamNews = (body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_NEWS_CREATE_REQUEST });
        return HTTP.callApiWithHeader('team-news/create', 'post', body).then(response => {
            if (response && response.status == 200) {
                if (response.data.status === -1) {
                    dispatch({ type: Types.TEAM_NEWS_CREATE_FAILURE });
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    dispatch({ type: Types.TEAM_NEWS_CREATE_SUCCESS, data: response.data.data });
                    navigation.goBack();
                }
            } else {
                dispatch({ type: Types.TEAM_NEWS_CREATE_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_NEWS_CREATE_FAILURE });
        });
    };
}

export const actLikeTeamNews = (TeamNewsId) => {
    var body = {
        TeamNewsId
    }
    return dispatch => {
        dispatch({ type: Types.LIKE_TEAM_NEWS_REQUEST, id: TeamNewsId });
        dispatch({ type: Types.UPDATE_LIKE_TEAM_NEWS_REQUEST, id: TeamNewsId });
        return HTTP.callApiWithHeader('likes/team-news', 'POST', body, true).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LIKE_TEAM_NEWS_SUCCESS, id: TeamNewsId });
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types.LIKE_TEAM_NEWS_FAILURE, id: TeamNewsId });
                dispatch({ type: Types.UPDATE_LIKE_TEAM_NEWS_FAILURE, id: TeamNewsId });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
            } else {
                dispatch({ type: Types.LIKE_TEAM_NEWS_FAILURE, id: TeamNewsId });
                dispatch({ type: Types.UPDATE_LIKE_TEAM_NEWS_FAILURE, id: TeamNewsId });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LIKE_TEAM_NEWS_FAILURE, id: TeamNewsId });
            dispatch({ type: Types.UPDATE_LIKE_TEAM_NEWS_FAILURE, id: TeamNewsId });
        });
    };
}

export const actLoadDataTeamNews = (id, page, iso_code) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_REQUEST });
        return HTTP.callApiWithHeader('teams/' + id + '/team-news?page=' + page + '&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_FAILURE });
        });
    };
}

export const actDeleteTeamNews = (Id) => {
    return dispatch => {
        dispatch({ type: Types.DELETE_TEAM_NEWS_REQUEST });
        return HTTP.callApiWithHeader('team-news/' + Id + '/delete', 'POST', null).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.DELETE_TEAM_NEWS_SUCCESS, id: Id });
            } else {
                dispatch({ type: Types.DELETE_TEAM_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.DELETE_TEAM_NEWS_FAILURE });
        });
    };
}

export const actEditTeamNews = (Id) => {
    return dispatch => {
        dispatch({ type: Types.EDIT_TEAM_NEWS_REQUEST });
        return HTTP.callApiWithHeader('team-news/' + Id, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EDIT_TEAM_NEWS_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.EDIT_TEAM_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EDIT_TEAM_NEWS_FAILURE });
        });
    };
}

export const actUpdateTeamNews = (Id, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.UPDATE_TEAM_NEWS_REQUEST });
        return HTTP.callApiWithHeader('team-news/' + Id + '/update', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.UPDATE_TEAM_NEWS_SUCCESS, data: response.data.data, id: Id });
                dispatch({ type: Types.UPDATE_TEAM_NEWS_LIST_SUCCESS, data: response.data.data, id: Id });
                navigation.goBack();
            } else {
                dispatch({ type: Types.UPDATE_TEAM_NEWS_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.UPDATE_TEAM_NEWS_FAILURE });
        });
    };
}

export const actCreateTeam = (body, navigation, type = '') => {
    return dispatch => {
        dispatch({ type: Types.CREATE_TEAM_REQUEST });
        return HTTP.callApiWithHeader('teams/create', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.CREATE_TEAM_SUCCESS, data: response.data.data });
                firebase.analytics().logEvent('create_team', {name: response.data.data.Name});
                if (type === 'event') {
                    dispatch({ type: Types.ADD_TEAM_TO_IMPORT, data: response.data.data });
                    navigation.goBack();
                } else {
                    navigation.replace('DetailTeam', { id: response.data.data.Id })
                }
            } else {
                dispatch({ type: Types.CREATE_TEAM_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.CREATE_TEAM_FAILURE });
        });
    };
}

export const actUpdateTeam = (teamId, body, navigation, isGoTeamPage, route) => {
    return dispatch => {
        dispatch({ type: Types.CREATE_TEAM_REQUEST });
        return HTTP.callApiWithHeader('teams/' + teamId + '/update', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.UPDATE_TEAM_SUCCESS, data: response.data.data, teamId: teamId });
                if(isGoTeamPage) {
                    navigation.replace('DetailTeam', { id: teamId })
                } else {
                    route.params['callback']();
                    navigation.goBack()
                }
            } else {
                dispatch({ type: Types.CREATE_TEAM_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.CREATE_TEAM_FAILURE });
        });
    };
}

export const actLoadBannerTeam = () => {
    return dispatch => {
        dispatch({ type: Types.LOAD_BANNER_TEAM_REQUEST });
        return HTTP.callApiWithHeader('banners/team', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_BANNER_TEAM_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.LOAD_BANNER_TEAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_BANNER_TEAM_FAILURE });
        });
    };
}

export const actCreateAboutTeam = (slug, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.CREATE_ABOUT_TEAM_REQUEST });
        return HTTP.callApiWithHeader('teams/' + slug + '/about/create', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.CREATE_ABOUT_TEAM_SUCCESS, data: response.data.data });
                navigation.goBack();
            } else {
                dispatch({ type: Types.CREATE_ABOUT_TEAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.CREATE_ABOUT_TEAM_FAILURE });
        });
    };
}

export const actLoadDataEventByTeam = (id, page) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_BY_TEAM_REQUEST, page });
        return HTTP.callApiWithHeader('teams/' + id + '/events?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_BY_TEAM_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_EVENT_BY_TEAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_BY_TEAM_FAILURE });
        });
    };
}

export const actLoadDataSaleReportList = (TeamId, Page, Time = '', TimeStart = '', TimeFinish = '') => {
    var url = Time == -1 ? `teams/${TeamId}/sales-report?page=${Page}&start=${TimeStart}&end=${TimeFinish}` : `teams/${TeamId}/sales-report?page=${Page}&time=${Time}`;
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_SALE_REPORT_REQUEST, page: Page });
        return HTTP.callApiWithHeader(url).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_SALE_REPORT_SUCCESS, data: response.data.data, total: response.data.total, page: Page });
            } else {
                dispatch({ type: Types.LOAD_DATA_SALE_REPORT_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_SALE_REPORT_FAILURE });
        });
    };
}

export const actReportTeamNews = (id, content, navigation) => {
    return dispatch => {
        dispatch({ type: Types.TEAM_REPORT_REQUEST });
        let body = {
            TeamNewsId: id,
            Content: content
        };
        return HTTP.callApiWithHeader('team-news/' + id + '/report', 'post', body, null).then(response => {
            if (response && response.status == 200) {
                if (response.data.status == 200) {
                    dispatch({ type: Types.TEAM_REPORT_SUCCESS });
                    navigation.goBack();
                    Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
                } else if (response.data.status == 401) {
                    dispatch({ type: Types.TEAM_REPORT_FAILURE });
                    dispatch({ type: Types.SHOW_MODAL_LOGIN });
                } else {
                    dispatch({ type: Types.TEAM_REPORT_FAILURE });
                    Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
                }
            } else {
                dispatch({ type: Types.TEAM_REPORT_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.TEAM_REPORT_FAILURE });
        });
    };
}

export const actClearListTeam = () => {
    return dispatch => {
        dispatch({ type: Types.CLEAR_TEAM_LIST });
    };
}

export const actLoadDataTeamNewsDetail = (id, notification_id, iso_code) => {
    var url = notification_id ? `team-news/${id}?notification=${notification_id}&iso_code=${iso_code}` : `team-news/${id}?iso_code=${iso_code}`;
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_DETAIL_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_DETAIL_SUCCESS, data: response.data.data });
            } else {
                if (response && response.data.status == 404) {
                    dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_DETAIL_FAILURE, status_code: 404 });
                    // Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                } else {
                    dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_DETAIL_FAILURE, status_code: 500 });
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_TEAM_NEWS_DETAIL_FAILURE, status_code: 500 });
        });
    };
}

export const actClearTeamInviteSearch = () => {

    return dispatch => {
        dispatch({ type: Types.CLEAR_TEAM_INVITE_SEARCH });
    }
}


