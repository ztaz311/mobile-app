import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
import Toast from 'react-native-simple-toast';


export const actSelectCity = (slug) => {
    return dispatch => {
        dispatch({type: Types.SELECT_CITY_REQUEST});
        return HTTP.callApiWithHeader('country/'+slug+'/area', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.SELECT_CITY_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.SELECT_CITY_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.SELECT_CITY_FAILURE});
        });
    };
}

export const actSelectCountry = () => {
    return dispatch => {
        dispatch({type: Types.SELECT_COUNTRY_REQUEST});
        return HTTP.callApiWithHeader('countries', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.SELECT_COUNTRY_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.SELECT_COUNTRY_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.SELECT_CITY_FAILURE});
        });
    };
}

export const actGetDistrictByCity = (city_id) => {
    return dispatch => {
        dispatch({type: Types.GET_DISTRICT_BY_CITY_REQUEST});
        return HTTP.callApiWithHeader('cities/'+city_id+'/districts', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.GET_DISTRICT_BY_CITY_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.GET_DISTRICT_BY_CITY_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.GET_DISTRICT_BY_CITY_FAILURE});
        });
    };
}

export const actSelectCountryByEvent = () => {
    return dispatch => {
        dispatch({type: Types.SELECT_COUNTRY_REQUEST});
        return HTTP.callApiWithHeader('countries/events', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.SELECT_COUNTRY_EVENT_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.SELECT_COUNTRY_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.SELECT_CITY_FAILURE});
        });
    };
}




