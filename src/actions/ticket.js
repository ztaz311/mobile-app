import firebase from 'react-native-firebase';
import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
import Toast from 'react-native-simple-toast';
import NavigationService from '../router/NavigationService';

export const actLoadDataMyticket = (page) => {
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_MY_TICKET_REQUEST, page});
        return HTTP.callApiWithHeader('user/show/tickets').then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_MY_TICKET_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.LOAD_DATA_MY_TICKET_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_MY_TICKET_FAILURE});
        });
    };
}

export const actLoadDataMyPastTicket = (page) => {
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_MY_PAST_TICKET_REQUEST, page});
        return HTTP.callApiWithHeader('user/show/past-tickets?page='+page).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_MY_PAST_TICKET_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.LOAD_DATA_MY_PAST_TICKET_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_MY_PAST_TICKET_FAILURE});
        });
    };
}

export const actLoadDataTicketPurchased = (event_id) => {
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_TICKET_PURCHASED_REQUEST});
        return HTTP.callApiWithHeader('payment/'+event_id+'/purchased').then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_TICKET_PURCHASED_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.LOAD_DATA_TICKET_PURCHASED_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_TICKET_PURCHASED_FAILURE});
        });
    };
}

export const actUseETicket = (indexTicket, body) => {
    return dispatch => {
        dispatch({type: Types.USE_E_TICKET_REQUEST});
        return HTTP.callApiWithHeader('use-ticket', 'POST', body).then(response => {
            if (response && response.data) {
                dispatch({type: Types.USE_E_TICKET_SUCCESS, data: response.data, id: body.TicketDetailId, indexTicket});
            } else {
                dispatch({type: Types.USE_E_TICKET_FAILURE, content: 'Fail!'});
            }
        }).catch(function (error) {
            dispatch({type: Types.USE_E_TICKET_FAILURE, content: 'Fail!'});
        });
    };
}

export const actCloseModalUseTicket = () => {
    return dispatch => {
        dispatch({type: Types.CLOSE_MODAL_USE_TICKET});
    };
}

export const actLoadDataTicket = (CodeEvent, CodeScan) => {
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_TICKET_REQUEST});
        return HTTP.callApiWithHeader('event/'+CodeEvent+'/ticket/'+CodeScan).then(response => {
            console.log(response.data)
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_TICKET_SUCCESS, data: response.data.data});
            } else if(response && response.data.status == -1) {
                dispatch({type: Types.LOAD_DATA_TICKET_FAILURE, errors: response.data.errors});
            } else {
                dispatch({type: Types.LOAD_DATA_TICKET_FAILURE, errors: {CodeScan: 'Scan Ticket Error!', Type: 'error'}});
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_TICKET_FAILURE, errors: {CodeScan: 'Scan Ticket Error!', Type: 'error'}});
        });
    };
}

export const actCloseModalDataTicket = () => {
    return dispatch => {
        dispatch({type: Types.CLOSE_MODAL_DATA_TICKET});
    };
}

export const actScanTicket = (body) => {
    return dispatch => {
        dispatch({type: Types.SCAN_TICKET_REQUEST});
        return HTTP.callApiWithHeader('scan', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.SCAN_TICKET_SUCCESS, data: response.data.data});
            } else if(response && response.data.status == -1) {
                dispatch({type: Types.SCAN_TICKET_FAILURE, error: response.data.errors && response.data.errors.CodeScan ? response.data.errors.CodeScan : response.data.errors.CodeEvent ? response.data.errors.CodeEvent : 'Fail!'});
            } else {
                dispatch({type: Types.SCAN_TICKET_FAILURE, error: 'Fail!'});
            }
        }).catch(function (error) {
            dispatch({type: Types.SCAN_TICKET_FAILURE, error: 'Fail!'});
        });
    };
}

export const actCloseModalScanTicket = () => {
    return dispatch => {
        dispatch({type: Types.CLOSE_MODAL_SCAN_TICKET});
    };
}

export const actLoadDataHistoryRefund = (id, iso_code) => {
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_TICKET_HISTORY_REFUND_REQUEST});
        return HTTP.callApiWithHeader('purchased/'+id+'/refund?iso_code='+iso_code).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_TICKET_HISTORY_REFUND_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.LOAD_DATA_TICKET_HISTORY_REFUND_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_TICKET_HISTORY_REFUND_FAILURE});
        });
    };
}

export const actRefund = (Status, body, TicketDetailId, indexTicket, type) => {
    var url = '';
    if(Status == -1) {
        url = 'refund/'+TicketDetailId+'/reply-client';
    } else {
        url = 'refund';
    }
    return dispatch => {
        dispatch({type: Types.REFUND_REQUEST, body});
        return HTTP.callApiWithHeader(url, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                firebase.analytics().logEvent('click_refund', { fullname: response.data?.data?.Payment?.FullName, ticket_code: response.data?.data?.Code, device: 'Mobile App', ticket_name: response.data?.data?.Ticket?.Name });
                dispatch({type: Types.REFUND_SUCCESS, data: response.data.comment, indexTicket, ticketType: type});
            }else if(response.data.errors) {
                dispatch({type: Types.REFUND_FAILURE});
                var errors = response.data.errors;
                Toast.showWithGravity(errors[Object.keys(errors)[0]], Toast.SHORT, Toast.TOP);
            } else {
                dispatch({type: Types.REFUND_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.REFUND_FAILURE});
        });
    };
}

export const actChangeComment = (Comment) => {
    return dispatch => {
        dispatch({type: Types.CHANGE_COMMENT, Comment: Comment});
    };
}

export const actLoadDataHolderList = (EventId, Keyword, Page, Type) => {
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_HOLDER_REQUEST, page: Page});
        return HTTP.callApiWithHeader('search/users-by-payment?page='+Page+'&search='+Keyword+'&type='+Type+'&eventId='+EventId).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_HOLDER_SUCCESS, data: response.data.data, total: response.data.total, page: Page});
            } else {
                dispatch({type: Types.LOAD_DATA_HOLDER_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_HOLDER_FAILURE});
        });
    };
}

export const actCommentHolder = (body) => {
    return dispatch => {
        dispatch({type: Types.COMMENT_HOLDER_REQUEST});
        return HTTP.callApiWithHeader('comment-list-holder', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.COMMENT_HOLDER_SUCCESS, data: response.data.data});
                Toast.showWithGravity(response.data.messages.messages, Toast.LONG, Toast.TOP)
                NavigationService.goBack();
            } else {
                Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                dispatch({type: Types.COMMENT_HOLDER_FAILURE});
            }
        }).catch(function (error) {
            Toast.showWithGravity('Connect server error!', Toast.LONG, Toast.TOP)
            dispatch({type: Types.COMMENT_HOLDER_FAILURE});
        });
    };
}


export const actCancelRefund = (Id, body, indexTicket, type) => {
    return dispatch => {
        dispatch({type: Types.CANCEL_REFUND_REQUEST});
        return HTTP.callApiWithHeader(`refund/${Id}/cancel-refund`, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.CANCEL_REFUND_SUCCESS, Id, indexTicket, ticketType: type});
                Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
                NavigationService.goBack();
            } else {
                Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                dispatch({type: Types.CANCEL_REFUND_FAILURE});
            }
        }).catch(function (error) {
            Toast.showWithGravity('Connect server error!', Toast.LONG, Toast.TOP)
            dispatch({type: Types.CANCEL_REFUND_FAILURE});
        });
    };
}

export const actLoadDataHandleRefund = (EventSlug, TicketDetailId, iso_code) => {
    console.log('events/'+EventSlug+'/refund-request/'+TicketDetailId+'?iso_code='+iso_code)
    return dispatch => {
        dispatch({type: Types.LOAD_DATA_TICKET_HANDLE_REFUND_REQUEST});
        return HTTP.callApiWithHeader('events/'+EventSlug+'/refund-request/'+TicketDetailId+'?iso_code='+iso_code).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.LOAD_DATA_TICKET_HANDLE_REFUND_SUCCESS, data: response.data.data});
            } else {
                dispatch({type: Types.LOAD_DATA_TICKET_HANDLE_REFUND_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.LOAD_DATA_TICKET_HANDLE_REFUND_FAILURE});
        });
    };
}

export const actRefuseRefund = (body) => {
    return dispatch => {
        dispatch({type: Types.REFUSE_REFUND_REQUEST});
        return HTTP.callApiWithHeader(`refund/refuse-refund`, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.REFUSE_REFUND_SUCCESS, TicketDetailId: body.TicketDetailId, data: response.data.comment});
                dispatch({type: Types.UPDATE_STATUS_REFUND_LIST_SUCCESS, Status: "-2", TicketDetailId: body.TicketDetailId});
                Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
            } else {
                Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                dispatch({type: Types.REFUSE_REFUND_FAILURE});
            }
        }).catch(function (error) {
            Toast.showWithGravity('Connect server error!', Toast.LONG, Toast.TOP)
            dispatch({type: Types.REFUSE_REFUND_FAILURE});
        });
    };
}

export const actHandleRefund = (body) => {
    return dispatch => {
        dispatch({ type: Types.HANDLE_REFUND_REQUEST });
        return HTTP.callApiWithHeader('refund/confirm-refund', 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                Toast.showWithGravity(JSON.stringify(response.data.messages.message), Toast.LONG, Toast.TOP)
                dispatch({ type: Types.HANDLE_REFUND_SUCCESS, TicketDetailId: body.TicketDetailId, Rate: body.Rate });
                dispatch({type: Types.UPDATE_STATUS_REFUND_LIST_SUCCESS, Status: "1", TicketDetailId: body.TicketDetailId});
            } else {
                dispatch({ type: Types.HANDLE_REFUND_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.HANDLE_REFUND_FAILURE });
        });
    };
}

export const actOpenModalRefundInfo = () => {
    return dispatch => {
        dispatch({ type: Types.OPEN_MODAL_REFUND_INFO });
    }
}

export const actCloseModalRefundInfo = () => {
    return dispatch => {
        dispatch({ type: Types.CLOSE_MODAL_REFUND_INFO });
    }
}

export const actCreateBankInfo = (id, body, indexTicket) => {
    return dispatch => {
        dispatch({ type: Types.CREATE_BANK_INFO_REQUEST });
        return HTTP.callApiWithHeader(`refund/user-bank-request/${id}`, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.CREATE_BANK_INFO_SUCCESS, data: response.data.data, indexTicket });
                setTimeout(function() {
                    Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
                }, 500)
            } else {
                dispatch({ type: Types.CREATE_BANK_INFO_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.CREATE_BANK_INFO_FAILURE });
        });
    };
}

export const actSendCommentRefund = (body, TicketDetailId) => {
    return dispatch => {
        dispatch({type: Types.SEND_COMMENT_REFUND_REQUEST});
        return HTTP.callApiWithHeader('refund/'+TicketDetailId+'/reply-client?messages='+body.Comment, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.SEND_COMMENT_REFUND_SUCCESS, data: response.data.comment, from: 'guest'});
            } else {
                dispatch({type: Types.SEND_COMMENT_REFUND_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.SEND_COMMENT_REFUND_FAILURE});
        });
    };
}

export const actAcceptRefund = (ticketDetailId, body, indexTicket, type) => {
    return dispatch => {
        dispatch({type: Types.ACCEPT_REFUND_REQUEST});
        return HTTP.callApiWithHeader(`refund/confirm-refund-client`, 'POST', body).then(response => {
            console.log(response)
            if (response && response.data.status == 200) {
                dispatch({type: Types.ACCEPT_REFUND_SUCCESS, ticketDetailId, indexTicket, ticketType: type});
                Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
            } else {
                Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.LONG, Toast.TOP)
                dispatch({type: Types.ACCEPT_REFUND_FAILURE});
            }
        }).catch(function (error) {
            Toast.showWithGravity('Connect server error!', Toast.LONG, Toast.TOP)
            dispatch({type: Types.ACCEPT_REFUND_FAILURE});
        });
    };
}

export const actHostSendCommentRefund = (body, TicketDetailId) => {
    return dispatch => {
        dispatch({type: Types.SEND_COMMENT_REFUND_REQUEST});
        return HTTP.callApiWithHeader('refund/confirm-refund?messages='+body.Comment, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.SEND_COMMENT_REFUND_SUCCESS, data: response.data.comment, from: 'host'});
            } else if(response.data.errors && response.data.errors.Error) {
                dispatch({ type: Types.SEND_COMMENT_REFUND_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data.errors.Error), Toast.LONG, Toast.TOP)
            } else {
                dispatch({type: Types.SEND_COMMENT_REFUND_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.SEND_COMMENT_REFUND_FAILURE});
        });
    };
}




