import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
import Toast from 'react-native-simple-toast';


export const actLoadDataNews = (page = 1, iso_code) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_NEWS_REQUEST, page });
        if(page === 1) {
            dispatch({ type: Types.UPDATE_OLD_NOTIFICATION });
        }
        return HTTP.callApiWithHeader('notifications?page=' + page + '&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.status == 200) {
                dispatch({ type: Types.LOAD_DATA_NEWS_SUCCESS, data: response.data, page: page });
            } else {
                dispatch({ type: Types.LOAD_DATA_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_NEWS_FAILURE });
        });
    };
}

export const actClearDataNotification = () => {
    return dispatch => {
        dispatch({ type: Types.CLEAR_DATA_NOTIFICATION });
    };
}

export const actLoadDataUnreadNews = (page = 1, iso_code) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_UNREAD_NEWS_REQUEST, page });
        return HTTP.callApiWithHeader('notifications/unread-news?page=' + page + '&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.status == 200) {
                dispatch({ type: Types.LOAD_DATA_UNREAD_NEWS_SUCCESS, data: response.data, page: page });
            } else {
                dispatch({ type: Types.LOAD_DATA_UNREAD_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_UNREAD_NEWS_FAILURE });
        });
    };
}

export const actLoadDataReadNews = (page = 1, iso_code) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_READ_NEWS_REQUEST, page });
        return HTTP.callApiWithHeader('notifications/read-news?page=' + page + '&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.status == 200) {
                dispatch({ type: Types.LOAD_DATA_READ_NEWS_SUCCESS, data: response.data, page: page });
            } else {
                dispatch({ type: Types.LOAD_DATA_READ_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_READ_NEWS_FAILURE });
        });
    };
}

export const actReadNotification = (newsId) => {
    return dispatch => {
        dispatch({ type: Types.READ_NEWS, newsId });
    };
}

export const actMarkReadAll = () => {
    return dispatch => {
        dispatch({ type: Types.MARK_READ_ALL_REQUEST });
        return HTTP.callApiWithHeader('notifications/mark-read-all', 'POST', null).then(response => {
            if (response && response.status == 200) {
                dispatch({ type: Types.MARK_READ_ALL_SUCCESS });
            } else {
                dispatch({ type: Types.MARK_READ_ALL_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.MARK_READ_ALL_FAILURE });
        });
    };
}


