import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
import Toast from 'react-native-simple-toast';
import NavigationService from '../router/NavigationService';


export const actLoadDataLiked = (page = 1, iso_code) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_LIKE_REQUEST });
        return HTTP.callApiWithHeader('liked?page=' + page + '&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_LIKE_SUCCESS, data: response.data, page: page, total: response.data.posts.total });
            } else {
                dispatch({ type: Types.LOAD_DATA_LIKE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_LIKE_FAILURE });
        });
    };
}

export const actLoadDataEventLiked = (page = 1) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_LIKE_REQUEST });
        return HTTP.callApiWithHeader('liked/events/now?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                console.log(response.data.events.total)
                if(response.data.events.total === 0) {
                    NavigationService.replace('PastEventLiked');
                }
                dispatch({ type: Types.LOAD_DATA_EVENT_LIKE_SUCCESS, data: response.data.events.data, total: response.data.events.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_EVENT_LIKE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_LIKE_FAILURE });
        });
    };
}

export const actLoadDataPastEventLiked = (page = 1) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_PAST_EVENT_LIKE_REQUEST });
        return HTTP.callApiWithHeader('liked/events/past?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_PAST_EVENT_LIKE_SUCCESS, data: response.data.events.data, total: response.data.events.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_PAST_EVENT_LIKE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_PAST_EVENT_LIKE_FAILURE });
        });
    };
}



