import * as Types from '../constants/ActionType';
import AsyncStorage from '@react-native-community/async-storage';

export const actUpdateLanguage = (language) => {
    return dispatch => {
        dispatch({ type: Types.CHANGE_LANGUAGE, data: language ? language : 'en' });
        AsyncStorage.setItem('lang', language);
    };
}

export const getLanguage = (languageCode) => {
    return dispatch => {
        AsyncStorage.getItem('lang').then((language) => {
            var local_lang = (languageCode === 'vi' || languageCode === 'ko') ? languageCode : 'en';
            dispatch({ type: Types.CHANGE_LANGUAGE, data: language ? language : local_lang });
        });
    };
}