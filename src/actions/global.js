import * as Types from '../constants/ActionType';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../router/NavigationService';
import HTTP from '../services/HTTP';

export const actShowModalLogin = () => {
    return dispatch => {
        dispatch({ type: Types.SHOW_MODAL_LOGIN });
    };
}

export const actHideModalLogin = () => {
    return dispatch => {
        dispatch({ type: Types.HIDE_MODAL_LOGIN });
    };
}
export const actGlobalCheckLogin = (nextPage = 'Home', params = {}) => dispatch => {
    AsyncStorage.getItem('auValue').then((auValue) => {
        if (auValue) {
            NavigationService.navigate(nextPage, params);
        } else {
            dispatch({ type: Types.SHOW_MODAL_LOGIN });
        }
    });
}

export const actLoadDataConfig = () => dispatch => {
    return HTTP.callApi('config', 'GET', null).then(response => {
        if (response && response.data.status == 200) {
            dispatch({ type: Types.LOAD_DATA_CONFIG_SUCCESS, data: response.data.data });
        }
    }).catch(function (error) {
        console.log(error)
    });
}

export const getCountry = (ip) => dispatch => {
    // 210.125.248.167
    return HTTP.callApi(`geo-ip?geo_ip=${ip}`, 'GET', null).then(response => {
        if (response && response.data.status == 200) {
            AsyncStorage.setItem('country', JSON.stringify(response.data.data));
            AsyncStorage.setItem('city', JSON.stringify(response.data.data.City));
            dispatch({ type: Types.LOAD_DATA_GEOIP_SUCCESS, data: response.data.data });
            dispatch({ type: Types.SET_DATA_COUNTRY_SUCCESS, data: response.data.data });
            dispatch({ type: Types.SET_DATA_CITY_SUCCESS, data: response.data.data.City });
        }
    }).catch(function (error) {
        console.log(error)
    });
}

export const getIsoCode = (ip) => dispatch => {
    // 210.125.248.167
    return HTTP.callApi(`geo-ip?geo_ip=${ip}`, 'GET', null).then(response => {
        if (response && response.data.status == 200) {
            dispatch({ type: Types.LOAD_DATA_GEOIP_SUCCESS, data: response.data.data });
        }
    }).catch(function (error) {
        console.log(error)
    });
}

export const setGlobalCity = (city) => {
    return dispatch => {
        dispatch({ type: Types.SET_DATA_CITY_SUCCESS, data: city });
    }
}

export const setGlobalCountry = (country) => {
    return dispatch => {
        dispatch({ type: Types.SET_DATA_COUNTRY_SUCCESS, data: country });
    }
}

export const setChangeCity = (city, country) => {
    return dispatch => {
        dispatch({ type: Types.SET_DATA_CITY_SUCCESS, data: city });
        dispatch({ type: Types.SET_DATA_COUNTRY_SUCCESS, data: country });
    }
}

export const actUseLinking = () => {
    return dispatch => {
        dispatch({ type: Types.USE_LINKING });
    }
}


