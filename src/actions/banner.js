import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';
// let HTTP = require('../services/HTTP');
import Toast from 'react-native-simple-toast';


export const actBannerLists = (id, type) => {
    var url = '';
    if(type === 'city') {
        url = `banners?page=1&per_page=12&city_id=${id}`;
    } else {
        url = `banners?page=1&per_page=12&iso_code=${id}`;
    }
    return dispatch => {
        dispatch({type: Types.BANNER_LIST_REQUEST});
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({type: Types.BANNER_LIST_SUCCESS, data: response.data.data, page: 1});
            } else {
                dispatch({type: Types.BANNER_LIST_FAILURE});
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({type: Types.BANNER_LIST_FAILURE});
        });
    };
}




