import { Platform } from 'react-native';

module.exports = ({
  VERSION: (Platform.OS === 'ios') ? '4.0.0' : '4.0.0',
});