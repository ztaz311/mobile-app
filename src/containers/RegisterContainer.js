import React, { Component } from "react";
import { connect } from "react-redux";

import { actLogin } from '../actions/auth';
import Register from '../screens/Register';

class Container extends Component {
    render() {
        var { onSubmit, loggingIn, errors } = this.props;
        return (
            <Register onSubmit={onSubmit} loggingIn={loggingIn} errors={errors} />
        );
    }
}

const mapStateToProps = state => {
    const { loggingIn, errors } = state.authentication;
    return {
        loggingIn,
        errors
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSubmit: (body) => {
            dispatch(actLogin(props, body))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);