import React, { Component } from "react";
import { connect } from "react-redux";
import { actLogin } from '../actions/auth';
import Login from '../screens/LoginWithMail';

class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }
    render() {
        let { navigation, loading } = this.props;
        return (
            <LoginWithMail
                navigation={navigation}
                loading={loading} />
        );
    }
}

const mapStateToProps = state => {
    const { loading } = state.authentication;
    return {
        loading
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSubmit: (auKey, auValue, goToWelcome = false, navigation) => {
            dispatch(actLogin(auKey, auValue, goToWelcome, navigation))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);