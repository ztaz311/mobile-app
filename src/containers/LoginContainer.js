import React, { Component } from "react";
import { connect } from "react-redux";
import { actLogin } from '../actions/user';
import { actUpdateLanguage } from '../actions/language';
import Login from '../screens/Login';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import appleAuth, {
	AppleAuthError,
	AppleAuthRequestScope,
	AppleAuthRealUserStatus,
	AppleAuthCredentialState,
	AppleAuthRequestOperation,
} from '@invertase/react-native-apple-authentication';

class LoginContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false
		};
	}
	componentDidMount() {
		this._setupGoogleSignin();
	}
	async _setupGoogleSignin() {
		try {
			await GoogleSignin.configure({
				webClientId: '1058119835706-onr4c64vqgdt5iiqp4upsa2b2psqvrg7.apps.googleusercontent.com',
				offlineAccess: false,
				iosClientId: '1058119835706-k22o7reor4deq5tbjrpdvjorfdaksogu.apps.googleusercontent.com'
			});
		}
		catch (err) {
			console.log("Google signin error", err.code, err.message);
		}
	}
	facebook = () => {
		var { navigation } = this.props;
		// Attempt a login using the Facebook login dialog asking for default permissions.
		LoginManager.logInWithPermissions(["public_profile", "email"]).then(
			(result) => {
				if (result.isCancelled) {
					//alert("Login cancelled");
				} else {
					AccessToken.getCurrentAccessToken().then(
						(data) => {
							const accessToken = data.accessToken.toString()
							if (accessToken) this.props.onSubmit('Fbtoken', accessToken, true, navigation)
							else alert('Error')
						}
					)
				}
			},
			(error) => {
				alert("Login fail with error: " + error);
			}
		);
	}

	//accessToken
	google = async () => {
		var { navigation } = this.props;
		GoogleSignin.signIn().then(() => {
			GoogleSignin.getTokens().then((token) => {
				this.props.onSubmit('Ggtoken', token.accessToken, true, navigation)
			}).catch((err) => {
				console.log('WRONG SIGNIN', err);
			});
		})
			.catch((err) => {
				// console.log(err);
			})
	}

	apple = (token) => {
		var { navigation } = this.props;
		this.props.onSubmit('Aptoken', token, true, navigation)
	};

	appleIos = async () => {
		var { navigation } = this.props;
		console.warn('Beginning Apple Authentication');

		// start a login request
		try {
			const appleAuthRequestResponse = await appleAuth.performRequest({
				requestedOperation: AppleAuthRequestOperation.LOGIN,
				requestedScopes: [
					AppleAuthRequestScope.EMAIL,
					AppleAuthRequestScope.FULL_NAME,
				],
			});

			console.log('appleAuthRequestResponse', appleAuthRequestResponse);

			const {
				user: newUser,
				email,
				nonce,
				identityToken,
				realUserStatus /* etc */,
			} = appleAuthRequestResponse;

			this.user = newUser;

			this.fetchAndUpdateCredentialState()
				.then(res => this.setState({ credentialStateForUser: res }))
				.catch(error =>
					this.setState({ credentialStateForUser: `Error: ${error.code}` }),
				);

			if (identityToken) {
				// e.g. sign in with Firebase Auth using `nonce` & `identityToken`
				this.props.onSubmit('Aptoken', identityToken, true, navigation)
			} else {
				// no token - failed sign-in?
			}

			if (realUserStatus === AppleAuthRealUserStatus.LIKELY_REAL) {
				// console.log("I'm a real person!");
			}

			// console.warn(`Apple Authentication Completed, ${this.user}, ${email}`);
		} catch (error) {
			if (error.code === AppleAuthError.CANCELED) {
				// console.warn('User canceled Apple Sign in.');
			} else {
				// console.error(error);
			}
		}
	};

	fetchAndUpdateCredentialState = async () => {
		if (this.user === null) {
			this.setState({ credentialStateForUser: 'N/A' });
		} else {
			const credentialState = await appleAuth.getCredentialStateForUser(this.user);
			if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
				this.setState({ credentialStateForUser: 'AUTHORIZED' });
			} else {
				this.setState({ credentialStateForUser: credentialState });
			}
		}
	}

	updateLanguage = (language) => {
		this.props.onUpdateLanguage(language)
	}

	render() {
		var { loading, errors, navigation, language } = this.props;
		return (
			<Login facebook={this.facebook} google={this.google} apple={(token) => this.apple(token)} updateLanguage={this.updateLanguage} appleIos={() => this.appleIos()} navigation={navigation} loading={loading} errors={errors} language={language} />
		);
	}
}

const mapStateToProps = state => {
	const { loading, errors } = state.user;
	const { language } = state.language;
	return {
		loading,
		errors,
		language
	}
}

const mapDispatchToProps = (dispatch, props) => {
	return {
		onSubmit: (auKey, auValue, goToWelcome = false, navigation) => {
			dispatch(actLogin(auKey, auValue, goToWelcome, navigation))
		},
		onUpdateLanguage: (language) => {
			dispatch(actUpdateLanguage(language))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);