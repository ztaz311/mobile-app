import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalCity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: ''
        }
    }


    render() {
        var { countries } = this.props;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <SrcView style={styles.container}>
                        <View style={styles.content}>
                            <Text style={[styles.txtCountry, { borderBottomWidth: 0 }]}>{convertLanguage(language, 'select_a_city')}</Text>
                            {
                                countries.map((item, index) => {
                                    return <React.Fragment key={index}>
                                        <Text style={[styles.txtCountry, { borderBottomWidth: this.state.current === index ? 1 : 0 }]} onPress={() => this.setState({ current: this.state.current === index ? '' : index })}>{item['Name_' + language]}</Text>
                                        {
                                            this.state.current === index &&
                                            <View style={[styles.boxChildren, { borderBottomWidth: item.Cites.length > 0 ? 1 : 0 }]}>
                                                {
                                                    item.Cites.map((city, index2) => {
                                                        return <Text style={styles.txtCity} key={index2} onPress={() => this.props.selectCity(city, item)}>{city['Name_' + language]}</Text>
                                                    })
                                                }
                                            </View>
                                        }
                                    </React.Fragment>
                                })
                            }
                        </View>
                    </SrcView>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        height: 400,
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 5
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        paddingHorizontal: 20,
        textAlign: 'center'
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default React.memo(connect(mapStateToProps, null)(ModalCity));