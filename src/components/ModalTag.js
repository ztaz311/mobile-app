import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import Touchable from '../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalTag extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: ''
        }
    }


    render() {
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.container}>
                        <View style={styles.content}>
                            <Text style={[styles.txtCountry, { borderBottomWidth: 0 }]}>{convertLanguage(language, 'select_a_category')}</Text>
                            <View style={styles.boxListCategory}>
                                {
                                    this.props.tags.map((data, index) => {
                                        var backgroundColor = '#white';
                                        var borderColor = '#333333'
                                        var color = "#333333";
                                        var borderRadius = 4;
                                        var fontWeight = 'normal';
                                        var marginRight = (index + 1) % 3 === 0 ? 0 : 10;
                                        var borderWidth = 1;
                                        return <Touchable style={[styles.btnCategory, { borderRadius, borderColor, backgroundColor, marginRight, borderWidth }]} key={index} onPress={() => this.props.selectTag(data)}>
                                            <Text style={[styles.txtCategory, { color, fontWeight }]} numberOfLines={1}>{index === 0 ? '' : '#'}{data.name}</Text>
                                        </Touchable>
                                    })
                                }
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    boxListCategory: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    btnCategory: {
        width: (width - 110) / 3,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 15,
        borderColor: '#00acfb'
    },
    txtCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        textAlign: 'center'
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default React.memo(connect(mapStateToProps, null)(ModalTag));