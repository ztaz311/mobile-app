
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Alert,
  Modal,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native'
import qs from 'qs'
import { WebView } from "react-native-webview";
const { width, height } = Dimensions.get('window')

import axios from 'axios'
const patchPostMessageJsCode = `(${String(function () {
  var originalPostMessage = window.postMessage
  var patchedPostMessage = function (message, targetOrigin, transfer) {
    originalPostMessage(message, targetOrigin, transfer)
  }
  patchedPostMessage.toString = function () {
    return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage')
  }
  window.postMessage = patchedPostMessage
})})();`

export default class Apple extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      key: 1
    }
  }

  show() {
    this.setState({ modalVisible: true })
  }

  hide() {
    this.setState({ modalVisible: false })
  }

  async _onMessage(reactMessage) {
    this.hide()
    const data = JSON.parse(reactMessage.nativeEvent.data);
    if (data.type === 'fail') {
      this.props.onLoginFailure();
      return;
    }
    const { appId, appSecret, redirectUrl } = this.props
    let headers = { 'Content-Type': 'application/x-www-form-urlencoded' }
    var body = {
      redirect_uri: redirectUrl,
      grant_type: 'authorization_code',
      client_secret: appSecret,
      client_id: appId,
      code: data.code,
    }
    var formData = qs.stringify(body);
    return axios({
      method: 'post',
      url: `https://appleid.apple.com/auth/token`,
      data: formData,
      headers: headers,
      timeout: 30000,
    }).then(response => {
      this.props.onLoginSuccess(response.data);
    }).catch((error) => {
      this.props.onLoginFailure()
    });
  }

  // _onLoadEnd () {
  //   const scriptToPostBody = "window.postMessage(document.body.innerText, '*')"
  //     this.webView.injectJavaScript(scriptToPostBody)
  // }

  renderClose() {
    const { renderClose } = this.props
    if (renderClose) return renderClose()
    return (
      <Image source={require('./assets/close-button.png')} style={styles.imgClose} resizeMode="contain" />
    )
  }

  onClose() {
    const { onClose } = this.props
    if (onClose) onClose()
    this.setState({ modalVisible: false })
  }

  makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  renderWebview() {
    const { appId, appSecret, redirectUrl, scopes, responseType } = this.props
    const { key } = this.state

    let ig_uri = `https://appleid.apple.com/auth/authorize?client_id=${appId}&redirect_uri=${redirectUrl}&scope=${scopes.join(' ')}&response_type=code&response_mode=form_post`
    return (
      <WebView
        {...this.props}
        key={key}
        style={[styles.webView, this.props.styles.webView]}
        source={{ uri: ig_uri }}
        startInLoadingState
        onMessage={this._onMessage.bind(this)}
        ref={(webView) => { this.webView = webView }}
        injectedJavaScript={patchPostMessageJsCode}
        cacheEnabled={false}
        incognito={true}
        thirdPartyCookiesEnabled={false}
        sharedCookiesEnabled={false}
        domStorageEnabled={false}
      />
    )
  }

  render() {
    const { wrapperStyle, containerStyle, closeStyle } = this.props
    return (
      <Modal
        animationType={'slide'}
        visible={this.state.modalVisible}
        onRequestClose={this.hide.bind(this)}
        transparent
      >
        <View style={[styles.container, containerStyle]}>
          <View style={[styles.wrapper, wrapperStyle]}>{this.renderWebview()}</View>
          <TouchableOpacity
            onPress={() => this.onClose()}
            style={[styles.close, closeStyle]}
            accessibilityComponentType={'button'}
            accessibilityTraits={['button']}
          >
            {this.renderClose()}
          </TouchableOpacity>
        </View>
      </Modal >

    )
  }
}
const propTypes = {
  appId: PropTypes.string.isRequired,
  appSecret: PropTypes.string.isRequired,
  redirectUrl: PropTypes.string,
  scopes: PropTypes.array,
  onLoginSuccess: PropTypes.func,
  modalVisible: PropTypes.bool,
  onLoginFailure: PropTypes.func,
  responseType: PropTypes.string,
  containerStyle: PropTypes.object,
  wrapperStyle: PropTypes.object,
  closeStyle: PropTypes.object,
}

const defaultProps = {
  redirectUrl: 'https://google.com',
  styles: {},
  scopes: ['basic'],
  onLoginSuccess: (token) => {
    Alert.alert(
      'Alert Title',
      'Token: ' + token,
      [
        { text: 'OK' }
      ],
      { cancelable: false }
    )
  },
  onLoginFailure: (failureJson) => {
    console.debug(failureJson)
  },
  responseType: 'code',
}

Apple.propTypes = propTypes
Apple.defaultProps = defaultProps

const styles = StyleSheet.create({
  webView: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    paddingVertical: 40,
    paddingHorizontal: 10,
  },
  wrapper: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 5,
    borderColor: 'rgba(0, 0, 0, 0.6)',
  },
  close: {
    position: 'absolute',
    top: 35,
    right: 5,
    backgroundColor: '#000',
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'rgba(0, 0, 0, 0.4)',
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15
  },
  imgClose: {
    width: 30,
    height: 30,
  }
})
