import React, { PureComponent } from 'react';
import { TouchableOpacity, ActivityIndicator } from 'react-native';

export default class TouchableLoading extends PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <TouchableOpacity
                {...this.props}
            > 
                { this.props.children }
                {
                    this.props.loading ? 
                        <ActivityIndicator size={this.props.loadSize ? this.props.loadSize : 'small'} color={this.props.loadColor ? this.props.loadColor : '#000000'} style={[{paddingLeft: 5},this.props.loadStyle]}/>
                    : null
                }
            </TouchableOpacity>
        );
    }
}
