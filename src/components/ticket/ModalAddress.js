import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TextInput, Platform, TouchableWithoutFeedback, Keyboard, ScrollView, KeyboardAvoidingView } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import PickerItem from '../../screens_view/PickerItem';
import Touchable from '../../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalAddress extends Component {
    constructor(props) {
        super(props);
        this.scrollView = null;
        this.state = {
            City: this.props.City,
            District: this.props.District,
            Address: this.props.Address
        }
    }

    showName(value, array) {
        var datas = array.filter((data) => data.value === value);
        return datas.length > 0 ? datas[0].name : '';
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
    }

    _keyboardDidShow = () => {
        this.scrollView.scrollToEnd();
    }

    render() {
        var { language } = this.props.language;
        var { modalAddress, arrCity, arrDistrict } = this.props;
        var { City, District, Address } = this.state;
        return (
            <Modal
                isVisible={modalAddress}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 0} >
                        <View style={styles.modalContent}>
                            <Touchable style={styles.btnClose} onPress={() => this.props.closeModal()} >
                                <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                            </Touchable>
                            <ScrollView style={styles.boxContentSelect} ref={(ref) => this.scrollView = ref}>
                                <Text style={[styles.txtTicketPrice, { fontWeight: 'bold' }]}>{convertLanguage(language, 'address')}</Text>
                                <PickerItem
                                    boxDisplayStyles={styles.boxPicker}
                                    containerStyle={{ height: '100%', paddingLeft: 10, paddingRight: 10 }}
                                    options={arrCity}
                                    selectedOption={City}
                                    isChange={false}
                                    onSubmit={(value) => this.setState({ City: value })}
                                />
                                <PickerItem
                                    boxDisplayStyles={styles.boxPicker}
                                    containerStyle={{ height: '100%', paddingLeft: 10, paddingRight: 10 }}
                                    options={arrDistrict}
                                    selectedOption={District}
                                    isChange={false}
                                    onSubmit={(value) => this.setState({ District: value })}
                                />
                                <TextInput
                                    style={styles.ipAddress}
                                    selectionColor="#bcbcbc"
                                    multiline={true}
                                    placeholder={'Write the rest address'}
                                    placeholderTextColor={'#757575'}
                                    value={Address}
                                    onChangeText={(Address) => this.setState({ Address })}
                                />
                                <Touchable style={[styles.btnOk, { backgroundColor: Address === '' || District === '' || City === '' ? '#e8e8e8' : '#03a9f4' }]} disabled={Address === '' || District === '' || City === ''} onPress={() => this.props.selectAddress(this.state)}>
                                    <Text style={[styles.txtOk, { color: Address === '' || District === '' || City === '' ? '#bdbdbd' : '#FFFFFF' }]}>{convertLanguage(language, 'get_ticket')}</Text>
                                </Touchable>
                            </ScrollView>
                        </View>
                </KeyboardAvoidingView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        justifyContent: 'space-around',
        borderRadius: 5
    },
    btnClose: {
        alignSelf: 'flex-end',
        padding: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    boxContentSelect: {
        padding: 30,
        paddingTop: 20,
    },
    boxPicker: {
        borderWidth: 1,
        borderColor: '#757575',
        height: 35,
        borderRadius: 2,
        width: '100%',
        marginTop: 15
    },
    txtPicker: {
        color: '#757575',
        fontWeight: 'bold',
        fontSize: 14,
        paddingLeft: 5
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5
    },
    btnOk: {
        marginLeft: 50,
        marginRight: 50,
        marginTop: 20,
        height: 48,
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtOk: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    ipAddress: {
        height: 80,
        borderWidth: 1,
        borderColor: '#757575',
        padding: 5,
        textAlignVertical: 'top',
        width: '100%',
        marginTop: 15
    },
    boxAddress: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 5,
        paddingTop: 5,
        marginBottom: 5,
        marginTop: 5,
    },
    txtAddress: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333',
        textAlign: 'center'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalAddress);