import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Platform, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalDataRefundInfo extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        var { data } = this.props;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.boxTitle}>
                            <Text style={styles.txtTitle}>{convertLanguage(language, 'refund_info')}</Text>
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'bank_name')}: <Text style={styles.txtContent}>{data.refund.BankName || 'N/A'}</Text></Text>
                        </View>
                        {
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'card_number')}: <Text style={styles.txtContent}>{data.refund.CardNo || 'N/A'}</Text></Text>
                            </View>
                        }
                        {
                            data.refund.AcountHolderName != null && data.refund.AcountHolderName !== "" &&
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'account_holder_name')}: <Text style={styles.txtContent}>{data.refund.AcountHolderName}</Text></Text>
                            </View>
                        }
                        {
                            data.refund.BankBranch != null && data.refund.BankBranch !== "" &&
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'bank_branch')}: <Text style={styles.txtContent}>{data.refund.BankBranch}</Text></Text>
                            </View>
                        }
                        {
                            data.refund.BankUser != null && data.refund.BankUser !== "" &&
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'acount_name')}: <Text style={styles.txtContent}>{data.refund.BankUser}</Text></Text>
                            </View>
                        }
                        {
                            data.refund.BankNumber != null && data.refund.BankNumber !== "" &&
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'acount_number')}: <Text style={styles.txtContent}>{data.refund.BankNumber}</Text></Text>
                            </View>
                        }
                        {
                            (data.ticketDetail.Status != 6 && data.ticketDetail.Payment.Type === 2) &&
                            <Touchable disabled={data.ticketDetail.Status === -7} style={data.ticketDetail.Status === -7 ? styles.btnAcceptDis : styles.btnAccept} onPress={() => this.props.onOpenModalRefundInfo()}>
                                <Image source={data.ticketDetail.Status === -7 ? require('../../assets/edit_dis.png') : require('../../assets/edit.png')} style={styles.arrow_right} />
                                <Text style={data.ticketDetail.Status === -7 ? styles.txtAcceptedDis : styles.txtAccepted}>{convertLanguage(language, 'edit_refund_info')}</Text>
                            </Touchable>
                        }
                        {
                            data.ticketDetail.Status === -7 &&
                            <Text style={styles.txtProceeded}>{convertLanguage(language, 'pop_status_refund')}</Text>
                        }
                        {
                            data.ticketDetail.Status === 5 &&
                            <Text style={[styles.txtProceeded, { color: '#EB5757' }]}>{convertLanguage(language, 'pop_status_refund1')}</Text>
                        }
                    </View>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 10
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 12,
    },
    boxInput: {
        paddingBottom: 5,
    },
    txtInput: {
        fontSize: 14,
        color: '#bdbdbd',
        fontWeight: 'bold',
        marginLeft: 10
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        fontWeight: '400',
    },
    boxInputContent: {
        justifyContent: 'center',
    },
    boxInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8'
    },
    txtTitle: {
        fontSize: 20,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxTitle: {
        paddingBottom: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    btnAccept: {
        borderWidth: 1,
        borderColor: '#00A9F4',
        borderRadius: 4,
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 20
    },
    txtAccepted: {
        fontSize: 16,
        color: '#00A9F4'
    },
    arrow_right: {
        width: 18,
        height: 18,
        marginRight: 5
    },
    btnAcceptDis: {
        borderWidth: 1,
        borderColor: '#B3B8BC',
        borderRadius: 4,
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 20
    },
    txtAcceptedDis: {
        fontSize: 16,
        color: '#B3B8BC'
    },
    txtProceeded: {
        fontSize: 12,
        color: '#4F4F4F',
        marginTop: 5
    }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalDataRefundInfo);