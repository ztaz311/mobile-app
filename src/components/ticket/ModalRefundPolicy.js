import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import Touchable from '../../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import { Modalize } from 'react-native-modalize';
class ModalRefundPolicy extends Component {
    modal = React.createRef();
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        if (this.modal.current) {
            this.modal.current?.open();
            this.props.navigation.setParams({visible: true})
        }
    }

    closeModal = () => {
        if (this.modal.current) {
            this.modal.current.close();
            this.props.navigation.setParams({visible: false})
        }
    };


    render() {
        var { language } = this.props.language;
        return (
            <Modalize
                ref={this.modal}
                scrollViewProps={{
                    showsVerticalScrollIndicator: false,
                    stickyHeaderIndices: [0],
                }}
                onClosed={() => this.props.closeModal()}
            >
                <View style={styles.boxInfo}>
                    <View style={styles.boxRow}>
                        <View style={styles.boxClose}></View>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'refund_policy')}</Text>
                        <Touchable style={styles.boxClose} onPress={this.closeModal}>
                            <Image source={require('../../assets/close.png')} style={styles.icClose} />
                        </Touchable>
                    </View>
                </View>
                <View style={styles.content}>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_1')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_2')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_3')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_4')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_5')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_6')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_7')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_7_1')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_7_2')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_ofter')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_ofter1')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'refund_policy_ofter2')}</Text>
                </View>
            </Modalize>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        paddingLeft: 15,
        paddingRight: 15
    },
    boxRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInfo: {
        flexDirection: 'row',
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.85)',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    boxClose: {
        width: 40,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    icClose: {
        width: 30,
        height: 30
    },
    txtTitle: {
        fontSize: 20,
        color: '#333333',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 14,
        color: '#333333',
        textAlign: 'justify',
        marginBottom: 8
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalRefundPolicy);