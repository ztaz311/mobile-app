import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
import { Linking } from 'react-native';
import Touchable from '../../screens_view/Touchable';
import { Modalize } from 'react-native-modalize';
class ModalHowToUse extends Component {
    modal = React.createRef();
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        if (this.modal.current) {
            this.modal.current?.open();
            this.props.navigation.setParams({visible: true})
        }
    }

    closeModal = () => {
        if (this.modal.current) {
            this.modal.current.close();
            this.props.navigation.setParams({visible: false})
        }
    };

    render() {
        var { language } = this.props.language;
        var { Title } = this.props;
        return (
            <Modalize
                ref={this.modal}
                scrollViewProps={{
                    showsVerticalScrollIndicator: false,
                    stickyHeaderIndices: [0],
                }}
                // withReactModal
                onClosed={() => this.props.closeModal()}
            >
                <View style={styles.boxInfo}>
                    <View style={styles.boxRow}>
                        <View style={styles.boxClose}></View>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'how_to_use_tickets')}?</Text>
                        <Touchable style={styles.boxClose} onPress={this.closeModal}>
                            <Image source={require('../../assets/close.png')} style={styles.icClose} />
                        </Touchable>
                    </View>
                </View>
                <View style={styles.content}>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'how_use_ticket_1')} {convertLanguage(language, 'e_ticket')} {convertLanguage(language, 'or')} {convertLanguage(language, 'cod_ticket')}</Text>
                    <Text style={[styles.txtContent, { fontWeight: 'bold' }]}>👉🏻 {convertLanguage(language, 'e_ticket')}:</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'e_ticket_tut_1')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'e_ticket_tut_2')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'e_ticket_tut_3')}</Text>
                    <Text style={[styles.txtContent, { fontWeight: 'bold' }]}>👉🏻 {convertLanguage(language, 'cod_ticket')}:</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'cod_ticket_tut_1')}</Text>
                    <Text style={styles.txtContent}>{convertLanguage(language, 'cod_ticket_tut_2')}</Text>

                    <Text style={styles.txtContent}>{convertLanguage(language, 'how_use_ticket_2')}<Text style={[styles.txtContent, { color: 'blue' }]} onPress={() => { Linking.openURL('mailto:info@comeup.vn?Subject=Tiêu đề') }}>{convertLanguage(language, 'how_use_ticket_2_here')}</Text><Text style={styles.txtContent}>{convertLanguage(language, 'how_use_ticket_2_2')}</Text></Text>



                </View>
            </Modalize>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        paddingLeft: 15,
        paddingRight: 15
    },
    boxRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInfo: {
        flexDirection: 'row',
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.85)',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    boxClose: {
        width: 40,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    icClose: {
        width: 30,
        height: 30
    },
    txtTitle: {
        fontSize: 20,
        color: '#333333',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 13,
        color: '#333333',
        textAlign: 'justify',
        marginBottom: 20
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalHowToUse);