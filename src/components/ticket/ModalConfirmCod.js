import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import Touchable from '../../screens_view/Touchable';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalConfirmCod extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: ''
        }
    }


    render() {
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                onBackdropPress={() => this.props.closeModal()}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <Touchable style={styles.btnCloseModal} onPress={() => this.props.closeModal()} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </Touchable>
                    </View>
                    <View style={styles.contentModal}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'are_you_sure_to_book_this_ticket')}</Text>
                        <View style={styles.boxButton}>
                            <Touchable style={styles.btnNo} onPress={() => this.props.onPayment('cash-on-delivery')} >
                                <Text style={styles.txtNo}>{convertLanguage(language, 'yes')}</Text>
                            </Touchable>
                            <Touchable style={styles.btnNo} onPress={() => this.props.closeModal()} >
                                <Text style={styles.txtNo}>{convertLanguage(language, 'no')}</Text>
                            </Touchable>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    contentModal: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnCloseModal: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtTitle: {
        fontSize: 14,
        color: '#333333',
        textAlign: 'center'
    },
    boxButton: {
        marginTop: 24,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    btnDiscard: {
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        flex: 0.45,
        borderWidth: 1,
        borderColor: '#EB5757'
    },
    txtDiscard: {
        color: '#EB5757',
        fontSize: 16
    },
    btnNo: {
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        flex: 0.45,
        borderWidth: 1,
        borderColor: '#4F4F4F'
    },
    txtNo: {
        color: '#4F4F4F',
        fontSize: 16
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalConfirmCod);