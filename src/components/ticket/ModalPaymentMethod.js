import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import Touchable from '../../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalPaymentMethod extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    checkPaymentGate() {
        var { purchases } = this.props;
        var checkPaymentGate = false;
        for (let i = 0; i < purchases.length; i++) {
            if (purchases[i].Type === 'e-ticket')  {
                checkPaymentGate = true;
                break;
            }
        }
        return checkPaymentGate;
    }

    checkDelivery() {
        var { purchases } = this.props;
        var checkDelivery = true;
        for (let i = 0; i < purchases.length; i++) {
            if (purchases[i].Type === 'e-ticket')  {
                checkDelivery = false;
                break;
            }
        }
        return checkDelivery;
    }

    render() {
        var { modalPaymentMethod, unitTicket } = this.props;
        console.log(unitTicket)
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={modalPaymentMethod}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                style={{marginLeft: 55, marginRight: 55}}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <Touchable style={styles.btnClose} onPress={() => this.props.closeModal()} >
                        <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                    </Touchable>
                    <View style={styles.boxContentSelect}>
                        <Text style={[styles.txtTicketPrice, { fontWeight: 'bold' }]}>{convertLanguage(language, 'select_a_payment_method')}</Text>
                        {/* <Touchable disabled={!this.checkPaymentGate()} onPress={() => this.props.onPayment('foreign-card')} style={[styles.btnMethod, this.checkPaymentGate() ? {} : {backgroundColor: '#cccccc', borderColor: '#cccccc'}]}>
                            <Text style={[styles.txtMethod, this.checkPaymentGate() ? {} : {color: '#FFFFFF'}]}>{convertLanguage(language, 'domestic_foreign_card')}</Text>
                        </Touchable> */}
                        <Touchable onPress={() => this.props.onPayment('domestic-card')} style={[styles.btnMethod]}>
                            <Text style={[styles.txtMethod]}>{convertLanguage(language, (unitTicket === 'KWR' || unitTicket === 'KRW') ? 'korean_card' : 'domestic_foreign_card')}</Text>
                        </Touchable>
                        {
                            (unitTicket === 'KWR' || unitTicket === 'KRW') &&
                            <Touchable onPress={() => this.props.onPayment('bank-transfer')} style={[styles.btnMethod]}>
                                <Text style={[styles.txtMethod]}>{convertLanguage(language, 'bank_transfer')}</Text>
                            </Touchable>
                        }
                        {
                            (unitTicket === 'KWR' || unitTicket === 'KRW') &&
                            <Touchable onPress={() => this.props.showModalComingSoon()} style={[styles.btnMethod]}>
                                <Text style={[styles.txtMethod]}>{convertLanguage(language, 'foreign_card')}</Text>
                            </Touchable>
                        }
                        {
                            this.checkDelivery() &&
                            <Touchable disabled={!this.checkDelivery()} onPress={() => this.props.onConfirmCod()} style={[styles.btnMethod, this.checkDelivery() ? {} : {backgroundColor: '#cccccc', borderColor: '#cccccc'}]}>
                                <Text style={[styles.txtMethod, this.checkDelivery() ? {} : {color: '#FFFFFF'}]}>{convertLanguage(language, 'cash_on_delivery')}</Text>
                            </Touchable>
                        }
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    btnClose: {
        alignSelf: 'flex-end',
        padding: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    boxContentSelect: {
        alignItems: 'center',
        margin: 20,
        marginTop: 10,
        marginBottom: 30
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333'
    },
    btnMethod: {
        width: '90%',
        height: 49,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
    },
    txtMethod: {
        fontSize: 15,
        color: '#333333'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalPaymentMethod);