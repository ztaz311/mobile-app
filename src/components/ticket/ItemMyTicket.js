import React, { Component } from 'react';
import { Text, Image, ImageBackground, StyleSheet, Dimensions, View } from 'react-native';
import Touchable from '../../screens_view/Touchable'
const { width, height } = Dimensions.get('window')
import { connect } from "react-redux";
class ItemMyTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    showStatus(Status) {
        var { language } = this.props.language;
        switch (Status) {
            case 1:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'used')}</Text>
            case 2:
                return <Text style={styles.txtPending}>{convertLanguage(language, 'refund_pending')}</Text>
            case 3:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'processing')}</Text>
            case 4:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'event_finished')}</Text>
            case 5:
                return <Text style={styles.txtIncorrect}>{convertLanguage(language, 'acount_incorrect')}</Text>
            case 6:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'refunded')}</Text>
            case -1:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'requested_refund')}</Text>
            case -2:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'add_your_bank_acount')}</Text>
            case -3:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'rejected_refund')}</Text>
            case -4:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'event_cancel')}</Text>
            case -5:
                return <Text style={styles.txtUsed}>{convertLanguage(language, 'ticket_canceled')}</Text>
            case -6:
            case -7:
                return <Text style={styles.txtPending}>{convertLanguage(language, 'refund_pending')}</Text>
            default:
                break;
        }
    }

    renderText(item) {
        var { event, indexTicket, type } = this.props;
        var { language } = this.props.language;
        if (item.Status == 0 || item.Status == -4) {
            // if (item.CheckRefund) {
            //     return <Touchable style={styles.btnStatus} onPress={() => this.props.navigation.navigate('RequestRefund', { ticketDetailId: item.Id, event, indexTicket, type })}>
            //         <Text style={styles.txtStatus} numberOfLines={1}>{convertLanguage(language, 'history')}</Text>
            //     </Touchable>
            // } else {
                return <Touchable style={styles.btnStatus} onPress={() => this.props.navigation.navigate('RequestRefund', { ticketDetailId: item.Id, event, indexTicket, type })}>
                    <Text style={styles.txtStatus} numberOfLines={1}>{convertLanguage(language, 'refund')}</Text>
                </Touchable>
            // }
        }
        if (item.Status == -1 || item.Status == -3 || item.Status == -6 || item.Status == 6 || item.Status == -7) {
            return <Touchable style={styles.btnStatus} onPress={() => this.props.navigation.navigate('RequestRefund', { ticketDetailId: item.Id, event, indexTicket, type })}>
                <Text style={styles.txtStatus} numberOfLines={1}>{convertLanguage(language, 'history')}</Text>
            </Touchable>
        }
        if ((item.Status == 4 || item.Status == 1) && item.CheckRefund) {
            return <Touchable style={styles.btnStatus} onPress={() => this.props.navigation.navigate('RequestRefund', { ticketDetailId: item.Id, event, indexTicket, type })}>
                <Text style={styles.txtStatus} numberOfLines={1}>{convertLanguage(language, 'history')}</Text>
            </Touchable>
        }
        if(item.Status === -2) {
            return <Touchable style={styles.btnProceed} onPress={() => this.props.navigation.navigate('RequestRefund', { ticketDetailId: item.Id, event, indexTicket, type })}>
                <Text style={styles.txtProceed} numberOfLines={1}>{convertLanguage(language, 'proceed_refund')}</Text>
            </Touchable>
        }
        if(item.Status === 5) {
            return <Touchable style={styles.btnProceed} onPress={() => this.props.navigation.navigate('RequestRefund', { ticketDetailId: item.Id, event, indexTicket, type })}>
                <Text style={styles.txtProceed} numberOfLines={1}>{convertLanguage(language, 'review_info')}</Text>
            </Touchable>
        }
    }

    render() {
        var { item, index, indexTicket, type, total } = this.props;
        var { language } = this.props.language;
        return (
            <View style={styles.boxTicket2}>
                <View style={[styles.boxRow, { justifyContent: 'space-between' }]}>
                    <View style={[styles.boxRow, { flex: 1, marginBottom: 0 }]}>
                        <Image source={item.Status != 0 && item.Status != -3 ? require('../../assets/ticket.png') : require('../../assets/ticket_active.png')} style={styles.icon} />
                        <Text style={[styles.txtTicketName, item.Status == 1 ? { color: '#828282' } : {}]}>{item.Ticket.Name}</Text>
                    </View>
                </View>
                {
                    this.showStatus(item.Status)
                }
                <View style={[styles.boxRow, { justifyContent: 'space-between' }]}>
                    <Text style={[styles.txtTicketPrice, item.Status == 1 ? { color: '#828282' } : {}]}>{item.Ticket.Unit.toUpperCase()} {formatNumber(item.Ticket.Price)}</Text>
                    <View style={styles.boxTicketType}>
                        {
                            item.Ticket.Type === 'd-p-ticket'
                                ?
                                <Image source={require('../../assets/ic_delivery_brown.png')} style={styles.ic_delivery} />
                                :
                                <Image source={require('../../assets/ic_eticket.png')} style={styles.ic_eticket} />
                        }
                        <Text style={styles.txtDelivery}>{item.Ticket.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket') : convertLanguage(language, 'e-ticket')}</Text>
                    </View>
                </View>
                {
                    item.Ticket.Description != ''
                    &&
                    <Text style={[styles.txtTicketDes, item.Status == 1 ? { color: '#828282' } : {}]}>{item.Ticket.Description}</Text>
                }
                <View style={styles.boxTicketInfo}>
                    {
                        (item.Status == 0 || item.Status == -1 || item.Status == -3) ?
                            <Touchable style={styles.btnView} onPress={() => this.props.navigation.navigate('TicketDetail', { number: index, indexTicket, type })}>
                                <Image source={require('../../assets/qrcode.png')} style={styles.icQRcode} />
                                <Text style={styles.txtView}>{convertLanguage(language, 'view_qrcode')}</Text>
                            </Touchable>
                            :
                            <Touchable style={[styles.btnView, { backgroundColor: '#E0E2E8', }]} onPress={() => this.props.navigation.navigate('TicketDetail', { number: index, indexTicket, type })}>
                                <Image source={require('../../assets/qrcode_dis.png')} style={styles.icQRcode} />
                                <Text style={[styles.txtView, { color: '#B3B8BC' }]}>{convertLanguage(language, 'view_qrcode')}</Text>
                            </Touchable>
                    }
                    {
                        (item.Ticket.Price != 0)
                        &&
                        this.renderText(item)
                    }
                </View>
                {
                    // index !== 9 && index !== tickets.length - 1 &&
                    ((total <= 10 && index < total - 1) || total > 10) &&
                    <View style={{ height: 1, width: '50%', backgroundColor: '#333333', marginTop: 16, alignSelf: 'center', opacity: 0.25 }}></View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    boxTicket2: {
        paddingTop: 16,
    },
    txtTicketName: {
        fontSize: 20,
        color: '#333333',
        fontWeight: 'bold',
        flex: 1
    },
    txtTicketDes: {
        fontSize: 13,
        color: '#757575',
        marginBottom: 15,
    },
    boxTicketPrice: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    },
    txtTicketPrice: {
        fontSize: 18,
        color: '#333333',
        fontWeight: 'bold'
    },
    btnView: {
        flex: 0.6,
        height: 36,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#03a9f4',
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        marginRight: 12
    },
    icQRcode: {
        width: 18,
        height: 18,
        marginRight: 10
    },
    txtView: {
        fontSize: 14,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    txtUsed: {
        color: '#828282',
        fontSize: 12,
        marginBottom: 8
    },
    txtIncorrect: {
        color: '#EB5757',
        fontSize: 12,
        marginBottom: 8
    },
    txtPending: {
        color: '#00A9F4',
        fontSize: 12,
        marginBottom: 8
    },
    boxTicketInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxTicketType: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ic_delivery: {
        width: 16,
        height: 16,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDelivery: {
        fontSize: 13,
        color: '#757575'
    },
    btnStatus: {
        flex: 0.4,
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#828282',
        borderRadius: 5
    },
    txtStatus: {
        fontSize: 13,
        color: '#757575'
    },
    btnProceed: {
        flex: 0.4,
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#00A9F4',
        borderRadius: 5
    },
    txtProceed: {
        fontSize: 13,
        color: '#00A9F4'
    },
    boxRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    icon: {
        width: 30,
        height: 30,
        marginRight: 7
    },
    boxViewAll: {
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#00A9F4',
        borderRadius: 4,
        marginTop: 16,
        marginHorizontal: 8
    },
    txtViewAll: {
        color: '#00A9F4',
        fontSize: 16
    },
    icRight: {
        width: 18,
        height: 18,
        marginLeft: 12
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
export default connect(mapStateToProps, null)(ItemMyTicket);
// export default ItemMyTicket;
