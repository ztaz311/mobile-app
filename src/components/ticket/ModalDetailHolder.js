import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../../screens_view/ScrView';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalDetailHolder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }


    render() {
        var { language } = this.props.language;
        var { data } = this.props;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/X_icon.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'ticket_info')}</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={styles.txtContent}>{convertLanguage(language, 'ticket_purchase')}</Text>
                            <Text style={styles.txtContent}>{data.totalQuanlity}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={styles.txtContent}>{convertLanguage(language, 'ticket_used')}</Text>
                            <Text style={styles.txtContent}>{data.totalUsedTicket}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={styles.txtContent}>{convertLanguage(language, 'ticket_refunded')}</Text>
                            <Text style={styles.txtContent}>{data.totalRefundTicket}</Text>
                        </View>
                        
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
    },
    container: {

    },
    content: {
        padding: 20,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 8,
        borderColor: '#cccccc',
        borderWidth: 1,
        borderRadius: 24,
        margin: 20,
        marginRight: 10,
        marginBottom: 10
    },
    txtTitle: {
        fontSize: 20,
        color: '#333333',
        textAlign: 'center',
        marginBottom: 30,
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        textAlign: 'justify',
        marginBottom: 15,
        flex: 0.5
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalDetailHolder);