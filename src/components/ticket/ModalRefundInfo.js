import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions, Platform, KeyboardAvoidingView, Animated, Keyboard, ScrollView } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../../screens_view/ScrView';
import Touchable from '../../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage, convertDate2 } from '../../services/Helper';
import { actCreateBankInfo } from '../../actions/ticket';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { TextField } from '../../components/react-native-material-textfield';
import Loading from '../../screens_view/Loading';
class ModalRefundInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            BankName: this.props.itemRefund.refund.BankName ? this.props.itemRefund.refund.BankName : '',
            BankBranch: this.props.itemRefund.refund.BankBranch ? this.props.itemRefund.refund.BankBranch : '',
            BankUser: this.props.itemRefund.refund.BankUser ? this.props.itemRefund.refund.BankUser : '',
            BankNumber: this.props.itemRefund.refund.BankNumber ? this.props.itemRefund.refund.BankNumber : '',
            BankContent: this.props.itemRefund.refund.BankContent ? this.props.itemRefund.refund.BankContent : '',
            keyboardShow: false
        }
        this.keyboardHeight = new Animated.Value(0);
        this.viewHeight = new Animated.Value(height - 150);
    }

    onCreateBankInfo = () => {
        var body = this.state;
        var { itemRefund, indexTicket } = this.props;
        this.props.onCreateBankInfo(itemRefund.refund.Id, body, indexTicket)
    }

    componentDidMount() {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        if (Platform.OS === 'ios') {
            Animated.parallel([
                Animated.timing(this.keyboardHeight, {
                    duration: event.duration,
                    toValue: event.endCoordinates.height,
                }),
            ]).start();
        }
        Animated.parallel([
            Animated.timing(this.viewHeight, {
                duration: event.duration,
                toValue: height - 44 - event.endCoordinates.height,
            }),
        ]).start();
        this.setState({ keyboardShow: true })
    };

    keyboardWillHide = (event) => {
        if (Platform.OS === 'ios') {
            Animated.parallel([
                Animated.timing(this.keyboardHeight, {
                    duration: event.duration,
                    toValue: 0,
                }),
            ]).start();
        }
        Animated.parallel([
            Animated.timing(this.viewHeight, {
                duration: event.duration,
                toValue: height - 150,
            }),
        ]).start();
        this.setState({ keyboardShow: false })
    };

    render() {
        var { language } = this.props.language;
        var { itemRefund } = this.props;
        var { BankName, BankBranch, BankUser, BankNumber, BankContent } = this.state;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                style={[this.state.keyboardShow ? { justifyContent: 'flex-end', marginBottom: 0 } : { marginBottom: 0 }]}
            >
                <Animated.View style={[styles.modalContent, { marginBottom: this.keyboardHeight, height: this.viewHeight }]}>
                    {
                        this.props.ticket.loadingCreateRefundInfo &&
                        <Loading />
                    }
                    <Touchable style={styles.btnClose} onPress={() => this.props.closeModal()} >
                        <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                    </Touchable>
                    <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={Platform.OS === 'android' ? -500 : -300} style={{ flex: 1 }} >
                        <ScrollView contentContainerStyle={styles.content} bounces={false} keyboardShouldPersistTaps='handled' ref={(ref) => this.scrollView = ref}>
                            <Text style={styles.txtTitle}>{convertDate2(itemRefund.ticketDetail.Payment.CreatedAt)} · Purchase ID: {itemRefund.ticketDetail.Payment.TransactionCode}</Text>
                            <TextField
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={BankName}
                                onChangeText={(BankName) => { this.setState({ BankName }) }}
                                // error={errors.Email}
                                returnKeyType='next'
                                label={convertLanguage(language, 'bank_name') + '*'}
                                baseColor={BankName.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                                containerStyle={styles.containerStyle}
                                labelHeight={25}
                                labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                                lineWidth={2}
                                selectionColor={'#3a3a3a'}
                                style={styles.input}
                                errorImage={require('../../assets/error.png')}
                            />
                            <TextField
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={BankBranch}
                                onChangeText={(BankBranch) => { this.setState({ BankBranch }) }}
                                // error={errors.Email}
                                returnKeyType='next'
                                label={convertLanguage(language, 'bank_branch') + '*'}
                                baseColor={BankBranch.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                                containerStyle={styles.containerStyle}
                                labelHeight={25}
                                labelTextStyle={{ paddingBottom: 15 }}
                                lineWidth={2}
                                selectionColor={'#3a3a3a'}
                                style={styles.input}
                                labelTextStyle={{ paddingLeft: 12 }}
                                errorImage={require('../../assets/error.png')}
                            />
                            <TextField
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={BankUser}
                                onChangeText={(BankUser) => { this.setState({ BankUser }) }}
                                // error={errors.Email}
                                returnKeyType='next'
                                label={convertLanguage(language, 'account_holder_name') + '*'}
                                baseColor={BankUser.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                                containerStyle={styles.containerStyle}
                                labelHeight={25}
                                labelTextStyle={{ paddingBottom: 15 }}
                                lineWidth={2}
                                selectionColor={'#3a3a3a'}
                                style={styles.input}
                                labelTextStyle={{ paddingLeft: 12 }}
                                errorImage={require('../../assets/error.png')}
                            />
                            <TextField
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={'' + BankNumber}
                                onChangeText={(BankNumber) => { this.setState({ BankNumber }) }}
                                // error={errors.Email}
                                returnKeyType='next'
                                label={convertLanguage(language, 'acount_number') + '*'}
                                baseColor={BankNumber.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                                containerStyle={styles.containerStyle}
                                labelHeight={25}
                                labelTextStyle={{ paddingBottom: 15 }}
                                lineWidth={2}
                                selectionColor={'#3a3a3a'}
                                style={styles.input}
                                labelTextStyle={{ paddingLeft: 12 }}
                                errorImage={require('../../assets/error.png')}
                                keyboardType="numeric"
                                onFocus={() => this.scrollView.scrollToEnd()}
                            />
                            <TextField
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={BankContent}
                                onChangeText={(BankContent) => { this.setState({ BankContent }) }}
                                // error={errors.Email}
                                returnKeyType='next'
                                label={convertLanguage(language, 'bank_note')}
                                baseColor={BankContent.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD', height: 175 }]}
                                containerStyle={styles.containerStyle}
                                labelHeight={25}
                                labelTextStyle={{ paddingBottom: 15 }}
                                lineWidth={2}
                                selectionColor={'#3a3a3a'}
                                style={[styles.input, { height: 135 }]}
                                multiline={true}
                                labelTextStyle={{ paddingLeft: 12 }}
                                errorImage={require('../../assets/error.png')}
                                onFocus={() => this.scrollView.scrollToEnd()}
                            />
                            <Touchable disabled={BankName === '' || BankBranch === '' || BankUser === '' || BankNumber === ''} style={[styles.btnSubmit, { backgroundColor: BankName === '' || BankBranch === '' || BankUser === '' || BankNumber === '' ? '#E0E2E8' : '#00A9F4' }]} onPress={() => this.onCreateBankInfo()}>
                                <Text style={[styles.txtSubmit, { color: BankName === '' || BankBranch === '' || BankUser === '' || BankNumber === '' ? '#B3B8BC' : '#FFFFFF' }]}>{convertLanguage(language, 'submit')}</Text>
                            </Touchable>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </Animated.View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5,
        // minHeight: 500,
        height: height - 150
    },
    container: {

    },
    content: {
        paddingHorizontal: 20,
        paddingBottom: 20
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        alignSelf: 'flex-end',
        padding: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    txtTitle: {
        fontSize: 12,
        color: '#828282',
        marginBottom: 20,
    },
    inputContainerStyle: {
        borderWidth: 1,
        borderRadius: 4,
        paddingHorizontal: 12,
    },
    containerStyle: {
        // paddingHorizontal: 20,
        // flex: 1
        marginVertical: 10,
    },
    input: {
        color: '#333333',
    },
    btnSubmit: {
        borderRadius: 4,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtSubmit: {
        fontSize: 16
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        ticket: state.ticket
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onCreateBankInfo: (id, body, indexTicket) => {
            dispatch(actCreateBankInfo(id, body, indexTicket))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalRefundInfo);