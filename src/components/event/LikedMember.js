import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import * as Colors from '../../constants/Colors';

class MemberFollowItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalLeave: false,
            modalSetRole: false,
        };
    }

    render() {
        var data = this.props.data;
        return (
            <View style={styles.container}>
                    <Image
                        style={styles.imgThumb}
                        source={{ uri: data.Avatars.Small }} />
                <View style={styles.colInfo}>
                    <Text style={styles.txtTitle}>{data.Name}</Text>
                    <Text style={styles.txtSmall}>{data.UserId}</Text>
                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        //   flex: 1,
        flexDirection: 'row',
        marginBottom: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    rowContent: {
        flexDirection: 'row',
    },
    colInfo: {
        flex: 1,
        // paddingLeft: 20,
        alignItems: 'flex-start',
        marginTop: 5
        // justifyContent: 'center'
    },
    imgThumb: {
        resizeMode: 'cover',
        height: 70,
        width: 70,
        borderRadius: 35,
        marginRight: 15
    },
    colAction: {
        flex: 0.4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtSmall: {
        fontSize: 14,
        color: Colors.GRAY,
        color: '#757575'
    },
    imgLogoutTeam: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    btnInvite: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 5,
        backgroundColor: Colors.PRIMARY
    },
    btnInviteDisable: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 5,
        backgroundColor: Colors.GRAY
    },
    txtBtnInvite: {
        color: '#FFFFFF',
        alignSelf: 'center'
    }
});

export default MemberFollowItem;