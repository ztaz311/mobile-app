import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, ImageBackground, StyleSheet, Dimensions } from 'react-native';
import FastImage from 'react-native-fast-image';
import Touchable from '../../screens_view/Touchable';
import { convertDate3 } from '../../services/Helper';
const { width, height } = Dimensions.get('window');

class ItemEventSlide extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        var data = this.props.data.item;
        const { language } = this.props
        return (
            <Touchable onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: data.Slug }, key: data.Slug })} style={styles.boxSlide}>
                <FastImage style={styles.boxThumb} source={{ uri: data.Posters ? data.Posters.Medium : data.Poster }} >
                    {
                        data.Role === 'Guest' && !this.props.isFromMap &&
                        <Touchable style={{ padding: 10 }} onPress={() => this.props.onLikeEvent(data.Id)}>
                            <Image source={data.Like ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')} style={styles.icon_like} />
                        </Touchable>
                    }
                </FastImage>
                <Text style={styles.txtName} numberOfLines={2}>{data.Title}</Text>
                <Text style={styles.txtTime} numberOfLines={2}>{convertDate3(data.TimeStart, data.TimeFinish, language)}</Text>
                <Text style={styles.txtVenue} numberOfLines={2}>{data.VenueName}</Text>
                <Text style={styles.txtCategory} numberOfLines={2}>
                    {
                        data.HashTag &&
                        data.HashTag.map((Tag, index) => {
                            return <React.Fragment key={index}>
                                #{Tag.HashTagName + ' '}
                            </React.Fragment>
                        })
                    }
                </Text>
            </Touchable>
        );
    }
}

const styles = StyleSheet.create({
    boxSlide: {
        // marginRight: 10,
        // marginLeft: 10,
        // width: width - 30,
        padding: 3
    },
    boxThumb: {
        width: width - 36,
        height: 170,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginBottom: 10,
        borderRadius: 4
    },
    icon_like: {
        width: 28,
        height: 28,
    },
    txtName: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 10
    },
    txtTime: {
        fontSize: 16,
        color: '#4F4F4F'
    },
    txtVenue: {
        fontSize: 14,
        color: '#4F4F4F'
    },
    txtCategory: {
        fontSize: 14,
        color: '#333333',
    },
});
export default ItemEventSlide;
