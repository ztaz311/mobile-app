import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import SafeView from '../../screens_view/SafeView';
import Modal from "react-native-modal";
import ExtraDimensions from 'react-native-extra-dimensions-android';

class ModalReport extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        var team = this.props.team;
        return (
            <Modal
                isVisible={true}
                onBackdropPress={() => this.props.closeModal()}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/X_icon.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.btnReport}>
                            <Text style={styles.txtReport}>Report</Text>
                        </View>
                        {/* <View style={styles.btnDelete}>
                            <Text style={styles.txtDelete}>Delete</Text>
                        </View> */}
                    </View>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 12,
    },
    btnReport: {
        width: 220,
        height: 50,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    txtReport: {
        fontSize: 15,
        color: '#333333'
    },
    btnDelete: {
        width: 220,
        height: 50,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    txtDelete: {
        fontSize: 15,
        color: '#bdbdbd'
    },
});

export default ModalReport;