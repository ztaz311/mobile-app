import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, ScrollView, TouchableOpacity, ActivityIndicator, FlatList } from 'react-native';
import SafeView from '../../screens_view/SafeView'
import * as Colors from '../../constants/Colors'
import Touchable from '../../screens_view/Touchable'
import Toast from 'react-native-simple-toast';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
import { actLoadDataInviteSearch, actClearInviteEvent, actInviteUser } from '../../actions/event';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            page: 1,
            UserId: 0
        };
    }
    componentWillMount() {
        this.props.onResetData()
    }

    loadMore() {
        var { loadMoreSearch, event } = this.props.event;
        var { page, keyword } = this.state;

        if (loadMoreSearch) {
            this.props.onLoadDataInviteFollowers(event.Id, page + 1, keyword)
            this.setState({ page: page + 1 })
        }
    }
    _renderFooter() {
        var { loadMoreSearch, is_empty, InviteSearch } = this.props.event;
        var { keyword } = this.state;
        var { language } = this.props.language;
        if (loadMoreSearch) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty && keyword != '' && InviteSearch.length === 0) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'data_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    onSearch() {
        this.props.onResetData()
        var { event } = this.props.event
        var { keyword } = this.state

        this.props.onLoadDataInviteFollowers(event.Id, 1, keyword)
        this.setState({ page: 1 })
    }

    renderTouchable = (eventId, itemId, invited, item) => {
        var { event } = this.props.event
        var { language } = this.props.language;
        if (invited === 'Invite') {
            return <Touchable onPress={() => { this.onInviteUser(eventId, itemId, item); this.props.onLoadDataMembers(event.Slug) }} style={styles.boxAction}>
                <Text style={styles.txtAction}>{convertLanguage(language, 'invite')}</Text>
            </Touchable>
        }
        if (invited == 'Invited') {
            return <Touchable disabled={true} style={[styles.boxAction, { backgroundColor: '#e8e8e8' }]}>
                <Text style={styles.txtAction, { color: '#bdbdbd' }}>{convertLanguage(language, 'invited')}</Text>
            </Touchable>
        }
    }
    renderInvited = (id) => {
        var { language } = this.props.language;
        var { list_event_invited, loadingInvite } = this.props.event
        var index = this.findIndex(list_event_invited, id)
        if (loadingInvite && id == this.state.UserId) {
            return <ActivityIndicator size="small" />
        } else {
            if (index == -1) {
                return <Text style={styles.txtAction}>{convertLanguage(language, 'invite')}</Text>

            } else {
                return <Text style={styles.txtAction, { color: '#bdbdbd' }}>{convertLanguage(language, 'invited')}</Text>
            }
        }


    }

    findIndex = (events, id) => {
        var result = -1;
        events.forEach((event, i) => {
            if (event.Id === id) {
                result = i;
                return result;
            }
        });
        return result;
    }
    onInviteUser = (Id, UserId, item) => {
        this.setState({
            UserId
        })
        this.props.onInviteUser(Id, UserId, item)
    }
    render() {
        // let user = this.props.user;
        var { InviteSearch, event } = this.props.event
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={styles.rowSearch}>
                    <TextInput
                        style={styles.inputSearch}
                        placeholder={convertLanguage(language, 'search_name_or_id')}
                        placeholderTextColor={"#bdbdbd"}
                        value={this.state.keyword}
                        onChangeText={(keyword) => this.setState({ keyword: keyword })}
                    />
                    <TouchableOpacity disabled={this.state.keyword.length === 0} style={styles.btnSearch} onPress={
                        () => this.onSearch()}>
                        <Image source={require('../../assets/search.png')} style={styles.imgSearch} />
                    </TouchableOpacity>
                </View>
                <View style={styles.content} >
                    <FlatList
                        // style={{ flex: 1, width: '100%' }}
                        data={InviteSearch}
                        contentContainerStyle={{ padding: 20 }}
                        renderItem={({ item }) => {
                            return (<View style={styles.boxItem}>
                                <View style={styles.boxInfo}>
                                    <Image source={{ uri: item.Avatars.Medium }} style={styles.imgAvatar} />
                                    <View style={styles.boxInfoDetail}>
                                        <Text style={[styles.txtName, { paddingRight: 15 }]}>{item.Name}</Text>
                                        <Text style={styles.txtId}>{item.UserId}</Text>
                                        <Text style={styles.txtRole}>{language === 'vi' ? item.RoleTeamLang : item.RoleTeam}</Text>
                                    </View>
                                </View>
                                {this.renderTouchable(event.Id, item.Id, item.Role, item)}
                            </View>)
                        }}
                        onEndReached={() => { this.loadMore() }}
                        // ListHeaderComponent={() => this.renderHeader()}
                        // // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        // // refreshing={this.props.team.loading}
                        // // onRefresh={() => {this.refresh()}}
                        ListFooterComponent={() => this._renderFooter()}
                    />
                </View>
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    boxItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5,
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    imgAvatar: {
        width: 60,
        height: 60,
        marginRight: 10,
        borderRadius: 30
    },
    boxInfoDetail: {
        flex: 1,
    },
    txtName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    txtId: {
        fontSize: 14,
        color: '#757575',
    },
    txtRole: {
        fontSize: 14,
        color: '#757575',
    },
    boxAction: {
        width: 60,
        height: 31,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#03a9f4'
    },
    txtAction: {
        fontSize: 13,
        color: '#FFFFFF'
    },
    rowSearch: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 15,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: 'flex-end'
    },
    inputSearch: {
        flex: 1,
        paddingBottom: 10,
        borderBottomWidth: 1,
        fontSize: 15,
        borderColor: '#bcbcbc',
        color: '#333333'
    },
    imgSearch: {
        marginLeft: 20,
        marginBottom: 10,
        width: 32,
        height: 32
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
        event: state.event
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataInviteFollowers: (id, page, key) => {
            dispatch(actLoadDataInviteSearch(id, page, key))
        },
        onResetData: () => {
            dispatch(actClearInviteEvent())
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);
