import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import * as Colors from '../../constants/Colors';
import Touchable from '../../screens_view/Touchable';
import { getTag } from '../../services/Helper';
class ItemTeamColla extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalLeave: false,
            modalSetRole: false,
        };
    }

    onOpenCategory() {
        this.props.closeModal();
        var { data, Slug } = this.props;
        this.props.navigation.navigate('CollaboratePart', { data: data, Slug: Slug })
    }

    render() {
        var { data, type, Slug } = this.props;
        return (
            <Touchable style={styles.container} onPress={() => type && type === 'show' ? null : this.onOpenCategory()}>
                <View style={{ flexDirection: 'row', flex: 1 }}>
                    <View style={styles.colThumb}>
                        <Image
                            style={styles.imgThumb}
                            source={{ uri: data.Logos.Small }}>
                        </Image>
                    </View>
                    <View style={styles.colInfo}>
                        <Text style={styles.txtTitle} numberOfLines={2}>{data.Name}</Text>
                        <Text style={styles.txtSmall} numberOfLines={2}>({data.Code})</Text>
                        <Text numberOfLines={2} style={styles.txtTag}>{getTag(data.HashTag)}</Text>
                    </View>
                </View>
                <Image source={require('../../assets/right.png')} style={styles.ic_radio} />
            </Touchable>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        //   flex: 1,
        flexDirection: 'row',
        marginBottom: 16,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    colThumb: {

    },
    rowContent: {
        flexDirection: 'row',
    },
    colInfo: {
        flex: 1,
        paddingLeft: 16,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    imgThumb: {
        resizeMode: 'cover',
        height: 70,
        width: 70,
        borderRadius: 35
    },
    colAction: {
        flex: 0.4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtSmall: {
        fontSize: 14,
        color: '#333333'
    },
    imgLogoutTeam: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    btnInvite: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 5,
        backgroundColor: Colors.PRIMARY
    },
    btnInviteDisable: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 5,
        backgroundColor: Colors.GRAY
    },
    txtBtnInvite: {
        color: '#FFFFFF',
        alignSelf: 'center'
    },
    txtTag: {
        fontSize: 14,
        color: '#333333',
    },
    ic_radio: {
        width: 24,
        height: 24
    },
});

export default ItemTeamColla;