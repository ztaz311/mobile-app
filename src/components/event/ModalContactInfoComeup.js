import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Platform, Dimensions, Linking } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";
import SafeView from '../../screens_view/SafeView';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import Touchable from '../../screens_view/Touchable';
class ModalContactInfoComeup extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        var team = this.props.team;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.boxContent}>
                            <Image source={require('../../assets/contact_user.png')} style={styles.contact_user} />
                            <View style={styles.boxInfo}>
                                <Text style={styles.txtInfo}>If you want to sell physical  tickets you have to contact our supports to approve this function, Contact us via:</Text>
                                <Text style={styles.txtInfo}><Text style={{ color: '#8c8c8c' }}>Email:</Text>contact@starindex.com</Text>
                                <Text style={styles.txtInfo}><Text style={{ color: '#8c8c8c' }}>Tel:</Text>08 2779 2656</Text>
                                <View style={styles.boxSend}>
                                    <Touchable style={styles.btnPhone} onPress={() => Linking.openURL('tel:0827792656')}>
                                        <Image source={require('../../assets/contact_tel.png')} style={styles.contact_tel} />
                                    </Touchable>
                                    <Touchable style={styles.btnPhone} onPress={() => Linking.openURL('mailto:contact@starindex.com')}>
                                        <Image source={require('../../assets/contact_mail.png')} style={styles.contact_tel} />
                                    </Touchable>
                                </View>
                            </View>
                        </View>

                    </View>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 10
    },
    content: {
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 12,
    },
    iconClose: {
        width: 30,
        height: 30
    },
    boxContent: {
        flexDirection: 'row',
        paddingHorizontal: 24,
        paddingBottom: 24
    },
    contact_user: {
        width: 64,
        height: 64,
        marginRight: 8
    },
    boxInfo: {
        flex: 1
    },
    txtInfo: {
        fontSize: 12,
        color: '#333333',
        marginBottom: 5
    },
    boxSend: {
        flexDirection: 'row',
        marginTop: 10
    },
    btnPhone: {
        width: 48,
        height: 48,
        marginRight: 8
    },
    contact_tel: {
        width: 48,
        height: 48,
    },

});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalContactInfoComeup);