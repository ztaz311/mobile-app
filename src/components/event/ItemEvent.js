import React, { Component } from 'react';
import { Text, Image, ImageBackground, StyleSheet, Dimensions, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Touchable from '../../screens_view/Touchable';
const { width, height } = Dimensions.get('window');
import { convertDate4 } from '../../services/Helper';
class ItemEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        var { data, language } = this.props;
        return (
            <Touchable onPress={() => this.props.navigation.push('EventDetail', { Slug: data.Slug })} style={styles.boxSlide}>
                <View style={styles.boxImageThumb}>
                    <FastImage style={styles.boxThumb} source={{ uri: data.Posters ? data.Posters.Small : data.Poster }} >
                        {
                            data.Role === 'Guest' &&
                            <Touchable style={{ padding: 10 }} onPress={() => this.props.onLikeEvent(data.Id)}>
                                <Image source={data.Like ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')} style={styles.icon_like} />
                            </Touchable>
                        }
                    </FastImage>
                    {
                        data.Status == 0
                        &&
                        <>
                            <View style={styles.imgCancel} >
                                <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#ff4081', height: 60, width: '115%', transform: [{ rotateZ: '-0.3rad' }] }}>
                                    <Text style={{ textAlign: 'center', color: '#FFFF', fontSize: 30, fontWeight: 'bold' }}>{this.props.cancel_text ? this.props.cancel_text : 'Canceled Event'}</Text>
                                </View>
                            </View>
                            {
                                data.Role === 'Guest' &&
                                <View style={{ position: 'absolute', width: '100%', height: '100%', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                    <Touchable style={{ padding: 10 }} onPress={() => this.props.onLikeEvent(data.Id)}>
                                        <Image source={data.Like ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')} style={[styles.icon_like, { paddingBottom: 10, paddingRight: 10 }]} />
                                    </Touchable>
                                </View>
                            }
                        </>
                    }
                </View>
                <Text style={styles.txtName} numberOfLines={2}>{data.Title}</Text>
                <Text style={styles.txtTime}>{convertDate4(data.TimeStart, data.TimeFinish, language)}</Text>
                <Text style={styles.txtVenue}>{data.VenueName}, {data.Address}</Text>
                <Text style={styles.txtCategory}>
                    {
                        data.HashTag &&
                        data.HashTag.map((Tag, index) => {
                            return <React.Fragment key={index}>
                                #{Tag.HashTagName + ' '}
                            </React.Fragment>
                        })
                    }
                </Text>
            </Touchable>
        );
    }
}

const styles = StyleSheet.create({
    boxSlide: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 16
    },
    boxImageThumb: {
        marginBottom: 10,
    },
    boxThumb: {
        width: width - 32,
        aspectRatio: 16 / 9,
        resizeMode: 'cover',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        backgroundColor: '#bdbdbd',
        borderRadius: 4
    },
    imgCancel: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        resizeMode: 'cover',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    icon_like: {
        width: 28,
        height: 28,
    },
    txtName: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 10
    },
    txtTime: {
        fontSize: 16,
        color: '#4F4F4F'
    },
    txtVenue: {
        fontSize: 14,
        color: '#4F4F4F'
    },
    txtCategory: {
        fontSize: 14,
        color: '#333333',
    },
});
export default React.memo(ItemEvent);
