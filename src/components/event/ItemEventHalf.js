import React, { Component } from 'react';
import { Text, Image, ImageBackground, StyleSheet, Dimensions, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Touchable from '../../screens_view/Touchable';
const { width, height } = Dimensions.get('window');
import { convertDate4 } from '../../services/Helper';
class ItemEventHalf extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        var { data, language } = this.props;
        return (
            <Touchable onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: data.Slug }, key: data.Slug })} style={styles.boxSlide}>
                <View style={styles.boxImageThumb}>
                    <FastImage style={[styles.boxThumb, { borderRadius: 2 }]} source={{ uri: data.Posters ? data.Posters.Small : data.Poster }} >
                        {
                            data.Role === 'Guest' &&
                            <Touchable style={{ padding: 5 }} onPress={() => this.props.onLikeEvent(data.Id)}>
                                <Image source={data.Like ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')} style={styles.icon_like} />
                            </Touchable>
                        }
                    </FastImage>
                    {
                        data.Status == 0
                        &&
                        <>
                            <View style={styles.imgCancel} >
                                <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#ff4081', height: 60 / 2, width: '115%', transform: [{ rotateZ: '-0.3rad' }] }}>
                                    <Text style={{ textAlign: 'center', color: '#FFFF', fontSize: 30 / 2, fontWeight: 'bold' }}>{this.props.cancel_text ? this.props.cancel_text : 'Canceled Event'}</Text>
                                </View>
                            </View>
                            {
                                data.Role === 'Guest' &&
                                <View style={{ position: 'absolute', width: '100%', height: '100%', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                    <Touchable style={{ padding: 5 }} onPress={() => this.props.onLikeEvent(data.Id)}>
                                        <Image source={data.Like ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')} style={[styles.icon_like, { paddingBottom: 5, paddingRight: 5 }]} />
                                    </Touchable>
                                </View>
                            }
                        </>
                    }
                </View>
                <Text style={styles.txtName} numberOfLines={2}>{data.Title}</Text>
                <Text style={styles.txtTime} numberOfLines={this.props.numberOfLines || 2}>{convertDate4(data.TimeStart, data.TimeFinish, language)}</Text>
                <Text style={styles.txtTime} numberOfLines={this.props.numberOfLines || 2}>{data.VenueName}, {data.Address}</Text>
                <Text style={styles.txtCategory} numberOfLines={this.props.numberOfLines || 2}>
                    {
                        data.HashTag &&
                        data.HashTag.map((Tag, index) => {
                            return <React.Fragment key={index}>
                                #{Tag.HashTagName + ' '}
                            </React.Fragment>
                        })
                    }
                </Text>
            </Touchable>
        );
    }
}

const styles = StyleSheet.create({
    boxSlide: {
        // flex: 1,
        width: '48%',
        paddingBottom: 20
    },
    boxImageThumb: {
        marginBottom: 25 / 2,
    },
    boxThumb: {
        width: '100%',
        aspectRatio: 16 / 9,
        resizeMode: 'cover',
        // height: 170,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        backgroundColor: '#bdbdbd',
        borderRadius: 2
    },
    imgCancel: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        resizeMode: 'cover',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    icon_like: {
        width: 24,
        height: 24,
    },
    txtName: {
        fontSize: 18,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtTime: {
        fontSize: 11,
        color: '#4F4F4F',
        lineHeight: 14
    },
    txtCategory: {
        fontSize: 14,
        color: '#333333',
        // marginTop: 24 / 2
    },
});
export default React.memo(ItemEventHalf);
