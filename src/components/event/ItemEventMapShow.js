import React, { Component } from 'react';
import { Text, Image, ImageBackground, StyleSheet, Dimensions, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Touchable from '../../screens_view/Touchable'
const { width, height } = Dimensions.get('window')
import { convertDate3 } from '../../services/Helper'
class ItemEventMapShow extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    _renderAddress = (VenueName, Address) => {
        let text = VenueName + ', ' + Address
        if (text.length > 36) {
            return text.substring(0, 36) + '...';
        } else {
            return text
        }
    }
    render() {
        var { data, language } = this.props;

        return (
            <Touchable onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: data.Slug }, key: data.Slug })} style={styles.boxSlide}>
                <FastImage style={styles.boxThumb} source={{ uri: data.Posters ? data.Posters.Medium : data.Poster }} />
                <View style={styles.boxContent}>
                    <Text style={styles.txtName} numberOfLines={2}>{data.Title}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10 }}>
                            <Image style={{ width: 16, height: 16 }} source={require('../../assets/item_vector_clock.png')} />
                        </View>
                        <Text style={styles.txtTime}>
                            {convertDate3(data.TimeStart, data.TimeFinish, language)}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10 }}>
                            <Image style={{ width: 16, height: 16 }} source={require('../../assets/item_vector_location.png')} />
                        </View>
                        <Text style={styles.txtVenue}>
                            {this._renderAddress(data.VenueName, data.Address)}
                        </Text>
                    </View>
                </View>
            </Touchable>
        );
    }
}

const styles = StyleSheet.create({
    boxSlide: {
        flex: 1,
        paddingBottom: 15,
        flexDirection: 'row'
    },
    boxThumb: {
        width: 100,
        height: 100,
        borderRadius: 5,
        backgroundColor: '#bdbdbd',
        marginRight: 10
    },
    boxContent: {
        flex: 1
    },
    txtName: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 10
    },
    txtTime: {
        fontSize: 16,
        color: '#4F4F4F'
    },
    txtVenue: {
        fontSize: 14,
        color: '#4F4F4F'
    },
});
export default ItemEventMapShow;
