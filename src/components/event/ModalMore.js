import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Dimensions, Platform } from 'react-native';
import Touchable from '../../screens_view/Touchable'
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import moment from "moment";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalMore extends Component {
    constructor(props) {
        super(props);
    }

    showTextDelete() {
        var language = this.props.language;
        var datetimenow = moment().format('YYYY-MM-DD HH:mm')
        if (moment(this.props.TimeFinish).isSameOrAfter(datetimenow)) {
            if (this.props.Status == 0) {
                return convertLanguage(language, 'delete')
            } else {
                return convertLanguage(language, 'cancel')
            }
        } else {
            return convertLanguage(language, 'delete')
        }
    }

    render() {
        var datetimenow = moment().format('YYYY-MM-DD HH:mm')
        var language = this.props.language;
        return (
            <Modal
                isVisible={true}
                onBackdropPress={() => this.props.closeModal()}
                animationIn="slideInRight"
                animationOut="slideOutRight"
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                style={styles.rightModal}>
                <View style={styles.modalContent}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <Text style={styles.txtCode}>Event Code : {this.props.Code}</Text>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'please_check_the_event_code_on_tickets')}</Text>
                        <Touchable style={styles.boxList} onPress={() => this.props.Role != 'Guest' ? this.props.holderList() : null}>
                            <Text style={this.props.Role  != 'Guest' ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'holder_list')}</Text>
                        </Touchable>
                        <Touchable style={styles.boxList} onPress={() => this.props.Role != 'Guest' ? this.props.scanTicket() : null}>
                            <Text style={this.props.Role != 'Guest' ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'ticket_scanning')}</Text>
                        </Touchable>
                        <Touchable style={styles.boxList} onPress={() => this.props.Role === 'Leader' || this.props.Role === 'Director' ? this.props.eventSaleReport() : null}>
                            <Text style={this.props.Role === 'Leader' || this.props.Role === 'Director' ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'ticket_sales')}/{convertLanguage(language, 'withdraw')}</Text>
                        </Touchable>
                        <Touchable style={styles.boxList} onPress={() => this.props.Role === 'Leader' || this.props.Role === 'Director' || this.props.Role === 'Manager' ? this.props.refundList() : null}>
                            <Text style={this.props.Role === 'Leader' || this.props.Role === 'Director' || this.props.Role === 'Manager' ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'refund_request')}</Text>
                            {/* <View style={styles.circle}></View> */}
                        </Touchable>
                        {/* <Touchable style={styles.boxList} onPress={() => this.props.Role === 'Leader' || this.props.Role === 'Director' ? this.props.withdrawRequest() : null}>
                            <Text style={this.props.Role === 'Leader' || this.props.Role === 'Director' ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'withdraw')}</Text>
                        </Touchable> */}
                        <Touchable style={styles.boxList} onPress={() => this.props.Role === 'Leader' || this.props.Role === 'Director' ? this.props.eventStaff() : null}>
                            <Text style={this.props.Role === 'Leader' || this.props.Role === 'Director' ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'staff')}</Text>
                        </Touchable>
                        <Touchable style={styles.boxList} onPress={() => this.props.Role === 'Leader' || this.props.Role === 'Director' ? this.props.collaborating() : null}>
                            <Text style={this.props.Role === 'Leader' || this.props.Role === 'Director' ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'collaborating_team')}</Text>
                            {/* <View style={styles.circle}></View> */}
                        </Touchable>
                        <Touchable style={styles.boxList} onPress={() => (this.props.Role === 'Leader' || this.props.Role === 'Director') ? this.props.edit() : null}>
                            <Text style={(this.props.Role === 'Leader' || this.props.Role === 'Director') ? styles.txtTicketBold : styles.txtTicket}>{convertLanguage(language, 'edit')}</Text>
                        </Touchable>
                        <Touchable style={styles.boxList} onPress={() => this.props.Role === 'Leader' ? this.props.delete() : null}>
                            <Text style={this.props.Role === 'Leader' ? styles.txtTicketBold : styles.txtTicket}>{this.showTextDelete()}</Text>
                        </Touchable>
                        <Touchable style={styles.boxList} onPress={() => this.props.showModelReport()}>
                            <Text style={styles.txtTicketBold}>{convertLanguage(language, 'contact')}</Text>
                        </Touchable>
                    </ScrollView>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        height: height,
        width: width - 100,
        padding: 25,
        paddingTop: 40
    },
    txtCode: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15
    },
    txtTitle: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15
    },
    boxList: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 2,
        borderBottomColor: '#e9e9e9',
        paddingTop: 20,
        paddingBottom: 20
    },
    txtTicket: {
        fontSize: 15,
        color: '#e8e8e8',
        fontWeight: 'bold',
        flex: 1
    },
    txtTicketBold: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        flex: 1
    },
    circle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#ff4081',
        margin: 10
    },
    rightModal: {
        alignItems: "flex-end",
        flex: 1,
        marginRight: 0
    }
});

export default ModalMore;