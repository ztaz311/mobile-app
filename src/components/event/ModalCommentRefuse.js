import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, TextInput, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalCommentRefuse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Comment: '',
        }
    }


    render() {
        var { language } = this.props.language;
        var { Comment } = this.state;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <TextInput
                            style={styles.ipContent}
                            selectionColor="#bdbdbd"
                            name="Comment"
                            placeholder={'Refuse reason'}
                            multiline={true}
                            value={Comment}
                            onChangeText={(Comment) => this.setState({Comment})}
                        />
                        <TouchableOpacity onPress={() => this.props.onRefuse(Comment)} style={[styles.btnRefund, Comment == '' ? {backgroundColor: '#e8e8e8'} : {}]} disabled={Comment == ''}>
                            <Text style={[styles.txtRefund, Comment == '' ? {color: '#bdbdbd'} : {}]}>{convertLanguage(language, 'refuse')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    content: {
        padding: 15,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10
    },
    ipContent: {
        height: 180,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        fontSize: 13,
        color: '#333333',
        textAlignVertical: 'top',
        padding: 10,
    },
    btnRefund: {
        height: 48,
        borderRadius: 2,
        backgroundColor: '#03a9f4',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    txtRefund: {
        fontSize: 14,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalCommentRefuse);