import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Modal, StatusBar } from 'react-native';
import SafeView from '../../screens_view/SafeView'
import Swiper from 'react-native-swiper';

class ModalZoomImage extends Component {
    constructor(props) {
        super(props);
    }

    renderImages() {
        return this.props.dataZoom.map((image, index) => {
            return <Image
                style={styles.imgZoom}
                source={{ uri: image.Photos.Large }}
                key={index}>
            </Image>
        });
    }
    render() {
        var event = this.props.event;
        return (
            <Modal
                visible={true}
            >
                <SafeView style={styles.container}>
                    <StatusBar barStyle="light-content" />
                    <View style={styles.rowTitle}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Image
                                style={styles.teamThumb}
                                source={{ uri: event.Posters.Small }}>
                            </Image>
                            <View style={styles.colTeamInfo}>
                                <Text style={styles.txtTeamName}>{event.Title}</Text>
                                {/* <Text style={styles.txtTeamTime}>{this.props.dataZoom.Images[this.props.index].TimeUpload}</Text> */}
                            </View>
                        </View>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/x_icon_white.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <Swiper loop={false} showsButtons={false} index={this.props.index} showsPagination={false}>
                            {this.renderImages()}
                        </Swiper>
                    </View>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: "#000000",
        flex: 1,
    },
    content: {
        flex: 1,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    rowTitle: {
        flexDirection: 'row',
        padding: 20,
        paddingTop: 30,
        justifyContent: 'space-between',
    },
    imgZoom: {
        resizeMode: 'contain',
        flex: 1,
    },
    teamThumb: {
        width: 50,
        height: 50,
        resizeMode: 'cover',
        borderRadius: 25,
        marginRight: 20,
    },
    colTeamInfo: {
    },
    txtTeamName: {
        color: '#FFFFFF',
        fontSize: 18
    },
    txtTeamTime: {
        color: '#FFFFFF'
    },
    iconClose: {
        width: 25,
        height: 25,
        padding: 10
    }
});

export default ModalZoomImage;