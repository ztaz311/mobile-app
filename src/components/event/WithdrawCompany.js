import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import SafeView from '../../screens_view/SafeView'
import * as Colors from '../../constants/Colors'
import Touchable from '../../screens_view/Touchable'
import { connect } from "react-redux";
import Popover from 'react-native-popover-view';
import { convertLanguage, formatNumber } from '../../services/Helper'
class WithdrawCompany extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false
        }
    }

    onChange(key, value) {
        this.props.onChange(key, value, 'company');
        // this.setState({[key]: value})
    }

    onWithdraw() {
        var body = this.props.company;
        body.WithdrawType = 'company';
        this.props.onWithdraw(body);
    }

    validate() {
        var { Name, CEO, RegistrationNo, Phone, Zipcode, Email, Address, Bank, AccountNo, AccountHolder } = this.props.company;
        return Name == '' || CEO == '' || RegistrationNo == '' || Phone == '' || Zipcode == '' || Email == '' || Address == '' || Bank == '' || AccountNo == '' || AccountHolder == '';
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    render() {
        var { Name, CEO, RegistrationNo, Phone, Zipcode, Email, Address, Bank, AccountNo, AccountHolder } = this.props.company;
        var { loadingWithdraw } = this.props;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <ScrollView style={styles.content} keyboardShouldPersistTaps='handled' >
                    <View style={{ paddingTop: 20, paddingBottom: 20 }}>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'company_name')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="Name"
                                value={Name}
                                onChangeText={(Name) => this.onChange('Name', Name)}
                            />
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'ceo')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="CEO"
                                value={CEO}
                                onChangeText={(CEO) => this.onChange('CEO', CEO)}
                            />
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'registration_no')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="RegistrationNo"
                                value={RegistrationNo}
                                keyboardType="numeric"
                                onChangeText={(RegistrationNo) => this.onChange('RegistrationNo', RegistrationNo)}
                            />
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'phone_number')}</Text>
                            <View style={styles.boxPhone}>
                                <TouchableOpacity onPress={() => this.showPopover()} style={styles.boxPicker} ref={ref => this.touchable = ref}>
                                    <Text style={styles.txtPicker}>{Zipcode}</Text>
                                    <Image source={require('../../assets/down_arrow_active.png')} style={styles.ic_dropdown} />
                                </TouchableOpacity>
                                <TextInput
                                    style={[styles.ipContent, { flex: 1 }]}
                                    selectionColor="#bcbcbc"
                                    name="Phone"
                                    value={Phone}
                                    keyboardType="numeric"
                                    onChangeText={(Phone) => this.onChange('Phone', Phone == 0 ? '' : Phone)}
                                />
                            </View>
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'email')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="Email"
                                value={Email}
                                onChangeText={(Email) => this.onChange('Email', Email)}
                            />
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'address')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="Address"
                                value={Address}
                                onChangeText={(Address) => this.onChange('Address', Address)}
                            />
                        </View>
                        <View style={styles.boxBankAccount}>
                            <Text style={styles.txtBankAccount}>{convertLanguage(language, 'bank_account_info')}</Text>
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'bank')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="Bank"
                                value={Bank}
                                onChangeText={(Bank) => this.onChange('Bank', Bank)}
                            />
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'account_no')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="AccountNo"
                                value={AccountNo}
                                keyboardType="numeric"
                                onChangeText={(AccountNo) => this.onChange('AccountNo', AccountNo)}
                            />
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'account_holder')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                name="Account Holder"
                                value={AccountHolder}
                                onChangeText={(AccountHolder) => this.onChange('AccountHolder', AccountHolder)}
                            />
                        </View>
                        <Touchable style={[styles.btnWithdraw, { backgroundColor: this.validate() ? '#e8e8e8' : '#03a9f4' }]} onPress={() => this.onWithdraw()} disabled={loadingWithdraw || this.validate()}>
                            <Text style={[styles.txtWithdraw, { color: this.validate() ? '#bdbdbd' : '#FFFFFF' }]}>{convertLanguage(language, 'withdrawal_request')}</Text>
                        </Touchable>
                    </View>
                </ScrollView>
                <Popover
                    isVisible={this.state.isVisible}
                    animationConfig={{ duration: 0 }}
                    fromView={this.touchable}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => this.closePopover()}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => { this.closePopover(); this.onChange('Zipcode', '+82') }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_kr.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+82</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.onChange('Zipcode', '+84') }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_vn.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+84</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.onChange('Zipcode', '+66') }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_th.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+66</Text>
                        </Touchable>
                    </View>
                </Popover>
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        paddingLeft: 20,
        paddingRight: 20
    },
    boxInput: {
        paddingBottom: 24,
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 5,
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 5,
        paddingTop: 5,
        paddingLeft: 0,
        fontWeight: 'bold'
    },
    boxPicker: {
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        borderRadius: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 100,
        height: 30,
        marginRight: 20
    },
    txtPicker: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5,
        marginLeft: 5
    },
    boxPhone: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    boxBankAccount: {
        paddingTop: 20, paddingBottom: 20,
        paddingLeft: 40, paddingRight: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtBankAccount: {
        fontSize: 14,
        color: '#333333',
        textAlign: 'center'
    },
    btnWithdraw: {
        width: 270,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e8e8e8',
        marginTop: 20,
        alignSelf: 'center',
        borderRadius: 1
    },
    txtWithdraw: {
        fontSize: 17,
        color: '#bdbdbd',
        fontWeight: 'bold'
    },
    boxPopover: {
        width: 100,
        padding: 12,
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 12
    },
    txtCountry: {
        color: '#333333',
        fontSize: 18
    },
});
const mapStateToProps = state => {
    return {
        // team: state.team,
        // user: state.user
        language: state.language,
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WithdrawCompany);
