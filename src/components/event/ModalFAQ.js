import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, ActivityIndicator, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../../screens_view/ScrView';
// import HTMLView from 'react-native-htmlview';
import AutoHeightWebView from 'react-native-autoheight-webview';
import { convertLanguage } from '../../services/Helper';
import SafeView from '../../screens_view/SafeView';
import { connect } from "react-redux";
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalFAQ extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    showDataFAQ(item) {
        var { language } = this.props.language
        switch (item) {
            case 'comeup-ticket':
                return convertLanguage(language, 'comeup_ticket');
            case 'printed-comeup-e-ticket':
                return convertLanguage(language, 'printed_comeup_e-ticket');
            case 'ticket-holder-name-&-phone-number':
                return convertLanguage(language, 'ticket_holder_name_&_phone_number');
            case 'delivered-ticket-from-this-event-host':
                return convertLanguage(language, 'delivered_ticket_from_this_event_host');
            case 'no-tickyet-check-in':
                return convertLanguage(language, 'no_ticket_check-in');
            case 'id-card-check':
                return convertLanguage(language, 'id_card_check');
            case 'no-minor':
                return convertLanguage(language, 'no_minor');
            case 'no-carrying-food-and-beverage':
                return convertLanguage(language, 'no_carrying_food_and_beverage');
            case 'no-pet':
                return convertLanguage(language, 'no_pet');
            case 'custom-restriction':
                return convertLanguage(language, 'custom_restriction');
            case 'no-car-park':
                return convertLanguage(language, 'no_car_park');
            case 'free':
                return convertLanguage(language, 'free');
            case 'paid':
                return convertLanguage(language, 'paid');
            case 'comeup-refund-policy':
                return convertLanguage(language, 'comeup_refund_policy');
            case 'full-refund-before-event-start':
                return convertLanguage(language, 'full_refund_before_event_start');
            case 'no-refund':
                return convertLanguage(language, 'no_refund');
            case 'host-refund-policy':
                return convertLanguage(language, 'host_refund_policy');
            default:
                return '';
        }
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }


    render() {
        var { Detail, FAQ } = this.props;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                style={{ margin: 0 }}
            >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/X_icon.png')} />
                        </TouchableOpacity>
                    </View>
                    <SrcView style={styles.container}>
                        <View style={styles.content}>
                            <View style={{ overflow: 'hidden' }}>
                                <AutoHeightWebView startInLoadingState={true} renderLoading={this.renderLoading} scalesPageToFit={false} zoomable={false} scrollEnabled={false} source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40) * 9 / 16}px} img {width: ${width - 40}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }} originWhitelist={['*']} />
                            </View>
                            {
                                (FAQ && ((FAQ['A'] && FAQ['A'].length > 0) || (FAQ['B'] && FAQ['B'].length > 0) || (FAQ['C'] && FAQ['C'].length > 0) || (FAQ['D'] && FAQ['D'].length > 0))) &&
                                <React.Fragment>
                                    <Text style={styles.txtFAQ}>FAQ</Text>
                                    {
                                        FAQ['A'] && FAQ['A'].length > 0
                                        &&
                                        <React.Fragment>
                                            <Text style={styles.txtLable}>A. Ticket check-in at the event</Text>
                                            {
                                                FAQ['A'].map((item, index) => {
                                                    return <Text style={styles.value} key={index}>
                                                        {
                                                            '- ' + this.showDataFAQ(item)
                                                        }
                                                    </Text>
                                                })
                                            }
                                        </React.Fragment>
                                    }
                                    {
                                        FAQ['B'] && FAQ['B'].length > 0
                                        &&
                                        <React.Fragment>
                                            <Text style={styles.txtLable}>B. Restrictions</Text>
                                            {
                                                FAQ['B'].map((item, index) => {
                                                    return <Text style={styles.value} key={index}>
                                                        {
                                                            '- ' + this.showDataFAQ(item)
                                                        }
                                                    </Text>
                                                })
                                            }
                                        </React.Fragment>
                                    }
                                    {
                                        FAQ['C'] && FAQ['C'].length > 0
                                        &&
                                        <React.Fragment>
                                            <Text style={styles.txtLable}>C. Parking</Text>
                                            {
                                                FAQ['C'].map((item, index) => {
                                                    return <Text style={styles.value} key={index}>
                                                        {
                                                            '- ' + this.showDataFAQ(item)
                                                        }
                                                    </Text>
                                                })
                                            }
                                        </React.Fragment>
                                    }
                                    {
                                        FAQ['D'] && FAQ['D'].length > 0
                                        &&
                                        <React.Fragment>
                                            <Text style={styles.txtLable}>D. Refund policy</Text>
                                            {
                                                FAQ['D'].map((item, index) => {
                                                    return <Text style={styles.value} key={index}>
                                                        {
                                                            '- ' + this.showDataFAQ(item)
                                                        }
                                                    </Text>
                                                })
                                            }
                                        </React.Fragment>
                                    }
                                </React.Fragment>
                            }
                        </View>
                    </SrcView>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        height: height,
        width: width
    },
    container: {

    },
    content: {
        padding: 20,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 8,
        borderColor: '#cccccc',
        borderWidth: 1,
        borderRadius: 24,
        margin: 20,
        marginRight: 10,
        marginBottom: 10
    },
    txtFAQ: {
        paddingTop: 30,
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15,
    },
    txtLable: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 10,
    },
    value: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 7,
        paddingLeft: 15,
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalFAQ);