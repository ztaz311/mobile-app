import React, { Component } from 'react';
import { Picker, View, Text, Image, Alert, StyleSheet, TextInput } from 'react-native';
import PickerItem from '../../screens_view/PickerItem'
import { datetimeNow } from '../../services/Helper';
import DatePicker from 'react-native-datepicker';
import * as Colors from '../../constants/Colors';
import Touchable from '../../screens_view/Touchable';
import { formatNumber, convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";
import moment from "moment";
class FormTicket extends Component {
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            listTypes: this.props.ticket.Unit === 'VND' ? [
                {
                    name: convertLanguage(language, 'e-ticket'),
                    value: 'e-ticket'
                },
                {
                    name: convertLanguage(language, 'delivery_printed_ticket'),
                    value: 'd-p-ticket'
                },
            ]
                :
                [
                    {
                        name: convertLanguage(language, 'e-ticket'),
                        value: 'e-ticket'
                    },
                ],
            TicketId: this.props.ticket.TicketId,
            NameTicket: this.props.ticket.NameTicket,
            Unit: this.props.ticket.Unit,
            TimeStartTicket: this.props.ticket.TimeStartTicket,
            TimeEndTicket: this.props.ticket.TimeEndTicket,
            Price: this.props.ticket.Price,
            Quantity: this.props.ticket.Quantity,
            LimitQuantity: this.props.ticket.LimitQuantity,
            Description: this.props.ticket.Description,
            Option: this.props.ticket.Option,
            Status: this.props.ticket.Status,
            Type: this.props.ticket.Type,
            Question2: this.props.ticket.Question2,
            TotalTicketDetail: this.props.ticket.TotalTicketDetail,
            show_more: false,
        };
    }

    onChange(key, value) {
        var { index } = this.props;
        if (key === 'Type') {
            this.setState({ show_more: true })
        }
        this.props.onChange(index, key, value)
    }

    onDelete() {
        var { language } = this.props.language;
        var { index } = this.props;
        Alert.alert(
            convertLanguage(language, 'delete_ticket'),
            convertLanguage(language, 'are_you_sure_to_delete_ticket'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => this.props.onDelete(index) },
            ],
            { cancelable: false },
        );

    }

    showType(Type) {
        var { listTypes } = this.state;
        var datas = listTypes.filter((data) => data.value === Type);
        return datas[0].name;
    }

    showDataQuantity(data) {
        var result = [];
        for (var i = 0; i <= data.length; i++) {
            result.push({ name: data[i].name, value: data[i].value })
        }
        return result;
    }

    displaySelected() {
        var { listTypes } = this.state;
        var { Type } = this.props.ticket;
        var selectedOption = this.state.selectedOption;
        var item = new Array();
        item = listTypes.filter(function (option) {
            return option.value == Type;
        })
        return item.length > 0 ? item[0].name : '';
    }

    showErrorPrice() {
        var { Price, Unit } = this.props.ticket;
        var { language } = this.props.language;
        if(Price === '' || Price == 0) {
            return null;
        } else {
            if(Unit.toUpperCase() === 'VND') {
                if(Price < 10000) {
                    return <Text style={[styles.txtInfoCod, {color: '#EB5757'}]}>{convertLanguage(language, 'validate_ticket_price_min', {zero: '0', min: '10,000'})}</Text>
                } else if(Price > 10000000000) {
                    return <Text style={[styles.txtInfoCod, {color: '#EB5757'}]}>{convertLanguage(language, 'validate_ticket_price_max', {max: '10,000,000,000'})}</Text>
                } else {
                    return null;
                }
            } else {
                if(Price < 1000) {
                    return <Text style={[styles.txtInfoCod, {color: '#EB5757'}]}>{convertLanguage(language, 'validate_ticket_price_min', {zero: '0', min: '1,000'})}</Text>
                } else if(Price > 1000000000) {
                    return <Text style={[styles.txtInfoCod, {color: '#EB5757'}]}>{convertLanguage(language, 'validate_ticket_price_max', {max: '1,000,000,000'})}</Text>
                } else {
                    return null;
                }
            }
        }
    }
    
    showErrorByPrice() {
        var { Price, Unit } = this.props.ticket;
        var { language } = this.props.language;
        if(Price === '' || Price == 0) {
            return null;
        } else {
            if(Unit.toUpperCase() === 'VND') {
                if(Price < 10000) {
                    return <Text style={[styles.txtInfoCod, {color: 'transparent'}]}>{convertLanguage(language, 'validate_ticket_price_min', {zero: '0', min: '10,000'})}</Text>
                } else if(Price > 10000000000) {
                    return <Text style={[styles.txtInfoCod, {color: 'transparent'}]}>{convertLanguage(language, 'validate_ticket_price_max', {max: '10,000,000,000'})}</Text>
                } else {
                    return null;
                }
            } else {
                if(Price < 1000) {
                    return <Text style={[styles.txtInfoCod, {color: 'transparent'}]}>{convertLanguage(language, 'validate_ticket_price_min', {zero: '0', min: '1,000'})}</Text>
                } else if(Price > 1000000000) {
                    return <Text style={[styles.txtInfoCod, {color: 'transparent'}]}>{convertLanguage(language, 'validate_ticket_price_max', {max: '1,000,000,000'})}</Text>
                } else {
                    return null;
                }
            }
        }
    }

    render() {
        var datetimenow = moment().format('YYYY-MM-DD HH:mm')
        var { NameTicket, Price, Unit, TimeStartTicket, TimeEndTicket, Quantity, LimitQuantity, Description, Option, Status, Question2, Type, TotalTicketDetail, StatusCOD } = this.props.ticket;
        var { show_more, listTypes } = this.state;
        var { index, ticket_count, EventId } = this.props;
        var { language } = this.props.language;
        return (
            <View style={[styles.content]}>
                <View style={{ opacity: Status == 0 ? 0.2 : 1 }}>
                    <View style={styles.boxInput}>
                        <Text style={styles.txtInput}>{convertLanguage(language, 'ticket_name')}</Text>
                        <TextInput
                            style={[styles.ipContent, TotalTicketDetail === 0 || !EventId ? {} : { backgroundColor: '#cccccc' }]}
                            selectionColor="#bcbcbc"
                            name="NameTicket"
                            value={NameTicket}
                            onChangeText={(NameTicket) => this.onChange('NameTicket', NameTicket)}
                            editable={TotalTicketDetail === 0 || !EventId}
                        // onEndEditing={(e) => this.onChange('NameTicket', e.nativeEvent.text)}
                        />
                    </View>
                    <View style={styles.boxInput}>
                        <View style={styles.boxQuantity}>
                            <View style={{ flex: 0.45, justifyContent: 'space-between' }}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'ticket_type')}</Text>
                                {
                                    EventId && TotalTicketDetail > 0 ?
                                        <View style={[styles.boxPicker, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: '#cccccc', }]}>
                                            <Text style={[{ fontSize: 15, fontWeight: 'normal', color: Colors.TEXT_P, flex: 1 }]} numberOfLines={1}>{this.displaySelected()}</Text>
                                            <Image source={require('../../assets/down_arrow_active.png')} style={{ marginLeft: 0, width: 13, height: 11 }} />
                                        </View>
                                        :
                                        <PickerItem
                                            boxDisplayStyles={styles.boxPicker}
                                            containerStyle={{ height: 35 }}
                                            options={listTypes}
                                            selectedOption={Type}
                                            isChange={false}
                                            onSubmit={(Type) => this.onChange('Type', Type)}
                                        />
                                }
                                {this.showErrorByPrice()}
                            </View>
                            <View style={{ flex: 0.1 }}></View>
                            <View style={{ flex: 0.45 }}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'price')}({Unit})</Text>
                                <View>
                                    <TextInput
                                        style={[styles.ipContent, TotalTicketDetail === 0 || !EventId ? {} : { backgroundColor: '#cccccc' }]}
                                        selectionColor="#bcbcbc"
                                        value={Price !== '' ? formatNumber(Price) : ''}
                                        onChangeText={(Price) => this.onChange('Price', Price)}
                                        keyboardType="numeric"
                                        name="Price"
                                        editable={TotalTicketDetail === 0 || !EventId}
                                    />
                                    {/* {
                                        Price !== '' && Unit.toUpperCase() === 'VND' && (Price < 10000 || Price > 10000000000) &&
                                        <View style={{ position: 'absolute', right: 0, bottom: 8 }}><Image source={require('../../assets/error.png')} style={{ width: 24, height: 24 }} /></View>
                                    } */}
                                    {this.showErrorPrice()}
                                    {/* <Text style={styles.txtInfoCod}>Price must be equal to 0 or greater than 10,000</Text> */}
                                </View>
                            </View>
                        </View>
                        {
                            Type === 'd-p-ticket' && StatusCOD === 0 &&
                            <Text style={styles.txtInfoCod}>{convertLanguage(language, 'text_this_physical_ticket')} <Text style={{ color: '#00A9F4' }} onPress={() => this.props.onShowModalContactInfo()}>{convertLanguage(language, 'see_more')}.</Text></Text>
                        }
                    </View>
                    {
                        (EventId && TotalTicketDetail > 0) &&
                        <Text style={{ color: '#333333' }}>{convertLanguage(language, 'text_total_detail', { total: TotalTicketDetail })}</Text>
                    }
                    <View style={styles.boxQuantity}>
                        <View style={[styles.boxInput, { flex: 0.45 }]}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'quantity')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                value={Quantity}
                                onChangeText={(Quantity) => this.onChange('Quantity', Quantity)}
                                name="Quantity"
                                keyboardType="numeric"
                            // onEndEditing={(e) => this.onChange('Quantity', e.nativeEvent.text)}
                            />
                        </View>
                        <View style={{ flex: 0.1 }}></View>
                        <View style={[styles.boxInput, { flex: 0.45 }]}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'limit_per_order')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                value={LimitQuantity}
                                onChangeText={(LimitQuantity) => this.onChange('LimitQuantity', LimitQuantity)}
                                keyboardType="numeric"
                                name="LimitQuantity"
                            // onEndEditing={(e) => this.onChange('LimitQuantity', e.nativeEvent.text)}
                            />
                        </View>
                    </View>
                    <View style={styles.boxInput}>
                        <Text style={styles.txtInput}>{convertLanguage(language, 'sales_period')}</Text>
                        <View style={styles.boxDate}>
                            <DatePicker
                                style={{ flex: 0.45 }}
                                date={TimeStartTicket}
                                mode="datetime"
                                placeholder={convertLanguage(language, 'now_ticket')}
                                format="YYYY-MM-DD HH:mm"
                                // minDate={datetimeNow()}
                                maxDate={this.props.TimeFinish}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                iconComponent={<Image style={{ position: 'absolute', right: 0 }} source={require('../../assets/calendar_icon.png')} />}
                                customStyles={{
                                    dateInput: {
                                        borderBottomColor: '#bdbdbd',
                                        borderBottomWidth: 1,
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                        borderTopWidth: 0,
                                    },
                                    dateText: {
                                        color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                    },
                                    btnTextConfirm: {
                                        color: Colors.PRIMARY, fontSize: 17
                                    },
                                    btnTextCancel: {
                                        color: Colors.TEXT_P, fontSize: 16
                                    },
                                    placeholderText: {
                                        alignSelf: 'flex-start',
                                        fontSize: 15,
                                        color: '#bdbdbd'
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(TimeStartTicket) => this.onChange('TimeStartTicket', TimeStartTicket)}
                            />
                            <Text style={styles.txtCenter}>~</Text>
                            <DatePicker
                                style={{ flex: 0.45 }}
                                date={TimeEndTicket}
                                mode="datetime"
                                placeholder={convertLanguage(language, 'finish')}
                                format="YYYY-MM-DD HH:mm"
                                minDate={TimeStartTicket === '' ? datetimeNow() : moment(TimeStartTicket).isSameOrAfter(datetimenow) ? TimeStartTicket : datetimeNow()}
                                disabled={TimeStartTicket === '' ? true : false}
                                maxDate={this.props.TimeFinish}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                iconComponent={<Image style={{ position: 'absolute', right: 0 }} source={require('../../assets/calendar_icon.png')} />}
                                customStyles={{
                                    dateInput: {
                                        borderBottomColor: '#bdbdbd',
                                        borderBottomWidth: 1,
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                        borderTopWidth: 0,
                                    },
                                    dateText: {
                                        color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                    },
                                    btnTextConfirm: {
                                        color: Colors.PRIMARY, fontSize: 17
                                    },
                                    btnTextCancel: {
                                        color: Colors.TEXT_P, fontSize: 16
                                    },
                                    placeholderText: {
                                        alignSelf: 'flex-start',
                                        fontSize: 15,
                                        color: '#bdbdbd'
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(TimeEndTicket) => this.onChange('TimeEndTicket', TimeEndTicket)}
                            />
                        </View>
                    </View>
                    {
                        !show_more &&
                        <Touchable style={styles.boxMoreInfo} onPress={() => this.setState({ show_more: true })}>
                            <View style={styles.boxContentMoreInfo}>
                                <Text style={styles.txtMoreInfo}>{convertLanguage(language, 'more_info')}</Text>
                                <Image source={require('../../assets/MoreInfo_arrow.png')} style={styles.MoreInfo_arrow} />
                            </View>
                        </Touchable>
                    }
                    {
                        show_more &&
                        <React.Fragment>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'description')}</Text>
                                <TextInput
                                    style={[styles.ipContent, TotalTicketDetail === 0 || !EventId ? {} : { backgroundColor: '#cccccc' }]}
                                    selectionColor="#bcbcbc"
                                    value={Description}
                                    onChangeText={(Description) => this.onChange('Description', Description)}
                                    editable={TotalTicketDetail === 0 || !EventId}
                                    multiline={true}
                                // onEndEditing={(e) => this.onChange('Description', e.nativeEvent.text)}
                                />
                            </View>
                            <View style={[styles.boxInput, { paddingBottom: 25, borderBottomWidth: 1, borderBottomColor: '#bdbdbd', }]}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'options')}</Text>
                                <Touchable style={styles.boxItemOptions} onPress={() => Type === 'd-p-ticket' ? null : this.onChange('Option', 1)}>
                                    <Image source={Type === 'd-p-ticket' ? require('../../assets/box_check.png') : Option.indexOf(1) !== -1 ? require('../../assets/box_checked.png') : require('../../assets/box_check.png')} style={styles.ic_radio} />
                                    <Text style={Type === 'd-p-ticket' ? styles.txtOptionsDisable : styles.txtOptions}>{convertLanguage(language, 'ask_a_delivery_address')}</Text>
                                </Touchable>
                                <Touchable style={styles.boxItemOptions} onPress={() => this.onChange('Option', 2)}>
                                    <Image source={Option.indexOf(2) !== -1 ? require('../../assets/box_checked.png') : require('../../assets/box_check.png')} style={styles.ic_radio} />
                                    <Text style={styles.txtOptions}>{convertLanguage(language, 'ask_a_customize_question')}</Text>
                                </Touchable>
                                {
                                    Option.indexOf(2) !== -1 &&
                                    <TextInput
                                        style={[styles.ipContent, { height: 200, borderWidth: 1, borderColor: '#bdbdbd', marginBottom: 7, textAlignVertical: 'top', padding: 5 }]}
                                        selectionColor="#bcbcbc"
                                        name="Question2"
                                        placeholder=""
                                        multiline={true}
                                        value={Question2}
                                        onChangeText={(Question2) => this.onChange('Question2', Question2)}
                                    />
                                }
                            </View>
                            <Touchable style={[styles.boxMoreInfo, { marginTop: 20 }]} onPress={() => this.setState({ show_more: false })}>
                                <View style={styles.boxContentMoreInfo}>
                                    <Text style={styles.txtMoreInfo}>{convertLanguage(language, 'close')}</Text>
                                    <Image source={require('../../assets/close_arrow.png')} style={styles.MoreInfo_arrow} />
                                </View>
                            </Touchable>
                        </React.Fragment>
                    }
                </View>
                <View style={styles.boxAction}>
                    <View style={styles.boxAction1}>
                        <Touchable style={[styles.btnAction, { borderColor: Status == 0 ? '#03a9f4' : '#bdbdbd' }]} onPress={() => this.onChange('Status', Status == 0 ? 1 : 0)}>
                            <Text style={Status == 0 ? styles.txtShow : styles.txtAction}>{Status == 0 ? convertLanguage(language, 'show') : convertLanguage(language, 'hide')}</Text>
                        </Touchable>
                        {
                            TotalTicketDetail === 0
                            &&
                            <Touchable style={styles.btnAction} onPress={() => this.onDelete()}>
                                <Text style={styles.txtAction}>{convertLanguage(language, 'delete')}</Text>
                            </Touchable>
                        }
                    </View>
                    <View style={styles.boxAction2}>
                        {
                            index > 0 &&
                            <Touchable style={styles.btnAction2} onPress={() => this.props.onUpTicket(index)}>
                                <Image source={require('../../assets/up_arrow_icon.png')} style={styles.up_arrow_icon} />
                            </Touchable>
                        }
                        {
                            index < ticket_count - 1 &&
                            <Touchable style={styles.btnAction2} onPress={() => this.props.onDownTicket(index)}>
                                <Image source={require('../../assets/down_arrow_icon.png')} style={styles.up_arrow_icon} />
                            </Touchable>
                        }
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        marginBottom: 20
    },
    boxInput: {
        paddingBottom: 24,
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 7,
        fontWeight: 'bold'
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 8,
        paddingTop: 8,
        fontWeight: 'bold'
    },
    boxPrice: {
        flexDirection: 'row'
    },
    boxDate: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtCenter: {
        flex: 0.1,
        color: '#757575',
        fontSize: 28,
        textAlign: 'center'
    },
    boxQuantity: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    boxItemOptions: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    ic_radio: {
        width: 16,
        height: 16,
        marginRight: 10
    },
    txtOptions: {
        color: '#333333',
        fontSize: 15,
    },
    txtOptionsDisable: {
        color: '#bdbdbd',
        fontSize: 15,
    },
    boxMoreInfo: {
        height: 35,
        borderWidth: 1,
        borderColor: '#757575',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxContentMoreInfo: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtMoreInfo: {
        fontSize: 15,
        color: '#757575',
        marginRight: 10
    },
    MoreInfo_arrow: {
        width: 14,
        height: 11
    },
    boxAction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 30
    },
    boxAction1: {
        flexDirection: 'row',
    },
    btnAction: {
        width: 72,
        height: 35,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        backgroundColor: '#ffffff'
    },
    txtAction: {
        fontSize: 14,
        color: '#757575',
    },
    txtShow: {
        fontSize: 14,
        color: '#03a9f4',
    },
    boxAction2: {
        flexDirection: 'row',
    },
    btnAction2: {
        width: 35,
        height: 35,
        borderRadius: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 15,
        backgroundColor: '#e8e8e8'
    },
    up_arrow_icon: {
        width: 31,
        height: 31
    },
    boxPicker: {
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        flex: 1,
        // borderRadius: 1,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        // width: 100,
        // height: 50,
        // marginRight: 20,
        // borderWidth: 1,
        // borderColor: '#bdbdbd',
        borderRadius: 1,
        // width: 100,
        height: 35,
    },
    txtPicker: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5,
        marginLeft: 5
    },
    txtInfoCod: {
        fontSize: 12,
        color: '#F2994A',
        marginTop: 5
    }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(FormTicket);
