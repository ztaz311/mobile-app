import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import * as Colors from '../../constants/Colors';
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalSetRole extends Component {
    constructor(props) {
        super(props);
    }

    checkRole(role) {
        var authRole = this.props.EventRole;
        var memberRole = this.props.MemberRole;
        var rules = [];
        if (authRole == 'Leader') {
            return true;
        }
        if (role == 'Discharge') {
            if (authRole == 'Director') {
                rules = ['Director', 'Manager', 'Staff'];
            } else if (authRole == 'Manager') {
                rules = ['Manager', 'Staff'];
            }
            if (rules.indexOf(memberRole) != -1) {
                return true;
            }
            return false;
        }
        if (authRole == 'Director') {
            rules = ['Director', 'Manager', 'Staff'];
        } else if (authRole == 'Manager') {
            rules = ['Manager', 'Staff'];
        }
        if (rules.indexOf(memberRole) != -1 && rules.indexOf(role) != -1) {
            return true;
        }
        return false;
    }

    render() {
        var { language } = this.props.language;
        let roleLeader = this.checkRole('Leader');
        let roleDirector = this.checkRole('Director');
        let roleManager = this.checkRole('Manager');
        let roleStaff = this.checkRole('Staff');
        let roleDischarge = this.checkRole('Discharge');
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal('modalSetRole') }} >
                            <Image style={styles.iconClose} source={require('../../assets/X_icon.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <TouchableOpacity
                            disabled={!roleLeader}
                            activeOpacity={roleLeader ? 0.2 : 1}
                            style={[styles.btnAction, roleLeader ? {} : styles.btnDisabled]} onPress={() => { roleLeader ? this.props.setEventRole('leader') && this.props.closeModal('modalSetRole') : null }} >
                            <Text style={roleLeader ? styles.txtBtn : styles.txtBtnDisabled} >{convertLanguage(language, 'set_as_a_leader')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            disabled={!roleDirector}
                            activeOpacity={roleDirector ? 0.2 : 1}
                            style={[styles.btnAction, roleDirector ? {} : styles.btnDisabled]} onPress={() => { roleDirector ? this.props.setEventRole('director') && this.props.closeModal('modalSetRole') : null }} >
                            <Text style={roleDirector ? styles.txtBtn : styles.txtBtnDisabled} >{convertLanguage(language, 'set_as_a_director')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            disabled={!roleManager}
                            activeOpacity={roleManager ? 0.2 : 1}
                            style={[styles.btnAction, roleManager ? {} : styles.btnDisabled]} onPress={() => { roleManager ? this.props.setEventRole('manager') && this.props.closeModal('modalSetRole') : null }} >
                            <Text style={roleManager ? styles.txtBtn : styles.txtBtnDisabled} >{convertLanguage(language, 'set_as_a_manager')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            disabled={!roleStaff}
                            activeOpacity={roleStaff ? 0.2 : 1}
                            style={[styles.btnAction, roleStaff ? {} : styles.btnDisabled]} onPress={() => { roleStaff ? this.props.setEventRole('staff') && this.props.closeModal('modalSetRole') : null }} >
                            <Text style={roleStaff ? styles.txtBtn : styles.txtBtnDisabled} >{convertLanguage(language, 'set_as_a_staff')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            disabled={!roleDischarge}
                            activeOpacity={roleDischarge ? 0.2 : 1}
                            style={[styles.btnAction, roleDischarge ? {} : styles.btnDisabled]} onPress={() => { roleDischarge ? this.props.leaveEvent() && this.props.closeModal('modalSetRole') : null }} >
                            <Text style={roleDischarge ? styles.txtBtn : styles.txtBtnDisabled}>{convertLanguage(language, 'discharge')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        //   flex: 1,
        //   justifyContent: "center",
        //   alignItems: "center"
    },
    btnAction: {
        padding: 12,
        margin: 4,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "#333333",
        borderWidth: 2,
    },
    modalContent: {
        backgroundColor: "white",
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 12,
    },
    btnDisabled: {
        borderColor: Colors.GRAY
    },
    txtBtn: {
        color: "#333333",
    },
    txtBtnDisabled: {
        color: Colors.GRAY
    }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalSetRole);