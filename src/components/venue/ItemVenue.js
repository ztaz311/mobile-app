import React, { Component } from 'react';
import { Text, Image, ImageBackground, StyleSheet, Dimensions } from 'react-native';

import Touchable from '../../screens_view/Touchable'
const { width, height } = Dimensions.get('window')
import { convertDate } from '../../services/Helper'
class ItemEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        var {data} = this.props;
        return (
            <Touchable onPress={() => this.props.navigation.navigate({name: 'EventDetail', params: {Slug: data.Slug}, key: data.Slug})} style={styles.boxSlide}>
                <ImageBackground style={styles.boxThumb} source={{uri: data.Posters ? data.Posters.Medium : data.Poster}} >
                    <Touchable style={{padding: 10}} onPress={() => this.props.onLikeEvent(data.Id)}>
                        <Image source={data.Like ? require('../../assets/like_icon_pink.png') : require('../../assets/like_icon_gry.png')} style={styles.icon_like} />
                    </Touchable>
                </ImageBackground>
                <Text style={styles.txtName} numberOfLines={2}>{ data.Title }</Text>
                <Text style={styles.txtTime}>{convertDate(data.TimeStart, data.TimeFinish)}</Text>
                <Text style={styles.txtTime}>{ data.VenueName }</Text>
                <Text style={styles.txtCategory}>
                    {
                        data.HashTag &&
                        data.HashTag.map((Tag, index) => {
                            return <React.Fragment key={index}>
                                #{Tag.HashTagName + ' '}
                            </React.Fragment>
                        })
                    }
                </Text>
            </Touchable>
        );
    }
}

const styles = StyleSheet.create({
    boxSlide: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20
    },
    boxThumb: {
        width: width - 40,
        height: 170,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginBottom: 10
    },
    icon_like: {
        width: 26,
        height: 22,
    },
    txtName: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 10
    },
    txtTime: {
        fontSize: 14,
        color: '#757575'
    },
    txtCategory: {
        fontSize: 14,
        color: '#333333',
        marginTop: 10
    },
});
export default ItemEvent;
