import React, { Component } from 'react';
import { Text, Image, ImageBackground, StyleSheet, Dimensions } from 'react-native';

import Touchable from '../../screens_view/Touchable'
const { width, height } = Dimensions.get('window')
import { convertDate } from '../../services/Helper'
class ItemVenueHalf extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        var {data} = this.props;
        return (
            <Touchable onPress={() => null} style={styles.boxSlide}>
                <ImageBackground style={styles.boxThumb} source={{uri: data.MainImages ? data.MainImages.Small : data.MainImage}} imageStyle={{ borderRadius: 2 }} >
                </ImageBackground>
                <Text style={styles.txtName} numberOfLines={2}>{ data.Name }</Text>
                <Text style={styles.txtTime} numberOfLines={2}>{ data.TimeOpen }</Text>
                <Text style={styles.txtCategory} numberOfLines={2}>
                    {
                        data.HashTag &&
                        data.HashTag.map((Tag, index) => {
                            return <React.Fragment key={index}>
                                #{Tag.HashTagName + ' '}
                            </React.Fragment>
                        })
                    }
                </Text>
            </Touchable>
        );
    }
}

const styles = StyleSheet.create({
    boxSlide: {
        // flex: 1,
        width: '48%',
        paddingBottom: 20
    },
    boxThumb: {
        width: '100%',
        aspectRatio: 16/9,
        resizeMode: 'cover',
        // height: 170,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginBottom: 10,
        backgroundColor: '#bdbdbd',
        borderRadius: 2
    },
    icon_like: {
        width: 26,
        height: 22,
    },
    txtName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtTime: {
        fontSize: 14,
        color: '#757575'
    },
    txtCategory: {
        fontSize: 14,
        color: '#333333',
        marginTop: 5
    },
});
export default ItemVenueHalf;
