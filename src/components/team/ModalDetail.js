import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, ActivityIndicator, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../../screens_view/ScrView';
// import HTMLView from 'react-native-htmlview';
import AutoHeightWebView from 'react-native-autoheight-webview';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import SafeView from '../../screens_view/SafeView';
class ModalFAQ extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }

    render() {
        var { Detail } = this.props;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                style={{ margin: 0 }}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/X_icon.png')} />
                        </TouchableOpacity>
                    </View>
                    <SrcView style={styles.container}>
                        {/* <View style={styles.content}> */}
                        <View style={{overflow:'hidden', marginLeft: 20, marginRight: 20}}>
                            <AutoHeightWebView scalesPageToFit={false} zoomable={false} scrollEnabled={false} startInLoadingState={true} renderLoading={this.renderLoading} source={{html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 10}px; height: ${(width - 10)*9/16}px} img {width: ${width - 10}px !important; margin-bottom: 10px; height: auto !important;}</style><body>` + Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com')  + `</body></html>`}} originWhitelist={['*']} />
                        </View>
                            {/* <HTMLView addLineBreaks={false} stylesheet={stylesHtml} value={Detail} renderNode={this.renderNode} /> */}
                        {/* </View> */}
                    </SrcView>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        flex: 1
    },
    container: {
        // padding: 20,
    },
    content: {
        padding: 20,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 8,
        borderColor: '#cccccc',
        borderWidth: 1,
        borderRadius: 24,
        margin: 20,
        marginRight: 10,
        marginBottom: 10,
        marginTop: 10
    },
    txtFAQ: {
        paddingTop: 30,
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15,
    },
    txtLable: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 10,
    },
    value: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 7,
        paddingLeft: 15,
    },
});

const stylesHtml = StyleSheet.create({
    p: {
        marginTop: 3,
        marginBottom: 3
    }
});

export default ModalFAQ;