import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Alert, ActivityIndicator, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Popover from 'react-native-popover-view';
import Touchable from '../../screens_view/Touchable'
import AutoHeightWebView from 'react-native-autoheight-webview';
import * as Colors from '../../constants/Colors'
import HTMLView from 'react-native-htmlview';
import YouTube from 'react-native-youtube';
import PhotoGrid from 'react-native-thumbnail-grid';
import ImageView from 'react-native-image-view';
import { Modalize } from 'react-native-modalize';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import Share from 'react-native-share';
import * as Config from '../../constants/Config';
import { convertLanguage } from '../../services/Helper';
import { actGlobalCheckLogin } from '../../actions/global';
import { connect } from "react-redux";
class ModalTeamNewsDetail extends Component {
    modal = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            isImageViewVisible: false,
            index: 0,
            isVisible: false
        }
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }

    getId = (url) => {
        var ID = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        }
        else {
            ID = url;
        }
        return ID;
    }

    renderNode = (node, index, siblings, parent, defaultRenderer) => {
        if (node.name == 'iframe') {
            const a = node.attribs;
            const iframeHtml = `<iframe src="${a.src}" allowfullscreen></iframe>`;
            return (
                <View key={index} style={{ width: width - 40, height: (width - 40) * Number(a.height) / Number(a.width) }}>
                    {
                        Platform.OS === 'ios' ?
                            <YouTube
                                videoId={this.getId(a.src)} // The YouTube video ID
                                play={false} // control playback of video with true/false
                                fullscreen={false} // control whether the video should play in fullscreen or inline
                                style={{ alignSelf: 'stretch', height: (width - 40) * Number(a.height) / Number(a.width) }}
                            />
                            :
                            <AutoHeightWebView source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40) * 9 / 16}px}</style><body>` + iframeHtml + `</body></html>` }} />
                    }
                </View>
            );
        }
    }

    getImages(datas) {
        var images = [];
        datas.forEach(data => {
            images.push(data.Photos.Large)
        });
        return images;
    }

    getImagesZoom(datas) {
        var images = [];
        datas.forEach(data => {
            images.push({
                source: {
                    uri: data.Photos.Large
                },
            })
        });
        return images;
    }

    onZoomImage(uri, images) {
        this.setState({
            isImageViewVisible: true,
            index: images.indexOf(uri) === -1 ? 0 : images.indexOf(uri)
        })
    }

    componentDidMount() {
        if (this.modal.current) {
            this.modal.current?.open();
            this.props.navigation.setParams({ visible: true })
        }
    }

    closeModal = () => {
        if (this.modal.current) {
            this.modal.current.close();
        }
    };

    share() {
        const { data } = this.props
        let options = {
            title: data.Team.Name,
            message: data.Team.Name + "\nFind and follow us in Comeup\n",
            url: Config.MAIN_URL + 'team-news?news=' + data.Id,
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    footerComponent = (data) => (
        <SafeAreaConsumer>
            {
                insets =>
                    <View style={[styles.boxEventMore, { paddingBottom: insets ? insets.bottom === 0 ? 10 : insets.bottom : 10 }]}>
                        <View style={styles.boxEventMoreLeft}>
                            <Touchable onPress={() => this.props.onLikeTeamNews(data.Id)}>
                                <Image source={data.IsLike ? require('../../assets/liked_red.png') : require('../../assets/like_border_black.png')} style={styles.icLike} />
                            </Touchable>
                            <Touchable onPress={() => this.share()}>
                                <Image source={require('../../assets/share_2.png')} style={styles.icLike} />
                            </Touchable>
                        </View>
                    </View>
            }
        </SafeAreaConsumer>
    );

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    onDeleteTeamNews() {
        const { data } = this.props
        var { language } = this.props.language;
        Alert.alert(
            convertLanguage(language, 'delete_team_news'),
            convertLanguage(language, 'do_you_want_to_delete_this_news'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => { this.setState({ isVisible: false }); this.closeModal(); this.props.onDeleteTeamNews(data.Id) } },
            ],
            { cancelable: false },
        );
    }

    render() {
        var { data } = this.props;
        var { language } = this.props.language;
        var images = this.getImages(data.Images);
        var imageZooms = this.getImagesZoom(data.Images);
        return (
            <Modalize
                ref={this.modal}
                // withReactModal={true}
                scrollViewProps={{
                    showsVerticalScrollIndicator: false,
                    stickyHeaderIndices: [0],
                }}
                onClose={() => this.props.navigation.setParams({ visible: false })}
                onClosed={() => this.props.closeModal()}
                FooterComponent={this.footerComponent(data)}
            // scrollViewProps={{ flex: 1 }}
            >
                <View style={styles.boxInfo}>
                    <View style={styles.boxRow}>
                        <View style={styles.boxClose}></View>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Team News</Text>
                        <Touchable style={styles.boxClose} onPress={this.closeModal}>
                            <Image source={require('../../assets/close.png')} style={styles.icClose} />
                        </Touchable>
                    </View>
                    <View style={[styles.boxRow, { marginTop: 10 }]}>
                        <Touchable style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.props.navigation.navigate({ name: 'DetailTeam', params: { id: data.Team.Id }, key: data.Team.Id })}>
                            <Image source={{ uri: data.Team ? data.Team.Logo : '' }} style={{ width: 60, height: 60, borderWidth: 1, borderRadius: 30, borderColor: Colors.TEXT_S }} />
                            <View style={{ marginLeft: 20 }}>
                                <Text numberOfLines={2} style={{ color: Colors.TEXT_P, fontSize: 18 }}>{data.Team ? data.Team.Name : ''}</Text>
                                <Text style={{ color: Colors.TEXT_S, fontSize: 16 }}>{data.CreatedAt}</Text>
                            </View>
                        </Touchable>
                        {
                            this.props.Role !== 'Guest' &&
                            <Touchable ref={ref => this.touchable = ref} onPress={() => this.showPopover()} style={{ width: 30, height: 30, alignItems: 'flex-end' }}>
                                <Image source={require('../../assets/more_horizontal.png')} style={{ width: 30, height: 30 }} />
                            </Touchable>
                        }
                    </View>
                </View>
                <View style={styles.modalContent}>
                    <HTMLView
                        value={data.Content.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com')}
                        renderNode={this.renderNode}
                        paragraphBreak={'\n'}
                    />
                    <PhotoGrid source={images} onPressImage={(data, uri) => this.onZoomImage(uri, images)} width={width - 40} styles={{ marginTop: 15 }} height={images.length === 1 ? (width - 40) * 9 / 16 : 300} />
                    {
                        data.Link !== "" &&
                        <YouTube
                            videoId={data.Link} // The YouTube video ID
                            play={false} // control playback of video with true/false
                            fullscreen={false} // control whether the video should play in fullscreen or inline
                            style={{ alignSelf: 'stretch', height: (width - 16) * 9 / 16, marginTop: 16 }}
                            apiKey={'AIzaSyASjD7n17ht9T7huOQ8V0GXbfqsz9xYWC0'}
                        />
                    }
                </View>
                <ImageView
                    images={imageZooms}
                    imageIndex={this.state.index}
                    isVisible={this.state.isImageViewVisible}
                    onClose={() => this.setState({ isImageViewVisible: false })}
                />
                {
                    this.state.isVisible &&
                    <Popover
                        isVisible={true}
                        animationConfig={{ duration: 0 }}
                        fromView={this.touchable}
                        placement='bottom'
                        arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                        backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                        onRequestClose={() => this.closePopover()}>
                        <View style={styles.boxPopover}>
                            {
                                data.Team.Role != 'Guest' ?
                                    <>
                                        <Touchable onPress={() => { this.closePopover(); this.props.navigation.navigate('CreateANews', { TeamNewsId: data.Id }) }} style={styles.boxNewRead}>
                                            <Text style={styles.txtNewRead}>{convertLanguage(language, 'edit')}</Text>
                                        </Touchable>
                                        <Touchable onPress={() => this.onDeleteTeamNews()} style={styles.boxNewRead}>
                                            <Text style={styles.txtNewRead}>{convertLanguage(language, 'delete')}</Text>
                                        </Touchable>
                                    </>
                                    :
                                    <Touchable onPress={() => { this.closePopover(); this.props.globalCheckLogin('TeamNewsReport', { TeamNewsId: data.Id }) }} style={styles.boxNewRead}>
                                        <Text style={styles.txtNewRead}>{convertLanguage(language, 'report')}</Text>
                                    </Touchable>
                            }
                        </View>
                    </Popover>
                }
            </Modalize>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        paddingLeft: 15,
        paddingRight: 15,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 8,
        borderColor: '#cccccc',
        borderWidth: 1,
        borderRadius: 24,
        marginLeft: 10,
        marginRight: 20,
        marginBottom: 10,
        marginTop: Platform.OS === 'ios' ? 30 : 10
    },
    boxInfo: {
        flexDirection: 'row',
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.85)',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    boxRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    boxClose: {
        width: 40,
        height: 40,
        alignItems: 'flex-end'
    },
    icClose: {
        width: 30,
        height: 30
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 23,
        marginRight: 8
    },
    boxInfoRight: {
        flex: 1
    },
    txtNameInfo: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 5
    },
    txtTimeAgo: {
        fontSize: 14,
        color: '#757575',
    },
    imgThumbNews: {
        width: width - 40,
        height: 190,
        marginBottom: 15
    },
    txtEventContent: {
        fontSize: 14,
        color: '#333333',
        marginBottom: 15
    },
    boxEventMore: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10,
    },
    boxEventMoreLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icLike: {
        width: 30,
        height: 30,
        marginRight: 10
    },
    boxImages: {
        marginBottom: 15
    },
    boxItem: {
        marginRight: 15,
    },
    image: {
        width: 100,
        height: 100
    },
    boxPopover: {
        paddingVertical: 8,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 4,
    },
    boxNewRead: {
        paddingVertical: 4,
        paddingRight: 48,
        paddingLeft: 10
    },
    txtNewRead: {
        color: '#212529',
        fontSize: 16
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        globalCheckLogin: (nextPage) => {
            dispatch(actGlobalCheckLogin(nextPage))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalTeamNewsDetail);