import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalTeamNews extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                onBackdropPress={() => this.props.closeModal()}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={500}
                animationOutTiming={0}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                backdropTransitionInTiming={1000}
                backdropTransitionOutTiming={1000}>
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        {
                            this.props.Role != 'Guest' ?
                            <>
                                <TouchableOpacity style={styles.btnReport} onPress={() => { this.props.onEditTeamNews() }}>
                                    <Text style={styles.txtReport}>{convertLanguage(language, 'edit')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.btnReport} onPress={() => { this.props.onDeleteTeamNews() }}>
                                    <Text style={styles.txtReport}>{convertLanguage(language, 'delete')}</Text>
                                </TouchableOpacity>
                            </>
                            :
                            <TouchableOpacity style={styles.btnReport} onPress={() => { this.props.onReportTeamNews() }}>
                                <Text style={styles.txtReport}>{convertLanguage(language, 'report')}</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    btnReport: {
        width: 220,
        height: 50,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
        borderRadius: 5
    },
    txtReport: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    btnDelete: {
        width: 220,
        height: 50,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    txtDelete: {
        fontSize: 15,
        color: '#bdbdbd'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalTeamNews);