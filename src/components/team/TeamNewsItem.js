import React, { Component } from 'react';
import {
    Dimensions, Platform,
    View, Text, Image, StyleSheet, TouchableOpacity, Alert
} from 'react-native';
import Popover from 'react-native-popover-view';
import Touchable from '../../screens_view/Touchable'
import * as Colors from '../../constants/Colors'
import Share from 'react-native-share';
import AutoHeightWebView from 'react-native-autoheight-webview';
const { width, height } = Dimensions.get('window')
import * as Config from '../../constants/Config';
// import LinearGradient from 'react-native-linear-gradient';
import HTMLView from 'react-native-htmlview';
import YouTube from 'react-native-youtube';
import PhotoGrid from 'react-native-thumbnail-grid';
import ImageView from 'react-native-image-view';
import { convertLanguage } from '../../services/Helper';
import { actGlobalCheckLogin } from '../../actions/global';
import { connect } from "react-redux";
class TeamNewsItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isViewAll: true,
            webviewHeight: 0,
            webviewParentHeight: 0,
            isImageViewVisible: false,
            index: 0,
            isVisible: false
        }
    }
    share() {
        const { data } = this.props
        let options = {
            title: data.Team.Name,
            message: data.Team.Name + "\nFind and follow us in Comeup\n",
            url: Config.MAIN_URL + 'team-news?news=' + data.Id,
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }
    getId = (url) => {
        var ID = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        }
        else {
            ID = url;
        }
        return ID;
    }
    renderNode = (node, index, siblings, parent, defaultRenderer) => {
        if (node.name == 'iframe') {
            const a = node.attribs;
            const iframeHtml = `<iframe src="${a.src}" allowfullscreen></iframe>`;
            return (
                <View key={index} style={{ width: width - 48, height: (width - 48) * Number(a.height) / Number(a.width) }}>
                    {
                        Platform.OS === 'ios' ?
                            <YouTube
                                videoId={this.getId(a.src)} // The YouTube video ID
                                // play // control playback of video with true/false
                                fullscreen={false} // control whether the video should play in fullscreen or inline
                                style={{ alignSelf: 'stretch', height: (width - 48) * Number(a.height) / Number(a.width) }}
                                // apiKey={'AIzaSyDVOHhrkvJ8_sZhQeLADdwVA19zFhq1hW0'}
                                play={false}
                            />
                            :
                            <AutoHeightWebView source={{ html: iframeHtml }} />
                    }
                </View>
            );
        }
    }
    getImages(datas) {
        var images = [];
        datas.forEach(data => {
            images.push(data.Photos.Large)
        });
        return images;
    }

    getImagesZoom(datas) {
        var images = [];
        datas.forEach(data => {
            images.push({
                source: {
                    uri: data.Photos.Large
                },
            })
        });
        return images;
    }

    onZoomImage(uri, images) {
        this.setState({
            isImageViewVisible: true,
            index: images.indexOf(uri) === -1 ? 0 : images.indexOf(uri)
        })
    }

    showYoutubeVideo() {
        var { data } = this.props;
        if (Platform.OS === 'ios') {
            return <YouTube
                videoId={data.Link} // The YouTube video ID
                play={false} // control playback of video with true/false
                fullscreen={false} // control whether the video should play in fullscreen or inline
                style={{ alignSelf: 'stretch', height: (width - 16) * 9 / 16, marginTop: 16 }}
                apiKey={'AIzaSyASjD7n17ht9T7huOQ8V0GXbfqsz9xYWC0'}
            />
        } else {
            return <View style={{ marginTop: 16 }}><AutoHeightWebView source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 48}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40) * 9 / 16}px}</style><body><iframe src="https://www.youtube.com/embed/${data.Link}" allowfullscreen></iframe></body></html>` }} /></View>
        }
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    onDeleteTeamNews() {
        const { data } = this.props
        var { language } = this.props.language;
        Alert.alert(
            convertLanguage(language, 'delete_team_news'),
            convertLanguage(language, 'do_you_want_to_delete_this_news'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => { this.setState({ isVisible: false }); this.props.onDeleteTeamNews(data.Id) } },
            ],
            { cancelable: false },
        );
    }

    render() {
        var { language } = this.props.language;
        const { data, style, Id } = this.props
        var images = this.getImages(data.Images);
        var imageZooms = this.getImagesZoom(data.Images);
        return <View style={styles.boxNews}>
            <View style={styles.boxHeader}>
                <Touchable style={styles.boxInfo} onPress={() => Id ? this.props.navigation.navigate({ name: 'DetailTeam', params: { id: Id }, key: Id }) : null}>
                    <Image source={{ uri: data.Team ? data.Team.Logo : '' }} style={{ width: 48, height: 48, borderWidth: 1, borderRadius: 24, borderColor: Colors.TEXT_S }} />
                    <View style={{ marginLeft: 8, flex: 1 }}>
                        <Text style={{ fontSize: 15, color: '#333333', fontWeight: 'bold' }} numberOfLines={2}>{data.Team ? data.Team.Name : ''}</Text>
                        <Text style={{ color: Colors.TEXT_S, fontSize: 16 }}>{data.CreatedAt}</Text>
                    </View>
                </Touchable>
                <Touchable style={styles.boxMore} ref={ref => this.touchable = ref} onPress={() => this.showPopover()}>
                    <Image source={require('../../assets/more_horizontal.png')} style={styles.icMore} />
                </Touchable>
            </View>
            {
                data.Content != '' &&
                <>
                    <TouchableOpacity disabled={!data.ContentShort} onPress={() => this.props.onOpenModelTeamNewsDetail()} activeOpacity={1}>
                        <HTMLView
                            value={data.ContentShort ? data.ContentShort.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') : data.Content.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com')}
                            renderNode={this.renderNode}
                            stylesheet={stylesHtml}
                            paragraphBreak={'\n'}
                        />
                    </TouchableOpacity>
                    {
                        data.ContentShort ?
                            <Text style={styles.txtReadMore}>... <Text style={{ color: '#03a9f4' }} onPress={() => this.props.onOpenModelTeamNewsDetail()}>{convertLanguage(language, 'read_more')}</Text></Text>
                            : null
                    }
                </>
            }
            {
                data.Images.length > 0 &&
                <PhotoGrid source={images} onPressImage={(data, uri) => this.onZoomImage(uri, images)} width={width - 48} styles={{ marginTop: 15 }} height={data.Images.length === 1 ? (width - 48) * 9 / 16 : 300} />
            }
            {
                (data.Link !== "") &&
                this.showYoutubeVideo()
            }
            <View style={{ flexDirection: 'row', alignSelf: 'stretch', marginTop: 12 }}>
                <Touchable
                    style={{ minWidth: 40, justifyContent: 'center' }}
                    onPress={() => this.props.onLikeTeamNews(data.Id)}>
                    <Image source={data.IsLike ? require('../../assets/liked_red.png') : require('../../assets/like_border_black.png')} style={{ width: 30, height: 30, marginRight: 10 }} />
                </Touchable>
                <Touchable
                    onPress={() => this.share()}
                    style={{ minWidth: 40, justifyContent: 'center' }}>
                    <Image source={require('../../assets/share_2.png')} style={{ width: 30, height: 30, marginRight: 10 }} />
                </Touchable>
            </View>
            <ImageView
                images={imageZooms}
                imageIndex={this.state.index}
                isVisible={this.state.isImageViewVisible}
                onClose={() => this.setState({ isImageViewVisible: false })}
            />
            {
                this.state.isVisible &&
                <Popover
                    isVisible={true}
                    animationConfig={{ duration: 0 }}
                    fromView={this.touchable}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => this.closePopover()}>
                    <View style={styles.boxPopover}>
                        {
                            data.Team.Role != 'Guest' ?
                                <>
                                    <Touchable onPress={() => { this.closePopover(); this.props.navigation.navigate('CreateANews', { TeamNewsId: data.Id }) }} style={styles.boxNewRead}>
                                        <Text style={styles.txtNewRead}>{convertLanguage(language, 'edit')}</Text>
                                    </Touchable>
                                    <Touchable onPress={() =>  this.onDeleteTeamNews()} style={styles.boxNewRead}>
                                        <Text style={styles.txtNewRead}>{convertLanguage(language, 'delete')}</Text>
                                    </Touchable>
                                </>
                                :
                                <Touchable onPress={() => { this.closePopover(); this.props.globalCheckLogin('TeamNewsReport', { TeamNewsId: data.Id }) }} style={styles.boxNewRead}>
                                    <Text style={styles.txtNewRead}>{convertLanguage(language, 'report')}</Text>
                                </Touchable>
                        }
                    </View>
                </Popover>
            }
        </View>
    }
}
const stylesHtml = StyleSheet.create({
    p: {
        marginBottom: 0,
    },
    img: {
        width: width - 40,
        resizeMode: 'cover'
    }
});
const styles = StyleSheet.create({
    boxNews: {
        paddingTop: 10,
        paddingBottom: 10,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 4,
        backgroundColor: '#F5F5F5',
        paddingLeft: 8,
        paddingRight: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.17,
        shadowRadius: 7.49,
        elevation: 12,
    },
    boxImages: {
        marginBottom: 15,
        marginTop: 15
    },
    boxItem: {
        marginRight: 15,
    },
    image: {
        width: 100,
        height: 100
    },
    btnReadAllNew: {
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtReadAllNew: {
        fontSize: 15,
        color: '#03a9f4',
        fontWeight: 'bold'
    },
    txtReadMore: {
        fontSize: 13,
        marginTop: 5
    },
    boxMore: {
        width: 30,
        height: 24,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    icMore: {
        width: 24, height: 24
    },
    boxHeader: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingBottom: 15,
        justifyContent: 'space-between'
    },
    boxInfo: {
        flexDirection: 'row',
        flex: 1
    },
    boxPopover: {
        paddingVertical: 8,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 4,
    },
    boxNewRead: {
        paddingVertical: 4,
        paddingRight: 48,
        paddingLeft: 10
    },
    txtNewRead: {
        color: '#212529',
        fontSize: 16
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        globalCheckLogin: (nextPage) => {
            dispatch(actGlobalCheckLogin(nextPage))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamNewsItem);