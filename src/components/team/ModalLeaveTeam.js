import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet, Dimensions, Platform, Image } from 'react-native';
const { width, height } = Dimensions.get('window')
import * as Colors from '../../constants/Colors'
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalLeaveTeam extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        var { language } = this.props.language
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal('modalLeave') }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.txtTitleSure}>{convertLanguage(language, 'sure_to_leave_your_team')}</Text>
                    <View style={styles.content}>
                        <TouchableOpacity
                            style={styles.btnLeave}
                            onPress={() => { this.props.closeModal('modalLeave') }}
                        >
                            <Text style={styles.txtBtnLeave}>{convertLanguage(language, 'no')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.btnLeave}
                            onPress={() => { this.props.leaveTeam() }}
                        >
                            <Text style={styles.txtBtnLeave}>{convertLanguage(language, 'yes')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    txtTitleSure: {
        padding: 10,
        fontSize: 20,
        textAlign: 'center',
        color: '#333333'
    },
    btnClose: {
        padding: 8,
        flex: 0.4,
        color: Colors.PRIMARY,
        borderWidth: 1,
        borderColor: Colors.PRIMARY,
        alignItems: 'center',
        borderRadius: 5
    },
    btnLeave: {
        padding: 8,
        flex: 0.4,
        color: '#333333',
        borderWidth: 1,
        borderColor: '#333333',
        alignItems: 'center',
        borderRadius: 5
    },
    txtBtnClose: {
        color: Colors.PRIMARY,
        fontSize: 18
    },
    txtBtnLeave: {
        color: '#333333',
        fontSize: 18
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 12,
    },
    iconClose: {
        width: 30,
        height: 30
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalLeaveTeam);