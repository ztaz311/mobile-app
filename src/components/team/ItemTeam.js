import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import Touchable from '../../screens_view/Touchable';
import * as Colors from '../../constants/Colors';
import { getTag } from '../../services/Helper';

import TouchableLoading from '../view/TouchableLoading';
import { convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";

class ItemTeam extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data
        };
    }

    inviteOrFollow = async () => {
        this.props.followTeam(this.props.data.Id);
    }

    render() {
        const { data } = this.props
        var { language } = this.props.language

        return <Touchable style={styles.content} onPress={() => this.props.navigation.navigate({name: 'DetailTeam', params: { id: this.props.data.Id }, key: this.props.data.Id})} >
            <FastImage style={styles.teamThumb} source={{ uri: data.Logos.Medium+'' }} />
            <View style={styles.colInfo}>
                <View>
                    <Text style={styles.txtName} numberOfLines={2}>{data.Name}</Text>
                    <Text style={styles.txtCode} numberOfLines={2}>({data.Code})</Text>
                    <Text numberOfLines={2} style={styles.txtTag}>{getTag(data.HashTag)}</Text>
                </View>
                {
                    data.Role != 'Guest' ? null :
                        <TouchableLoading
                        activeOpacity={0.7}
                            disabled={this.props.disabled}
                            onPress={() => { this.inviteOrFollow() }}
                            style={data.IsFollow ? styles.btnFollowing : styles.btnFollow}
                            loading={false}
                        >
                            <Text style={data.IsFollow ? styles.txtBtnFollowing : styles.txtBtnFollow}>{data.IsFollow ? convertLanguage(language, 'following') : convertLanguage(language, 'follow')}</Text>
                        </TouchableLoading>
                }
            </View>
        </Touchable>

    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        flexDirection: 'row'
    },
    teamThumb: {
        width: 112,
        height: 112,
        borderRadius: 56,
        backgroundColor: '#bdbdbd',
    },
    colInfo: {
        flex: 1,
        marginLeft: 16,
        marginRight: 16,
        justifyContent: 'space-between',
    },
    txtName: {
        color: Colors.TEXT_P,
        fontSize: 17,
        fontWeight: 'bold'
    },
    txtCode: {
        color: Colors.GRAY,
        marginTop: 4
    },
    txtTag: {
        fontSize: 14,
        color: '#333333',
        marginTop: 4,
        marginBottom: 12
    },
    btnFollow: {
        minWidth: 110,
        height: 32,
        backgroundColor: Colors.PRIMARY,
        borderRadius: 4, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-start',
        flexDirection: 'row',
        paddingHorizontal: 12
    },
    txtBtnFollow: {
        fontSize: 17,
        color: 'white',
        fontWeight: '600'
    },
    btnFollowing: {
        minWidth: 110,
        height: 32,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#bdbdbd',
        borderRadius: 4, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-start',
        flexDirection: 'row',
        paddingHorizontal: 12
    },
    txtBtnFollowing: {
        fontSize: 17,
        color: '#bdbdbd',
        fontWeight: '600'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ItemTeam);
