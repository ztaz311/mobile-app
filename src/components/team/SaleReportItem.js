import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet } from 'react-native';
import { formatNumber, convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";
import { convertDate5 } from '../../services/Helper';

class SaleReportItem extends Component {
    constructor(props) {
        super(props);
    }

    onNavigate = (slug) => {
        this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: slug }, key: slug })
    }
    render() {
        var { item } = this.props;
        var { language } = this.props.language;
        return (
            <View style={styles.content}>
                <View style={styles.container}>
                    <TouchableOpacity style={styles.colThumb} onPress={() => this.onNavigate(this.props.item.Slug)}>
                        <Image
                            style={styles.imgThumb}
                            source={{ uri: item.Posters.Small }}>
                        </Image>
                    </TouchableOpacity>
                    <View style={styles.colContent}>
                        <TouchableOpacity onPress={() => this.onNavigate(this.props.item.Slug)}><Text style={styles.txtName} numberOfLines={2}>{item.Title}</Text></TouchableOpacity>
                        {/* <Text style={styles.txtDate} numberOfLines={2}>{item.TimeStart.replace(/-/gi, '.')} - {item.TimeFinish.replace(/-/gi, '.')}</Text> */}
                        <Text style={styles.txtDate} numberOfLines={2}>{convertDate5(item.TimeStart, item.TimeFinish, language)}</Text>
                        <Text style={styles.txtDate} numberOfLines={2}>{item.Address}</Text>
                    </View>
                </View>
                {
                    item.HostTeam &&
                    <View style={styles.boxTeam}>
                        <Text style={styles.txtDate}>{convertLanguage(language, 'hosted_by')}</Text>
                        <Image source={{ uri: item.HostTeam.Logos ? item.HostTeam.Logos.Small : '' }} style={styles.teamLogo} />
                        <Text style={styles.txtTeam}>{item.HostTeam.Name}</Text>
                    </View>
                }
                <View style={styles.hr} />
                <Text style={[styles.txtPrice]} numberOfLines={2}>{convertLanguage(language, 'total_sales')}: {item.Unit} {formatNumber(item.TotalSales)}</Text>
                <Text style={[styles.txtPrice, { marginBottom: 15 }]} numberOfLines={2}>{convertLanguage(language, 'revenue')}: {item.Unit} {formatNumber(item.TotalRevenue)}</Text>
                <TouchableOpacity style={styles.btnViewReport} onPress={() => this.props.navigation.navigate('EventSaleReport', { Slug: item.Slug })}>
                    <Text style={styles.txtBtnViewReport}>{convertLanguage(language, 'see_detail')}</Text>
                    <Image source={require('../../assets/arrow_right_gray.png')} style={styles.arrowRight} />
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        marginBottom: 20,
        borderRadius: 4,
        shadowOffset: { width: 0, height: 3 },
        shadowColor: 'black',
        shadowRadius: 4,
        shadowOpacity: 0.2,
        backgroundColor: '#FFFFFF',
        elevation: 5,
        padding: 8
    },
    container: {
        //   flex: 1,
        flexDirection: 'row',
        marginBottom: 15
    },
    colThumb: {
        marginRight: 8,
    },
    colContent: {
        flex: 1,
    },
    rowContent: {
        flexDirection: 'row',
    },
    colInfo: {
        flex: 0.75,
    },
    imgThumb: {
        height: 100,
        width: 100,
        borderRadius: 4
    },
    colAction: {
        flex: 0.25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imgCollect: {
        width: 40,
        height: 40
    },
    rowDraw: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    btnViewReport: {
        flex: 0.5,
        height: 36,
        // marginRight: 15,
        borderWidth: 1,
        borderColor: '#4F4F4F',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 5,
    },
    txtBtnViewReport: {
        fontSize: 13,
        color: '#333333'
    },
    btnDraw: {
        flex: 0.5,
        height: 31,
        marginLeft: 15,
        borderWidth: 1,
        borderColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
    },
    txtName: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333333',
        marginBottom: 5
    },
    txtDate: {
        fontSize: 12,
        color: '#828282',
        marginBottom: 5
    },
    txtPrice: {
        fontSize: 16,
        color: '#00A9F4',
        marginBottom: 5
    },
    boxTeam: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 11
    },
    teamLogo: {
        width: 36,
        height: 36,
        marginLeft: 12,
        backgroundColor: '#bdbdbd',
        borderRadius: 18
    },
    txtTeam: {
        fontSize: 14,
        color: '#333333',
        marginLeft: 8
    },
    hr: {
        height: 1,
        alignSelf: 'center',
        width: '95%',
        backgroundColor: '#333333',
        marginBottom: 11,
        opacity: 0.25
    },
    arrowRight: {
        width: 16,
        height: 16,
        marginLeft: 10
    }
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}

export default connect(mapStateToProps, null)(SaleReportItem);
