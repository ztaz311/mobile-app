import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useSelector } from "react-redux";
import Touchable from '../../screens_view/Touchable';

import { convertLanguage } from '../../services/Helper';

function ItemTeamRequest({ data, onAcceptTeam, onDenyTeam, navigation }) {

    const language = useSelector(state => state.language)

    function getListHashTag(HashTags) {
        let result = '';
        if (HashTags.length > 0) {
            HashTags.forEach((HashTag, index) => {
                result = index === HashTags.length - 1 ? result + '#' + HashTag.HashTagName : result + '#' + HashTag.HashTagName + ' '
            })
        }
        return result;
    }

    return <View style={styles.boxRequest}>
        <View style={styles.row}>
            <Touchable onPress={() => navigation.navigate({ name: 'DetailTeam', params: { id: data.Id }, key: data.Id })}><FastImage source={{ uri: data.Logos.Medium + '' }} style={styles.avatar_team} /></Touchable>
            <View style={styles.boxContent}>
                <Text style={styles.txtInvite}>{convertLanguage(language.language, 'invited_as_a_member_by_manager', { name: data.InviteBy && data.InviteBy.Name ? data.InviteBy.Name : 'Manager' })}</Text>
                <Touchable onPress={() => navigation.navigate({ name: 'DetailTeam', params: { id: data.Id }, key: data.Id })}>
                    <Text style={styles.txtTitle} numberOfLines={1}>{data.Name}</Text>
                    <Text style={styles.txtContent} numberOfLines={1}>({data.Code})</Text>
                    <Text style={styles.txtCategory} numberOfLines={1}>{getListHashTag(data.HashTag)}</Text>
                </Touchable>
                {
                    data.isAccept &&
                    <Text style={styles.txtLabel}>{convertLanguage(language.language, 'invite_accepted_ev')}</Text>
                }
                {
                    data.isDeny &&
                    <Text style={styles.txtLabel}>{convertLanguage(language.language, 'invite_denied')}</Text>
                }
                {
                    !data.isAccept && !data.isDeny &&
                    <View style={styles.row}>
                        <Touchable style={styles.btnAccept} onPress={() => onAcceptTeam(data.Id)}>
                            <Text style={styles.txtAccept}>{convertLanguage(language.language, 'accept')}</Text>
                        </Touchable>
                        <Touchable style={styles.btnDeny} onPress={() => onDenyTeam(data.Id)}>
                            <Text style={styles.txtDeny}>{convertLanguage(language.language, 'deny')}</Text>
                        </Touchable>
                    </View>
                }
            </View>
        </View>
    </View>
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    boxContent: {
        flex: 1,
        paddingRight: 16
    },
    boxRequest: {
        marginBottom: 16
    },
    txtTitle: {
        fontSize: 16,
        color: '#333333',
        marginBottom: 5,
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 12,
        color: '#828282',
        marginBottom: 5
    },
    txtInvite: {
        color: '#00A9F4',
        fontSize: 12,
        marginBottom: 8
    },
    avatar_team: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginRight: 16
    },
    txtCategory: {
        fontSize: 14,
        color: '#333333',
        marginBottom: 5
    },
    btnAccept: {
        height: 32,
        paddingHorizontal: 16,
        borderRadius: 4,
        backgroundColor: '#00A9F4',
        marginRight: 10,
        justifyContent: 'center'
    },
    txtAccept: {
        color: '#FFFFFF',
        fontSize: 16
    },
    btnDeny: {
        height: 32,
        paddingHorizontal: 16,
        borderRadius: 4,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#4F4F4F',
        justifyContent: 'center'
    },
    txtDeny: {
        color: '#4F4F4F',
        fontSize: 16
    },
    txtLabel: {
        fontSize: 16,
        color: '#00A9F4',
        marginBottom: 5
    }
});

export default ItemTeamRequest;
