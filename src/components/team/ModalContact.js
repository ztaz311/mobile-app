import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Platform, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";
import SafeView from '../../screens_view/SafeView';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalContact extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        var team = this.props.team;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal('modalContact') }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.boxTitle}>
                            <Text style={styles.txtTitle}>{team.Name}</Text>
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'team_id')}: <Text style={styles.txtContent}>{team.Code}</Text></Text>
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'phone')}: <Text style={styles.txtContent}>{team.Phone}</Text></Text>
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'email')}: <Text style={styles.txtContent}>{team.Email}</Text></Text>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.btnReport} onPress={() => { this.props.onReport() }} >
                        <Text style={styles.txtReport}>{convertLanguage(language, 'report_to_comeup')}</Text>
                    </TouchableOpacity>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 10
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 12,
    },
    boxInput: {
        paddingBottom: 5,
    },
    txtInput: {
        fontSize: 14,
        color: '#bdbdbd',
        fontWeight: 'bold',
        marginLeft: 10
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        fontWeight: '400',
    },
    boxInputContent: {
        justifyContent: 'center',
    },
    boxInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8'
    },
    txtTitle: {
        fontSize: 20,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxTitle: {
        paddingBottom: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    btnReport: {
        height: 36,
        borderWidth: 1,
        borderColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 22,
        marginTop: 0,
        borderRadius: 5
    },
    txtReport: {
        color: '#333333',
        fontSize: 16
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalContact);