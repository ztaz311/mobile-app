import React, { Component } from 'react';
import {
    Dimensions, Linking,
    View, Text, Image,
    ScrollView, StyleSheet, ActivityIndicator
} from 'react-native';
import Touchable from '../../screens_view/Touchable'
import * as Colors from '../../constants/Colors'
import Share from 'react-native-share';
import AutoHeightWebView from 'react-native-autoheight-webview';
import * as Config from '../../constants/Config';
const { width, height } = Dimensions.get('window')
export default class ItemNews extends Component {
    constructor(props) {
        super(props);
    }
    share(team) {
        let options = {
            title: team.Name,
            message: team.Name+"\nFind and follow us in Comeup\n"+Config.MAIN_URL+"teams/"+team.Slug,
            url: Config.MAIN_URL+'teams/'+team.Slug,
            subject: "Follow us" //  for email
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }

    render() {
        const { data } = this.props
        const style= `<style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40)*9/16}} img {width: ${width - 40}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style>`
        return <View style={{marginBottom: 20}}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image source={{ uri: data.Team ? data.Team.Logo : '' }} style={{ width: 60, height: 60, borderWidth: 1, borderRadius: 30, borderColor: Colors.TEXT_S }} />
                <View style={{ marginLeft: 20 }}>
                    <Text style={{ color: Colors.TEXT_P, fontSize: 18 }}>{data.Team ? data.Team.Name : ''}</Text>
                    <Text style={{ color: Colors.TEXT_S, fontSize: 16 }}>{data.CreatedAt}</Text>
                </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.boxImages}>
                {
                    data.Images.length > 0 &&
                    data.Images.map((item2, index2) => {
                        return <Touchable style={styles.boxItem} key={index2} onPress={() => this.props.zoomImage(index2, data.Images)}>
                            <Image source={{ uri : item2.Photos.Small }} style={styles.image} />
                        </Touchable>
                    })
                }
            </ScrollView>
            {
                data.Content != '' &&
                <View style={{width: width - 40, overflow:'hidden'}}>
                    <AutoHeightWebView scalesPageToFit={false} zoomable={false} scrollEnabled={false} startInLoadingState={true} renderLoading={this.renderLoading} source={{html: style + data.Content.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com')}} originWhitelist={['*']} />
                </View>
            }
            <View style={{ flexDirection: 'row', alignSelf: 'stretch', marginTop: 12 }}>
                <Touchable
                    style={{ minWidth: 40, minHeight: 40, justifyContent: 'center' }}
                    onPress={() => this.props.onLikeTeamNews(data.Id)}>
                    <Image source={data.IsLike ? require('../../assets/liked_red.png') : require('../../assets/like_border_black.png')} style={styles.icLike} />
                </Touchable>
                <Touchable
                    onPress={() => this.props.share(data.Team)}
                    style={{ minWidth: 40, minHeight: 40, justifyContent: 'center' }}>
                    <Image source={require('../../assets/share_2.png')} style={styles.icLike} />
                </Touchable>
                {
                    data.Team.Role != 'Guest' &&
                    <Touchable
                        onPress={ () => { this.props.openModalAction(data.Id) }}
                        style={{ minWidth: 40, minHeight: 40, justifyContent: 'center', alignItems: 'flex-end', position: 'absolute', right: 0 }}>
                        <Image source={require('../../assets/more_2_contents.png')} />
                    </Touchable>
                }
            </View>
        </View>
    }
}

const styles = StyleSheet.create({
    boxImages: {
        marginBottom: 15,
        marginTop: 15
    },
    boxItem: {
        marginRight: 15,
    },
    image: {
        width: 100,
        height: 100
    },
    icLike: {
        width: 30,
        height: 30,
        marginRight: 10
    },
});