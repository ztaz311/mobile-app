import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Modal, StatusBar, Dimensions } from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import Swiper from 'react-native-swiper';
import SafeView from '../../screens_view/SafeView';
const { width } = Dimensions.get('window');
class ModalZoomImage extends Component {
    constructor(props) {
        super(props);
    }

    renderImages() {
        return this.props.images.map((image, index) => {
        // console.log(`<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body><iframe style="background-image: url(${image.Image})" frameborder="0" width="${width}" height="${width * 9 / 16}" src="${image.VideoUrl}"></iframe></body></html>`)
        return image.Type === 'VIDEO' ?
            <View style={[{ alignItems: 'center', justifyContent: 'center', flex: 1 }]} key={index}>
                <View style={{ width, height: width * 9 / 16}} >
                    <AutoHeightWebView originWhitelist={['*']} source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body><iframe style="background-image: url(${image.Image})" frameborder="0" width="${width}" height="${width * 9 / 16}" src="${image.VideoUrl}"></iframe></body></html>` }} />
                </View>
            </View>
            :
            <Image
                key={index}
                style={styles.imgZoom}
                source={{ uri: image.Image }}>
            </Image>
        });
    }
    render() {
        var team = this.props.team;
        return (
            <Modal
                visible={true}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
            >
                <SafeView style={styles.container}>
                    <StatusBar barStyle="light-content" />
                    <View style={styles.rowTitle}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Image
                                style={styles.teamThumb}
                                source={{ uri: team.Logos.Small }}>
                            </Image>
                            <View style={styles.colTeamInfo}>
                                <Text style={styles.txtTeamName}>{team.Name}</Text>
                                <Text style={styles.txtTeamTime}>{this.props.images[this.props.index].TimeUpload}</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal('modalZoom') }} >
                            <Image style={styles.iconClose} source={require('../../assets/x_icon_white.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <Swiper loop={false} showsButtons={false} index={this.props.index} showsPagination={false} loadMinimal loadMinimalSize={0}>
                            {this.renderImages()}
                        </Swiper>
                    </View>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: "#000000",
        flex: 1,
    },
    content: {
        flex: 1,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    rowTitle: {
        flexDirection: 'row',
        padding: 20,
        paddingTop: 30
    },
    imgZoom: {
        resizeMode: 'contain',
        flex: 1,
    },
    teamThumb: {
        width: 50,
        height: 50,
        resizeMode: 'cover',
        borderRadius: 25,
        marginRight: 20,
    },
    colTeamInfo: {
        flex: 1,
    //     justifyContent: 'center'
    },
    txtTeamName: {
        color: '#FFFFFF',
        fontSize: 18,
    },
    txtTeamTime: {
        color: '#FFFFFF'
    },
    iconClose: {
        width: 25,
        height: 25,
        padding: 10
    }
});

export default ModalZoomImage;