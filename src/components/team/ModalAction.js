import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Platform, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { convertLanguage } from '../../services/Helper'
import { connect } from "react-redux";
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalAction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: this.props.modalVisible,
        };
    }

    openModal(props) {
        this.props.openModal(props)
    }

    render() {
        var { language } = this.props.language
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal('modalAction') }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <TouchableOpacity style={styles.btnAction} onPress={() => { this.openModal('modalContact') }} >
                            <Text>{convertLanguage(language, 'contact')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnAction}
                            onPress={() => {
                                this.props.closeModal('modalAction')
                                this.props.openViewReport()
                            }}
                        >
                            <Text>{convertLanguage(language, 'report_to_comeup')}</Text>
                        </TouchableOpacity>
                        {
                            this.props.team.Role != 'Guest'
                                ?
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.closeModal('modalAction')
                                        this.props.updateTeam()
                                    }}
                                    style={styles.btnAction}
                                >
                                    <Text>{convertLanguage(language, 'edit_team_profile')}</Text>
                                </TouchableOpacity>
                                : null
                        }
                        {
                            this.props.team.Role != 'Guest'
                                ?
                                <TouchableOpacity style={styles.btnAction}
                                    onPress={() => {
                                        this.props.closeModal('modalAction')
                                        this.props.navigation.navigate('SaleReport')
                                    }}
                                >
                                    <Text>{convertLanguage(language, 'sales_report')}</Text>
                                </TouchableOpacity>
                                : null
                        }
                        {
                            this.props.team.Role != 'Guest' && this.props.team.Role == 'Leader'
                                ?
                                <TouchableOpacity
                                    style={styles.btnAction}
                                    onPress={() => {
                                        this.props.closeModal('modalAction')
                                        this.props.navigation.navigate('TeamDelete')
                                    }}
                                >
                                    <Text>{convertLanguage(language, 'delete_this_team')}</Text>
                                </TouchableOpacity>
                                : null
                        }
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        //   flex: 1,
        //   justifyContent: "center",
        //   alignItems: "center"
    },
    btnAction: {
        padding: 12,
        margin: 4,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "#000000",
        borderWidth: 1,
    },
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalAction);