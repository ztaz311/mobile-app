import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalRegisterTeam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: ''
        }
    }


    render() {
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                onBackdropPress={() => this.props.closeModal()}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <Image source={require('../../assets/arrow_circle_up.png')} style={{width: 96, height: 96, marginBottom: 16, alignSelf: 'center'}} />
                        <Text style={styles.txtTitle}>{'Welcome back to ComeUp, We have just updated. You now have a new team to manage your old events maybe new one in the future. However, we encourage you to go to your team settings page to update new information about your team for better expierence & support. Best regards, ComeUp Team.'}</Text>
                        <TouchableOpacity style={styles.btnTeam} onPress={() => this.props.onGoToSetting()}>
                            <Text style={styles.txtTeam}>Go To Team Settings</Text>
                            <Image source={require('../../assets/arrow_right.png')} style={styles.arrow_right} />
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5,
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center',
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    boxListCategory: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    btnCategory: {
        width: (width - 110) / 3,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 15,
        borderColor: '#00acfb'
    },
    txtCountry: {
        fontSize: 17,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    txtTitle: {
        fontSize: 16,
        color: '#333333',
        marginBottom: 20,
        textAlign: 'center'
    },
    btnTeam: {
        width: '100%',
        backgroundColor: '#00A9F4',
        borderRadius: 4,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 16,
        height: 36
    },
    txtTeam: {
        fontSize: 16,
        color: '#FFFFFF',
    },
    arrow_right: {
        width: 18,
        height: 18,
        marginLeft: 10
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalRegisterTeam);