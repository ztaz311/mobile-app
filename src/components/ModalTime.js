import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import DatePicker from 'react-native-datepicker';
import * as Colors from '../constants/Colors'
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalTime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: '',
            TimeStart: '',
            TimeFinish: '',
            data: {},
        }
    }

    onSelectTime(index, time) {
        this.setState({ current: index, data: time })
        if (index !== 5) {
            this.props.selectTime(time)
        }
    }

    onSelectFinish(TimeFinish) {
        this.setState({ TimeFinish })
        var { data } = this.state;
        data.start = this.state.TimeStart;
        data.finish = TimeFinish;
        this.props.selectTime(data)
    }


    render() {
        var { times } = this.props;
        var { TimeStart, TimeFinish } = this.state;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.container}>
                        <View style={styles.content}>
                            <Text style={[styles.txtCountry, { borderBottomWidth: 0 }]}>{convertLanguage(language, 'select_a_schedule')}</Text>
                            {
                                times.map((item, index) => {
                                    return <React.Fragment key={index}>
                                        <Text style={styles.txtCity} onPress={() => this.onSelectTime(index, item)}>{item.name_show}</Text>
                                    </React.Fragment>
                                })
                            }
                            {
                                this.state.current === 5 &&
                                <View style={styles.boxDate}>
                                    <DatePicker
                                        style={{ flex: 0.45 }}
                                        date={TimeStart}
                                        mode="date"
                                        placeholder={convertLanguage(language, 'start')}
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        iconComponent={<Image style={{ position: 'absolute', right: 0 }} source={require('../assets/calendar_icon.png')} />}
                                        customStyles={{
                                            dateInput: {
                                                borderBottomColor: '#bdbdbd',
                                                borderBottomWidth: 1,
                                                borderLeftWidth: 0,
                                                borderRightWidth: 0,
                                                borderTopWidth: 0,
                                            },
                                            dateText: {
                                                color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                            },
                                            btnTextConfirm: {
                                                color: Colors.PRIMARY, fontSize: 17
                                            },
                                            btnTextCancel: {
                                                color: Colors.TEXT_P, fontSize: 16
                                            },
                                            placeholderText: {
                                                alignSelf: 'flex-start',
                                                fontSize: 15,
                                                color: '#bdbdbd'
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(TimeStart) => this.setState({ TimeStart, TimeFinish: '' })}
                                    />
                                    <Text style={styles.txtCenter}>~</Text>
                                    <DatePicker
                                        style={{ flex: 0.45 }}
                                        date={TimeFinish}
                                        mode="date"
                                        placeholder={convertLanguage(language, 'finish')}
                                        format="YYYY-MM-DD"
                                        minDate={TimeStart}
                                        disabled={TimeStart === '' ? true : false}
                                        // maxDate="01-01-2010"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        iconComponent={<Image style={{ position: 'absolute', right: 0 }} source={require('../assets/calendar_icon.png')} />}
                                        customStyles={{
                                            dateInput: {
                                                borderBottomColor: '#bdbdbd',
                                                borderBottomWidth: 1,
                                                borderLeftWidth: 0,
                                                borderRightWidth: 0,
                                                borderTopWidth: 0,
                                            },
                                            dateText: {
                                                color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                            },
                                            btnTextConfirm: {
                                                color: Colors.PRIMARY, fontSize: 17
                                            },
                                            btnTextCancel: {
                                                color: Colors.TEXT_P, fontSize: 16
                                            },
                                            placeholderText: {
                                                alignSelf: 'flex-start',
                                                fontSize: 15,
                                                color: '#bdbdbd'
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(TimeFinish) => this.onSelectFinish(TimeFinish)}
                                    />
                                </View>
                            }
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 5,
        // flex: 1,
        // height: 350
    },
    container: {
        // flex: 1
    },
    content: {
        padding: 22,
        // justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center',
        // flex: 1
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    },
    boxDate: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtCenter: {
        flex: 0.1,
        color: '#757575',
        fontSize: 28,
        textAlign: 'center'
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default React.memo(connect(mapStateToProps, null)(ModalTime));