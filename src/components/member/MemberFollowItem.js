import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import * as Colors from '../../constants/Colors';

class MemberFollowItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalLeave: false,
            modalSetRole: false,
        };
    }

    render() {
        var member = this.props.member;
        return (
            <View style={styles.container}>
                <View style={styles.colThumb}>
                    <Image
                        style={styles.imgThumb}
                        source={member.Avatars ? { uri: member.Avatars.Small } : require('../../assets/profile_default.png')}>
                    </Image>
                </View>
                <View style={styles.colInfo}>
                    <Text style={styles.txtTitle}>{member.Name}</Text>
                    <Text style={styles.txtSmall}>{member.UserId}</Text>
                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        //   flex: 1,
        flexDirection: 'row',
        marginBottom: 40
    },
    colThumb: {
        flex: 0.3,
    },
    rowContent: {
        flexDirection: 'row',
    },
    colInfo: {
        flex: 1,
        paddingLeft: 20,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    imgThumb: {
        resizeMode: 'cover',
        height: 70,
        width: 70,
        borderRadius: 35
    },
    colAction: {
        flex: 0.4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtSmall: {
        fontSize: 14,
        color: Colors.GRAY,
        color: '#757575'
    },
    imgLogoutTeam: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    btnInvite: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 5,
        backgroundColor: Colors.PRIMARY
    },
    btnInviteDisable: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 5,
        backgroundColor: Colors.GRAY
    },
    txtBtnInvite: {
        color: '#FFFFFF',
        alignSelf: 'center'
    }
});

export default MemberFollowItem;