import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet } from 'react-native';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
class MemberInviteItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalLeave: false,
            modalSetRole: false,
        };
    }

    render() {
        var { language } = this.props.language;
        var member = this.props.member;
        return (
            <View style={styles.container}>
                <View style={styles.colThumb}>
                    <Image
                        style={styles.imgThumb}
                        source={member.Avatars ? { uri: member.Avatars.Small } : require('../../assets/profile_default.png')}>
                    </Image>
                </View>
                <View style={styles.colInfo}>
                    <Text style={styles.txtTitle}>{member.Name}</Text>
                    <Text style={styles.txtSmall}>{member.UserId}</Text>
                </View>
                <View style={styles.colAction}>
                    {
                        member.Role == 'Invite' || member.Role == 'Invited'
                            ?
                            <TouchableOpacity style={[styles.btnInvite, member.Role == 'Invited' ? styles.btnInviteDisable : {}]} disabled={member.Role == 'Invited'} onPress={() => { member.Role == 'Invite' ? this.props.invitedMember() : null }}>
                                <Text style={styles.txtBtnInvite}>{member.Role == 'Invited' ? convertLanguage(language, 'invited') : convertLanguage(language, 'invite')}</Text>
                            </TouchableOpacity>
                            : null
                    }

                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        //   flex: 1,
        flexDirection: 'row',
        marginBottom: 40
    },
    colThumb: {
        flex: 0.3,
    },
    rowContent: {
        flexDirection: 'row',
    },
    colInfo: {
        flex: 1,
        paddingLeft: 20,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    imgThumb: {
        resizeMode: 'cover',
        height: 70,
        width: 70,
        borderRadius: 35
    },
    colAction: {
        flex: 0.4,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtSmall: {
        fontSize: 14,
        color: Colors.GRAY,
        color: '#757575'
    },
    imgLogoutTeam: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    btnInvite: {
        paddingTop: 8,
        paddingBottom: 8,
        borderRadius: 5,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.PRIMARY
    },
    btnInviteDisable: {
        paddingTop: 8,
        paddingBottom: 8,
        borderRadius: 5,
        backgroundColor: Colors.GRAY
    },
    txtBtnInvite: {
        color: '#FFFFFF',
        alignSelf: 'center'
    }
});
const mapStateToProps = state => {
    return {
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    
}
export default connect(mapStateToProps, null)(MemberInviteItem);