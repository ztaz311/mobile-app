import React, {Component} from 'react';
import {Text, TouchableOpacity, View, Image, StyleSheet} from 'react-native';
import * as Colors from '../../constants/Colors';

import ModalLeaveTeam from '../team/ModalLeaveTeam';
import ModalSetRole from '../team/ModalSetRole';

class MemberTeamItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalLeave: false,
            modalSetRole: false,
        };
    }
    openModel(props) {
        this.setState({ [props]: true });
    }
    closeModal(props) {
        this.setState({ [props]: false });
    }

    render() {
        var member = this.props.member;
        var auth = this.props.auth;
        var team = this.props.team;
      return (
        <View style={styles.container}>
            <View style={styles.colThumb}>
                <Image
                    style={styles.imgThumb}
                    source={member.Avatars ? { uri: member.Avatars.Small } : require('../../assets/profile_default.png')}>
                </Image>
            </View>
            <View style={styles.colInfo}>
                <Text style={styles.txtTitle}>{ member.Name }</Text>
                <Text style={styles.txtSmall}>{member.UserId}</Text>
                <Text style={styles.txtSmall}>{member.Role !== 'Invited' ? member.Role : ''}</Text>
            </View>
            <View style={styles.colAction}>
                {
                    member.Id == auth.Id?
                        <TouchableOpacity onPress={ () => {this.openModel('modalLeave')} }>
                            <Image style={styles.imgLogoutTeam} source={require('../../assets/leave_icon.png')} />
                        </TouchableOpacity>
                    : member.Role == 'Leader' &&  member.Id != auth.Id ?
                        <TouchableOpacity onPress={ () => {this.openModel('modalSetRole')} }>
                            <Image style={styles.imgLogoutTeam} source={require('../../assets/set_role.png')} />
                        </TouchableOpacity>
                    : member.Role == 'Director' ?
                        <TouchableOpacity onPress={ () => {this.openModel('modalSetRole')} }>
                            <Image style={styles.imgLogoutTeam} source={require('../../assets/set_role.png')} />
                        </TouchableOpacity>
                    : member.Role == 'Manager' ?
                        <TouchableOpacity onPress={ () => {this.openModel('modalSetRole')} }>
                            <Image style={styles.imgLogoutTeam} source={require('../../assets/set_role.png')} />
                        </TouchableOpacity>
                    : member.Role == 'Staff' ?
                        <TouchableOpacity onPress={ () => {this.openModel('modalSetRole')} }>
                            <Image style={styles.imgLogoutTeam} source={require('../../assets/set_role.png')} />
                        </TouchableOpacity>
                    : member.Role == 'Invited' && (team.Role === 'Leader' || team.Role === 'Director') ?
                        <TouchableOpacity onPress={ () => {this.props.invitedMember()} }>
                            <Image style={styles.imgLogoutTeam} source={require('../../assets/x_cancel.png')} />
                        </TouchableOpacity>
                    : null
                }
            </View>
            {
                this.state.modalLeave
                ?
                    <ModalLeaveTeam
                        modalVisible={this.state.modalLeave}
                        closeModal={(value) => {this.closeModal(value)}}
                        leaveTeam={() => {this.props.leaveTeam()}}
                    />
                : null
            }
            
            {
                this.state.modalSetRole
                ?
                    <ModalSetRole
                        modalVisible={this.state.modalSetRole}
                        closeModal={(value) => {this.closeModal(value)}}
                        setTeamRole={(value) => {this.props.setTeamRole(value)}}
                        TeamRole={this.props.team.Role}
                        MemberRole={member.Role}
                        leaveTeam={() => {this.props.leaveTeam()}}
                    />
                : null
            }

        </View>
      );
    }
  }
  const styles = StyleSheet.create({
    container: {
    //   flex: 1,
      flexDirection: 'row',
      marginBottom: 40
    },
    colThumb: {
        flex: 0.3,
    },
    rowContent: {
        flexDirection: 'row',
    },
    colInfo: {
        flex: 1,
        paddingLeft: 20,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    imgThumb: {
        resizeMode: 'cover',
        height: 70,
        width: 70,
        borderRadius: 35
    },
    colAction: {
        flex: 0.2,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    txtTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtSmall: {
        fontSize: 14,
        color: Colors.GRAY,
        color: '#757575'
    },
    imgLogoutTeam: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
  });

  export default MemberTeamItem;