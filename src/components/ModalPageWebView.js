import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions, ActivityIndicator, Linking } from 'react-native';
const { width, height } = Dimensions.get('window')
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
import AutoHeightWebView from 'react-native-autoheight-webview';
import Touchable from '../screens_view/Touchable';
import { Modalize } from 'react-native-modalize';
class ModalPageWebView extends Component {
    modal = React.createRef();
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        if (this.modal.current) {
            this.modal.current?.open();
            this.props.navigation.setParams({ visible: true })
        }
    }

    closeModal = () => {
        if (this.modal.current) {
            this.modal.current.close();
            this.props.navigation.setParams({ visible: false })
        }
    };

    renderLoading() {
        return <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color="#000000" />
        </View>
    }

    render() {
        var { language } = this.props.language;
        return (
            <Modalize
                ref={this.modal}
                scrollViewProps={{
                    showsVerticalScrollIndicator: false,
                    stickyHeaderIndices: [0],
                }}
                // withReactModal
                modalHeight={height-64}
                onClosed={() => this.props.closeModal()}
            >
                <View style={styles.boxInfo}>
                    <View style={styles.boxRow}>
                        <View style={styles.boxClose}></View>
                        <Text style={styles.txtTitle}>{this.props.title}</Text>
                        <Touchable style={styles.boxClose} onPress={this.closeModal}>
                            <Image source={require('../assets/close.png')} style={styles.icClose} />
                        </Touchable>
                    </View>
                </View>
                <View style={styles.content}>
                    <AutoHeightWebView
                        ref={(c) => { this.webview = c }}
                        source={{ uri: this.props.uri+'?lang='+language }}
                        startInLoadingState
                        renderLoading={this.renderLoading}
                        style={{ width: width - 30 }}
                        onNavigationStateChange={(event) => {
                            if (event.url !== this.props.uri+'?lang='+language) {
                                this.webview.stopLoading();
                                Linking.openURL(event.url);
                            }
                        }}
                    />
                </View>
            </Modalize>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        paddingLeft: 15,
        paddingRight: 15,
        flex: 1
    },
    boxRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInfo: {
        flexDirection: 'row',
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.85)',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    boxClose: {
        width: 40,
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    icClose: {
        width: 30,
        height: 30
    },
    txtTitle: {
        fontSize: 20,
        color: '#333333',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 13,
        color: '#333333',
        textAlign: 'justify',
        marginBottom: 20
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalPageWebView);