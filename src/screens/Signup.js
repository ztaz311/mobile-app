import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet } from 'react-native';
import Toast from 'react-native-simple-toast';
import firebase from 'react-native-firebase';
import SafeView from '../screens_view/SafeView'
import ScrView from '../screens_view/ScrView'
import Touchable from '../screens_view/Touchable'
import { convertLanguage } from '../services/Helper'
import { connect } from "react-redux";
import * as Colors from '../constants/Colors'
import * as Config from '../constants/Config';
import ModalPageWebView from '../components/ModalPageWebView';
class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            //
            username: '',
            name: '',
            passwd: '',
            // errors
            err_username: '',
            err_name: '',
            err_passwd: '',
            modalPageWebView: false,
            uri: '',
            title: ''
        };
    }

    shouldLogin = () => {
        return (!this.state.loading && this.state.username && this.state.name && this.state.passwd)
    }

    signup = () => {
        const { username, name, passwd } = this.state
        let formdata = new FormData();
        formdata.append("Email", username)
        formdata.append("Name", name)
        formdata.append("Password", passwd)

        this.setState({ loading: true, err_username: '', err_name: '', err_passwd: '' })
        fetch(Config.API_URL + '/users/register', {
            method: 'post',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: formdata
        }).then(async (response) => {
            if (response.status == 200) {
                response = await response.json()
                if (response.status == 200) {
                    firebase.analytics().logEvent('sign_up', { username: response.data?.UserId, device: 'Mobile App', fullname: response.data?.Name, member_id: response.data?.Id });
                    Toast.showWithGravity(response.messages, Toast.LONG, Toast.TOP)
                    const { navigation } = this.props
                    this.props.route.params['cb'](this.state.username, this.state.passwd)
                    navigation.goBack()
                } else {
                    if (response.errors) {
                        this.setState(state => {
                            state.err_username = response.errors.Email ? response.errors.Email : ''
                            state.err_name = response.errors.Name ? response.errors.Name : ''
                            state.err_passwd = response.errors.Password ? response.errors.Password : ''
                        })
                        // Toast.showWithGravity(JSON.stringify(response.errors), Toast.LONG, Toast.TOP)
                    }
                    this.setState({ loading: false })
                }
            } else {
                alert('Cannot connect to server.')
                this.setState({ loading: false })
            }
        }).catch(err => {
            console.log(err)
            this.setState({ loading: false })
        })
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ flex: 1, backgroundColor: Colors.BG }}>
                <Touchable
                    onPress={() => this.props.navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, alignSelf: 'flex-start', alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                </Touchable>

                <ScrView>
                    <Text style={{ color: Colors.TEXT_P, fontSize: 28, fontWeight: 'bold', marginLeft: 20, marginTop: 20 }}>{convertLanguage(language, 'create_your_account')}</Text>
                    <View style={[styles.boxInput]}>
                        <Text style={styles.txtInput}>{convertLanguage(language, 'email')}</Text>
                        <TextInput
                            style={[styles.ipContent, this.state.err_username !== '' ? { borderBottomColor: '#ff4081' } : {}]}
                            selectionColor="#bcbcbc"
                            value={this.state.username}
                            onChangeText={(username) => this.setState({ username, err_username: '' })}
                            keyboardType={'email-address'}
                            autoCapitalize='none'
                        />
                        {
                            this.state.err_username !== '' &&
                            <Text style={styles.txtError}>{this.state.err_username}</Text>
                        }
                    </View>
                    <View style={[styles.boxInput]}>
                        <Text style={styles.txtInput}>{convertLanguage(language, 'name')}</Text>
                        <TextInput
                            style={[styles.ipContent, this.state.err_name !== '' ? { borderBottomColor: '#ff4081' } : {}]}
                            selectionColor="#bcbcbc"
                            value={this.state.name}
                            onChangeText={(name) => this.setState({ name, err_name: '' })}
                        />
                        {
                            this.state.err_name !== '' &&
                            <Text style={styles.txtError}>{this.state.err_name}</Text>
                        }
                    </View>
                    <View style={[styles.boxInput, this.state.err_passwd !== '' ? { borderBottomColor: '#ff4081' } : {}]}>
                        <Text style={styles.txtInput}>{convertLanguage(language, 'password')}</Text>
                        <TextInput
                            style={[styles.ipContent, this.state.err_username !== '' ? { borderBottomColor: '#ff4081' } : {}]}
                            selectionColor="#bcbcbc"
                            value={this.state.passwd}
                            onChangeText={(passwd) => this.setState({ passwd, err_passwd: '' })}
                            secureTextEntry={true}
                        />
                        {
                            this.state.err_passwd !== '' &&
                            <Text style={styles.txtError}>{this.state.err_passwd}</Text>
                        }
                    </View>

                    <Touchable
                        onPress={this.signup}
                        disabled={!this.shouldLogin()}
                        style={{
                            minHeight: 56, borderColor: this.shouldLogin() ? Colors.PRIMARY : Colors.DISABLE, borderWidth: 1, borderRadius: 4,
                            marginLeft: 20, marginRight: 20, marginTop: 32, justifyContent: 'center', alignItems: 'center',
                            backgroundColor: this.shouldLogin() ? Colors.PRIMARY : '#FFFFFF'
                        }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', color: this.shouldLogin() ? '#FFFFFF' : Colors.DISABLE }}>{this.state.loading ? convertLanguage(language, 'waiting') : convertLanguage(language, 'sign_up')}</Text>
                    </Touchable>

                    <Text
                        style={{ alignSelf: 'center', fontWeight: 'normal', fontSize: 15, marginTop: 10, color: '#757575' }}>
                        {convertLanguage(language, 'by_signing')}</Text>

                    <View style={{ alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: '#757575', flexDirection: 'row' }}>
                        <Touchable
                            onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/terms-of-service`, title: convertLanguage(language, 'terms_of_service') })}>
                            <Text
                                style={{ textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: Colors.PRIMARY }}>
                                {convertLanguage(language, 'terms_of_service')}</Text>
                        </Touchable>
                        <Text style={{ alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: '#757575' }}>  &  </Text>
                        <Touchable
                            onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/privacy-policy`, title: convertLanguage(language, 'privacy_policy') })}>
                            <Text
                                style={{ textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: Colors.PRIMARY }}>
                                {convertLanguage(language, 'privacy_policy')}</Text>
                        </Touchable>
                    </View>
                </ScrView>
                {
                    this.state.modalPageWebView
                        ?
                        <ModalPageWebView
                            closeModal={() => this.setState({ modalPageWebView: false })}
                            navigation={this.props.navigation}
                            uri={this.state.uri}
                            title={this.state.title}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    boxInput: {
        paddingBottom: 24,
        margin: 20,
        marginTop: 10,
        marginBottom: 10
    },
    txtInput: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    ipContent: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 12,
        paddingBottom: 12,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    txtError: {
        color: '#ff4081',
        fontSize: 12,
        marginTop: 2
    }
})

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
export default connect(mapStateToProps, null)(Signup);
