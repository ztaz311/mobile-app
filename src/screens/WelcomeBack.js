import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import Touchable from '../screens_view/Touchable';
import ImagePicker from 'react-native-image-crop-picker';
import * as Colors from '../constants/Colors';
import Toast from 'react-native-simple-toast';
import { actUpdateAvatar } from '../actions/user';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import SafeView from '../screens_view/SafeView';
import FastImage from 'react-native-fast-image';
class WelcomeBack extends Component {
    constructor(props) {
        super(props);

        const { Avatars, Avatar, newUser } = this.props.user.profile;
        this.state = {
            avatar: Avatars ? Avatars.Small : '',
            avatars: Avatars ? Avatars : {},
            avatarSource: Avatar != '' ? { uri: Avatars.Small } : require('../assets/profile_default_2.png'),
            avatarBase64: '',
            is_change_avatar: false,
            newUser: newUser ? newUser : false
        };
    }

    pickAvatar = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then(image => {
            this.setState({
                is_change_avatar: true,
                avatarBase64: 'data:image/png;base64,' + image.data,
            })
            var body = {
                AvatarBase: 'data:image/png;base64,' + image.data
            }
            this.props.updateAvatar(body)
        }).catch(e => { });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.user) {
            var profile = nextProps.user.profile;
            this.setState({
                avatar: profile.Avatar ? profile.Avatar : '',
                avatars: profile.Avatars ? profile.Avatars : {},
                avatarSource: profile.Avatar != '' ? { uri: profile.Avatars.Small } : require('../assets/profile_default_2.png'),
            })
        }
    }

    render() {
        var { language } = this.props.language;
        var { UserId, Email, Name } = this.props.user.profile;
        var { newUser } = this.state;
        return (
            <SafeView style={{ flex: 1, backgroundColor: Colors.BG, justifyContent: 'center', alignItems: 'center' }}>
                {/* <Touchable
                    onPress={() => this.props.navigation.replace('Login')}
                    style={{ minWidth: 50, minHeight: 50, marginLeft: 20, marginTop: 20 }}>
                    <Image source={require('../assets/icon_back.png')} />
                </Touchable> */}

                <Text style={{ fontSize: 27, color: Colors.TEXT_P, fontWeight: 'bold' }}>{newUser ? convertLanguage(language, 'welcome_to_join_us') : convertLanguage(language, 'welcome_back')}</Text>
                <Text style={{ fontSize: 27, color: Colors.PRIMARY }}>{Name}</Text>
                <Touchable onPress={this.pickAvatar}>
                    <FastImage source={this.state.is_change_avatar ? { uri: this.state.avatarBase64 } : this.state.avatarSource} style={{ margin: 20, width: 100, height: 100, borderRadius: 50, backgroundColor: '#bdbdbd' }} />
                </Touchable>
                <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }}>
                    <Text style={{ flex: 1, fontSize: 17, color: Colors.TEXT_P, textAlign: 'right' }}>{convertLanguage(language, 'id')}</Text>
                    <View style={{ flex: 3, marginLeft: 20 }}>
                        {UserId ?
                            <Text style={{ fontSize: 17, color: Colors.TEXT_P, fontWeight: 'bold' }}>{UserId}</Text>
                            :
                            <Touchable
                                onPress={() => this.props.navigation.navigate('EditProfile')}
                                style={{
                                    justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 4, borderColor: Colors.PRIMARY,
                                    paddingLeft: 10, paddingRight: 10, minHeight: 35,
                                    alignSelf: 'flex-start'
                                }}>
                                <Text style={{ color: Colors.PRIMARY }}>Set ID</Text>
                            </Touchable>}
                    </View>
                </View>
                {
                    Email &&
                    <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                        <Text style={{ flex: 1, fontSize: 17, color: Colors.TEXT_P, textAlign: 'right' }}>{convertLanguage(language, 'email')}</Text>
                        <Text style={{ flex: 3, marginLeft: 20, fontSize: 17, color: Colors.TEXT_P, fontWeight: 'bold' }}>{Email}</Text>
                    </View>
                }

                <Touchable
                    onPress={() => this.props.route && this.props.route.params && this.props.route.params['type'] ? this.props.navigation.goBack() : this.props.navigation.replace('Main')}
                    style={{
                        marginTop: 40,
                        marginLeft: 20, marginRight: 20,
                        alignSelf: 'stretch',
                        minHeight: 50, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                        justifyContent: 'center', alignItems: 'center',
                        backgroundColor: Colors.PRIMARY
                    }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#FFFFFF' }}>{convertLanguage(language, 'ok')}</Text>
                </Touchable>
            </SafeView>
        );
    }
}

const mapStateToProps = state => {
    return {
        language: state.language,
        user: state.user
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        updateAvatar: (body) => {
            dispatch(actUpdateAvatar(body))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WelcomeBack);
