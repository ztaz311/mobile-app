import React, { Component } from 'react';
import { View, Text, Button, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from '@react-native-community/google-signin';
import Touchable from '../screens_view/Touchable';
import * as Colors from '../constants/Colors';

import { actLogout } from '../actions/user';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
class My extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: null,
            profile: this.props.user.profile
        };
        this.logout = this.logout.bind(this);
    }

    componentWillMount() {
        // this.getData()
    }

    logout = () => {
        LoginManager.logOut()
        GoogleSignin.revokeAccess();
        GoogleSignin.signOut()
        let arr = []
        arr.push(['auKey', ''])
        arr.push(['auValue', ''])
        AsyncStorage.multiSet(arr, () => this.props.navigation.replace('Login'))
        this.props.getLogout(this.props.navigation);
    }

    // updateState(data) {
    //     var { profile } = this.state;
    //     profile.UserId = data.UserId;
    //     profile.Email = data.Email;
    //     profile.Name = data.Name;
    //     profile.Zipcode = data.Zipcode;
    //     profile.Phone = data.Phone;
    //     profile.Avatars = data.Avatars;
    //     profile.Birthday = data.Birthday;
    //     profile.Gender = data.Gender;
    //     profile.Language = data.Language;
    //     this.setState({profile})
    // }


    render() {
        var { profile } = this.props.user;
        var { language } = this.props.language;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF' }}>
                {
                    profile.Id ?
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{ fontSize: 27, color: Colors.PRIMARY, textAlign: 'center' }}>{profile.Name}</Text>
                            <Touchable onPress={() => this.props.navigation.navigate('EditProfile', {data: profile})}>
                                <Image source={{uri: profile.Avatars.Small}} style={{ margin: 20, width: 100, height: 100, borderRadius: 50 }} />
                            </Touchable>
                            <View style={{ flexDirection: 'row', marginTop: 20}}>
                                <Text style={{ flex: 1, fontSize: 17, color: Colors.TEXT_P, textAlign: 'right' }}>{convertLanguage(language, 'id')}</Text>
                                <View style={{ flex: 3, marginLeft: 20 }}>
                                    {profile.UserId ?
                                        <Text style={{ fontSize: 17, color: Colors.TEXT_P, fontWeight: 'bold' }}>{profile.UserId}</Text>
                                        :
                                        <Touchable
                                            onPress={() => this.props.navigation.navigate('EditProfile', {data: profile})}
                                            style={{
                                                justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 4, borderColor: Colors.PRIMARY,
                                                paddingLeft: 10, paddingRight: 10, minHeight: 35,
                                                alignSelf: 'flex-start'
                                            }}>
                                            <Text style={{ color: Colors.PRIMARY }}>Set ID</Text>
                                        </Touchable>}
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <Text style={{ flex: 1, fontSize: 17, color: Colors.TEXT_P, textAlign: 'right' }}>{convertLanguage(language, 'email')}</Text>
                                <Text style={{ flex: 3, marginLeft: 20, fontSize: 17, color: Colors.TEXT_P, fontWeight: 'bold' }}>{profile.Email}</Text>
                            </View>
                            <Touchable
                                onPress={ this.logout }
                                style={{ alignSelf: 'center', borderRadius: 4, marginTop: 20, marginBottom: 20, paddingLeft: 40, paddingRight: 40, paddingTop: 10, paddingBottom: 10, backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>{convertLanguage(language, 'logout')}</Text>
                            </Touchable>
                        </View>
                        
                    :
                    <Touchable
                        onPress={() => this.props.navigation.navigate('Login')}
                        style={{ alignSelf: 'center', borderRadius: 4, marginTop: 20, marginBottom: 20, paddingLeft: 40, paddingRight: 40, paddingTop: 10, paddingBottom: 10, backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>{convertLanguage(language, 'login')}</Text>
                    </Touchable>
                }
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getLogout: (navigation) => {
            dispatch(actLogout(navigation))
        },
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(My);
