import React, { Component } from 'react';
import {
    FlatList, View, Text,
    ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
// params: api, renderItem
export default class List extends Component {
    // extend me!
    api = () => ""
    renderItem = (item, index) => null

    exportResponse = (responseJson) => responseJson
    renderSep = () => null
    titleEmpty = () => "List is empty!"
    renderHeader = () => null
    //

    state = {
        data: [],
        isLoading: false,
        isEmpty: false,
        isRefreshing: false
    }

    componentWillMount() { this.getData() }

    page = 0
    getData = async () => {
        if (!this.api()) {
            alert('API CANNOT EMPTY!')
            return
        }

        if (this.state.isLoading || this.state.isEmpty) return
        else this.setState({ isLoading: true })

        this.page++
        let api = this.api() + '?page=' + this.page
        try {
            if (!this.headers) {
                const auKey = await AsyncStorage.getItem('auKey')
                const auValue = await AsyncStorage.getItem('auValue')
                this.headers = {}
                this.headers[auKey] = auValue
            }
            let response = await fetch(
                api, this.headers
            );
            response = await response.json();

            if (response.data.length == 0) {
                this.setState({ isEmpty: true })
            } else {
                let { data } = this.state
                data = data.concat(response.data)
                this.setState({ data })
            }
            this.setState({ isLoading: false, isRefreshing: false })
        } catch (error) {
            this.setState({ isLoading: false, isRefreshing: false })
        }
    }

    refresh = () => {
        this.page = 0
        this.setState({
            data: [],
            isLoading: false,
            isEmpty: false
        })
    }

    renderList() {
        let { data, isEmpty } = this.state

        return (
            <View style={{ flex: 1 }}>
                {(data.length == 0 && isEmpty) ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        {
                            (this.state.isLoading && this.page == 1) ?
                                <ActivityIndicator />
                                :
                                <Text style={Styles.textHint}>{this.titleEmpty()}</Text>
                        }
                    </View>
                    :
                    <FlatList
                        style={{ flex: 1, width: '100%' }}
                        data={[...data, { end: !isEmpty }]}
                        renderItem={({ item, index }) => {
                            if (item.end == undefined)
                                return this.renderItem(item, index)
                            else if (item.end) {
                                return <ActivityIndicator style={{ padding: 8 }} />
                            } else return null
                        }}
                        onEndReached={this.getData}
                        ListHeaderComponent={this.renderHeader}
                        ItemSeparatorComponent={this.renderSep}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.refresh}
                    />}
            </View>
        );
    }
}
