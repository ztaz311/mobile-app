import React, { Component } from 'react';
import { View, Image, Text, ActivityIndicator, Dimensions, TextInput, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import SafeView from '../screens_view/SafeView';
import Touchable from '../screens_view/Touchable';
import Line from '../screens_view/Line';
import ScrView from '../screens_view/ScrView';
import PickerItem from '../screens_view/PickerItem';
import * as Colors from '../constants/Colors';
import ImagePicker from 'react-native-image-crop-picker';
import SimpleToast from 'react-native-simple-toast';
import { actTeamDetail, actCreateTeam, actUpdateTeam } from '../actions/team';
import { convertLanguage } from '../services/Helper'
const { width, height } = Dimensions.get('window')
import { connect } from "react-redux";
import * as Config from '../constants/Config';
import Popover from 'react-native-popover-view';

class RegisterTeam extends Component {
    constructor(props) {
        super(props);
        const { navigation } = this.props
        let data = null
        if (this.props.route && this.props.route.params)
            data = this.props.route.params['data']
        this.state = {
            loadingData: true,
            loadingGetCode: false,
            loadingCheckCode: false,
            loading: false,
            loadingSave: false,
            //
            data: data,
            avatarSource: data ? { uri: data.Logos.Small } : require('../assets/team_default.png'),
            avatarBase64: '',
            name: data ? data.Name : '',
            id: data && data.isCode ? data.Code : '',
            teamId: data ? data.Id : '',
            Zipcode: data ? data.Zipcode : this.props.global.country.Zipcode,
            phone: data ? data.Phone : '',
            Code: '',
            isCode: data && data.isCode ? data.isCode : false,
            email: data ? data.Email : '',
            emailEditable: true,
            country: data ? parseInt(data.CountryId) : this.props.global.country.Id,
            arrHeadPhone: [],
            arrCountry: [],
            arrSpecialty: data ? data.HashTag : [],
            // err
            err_name: '',
            err_id: '',
            err_phone: '',
            err_code: '',
            err_email: '',
            is_change_avatar: false,
            isVisible: false,
            is_send_code: false,
            countdown: 0,
            is_verify: false,
            AvatarCheck: data ? true : false
        };
    }

    componentDidMount() {
        this.getData()
    }

    getData = async () => {
        let data = null
        if (this.props.route && this.props.route.params)
            data = this.props.route.params['data']
        const auKey = await AsyncStorage.getItem('auKey')
        const auValue = await AsyncStorage.getItem('auValue')
        let headers = {}
        headers[auKey] = auValue

        fetch(Config.API_URL + '/countries', {
            headers: headers
        })
            .then(async (response) => {
                if (response.status == 200) {
                    response = await response.json()
                    if (response.status == 200) {
                        let { arrHeadPhone, arrCountry } = this.state
                        response.data.forEach((e, index) => {
                            arrHeadPhone.push({
                                name: e.Zipcode,
                                value: e.Zipcode
                            })
                            arrCountry.push({
                                name: e.Name,
                                value: e.Id
                            })
                        })
                        this.setState({ arrHeadPhone, arrCountry })
                    }
                    this.setState({ loadingData: false })
                } else {
                    alert('Cannot connect to server.')
                    this.setState({ loadingData: false })
                }
            }).catch(err => {
                this.setState({ loadingData: false })
            })
    }

    getCode = async () => {
        this.setState({ loadingGetCode: true })
        const auKey = await AsyncStorage.getItem('auKey')
        const auValue = await AsyncStorage.getItem('auValue')
        let headers = {}
        headers[auKey] = auValue
        headers['Content-Type'] = 'multipart/form-data'

        let formdata = new FormData();
        formdata.append("Zipcode", this.state.Zipcode)
        formdata.append("Phone", this.state.phone)
        console.log(formdata)
        fetch(Config.API_URL + '/set-code', {
            method: 'post',
            headers: headers,
            body: formdata
        }).then(async (response) => {
            if (response.status == 200) {
                response = await response.json()
                console.log(response)
                if (response.status == 200) {
                    this.setState({ is_send_code: true });
                    await this.setState({ countdown: 60 });
                    var x = setInterval(() => {
                        this.setState((prevstate) => ({ countdown: prevstate.countdown - 1 }));
                        if (this.state.countdown === 0) {
                            clearInterval(x);
                        }
                    }, 1000);
                } else {
                    SimpleToast.showWithGravity(response.errors.Error, SimpleToast.SHORT, SimpleToast.CENTER)
                }
                this.setState({ loadingGetCode: false })
            } else {
                alert('Cannot connect to server.')
                this.setState({ loadingGetCode: false })
            }
        }).catch(err => {
            this.setState({ loadingGetCode: false })
        })
    }

    checkCode = async () => {
        var { language } = this.props.language;
        this.setState({ loadingCheckCode: true })
        const auKey = await AsyncStorage.getItem('auKey')
        const auValue = await AsyncStorage.getItem('auValue')
        let headers = {}
        headers[auKey] = auValue
        headers['Content-Type'] = 'multipart/form-data'

        let formdata = new FormData();
        formdata.append("Zipcode", this.state.Zipcode)
        formdata.append("Phone", this.state.phone)
        formdata.append("Code", this.state.Code)
        console.log(formdata)
        fetch(Config.API_URL + '/check-code', {
            method: 'post',
            headers: headers,
            body: formdata
        }).then(async (response) => {
            if (response.status == 200) {
                response = await response.json()
                console.log(response)
                if (response.status == 200) {
                    this.setState({ is_verify: true })
                } else {
                    console.log(1)
                    SimpleToast.showWithGravity(convertLanguage(language, 'incorrect_code'), SimpleToast.SHORT, SimpleToast.CENTER)
                }
                this.setState({ loadingCheckCode: false })
            } else {
                alert('Cannot connect to server.')
                this.setState({ loadingCheckCode: false })
            }
        }).catch(err => {
            this.setState({ loadingCheckCode: false })
        })
    }

    openSelect = () => {
        this.props.navigation.navigate('SelectTeams', {
            data: this.state.arrSpecialty,
            callback: (arrSpecialty) => this.setState({ arrSpecialty })
        })
    }

    should = () => {
        const { avatarSource, name, id, phone, email, country, arrSpecialty } = this.state
        const { loading } = this.props.team;
        return (!loading && avatarSource && name != '' && id != '' && phone != '' && email != '' && country != '' && arrSpecialty.length > 0)
    }

    validate() {
        var { avatarBase64, name, id, phone, email, country, arrSpecialty, data } = this.state;
        var { language } = this.props.language;
        if (!data && avatarBase64 === '') {
            SimpleToast.showWithGravity(convertLanguage(language, 'validate_avatar'), SimpleToast.LONG, SimpleToast.TOP)
            return true;
        }
        if (name === '') {
            SimpleToast.showWithGravity(convertLanguage(language, 'validate_name'), SimpleToast.LONG, SimpleToast.TOP)
            return true;
        }
        if (id === '') {
            SimpleToast.showWithGravity(convertLanguage(language, 'validate_team_id'), SimpleToast.LONG, SimpleToast.TOP)
            return true;
        }
        if (phone === '') {
            SimpleToast.showWithGravity(convertLanguage(language, 'validate_phone'), SimpleToast.LONG, SimpleToast.TOP)
            return true;
        }
        if (country === '') {
            SimpleToast.showWithGravity(convertLanguage(language, 'validate_country'), SimpleToast.LONG, SimpleToast.TOP)
            return true;
        }
        if (arrSpecialty.length === 0) {
            SimpleToast.showWithGravity(convertLanguage(language, 'validate_specialty'), SimpleToast.LONG, SimpleToast.TOP)
            return true;
        }
        if (email === '') {
            SimpleToast.showWithGravity(convertLanguage(language, 'validate_email'), SimpleToast.LONG, SimpleToast.TOP)
            return true;
        }
        return false;
    }

    register = () => {
        if (this.validate()) {
            return;
        }
        var { avatarBase64, name, id, phone, email, country, arrSpecialty, code, data, teamId } = this.state
        var tags = [];
        arrSpecialty.forEach(e => {
            tags.push(e.Id)
        })
        var body = {
            Email: email,
            Name: name,
            Zipcode: this.state.Zipcode,
            Phone: phone,
            Code: code,
            TeamId: id,
            CountryId: country,
            Tag: tags,
        }
        if (avatarBase64) {
            body.LogoBase = avatarBase64
        }
        if (data) {
            var isGoTeamPage = (this.props.route.params && this.props.route.params['from']) ? true : false;
            this.props.onUpdateTeam(teamId, body, this.props.navigation, isGoTeamPage, this.props.route);
        } else {
            const type = (this.props.route.params && this.props.route.params['type']) ? this.props.route.params['type'] : '';
            this.props.onCreateTeam(body, this.props.navigation, type);
        }
    }

    getSpecialty = () => {
        let res = ''
        const { arrSpecialty } = this.state
        if (arrSpecialty.length == 0) return res
        this.state.arrSpecialty.forEach(e => res += (e.HashTagName + ', '))

        return res.substring(0, res.length - 2)
    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    openPicker = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
            includeBase64: true
        }).then(image => {
            this.setState({
                is_change_avatar: true,
                avatarBase64: 'data:image/png;base64,' + image.data,
            })
        }).catch(e => { });
    }

    openCamera = () => {
        ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: true,
            includeBase64: true
        }).then(image => {
            this.setState({
                is_change_avatar: true,
                avatarBase64: 'data:image/png;base64,' + image.data,
            })
        }).catch(e => { });
    }

    showCountryName() {
        var { country, arrCountry } = this.state;
        var datas = arrCountry.filter((data) => data.value === country);
        return datas[0].name;
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    render() {
        var { loading } = this.props.team;
        var { language } = this.props.language;
        var { countdown, is_send_code, is_verify, isCode } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1, alignItems: 'center' }}>
                <View style={{
                    flexDirection: 'row', alignSelf: 'stretch',
                    alignItems: 'center', justifyContent: 'space-between'
                }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                </View>
                <Line />
                {
                    (this.state.loadingData || loading) &&
                    <View style={{ flex: 1, top: height / 2 - 30, width, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#000000" />
                    </View>
                }

                {
                    this.state.loadingData ?
                        <View style={{
                            position: 'absolute',
                            left: 0, top: 0, right: 0, bottom: 0,
                            backgroundColor: '#7F00000'
                        }} />
                        :
                        <>
                            <ScrView style={{ opacity: loading ? 0.2 : 1 }} keyboardShouldPersistTaps='handled'>
                                {this.state.data ? null : <Text style={{ marginHorizontal: 20, marginTop: 20, fontSize: 20, color: Colors.TEXT_P, textAlign: 'center', fontWeight: 'bold' }}>{convertLanguage(language, 'build_your_team')}</Text>}
                                {this.state.data ? null : <Text style={{ marginHorizontal: 20, marginTop: 20, fontSize: 16, color: Colors.TEXT_P, textAlign: 'center' }}>{convertLanguage(language, 'team_create_text')}</Text>}

                                {/* avatar */}
                                <Touchable
                                    onPress={() => this.openPicker()}
                                    style={{ alignSelf: 'center', marginTop: 20 }}>
                                    <Image
                                        resizeMode='cover'
                                        style={{ width: 96, height: 96, borderRadius: !this.state.data && !this.state.is_change_avatar ? 0 : 48 }}
                                        source={this.state.is_change_avatar ? { uri: this.state.avatarBase64 } : this.state.avatarSource} />
                                    {
                                        (this.state.is_change_avatar || this.state.AvatarCheck) &&
                                        <Touchable onPress={this.pickAvatar} style={{ position: 'absolute', bottom: 0, right: 0, width: 32, height: 32, borderRadius: 16, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FFFFFF' }}>
                                            <Image source={require('../assets/pencil.png')} style={{ width: 24, height: 24 }} />
                                        </Touchable>
                                    }
                                </Touchable>
                                {/* name */}
                                <View style={styles.boxInput}>
                                    <Text style={{ flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'team_name')}</Text>
                                    <TextInput
                                        style={styles.ipContent}
                                        selectionColor="#bcbcbc"
                                        value={this.state.name}
                                        onChangeText={(name) => this.setState({ name })}
                                    />
                                </View>


                                {/* id */}
                                <View style={styles.boxInput}>
                                    <Text style={{ flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'team_id')}</Text>
                                    <TextInput
                                        style={[styles.ipContent, { color: (this.state.data && isCode) ? '#c0c0c0' : '#333333' }]}
                                        selectionColor="#bcbcbc"
                                        value={this.state.id}
                                        onChangeText={(id) => this.setState({ id })}
                                        placeholder={convertLanguage(language, 'can_not_change_later')}
                                        editable={(this.state.data && isCode) ? false : true}
                                        autoCapitalize='none'
                                    />
                                </View>

                                {/* phone */}
                                <View style={styles.boxInput}>
                                    <Text style={{ marginTop: 6, flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'phone')}</Text>
                                    <View style={{ flex: 2.5 }}>
                                        <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                            <TouchableOpacity
                                                onPress={() => this.showPopover()}
                                                disabled={is_verify || countdown > 0}
                                                ref={ref => this.touchable = ref}
                                                style={{
                                                    borderBottomWidth: 1,
                                                    borderBottomColor: '#bdbdbd',
                                                    paddingBottom: Platform.OS === 'ios' ? 5 : 8,
                                                    paddingTop: Platform.OS === 'ios' ? 5 : 8,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    width: 72,
                                                    marginRight: 10
                                                }}>
                                                <Text style={{
                                                    fontSize: 15,
                                                    color: '#333333',
                                                    fontWeight: 'bold'
                                                }}>{this.state.Zipcode}</Text>
                                                <Image source={require('../assets/down_arrow_active.png')} style={{
                                                    width: 13,
                                                    height: 11,
                                                    marginRight: 5
                                                }} />
                                            </TouchableOpacity>
                                            <TextInput
                                                editable={is_verify || countdown > 0 ? false : true}
                                                style={[styles.ipContent, is_verify ? { paddingRight: 30 } : {}]}
                                                selectionColor="#bcbcbc"
                                                value={this.state.phone}
                                                onChangeText={(Phone) => this.setState({ phone: Phone == 0 ? '' : Phone })}
                                                keyboardType="numeric"
                                            />
                                            {
                                                is_verify &&
                                                <Image source={require('../assets/ic_verify.png')} style={[{ width: 20, height: 20, position: 'absolute', right: 0 }, Platform.OS === 'ios' ? { bottom: 5 } : { bottom: 10 }]} />
                                            }
                                        </View>
                                    </View>
                                </View>
                                {
                                    !is_verify &&
                                    <>
                                        <View style={styles.boxInput}>
                                            <Text style={{ marginTop: 6, flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold' }}></Text>
                                            <Touchable
                                                style={{ height: 31, borderRadius: 4, borderWidth: 1, borderColor: this.state.phone.length < 8 ? '#bcbcbc' : '#333333', alignItems: 'center', justifyContent: 'center', flex: 2.5 }}
                                                disabled={this.state.phone.length < 8 || this.state.loadingGetCode || countdown > 0}
                                                onPress={this.getCode}
                                            >
                                                <Text style={{ color: this.state.phone.length < 8 ? '#bcbcbc' : '#333333', }}>{this.state.loadingGetCode ? convertLanguage(language, 'loading') + '...' : countdown > 0 ? convertLanguage(language, 'resend_code_after', { time: countdown + convertLanguage(language, 'second') }) : convertLanguage(language, 'get_code')}</Text>
                                            </Touchable>
                                        </View>
                                        {
                                            is_send_code &&
                                            <View style={styles.boxInput}>
                                                <Text style={{ marginTop: 6, flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold' }}></Text>
                                                <View style={{ flexDirection: 'row', flex: 2.5, alignItems: 'flex-end' }}>
                                                    <TextInput
                                                        style={[styles.ipContent]}
                                                        selectionColor="#bcbcbc"
                                                        value={this.state.Code}
                                                        onChangeText={(Code) => this.setState({ Code })}
                                                    />

                                                    <Touchable
                                                        disabled={!this.state.phone || this.state.loadingCheckCode}
                                                        onPress={this.checkCode}
                                                        style={{
                                                            marginLeft: 10,
                                                            justifyContent: 'center', alignItems: 'center', height: 30, borderWidth: 1,
                                                            borderColor: this.state.phone && !this.state.loadingCheckCode ? Colors.PRIMARY : Colors.DISABLE, borderRadius: 4,
                                                        }} >
                                                        <Text style={{
                                                            color: this.state.phone && !this.state.loadingCheckCode ? Colors.PRIMARY : Colors.DISABLE,
                                                            paddingLeft: 10, paddingRight: 10,
                                                        }}>{this.state.loadingCheckCode ? convertLanguage(language, 'loading') + '...' : convertLanguage(language, 'ok')}</Text>
                                                    </Touchable>
                                                </View>
                                            </View>
                                        }
                                    </>
                                }

                                {/* email */}
                                <View style={styles.boxInput}>
                                    <Text style={{ flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'email')}</Text>
                                    <TextInput
                                        style={styles.ipContent}
                                        selectionColor="#bcbcbc"
                                        value={this.state.email}
                                        keyboardType={'email-address'}
                                        onChangeText={(email) => this.setState({ email })}
                                        autoCapitalize='none'
                                    />
                                </View>


                                {/* Country */}
                                <View style={styles.boxInput}>
                                    <Text style={{ flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'country')}</Text>
                                    <PickerItem
                                        boxDisplayStyles={{ flex: 2.5, borderBottomWidth: 1, borderBottomColor: '#bcbcbc', borderBottomWidth: 1, borderBottomColor: '#bcbcbc', paddingBottom: 5, paddingTop: 5, height: 35 }}
                                        containerStyle={{ paddingLeft: 5, paddingRight: 5, height: 35 }}
                                        options={this.state.arrCountry}
                                        selectedOption={this.state.country}
                                        isChange={false}
                                        onSubmit={(value) => this.setState({ country: value })}
                                    />
                                </View>


                                {/* Specialty */}
                                <View style={{
                                    flexDirection: 'row', alignSelf: 'stretch',
                                    marginLeft: 20, marginRight: 20, marginTop: 20
                                }}>
                                    <Text style={{ flex: 1, color: Colors.TEXT_P, fontSize: 16, fontWeight: 'bold', marginTop: 12 }}>{convertLanguage(language, 'specialty')}</Text>
                                    <View style={{ flexDirection: 'row', flex: 2.5, alignItems: 'flex-end' }}>
                                        <Touchable
                                            onPress={this.openSelect}
                                            style={{
                                                height: 32, paddingLeft: 12, paddingRight: 12,
                                                alignSelf: 'center', marginTop: 10, alignSelf: 'stretch',
                                                justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: Colors.TEXT_P, borderRadius: 4,
                                            }} >
                                            <Text style={{ color: Colors.TEXT_P }}>{this.state.arrSpecialty.length == 0 ? convertLanguage(language, 'select') : convertLanguage(language, 'edit')}</Text>
                                        </Touchable>
                                        <View style={{ flex: 1, marginLeft: 10, paddingBottom: 5, borderBottomWidth: 1, borderBottomColor: '#bcbcbc' }}>
                                            <Text style={{ color: '#333333', fontSize: 16, marginLeft: 8, marginTop: 12, lineHeight: 26, fontWeight: 'bold' }}>{this.getSpecialty()}</Text>
                                        </View>
                                    </View>
                                </View>
                            </ScrView>
                            <Touchable
                                disabled={!this.should()}
                                onPress={this.register}
                                style={{
                                    marginTop: 15,
                                    marginLeft: 20, marginRight: 20,
                                    marginBottom: 15,
                                    alignSelf: 'stretch',
                                    minHeight: 50, borderColor: this.should() ? Colors.PRIMARY : Colors.DISABLE, borderWidth: 1, borderRadius: 4,
                                    justifyContent: 'center', alignItems: 'center',
                                    backgroundColor: this.should() ? Colors.PRIMARY : '#FFFFFF'
                                }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', color: this.should() ? '#FFFFFF' : Colors.DISABLE }}>{this.props.team.loading ? convertLanguage(language, 'waiting') : this.state.data ? convertLanguage(language, 'save') : convertLanguage(language, 'register')}</Text>
                            </Touchable>
                            <Popover
                                isVisible={this.state.isVisible}
                                animationConfig={{ duration: 0 }}
                                fromView={this.touchable}
                                placement='bottom'
                                arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                                backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                                onRequestClose={() => this.closePopover()}>
                                <View style={styles.boxPopover}>
                                    <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+82' }) }} style={styles.boxCountry}>
                                        <Image source={require('../assets/flag_kr.png')} style={styles.ic_country} />
                                        <Text style={styles.txtCountry}>+82</Text>
                                    </Touchable>
                                    <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+84' }) }} style={styles.boxCountry}>
                                        <Image source={require('../assets/flag_vn.png')} style={styles.ic_country} />
                                        <Text style={styles.txtCountry}>+84</Text>
                                    </Touchable>
                                    <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+66' }) }} style={styles.boxCountry}>
                                        <Image source={require('../assets/flag_th.png')} style={styles.ic_country} />
                                        <Text style={styles.txtCountry}>+66</Text>
                                    </Touchable>
                                </View>
                            </Popover>
                        </>
                }
            </SafeView >
        );
    }
}

const styles = StyleSheet.create({
    ipContent: {
        fontSize: 15,
        color: Colors.TEXT_P,
        borderBottomWidth: 1,
        borderBottomColor: '#bcbcbc',
        flex: 2.5,
        paddingBottom: 5,
        paddingTop: 5,
        // height: 32,
        fontWeight: 'bold'
    },
    boxPicker: {
        borderBottomWidth: 1,
        borderBottomColor: '#bcbcbc',
        paddingBottom: 5,
        paddingTop: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 72,
        marginRight: 10
    },
    txtPicker: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxOption: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 10
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5
    },
    boxInput: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 16,
    },
    boxPopover: {
        width: 100,
        padding: 12,
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 12
    },
    txtCountry: {
        color: '#333333',
        fontSize: 18
    },
})

const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language,
        global: state.global,
        city: state.city
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getTeamDetail: (id, navigation) => {
            dispatch(actTeamDetail(id, navigation))
        },
        onCreateTeam: (body, navigation, type) => {
            dispatch(actCreateTeam(body, navigation, type))
        },
        onUpdateTeam: (teamId, body, navigation, isGoTeamPage, route) => {
            dispatch(actUpdateTeam(teamId, body, navigation, isGoTeamPage, route))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(RegisterTeam);