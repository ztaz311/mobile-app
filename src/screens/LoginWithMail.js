import React, { Component } from 'react';
import {
    StyleSheet,
    View, ScrollView,
    Text, Image,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import AvoidView from '../screens_view/AvoidView';
import SafeView from '../screens_view/SafeView';
import Touchable from '../screens_view/Touchable';
import Line from '../screens_view/Line';
import * as Colors from '../constants/Colors';
import Loading from '../screens_view/Loading';
import { connect } from "react-redux";
import ModalPageWebView from '../components/ModalPageWebView';
import { actLogin } from '../actions/user';
import { convertLanguage } from '../services/Helper';
const base64 = require('base-64');
import * as Config from '../constants/Config';
class LoginWithMail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            id: '',
            passwd: '',
            err_id: '',
            err_passwd: '',
            modalPageWebView: false,
            uri: '',
            title: ''
            // errors: {},
        };
    }

    shouldLogin = () => {
        return (this.state.id && this.state.passwd)
    }

    login = (goToWelcome = false, register = false) => {
        console.log(this.state.id + ":" + this.state.passwd)
        const encode = base64.encode(this.state.id + ":" + this.state.passwd)
        const auKey = 'Authorization'
        const auValue = 'Basic ' + encode
        this.props.login(auKey, auValue, goToWelcome, this.props.navigation, this.props.route && this.props.route.params ? this.props.route.params['type'] : '', register);
    }

    cb = (id, passwd) => {
        this.setState({ id, passwd }, () => this.login(true, true))
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.errors.UserId) {
            this.setState({
                err_id: nextProps.user.errors.UserId,
            })
        }
        if (nextProps.user.errors.Password) {
            this.setState({
                err_passwd: nextProps.user.errors.Password,
            })
        }
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ flex: 1, backgroundColor: Colors.BG }}>
                {
                    this.props.user.loading &&
                    <Loading />
                }
                <Touchable
                    onPress={() => this.props.navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, alignSelf: 'flex-start', alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                </Touchable>
                <Line />
                <AvoidView style={{ flex: 1 }}>
                    <ScrollView keyboardShouldPersistTaps='always' >
                        <View>
                            <Text style={{ color: Colors.TEXT_P, fontSize: 38, fontWeight: 'bold', marginLeft: 20, marginTop: 20 }}>{convertLanguage(language, 'login')}</Text>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'id_or_email')}</Text>
                                <TextInput
                                    style={[styles.ipContent, this.state.err_id !== '' ? { borderBottomColor: '#ff4081' } : {}]}
                                    selectionColor="#bcbcbc"
                                    value={this.state.id}
                                    onChangeText={(id) => this.setState({ id, err_id: '' })}
                                    keyboardType={'email-address'}
                                    autoCapitalize='none'
                                />
                                {
                                    this.state.err_id !== '' &&
                                    <Text style={styles.txtError}>{this.state.err_id}</Text>
                                }
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'password')}</Text>
                                <TextInput
                                    style={[styles.ipContent, this.state.err_passwd !== '' ? { borderBottomColor: '#ff4081' } : {}]}
                                    selectionColor="#bcbcbc"
                                    value={this.state.passwd}
                                    onChangeText={(passwd) => this.setState({ passwd, err_passwd: '' })}
                                    secureTextEntry={true}
                                />
                                {
                                    this.state.err_passwd !== '' &&
                                    <Text style={styles.txtError}>{this.state.err_passwd}</Text>
                                }
                            </View>

                            <Touchable
                                style={{ marginRight: 20, alignSelf: 'flex-end' }}
                                onPress={() => this.props.navigation.navigate('ForgotPw')}>
                                <Text
                                    style={{ textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: Colors.PRIMARY }}>
                                    {convertLanguage(language, 'forgot_password')}?</Text>
                            </Touchable>

                            <Touchable
                                disabled={!this.shouldLogin() || this.props.user.loading}
                                onPress={() => !this.props.user.loading ? this.login(true) : null}
                                style={{
                                    minHeight: 56, borderColor: this.shouldLogin() ? Colors.PRIMARY : Colors.DISABLE, borderWidth: 1, borderRadius: 4,
                                    marginLeft: 20, marginRight: 20, marginTop: 40, justifyContent: 'center', alignItems: 'center',
                                    backgroundColor: this.shouldLogin() ? Colors.PRIMARY : '#FFFFFF'
                                }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', color: this.shouldLogin() ? '#FFFFFF' : Colors.DISABLE }}>{this.props.user.loading ? convertLanguage(language, 'loading') + '...' : convertLanguage(language, 'login')}</Text>
                            </Touchable>

                            <Text style={{ alignSelf: 'center', fontWeight: 'normal', fontSize: 15, marginTop: 10, color: '#757575' }}>
                                {convertLanguage(language, 'by_logging')}</Text>

                            <View style={{ alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: '#757575', flexDirection: 'row' }}>
                                <Touchable
                                    onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/terms-of-service`, title: convertLanguage(language, 'terms_of_service') })}>
                                    <Text
                                        style={{ textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: Colors.PRIMARY }}>
                                        {convertLanguage(language, 'terms_of_service')}</Text>
                                </Touchable>
                                <Text style={{ alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: '#757575' }}>  &  </Text>
                                <Touchable
                                    onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/privacy-policy`, title: convertLanguage(language, 'privacy_policy') })}>
                                    <Text
                                        style={{ textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: Colors.PRIMARY }}>
                                        {convertLanguage(language, 'privacy_policy')}</Text>
                                </Touchable>
                            </View>


                            <Text style={{ margin: 20, color: '#757575', fontWeight: 'bold', fontSize: 20, alignSelf: 'center' }}>{convertLanguage(language, 'or')}</Text>


                            <Touchable
                                onPress={() => { this.props.navigation.navigate('Signup', { cb: this.cb }) }}
                                style={{
                                    minHeight: 56, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                                    marginLeft: 20, marginRight: 20, justifyContent: 'center', alignItems: 'center'
                                }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', color: Colors.PRIMARY }}>{convertLanguage(language, 'create_new_account')}</Text>
                            </Touchable>

                            <View style={{ height: 30 }} />
                        </View>
                    </ScrollView>
                </AvoidView>
                {
                    this.state.modalPageWebView
                        ?
                        <ModalPageWebView
                            closeModal={() => this.setState({ modalPageWebView: false })}
                            navigation={this.props.navigation}
                            uri={this.state.uri}
                            title={this.state.title}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    boxInput: {
        // paddingBottom: 24,
        margin: 20,
        marginTop: 10,
        marginBottom: 12
    },
    txtInput: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    ipContent: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 12,
        paddingBottom: 12,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    txtError: {
        color: '#ff4081',
        fontSize: 12,
        marginTop: 2
    }
})

const mapStateToProps = state => {
    return {
        user: state.user,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        login: (auKey, auValue, isWellocme, navigation, type, register) => {
            dispatch(actLogin(auKey, auValue, isWellocme, navigation, type, register))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginWithMail);
