import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ActivityIndicator, StatusBar, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import Popover from 'react-native-popover-view';
import Touchable from '../screens_view/Touchable'
import ScrView from '../screens_view/ScrView'
import * as Colors from '../constants/Colors'
import { convertLanguage } from '../services/Helper'
import AppleLogin from '../components/react-native-apple-login/Apple'
const { width, height } = Dimensions.get('window')
import appleAuth, {
    AppleButton
} from '@invertase/react-native-apple-authentication';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.authCredentialListener = null;
        this.user = null;
        this.state = {
            loading: false,
            tokenInstagram: '',
            credentialStateForUser: -1,
            isVisible: false
        };
        this.instagramLogin = null;
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    componentDidMount() {
        /**
         * subscribe to credential updates.This returns a function which can be used to remove the event listener
         * when the component unmounts.
         */
        // this.authCredentialListener = appleAuth.onCredentialRevoked(async () => {
        //     console.warn('Credential Revoked');
        //     this.fetchAndUpdateCredentialState().catch(error =>
        //         this.setState({ credentialStateForUser: `Error: ${error.code}` }),
        //     );
        // });

        // this.fetchAndUpdateCredentialState()
        //     .then(res => this.setState({ credentialStateForUser: res }))
        //     .catch(error => this.setState({ credentialStateForUser: `Error: ${error.code}` }))
    }

    componentWillUnmount() {
        /**
         * cleans up event listener
         */
        // this.authCredentialListener();
    }

    setTokenApple = (token) => {
        this.props.apple(token.id_token)
    }

    render() {
        var { language } = this.props;
        var { isVisible } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: Colors.BG }}>
                <StatusBar barStyle="light-content" />
                <ImageBackground
                    style={{
                        backgroundColor: '#bdbdbd',
                        width: width,
                        height: width / 1.78,
                        alignItems: 'flex-start', justifyContent: 'flex-start'
                    }}
                    source={require('../assets/banner.png')}>
                    <SafeAreaConsumer>
                        {insets => <View style={{ paddingTop: insets.top }} />}
                    </SafeAreaConsumer>
                    <TouchableOpacity
                        onPress={() => this.showPopover()}
                        ref={ref => this.touchable = ref}
                        style={[{
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row',
                            margin: 8,
                            padding: 4
                        }, isVisible ? {
                            borderRadius: 4,
                            backgroundColor: 'rgba(27, 29, 31, 0.1)',
                            shadowColor: 'black',
                            borderWidth: 1,
                            shadowRadius: 10,
                            shadowOpacity: 1,
                            overflow: 'hidden',
                        } : {}]}>
                        <Image source={require('../assets/ic_language.png')} style={{ marginRight: 5, width: 16, height: 16 }} />
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}>{language.toUpperCase()}</Text>
                    </TouchableOpacity>
                </ImageBackground>

                <ScrView style={{ flex: 1 }}>
                    <Text style={{
                        alignSelf: 'center', marginTop: 20,
                        fontSize: 20, fontWeight: 'bold', color: '#333333'
                    }}>{convertLanguage(language, 'we_like_party')}</Text>

                    <Image source={require('../assets/logo_blue.png')} style={{ alignSelf: 'center', marginTop: 10 }} />

                    <Touchable
                        onPress={this.props.facebook}
                        style={{
                            alignSelf: 'stretch', height: 40, backgroundColor: '#3C5A96',
                            marginTop: 20, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            paddingHorizontal: 16,
                        }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{ marginRight: 8, width: 24, height: 24, }} source={require('../assets/facebook_logo.png')} />
                            <Text style={{ color: 'white', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'connect_facebook')}</Text>
                        </View>
                    </Touchable>

                    <Touchable
                        onPress={this.props.google}
                        style={{
                            alignSelf: 'stretch', height: 40, backgroundColor: '#EA4335',
                            marginTop: 12, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            paddingHorizontal: 16,
                        }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{ marginRight: 8, width: 24, height: 24, }} source={require('../assets/G_icon.png')} />
                            <Text style={{ color: 'white', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'connect_google')}</Text>
                        </View>
                    </Touchable>
                    <Touchable
                        onPress={() => appleAuth.isSupported ? this.props.appleIos() : this.instagramLogin.show()}
                        style={{
                            alignSelf: 'stretch', height: 40, backgroundColor: '#333333',
                            marginTop: 12, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            paddingHorizontal: 16,
                        }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{ width: 24, height: 24, marginRight: 8 }} source={require('../assets/ic_apple.png')} />
                            <Text style={{ color: 'white', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'connect_apple')}</Text>
                        </View>
                    </Touchable>
                    <AppleLogin
                        ref={ref => this.instagramLogin = ref}
                        appId='com.starindex.login-apple'
                        appSecret='eyJraWQiOiI3NVdRNFUyRzM0IiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiIzSzlHWTkySzNKIiwiaWF0IjoxNTkwMDM2MjU5LCJleHAiOjE2MDU1ODgyNTksImF1ZCI6Imh0dHBzOi8vYXBwbGVpZC5hcHBsZS5jb20iLCJzdWIiOiJjb20uc3RhcmluZGV4LmxvZ2luLWFwcGxlIn0.Tbfbpla8740SCKU2mJ-JfLfON1x4rPrcpBwEyLOriJlaM3qa0T0IfW2kNDGOaIHenoGJnH4KI33KntSaZPqO-A'
                        redirectUrl='https://www.comeup.asia/mobile-login-apple'
                        scopes={['name', 'email']}
                        modalVisible={true}
                        onLoginSuccess={this.setTokenApple}
                        onLoginFailure={(data) => console.log(data)}
                    />
                    <Touchable
                        onPress={() => this.props.navigation.navigate('LoginWithMail')}
                        style={{
                            alignSelf: 'stretch', height: 40, backgroundColor: '#00A9F4',
                            marginTop: 12, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            paddingHorizontal: 16,
                        }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{ width: 24, height: 24, marginRight: 8 }} source={require('../assets/ic_mail.png')} />
                            <Text style={{ color: 'white', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'login')} / {convertLanguage(language, 'sign_up')}</Text>
                        </View>
                    </Touchable>

                    <Touchable
                        onPress={() => this.props.navigation.reset({
                            index: 0,
                            routes: [{ name: 'Main' }],
                        })}
                        style={{
                            alignSelf: 'stretch', height: 40,
                            marginTop: 24, marginLeft: 48, marginRight: 48, justifyContent: 'center', alignItems: 'center',
                            borderRadius: 4, borderWidth: 1, borderColor: Colors.PRIMARY,
                        }}>
                        <Text style={{ color: Colors.PRIMARY, fontSize: 16 }}>{convertLanguage(language, 'later')}</Text>
                    </Touchable>

                    <View style={{ height: 20 }}></View>
                </ScrView>

                <Popover
                    isVisible={this.state.isVisible}
                    fromView={this.touchable}
                    animationConfig={{ duration: 0 }}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => this.closePopover()}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => { this.closePopover(); this.props.updateLanguage('ko') }} style={styles.boxCountry}>
                            <Image source={require('../assets/flag_kr.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>Korean</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.props.updateLanguage('vi') }} style={styles.boxCountry}>
                            <Image source={require('../assets/flag_vn.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>Vietnamese</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.props.updateLanguage('en') }} style={styles.boxCountry}>
                            <Image source={require('../assets/flag_en.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>English</Text>
                        </Touchable>
                    </View>
                </Popover>

                {this.props.loading ?
                    <View style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, backgroundColor: '#000', opacity: 0.7, alignItems: 'center', justifyContent: 'center' }} >
                        <ActivityIndicator size="large" color="#FFFFFF" />
                    </View>
                    : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    appleButton: {
        width: 200,
        height: 60,
        margin: 10,
    },
    header: {
        margin: 10,
        marginTop: 30,
        fontSize: 18,
        fontWeight: 'bold',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'pink',
    },
    horizontal: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    boxPopover: {
        // width: 130,
        padding: 12,
        borderRadius: 8
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 12
    },
    txtCountry: {
        color: '#333333',
        fontSize: 18
    },
});
