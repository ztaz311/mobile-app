import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import * as Colors from '../constants/Colors'
import ModalLogin from '../components/global/ModalLogin'

import { connect } from "react-redux";
import { actCheckLogin } from '../actions/user';
import { getLanguage } from '../actions/language';
import { getCountry, setGlobalCity, setGlobalCountry, getIsoCode } from '../actions/global';
import firebase from 'react-native-firebase';
import * as RNLocalize from "react-native-localize";
class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isConnected: true,
        }
    }

    componentWillUnmount() {
        clearTimeout(this.run)
    }

    componentDidMount() {
        firebase.analytics().setAnalyticsCollectionEnabled(true);
        this.init()
    }

    async getIp() {
        try {
            const response = await fetch('https://api.ipify.org');
            const ip = response.text();
            return ip;
        } catch (e) {
            throw 'Unable to get IP address.';
        }
    }

    async init() {
        const locales = RNLocalize.getLocales();
        const languageCode = locales.length > 0 ? locales[0].languageCode : 'en';
        this.props.getLanguage(languageCode);
        // console.log('vao day roi');
        this.setState({ isConnected: true })
        var city = await AsyncStorage.getItem('city');
        if (city) {
            city = JSON.parse(city)
            this.props.setGlobalCity(city)
        }
        var country = await AsyncStorage.getItem('country');
        if (country) {
            country = JSON.parse(country)
            this.props.setGlobalCountry(country)
            await this.getIp().then(ip => {
                this.props.getIsoCode(ip);
            }).catch((e) => {
                console.log(e)
                this.props.getIsoCode('');
            });
        } else {
            await this.getIp().then(ip => {
                this.props.getCountry(ip);
            }).catch((e) => {
                console.log(e)
                this.props.getCountry('');
            });
        }
        const auKey = await AsyncStorage.getItem('auKey')
        const auValue = await AsyncStorage.getItem('auValue')
        if (auValue) {
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    this.login(auKey, auValue)
                } else {
                    this.setState({ isConnected: false })
                }
            });
        } else {
            this.go('Login')
        }
    }

    go = (goWhere) => {
        this.run = setTimeout(() => {
            clearTimeout(this.run)
            this.props.navigation.replace(goWhere)
        }, 200)
    }

    login = (auKey, auValue) => {
        var headers = {}
        headers[auKey] = auValue

        this.props.checkLogin(auKey, auValue, false, this.props.navigation);

        this.setState({ loading: true, })
    }

    render() {
        var { isConnected } = this.state;
        var { check_login_loading, error_status } = this.props.user;
        return (
            <View style={{ flex: 1, backgroundColor: Colors.PRIMARY }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/logo.png')} />
                    <View style={{ height: 244, justifyContent: 'center', alignItems: 'center' }}>
                        {
                            check_login_loading &&
                            <ActivityIndicator size="large" color="#FFFFFF" />
                        }
                        {
                            (!isConnected || error_status === 500) &&
                            <>
                                <Image source={require('../assets/wifi_disconnect.png')} style={{ width: 108, height: 108, marginTop: 20, marginBottom: 20 }} />
                                <Text style={{ color: '#FFFFFF', fontSize: 14, textAlign: 'center' }}>{'Network unavailable'}</Text>
                                <Text style={{ color: '#FFFFFF', fontSize: 14, textAlign: 'center' }}>{'Check your internet connection and try again.'}</Text>
                                <TouchableOpacity onPress={() => this.init()} style={{ width: 101, height: 36, borderRadius: 4, borderColor: '#FFFFFF', borderWidth: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                                    <Text style={{ color: '#FFFFFF', fontSize: 14 }}>Reload</Text>
                                </TouchableOpacity>
                            </>
                        }
                    </View>
                </View>
                <Text style={{ color: '#FFFFFF', fontSize: 16, textAlign: 'center', bottom: 64 }}>© Starindex, Inc</Text>
                <ModalLogin navigation={this.props.navigation} />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        checkLogin: (auKey, auValue, isWellocme, navigation) => {
            dispatch(actCheckLogin(auKey, auValue, isWellocme, navigation))
        },
        getLanguage: (country_code) => {
            dispatch(getLanguage(country_code))
        },
        getCountry: (ip) => {
            dispatch(getCountry(ip))
        },
        setGlobalCity: (city) => {
            dispatch(setGlobalCity(city))
        },
        setGlobalCountry: (country) => {
            dispatch(setGlobalCountry(country))
        },
        getIsoCode: (ip) => {
            dispatch(getIsoCode(ip))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Splash);

