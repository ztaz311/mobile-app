import React, { Component } from 'react';
import { View, Text, Image, FlatList, ActivityIndicator, Alert } from 'react-native';

import SafeView from '../screens_view/SafeView';
import Touchable from '../screens_view/Touchable';
import Line from '../screens_view/Line';
import TeamNewsItem from '../components/team/TeamNewsItem';
import ModalTeamNews from '../components/team/ModalTeamNews';
import ModalZoomImage from '../components/team/ModalZoomImage';
import { connect } from "react-redux";
import * as Colors from '../constants/Colors';
import { actLoadDataTeamNews, actLikeTeamNews, actDeleteTeamNews } from '../actions/team';
import { convertLanguage } from '../services/Helper';
import ModalTeamNewsDetail from '../components/team/ModalTeamNewsDetail';
class TeamListNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            page: 1,
            modelTeamNews: false,
            TeamNewsId: '',
            modalZoomImage: false,
            zoomImageIndex2: 0,
            modelImage: false,
            modalTeamNewsDetail: false,
            teamnews: {},
            Role: ''
        }
    }

    componentWillMount() {
        var { page } = this.state;
        var { iso_code } = this.props.global;
        this.props.onLoadDataTeamNews(this.props.route.params['id'], page, iso_code);
        // this.getTeamNews()
    }

    loadMore() {
        var { loadMore } = this.props.team;
        var { page } = this.state;
        var { iso_code } = this.props.global;
        if (loadMore) {
            this.props.onLoadDataTeamNews(this.props.route.params['id'], page + 1, iso_code);
            this.setState({ page: page + 1 })
        }
    }

    _renderFooter() {
        var { loadMore, is_empty } = this.props.team;
        var { language } = this.props.language;
        if (loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'team_news_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    onOpenModelTeamNews(Id) {
        this.setState({
            modelTeamNews: true,
            TeamNewsId: Id,
        })
    }

    onEditTeamNews() {
        var { TeamNewsId } = this.state;
        this.setState({
            modelTeamNews: false,
        })
        this.props.navigation.navigate('CreateANews', { TeamNewsId: TeamNewsId })
    }

    onDeleteTeamNews() {
        var { TeamNewsId } = this.state;
        var { language } = this.props.language;
        Alert.alert(
            convertLanguage(language, 'delete_team_news'),
            convertLanguage(language, 'do_you_want_to_delete_this_news'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => { this.setState({ modalTeamNewsDetail: false, modelTeamNews: false, teamnews: {} }); this.props.onDeleteTeamNews(TeamNewsId) } },
            ],
            { cancelable: false },
        );
    }

    onReportTeamNews() {
        var { TeamNewsId } = this.state;
        this.setState({
            modelTeamNews: false,
        })
        this.props.globalCheckLogin('TeamNewsReport', { TeamNewsId: TeamNewsId })
    }

    onOpenModelTeamNewsDetail(item, Role) {
        this.setState({
            modalTeamNewsDetail: true,
            teamnews: item,
            Role: Role
        })
    }

    closeModalTeamNewsDetail = () => {
        this.setState({ modalTeamNewsDetail: false, teamnews: {} })
    }

    renderItem = ({ item, index }) => {
        return <TeamNewsItem
            navigation={this.props.navigation}
            onDeleteTeamNews={(Id) => this.props.onDeleteTeamNews(Id)}
            data={item}
            onLikeTeamNews={(Id) => this.props.onLikeTeamNews(Id)}
            openModalAction={(Id) => this.onOpenModelTeamNews(Id)}
            zoomImage={(index, Images) => this.setState({ zoomImageIndex2: index, modalZoomImage: true, dataZoomImage: Images })}
            onOpenModelTeamNewsDetail={() => this.onOpenModelTeamNewsDetail(item, item.Team.Role)} />
    }

    dataZoomImage() {
        var images = [];
        this.state.dataZoomImage.forEach(image => {
            images.push({ Image: image.Photos.Large });
        });
        return images;
    }

    render() {
        var { language } = this.props.language;
        return (
            <>
                <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: 50 }} />
                        <Text style={{ fontSize: 20, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'team_news')}</Text>
                        <Touchable
                            onPress={() => {
                                this.props.navigation.goBack()
                            }}
                            style={{
                                width: 50, minHeight: 40,
                                justifyContent: 'center', alignItems: 'center',
                            }}>
                            <Image source={require('../assets/X_icon.png')} />
                        </Touchable>
                    </View>

                    <Line />

                    <FlatList
                        style={{
                            paddingLeft: 16,
                            paddingRight: 16
                        }}
                        data={this.props.team.team_news}
                        renderItem={this.renderItem}
                        onEndReached={() => { this.loadMore() }}
                        onEndReachedThreshold={0.5}
                        ListFooterComponent={() => this._renderFooter()} />
                    {
                        this.state.modelTeamNews &&
                        <ModalTeamNews
                            closeModal={() => this.setState({ modelTeamNews: false, TeamNewsId: '' })}
                            onEditTeamNews={() => this.onEditTeamNews()}
                            onDeleteTeamNews={() => this.onDeleteTeamNews()}
                            onReportTeamNews={() => this.onReportTeamNews()} />
                    }
                    {
                        this.state.modalZoomImage
                            ?
                            <ModalZoomImage
                                modalVisible={this.state.modalZoomImage}
                                closeModal={() => this.setState({ modalZoomImage: false })}
                                team={this.props.route.params['team']}
                                images={this.dataZoomImage()}
                                index={this.state.zoomImageIndex2}
                            />
                            : null
                    }
                </SafeView>
                {
                    this.state.modalTeamNewsDetail &&
                    <ModalTeamNewsDetail
                        closeModal={this.closeModalTeamNewsDetail}
                        data={this.state.teamnews}
                        onLikeTeamNews={(Id) => this.props.onLikeTeamNews(Id)}
                        onDeleteTeamNews={(Id) => this.props.onDeleteTeamNews(Id)}
                        openModalAction={(Id) => { this.onOpenModelTeamNews(Id, this.state.Role) }}
                        navigation={this.props.navigation}
                    />
                }
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataTeamNews: (id, page, iso_code) => {
            dispatch(actLoadDataTeamNews(id, page, iso_code))
        },
        onDeleteTeamNews: (TeamNewsId) => {
            dispatch(actDeleteTeamNews(TeamNewsId))
        },
        onLikeTeamNews: (Id) => {
            dispatch(actLikeTeamNews(Id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamListNews);
