import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, ActivityIndicator, ScrollView } from 'react-native';
import SafeView from '../screens_view/SafeView'

import Share from 'react-native-share';

import * as Colors from '../constants/Colors'

import { } from '../actions/team';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import * as Config from '../constants/Config';
class TeamShare extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    share() {
        let team = this.props.team.team;
        let options = {
            title: team.Name,
            message: team.Name+"\nFind and follow us in Comeup\n"+this.getTag()+Config.MAIN_URL+'teams/'+team.Slug,
            url: Config.MAIN_URL+'teams/'+team.Slug,
            subject: "Follow us" //  for email
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    getTag = () => {
        const team  = this.props.team.team
        if (!team.HashTag) return '';
        return team.HashTag.map((e) => {
            if (e) {
                return '#' + e.HashTagName + '  '
            }
            return ''
        })
    }
        

    render() {
        var { language } = this.props.language;
        let team = this.props.team.team;
        return (
            <ScrollView>
                <View style={styles.content}>
                    <View style={styles.rowDes}>
                        <Text style={styles.txtDes}>{convertLanguage(language, 'text_share_team')}</Text>
                    </View>
                    <View style={styles.rowBoxShare}>
                        <Image
                            style={styles.imgLogo}
                            source={team.Logos ? { uri: team.Logos.Medium } : null}>
                        </Image>
                        <View style={styles.colInfo}>
                            <Text style={styles.txtTitle}>{team.Name}</Text>
                            <Text style={styles.txtSmall}>{convertLanguage(language, 'find_and_follow_us_in_comeup')}</Text>
                            <Text style={styles.txtSmall}>{ this.getTag() }</Text>
                            <View style={styles.rowLink}>
                                <View style={styles.colLink}><Text style={[styles.txtSmall, styles.link]} >{ Config.MAIN_URL+'teams/'+team.Slug }</Text></View>
                                <View style={styles.colEmpty}></View>
                                
                            </View>
                        </View>
                    </View>
                    <View style={styles.rowBtnShare}>
                        <TouchableOpacity style={styles.btnShare} onPress={ () => {this.share()} }>
                            <Text style={styles.txtBtnShare}>{convertLanguage(language, 'share')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        padding: 20
    },
    rowDes: {
        padding: 25,
        paddingTop: 15,
        paddingBottom: 15
    },
    txtDes: {
        lineHeight: 20,
        fontSize: 15,
        textAlign: 'center',
        color: '#333333'
    },
    rowBoxShare: {
        flexDirection: 'row',
        padding: 10,
        marginTop: 10,
        alignItems: 'center'
    },
    colThumb: {
        // flex: 0.3,
    },
    colInfo: {
        flex: 1,
    },
    imgLogo: {
        alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 40,
        marginRight: 15
    },
    txtTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtSmall: {
        // paddingTop: 5
        fontSize: 15,
        color: '#333333'
    },
    rowBtnShare: {
        marginTop: 50,
        // alignItems: 'center',
    },
    btnShare: {
        backgroundColor: Colors.PRIMARY,
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtBtnShare: {
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    rowLink: {
        flexDirection: 'row'
    },
    colLink: {
        borderColor: Colors.PRIMARY,
    },
    link: {
        color: Colors.PRIMARY,
    },
    colEmpty: {
        flex: 1
    }
  });
const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamShare);
