import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';
import ItemTeam from '../../components/team/ItemTeam'
import { actTeamFollow } from '../../actions/team';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
import { actLoadMoreTeam, actSearchTeam } from '../../actions/search';
class TeamList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
    }
    componentDidMount() {
        // this.refresh()
    }
    followTeam = (teamId) => {
        this.props.followTeam(teamId)
    }
    renderItem = (item, index) => {
        var disabled = this.props.search.teamIdAction == item.Id ? this.props.search.loadingFollowers : false;
        return <ItemTeam
            style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}
            key={'team_' + item.Id}
            data={item}
            followTeam={this.followTeam}
            disabled={disabled}
            navigation={this.props.navigation}
        />
    }


    refresh() {
        // this.setState({ page: 1 });
        // var { search } = this.props
        // this.props.onSearchTeam(search.searchValue, this.props.city_id)
    }

    loadMore() {
        var { search, noLoadMore } = this.props
        if (search.loadMoreTeam && !noLoadMore) {
            this.props.onLoadMoreTeam(this.props.searchValue, this.props.city_id, search.dataSearch.teamList.current_page + 1, search.dataSearch.teamList.scroll_id)
        }
    }

    onSearch(key) {
        this.setState({ searchValue: key })
        this.props.onSearch(key)
    }

    _renderFooter() {
        var { loadMoreTeam } = this.props.search;
        var { is_empty } = this.props.search.dataSearch.teamList
        var teams = this.props.search.dataSearch.teamList.teams;
        var { language } = this.props.language;
        if (loadMoreTeam && !this.props.noLoadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty && !teams.length) {
                return <>
                    <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                        <View style={{ alignItems: 'center', paddingTop: 48 }}>
                            <Image source={require('../../assets/search_empty.png')} style={{ width: 64, height: 60 }} />
                            <Text style={{ color: '#757575', textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'not_find_matched_result')}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' }}>
                            {this.props.showSuggestKeyword}
                        </View>
                    </View>
                </>
                // return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'team_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    render() {
        var { search } = this.props;
        var { dataSearch } = search;
        var teams = dataSearch.teamList.teams;
        var { language } = this.props.language;
        return (
            search.loading ?
                <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
                :
                teams.length === 0 ?
                    <ScrollView>
                        <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                            <View style={{ alignItems: 'center', paddingTop: 48 }}>
                                <Image source={require('../../assets/search_empty.png')} style={{ width: 64, height: 60 }} />
                                <Text style={{ color: '#757575', textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'not_find_matched_result')}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' }}>
                                {this.props.showSuggestKeyword}
                            </View>
                        </View>
                    </ScrollView>
                    :
                    <FlatList
                        contentContainerStyle={this.props.style_}
                        data={teams}
                        renderItem={({ item, index }) => {
                            return this.renderItem(item, index)
                        }}
                        onEndReached={() => { this.loadMore() }}
                        // ListHeaderComponent={this.renderHeader}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                    // refreshing={this.props.search.loadingTeam}
                    // onRefresh={() => this.refresh()}
                    // ListFooterComponent={() => this._renderFooter()}
                    />


        );
    }
}

const styles = StyleSheet.create({
    tagSuggest: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#bebebe',
        borderRadius: 20,
        padding: 10,
        borderWidth: 1,
        borderColor: '#bebebe',

    },
    boxFilter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
    },
    loading: {
        padding: 20
    },
    textHint: {
        padding: 20,
        textAlign: 'center'
    },
    boxItemFilter: {
        flex: 0.3,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 13,
        height: 11
    },
});

const mapStateToProps = state => {
    return {
        team: state.team,
        city: state.city,
        category: state.category,
        language: state.language,
        search: state.search
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        followTeam: (teamId) => {
            dispatch(actTeamFollow(teamId))
        },
        onLoadMoreTeam: (searchValue, Id, page, per_page, scroll_id) => {
            dispatch(actLoadMoreTeam(searchValue, Id, page, per_page, scroll_id))
        },
        onSearchTeam: (searchValue, Id, page, per_page) => {
            dispatch(actSearchTeam(searchValue, Id, page, per_page))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamList);
