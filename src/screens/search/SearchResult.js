import React, { Component } from 'react';
import {
    Keyboard, Platform,
    View, ScrollView, RefreshControl, Image,
    Text, StyleSheet,
    ActivityIndicator,
    ImageBackground, TextInput,
    Linking, FlatList
} from 'react-native';
import Touchable from '../../screens_view/Touchable'
import * as Colors from '../../constants/Colors'
import SafeView from '../../screens_view/SafeView'
import Line from '../../screens_view/Line'
import { actLikeEvent } from '../../actions/event';
import { actUpdateLanguage } from '../../actions/language'
import ModalCity from '../../components/ModalCity'
import ItemEventHalf from '../../components/event/ItemEventHalf';
import { actLoadDataLiked } from '../../actions/like';
import { connect } from "react-redux";
import { actTeamFollow } from '../../actions/team';
import { actSearch, actUpdateTabView, actSearchVenue, actSearchData, actClearDataSearch } from '../../actions/search';
import ResultSearchTeam from './ResultSearchTeam';
import SimpleToast from 'react-native-simple-toast';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            loading: false,
            position: 'se',
            isLoadTeamList: false,
            city: this.props.global.city.Id ? this.props.global.city : this.props.global.country,
            type: this.props.global.city.Id ? 'city' : 'country',
            modalCity: false,
            searchValue: '',
            page: 1,
            searched: false,
            oldSearchValue: '',
        };
    }

    componentDidMount() {
        this.props.onSearchData('', this.state.city.Id, 'city')
        var { event_countries } = this.props.city;
        if (event_countries.length === 0) {
            this.props.onLoadDataCountry();
        }
    }

    componentWillUnmount() {
        this.props.onClearDataSearch()
        Linking.removeEventListener('url', this.handleOpenURL);
    }

    onSelect(city) {
        Keyboard.dismiss();
        this.setState({
            city: city,
            modalCity: false,
            type: 'city'
        })
        if (this.state.searchValue && this.state.searchValue.length > 0) {
            this.props.onSearchData(this.state.searchValue, city.Id, 'city')
        }
    }

    renderEvent() {
        var { language } = this.props.language;
        var { dataSearch } = this.props.search;
        return <>
            {
                dataSearch && dataSearch.data && dataSearch.data.events.data && dataSearch.data.events.data.length > 0 &&
                <>
                    <View style={styles.boxTitle}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'event')}</Text>
                    </View>
                    <View style={styles.boxListEvent}>
                        {
                            dataSearch.data.events.data.map((item, index) => {
                                return <ItemEventHalf cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} key={index} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} />
                            })
                        }
                    </View>
                    <Touchable style={styles.boxSeeAll} onPress={() => this.ResultSearchTabView(0)} >
                        <Text style={styles.txtSeeAll}>{convertLanguage(language, 'see_more')}</Text>
                    </Touchable>
                </>
            }
        </>
    }

    followTeam = (teamId) => {
        this.props.followTeam(teamId)
    }

    ResultSearchTabView(tabView) {
        const { city, searchValue } = this.state;
        this.props.onUpdateTabView(tabView)
        this.props.navigation.navigate('ResultSearchTabView', { city, searchValue, callback: (city, searchValue) => this.setState({ city, searchValue }) })
    }

    renderTeam() {
        var { language } = this.props.language;
        var { dataSearch } = this.props.search;
        return <>
            {dataSearch && dataSearch.teamList && dataSearch.teamList.teams && dataSearch.teamList.teams.length > 0 || dataSearch.data.teams.data.length > 0 ?
                <>
                    <View style={styles.boxTitle}>
                        <Text style={[styles.txtTitle]}>{convertLanguage(language, 'team')}</Text>
                    </View>
                    <View style={{ marginHorizontal: -20 }}>
                        <ResultSearchTeam data={dataSearch.data} city_id={this.state.city.Id} navigation={this.props.navigation} noLoadMore={true} />
                    </View>
                </> : null
            }
        </>
    }
    renderVenue() {
        var { language } = this.props.language;
        var { dataSearch } = this.props.search;
        return <>
            {
                dataSearch && dataSearch.data && dataSearch.data.venues.data && dataSearch.data.venues.data.length > 0 &&
                <>
                    <View style={[styles.boxTitle]}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'venue')}</Text>
                    </View>
                    <View style={styles.boxListEvent}>
                        {
                            dataSearch.data.venues.data.map((item, index) => {
                                return <ItemEventHalf cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} key={index} />
                            })
                        }
                    </View>
                    <Touchable style={styles.boxSeeAll} onPress={() => this.ResultSearchTabView(0)} >
                        <Text style={styles.txtSeeAll}>{convertLanguage(language, 'see_more')}</Text>
                    </Touchable>
                </>
            }
        </>

    }

    onSearch(keyword) {
        Keyboard.dismiss();
        var { city } = this.state;
        if (keyword && keyword.length > 0) {
            this.props.onSearchData(keyword, city.Id, this.state.type)
            this.setState({ oldSearchValue: keyword })
        }
        else {
            SimpleToast.show('Must have at least 1 characters contain')
        }
        this.setState({ searched: true })


    }

    setSearch(searchValue) {
        this.setState({
            searchValue,
        })
    }
    reset() {
        this.setState({ searchValue: '', searched: false, oldSearchValue: '' })
    }
    showAlert() {
        var { dataSearch } = this.props.search;
        var { language } = this.props.language;
        if (dataSearch.eventList.events.length == 0 && dataSearch.eventList.events.length == 0 && dataSearch.venueList.venues.length == 0 && this.state.searchValue && this.state.searched) {
            return <View style={{ alignItems: 'center', paddingTop: 48 }}>
                <Image source={require('../../assets/search_empty.png')} style={{ width: 64, height: 60 }} />
                <Text style={{ color: '#333333', textAlign: 'center', fontSize: 12 }}>{convertLanguage(language, 'not_find_matched_result')}</Text>
            </View>
        } else {
            return <Text style={{ color: '#333333', paddingTop: 48, textAlign: 'center', fontSize: 12 }}>{convertLanguage(language, 'popular_searched_keywords')}</Text>
        }
    }
    showSuggestKeyword() {
        var { dataSearch } = this.props.search;
        if (dataSearch.data && dataSearch.data.keywords && dataSearch.data.keywords.length > 0) {
            return dataSearch.data.keywords.map((item, index) => {
                return <Touchable key={index} onPress={() => { this.onSearch(item.Keyword); this.setState({ searchValue: item.Keyword }) }} style={styles.btnSuggest}>
                    <Text style={styles.tagSuggest}>{item.Keyword}</Text>
                </Touchable>

            })
        }
    }
    render() {
        var { city } = this.state;
        var { language } = this.props.language;
        var { dataSearch } = this.props.search;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 34 / 2, fontWeight: 'bold', color: Colors.TEXT_P, justifyContent: 'center' }}>{convertLanguage(language, 'search')}</Text>
                    <View style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    </View>
                </View>

                <Line />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 16 }}>
                    <Touchable style={styles.boxItemFilter} onPress={() => this.setState({ modalCity: true })}>
                        <Text style={styles.txtFilter}>{city.Id ? city['Name_' + language] : country['Name_' + language]}</Text>
                        <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                    </Touchable>
                </View>

                <View style={[styles.boxInput, { flexDirection: 'row' }]}>

                    <TextInput
                        onChangeText={(searchValue) => this.setSearch(searchValue)}
                        style={[styles.ipContent, { flex: 1, paddingVertical: 0 }]}
                        selectionColor="#bcbcbc"
                        value={this.state.searchValue}
                        keyboardType={'default'}
                        autoCapitalize='none'
                        placeholder={convertLanguage(language, 'keyword')}
                        onSubmitEditing={() => this.onSearch(this.state.searchValue)}
                    />
                    {
                        this.state.searchValue !== this.state.oldSearchValue || !this.state.searchValue ?
                            <Touchable onPress={() => this.onSearch(this.state.searchValue)} style={{ paddingHorizontal: 12 }}>
                                <Image source={require('../../assets/search.png')} style={{ width: 24, height: 24 }} />
                            </Touchable>
                            :
                            <Touchable onPress={() => { this.reset() }} style={{ paddingHorizontal: 12 }}>
                                <Image source={require('../../assets/close_search.png')} style={{ width: 24, height: 24 }} />
                            </Touchable>
                    }

                </View>
                <View style={styles.boxFilter}>
                    <Touchable style={[styles.boxItemFilter2]} onPress={() => this.props.navigation.navigate('ResultSearchTabView', { city, searchValue: this.state.searchValue, index: 0, callback: (city, searchValue) => this.setState({ city, searchValue }) })}>
                        <Text style={styles.texttabViewDefault}>{convertLanguage(language, 'event')}</Text>
                    </Touchable>
                    <Touchable style={[styles.boxItemFilter2]} onPress={() => this.props.navigation.navigate('ResultSearchTabView', { city, searchValue: this.state.searchValue, index: 1, callback: (city, searchValue) => this.setState({ city, searchValue }) })}>
                        <Text style={styles.texttabViewDefault}>{convertLanguage(language, 'venue')}</Text>
                    </Touchable>
                    <Touchable style={[styles.boxItemFilter2]} onPress={() => this.props.navigation.navigate('ResultSearchTabView', { city, searchValue: this.state.searchValue, index: 2, callback: (city, searchValue) => this.setState({ city, searchValue }) })}>
                        <Text style={styles.texttabViewDefault}>{convertLanguage(language, 'team')}</Text>
                    </Touchable>
                </View>
                {this.props.search.loading
                    ? <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />

                    :
                    this.state.searchValue ?
                        <>
                            {
                                dataSearch.data.teams.data.length == 0 && dataSearch.data.events.data.length == 0 && dataSearch.data.venues.data.length == 0 || !this.state.searchValue ?
                                    <ScrollView>
                                        <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                                            <View style={{ alignSelf: 'center' }}>
                                                {this.props.search.loading
                                                    ? <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                                                    :
                                                    this.showAlert()}
                                            </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap', marginTop: 12 }}>
                                                {this.showSuggestKeyword()}
                                            </View>
                                        </View>

                                        {/* <View style={styles.advertise}>
                                            <View style={{ height: 156, backgroundColor: '#bebebe', marginBottom: 10 }}></View>
                                            <View style={{ height: 156, backgroundColor: 'red', marginBottom: 10 }}></View>
                                            <View style={{ height: 156, backgroundColor: '#bebebe', marginBottom: 10 }}></View>
                                        </View> */}
                                    </ScrollView>
                                    : <View style={styles.content}>
                                        <ScrollView contentContainerStyle={{ paddingHorizontal: 20, paddingVertical: 16 }} style={{ marginTop: 16 }}>
                                            {this.renderEvent()}
                                            {this.renderVenue()}
                                            {this.renderTeam()}
                                        </ScrollView>
                                    </View>
                            }
                        </>
                        : <ScrollView>
                            <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                                <View style={{ alignSelf: 'center' }}>
                                    {this.props.search.loading
                                        ? <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                                        :
                                        this.showAlert()}
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap', marginTop: 12 }}>
                                    {this.showSuggestKeyword()}

                                </View>
                            </View>

                            {/* <View style={styles.advertise}>
                                <View style={{ height: 156, backgroundColor: '#bebebe', marginBottom: 10 }}></View>
                                <View style={{ height: 156, backgroundColor: 'red', marginBottom: 10 }}></View>
                                <View style={{ height: 156, backgroundColor: '#bebebe', marginBottom: 10 }}></View>
                            </View> */}
                        </ScrollView>
                }
                {
                    this.state.modalCity && this.props.city.event_countries.length > 0
                        ?
                        <ModalCity
                            modalVisible={this.state.modalCity}
                            closeModal={() => this.setState({ modalCity: false })}
                            countries={this.props.city.event_countries}
                            selectCity={(city) => this.onSelect(city)}
                        />
                        : null
                }
            </SafeView>
        );
    }
}


const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    txtTitle: {
        color: Colors.TEXT_P,
        fontSize: 34 / 2,
        fontWeight: 'bold',
        // marginTop: 20,
    },
    boxListEvent: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        flexGrow: 2,
        justifyContent: 'space-between',
        marginTop: 16,
        paddingBottom: (62 - 42) / 2,
    },
    boxSeeAll: {
        marginBottom: 24,
        borderWidth: 1,
        borderColor: '#4F4F4F',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        height: 36
    },
    txtSeeAll: {
        fontSize: 16,
        color: '#4F4F4F',
    },
    advertise: {
        backgroundColor: "#FFF",
        paddingTop: 45
    },
    btnSuggest: {
        borderRadius: 20,
        paddingHorizontal: 12,
        paddingVertical: 4,
        backgroundColor: '#F2F2F2',
        marginHorizontal: 4,
        marginTop: 7
    },
    tagSuggest: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#757575',
        borderColor: '#757575',
    },
    ipContent: {
        fontSize: 16,
        color: '#333333',
        // paddingTop: 8,
        paddingLeft: 12
    },
    boxInput: {
        borderBottomWidth: 1,
        borderBottomColor: '#555555',
        marginHorizontal: 16,
        alignItems: 'center',
        paddingBottom: 8,
        paddingTop: 44
    },
    boxItemFilter: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginLeft: 16,
        paddingHorizontal: 12,
        borderBottomWidth: 1,
        borderBottomColor: '#4F4F4F',
        paddingBottom: 8
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: '600',
        marginRight: 30
    },
    ic_dropdown: {
        width: 16,
        height: 16
    },
    boxFilter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
        paddingBottom: 0
    },
    boxItemFilter2: {
        flex: 0.32,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 40,
        borderRadius: 4,
        borderColor: '#4F4F4F',
        borderWidth: 1
    },
    texttabViewDefault: {
        fontSize: 16,
        color: '#4F4F4F',
    },
});

const mapStateToProps = state => {
    return {
        team: state.team,
        event: state.event,
        venue: state.venue,
        city: state.city,
        language: state.language,
        like: state.like,
        search: state.search,
        global: state.global
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        followTeam: (teamId) => {
            dispatch(actTeamFollow(teamId))
        },
        onLoadDataLiked: (page) => {
            dispatch(actLoadDataLiked(page))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onLoadDataCountry: () => {
            dispatch(actSelectCountryByEvent())
        },
        onUpdateLanguage: (language) => {
            dispatch(actUpdateLanguage(language))
        },
        onUpdateSearchValue: (searchValue) => {
            dispatch(actSearch(searchValue))
        },
        onUpdateTabView: (tabView) => {
            dispatch(actUpdateTabView(tabView))
        },
        onSearchVenue: (searchValue, Id) => {
            dispatch(actSearchVenue(searchValue, Id))
        },
        onSearchData: (searchValue, Id, type) => {
            dispatch(actSearchData(searchValue, Id, type))
        },
        onClearDataSearch: () => {
            dispatch(actClearDataSearch())
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);