import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions, TextInput, Keyboard } from 'react-native';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import ModalCity from '../../components/ModalCity'
import { actLikeEvent } from '../../actions/event'
const { width, height } = Dimensions.get('window');
import { convertLanguage } from '../../services/Helper';
import Line from '../../screens_view/Line';
import { TabView } from 'react-native-tab-view';
import ResultSearchEvent from './ResultSearchEvent';
import ResultSearchTeam from './ResultSearchTeam';
import ResultSearchVenue from './ResultSearchVenue';
import { actSearchEvent, actSearchTeam, actSearchVenue, actSearch, actClearDataSearchTabview, actSearchData } from '../../actions/search';
import SimpleToast from 'react-native-simple-toast';
class EventList extends Component {
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            city: this.props.route.params['city'],
            modalCity: false,
            page: 1,
            index: this.props.route.params['index'] || 0,
            searchValue: this.props.route.params['searchValue'],
            routes: [
                { key: 'Event', title: convertLanguage(language, 'events') },
                { key: 'Venue', title: convertLanguage(language, 'venue') },
                { key: 'Team', title: convertLanguage(language, 'team') },
            ],
        };
    }
    showSuggestKeyword() {
        var { dataSearch } = this.props.search;
        if (dataSearch.data && dataSearch.data.keywords && dataSearch.data.keywords.length > 0) {
            return dataSearch.data.keywords.map((item, index) => {
                return <Touchable key={index} onPress={() => { this.onSearch(item.Keyword); this.setState({ searchValue: item.Keyword }) }} style={styles.btnSuggest}>
                    <Text style={styles.tagSuggest}>{item.Keyword}</Text>
                </Touchable>

            })
        }
    }

    _renderTabBar = props => {
        var { language } = this.props.language;
        return (
            <View style={styles.boxFilter}>
                <Touchable style={[styles.boxItemFilter2, this.state.index == 0 ? styles.backgrtabViewSelected : styles.backgrtabViewDefault]} onPress={() => this.state.index === 0 ? this.onGoBack() : this.setState({ index: 0 })}>
                    <Text style={this.state.index == 0 ? styles.texttabViewSelected : styles.texttabViewDefault}>{convertLanguage(language, 'event')}</Text>
                </Touchable>
                <Touchable style={[styles.boxItemFilter2, this.state.index == 1 ? styles.backgrtabViewSelected : styles.backgrtabViewDefault]} onPress={() => this.state.index === 1 ? this.onGoBack() : this.setState({ index: 1 })}>
                    <Text style={this.state.index == 1 ? styles.texttabViewSelected : styles.texttabViewDefault}>{convertLanguage(language, 'venue')}</Text>
                </Touchable>
                <Touchable style={[styles.boxItemFilter2, this.state.index == 2 ? styles.backgrtabViewSelected : styles.backgrtabViewDefault]} onPress={() => this.state.index === 2 ? this.onGoBack() : this.setState({ index: 2 })}>
                    <Text style={this.state.index == 2 ? styles.texttabViewSelected : styles.texttabViewDefault}>{convertLanguage(language, 'team')}</Text>
                </Touchable>
            </View>
        );
    };

    onSelect(city) {
        this.setState({
            city,
            page: 1,
            modalCity: false,
        })
        Keyboard.dismiss();
        if (this.state.searchValue && this.state.searchValue.length > 0) {
            this.props.onSearchData(this.state.searchValue, city.Id, 'city')
        }
        else {
            SimpleToast.show('Must have at least 1 characters contain')
        }
    }
    onSearch(key) {
        Keyboard.dismiss();
        this.setState({ searchValue: key })
        if (key && key.length > 0) {
            this.props.onSearchData(key, this.state.city.Id, 'city')
            this.props.onUpdateSearchValue(key)
        }
        else {
            SimpleToast.show('Must have at least 1 characters contain')
        }
    }

    onGoBack() {
        const { city, searchValue } = this.state;
        this.props.route.params['callback'](city, searchValue);
        this.props.navigation.goBack()
    }

    render() {
        var { language } = this.props.language;
        var { city } = this.state;
        return (
            <View style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <SafeAreaConsumer>
                    {insets => <View style={{ paddingTop: insets.top }} />}
                </SafeAreaConsumer>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.pop(2)}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 17, fontWeight: 'bold', color: Colors.TEXT_P, justifyContent: 'center' }}>{convertLanguage(language, 'search')}</Text>
                    <View style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    </View>
                </View>

                <Line />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 16 }}>
                    <Touchable style={styles.boxItemFilter} onPress={() => this.setState({ modalCity: true })}>
                        <Text style={styles.txtFilter}>{city.Id ? city['Name_' + language] : country['Name_' + language]}</Text>
                        <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                    </Touchable>
                </View>

                <View style={[styles.boxInput, { flexDirection: 'row' }]}>
                    <TextInput
                        onChangeText={(searchValue) => this.setState({ searchValue })}
                        style={[styles.ipContent, { flex: 1, paddingVertical: 0 }]}
                        selectionColor="#bcbcbc"
                        value={this.state.searchValue}
                        keyboardType={'default'}
                        autoCapitalize='none'
                        placeholder={convertLanguage(language, 'keyword')}
                        onSubmitEditing={() => this.onSearch(this.state.searchValue)}
                    />
                    {
                        this.state.searchValue !== this.props.search.searchValue || !this.state.searchValue ?
                            <Touchable onPress={() => this.onSearch(this.state.searchValue)} style={{ paddingHorizontal: 12 }}>
                                <Image source={require('../../assets/search.png')} style={{ width: 24, height: 24 }} />
                            </Touchable>
                            :
                            <Touchable onPress={() => this.setState({ searchValue: '' })} style={{ paddingHorizontal: 12 }}>
                                <Image source={require('../../assets/close_search.png')} style={{ width: 24, height: 24 }} />
                            </Touchable>
                    }
                </View>
                <TabView
                    lazy={true}
                    navigationState={this.state}
                    labelStyle={{ backgroundColor: 'red' }}
                    renderScene={({ route }) => {
                        switch (route.key) {
                            case 'Event':
                                return <ResultSearchEvent style_={[{ paddingBottom: 16 }]} searchValue={this.state.searchValue} city_id={this.state.city.Id} showSuggestKeyword={this.showSuggestKeyword()} onSearch={(searchValue) => this.onSearch(searchValue)} navigation={this.props.navigation} />
                            case 'Venue':
                                return <ResultSearchVenue style_={[{ paddingBottom: 16 }]} searchValue={this.state.searchValue} city_id={this.state.city.Id} showSuggestKeyword={this.showSuggestKeyword()} onSearch={(searchValue) => this.onSearch(searchValue)} navigation={this.props.navigation} />
                            case 'Team':
                                return <ResultSearchTeam style_={[{ paddingBottom: 16 }]} searchValue={this.state.searchValue} data={null} city_id={this.state.city.Id} showSuggestKeyword={this.showSuggestKeyword()} onSearch={(searchValue) => this.onSearch(searchValue)} navigation={this.props.navigation} />
                        }
                    }}
                    onIndexChange={index => this.setState({ index })}
                    renderTabBar={this._renderTabBar}
                />

                {
                    this.state.modalCity && this.props.city.event_countries.length > 0
                        ?
                        <ModalCity
                            modalVisible={this.state.modalCity}
                            closeModal={() => this.setState({ modalCity: false })}
                            countries={this.props.city.event_countries}
                            selectCity={(city) => this.onSelect(city)}
                        />
                        : null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    ipContent: {
        fontSize: 16,
        color: '#333333',
        paddingLeft: 12
    },
    btnSuggest: {
        borderRadius: 20,
        paddingHorizontal: 12,
        paddingVertical: 4,
        backgroundColor: '#F2F2F2',
        marginHorizontal: 4,
        marginTop: 7
    },
    tagSuggest: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#757575',
        borderColor: '#757575',
    },
    backgrtabViewDefault: {
        borderRadius: 4,
        borderColor: '#4F4F4F',
        borderWidth: 1
    },
    texttabViewDefault: {
        fontSize: 16,
        color: '#4F4F4F',
    },
    backgrtabViewSelected: {
        backgroundColor: '#03a9f4',
        borderWidth: 1,
        borderColor: '#03a9f4',
        borderRadius: 4
    },
    texttabViewSelected: {
        fontSize: 16,
        color: 'white',
    },
    boxInput: {
        borderBottomWidth: 1,
        borderBottomColor: '#555555',
        marginHorizontal: 16,
        alignItems: 'center',
        paddingBottom: 8,
        paddingTop: 44
    },
    content: {
        flex: 1,
    },
    imgBanner: {
        width: width,
        height: width * 9 / 16,
        marginBottom: 20,
    },
    boxFilter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20
    },
    boxViewMaps: {
        width: 54,
        height: 54,
        borderRadius: 27,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'black',
        shadowRadius: 5,
        shadowOpacity: 0.3,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        elevation: 5,
        bottom: 20,
        right: 20,
        backgroundColor: '#FFFFFF'
    },
    pinIcon: {
        width: 37,
        height: 45
    },
    boxItemFilter2: {
        flex: 0.32,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 40,
        borderRadius: 4,
    },
    boxItemFilter: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginLeft: 16,
        paddingHorizontal: 12,
        borderBottomWidth: 1,
        borderBottomColor: '#4F4F4F',
        paddingBottom: 8
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: '600',
        marginRight: 30
    },
    ic_dropdown: {
        width: 16,
        height: 16
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        category: state.category,
        city: state.city,
        language: state.language,
        search: state.search,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onLoadDataCategory: (type) => {
            dispatch(actLoadDataCategory(type))
        },
        onSearchEvent: (searchValue, Id) => {
            dispatch(actSearchEvent(searchValue, Id))
        },
        onSearchTeam: (searchValue, Id, page, per_page) => {
            dispatch(actSearchTeam(searchValue, Id, page, per_page))
        },
        onSearchVenue: (searchValue, Id) => {
            dispatch(actSearchVenue(searchValue, Id))
        },
        onUpdateSearchValue: (searchValue) => {
            dispatch(actSearch(searchValue))
        },
        onClearData: () => {
            dispatch(actClearDataSearchTabview())
        },
        onSearchData: (searchValue, Id, type) => {
            dispatch(actSearchData(searchValue, Id, type))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventList);
// export default EventHostList;
