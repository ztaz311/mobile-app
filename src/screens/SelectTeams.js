import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import SafeView from '../screens_view/SafeView'
import Touchable from '../screens_view/Touchable'
import ScrView from '../screens_view/ScrView'
import * as Colors from '../constants/Colors'
import { convertLanguage } from '../services/Helper'
const base64 = require('base-64');
import { connect } from "react-redux";
import * as Config from '../constants/Config';
class SelectTeams extends Component {
    constructor(props) {
        super(props);

        const selected = this.props.route.params['data']

        this.state = {
            loading: false,
            data: [],
            selected: selected ? selected : []
        };

        this.getData()
    }

    getData = async () => {
        const auKey = await AsyncStorage.getItem('auKey')
        const auValue = await AsyncStorage.getItem('auValue')
        let headers = {}
        headers[auKey] = auValue

        this.setState({
            loading: true
        })
        fetch(Config.API_URL + "/tag-categories?type=team", {
            headers: headers
        }).then(async (response) => {
            console.log(response)
            if (response.status == 200) {
                response = await response.json()
                if (response.status == 200) {
                    this.setState({ data: response.data })
                } else {
                    Toast.showWithGravity((response.errors.message), Toast.SHORT, Toast.TOP)
                }
            } else {
                alert('Cannot connect to server.')
            }

            this.setState({ loading: false })
        }).catch(err => {
            console.log(err)
            this.setState({ loading: false })
        })
    }

    should = () => {
        return this.state.selected.length > 0
    }

    save = () => {
        const { navigation } = this.props
        this.props.route.params['callback'](this.state.selected)
        navigation.goBack()
    }

    createList = () => {
        let arr = []
        let arrE = []
        this.state.data.forEach((e, index) => {
            if (arrE.length < 3) {
                arrE.push(e)
            }

            if (arrE.length >= 3 || index <= this.state.length - 1) {
                arr.push(this.createList_Row(arrE))
                arrE = []
            }
        });
        return arr
    }

    createList_Row = (arr) => {
        while (arr.length < 3) {
            arr.push(null)
        }
        return <View style={{ flexDirection: 'row' }}>
            {arr.map((value) => this.createList_Item(value))}
        </View>
    }

    createList_Item = (data) => {
        if (data) {
            return <Child
                total={this.state.selected.length}
                name={data.HashTagName}
                selected={this.state.selected.findIndex(this.find, data.Id) >= 0}
                first={this.state.selected.some(el => el.Id === data.Id) && this.state.selected[0].Id === data.Id}
                onPress={() => {
                    let { selected } = this.state

                    const index = selected.findIndex(this.find, data.Id)
                    if (index >= 0) selected.splice(index, 1)
                    else selected.push(data)

                    this.setState({ selected })
                }}
            />
        }
        return <View style={{ flex: 1, margin: 6 }} />
    }

    find(value, index, arr) {
        return value.Id == this
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1, alignItems: 'center' }}>
                <Touchable
                    onPress={() => this.props.navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, alignItems: 'center', justifyContent: 'center', alignSelf: 'flex-start' }}>
                    <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                </Touchable>

                <Text style={{ marginTop: 20, fontSize: 20, color: Colors.TEXT_P, textAlign: 'center', fontWeight: 'bold' }}>{convertLanguage(language, 'select_your_team_specialities')}</Text>
                <Text style={{ marginTop: 20, fontSize: 16, color: Colors.TEXT_P, textAlign: 'center', marginBottom: 15 }}>{convertLanguage(language, 'first_select_filed_will_be_the_primary_filed')}</Text>

                <ScrView contentContainerStyle={{ marginLeft: 14, marginRight: 14, marginTop: 15 }} showsVerticalScrollIndicator={false}>
                    {this.createList()}
                    {/* <View style={{ height: 30 }} /> */}
                </ScrView>

                <Touchable
                    onPress={this.save}
                    style={{
                        marginBottom: 30, marginTop: 20,
                        marginLeft: 20, marginRight: 20,
                        alignSelf: 'stretch',
                        backgroundColor: this.should() ? Colors.PRIMARY : '#FFFFFF',
                        minHeight: 50, borderColor: this.should() ? Colors.PRIMARY : Colors.DISABLE,
                        borderWidth: 1,
                        borderRadius: 4,
                        justifyContent: 'center', alignItems: 'center',
                    }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: this.should() ? Colors.BG : Colors.DISABLE }}>{this.state.loading ? convertLanguage(language, 'waiting') : convertLanguage(language, 'save')}</Text>
                </Touchable>

            </SafeView>
        );
    }
}

class Child extends Component {
    render() {
        return <TouchableOpacity
            onPress={this.props.onPress}
            disabled={!this.props.selected && this.props.total >= 10}
            style={{

                //#FFFFFF       #e8e8e8
                minHeight: 40, margin: 6,
                flex: 1, borderRadius: 4, backgroundColor: this.props.first ? Colors.PRIMARY : this.props.selected ? '#FFFFFF' : '#FFFFFF', justifyContent: 'center', alignItems: 'center',
                borderWidth: this.props.selected ? 1 : 1, borderColor: this.props.selected ? Colors.PRIMARY : '#4F4F4F'
            }}>
            <Text style={{ fontWeight: this.props.selected ? 'bold' : '', color: this.props.first ? 'white' : this.props.selected ? '#03a9f4' : Colors.BLACK }}>{this.props.name}</Text>
        </TouchableOpacity>
    }
}

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
export default connect(mapStateToProps, null)(SelectTeams);
