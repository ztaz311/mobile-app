import React, { Component } from 'react';
import { StyleSheet, Image } from 'react-native';
import TabNavigator from 'react-native-tab-navigator';

import * as Colors from '../constants/Colors';
import Home from './Home';
import LikePage from './liked/LikePage';
import Ticket from './tickets/MyTicket';
import News from './news/News';
import MyPage from './profile/MyPage';
import { convertLanguage } from '../services/Helper';
import { actShowModalLogin } from '../actions/global';
import { connect } from "react-redux";
class Main extends Component {
    constructor(props) {
		super(props);
		this.state = {
			page: this.props.route.params && this.props.route.params['page'] ? this.props.route.params['page'] : 'home',
		};
	}

    changeTab(page) {
        var { profile } = this.props.user;
        if((page === 'like' || page === 'ticket' || page === 'news' || page === 'my') && !profile.Id) {
            this.props.onShowModalLogin();
        } else {
            this.setState({ page: page });
        }
	}

    render() {
        var { language } = this.props.language;
        return (
            <TabNavigator tabBarStyle={styles.tabNavigator}>
                <TabNavigator.Item
                    selected={this.state.page === 'home'}
                    renderIcon={() => <Image style={styles.ic_home} source={require('../assets/1_home_blk_.png')} />}
                    renderSelectedIcon={() => <Image style={styles.ic_home} source={require('../assets/1_home_blu_.png')} />}
                    title={convertLanguage(language, 'home').toUpperCase()}
                    tabStyle={styles.tabStyle}
                    titleStyle={styles.titleStyle}
                    selectedTitleStyle={styles.selectedTitleStyle}
                    onPress={() => this.changeTab('home')}>
                    <Home navigation={this.props.navigation} />
                </TabNavigator.Item>

                <TabNavigator.Item
                    selected={this.state.page === 'like'}
                    renderIcon={() => <Image style={styles.ic_home} source={require('../assets/2_bookmark_blk_.png')} />}
                    renderSelectedIcon={() => <Image style={styles.ic_home} source={require('../assets/2_bookmark_blu_.png')} />}
                    title={convertLanguage(language, 'liked').toUpperCase()}
                    tabStyle={styles.tabStyle}
                    titleStyle={styles.titleStyle}
                    selectedTitleStyle={styles.selectedTitleStyle}
                    onPress={() => this.changeTab('like')}>
                    <LikePage navigation={this.props.navigation} />
                </TabNavigator.Item>

                {
                    this.props.user.profile.Id &&
                    <TabNavigator.Item
                        selected={this.state.page === 'ticket'}
                        renderIcon={() => <Image style={styles.ic_home} source={require('../assets/3_ticket_blk_.png')} />}
                        renderSelectedIcon={() => <Image style={styles.ic_home} source={require('../assets/3_ticket_blu_.png')} />}
                        title={convertLanguage(language, 'ticket').toUpperCase()}
                        tabStyle={styles.tabStyle}
                        titleStyle={styles.titleStyle}
                        selectedTitleStyle={styles.selectedTitleStyle}
                        onPress={() => this.changeTab('ticket')}>
                        <Ticket navigation={this.props.navigation} />
                    </TabNavigator.Item>
                }
                <TabNavigator.Item
                    selected={this.state.page === 'news'}
                    renderIcon={() => <Image style={styles.ic_home} source={require('../assets/4_news_blk_.png')} />}
                    renderSelectedIcon={() => <Image style={styles.ic_home} source={require('../assets/4_news_blu_.png')} />}
                    title={convertLanguage(language, 'news').toUpperCase()}
                    tabStyle={styles.tabStyle}
                    titleStyle={styles.titleStyle}
                    selectedTitleStyle={styles.selectedTitleStyle}
                    onPress={() => this.changeTab('news')}>
                    <News navigation={this.props.navigation} />
                </TabNavigator.Item>
                <TabNavigator.Item
                    selected={this.state.page === 'my'}
                    renderIcon={() => <Image style={styles.ic_home} source={require('../assets/5_my_blk_.png')} />}
                    renderSelectedIcon={() => <Image style={styles.ic_home} source={require('../assets/5_my_blu_.png')} />}
                    title={convertLanguage(language, 'my').toUpperCase()}
                    tabStyle={styles.tabStyle}
                    titleStyle={styles.titleStyle}
                    selectedTitleStyle={styles.selectedTitleStyle}
                    onPress={() => this.changeTab('my')}>
                    <MyPage navigation={this.props.navigation} />
                </TabNavigator.Item>
            </TabNavigator>
        );
    }
}

const styles = StyleSheet.create({
	tabNavigator: {
		backgroundColor: "#FFFFFF",
	    justifyContent: 'center',
	    flex: 1,
	    height: 54,
	    borderTopColor: 'rgb(236, 238, 240)',
	    borderTopWidth: 1,
	},
	ic_home: {
	    width: 25,
	    height: 22
	},
	tabStyle: {
	    paddingBottom: 0,
	    justifyContent: 'center'
	},
	titleStyle: {
	    color: Colors.TEXT_P
	},
	selectedTitleStyle: {
	    color: Colors.PRIMARY
	},
});

const mapStateToProps = state => {
    return {
        user: state.user,
        language: state.language,
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onShowModalLogin: () => {
            dispatch(actShowModalLogin())
        }

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
