import React, { Component } from 'react';
import { Text, StyleSheet, ActivityIndicator, ScrollView } from 'react-native';
import { actListMemberFollowers, actInvitedMember } from '../actions/team';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
import MemberTeamItem from '../components/member/MemberInviteItem'

class TeamTabFollowers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: ''
        };
    }

    componentWillMount() {
        this.props.listMemberFollowers(this.props.team.team.Id);
    }

    submitInvitedMember(memberId) {
        this.props.invitedMember(this.props.team.team.Id, memberId, this.props.navigation);
    }

    renderMember() {
        var members = this.props.team.team.Followers;
        if (!members) {
            return null;
        }
        return members.map((member, index) => {
            return <MemberTeamItem key={index} member={member}
                        invitedMember={() => {this.submitInvitedMember(member.Id)}}
                    />
        });
    }

    render() {
        var { language } = this.props.language;
        let team = this.props.team.team;
        return (
            <ScrollView style={styles.content}>
                <Text style={styles.txtTotal}> {team.Followers ? team.Followers.length : 0} {convertLanguage(language, 'follower')}</Text>
                {
                        this.props.team.loadingFollowers ? 
                            <ActivityIndicator size="large" />
                        : null
                    }
                {
                    this.renderMember()
                }
            </ScrollView>
        );
    }
}
  const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    rowSearch: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    inputSearch: {
        flex: 1,
        paddingBottom: 10,
        borderBottomWidth: 1,
        fontSize: 16,
        borderColor: '#383838'
    },
    imgSearch: {
        marginLeft: 20
    },
    txtTotal: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 15
    },
    content: {
        flex: 1,
        padding: 20,
        paddingTop: 45,
    },
  });
const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        listMemberFollowers: (teamId) => {
            dispatch(actListMemberFollowers(teamId))
        },
        invitedMember: (teamId, memberId, navigation) => {
            dispatch(actInvitedMember(teamId, memberId, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamTabFollowers);
