import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Alert, ActivityIndicator, Dimensions, Linking, IntentAndroid } from 'react-native';
import { WebView } from "react-native-webview";
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
const { width, height } = Dimensions.get('window')
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import * as Config from '../../constants/Config';
var SendIntentAndroid = require("react-native-send-intent");
import firebase from 'react-native-firebase';
class PaymentOnline extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    onMessage = (data) => {
        var rels = JSON.parse(data.nativeEvent.data);
        if (rels.status == 200) {
            var purchaseInfo = rels.purchaseInfo;
            purchaseInfo.value = parseInt(purchaseInfo.value);
            firebase.analytics().logEvent('purchase', purchaseInfo);
            this.props.navigation.navigate('PaymentSuccess');
        } else {
            this.props.navigation.goBack();
        }
    }

    goBack() {
        Alert.alert(
            'Cancel Payment',
            'Are you sure to Cancel Payment?',
            [
                {
                    text: 'NO',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'YES', onPress: () => this.props.navigation.goBack() },
            ],
            { cancelable: false },
        );
    }

    renderLoading() {
        return <View style={{ width, height: height - 100, position: 'absolute', justifyContent: 'center', alignItems: 'center', zIndex: 10 }}>
            <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        </View>
    }

    convertIntentURL(url) {
        var n1 = url.indexOf("package");
        var n2 = url.indexOf(";end");
        var res = url.substring(n1+8, n2);
        return res;
    }

    render() {
        var { CodePayment } = this.props.route.params;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, position: 'relative', zIndex: 10 }}>
                    <View style={{ width: 50 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>Payment</Text>
                    <Touchable
                        onPress={() => {
                            this.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    {
                        Platform.OS === 'android' ?
                            <WebView
                                source={{ uri: Config.API_URL + '/payment/' + CodePayment + '/pay' }}
                                onMessage={(data) => this.onMessage(data)}
                                startInLoadingState
                                renderLoading={this.renderLoading}
                                originWhitelist={['*']}
                                onShouldStartLoadWithRequest={event => {
                                    const { url } = event;
                                    if(url.startsWith('http://') || url.startsWith('https://')) {
                                        return true;
                                    }
                                    // console.log(url)
                                    if (url.startsWith('intent://')) {
                                        SendIntentAndroid.openChromeIntent(url).then((wasOpen) => {
                                            if(!wasOpen) {
                                                Linking.openURL(`market://details?id=${this.convertIntentURL(url)}`)
                                            }
                                        })
                                        return false;
                                    }
                                    Linking.openURL(url)
                                    return false;
                                }}
                            />
                            :
                            <WebView
                                source={{ uri: Config.API_URL + '/payment/' + CodePayment + '/pay' }}
                                onMessage={(data) => this.onMessage(data)}
                                startInLoadingState
                                renderLoading={this.renderLoading}
                                onShouldStartLoadWithRequest={event => {
                                    const { url } = event;
                                    console.log(url)
                                    return true;
                                }}
                            />
                    }
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        zIndex: 9
    },
});

const mapStateToProps = state => {
    return {
        // team: state.team
    };
}
// const mapDispatchToProps = (dispatch, props) => {
//     return {


//     }
// }
export default connect(mapStateToProps, null)(PaymentOnline);
// export default EventHostList;
