import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Platform, FlatList, StyleSheet, StatusBar, Image, Dimensions } from 'react-native';
import FastImage from 'react-native-fast-image';
import { actGlobalCheckLogin } from '../../actions/global';
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import ItemMyTicket from '../../components/ticket/ItemMyTicket'
import { actLoadDataMyticket } from '../../actions/ticket'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { convertLanguage, convertDate3 } from '../../services/Helper';
import NetInfo from "@react-native-community/netinfo";
import NoInternet from '../../screens_view/NoInternet';
const { width, height } = Dimensions.get('window')
class MyTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            isConnected: true
        };
    }

    componentDidMount() {
        // this.props.navigation.addListener('blur', () => alert('Screen was focused'));
        this.props.navigation.setParams({
            scrollToTop: this._scrollToTop,
        });
        this.checkConnect()
    }

    checkConnect() {
        this.setState({ isConnected: true })
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                var { profile } = this.props.user;
                if (profile.Id) {
                    var { page } = this.state;
                    this.props.onLoadDataMyticket(page);
                }
            } else {
                this.setState({ isConnected: false })
            }
        });
    }

    _scrollToTop = () => {
        // Scroll to top, in this case I am using FlatList
        if (!!this.refs.listRef) {
            this.refs.listRef.scrollToOffset({ x: 0, y: 0, animated: true })
        }
    }

    refresh() {
        this.props.onLoadDataMyticket(1);
    }

    _renderFooter() {
        var { language } = this.props.language;
        var { loading, mytickets } = this.props.ticket;
        if (loading) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            return <View>
                {
                    mytickets.length === 0 &&
                    <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'ticket_empty')}</Text>
                }
                <Touchable style={styles.boxPastTicket} onPress={() => this.props.navigation.navigate('MyPastTicket')}>
                    <Text style={{ color: '#4F4F4F', fontSize: 16 }}>{convertLanguage(language, 'past_tickets')}</Text>
                </Touchable>
            </View>
        }
    }

    renderItem = (item, indexTicket) => {
        var { language } = this.props.language;
        var { event, tickets } = item;
        return <View style={styles.boxSlide}>
            <Touchable onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: event.Slug }, key: event.Slug })}>
                <FastImage style={styles.boxThumb} source={{ uri: event.Posters ? event.Posters.Medium : event.Poster }} >
                    {
                        event.Status == 0
                        &&
                        <View style={styles.boxCancel}>
                            <Image source={require('../../assets/canceled_event.png')} style={styles.imgCancel} />
                        </View>
                    }
                </FastImage>
            </Touchable>
            <Touchable style={styles.boxContent} onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: event.Slug }, key: event.Slug })}>
                <Text style={styles.txtName}>{event.Title.length > 50 ? event.Title.substring(0, 50) + "..." : event.Title}</Text>
                <Text style={styles.txtTime}>{convertDate3(event.TimeStart, event.TimeFinish, language)}</Text>
                <Text style={styles.txtTime}>{event.VenueName.length > 50 ? event.VenueName.substring(0, 50) + ".." : event.VenueName} - {event.Address.length > 50 ? event.Address.substring(0, 50) + ".." : event.Address}</Text>
            </Touchable>
            {
                tickets && tickets.length > 0
                &&
                <View style={{ paddingHorizontal: 16 }}>
                    {
                        tickets.map((item, index) => {
                            return index < 10 ? <ItemMyTicket key={index} total={tickets.length} navigation={this.props.navigation} type={'current'} index={index} item={item} event={event} indexTicket={indexTicket} /> : null
                        })
                    }
                </View>
            }
            {
                tickets.length > 10 &&
                <Touchable style={styles.boxViewAll} onPress={() => this.props.navigation.navigate('TicketByEvent', { indexTicket, type: 'current' })} >
                    <Text style={styles.txtViewAll}>{convertLanguage(language, 'view_all_ticket')}</Text>
                    <Image source={require('../../assets/right_blue.png')} style={styles.icRight} />
                </Touchable>
            }
        </View>
    }

    render() {
        StatusBar.setBarStyle(Platform.OS === 'android' ? 'light-content' : 'dark-content', true);
        var { language } = this.props.language;
        var { profile } = this.props.user;
        var { isConnected } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, minHeight: 50 }}>
                    <View style={{ width: 84 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'my_ticket')}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Touchable style={{ minHeight: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.globalCheckLogin('News')}>
                            <Image source={profile.isNewNotification ? require('../../assets/notification_active.png') : require('../../assets/notification.png')} style={{ width: 32, height: 32 }} />
                        </Touchable>
                        <Touchable style={{ minHeight: 50, minWidth: 50, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')}>
                            <Image source={require('../../assets/search.png')} style={{ width: 32, height: 32 }} />
                        </Touchable>
                    </View>
                </View>

                <Line />
                {
                    !isConnected ?
                        <NoInternet onPress={() => this.checkConnect()} />
                        :
                        profile.Id ?
                            <View style={styles.content}>
                                <FlatList
                                    ref="listRef"
                                    contentContainerStyle={{ paddingTop: 24 }}
                                    data={this.props.ticket.mytickets}
                                    renderItem={({ item, index }) => {
                                        return this.renderItem(item, index)
                                    }}
                                    onEndReachedThreshold={0.5}
                                    keyExtractor={(item, index) => index.toString()}
                                    ListFooterComponent={() => this._renderFooter()}
                                    onRefresh={() => { this.refresh() }}
                                    refreshing={false}
                                />
                            </View>
                            :
                            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                                <Touchable
                                    onPress={() => this.props.navigation.navigate('Login')}
                                    style={{ alignSelf: 'center', borderRadius: 4, marginTop: 20, marginBottom: 20, paddingLeft: 40, paddingRight: 40, paddingTop: 10, paddingBottom: 10, backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>{convertLanguage(language, 'login')}</Text>
                                </Touchable>
                            </View>
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxPastTicket: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginHorizontal: 16,
        height: 48,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#4F4F4F',
        marginTop: 20,
        marginBottom: 20
    },
    boxSlide: {
        // flex: 1,
        marginHorizontal: 8,
        paddingBottom: 20,
        paddingTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.17,
        shadowRadius: 7.49,
        elevation: 12,
        backgroundColor: '#FFFFFF',
        marginVertical: 10,
        borderRadius: 4
    },
    boxContent: {
        marginHorizontal: 16,
        borderBottomColor: '#00A9F4',
        borderBottomWidth: 1,
        paddingBottom: 12
    },
    boxThumb: {
        width: width - 48,
        aspectRatio: 296 / 166,
        // alignItems: 'flex-end',
        // justifyContent: 'flex-end',
        marginBottom: 10,
        backgroundColor: '#BDBDBD',
        alignSelf: 'center',
        borderRadius: 4
    },
    boxCancel: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width - 64,
        height: (width - 64) * 166 / 296,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    imgCancel: {
        width: width - 64,
        height: (width - 64) * 166 / 296,
        // aspectRatio: 296 / 166,
    },
    boxTicket: {
        margin: 13,
        padding: 18,
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 1,
        borderColor: '#068ad2',
        backgroundColor: '#03a9f4',
        alignItems: 'center',
        justifyContent: "space-between"
    },
    icon_ticket: {
        width: 25,
        height: 18
    },
    txt_ticket: {
        color: '#FFFFFF',
        fontSize: 17
    },
    num_ticket: {
        fontSize: 26
    },
    txtName: {
        fontSize: 24,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtTime: {
        fontSize: 16,
        color: '#828282',
        marginBottom: 5
    },
    boxViewAll: {
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#00A9F4',
        borderRadius: 4,
        marginTop: 16,
        marginHorizontal: 16,
        marginBottom: 6
    },
    txtViewAll: {
        color: '#00A9F4',
        fontSize: 16
    },
    icRight: {
        width: 18,
        height: 18,
        marginLeft: 12
    },
});

const mapStateToProps = state => {
    return {
        ticket: state.ticket,
        language: state.language,
        user: state.user
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataMyticket: (page) => {
            dispatch(actLoadDataMyticket(page))
        },
        globalCheckLogin: (nextPage) => {
            dispatch(actGlobalCheckLogin(nextPage))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MyTicket);
// export default EventHostList;
