import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions } from 'react-native';
import FastImage from 'react-native-fast-image';
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import ItemMyTicket from '../../components/ticket/ItemMyTicket'
import { actLoadDataMyPastTicket } from '../../actions/ticket'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
const { width, height } = Dimensions.get('window')
class MyPastTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
    }

    componentDidMount() {
        var { page } = this.state;
        this.props.onLoadDataMyPastTicket(page);
    }

    _renderFooter() {
        var { loadingPastTicket, myPastTickets } = this.props.ticket;
        var { language } = this.props.language;
        if (loadingPastTicket) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            return <View style={{alignItems: 'center'}}>
                {
                    myPastTickets.length === 0 &&
                    <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'ticket_empty')}</Text>
                }
            </View>
        }
    }

    refresh() {
        this.props.onLoadDataMyPastTicket(1);
    }

    renderItem = (item, indexTicket) => {
        var { language } = this.props.language;
        var { event, tickets } = item;
        return <View style={styles.boxSlide}>
            <Touchable onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: event.Slug }, key: event.Slug })}>
                <FastImage style={styles.boxThumb} source={{ uri: event.Posters ? event.Posters.Medium : event.Poster }} >
                    {
                        event.Status == 0
                        &&
                        <View style={styles.boxCancel}>
                            <Image source={require('../../assets/canceled_event.png')} style={styles.imgCancel} />
                        </View>
                    }
                </FastImage>
            </Touchable>
            <Touchable style={styles.boxContent} onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: event.Slug }, key: event.Slug })}>
                <Text style={styles.txtName}>{event.Title.length > 50 ? event.Title.substring(0, 50) + "..." : event.Title}</Text>
                <Text style={styles.txtTime}>{convertDate(event.TimeStart, event.TimeFinish)}</Text>
                <Text style={styles.txtTime}>{event.VenueName.length > 50 ? event.VenueName.substring(0, 50) + ".." : event.VenueName} - {event.Address.length > 50 ? event.Address.substring(0, 50) + ".." : event.Address}</Text>
            </Touchable>
            {
                // tickets && tickets.length > 0
                // &&
                // <View style={{paddingHorizontal: 12}}>
                //     {
                //         tickets.map((item, index) => {
                //             return index < 10 ? <ItemMyTicket navigation={this.props.navigation} total={tickets.length} key={index} type={'past'} index={index} item={item} event={event} indexTicket={indexTicket} /> : null
                //         })
                //     }
                // </View>
            }
            {
                tickets.length > 2 &&
                <Touchable style={styles.boxViewAll} onPress={() => this.props.navigation.navigate('TicketByEvent', { indexTicket, type: 'past' })} >
                    <Text style={styles.txtViewAll}>{convertLanguage(language, 'all_past_ticket')}</Text>
                    <Image source={require('../../assets/right.png')} style={styles.icRight} />
                </Touchable>
            }
        </View>
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'past_ticket')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <View style={styles.content}>
                    <FlatList
                        data={this.props.ticket.myPastTickets}
                        renderItem={({ item, index }) => {
                            return this.renderItem(item, index)
                        }}
                        onEndReachedThreshold={0.1}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => this._renderFooter()}
                        onRefresh={() => {this.refresh()}}
                        refreshing={false}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxPastTicket: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        height: 30,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        marginTop: 20,
        marginBottom: 20
    },
    boxSlide: {
        // flex: 1,
        marginHorizontal: 16,
        paddingBottom: 10,
        paddingTop: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.17,
        shadowRadius: 7.49,
        elevation: 12,
        backgroundColor: '#FFFFFF',
        marginVertical: 10,
        borderRadius: 4
    },
    boxContent: {
        marginHorizontal: 16,
        borderBottomColor: 'rgba(0,169,244,0.25);',
        borderBottomWidth: 1,
        paddingBottom: 12
    },
    boxThumb: {
        width: width - 64,
        aspectRatio: 296 / 166,
        // alignItems: 'flex-end',
        // justifyContent: 'flex-end',
        marginBottom: 10,
        backgroundColor: '#BDBDBD',
        alignSelf: 'center',
        borderRadius: 4
    },
    boxCancel: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width - 64,
        height: (width - 64) * 166 / 296,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    imgCancel: {
        width: width - 64,
        height: (width - 64) * 166 / 296,
        // aspectRatio: 296 / 166,
    },
    boxTicket: {
        margin: 13,
        padding: 18,
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 1,
        borderColor: '#068ad2',
        backgroundColor: '#03a9f4',
        alignItems: 'center',
        justifyContent: "space-between"
    },
    icon_ticket: {
        width: 25,
        height: 18
    },
    txt_ticket: {
        color: '#FFFFFF',
        fontSize: 17
    },
    num_ticket: {
        fontSize: 26
    },
    txtName: {
        fontSize: 24,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtTime: {
        fontSize: 16,
        color: '#828282',
        marginBottom: 5
    },
    boxViewAll: {
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 6,
        marginTop: 24,
        marginHorizontal: 16
    },
    txtViewAll: {
        color: '#333333',
        fontSize: 16
    },
    icRight: {
        width: 18,
        height: 18,
        marginLeft: 4
    },
});

const mapStateToProps = state => {
    return {
        ticket: state.ticket,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataMyPastTicket: (page) => {
            dispatch(actLoadDataMyPastTicket(page))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MyPastTicket);
// export default EventHostList;
