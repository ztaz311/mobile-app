import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Dimensions, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import { formatNumber, convertLanguage } from '../../services/Helper';
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
const { width, height } = Dimensions.get('window');
import { actLoadPurchaseDetail, actPaymentCod, actPaymentCodFree, actPaymentETicketFree } from '../../actions/payment';
class PurchaseDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.props.onLoadPurchaseDetail();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    getTotal(tickets) {
        var total = 0;
        tickets.forEach((tickets) => {
            total = total + parseInt(tickets.purchaseTotalMoney);
        })
        return total;
    }

    paymentCod() {
        this.props.onPaymentCod();
    }

    paymentCodFree() {
        this.props.onPaymentCodFree();
    }

    paymentETicketFree() {
        this.props.onPaymentETicketFree();
    }

    render() {
        var { data, is_buy_cod, loading_buy_cod, loading_e_ticket_free } = this.props.payment;
        var delivery_tickets = data.data.filter(el => el.Type === 'd-p-ticket')
        var e_tickets = data.data.filter(el => el.Type === 'e-ticket')
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, position: 'relative', zIndex: 10 }}>
                    <View style={{ width: 50 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'language')}</Text>
                    <Touchable
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <View style={{flex: 1}}>
                    {
                        loading_e_ticket_free &&
                        <View style={{position: 'absolute', backgroundColor: 'rgba(0,0,0,0.3)', left: 0, top: 0, width, height, zIndex: 9, alignItems: 'center', justifyContent: 'center'}}>
                            <ActivityIndicator size="large" color="#FFFFFF" style={{ marginBottom: 50 }}/>
                        </View>
                    }
                    <ScrollView style={styles.container}>
                        <View style={styles.content}>
                            {
                                delivery_tickets.length > 0
                                &&
                                <View style={styles.boxDelivery}>
                                    <Text style={styles.txtDelivery}>{convertLanguage(language, 'available_ticket_for_cash_on_delivery')}</Text>
                                    {
                                        delivery_tickets.map((item, index) => {
                                            return <View style={styles.boxItem} key={index}>
                                                <Text style={styles.txtTicketName}>{item.Name}</Text>
                                                <View style={styles.boxTicketType}>
                                                    <Image source={require('../../assets/ic_delivery_brown.png')} style={styles.ic_delivery} />
                                                    <Text style={styles.txtDeliveryList}>{'Delivery Ticket'}</Text>
                                                </View>
                                                <View style={styles.boxTicketPrice}>
                                                    <Text style={styles.txtTicketPrice}>{formatNumber(item.Price)} x {item.purchaseQuantity} = {formatNumber(item.purchaseTotalMoney)}</Text>
                                                </View>
                                            </View>
                                        })
                                    }
                                    <Text style={styles.txtTotal}>Total: <Text style={styles.txtTotalNumber}>{formatNumber(this.getTotal(delivery_tickets))}</Text></Text>
                                    <Text style={styles.txtDeliveryFee}>+ Delivery Fee</Text>
                                    <Touchable disabled={is_buy_cod || loading_buy_cod} style={[styles.btnPaymentDelivery, is_buy_cod || loading_buy_cod ? {backgroundColor: '#c5c5c5'} : {}]} onPress={() => this.getTotal(delivery_tickets) == 0 ? this.paymentCodFree() : this.paymentCod()}>
                                        <Text style={styles.txtPaymentDelivery}>{
                                            is_buy_cod ?
                                            convertLanguage(language, 'ordered')
                                            :
                                            loading_buy_cod ?
                                            convertLanguage(language, 'in_process')
                                            :
                                            convertLanguage(language, 'cash_on_delivery')
                                        }</Text>
                                    </Touchable>
                                    <Text style={styles.txtNote}>{convertLanguage(language, 'give_delivery_text_in_purchase_cod')}</Text>
                                </View>
                            }
                            {
                                e_tickets.length > 0
                                &&
                                <View style={styles.boxETicket}>
                                    <Text style={styles.txtDelivery}>{convertLanguage(language, 'e-ticket_is_not_purchasable_with_cash_on_delivery')}</Text>
                                    {
                                        e_tickets.map((item, index) => {
                                            return <View style={styles.boxItem} key={index}>
                                                <Text style={styles.txtTicketName}>{item.Name}</Text>
                                                <View style={styles.boxTicketType}>
                                                    <Image source={require('../../assets/ic_eticket.png')} style={styles.ic_eticket} />
                                                    <Text style={styles.txtDeliveryList}>{'E-Ticket'}</Text>
                                                </View>
                                                <View style={styles.boxTicketPrice}>
                                                    <Text style={styles.txtTicketPrice}>{formatNumber(item.Price)} x {item.purchaseQuantity} = {formatNumber(item.purchaseTotalMoney)}</Text>
                                                </View>
                                            </View>
                                        })
                                    }
                                    <Text style={styles.txtTotal}>Total: <Text style={styles.txtTotalNumber}>{formatNumber(this.getTotal(e_tickets))}</Text></Text>
                                    <Text style={styles.txtNote}>{convertLanguage(language, 'select_a_payment_method_to_purchase_the_e-ticket')}</Text>
                                    {
                                        is_buy_cod
                                        &&
                                        <View style={styles.boxTransfer}>
                                        {
                                            this.getTotal(e_tickets) == 0
                                            ?
                                            <Touchable style={styles.btnTransfer} onPress={() => this.paymentETicketFree()}>
                                                <Text style={styles.txtTransfer}>{convertLanguage(language, 'give_ticket_free')}</Text>
                                            </Touchable>
                                            :
                                            <React.Fragment>
                                                <Touchable style={styles.btnTransfer} onPress={() => this.props.navigation.navigate('PaymentOnline', {CodePayment: e_tickets[0].CodePayment})}>
                                                    <Text style={styles.txtTransfer}>{convertLanguage(language, 'domestic_card')}</Text>
                                                </Touchable>
                                                <Touchable style={styles.btnTransfer} onPress={() => this.props.navigation.navigate('PaymentOnline', {CodePayment: e_tickets[0].CodePayment})}>
                                                    <Text style={styles.txtTransfer}>{convertLanguage(language, 'foreign_card')}</Text>
                                                </Touchable>
                                                <Touchable disabled={true} style={[styles.btnTransfer, { backgroundColor: '#cccccc', borderColor: '#cccccc' }]}>
                                                    <Text style={[styles.txtTransfer, { color: '#FFFFFF' }]}>{convertLanguage(language, 'repeat_date')}</Text>
                                                </Touchable>
                                            </React.Fragment>
                                        }
                                        </View>
                                    }
                                </View>
                            }
                        </View>
                    </ScrollView>
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    container: {
        flex: 1,
    },
    boxDelivery: {
        paddingTop: 40,
        paddingBottom: 50,
        borderBottomColor: '#757575',
        borderBottomWidth: 1
    },
    txtDelivery: {
        fontSize: 28,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 50,
        textAlign: 'center'
    },
    boxItem: {
        backgroundColor: '#F2F2F2',
        borderWidth: 2,
        borderColor: '#333333',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 20,
        paddingBottom: 20,
        marginBottom: 15
    },
    txtTicketName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxTicketType: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5
    },
    ic_delivery: {
        width: 16,
        height: 16,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDeliveryList: {
        fontSize: 13,
        color: '#333333'
    },
    boxTicketPrice: {
        alignSelf: "flex-end"
    },
    txtTicketPrice: {
        fontSize: 16,
        color: '#333333'
    },
    txtTotal: {
        fontSize: 15,
        color: '#333333',
        alignSelf: "flex-end",
    },
    txtTotalNumber: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    txtDeliveryFee: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        marginTop: 5,
        textAlign: 'right',
    },
    btnPaymentDelivery: {
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#03a9f4',
        marginTop: 30,
        marginLeft: 40,
        marginRight: 40
    },
    txtPaymentDelivery: {
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    txtNote: {
        textAlign: 'center',
        fontSize: 13,
        color: '#757575',
        fontStyle: 'italic',
        marginTop: 15,
    },
    boxETicket: {
        paddingTop: 40,
        paddingBottom: 50,
    },
    boxTransfer: {
        marginTop: 35,
        marginBottom: 20
    },
    btnTransfer: {
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginLeft: 40,
        marginRight: 40,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#03a9f4',
        marginBottom: 15
    },
    txtTransfer: {
        fontSize: 15,
        color: '#03a9f4',
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        payment: state.payment,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadPurchaseDetail: () => {
            dispatch(actLoadPurchaseDetail())
        },
        onPaymentCod: () => {
            dispatch(actPaymentCod())
        },
        onPaymentCodFree: () => {
            dispatch(actPaymentCodFree())
        },
        onPaymentETicketFree: () => {
            dispatch(actPaymentETicketFree())
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PurchaseDetail);
// export default EventHostList;
