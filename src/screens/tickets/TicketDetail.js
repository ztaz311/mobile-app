import React, { Component } from 'react';
import { Dimensions, View, Text, Image, ActivityIndicator, StyleSheet, Alert, ImageBackground, Platform } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SrcView from '../../screens_view/ScrView';
// import Swiper from 'react-native-swiper';
import * as Colors from '../../constants/Colors'
import { actUseETicket, actCloseModalUseTicket } from '../../actions/ticket'
import { convertDate, convertLanguage } from '../../services/Helper'
const { width, height } = Dimensions.get('window')
import QRCode from 'react-native-qrcode-svg';
import Modal from "react-native-modal";
import Carousel from 'react-native-snap-carousel';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import Dots from 'react-native-dots-pagination';
import { connect } from "react-redux";
class TicketDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sliderActiveSlide: 0,
        };
    }

    componentDidMount() {
        const { number } = this.props.route.params;
        this.setState({
            sliderActiveSlide: number,
        })
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    onUseETicket(item) {
        var { indexTicket } = this.props.route.params;
        var type = this.props.route.params['type'];
        var { event } = type === 'current' ? this.props.ticket.mytickets[indexTicket] : this.props.ticket.myPastTickets[indexTicket];
        var { language } = this.props.language;
        var body = {
            CodeEvent: event.Code,
            TicketDetailId: item.Id,
            CodeScan: item.Code,
        }
        Alert.alert(
            convertLanguage(language, 'title_used_ticket'),
            convertLanguage(language, 'content_used_ticket'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => this.props.onUseETicket(indexTicket, body) },
            ],
            { cancelable: false },
        );

    }

    showTicketStatus(item) {
        var { language } = this.props.language;
        switch (item.Status) {
            case -3:
            case -2:
            case -1:
            case 0:
                if (item.Ticket.Type === 'd-p-ticket') {
                    return <View style={styles.boxCenter}>
                        <Text style={{ fontSize: 38, color: '#FFFFFF', fontWeight: 'bold' }}>!</Text>
                        <Text style={{ fontSize: 17, color: '#FFFFFF' }}>{convertLanguage(language, 'it_s_not_an_e-ticket')}</Text>
                        <Text style={{ fontSize: 20, color: '#FFFFFF', fontWeight: 'bold' }}>{convertLanguage(language, 'show_your_delivery_ticket')}</Text>
                    </View>
                } else {
                    return <View style={[styles.boxCenter, { width: 108, height: 108, backgroundColor: 'transparent', justifyContent: 'center', borderWidth: 4, borderColor: '#FFFFFF' }]}>
                        <QRCode
                            value={item.Code}
                            size={102} />
                    </View>
                }
            case 1:
                return <View style={styles.boxCenter}>
                    <Image source={require('../../assets/ticket_used.png')} style={styles.ic_ticket} />
                    <Text style={styles.txtUsed}>{convertLanguage(language, 'used')}</Text>
                </View>
            case -6:
            case -7:
            case 5:
                return <View style={styles.boxCenter}>
                    <Text style={{ fontSize: 38, color: '#FFFFFF', fontWeight: 'bold' }}>!</Text>
                    <Text style={{ fontSize: 20, color: '#FFFFFF', fontWeight: 'bold' }}>{convertLanguage(language, 'request_pending')}</Text>
                </View>
            case 6:
                return <View style={styles.boxCenter}>
                    <Image source={require('../../assets/ticket_refunded.png')} style={styles.ic_ticket} />
                    <Text style={styles.txtUsed}>{convertLanguage(language, 'refunded')}</Text>
                </View>
            case 4:
                return <View style={styles.boxCenter}>
                    <Image source={require('../../assets/ticket_expired.png')} style={styles.ic_ticket} />
                    <Text style={styles.txtUsed}>{convertLanguage(language, 'expired')}</Text>
                </View>
            default:
                break;
        }
    }

    itemBanner = ({ item, index }) => {
        var { indexTicket } = this.props.route.params;
        var type = this.props.route.params['type'];
        var { event } = type === 'current' ? this.props.ticket.mytickets[indexTicket] : this.props.ticket.myPastTickets[indexTicket];
        var { language } = this.props.language;
        var { loadingUseTicket } = this.props.ticket;
        return <View style={styles.boxSlide}>
            <View style={styles.boxItem} key={index} removeClippedSubviews={false}>
                <ImageBackground style={styles.boxImageThumb} source={{ uri: event.Posters ? event.Posters.Medium : event.Poster }}>
                    <View style={{ position: 'absolute', backgroundColor: 'rgba(0,0,0,0.6)', left: 0, top: 0, width: '100%', height: '100%', alignItems: 'center', justifyContent: 'space-around', }}>
                        <View></View>
                        {
                            this.showTicketStatus(item)
                        }
                        <Text style={styles.txtEventCode}>{convertLanguage(language, 'event_code')} <Text style={styles.txtCode}>{event.Code}</Text></Text>
                    </View>
                </ImageBackground>
                <SrcView contentContainerStyle={styles.content}>
                    <View style={styles.boxEvent}>
                        <Text style={styles.txtNameEvent}>{event.Title}</Text>
                        <Text style={styles.txtCategory}>{convertDate(event.TimeStart, event.TimeFinish)}</Text>
                        <Text style={styles.txtCategory}>{event.VenueName} - {event.Address}</Text>
                    </View>
                    <View style={styles.hr}></View>
                    <View style={styles.boxTicket}>
                        <View style={styles.boxRow}>
                            <Image source={require('../../assets/ticket_active.png')} style={styles.icon} />
                            <Text style={styles.txtTicketName}>{item.Ticket.Name}</Text>
                        </View>
                        <View style={[styles.boxRow, { justifyContent: 'space-between' }]}>
                            <Text style={[styles.txtTicketPrice]}>{item.Ticket.Unit.toUpperCase()} {formatNumber(item.Ticket.Price)}</Text>
                            <View style={[styles.boxRow, { marginBottom: 0 }]}>
                                {
                                    item.Ticket.Type === 'd-p-ticket'
                                        ?
                                        <Image source={require('../../assets/ic_delivery_brown.png')} style={styles.ic_delivery} />
                                        :
                                        <Image source={require('../../assets/ic_eticket.png')} style={styles.ic_eticket} />
                                }
                                <Text style={styles.txtDelivery}>{item.Ticket.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket') : convertLanguage(language, 'e-ticket')}</Text>
                            </View>
                        </View>
                        <Text style={styles.txtTicketDes}>{item.Ticket.Description}</Text>
                    </View>
                </SrcView>
                {
                    (item.Ticket.Type === 'e-ticket' && (item.Status == 0 || item.Status == -1 || item.Status == -3 || item.Status == -2)) &&
                    <Touchable disabled={loadingUseTicket} style={styles.btnTicket} onPress={() => this.onUseETicket(item)}>
                        <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#FFFFFF' }}>{convertLanguage(language, 'use_ticket')}</Text>
                    </Touchable>
                }
            </View>
        </View>
    }

    setIndexDot(index) {
        this.setState({ sliderActiveSlide: index })
    }

    render() {
        var { number, indexTicket } = this.props.route.params;
        var type = this.props.route.params['type'];
        var { tickets } = type === 'current' ? this.props.ticket.mytickets[indexTicket] : this.props.ticket.myPastTickets[indexTicket];
        var { loadingUseTicket, isUseSuccess, messageUseTicket, isShowModalUseTicket } = this.props.ticket;
        var { language } = this.props.language;
        var { sliderActiveSlide } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <View style={styles.container}>
                    {
                        (loadingUseTicket) &&
                        <View style={{ position: 'absolute', backgroundColor: 'rgba(0,0,0,0.3)', left: 0, top: 0, width, height, zIndex: 9, alignItems: 'center', justifyContent: 'center' }}>
                            <ActivityIndicator size="large" color="#000000" style={{ marginBottom: 50 }} />
                        </View>
                    }
                    <Carousel
                        ref={ref => this.carouselRef = ref}
                        data={tickets}
                        renderItem={this.itemBanner}
                        sliderWidth={width}
                        itemWidth={width - 40}
                        firstItem={number}
                        contentContainerCustomStyle={{
                            minWidth: width * tickets.length
                        }}
                        inactiveSlideScale={0.99}
                        onSnapToItem={(index) => this.setIndexDot(index)}
                        removeClippedSubviews={false}
                    />
                    <Dots length={tickets.length} marginHorizontal={3} active={sliderActiveSlide} width={100} passiveDotWidth={8} passiveDotHeight={8} activeDotWidth={12} activeDotHeight={12} paddingVertical={12} activeColor={'#00A9F4'} />

                </View>
                <Modal
                    isVisible={isShowModalUseTicket}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    onBackdropPress={() => this.props.onCloseModalUseTicket()}
                    animationInTiming={1}
                    animationOutTiming={1}
                    backdropTransitionInTiming={1}
                    backdropTransitionOutTiming={1}
                    backdropOpacity={0.3}
                    deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                    style={styles.boxModel}>
                    <View style={styles.boxShowAnswer}>
                        <Image source={isUseSuccess ? require('../../assets/success.gif') : require('../../assets/close-icon.png')} style={isUseSuccess ? styles.ic_success : styles.ic_fail} />
                        <Text style={isUseSuccess ? styles.txtSuccess : styles.txtFail}>{messageUseTicket}</Text>
                        <Touchable style={styles.btnContinue} onPress={() => this.props.onCloseModalUseTicket()}>
                            <Text style={styles.txtContinue}>{convertLanguage(language, 'continue')}</Text>
                        </Touchable>
                    </View>
                </Modal>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        // flex: 1,
        paddingLeft: 16,
        paddingRight: 16
    },
    container: {
        flex: 1,
    },
    boxSlide: {
        flex: 1,
        padding: 10,
        paddingBottom: 20,
    },
    boxItem: {
        flex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
    },
    boxImageThumb: {
        marginBottom: 25,
        width: width - 60,
        aspectRatio: 16 / 9,
        resizeMode: 'cover',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden'
    },
    boxCenter: {
        alignItems: 'center'
    },
    ic_ticket: {
        width: 62,
        height: 48,
        marginBottom: 5
    },
    txtUsed: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    txtEventCode: {
        fontSize: 15,
        color: '#FFFFFF'
    },
    txtCode: {
        fontWeight: 'bold'
    },
    boxEvent: {
        paddingBottom: 20,
    },
    hr: {
        width: '50%',
        height: 1,
        backgroundColor: '#333333',
        alignSelf: 'center',
        opacity: 0.25
    },
    txtNameEvent: {
        fontSize: 20,
        color: '#333333',
        marginBottom: 10,
        fontWeight: 'bold'
    },
    txtCategory: {
        fontSize: 14,
        color: '#757575',
    },
    boxTicket: {
        paddingTop: 25,
        paddingBottom: 25,
    },
    txtTicketName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        flex: 1
    },
    txtTicketDes: {
        fontSize: 13,
        color: '#757575',
        marginBottom: 15,
    },
    btnTicket: {
        height: 54,
        backgroundColor: '#03a9f4',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        margin: 16
    },
    boxModel: {
        flex: 1,
    },
    boxShowAnswer: {
        height: 350,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    ic_success: {
        width: 150,
        height: 150 * 261 / 257,
        padding: 20
    },
    ic_fail: {
        width: 150,
        height: 150,
        padding: 20
    },
    txtSuccess: {
        fontSize: 20,
        color: '#78B348',
        padding: 20,
        textAlign: 'center'
    },
    txtFail: {
        fontSize: 20,
        color: 'red',
        padding: 20,
        textAlign: 'center'
    },
    btnContinue: {
        width: 150,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        backgroundColor: '#78B348',
        marginTop: 20,
        marginBottom: 20
    },
    txtContinue: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    boxRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    icon: {
        width: 30,
        height: 30,
        marginRight: 7
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_delivery: {
        width: 16,
        height: 16,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDelivery: {
        fontSize: 13,
        color: '#757575'
    },
    paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 2
    }
});
const mapStateToProps = state => {
    return {
        ticket: state.ticket,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onUseETicket: (indexTicket, body) => {
            dispatch(actUseETicket(indexTicket, body))
        },
        onCloseModalUseTicket: () => {
            dispatch(actCloseModalUseTicket())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TicketDetail);
