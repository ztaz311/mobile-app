import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, PermissionsAndroid, TextInput, Platform, Alert } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import { actLoadDataHolderList } from '../../actions/ticket';
import ModalDetailHolder from '../../components/ticket/ModalDetailHolder';
import * as Config from '../../constants/Config';
import HTTP from '../../services/HTTP';
class HolderList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Keyword: '',
            Page: 1,
            Type: 'time',
            modalDetailHolder: false,
            data: {},
            loadingDownload: false
        };
    }

    componentDidMount() {
        var { event } = this.props.event;
        var { Keyword, Page, Type } = this.state;
        this.props.onLoadDataHolderList(event.Id, Keyword, Page, Type);
    }

    _renderFooter() {
        var { loadMoreHolder, is_empty } = this.props.ticket;
        var { language } = this.props.language;
        if (loadMoreHolder) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20, paddingBottom: 20 }}>{convertLanguage(language, 'data_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { event } = this.props.event;
        var { loadMoreHolder } = this.props.ticket;
        var { Keyword, Page, Type } = this.state;
        if (loadMoreHolder) {
            this.props.onLoadDataHolderList(event.Id, Keyword, Page + 1, Type);
            this.setState({ Page: Page + 1 })
        }
    }

    onChange(key, value) {
        var { event } = this.props.event;
        this.setState({ [key]: value, Page: 1 })
        var { Keyword, Type } = this.state;
        if (key === 'Keyword') {
            this.props.onLoadDataHolderList(event.Id, value, 1, Type);
        } else {
            this.props.onLoadDataHolderList(event.Id, Keyword, 1, value);
        }
    }

    downloadExcel() {
        var { event } = this.props.event;
        this.setState({ loadingDownload: true })
        var url = `events/${event.Slug}/holder-list-export-excel`;
        return HTTP.callApi(url, 'GET', null).then(async response => {
            if (response && response.data.status == 200) {
                if (Platform.OS === 'android') {
                    try {
                        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
                        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                            this.setState({ loading: true });
                            const android = RNFetchBlob.android
                            RNFetchBlob.config({
                                addAndroidDownloads: {
                                    useDownloadManager: true,
                                    title: event.Slug + "_list_holder.excel",
                                    description: 'An excel that will be installed',
                                    mime: 'application/excel',
                                    mediaScannable: true,
                                    notification: true,
                                    path: RNFetchBlob.fs.dirs.DownloadDir + "/" + event.Slug + "-holder-list-export-excel.xlsx",
                                }
                            })
                                .fetch('GET', response.data.url)
                                .then((res) => {
                                    this.setState({ loadingDownload: false });
                                    Alert.alert(
                                        'Thông báo!',
                                        'Tải file excel thành công',
                                        [
                                            { text: 'Đóng', onPress: () => { } }
                                        ]
                                    );
                                }).catch((err) => {
                                    console.log(err)
                                    this.setState({ loadingDownload: false });
                                    Alert.alert(
                                        'Thông báo!',
                                        'Lỗi khi tải file excel',
                                        [
                                            { text: 'Đóng', onPress: () => { } }
                                        ]
                                    );
                                })
                        } else {
                            this._alertForStoragePermission()
                        }
                    } catch (err) {
                        console.warn(err);
                    }
                } else {
                    const { dirs: { DocumentDir } } = RNFetchBlob.fs;
                    const fPath = `${DocumentDir}/${event.Slug}-holder-list-export-excel.xlsx`;
                    RNFetchBlob.config({
                        fileCache: true,
                        path: fPath,
                        appendExt: 'xlsx',
                    })
                        .fetch('GET', response.data.url)
                        .then((res) => {
                            this.setState({ loadingDownload: false });
                            RNFetchBlob.fs.writeFile(fPath, res.path(), 'base64')
                            RNFetchBlob.ios.previewDocument(res.path());
                        }).catch(() => {
                            this.setState({ loadingDownload: false });
                            Alert.alert(
                                'Thông báo!',
                                'Lỗi khi tải file excel',
                                [
                                    { text: 'Đóng', onPress: () => { } }
                                ]
                            );
                        });
                }
            }
        }).catch(function (error) {
            console.log(error)
        });
    }

    render() {
        var { Keyword, Type, loadingDownload } = this.state;
        var { language } = this.props.language;
        var { totalHolders, holders } = this.props.ticket;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'holder_list')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <View style={styles.content}>
                    <View style={styles.boxHeader}>
                        {
                            loadingDownload ?
                            <ActivityIndicator size="small" color="#000000" style={{ alignSelf: 'flex-end', marginTop: 35, marginBottom: 15 }} />
                            :
                            <Touchable style={styles.boxButton} onPress={() => this.downloadExcel()}>
                                <Image source={require('../../assets/ic_download.png')} style={styles.ic_download} />
                                <Text style={styles.txtDownload}>{convertLanguage(language, 'download')}</Text>
                            </Touchable>
                        }
                        <TextInput
                            style={styles.ipContent}
                            selectionColor="#bdbdbd"
                            placeholder={convertLanguage(language, 'placeholder_keyword')}
                            name="Keyword"
                            value={Keyword}
                            onChangeText={(Keyword) => this.onChange('Keyword', Keyword)}
                        />
                        <View style={styles.boxFilter}>
                            <View style={styles.boxLeft}>
                                <View style={styles.boxTotal}>
                                    <Image source={require('../../assets/icon_user.png')} style={styles.icon_user} />
                                    <Text style={styles.txtTotal}>{totalHolders}</Text>
                                </View>
                            </View>
                            <View style={styles.boxRight}>
                                <Touchable style={[styles.boxTime, Type === 'name' ? { borderBottomWidth: 1, borderBottomColor: '#e8e8e8' } : {}]} onPress={() => this.onChange('Type', 'time')} disabled={Type === 'time' ? true : false}>
                                    <Text style={[styles.txtTime, Type === 'name' ? { fontWeight: 'normal', color: '#333333' } : {}]}>{convertLanguage(language, 'time')}</Text>
                                </Touchable>
                                <Touchable style={[styles.boxTime, Type === 'time' ? { borderBottomWidth: 1, borderBottomColor: '#e8e8e8' } : {}]} onPress={() => this.onChange('Type', 'name')} disabled={Type === 'name' ? true : false}>
                                    <Text style={[styles.txtTime, Type === 'time' ? { fontWeight: 'normal', color: '#333333' } : {}]}>{convertLanguage(language, 'name')}</Text>
                                </Touchable>
                            </View>
                        </View>
                    </View>
                    <FlatList
                        data={holders}
                        contentContainerStyle={{ padding: 20 }}
                        renderItem={({ item }) => {
                            return <View style={styles.boxDataItem}>
                                <View style={styles.boxItem}>
                                    <Touchable style={styles.boxInfo} onPress={() => this.setState({ modalDetailHolder: true, data: item.total })}>
                                        <Image source={{ uri: item.Avatars.Small }} style={styles.imgAvatar} />
                                        <View style={styles.boxInfoDetail}>
                                            <Text style={styles.txtName}>{item.NameHolder}</Text>
                                            <Text style={styles.txtId}>{item.Name}{item.UserId && ' / ID:' + item.UserId}</Text>
                                        </View>
                                    </Touchable>
                                    <Touchable style={styles.boxAction} onPress={() => this.props.navigation.navigate('HolderListComment', { item })}>
                                        <Image source={require('../../assets/ic_comment.png')} style={styles.ic_comment} />
                                    </Touchable>
                                </View>
                                {
                                    item.commentHolderList && item.commentHolderList.length > 0 &&
                                    <Text style={styles.txtReason}><Text style={{ color: '#03a9f4' }}>*</Text> {item.commentHolderList}</Text>
                                }
                            </View>
                        }}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => this._renderFooter()}
                        onEndReached={() => { this.loadMore() }}
                    // onRefresh={() => {this.refresh()}}
                    // refreshing={false}
                    />
                </View>
                {
                    this.state.modalDetailHolder
                        ?
                        <ModalDetailHolder
                            modalVisible={this.state.modalDetailHolder}
                            closeModal={() => this.setState({ modalDetailHolder: false })}
                            data={this.state.data}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxHeader: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxDataItem: {
        marginBottom: 15
    },
    txtReason: {
        fontSize: 14,
        color: '#333333',
        marginTop: 10
    },
    boxButton: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 5,
        paddingTop: 10,
        paddingBottom: 10,
    },
    ic_download: {
        width: 7,
        height: 8.5,
        marginRight: 5
    },
    txtDownload: {
        fontSize: 12,
        color: '#03a9f4',
    },
    ipContent: {
        fontSize: 17,
        color: '#333333',
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 8,
        paddingTop: 8,
        paddingLeft: 0,
        marginBottom: 25
    },
    boxFilter: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    boxLeft: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#e8e8e8',
        borderBottomWidth: 1,
        paddingBottom: 5
    },
    boxTotal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15
    },
    icon_user: {
        width: 15,
        height: 17,
        marginRight: 5
    },
    txtTotal: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxRight: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    boxTime: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomWidth: 2,
        borderBottomColor: '#03a9f4'
    },
    txtTime: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#03a9f4'
    },
    boxData: {
        paddingLeft: 20,
        paddingRight: 20
    },
    boxItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    imgAvatar: {
        width: 60,
        height: 60,
        marginRight: 10,
        borderRadius: 30
    },
    boxInfoDetail: {
        flex: 1,
    },
    txtName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    txtId: {
        fontSize: 14,
        color: '#757575',
    },
    txtRole: {
        fontSize: 14,
        color: '#757575',
    },
    boxAction: {
        width: 31,
        height: 31,
        alignItems: 'center',
        justifyContent: 'center',
    },
    ic_comment: {
        width: 21,
        height: 21
    }
});

const mapStateToProps = state => {
    return {
        language: state.language,
        ticket: state.ticket,
        event: state.event
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataHolderList: (EventId, Keyword, Page, Type) => {
            dispatch(actLoadDataHolderList(EventId, Keyword, Page, Type))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HolderList);
// export default EventHostList;
