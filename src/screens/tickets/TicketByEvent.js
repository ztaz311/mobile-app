import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import { actLoadDataTicketPurchased } from '../../actions/ticket'
import ModalHowToUse from '../../components/ticket/ModalHowToUse'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import SrcView from '../../screens_view/ScrView';
import ItemMyTicket from '../../components/ticket/ItemMyTicket'
const { width, height } = Dimensions.get('window')
import { formatNumber, convertDate, convertLanguage } from '../../services/Helper'
import FastImage from 'react-native-fast-image';
class TicketByEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalHowToUse: false,
        };
    }

    render() {
        var indexTicket = this.props.route.params['indexTicket'];
        var type = this.props.route.params['type'];
        var { event, tickets } = type === 'current' ? this.props.ticket.mytickets[indexTicket] : this.props.ticket.myPastTickets[indexTicket];
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'ticket_list')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />
                <View style={{ flex: 1 }}>
                    <SrcView contentContainerStyle={styles.container}>
                        <FastImage style={styles.boxImageThumb} source={{ uri: event.Posters ? event.Posters.Small : event.Poster }} />
                        {
                            event.Status == 0
                            &&
                            <View style={styles.boxCancel}>
                                <Image source={require('../../assets/canceled_event.png')} style={styles.imgCancel} />
                            </View>
                        }
                        <View style={styles.content}>
                            <Touchable style={styles.boxEvent} onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: event.Slug }, key: event.Slug })}>
                                <Text style={styles.txtNameEvent}>{event.Title}</Text>
                                <Text style={styles.txtCategory}>{convertDate(event.TimeStart, event.TimeFinish)}</Text>
                                <Text style={styles.txtCategory}>{event.VenueName} - {event.Address}</Text>
                            </Touchable>
                            {
                                tickets.length > 0
                                &&
                                tickets.map((item, index) => {
                                    return <ItemMyTicket navigation={this.props.navigation} total={tickets.length} key={index} type={type} index={index} item={item} event={event} indexTicket={indexTicket} />
                                })
                            }
                            <Touchable style={styles.boxHowToUse} onPress={() => this.setState({ modalHowToUse: true })}>
                                <Text style={styles.txtHowToUse}>{convertLanguage(language, 'how_to_use_tickets')}...</Text>
                            </Touchable>
                        </View>
                    </SrcView>
                </View>
                {
                    this.state.modalHowToUse
                        ?
                        <ModalHowToUse
                            modalVisible={this.state.modalHowToUse}
                            closeModal={() => this.setState({ modalHowToUse: false })}
                            Title={event.Title}
                            navigation={this.props.navigation}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 16
    },
    boxImageThumb: {
        marginBottom: 25,
        width: width - 32,
        aspectRatio: 16 / 9,
    },
    boxCancel: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width - 32,
        height: (width - 32) * 9 / 16,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    imgCancel: {
        width: width - 32,
        height: (width - 32) * 9 / 16,
    },
    content: {
        flex: 1,
    },
    boxEvent: {
        paddingBottom: 20,
        borderBottomColor: '#03a9f4',
        borderBottomWidth: 2
    },
    txtNameEvent: {
        fontSize: 20,
        color: '#333333',
        marginBottom: 10,
        fontWeight: 'bold'
    },
    txtCategory: {
        fontSize: 14,
        color: '#757575',
    },
    boxTicket: {
        paddingTop: 25,
        paddingBottom: 25,
        borderBottomColor: '#757575',
        borderBottomWidth: 1
    },
    txtTicketName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        flex: 1
    },
    txtTicketDes: {
        fontSize: 13,
        color: '#757575',
        marginBottom: 15,
    },
    boxTicketPrice: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    btnView: {
        flex: 0.55,
        height: 31,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#03a9f4',
        borderRadius: 2,
        paddingLeft: 10,
        paddingRight: 10
    },
    icQRcode: {
        width: 18,
        height: 18,
        marginRight: 10
    },
    txtView: {
        fontSize: 14,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    txtUsed: {
        color: '#ff4081',
        fontSize: 14,
        fontWeight: 'bold'
    },
    boxTicketInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxTicketType: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ic_delivery: {
        width: 14,
        height: 10,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDelivery: {
        fontSize: 13,
        color: '#757575'
    },
    btnStatus: {
        flex: 0.4,
        height: 31,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#828282',
        borderRadius: 2
    },
    txtStatus: {
        fontSize: 13,
        color: '#757575'
    },
    boxHowToUse: {
        margin: 20,
        alignSelf: 'center',
        // width: 162,
        height: 31,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#00A9F4',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4
    },
    txtHowToUse: {
        fontSize: 16,
        color: '#00A9F4',
    },
    boxRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    icon: {
        width: 30,
        height: 30,
        marginRight: 7
    },
});

const mapStateToProps = state => {
    return {
        ticket: state.ticket,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataTicketPurchased: (event_id) => {
            dispatch(actLoadDataTicketPurchased(event_id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TicketByEvent);
// export default EventHostList;
