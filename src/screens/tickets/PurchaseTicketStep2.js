import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, StyleSheet, TextInput, ScrollView, Dimensions, TouchableOpacity, Keyboard } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataPurchase } from '../../actions/event';
import { actGetDistrictByCity } from '../../actions/city';
import { actPayment, actPaymentFree } from '../../actions/payment';
const { width, height } = Dimensions.get('window');
import { formatNumber, convertLanguage } from '../../services/Helper';
import ModalAddress from '../../components/ticket/ModalAddress';
import ModalPaymentMethod from '../../components/ticket/ModalPaymentMethod';
import ModalComingSoon from '../../components/ModalComingSoon';
import ModalPageWebView from '../../components/ModalPageWebView';
import ModalConfirmCod from '../../components/ticket/ModalConfirmCod';
import Popover from 'react-native-popover-view';
import * as Config from '../../constants/Config';
class PurchaseTicketStep2 extends Component {
    constructor(props) {
        super(props);
        this.scrollView = null;
        this.state = {
            checked: false,
            Name: this.props.user.profile.Name ? this.props.user.profile.Name : '',
            Phone: this.props.user.profile.Phone ? this.props.user.profile.Phone : '',
            Zipcode: this.props.user.profile.Zipcode ? this.props.user.profile.Zipcode : this.props.event.event.UnitTicket.toUpperCase() === 'VND' ? '+84' : '+82',
            NameHolder: this.props.user.profile.Name ? this.props.user.profile.Name : '',
            PhoneHolder: this.props.user.profile.Phone ? this.props.user.profile.Phone : '',
            ZipcodeHolder: this.props.user.profile.Zipcode ? this.props.user.profile.Zipcode : this.props.event.event.UnitTicket.toUpperCase() === 'VND' ? '+84' : '+82',
            Address: '',
            Question: '',
            datas: [],
            modalAddress: false,
            modalPaymentMethod: false,
            arrCity: [
                {
                    name: 'Hà Nội',
                    value: '1'
                }
            ],
            City: '1',
            arrDistrict: [
                {
                    name: 'Select a district',
                    value: ''
                }
            ],
            District: '',
            modalPageWebView: false,
            isVisible: false,
            isVisibleHolder: false,
            modalComingSoon: false,
            modalConfirmCod: false,
            uri: '',
            title: '',
            isShowAction: true
        };
    }

    componentDidMount() {
        this.props.onLoadDataPurchase();
        this.props.onGetDistrictByCity(this.state.City);
        this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        this.setState({
            isShowAction: false
        })
    };

    keyboardWillHide = (event) => {
        this.setState({
            isShowAction: true
        })
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.event.purchases.data.length > 0) {
            var newPurchase = [];
            nextProps.event.purchases.data.forEach((purchase) => {
                var item = {};
                item.Id = purchase.Id;
                if (purchase.Option.some(el => el === 2)) {
                    item.Question2 = '';
                }
                newPurchase.push(item)
            })
            this.setState({ datas: newPurchase })
        }
    }

    checkDelivery() {
        var { data } = this.props.event.purchases;
        var checkDelivery = false;
        for (let i = 0; i < data.length; i++) {
            if (data[i].Type === 'd-p-ticket' || data[i].Option.some(el => el === 1)) {
                checkDelivery = true;
                break;
            }
        }
        return checkDelivery;
    }

    checkValidate() {
        var { Name, Phone, Address, datas, checked, NameHolder, PhoneHolder } = this.state;
        if (NameHolder === '' || PhoneHolder === '') {
            return false;
        }
        for (let i = 0; i < datas.length; i++) {
            if (typeof (datas[i].Question2) !== 'undefined' && datas[i].Question2 === '') {
                return false;
            }
        }
        if (!checked) {
            return false;
        }
        if (this.checkDelivery()) {
            if (Name === '' || Phone === '' || Address === '') {
                return false;
            }
        }
        return true;
    }

    checkValidateInput() {
        var { Name, Phone, Address, datas, checked, NameHolder, PhoneHolder } = this.state;
        if (NameHolder === '' || PhoneHolder === '') {
            return false;
        }
        if (this.checkDelivery()) {
            if (Name === '' || Phone === '' || Address === '') {
                return false;
            }
        }
        for (let i = 0; i < datas.length; i++) {
            if (typeof (datas[i].Question2) !== 'undefined' && datas[i].Question2 === '') {
                return false;
            }
        }
        return true;
    }

    onChangeQuestion(index, Question2) {
        var { datas } = this.state;
        if (datas[index] && typeof (datas[index].Question2) !== 'undefined') {
            datas[index].Question2 = Question2;
        }
        this.setState({ datas })
    }

    convertDistrict() {
        var { districts } = this.props.city;
        var arrDistrict = [
            {
                name: 'Select a district',
                value: ''
            }
        ];
        // alert(JSON.stringify(districts));
        districts.forEach((item) => {
            var district = {
                name: item.Name,
                value: item.Id
            }
            arrDistrict.push(district);
        })
        return arrDistrict;
    }

    updateAddress(data) {
        this.setState({
            modalAddress: false,
            City: data.City,
            District: data.District,
            Address: data.Address,
        })
    }

    onPayment(TypePayment) {
        var { Name, Phone, Address, datas, City, District, NameHolder, PhoneHolder, Zipcode, ZipcodeHolder } = this.state;
        this.setState({
            modalPaymentMethod: false,
            modalConfirmCod: false
        })
        var body = {
            Name, Phone, Address, City, District, TypePayment, NameHolder, PhoneHolder, Zipcode, ZipcodeHolder,
            purchases: datas,
        }
        this.props.onPayment(body, this.props.route.params['dataAnalytics'])
    }

    onPaymentFree() {
        var { Name, Phone, Address, datas, City, District, NameHolder, PhoneHolder, Zipcode, ZipcodeHolder } = this.state;
        this.setState({
            modalPaymentMethod: false
        })
        var body = {
            Name, Phone, Address, City, District, NameHolder, PhoneHolder, Zipcode, ZipcodeHolder,
            purchases: datas,
        }
        this.props.onPaymentFree(body, this.props.route.params['dataAnalytics'])
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    showPopoverHolder() {
        this.setState({ isVisibleHolder: true });
    }

    closePopoverHolder() {
        this.setState({ isVisibleHolder: false });
    }

    render() {
        var { Name, Phone, Address, datas, NameHolder, PhoneHolder, ZipcodeHolder, Zipcode, isShowAction } = this.state;
        var { purchases, loadingPurchase, event } = this.props.event;
        var { loading } = this.props.payment;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1, marginBottom: 0 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{
                            minWidth: 48, minHeight: 48, alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'select_ticket')}</Text>
                    <View
                        style={{
                            width: 48, minHeight: 48,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                    </View>
                </View>

                <Line />
                {
                    (loadingPurchase || loading) &&
                    <View style={{ flex: 1, top: height / 2 - 30, width, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#000000" />
                    </View>
                }
                <View style={[styles.content, { opacity: loadingPurchase ? 0 : loading ? 0.2 : 1 }]}>
                    <KeyboardAwareScrollView style={styles.container} >
                        <ScrollView style={styles.container} ref={(ref) => this.scrollView = ref}>
                            <View style={styles.boxContent}>
                                {
                                    purchases.data.length > 0 &&
                                    purchases.data.map((item, index) => {
                                        return <View style={styles.boxItem} key={index}>
                                            <Text style={styles.txtTicketName}>{item.Name}</Text>
                                            <Text style={styles.txtTicketDes}>{item.Description}</Text>
                                            <View style={styles.boxTicketType}>
                                                {
                                                    item.Type === 'd-p-ticket'
                                                        ?
                                                        <Image source={require('../../assets/ic_delivery_brown.png')} style={styles.ic_delivery} />
                                                        :
                                                        <Image source={require('../../assets/ic_eticket.png')} style={styles.ic_eticket} />
                                                }
                                                <Text style={styles.txtDeliveryList}>{item.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket') : convertLanguage(language, 'e-ticket')}</Text>
                                            </View>
                                            <View style={styles.boxTicketPrice}>
                                                <Text style={styles.txtTicketPrice}>{item.Unit.toUpperCase()} {formatNumber(item.Price)} x {item.purchaseQuantity}</Text>
                                                <Text style={styles.txtTicketTotal}>{item.Unit.toUpperCase()} {formatNumber(item.purchaseTotalMoney)}</Text>
                                            </View>
                                            {
                                                item.Option.some(el => el === 2) &&
                                                <React.Fragment>
                                                    <Text style={[styles.txtDelivery, { marginTop: 12 }]}>{convertLanguage(language, 'question')} : {item.Question2}</Text>
                                                    <TextInput
                                                        style={styles.ipContent}
                                                        selectionColor="#bcbcbc"
                                                        value={datas.length > 0 ? datas[index].Question2 : ''}
                                                        placeholderTextColor="#bdbdbd"
                                                        onChangeText={(Question) => this.onChangeQuestion(index, Question)}
                                                        placeholder={'Write'}
                                                    />
                                                </React.Fragment>
                                            }
                                        </View>
                                    })
                                }
                                <View style={styles.boxTotalPrice}>
                                    <Text style={styles.txtTotalTitle}></Text>
                                    <Text style={styles.txtTotalValue}>{convertLanguage(language, 'total')}: {purchases.unit.toUpperCase()} {formatNumber(purchases.totalPrice)}</Text>
                                </View>
                                <Text style={styles.txtDeliveryFee}>{this.checkDelivery() ? '+ Delivery Fee' : ''}</Text>
                                <View style={styles.boxDelivery}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../assets/icon_user.png')} style={styles.ic_user} />
                                        <Text style={styles.txtDelivery}>{convertLanguage(language, 'ticket_holder')}</Text>
                                    </View>
                                    <View style={styles.boxInput}>
                                        <Text style={styles.txtInput}>{convertLanguage(language, 'name')}</Text>
                                        <TextInput
                                            style={styles.input}
                                            selectionColor="#bcbcbc"
                                            value={NameHolder}
                                            placeholderTextColor="#bdbdbd"
                                            onChangeText={(NameHolder) => this.setState({ NameHolder })}
                                        />
                                    </View>
                                    <View style={styles.boxInput}>
                                        <Text style={styles.txtInput}>{convertLanguage(language, 'phone')}</Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'flex-end', flex: 1 }}>
                                            <TouchableOpacity
                                                ref={ref => this.touchable = ref}
                                                onPress={() => this.showPopover()}
                                                style={{
                                                    borderBottomWidth: 1,
                                                    borderBottomColor: '#bdbdbd',
                                                    marginTop: 5,
                                                    marginBottom: 5,
                                                    paddingTop: 5,
                                                    paddingBottom: 5,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    width: 96,
                                                    marginRight: 10
                                                }}>
                                                {/* <Text style={{ fontSize: 16, color: '#333333', fontWeight: 'bold' }}>{ZipcodeHolder}</Text> */}
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={ZipcodeHolder === '+84' ? require('../../assets/flag_vn.png') : ZipcodeHolder === '+66' ? require('../../assets/flag_th.png') : require('../../assets/flag_kr.png')} style={[styles.ic_country, { marginRight: 6 }]} />
                                                    <Text style={[styles.txtCountry, { fontWeight: 'bold' }]}>{ZipcodeHolder}</Text>
                                                </View>
                                                <Image source={require('../../assets/down_arrow_active.png')} style={{
                                                    width: 13,
                                                    height: 11,
                                                    marginRight: 5
                                                }} />
                                            </TouchableOpacity>
                                            {/* <Text style={[styles.txtTicketPrice, { marginRight: 20, fontWeight: 'bold' }]}>+84</Text> */}
                                            <TextInput
                                                style={styles.input}
                                                selectionColor="#bcbcbc"
                                                value={PhoneHolder}
                                                placeholderTextColor="#bdbdbd"
                                                onChangeText={(PhoneHolder) => this.setState({ PhoneHolder })}
                                                keyboardType="numeric"
                                            />
                                        </View>
                                    </View>
                                </View>
                                {
                                    this.checkDelivery() &&
                                    <View style={[styles.boxDelivery, { borderTopWidth: 0 }]}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={require('../../assets/ic_delivery_black.png')} style={styles.ic_delivery_black} />
                                            <Text style={styles.txtDelivery}>{convertLanguage(language, 'delivery_ticket')}</Text>
                                        </View>
                                        <Text style={styles.txtInfo}>{convertLanguage(language, 'input_a_receiver_information')}</Text>
                                        <View style={styles.boxInput}>
                                            <Text style={styles.txtInput}>{convertLanguage(language, 'name')}</Text>
                                            <TextInput
                                                style={styles.input}
                                                selectionColor="#bcbcbc"
                                                value={Name}
                                                placeholderTextColor="#bdbdbd"
                                                onChangeText={(Name) => this.setState({ Name })}
                                            />
                                        </View>
                                        <View style={styles.boxInput}>
                                            <Text style={styles.txtInput}>{convertLanguage(language, 'phone')}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'flex-end', flex: 1 }}>
                                                <TouchableOpacity
                                                    onPress={() => this.showPopoverHolder()}
                                                    animationConfig={{ duration: 0 }}
                                                    ref={ref => this.touchable2 = ref}
                                                    style={{
                                                        borderBottomWidth: 1,
                                                        borderBottomColor: '#bdbdbd',
                                                        marginTop: 5,
                                                        marginBottom: 5,
                                                        paddingTop: 5,
                                                        paddingBottom: 5,
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'space-between',
                                                        width: 96,
                                                        marginRight: 10
                                                    }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={Zipcode === '+84' ? require('../../assets/flag_vn.png') : Zipcode === '+66' ? require('../../assets/flag_th.png') : require('../../assets/flag_kr.png')} style={[styles.ic_country, { marginRight: 6 }]} />
                                                        <Text style={[styles.txtCountry, { fontWeight: 'bold' }]}>{Zipcode}</Text>
                                                    </View>
                                                    <Image source={require('../../assets/down_arrow_active.png')} style={{ width: 13, height: 11, marginRight: 5 }} />
                                                </TouchableOpacity>
                                                {/* <Text style={[styles.txtTicketPrice, { marginRight: 20, fontWeight: 'bold' }]}>+84</Text> */}
                                                <TextInput
                                                    style={styles.input}
                                                    selectionColor="#bcbcbc"
                                                    value={Phone}
                                                    placeholderTextColor="#bdbdbd"
                                                    onChangeText={(Phone) => this.setState({ Phone: Phone == 0 ? '' : Phone })}
                                                    keyboardType="numeric"
                                                />
                                            </View>
                                        </View>
                                        <View style={styles.boxInput}>
                                            <Text style={styles.txtInput}>{convertLanguage(language, 'address')}</Text>
                                            <Touchable style={styles.boxAddress} onPress={() => this.setState({ modalAddress: true })}>
                                                <Text style={styles.txtAddress}>{Address}</Text>
                                            </Touchable>
                                        </View>
                                        <Text style={styles.txtNote}>{convertLanguage(language, 'depend_on_the_ticket_text')}</Text>
                                    </View>
                                }
                                <View style={styles.boxRefund}>
                                    <Touchable style={styles.btnRefund} onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/refund-policy`, title: convertLanguage(language, 'refund_policy') })}>
                                        <Text style={styles.txtRefund}>{convertLanguage(language, 'refund_policy')}</Text>
                                    </Touchable>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAwareScrollView>
                    {
                        isShowAction &&
                        <View style={styles.boxPurchase}>
                            <Touchable disabled={!this.checkValidateInput()} style={[styles.boxPolicy, { backgroundColor: !this.checkValidateInput() ? '#E0E2E8' : this.state.checked ? 'transparent' : '#EB5757' }]} onPress={() => this.setState({ checked: !this.state.checked })}>
                                <Image source={!this.checkValidateInput() ? require('../../assets/box_checked_dis.png') : this.state.checked ? require('../../assets/box_checked_white.png') : require('../../assets/box_unchecked.png')} style={styles.imgCheckbox} />
                                <Text style={[styles.txtPolicy, !this.checkValidateInput() ? { color: '#B3B8BC' } : {}]}>{convertLanguage(language, 'i_have_checked_the_refund_policy_and_I_agree_to_purchase_all_the_selected_tickets')}</Text>
                            </Touchable>
                            <View style={styles.boxPurchaseButton}>
                                <Touchable disabled={!this.checkValidate()} style={[styles.btnPurchase, { backgroundColor: this.checkValidate() ? '#FFFFFF' : '#0288d1' }]} onPress={() => purchases.totalPrice == 0 ? this.onPaymentFree() : this.setState({ modalPaymentMethod: true })}>
                                    <Text style={styles.txtPurchase}>{convertLanguage(language, 'purchase')}</Text>
                                </Touchable>
                            </View>
                        </View>
                    }
                </View>
                {
                    this.state.modalAddress
                        ?
                        <ModalAddress
                            modalAddress={this.state.modalAddress}
                            closeModal={() => this.setState({ modalAddress: false })}
                            arrDistrict={this.convertDistrict()}
                            arrCity={this.state.arrCity}
                            City={this.state.City}
                            District={this.state.District}
                            Address={this.state.Address}
                            selectAddress={(data) => this.updateAddress(data)}
                        />
                        : null
                }
                {
                    this.state.modalPaymentMethod
                        ?
                        <ModalPaymentMethod
                            modalPaymentMethod={this.state.modalPaymentMethod}
                            closeModal={() => this.setState({ modalPaymentMethod: false })}
                            purchases={purchases.data}
                            onPayment={(TypePayment) => this.onPayment(TypePayment)}
                            unitTicket={event.UnitTicket}
                            showModalComingSoon={() => this.setState({ modalPaymentMethod: false, modalComingSoon: true })}
                            onConfirmCod={() => this.setState({ modalPaymentMethod: false, modalConfirmCod: true })}
                        />
                        : null
                }
                {
                    this.state.modalPageWebView
                        ?
                        <ModalPageWebView
                            closeModal={() => this.setState({ modalPageWebView: false })}
                            navigation={this.props.navigation}
                            uri={this.state.uri}
                            title={this.state.title}
                        />
                        : null
                }
                {
                    this.state.modalComingSoon
                    &&
                    <ModalComingSoon
                        modalVisible={this.state.modalComingSoon}
                        closeModal={() => this.setState({ modalComingSoon: false, modalPaymentMethod: true })}
                    />
                }
                {
                    this.state.modalConfirmCod
                    &&
                    <ModalConfirmCod
                        modalVisible={this.state.modalConfirmCod}
                        closeModal={() => this.setState({ modalConfirmCod: false, modalPaymentMethod: true })}
                        onPayment={(TypePayment) => this.onPayment(TypePayment)}
                    />
                }
                <Popover
                    isVisible={this.state.isVisible}
                    animationConfig={{ duration: 0 }}
                    fromView={this.touchable}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => this.closePopover()}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ ZipcodeHolder: '+82' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_kr.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+82</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ ZipcodeHolder: '+84' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_vn.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+84</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ ZipcodeHolder: '+66' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_th.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+66</Text>
                        </Touchable>
                    </View>
                </Popover>
                <Popover
                    isVisible={this.state.isVisibleHolder}
                    fromView={this.touchable2}
                    animationConfig={{ duration: 0 }}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => this.closePopoverHolder()}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => { this.closePopoverHolder(); this.setState({ Zipcode: '+82' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_kr.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+82</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopoverHolder(); this.setState({ Zipcode: '+84' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_vn.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+84</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopoverHolder(); this.setState({ Zipcode: '+66' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_th.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+66</Text>
                        </Touchable>
                    </View>
                </Popover>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    container: {
        flex: 1,
    },
    boxContent: {
        paddingLeft: 16,
        paddingRight: 16,
        flex: 1,
    },
    boxItem: {
        paddingBottom: 40,
        marginBottom: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#757575'
    },
    txtTicketName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxTicketPrice: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333'
    },
    txtTicketTotal: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    txtTicketDes: {
        marginTop: 15,
        fontSize: 13,
        color: '#757575'
    },
    txtDelivery: {
        marginTop: 20,
        color: '#333333',
        fontWeight: 'bold'
    },
    ipContent: {
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        fontSize: 14,
        paddingLeft: 0,
        paddingBottom: 5,
        paddingTop: 5,
        marginBottom: 5,
        marginTop: 5
    },
    boxTotalPrice: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    txtTotalTitle: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    txtTotalValue: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxRefund: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 30,
        marginTop: 30,
    },
    btnRefund: {
        width: 160,
        height: 30,
        borderWidth: 1,
        borderColor: '#757575',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtRefund: {
        color: '#757575',
        fontSize: 13
    },
    boxPurchase: {
        backgroundColor: '#03a9f4'
    },
    boxPolicy: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 13,
        paddingLeft: 20,
        paddingRight: 20
    },
    imgCheckbox: {
        width: 15,
        height: 15,
        marginRight: 15
    },
    txtPolicy: {
        fontSize: 13,
        color: '#FFFFFF',
        flex: 1
    },
    boxPurchaseButton: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        marginBottom: 15
    },
    btnPurchase: {
        width: 240,
        height: 35,
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0288d1'
    },
    txtPurchase: {
        color: '#03a9f4',
        fontSize: 17,
        fontWeight: 'bold'
    },
    boxDelivery: {
        paddingTop: 30,
        borderTopWidth: 2,
        borderTopColor: '#03a9f4'
    },
    ic_delivery_black: {
        width: 22,
        height: 16,
        marginRight: 6
    },
    ic_user: {
        width: 15,
        height: 17,
        marginRight: 6
    },
    txtDelivery: {
        fontWeight: 'bold',
        fontSize: 15,
        color: '#333333'
    },
    txtInfo: {
        fontSize: 13,
        color: '#757575',
        marginTop: 5
    },
    boxInput: {
        flexDirection: 'row',
        marginTop: 15,
        alignItems: 'center'
    },
    txtInput: {
        width: 90,
        color: '#333333',
        fontSize: 14,
        fontWeight: 'bold'
    },
    input: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        fontSize: 16,
        paddingBottom: 5,
        paddingTop: 5,
        marginBottom: 5,
        marginTop: 5,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtNote: {
        marginTop: 15,
        fontSize: 13,
        color: '#757575'
    },
    boxTicketType: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5
    },
    ic_delivery: {
        width: 16,
        height: 16,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDeliveryList: {
        fontSize: 13,
        color: '#757575'
    },
    txtDeliveryFee: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        marginTop: 5,
        marginBottom: 30,
        textAlign: 'right'
    },
    boxAddress: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 5,
        paddingTop: 5,
        marginBottom: 5,
        marginTop: 5,
    },
    txtAddress: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#333333'
    },
    boxPopover: {
        width: 130,
        padding: 12,
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 12
    },
    txtCountry: {
        color: '#333333',
        fontSize: 18
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        user: state.user,
        city: state.city,
        payment: state.payment,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataPurchase: () => {
            dispatch(actLoadDataPurchase())
        },
        onGetDistrictByCity: (city_id) => {
            dispatch(actGetDistrictByCity(city_id))
        },
        onPayment: (body, dataAnalytics) => {
            dispatch(actPayment(body, dataAnalytics))
        },
        onPaymentFree: (body, dataAnalytics) => {
            dispatch(actPaymentFree(body, dataAnalytics))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PurchaseTicketStep2);
