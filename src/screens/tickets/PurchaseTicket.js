import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Dimensions, StyleSheet, ScrollView } from 'react-native';
const { width, height } = Dimensions.get('window')
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Colors from '../../constants/Colors'
import SrcView from '../../screens_view/ScrView';
import ModalPageWebView from '../../components/ModalPageWebView';
import ModalHowToUse from '../../components/ticket/ModalHowToUse'
import ItemTicket from '../../components/ticket/ItemTicket'
import { connect } from "react-redux";
import { actLoadDataEventTickets, actGetTicket } from '../../actions/event'
import { formatNumber, convertDate, convertLanguage } from '../../services/Helper'
import * as Config from '../../constants/Config';
class PurchaseTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalHowToUse: false,
            purchases: [],
            tickets: [],
            isOnpress: true,
            modalPageWebView: false,
            uri: '',
            title: '',
        };
    }

    componentDidMount() {
        this.props.onLoadDataEventTickets(this.props.route.params['Slug']);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isOnpress: !nextProps.event.loadingGetTicket
        })
        if (nextProps.event.refreshTicket) {
            this.setState({ purchases: [] })
            this.props.onLoadDataEventTickets(this.props.route.params['Slug']);
        }
    }

    onSelect(Id, QuantityTicket) {
        var { purchases } = this.state;
        var index = purchases.some(el => el.Id === Id);
        if (index) {
            var newPurchase = [];
            if (QuantityTicket == 0) {
                newPurchase = purchases.filter(el => el.Id !== Id);
            } else {
                newPurchase = purchases.map((purchase) => {
                    if (purchase.Id === Id) {
                        purchase.QuantityTicket = QuantityTicket;
                    }
                    return purchase;
                })
            }
            this.setState({ purchases: newPurchase })
        } else {
            if (QuantityTicket != 0) {
                purchases.push({
                    Id,
                    QuantityTicket
                })
            }
            this.setState({ purchases })
        }
    }

    getTotal() {
        var { purchases } = this.state;
        var { tickets } = this.props.event;
        var total = 0;
        var ticket = [];
        purchases.forEach((purchase) => {
            ticket = tickets.filter(el => el.Id == purchase.Id);
            if (ticket.length > 0) {
                total = total + ticket[0].Price * purchase.QuantityTicket;
            }
        })
        return formatNumber(total);
    }

    getTicket() {
        this.setState({
            isOnpress: false
        })
        var { purchases } = this.state;
        var { tickets, event } = this.props.event;
        var items = [];
        purchases.forEach((purchase) => {
            var data = tickets.filter(el => el.Id === purchase.Id);
            if (data.length > 0) {
                var ticket = {
                    item_id: data[0].Id,
                    item_name: data[0].Name,
                    price: formatNumber(data[0].Price),
                    quantity: purchase.QuantityTicket
                }
                items.push(ticket)
            }
        })
        var dataAnalytics = {
            currency: event.UnitTicket?.toUpperCase(),
            value: this.getTotal(),
            items: items
        }
        this.props.onGetTicket(this.props.route.params['Slug'], purchases, dataAnalytics);
    }

    render() {
        var { tickets, loadingTicket, loadingGetTicket, event } = this.props.event;
        var { purchases } = this.state;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{
                            minWidth: 48, minHeight: 48, alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'select_ticket')}</Text>
                    <View
                        style={{
                            width: 48, minHeight: 48,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                    </View>
                </View>

                <Line />
                {
                    (loadingTicket || loadingGetTicket) &&
                    <View style={{ flex: 1, top: height / 2 - 30, width, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#000000" />
                    </View>
                }
                <View style={[styles.content, { opacity: loadingTicket ? 0 : loadingGetTicket ? 0.2 : 1 }]}>
                    <SrcView style={styles.container}>
                        <View style={styles.boxContent}>
                            <View style={styles.boxIntroduce}>
                                <Text style={styles.txtEventName}>{event.Title}</Text>
                                <Text style={styles.txtEventTime}>{convertDate(event.TimeStart, event.TimeFinish)}</Text>
                                <Text style={styles.txtEventTime}>{event.VenueName} - {event.Address}</Text>
                            </View>
                            {
                                tickets.length > 0
                                &&
                                <View style={styles.boxTicket}>
                                    {
                                        tickets.map((ticket, index) => {
                                            if (ticket.Type === 'd-p-ticket' && ticket.statusCOD === 0) {
                                                return null;
                                            }
                                            return <ItemTicket key={index} ticket={ticket} onSelect={(Id, QuantityTicket) => this.onSelect(Id, QuantityTicket)} />
                                        })
                                    }
                                </View>
                            }
                            <View style={styles.boxButton}>
                                <Touchable style={styles.btnHowToUse} onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/how-to-use-tickets`, title: convertLanguage(language, 'how_to_use_tickets') })}>
                                    <Text style={styles.txtHowToUse} numberOfLines={1}>{convertLanguage(language, 'how_to_use_tickets')}</Text>
                                </Touchable>
                                <Touchable style={styles.btnHowToUse} onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/refund-policy`, title: convertLanguage(language, 'refund_policy') })}>
                                    <Text style={styles.txtHowToUse} numberOfLines={1}>{convertLanguage(language, 'refund_policy')}</Text>
                                </Touchable>
                            </View>
                        </View>
                    </SrcView>
                    {tickets.length === 0 &&
                        <View style={styles.boxEmptyTicket}>
                            <Image source={require('../../assets/empty_ticket.png')} style={styles.imgEmptyTicket} />
                            <Text style={styles.txtEmptyTicket}>{convertLanguage(language, 'no_ticket_is_on_sales_now')}</Text>
                        </View>
                    }
                    <View style={styles.btnTicket}>
                        <View style={styles.boxEventMoreLeft}>
                            <Text style={styles.txtTotalTitle}>{convertLanguage(language, 'total')}</Text>
                            <Text style={styles.txtTotalValue}>{event.UnitTicket && event.UnitTicket.toUpperCase() + ' '}{this.getTotal()}</Text>
                        </View>
                        <Touchable disabled={purchases.length === 0} style={[styles.btnGetTicket, { backgroundColor: purchases.length > 0 ? '#FFFFFF' : '#0288d1' }]} onPress={() => this.getTicket()}>
                            <Text style={styles.txtGetTicket}>{convertLanguage(language, 'get_ticket')}</Text>
                        </Touchable>
                    </View>
                </View>
                {
                    this.state.modalPageWebView
                        ?
                        <ModalPageWebView
                            closeModal={() => this.setState({ modalPageWebView: false })}
                            navigation={this.props.navigation}
                            uri={this.state.uri}
                            title={this.state.title}
                        />
                        : null
                }
                {
                    this.state.modalHowToUse
                        ?
                        <ModalHowToUse
                            modalVisible={this.state.modalHowToUse}
                            closeModal={() => this.setState({ modalHowToUse: false })}
                            Title={event.Title}
                            navigation={this.props.navigation}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    container: {
        flex: 1,
    },
    boxContent: {
        paddingLeft: 20,
        paddingRight: 20,
        flex: 1
    },
    boxIntroduce: {
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 2,
        borderBottomColor: '#03a9f4'
    },
    txtEventName: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtEventTime: {
        color: '#757575',
        fontSize: 14
    },
    boxTicket: {

    },
    boxItem: {
        paddingTop: 25,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    txtTicketName: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtTicketDes: {
        fontSize: 13,
        color: '#757575',
        paddingBottom: 10,
    },
    txtShow: {
        fontSize: 13,
        color: '#333333',
        fontWeight: '400',
        paddingBottom: 15
    },
    txtDetailActive: {
        fontSize: 13,
        color: '#03a9f4'
    },
    boxOption: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5
    },
    txtDetail: {
        fontSize: 13,
        color: '#bdbdbd'
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20
    },
    txtTicketPrice: {
        color: '#333333',
        fontSize: 14,
        fontWeight: 'bold'
    },
    boxButton: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnHowToUse: {
        flex: 0.48,
        height: 30,
        borderWidth: 1,
        borderColor: '#757575',
        borderRadius: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
    },
    txtHowToUse: {
        fontSize: 13,
        color: '#757575'
    },
    btnTicket: {
        height: 55,
        backgroundColor: '#03a9f4',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    boxEventMoreLeft: {

    },
    txtTotalTitle: {
        fontSize: 14,
        color: '#FFFFFF'
    },
    txtTotalValue: {
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    btnGetTicket: {
        backgroundColor: '#0288d1',
        height: 35,
        width: 130,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    txtGetTicket: {
        color: '#03a9f4',
        fontSize: 17,
        fontWeight: 'bold'
    },
    boxEmptyTicket: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 90
    },
    imgEmptyTicket: {
        width: 97,
        height: 89,
        marginBottom: 15
    },
    txtEmptyTicket: {
        color: '#bdbdbd',
        fontSize: 15
    },
    txtSoldOut: {
        fontSize: 14,
        color: '#bdbdbd',
        fontWeight: 'bold'
    },
    boxPicker: {
        borderWidth: 1,
        borderColor: '#bdbdbd',
        borderRadius: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: 72,
        height: 30
    },
    txtPicker: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginTop: 1
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataEventTickets: (slug) => {
            dispatch(actLoadDataEventTickets(slug))
        },
        onGetTicket: (slug, purchases, dataAnalytics) => {
            dispatch(actGetTicket(slug, purchases, dataAnalytics))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PurchaseTicket);
