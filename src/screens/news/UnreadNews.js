import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView, Linking, StatusBar, Platform, TouchableOpacity } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import { convertDateTime } from '../../services/Helper'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataUnreadNews, actReadNotification, actMarkReadAll } from '../../actions/news';
import moment from "moment";
import TimeAgo from 'react-native-timeago';
class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
    }

    componentDidMount() {
        if (this.props.language.language === 'vi') {
            require('moment/locale/vi')
        }
        if (this.props.language.language === 'en') {
            require('moment/locale/en-au')
        }
        if (this.props.language.language === 'ko') {
            require('moment/locale/ko')
        }
        var { iso_code } = this.props.global;
        this.props.onLoadData(1, iso_code)
    }

    _renderFooter = () => {
        var { dataUnreadNews } = this.props.news
        var { language } = this.props.language;


        if (dataUnreadNews.loadMore) { return <ActivityIndicator size="large" color="#000000" style={styles.loading} /> } else {
            if (dataUnreadNews.is_empty) {
                return <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{ color: '#757575', paddingTop: 36, textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'no_notification')}</Text>
                    </View>
                </View>
            } else {
                return null
            }
        }
    }


    renderItem = (item) => {
        var url = 'comeup:/' + item.Notification.Url;
        return <Touchable style={{ flexDirection: 'row', paddingTop: 15 }} onPress={() => { Linking.openURL(url); this.props.onReadNews(item.Id) }}>
            <Image source={{ uri: item.Notification.Thumbnail }} style={{ borderRadius: 50, width: 112 / 2, height: 112 / 2, backgroundColor: '#bdbdbd' }} />
            <View style={{ justifyContent: 'center', paddingHorizontal: 10, flex: 1 }}>
                <Text style={{ fontSize: 14, color: '#333333', fontWeight: item.IsRead == 1 || item.IsJustRead ? 'normal' : 'bold' }} >{item.Notification.Context}</Text>
                <Text style={{ fontSize: 12 }} >
                    {/* <TimeAgo time={convertDateTime(item.Notification.CreatedAt)} /> */}
                    {item.CreatedAt}
                </Text>
            </View>
        </Touchable>
    }
    refresh = () => {
        var { iso_code } = this.props.global;
        this.props.onLoadData(1, iso_code)
        this.setState({ page: 1 })
    }
    loadMore = () => {
        var { dataUnreadNews } = this.props.news;
        var { iso_code } = this.props.global;
        if (dataUnreadNews.loadMore) {
            this.props.onLoadData(this.state.page + 1, iso_code)
            this.setState({ page: this.state.page + 1 })
        }
    }

    render() {
        StatusBar.setBarStyle(Platform.OS === 'android' ? 'light-content' : 'dark-content', true);
        var { language } = this.props.language;
        var { dataUnreadNews } = this.props.news;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <View style={{ flex: 0.3 }}>
                        <Touchable
                            onPress={() => this.props.navigation.goBack()}
                            style={{
                                minWidth: 40, minHeight: 50, marginLeft: 20, alignSelf: 'flex-start',
                                justifyContent: 'center',
                            }}>
                            <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                        </Touchable>
                    </View>
                    <View style={{ flex: 0.4, alignSelf: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'unread_news')}</Text>
                    </View>
                    <Touchable onPress={() => this.props.onMarkReadAll()} style={{ minHeight: 50, marginRight: 5, justifyContent: 'center', alignItems: 'flex-end', flex: 0.3, alignSelf: 'flex-end' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, color: '#00A9F4', marginRight: 16 }} numberOfLines={1}>{convertLanguage(language, 'mark_as_all_read')}</Text>
                    </Touchable>
                </View>

                <Line />
                <View style={styles.content}>
                    <FlatList
                        contentContainerStyle={{ paddingLeft: 16, paddingRight: 16, paddingBottom: 30 }}
                        data={dataUnreadNews.data}
                        renderItem={({ item }) => {
                            return this.renderItem(item)
                        }}
                        onEndReached={this.loadMore}
                        // ListHeaderComponent={this.renderHeader}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.2}
                        keyExtractor={(item, index) => index.toString()}
                        refreshing={false}
                        onRefresh={this.refresh}
                        ListFooterComponent={this._renderFooter}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({

    new1: {
        paddingTop: 20,
        // paddingBottom:12,
        borderBottomColor: '#bdbdbd',
        borderBottomWidth: 1
    },
    content: {
        flex: 1,
    },
    txtTitle: {
        color: Colors.TEXT_P,
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 15,
    },
    loading: {
        padding: 20
    },
    boxPopover: {
        paddingVertical: 8,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 4,
    },
    boxNewRead: {
        paddingVertical: 4,
        paddingHorizontal: 24
    },
    txtNewRead: {
        color: '#212529',
        fontSize: 16
    },
});

const mapStateToProps = state => {
    return {
        news: state.news,
        language: state.language,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadData: (page, iso_code) => {
            dispatch(actLoadDataUnreadNews(page, iso_code))
        },
        onReadNews: (newsId) => {
            dispatch(actReadNotification(newsId))
        },
        onMarkReadAll: () => {
            dispatch(actMarkReadAll())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(News);
// export default EventHostList;
