import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView, Linking, StatusBar, Platform, TouchableOpacity } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import { convertDateTime } from '../../services/Helper'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataNews, actClearDataNotification, actReadNotification } from '../../actions/news';
import TimeAgo from 'react-native-timeago';
import NetInfo from "@react-native-community/netinfo";
import NoInternet from '../../screens_view/NoInternet';
import Popover from 'react-native-popover-view';
let moment = require('moment');
class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            x: false,
            isConnected: true,
            isVisible: false
        };
    }

    componentDidMount() {
        if (this.props.language.language === 'vi') {
            require('moment/locale/vi')
        }
        if (this.props.language.language === 'en') {
            require('moment/locale/en-au')
        }
        if (this.props.language.language === 'ko') {
            require('moment/locale/ko')
        }
        this.checkConnect()
    }

    checkConnect() {
        this.setState({ isConnected: true })
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                var { profile } = this.props.user;
                var { iso_code } = this.props.global;
                if (profile.Id) {
                    var { page } = this.state;
                    this.props.onLoadData(1, iso_code)
                }
            } else {
                this.setState({ isConnected: false })
            }
        });
    }


    _renderFooter = () => {
        var { dataNews } = this.props.news;
        var { language } = this.props.language;


        if (dataNews.loadMore) { return <ActivityIndicator size="large" color="#000000" style={styles.loading} /> } else {
            if (dataNews.is_empty) {
                return <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{ color: '#757575', paddingTop: 36, textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'no_notification')}</Text>
                    </View>
                </View>
            } else {
                return null
            }
        }
    }
    renderHeader = () => {
        var { language } = this.props.language;
        return <Text style={styles.txtTitle}>{convertLanguage(language, 'new')}</Text>
    }


    renderItem = (item, index, prevItem, ) => {
        // moment.locale('en-au');
        var url = 'comeup:/' + item.Notification.Url;
        var checkListChange = false
        if (item.IsRead == 1 && prevItem && prevItem.IsRead == 0) {
            checkListChange = true
        }
        return <View key={'new2_' + index}>
            {
                checkListChange && <View style={{ height: 1, backgroundColor: '#bdbdbd', marginTop: 15 }}></View>
            }
            <Touchable style={{ flexDirection: 'row', paddingTop: 15 }} onPress={() => { console.log(url); Linking.openURL(url); this.props.onReadNews(item.Id) }}>
                <Image source={{ uri: item.Notification.Thumbnail }} style={{ borderRadius: 50, width: 112 / 2, height: 112 / 2, backgroundColor: '#bdbdbd' }} />
                <View style={{ justifyContent: 'center', paddingHorizontal: 10, flex: 1 }}>
                    <Text style={{ fontSize: 16, color: '#333333', fontWeight: item.IsRead == 1 || item.IsJustRead ? 'normal' : 'bold' }} >{item.Notification.Context}</Text>
                    <Text style={{ fontSize: 12 }} >
                        {/* <TimeAgo time={convertDateTime(item.Notification.CreatedAt)} /> */}
                        {item.CreatedAt}
                    </Text>
                </View>
            </Touchable>
        </View>
    }
    refresh = () => {
        var { iso_code } = this.props.global;
        this.props.onLoadData(1, iso_code)
        this.setState({ page: 1 })
    }
    loadMore = () => {
        var { dataNews } = this.props.news;
        if (dataNews.loadMore) {
            var { iso_code } = this.props.global;
            this.props.onLoadData(this.state.page + 1, iso_code)
            this.setState({ page: this.state.page + 1 })
        }
    }

    componentWillUnmount() {
        this.props.clearDataNotification()
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    render() {
        StatusBar.setBarStyle(Platform.OS === 'android' ? 'light-content' : 'dark-content', true);
        var { language } = this.props.language;
        var { profile } = this.props.user;
        var { dataNews } = this.props.news;
        var { isConnected } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{
                            minWidth: 40, minHeight: 50, marginLeft: 20, alignSelf: 'flex-start',
                            justifyContent: 'center'
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ alignItems: 'center', fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'news')}</Text>
                    <TouchableOpacity style={{ minHeight: 50, minWidth: 50, justifyContent: 'center', alignItems: 'center' }} ref={ref => this.touchable = ref} onPress={() => this.showPopover()}>
                        <Image style={{ width: 12, height: 22 }} source={require('../../assets/more_1_top.png')} />
                    </TouchableOpacity>
                </View>

                <Line />
                {
                    !isConnected ?
                        <NoInternet onPress={() => this.checkConnect()} />
                        :
                        profile.Id ?
                            <View style={styles.content}>
                                <FlatList
                                    contentContainerStyle={{ paddingLeft: 20, paddingRight: 20, paddingBottom: 30 }}
                                    data={dataNews.data}
                                    renderItem={({ item, index }) => {
                                        return this.renderItem(item, index, dataNews.data[index - 1])
                                    }}
                                    onEndReached={this.loadMore}
                                    ListHeaderComponent={this.renderHeader}
                                    // ItemSeparatorComponent={this.renderSep}
                                    onEndReachedThreshold={0.2}
                                    keyExtractor={(item, index) => index.toString()}
                                    refreshing={false}
                                    onRefresh={this.refresh}
                                    ListFooterComponent={this._renderFooter}
                                />
                            </View>
                            :
                            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                                <Touchable
                                    onPress={() => this.props.navigation.navigate('Login')}
                                    style={{ alignSelf: 'center', borderRadius: 4, marginTop: 20, marginBottom: 20, paddingLeft: 40, paddingRight: 40, paddingTop: 10, paddingBottom: 10, backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>{convertLanguage(language, 'login')}</Text>
                                </Touchable>
                            </View>
                }
                <Popover
                    isVisible={this.state.isVisible}
                    animationConfig={{duration: 0}}
                    fromView={this.touchable}
                    placement='bottom'
                    arrowStyle={{backgroundColor: 'transparent', height: 0, width: 0,}}
                    backgroundStyle={{backgroundColor: 'rgba(0, 0, 0, 0.1)',}}
                    onRequestClose={() => this.closePopover()}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => { this.closePopover(); this.props.navigation.navigate('UnreadNews') }} style={styles.boxNewRead}>
                            <Text style={styles.txtNewRead}>{convertLanguage(language, 'unread_news')}</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.props.navigation.navigate('ReadNews') }} style={styles.boxNewRead}>
                            <Text style={styles.txtNewRead}>{convertLanguage(language, 'read_news')}</Text>
                        </Touchable>
                    </View>
                </Popover>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({

    new1: {
        paddingTop: 20,
        // paddingBottom:12,
        borderBottomColor: '#bdbdbd',
        borderBottomWidth: 1
    },
    content: {
        flex: 1,
    },
    txtTitle: {
        color: Colors.TEXT_P,
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 15,
    },
    loading: {
        padding: 20
    },
    boxPopover: {
        paddingVertical: 8,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 4,
    },
    boxNewRead: {
        paddingVertical: 4,
        paddingHorizontal: 24
    },
    txtNewRead: {
        color: '#212529',
        fontSize: 16
    },
});

const mapStateToProps = state => {
    return {
        user: state.user,
        language: state.language,
        news: state.news,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadData: (page, iso_code) => {
            dispatch(actLoadDataNews(page, iso_code))
        },
        clearDataNotification: () => {
            dispatch(actClearDataNotification())
        },
        onReadNews: (newsId) => {
            dispatch(actReadNotification(newsId))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(News);
// export default EventHostList;
