import React, { Component } from 'react';
import { View, Text, Image, FlatList, StyleSheet, TouchableOpacity, ScrollView, ActivityIndicator, Platform } from 'react-native';

import Touchable from '../screens_view/Touchable';
import SafeView from '../screens_view/SafeView';
import SaleReportItem from '../components/team/SaleReportItem';
import DatePicker from 'react-native-datepicker';
import * as Colors from '../constants/Colors';
import { actLoadDataSaleReportList } from '../actions/team';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import Line from '../screens_view/Line';
import { formatNumber } from '../services/Helper';
import PickerItem from '../screens_view/PickerItem';
class SaleReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TimeStart: '',
            TimeFinish: '',
            Page: 1,
            time: ''
        };
    }

    componentDidMount() {
        var { team } = this.props.team;
        var { Page } = this.state;
        this.props.onLoadDataSaleReportList(team.Id, Page);
    }

    onChangeTime(time) {
        var { team } = this.props.team;
    this.setState({time})
        if(time != -1) {
            this.setState({Page: 1, TimeStart: '', TimeFinish: ''})
            this.props.onLoadDataSaleReportList(team.Id, 1, time);
        }
    }

    onChangeTimeFinish(TimeFinish) {
        var { team } = this.props.team;
        this.setState({TimeFinish})
        var { TimeStart } = this.state;
        this.setState({Page: 1})
        this.props.onLoadDataSaleReportList(team.Id, 1, '-1', TimeStart, TimeFinish);
    }

    _renderHeader() {
        var { language } = this.props.language;
        var { TimeStart, TimeFinish, time } = this.state;
        var { saleReport } = this.props.team;
        return <>
            {
                // ((saleReport.SaleReportVn && saleReport.SaleReportVn.TotalSales > 0) || (saleReport.SaleReportKr && saleReport.SaleReportKr.TotalSales > 0))  &&
                <>
                    {/* <TouchableOpacity style={styles.btnFilterChoose}>
                        <Text style={styles.txtBtnFilterChoose}>{convertLanguage(language, 'all_days')}</Text>
                        <Image source={require('../../assets/select_arrow_black.png')} style={{ width: 24, height: 24 }} />
                    </TouchableOpacity> */}
                    <PickerItem
                        boxDisplayStyles={styles.boxPicker}
                        containerStyle={[{height: 56}, Platform.OS === 'ios' ? {paddingHorizontal: 12} : {}]}
                        iconStyle={{marginRight: 15}}
                        options={[
                            {
                                name: convertLanguage(language, 'all_days'),
                                value: ''
                            },
                            {
                                name: convertLanguage(language, 'last_30_day'),
                                value: '30'
                            },
                            {
                                name: convertLanguage(language, 'last_60_day'),
                                value: '60'
                            },
                            {
                                name: convertLanguage(language, 'last_90_day'),
                                value: '90'
                            },
                            {
                                name: convertLanguage(language, 'custom'),
                                value: '-1'
                            },
                        ]}
                        selectedOption={time}
                        onSubmit={(time) => this.onChangeTime(time)}
                    />
                    <View style={styles.boxDate}>
                        <DatePicker
                            style={{ flex: 0.45 }}
                            date={TimeStart}
                            mode="date"
                            placeholder={convertLanguage(language, 'total_sales')}
                            format="YYYY-MM-DD"
                            minDate="1972-01-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            // disabled={time == -1 ? false : true}
                            iconComponent={<Image style={{ position: 'absolute', right: 5, top: 14, width: 24, height: 24 }} source={require('../assets/calendar_icon2.png')} />}
                            customStyles={{
                                dateInput: {
                                    borderColor: '#bdbdbd',
                                    borderRadius: 4,
                                    borderWidth: 1,
                                    paddingLeft: 5,
                                    paddingRight: 5,
                                    height: 52,
                                    marginTop: 12,
                                    backgroundColor: time !== '-1' ? '#ECEEE1' : '#FFFFFF',
                                    opacity: time !== '-1' ? 0.3 : 1
                                },
                                dateText: {
                                    color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                },
                                btnTextConfirm: {
                                    color: Colors.PRIMARY, fontSize: 17
                                },
                                btnTextCancel: {
                                    color: Colors.TEXT_P, fontSize: 16
                                },
                                placeholderText: {
                                    alignSelf: 'flex-start',
                                    fontSize: 15,
                                    color: '#bdbdbd'
                                }
                                // ... You can check the source to find the other keys.
                            }}
                            onDateChange={(TimeStart) => this.setState({ TimeStart, TimeFinish: '', time: '-1' })}
                        />
                        <Text style={styles.txtCenter}>~</Text>
                        <DatePicker
                            style={{ flex: 0.45 }}
                            date={TimeFinish}
                            mode="date"
                            placeholder={convertLanguage(language, 'finish')}
                            format="YYYY-MM-DD"
                            minDate={TimeStart === '' ? '1972-01-01' : TimeStart}
                            disabled={time == -1 && TimeStart !== '' ? false : true}
                            // maxDate="01-01-2010"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            iconComponent={<Image style={{ position: 'absolute', right: 5, top: 14, width: 24, height: 24 }} source={require('../assets/calendar_icon2.png')} />}
                            customStyles={{
                                dateInput: {
                                    borderColor: '#bdbdbd',
                                    borderRadius: 4,
                                    borderWidth: 1,
                                    paddingLeft: 5,
                                    paddingRight: 5,
                                    height: 52,
                                    marginTop: 12,
                                    backgroundColor: time !== '-1' ? '#ECEEE1' : '#FFFFFF',
                                    opacity: time !== '-1' ? 0.3 : 1
                                },
                                dateText: {
                                    color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                },
                                btnTextConfirm: {
                                    color: Colors.PRIMARY, fontSize: 17
                                },
                                btnTextCancel: {
                                    color: Colors.TEXT_P, fontSize: 16
                                },
                                placeholderText: {
                                    alignSelf: 'flex-start',
                                    fontSize: 15,
                                    color: '#bdbdbd'
                                }
                                // ... You can check the source to find the other keys.
                            }}
                            onDateChange={(TimeFinish) => this.onChangeTimeFinish(TimeFinish)}
                        />
                    </View>
                    <View style={styles.boxContent}>
                        {
                            saleReport.SaleReportVn && saleReport.SaleReportVn.TotalSales > 0 &&
                            <View style={styles.rowDashboard}>
                                <Text style={[styles.txtDashboard]}>{convertLanguage(language, 'total_sales')}: VND {formatNumber(saleReport.SaleReportVn.TotalSales.toFixed(0))}</Text>
                                <Text style={[styles.txtRevenue]}>{convertLanguage(language, 'revenue')}: VND {formatNumber(saleReport.SaleReportVn.TotalRevenue.toFixed(0))}</Text>
                            </View>
                        }
                        {
                            saleReport.SaleReportKr && saleReport.SaleReportKr.TotalSales > 0 &&
                            <View style={styles.rowDashboard}>
                                <Text style={[styles.txtDashboard]}>{convertLanguage(language, 'total_sales')}: KRW {formatNumber(saleReport.SaleReportKr.TotalSales.toFixed(0))}</Text>
                                <Text style={[styles.txtRevenue]}>{convertLanguage(language, 'revenue')}: KRW {formatNumber(saleReport.SaleReportKr.TotalRevenue.toFixed(0))}</Text>
                            </View>
                        }
                    </View>
                </>
            }
        </>
    }

    _renderFooter() {
        var { loadMoreSaleReport, is_empty } = this.props.team;
        var { language } = this.props.language;
        if (loadMoreSaleReport) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20, paddingBottom: 20 }}>{convertLanguage(language, 'data_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { team, loadMoreSaleReport } = this.props.team;
        var { Page, time, TimeStart, TimeFinish } = this.state;
        if (loadMoreSaleReport) {
            this.props.onLoadDataSaleReportList(team.Id, Page + 1, time, TimeStart, TimeFinish);
            this.setState({ Page: Page + 1 })
        }
    }

    render() {
        var { saleReport } = this.props.team;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <View style={{ width: 48 }} />
                </View>
                <Line />

                <View style={styles.content}>
                    <FlatList
                        data={saleReport.Events}
                        contentContainerStyle={{ padding: 16 }}
                        renderItem={({ item }) => {
                            return <SaleReportItem item={item} navigation={this.props.navigation} />
                        }}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        ListHeaderComponent={() => this._renderHeader()}
                        ListFooterComponent={() => this._renderFooter()}
                        onEndReached={() => { this.loadMore() }}
                    // onRefresh={() => {this.refresh()}}
                    // refreshing={false}
                    />
                </View>

            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
    },
    filter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'

    },
    boxPicker: {
        borderWidth: 1,
        borderColor: '#bdbdbd',
        borderRadius: 1,
        height: 56,
        marginBottom: 15,
        borderRadius: 4
    },
    btnFilterDisable: {
        borderColor: Colors.DISABLE
    },
    txtBtnFilterChoose: {
        fontSize: 15,
        color: '#333333',
        marginRight: 5
    },
    inputDate: {
        flex: 1
    },
    rowDashboard: {
        borderBottomWidth: 1,
        borderColor: '#000000',
        paddingBottom: 30,
        paddingTop: 10
    },
    txtBold: {
        fontWeight: 'bold'
    },
    txtDashboard: {
        paddingTop: 14,
        fontSize: 20,
        color: '#333333',
        fontWeight: 'bold'
    },
    txtRevenue: {
        paddingTop: 5,
        fontSize: 16,
        color: '#828282'
    },
    rowCurrent: {
        marginTop: 16,
        marginBottom: 24
    },
    boxDate: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 24
    },
    txtCenter: {
        flex: 0.1,
        color: '#757575',
        fontSize: 28,
        textAlign: 'center'
    },
    boxContent: {
        marginBottom: 24,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(51, 51, 51, 0.25)',
    }
});
const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataSaleReportList: (TeamId, Page, Time, TimeStart, TimeFinish) => {
            dispatch(actLoadDataSaleReportList(TeamId, Page, Time, TimeStart, TimeFinish))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SaleReport);
