import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet } from 'react-native';

import SafeView from '../screens_view/SafeView'
import Touchable from '../screens_view/Touchable'
import Line from '../screens_view/Line'
import ItemEvent from '../components/event/ItemEvent'
import * as Colors from '../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataEventByTeam } from '../actions/team'
import { actLikeEvent } from '../actions/event'
import { convertLanguage } from '../services/Helper'
class EventByTeams extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1
        };
    }

    componentDidMount() {
        this.props.onLoadDataEventByTeam(this.props.route.params['Slug'], 1);
    }

    _renderFooter() {
        var { language } = this.props.language;
        var { loadMoreEvent, is_empty } = this.props.team;
        if (loadMoreEvent) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'event_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { loadMoreEvent } = this.props.team;
        var { page } = this.state;
        if (loadMoreEvent) {
            this.props.onLoadDataEventByTeam(this.props.route.params['Slug'], page+1);
            this.setState({ page: page + 1 })
        }
    }

    render() {
        var { events } = this.props.team;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <View style={{ minHeight: 48, marginRight: 20, justifyContent: 'center', alignItems: 'center' }}></View>
                </View>

                <Line />

                <View style={styles.content}>
                    <FlatList
                        style={{ paddingTop: 10 }}
                        data={events}
                        renderItem={({ item }) => {
                            return <ItemEvent cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} />
                        }}
                        onEndReached={() => { this.loadMore() }}
                        // ListHeaderComponent={() => this.renderHeader()}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        // refreshing={this.props.team.loading}
                        // onRefresh={() => {this.refresh()}}
                        ListFooterComponent={() => this._renderFooter()}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
});

const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataEventByTeam: (id, page) => {
            dispatch(actLoadDataEventByTeam(id, page))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventByTeams);
// export default EventHostList;
