import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import ItemEvent from '../../components/event/ItemEvent';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { actLoadDataPastEventLiked } from '../../actions/like';
import { actLikeEvent } from '../../actions/event'
class PastEventLiked extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
    }

    componentDidMount() {
        var { page } = this.state;
        this.props.onLoadDataPastEventLiked(page);
    }

    refresh() {
        this.setState({page: 1})
        this.props.onLoadDataPastEventLiked(1);
    }

    _renderFooter() {
        var { language } = this.props.language;
        var { loadMorePastEvent, past_events } = this.props.like;
        if (loadMorePastEvent) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            return <View style={{alignItems: 'center'}}>
                {
                    past_events.length === 0 &&
                    <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'no_data')}</Text>
                }
            </View>
        }
    }

    loadMore() {
        var { loadMorePastEvent } = this.props.like;
        var { page } = this.state;
        if (loadMorePastEvent) {
            this.props.onLoadDataPastEventLiked(page + 1);
            this.setState({ page: page + 1 })
        }
    }

    render() {
        var { language } = this.props.language;
        var { past_events } = this.props.like;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'liked_past_event')}</Text>
                    <View style={{ minHeight: 48, marginRight: 20, justifyContent: 'center', alignItems: 'center' }}>

                    </View>
                </View>

                <Line />

                <View style={styles.content}>
                    <FlatList
                        contentContainerStyle={styles.boxListEvent}
                        data={past_events}
                        renderItem={({ item }) => {
                            return <ItemEvent cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} />
                        }}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => this._renderFooter()}
                        // ListHeaderComponent={() => this._renderHeader()}
                        onRefresh={() => {this.refresh()}}
                        onEndReached={() => { this.loadMore() }}
                        refreshing={false}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxListEvent: {
        paddingTop: 20,
    },
    boxPastTicket: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        height: 30,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        marginTop: 20,
        marginBottom: 20
    }
});

const mapStateToProps = state => {
    return {
        language: state.language,
        like: state.like
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataPastEventLiked: (page) => {
            dispatch(actLoadDataPastEventLiked(page))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PastEventLiked);
// export default EventHostList;
