import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import ItemVenue from '../../components/venue/ItemVenue';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { actLoadDataEventLiked } from '../../actions/like';
class VenueLiked extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
    }

    componentDidMount() {
        var { page } = this.state;
        this.props.onLoadDataEventLiked(page);
    }

    refresh() {
        this.setState({ page: 1 })
        this.props.onLoadDataEventLiked(1);
    }

    _renderFooter() {
        var { language } = this.props.language;
        var { loadMoreEvent, events } = this.props.like;
        if (loadMoreEvent) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            return <View style={{ alignItems: 'center' }}>
                {
                    events.length === 0 &&
                    <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'no_data')}</Text>
                }
            </View>
        }
    }

    loadMore() {
        var { loadMoreEvent } = this.props.like;
        var { page } = this.state;
        if (loadMoreEvent) {
            this.props.onLoadDataEventLiked(page + 1);
            this.setState({ page: page + 1 })
        }
    }

    render() {
        var { events } = this.props.like;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'liked_venue')}</Text>
                    <Touchable style={{ minHeight: 48, minWidth: 48, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')} >
                        <Image source={require('../../assets/search.png')} style={{ width: 32, height: 32 }} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    <FlatList
                        contentContainerStyle={styles.boxListEvent}
                        data={events}
                        renderItem={({ item }) => {
                            return <ItemVenue data={item} navigation={this.props.navigation} />
                        }}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => this._renderFooter()}
                        // ListHeaderComponent={() => this._renderHeader()}
                        onRefresh={() => { this.refresh() }}
                        onEndReached={() => { this.loadMore() }}
                        refreshing={false}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    txtTitle: {
        color: Colors.TEXT_P,
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 20,
    },
    boxListEvent: {
        // flexDirection: 'column',
        paddingTop: 20,
        // paddingLeft: 20,
        // paddingRight: 20
    },
    boxSeeAll: {
        height: 48,
        borderWidth: 1,
        borderColor: '#333333',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
    },
    txtSeeAll: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        like: state.like
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataEventLiked: (page) => {
            dispatch(actLoadDataEventLiked(page))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(VenueLiked);
// export default EventHostList;
