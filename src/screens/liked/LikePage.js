import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Alert, StatusBar, Platform } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import ItemEventHalf from '../../components/event/ItemEventHalf';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { actLoadDataLiked } from '../../actions/like';
import { actLikeTeamNews, actDeleteTeamNews } from '../../actions/team';
import { actLikeEventNews, actLikeEvent, actDeleteEventNews } from '../../actions/event';
import ItemEventNews from '../../components/event/ItemEventNews';
import TeamNewsItem from '../../components/team/TeamNewsItem';
import ModalEventNews from '../../components/event/ModalEventNews';
import ModalTeamNews from '../../components/team/ModalTeamNews';
import ModalEventNewsDetail from '../../components/event/ModalEventNewsDetail';
import ModalTeamNewsDetail from '../../components/team/ModalTeamNewsDetail';
import NoInternet from '../../screens_view/NoInternet';
import NetInfo from "@react-native-community/netinfo";
import { actGlobalCheckLogin } from '../../actions/global';
class LikePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            modelEventNews: false,
            EventNewsId: '',
            modelTeamNews: false,
            TeamNewsId: '',
            Role: 'Guest',
            isConnected: true,
            eventnews: {},
            modalEventNewsDetail: false,
            teamnews: {},
            modalTeamNewsDetail: false,
        };
    }

    componentDidMount() {
        this.props.navigation.setParams({
            scrollToTop: this._scrollToTop,
        });
        this.checkConnect()
    }

    _scrollToTop = () => {
        // Scroll to top, in this case I am using FlatList
        if (!!this.refs.listRef) {
            this.refs.listRef.scrollToOffset({ x: 0, y: 0, animated: true })
        }
    }

    checkConnect() {
        this.setState({ isConnected: true })
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                var { profile } = this.props.user;
                var { iso_code } = this.props.global;
                if (profile.Id) {
                    var { page } = this.state;
                    this.props.onLoadDataLiked(page, iso_code);
                }
            } else {
                this.setState({ isConnected: false })
            }
        });
    }

    refresh = () => {
        this.setState({ page: 1 })
        var { iso_code } = this.props.global;
        this.props.onLoadDataLiked(1, iso_code);
    }

    _renderFooter = () => {
        var { language } = this.props.language;
        var { liked } = this.props.like;
        if (liked.loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            return <View style={{ alignItems: 'center' }}>
                {
                    liked.posts.length === 0 &&
                    <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'no_data')}</Text>
                }
            </View>
        }
    }

    loadMore = () => {
        var { liked } = this.props.like;
        var { page } = this.state;
        var { iso_code } = this.props.global;
        if (liked.loadMore) {
            this.props.onLoadDataLiked(page + 1, iso_code);
            this.setState({ page: page + 1 })
        }
    }

    _renderHeader = () => {
        var { language } = this.props.language;
        var { liked } = this.props.like;
        return <>
            {
                liked.events.length > 0 &&
                <>
                    <View style={styles.boxTitle}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'event')}</Text>
                    </View>
                    <View style={styles.boxListEvent}>
                        {
                            liked.events.map((item, index) => {
                                return <ItemEventHalf cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} key={index} onLikeEvent={(id) => this.props.onLikeEvent(id)} />
                            })
                        }
                    </View>
                    <Touchable style={styles.boxSeeAll} onPress={() => this.props.navigation.navigate('EventLiked')}>
                        <Text style={styles.txtSeeAll}>{convertLanguage(language, 'see_all')}</Text>
                    </Touchable>
                </>
            }
            {
                // liked.posts.length > 0 &&
                <View style={styles.boxTitle}>
                    <Text style={styles.txtTitle}>{convertLanguage(language, 'content')}</Text>
                </View>
            }
        </>
    }

    onOpenModelEventNews(Id, Role) {
        this.setState({
            modelEventNews: true,
            EventNewsId: Id,
            Role: Role
        })
    }

    onOpenModelEventNewsDetail(item, Role) {
        this.setState({
            modalEventNewsDetail: true,
            eventnews: item,
            Role: Role
        })
    }

    onOpenModelTeamNewsDetail(item, Role) {
        this.setState({
            modalTeamNewsDetail: true,
            teamnews: item,
            Role: Role
        })
    }

    onReportEventNews() {
        var { EventNewsId } = this.state;
        this.setState({
            modelEventNews: false,
        })
        this.props.navigation.navigate('EventNewsReport', { Id: EventNewsId })
    }

    onOpenModelTeamNews(Id, Role) {
        this.setState({
            modelTeamNews: true,
            TeamNewsId: Id,
            Role: Role
        })
    }

    onReportTeamNews() {
        var { TeamNewsId } = this.state;
        this.setState({
            modelTeamNews: false,
        })
        this.props.navigation.navigate('TeamNewsReport', { TeamNewsId: TeamNewsId })
    }

    onEditTeamNews() {
        var { TeamNewsId } = this.state;
        this.setState({
            modelTeamNews: false,
        })
        this.props.navigation.navigate('CreateANews', { TeamNewsId: TeamNewsId })
    }

    onDeleteTeamNews() {
        var { language } = this.props.language;
        var { TeamNewsId } = this.state;
        Alert.alert(
            convertLanguage(language, 'delete_team_news'),
            convertLanguage(language, 'do_you_want_to_delete_this_news'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => { this.setState({ modalTeamNewsDetail: false, modelTeamNews: false, teamnews: {} }); this.props.onDeleteTeamNews(TeamNewsId) } },
            ],
            { cancelable: false },
        );
    }

    onEditEventNews() {
        var { EventNewsId } = this.state;
        this.setState({
            modelEventNews: false,
        })
        this.props.navigation.navigate('WriteEventNews', { Id: EventNewsId })
    }

    onDeleteEventNews() {
        var { language } = this.props.language
        var { EventNewsId } = this.state;
        setTimeout(() => {
            Alert.alert(
                convertLanguage(language, 'delete_event_news'),
                convertLanguage(language, 'are_you_sure_delete_event_news'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    { text: 'OK', onPress: () => { this.setState({ modalEventNewsDetail: false, modelEventNews: false, eventnews: {} }); this.props.onDeleteEventNews(EventNewsId) } },
                ],
                { cancelable: false },
            );
        }, 100)
    }

    onLikeTeamNews = (Id) => {
        this.props.onLikeTeamNews(Id)
    }

    onLikeEventNews = (Id) => {
        this.props.onLikeEventNews(Id)
    }

    closeModal = () => {
        this.setState({ modelTeamNews: false, TeamNewsId: '', modelEventNews: false, EventNewsId: '' })
    }

    closeModalEventNewsDetail = () => {
        this.setState({ modalEventNewsDetail: false, eventnews: {} })
    }

    closeModalTeamNewsDetail = () => {
        this.setState({ modalTeamNewsDetail: false, teamnews: {} })
    }

    render() {
        StatusBar.setBarStyle(Platform.OS === 'android' ? 'light-content' : 'dark-content', true);
        var { liked } = this.props.like;
        var { language } = this.props.language;
        var { profile } = this.props.user;
        var { isConnected } = this.state;
        return (
            <>
                <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ width: 85 }}></View>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'liked')}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Touchable style={{ minHeight: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.globalCheckLogin('News')}>
                                <Image source={profile.isNewNotification ? require('../../assets/notification_active.png') : require('../../assets/notification.png')} style={{ width: 32, height: 32 }} />
                            </Touchable>
                            <Touchable style={{ minHeight: 50, minWidth: 50, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')}>
                                <Image source={require('../../assets/search.png')} style={{ width: 32, height: 32 }} />
                            </Touchable>
                        </View>
                    </View>

                    <Line />
                    {
                        !isConnected ?
                            <NoInternet onPress={() => this.checkConnect()} />
                            :
                            profile.Id ?
                                <View style={styles.content}>
                                    <FlatList
                                        ref="listRef"
                                        contentContainerStyle={{ paddingLeft: 16, paddingRight: 16, paddingBottom: 16 }}
                                        data={liked.posts}
                                        renderItem={({ item }) => {
                                            if (item.Event) {
                                                return <ItemEventNews
                                                    cancel_text={convertLanguage(language, 'canceled_event')}
                                                    data={item}
                                                    zoomImage={(index, dataZoom) => this.setState({ zoomeImageIndex: index, modalZoom: true, dataZoom: dataZoom })}
                                                    Role={item.Event.Role}
                                                    Slug={item.Event.Slug}
                                                    Poster={item.Event.Posters ? item.Event.Posters.Medium : item.Event.Poster}
                                                    Title={item.Event.Title}
                                                    navigation={this.props.navigation}
                                                    onLikeEventNews={this.onLikeEventNews}
                                                    onDeleteEventNews={(Id) => this.props.onDeleteEventNews(Id)}
                                                    // share={() => this.share()}
                                                    onOpenModelEventNews={(Id) => this.onOpenModelEventNews(Id, item.Event.Role)}
                                                    onOpenModelEventNewsDetail={() => this.onOpenModelEventNewsDetail(item, item.Event.Role)} />
                                            } else {
                                                return <TeamNewsItem
                                                    style={{ marginTop: 20, borderBottomWidth: 2, borderBottomColor: '#e8e8e8', paddingBottom: 10 }}
                                                    data={item}
                                                    navigation={this.props.navigation}
                                                    onDeleteTeamNews={(Id) => this.props.onDeleteTeamNews(Id)}
                                                    Id={item.Team.Id}
                                                    onLikeTeamNews={this.onLikeTeamNews}
                                                    openModalAction={(Id) => this.onOpenModelTeamNews(Id, item.Team.Role)}
                                                    navigation={this.props.navigation}
                                                    zoomImage={(index, Images) => this.setState({ zoomImageIndex2: index, modalZoomImage: true, dataZoomImage: Images })}
                                                    onOpenModelTeamNewsDetail={() => this.onOpenModelTeamNewsDetail(item, item.Team.Role)} />
                                            }
                                        }}
                                        onEndReachedThreshold={0.5}
                                        keyExtractor={(item, index) => index.toString()}
                                        ListFooterComponent={this._renderFooter}
                                        ListHeaderComponent={this._renderHeader}
                                        onRefresh={this.refresh}
                                        onEndReached={this.loadMore}
                                        refreshing={false}
                                    />
                                    {
                                        this.state.modelEventNews &&
                                        <ModalEventNews
                                            closeModal={this.closeModal}
                                            Role={this.state.Role}
                                            onReportEventNews={() => this.onReportEventNews()}
                                            onEditEventNews={() => this.onEditEventNews()}
                                            onDeleteEventNews={() => this.onDeleteEventNews()}
                                        />
                                    }
                                    {
                                        this.state.modelTeamNews &&
                                        <ModalTeamNews
                                            Role={this.state.Role}
                                            closeModal={this.closeModal}
                                            onReportTeamNews={() => this.onReportTeamNews()}
                                            onEditTeamNews={() => this.onEditTeamNews()}
                                            onDeleteTeamNews={() => this.onDeleteTeamNews()}
                                        />
                                    }
                                </View>
                                :
                                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                                    <Touchable
                                        onPress={() => this.props.navigation.navigate('Login')}
                                        style={{ alignSelf: 'center', borderRadius: 4, marginTop: 20, marginBottom: 20, paddingLeft: 40, paddingRight: 40, paddingTop: 10, paddingBottom: 10, backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignItems: 'center', }}>
                                        <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>{convertLanguage(language, 'login')}</Text>
                                    </Touchable>
                                </View>
                    }
                </SafeView>
                {
                    this.state.modalEventNewsDetail &&
                    <ModalEventNewsDetail
                        closeModal={this.closeModalEventNewsDetail}
                        data={this.state.eventnews}
                        onLikeEventNews={(Id) => this.onLikeEventNews(Id)}
                        onOpenModelEventNews={(Id) => this.onOpenModelEventNews(Id, this.state.Role)}
                        navigation={this.props.navigation}
                        onDeleteEventNews={(Id) => this.props.onDeleteEventNews(Id)}
                    />
                }
                {
                    this.state.modalTeamNewsDetail &&
                    <ModalTeamNewsDetail
                        closeModal={this.closeModalTeamNewsDetail}
                        data={this.state.teamnews}
                        onLikeTeamNews={(Id) => this.props.onLikeTeamNews(Id)}
                        openModalAction={(Id) => { this.onOpenModelTeamNews(Id, this.state.Role) }}
                        navigation={this.props.navigation}
                        onDeleteTeamNews={(Id) => this.props.onDeleteTeamNews(Id)}
                    />
                }
            </>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    txtTitle: {
        color: Colors.TEXT_P,
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 20,
    },
    boxListEvent: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        flexGrow: 2,
        justifyContent: 'space-between',
        marginTop: 20
    },
    boxSeeAll: {
        height: 48,
        borderWidth: 1,
        borderColor: '#333333',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
    },
    txtSeeAll: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        like: state.like,
        user: state.user,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataLiked: (page, iso_code) => {
            dispatch(actLoadDataLiked(page, iso_code))
        },
        onLikeTeamNews: (Id) => {
            dispatch(actLikeTeamNews(Id))
        },
        onLikeEventNews: (EventNewsId) => {
            dispatch(actLikeEventNews(EventNewsId))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onDeleteEventNews: (EventNewsId) => {
            dispatch(actDeleteEventNews(EventNewsId))
        },
        onDeleteTeamNews: (TeamNewsId) => {
            dispatch(actDeleteTeamNews(TeamNewsId))
        },
        globalCheckLogin: (nextPage) => {
            dispatch(actGlobalCheckLogin(nextPage))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LikePage);
// export default EventHostList;
