import React, { Component } from 'react';
import { View, Text, Image, Platform, Dimensions, ActivityIndicator, StyleSheet, Keyboard } from 'react-native';

import Touchable from '../screens_view/Touchable'
import SafeView from '../screens_view/SafeView'
import * as Colors from '../constants/Colors'
const { width, height } = Dimensions.get('window')
import { connect } from "react-redux";
import { actCreateAboutTeam } from '../actions/team';
import { convertLanguage } from '../services/Helper'
import AndroidWebView from 'react-native-webview';
import * as Config from '../constants/Config';
import Line from '../screens_view/Line';
export class CreateIntroTeam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            Content: this.props.route.params['aboutTeam'] ? this.props.route.params['aboutTeam'] : '',
            slug: this.props.route.params['slug'],
        };
    }

    save = async () => {
        var { Content } = this.state;
        var body = {
            AboutTeam: Content,
        }
        this.props.createAboutTeam(this.state.slug, body, this.props.navigation);
    }

    onMessage(data) {
        this.setState({ Content: data.nativeEvent.data })
    }

    sendPostMessage() {
        var { Content } = this.state;
        var { language } = this.props.language;
        var data = JSON.stringify({ content: Content, placeholder: convertLanguage(language, 'register_about'), type: 'team', height: height - 160 })
        this.webView2.postMessage(data);
    }

    renderLoading() {
        return <View style={{ width, height: height - 100, position: 'absolute', justifyContent: 'center', alignItems: 'center', zIndex: 9 }}>
            <ActivityIndicator size="large" color="#000000" />
        </View>
    }

    componentDidMount() {
        this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
        this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }

    componentWillUnmount() {
        this.keyboardDidShowSub.remove();
        this.keyboardDidHideSub.remove();
    }

    keyboardDidShow = (event) => {
        var data = JSON.stringify({ height: height - 160 - event.endCoordinates.height, type: 'height', isShowKeyboard: true })
        this.webView2.postMessage(data);
    };

    keyboardDidHide = (event) => {
        var data = JSON.stringify({ height: height - 160, type: 'height', isShowKeyboard: false })
        this.webView2.postMessage(data);
    };

    render() {
        var { isFetching } = this.props.team;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ position: 'relative', zIndex: 10, justifyContent: 'space-between', flexDirection: 'row', marginRight: 20, alignItems: 'center' }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{
                            minWidth: 48, minHeight: 48, alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>

                    <Touchable
                        disabled={!this.state.Content || isFetching}
                        onPress={isFetching ? null : this.save}
                        style={{
                            paddingTop: 8, paddingBottom: 8, paddingLeft: 12, paddingRight: 12,
                            borderWidth: 1, borderColor: this.state.Content ? Colors.PRIMARY : Colors.DISABLE,
                            borderRadius: 4,
                            backgroundColor: this.state.Content ? Colors.PRIMARY : '#FFFFFF',
                        }} >
                        <Text style={{ color: this.state.Content ? '#FFFFFF' : Colors.DISABLE }}>{convertLanguage(language, 'save')}</Text>
                    </Touchable>
                </View>
                <Line />
                <View style={{ flex: 1 }}>
                    {
                        (isFetching) &&
                        <View style={{ width, height: height - 100, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size="large" color="#000000" />
                        </View>
                    }
                    <View style={[{ opacity: isFetching ? 0.2 : 1, flex: 1 }]}>
                        <AndroidWebView
                            ref={(webView2) => this.webView2 = webView2}
                            source={{ uri: Platform.OS === 'ios' ? Config.MAIN_URL + "editor-mobile" : Config.MAIN_URL + "editor-mobile-android" }}
                            onMessage={(data) => this.onMessage(data)}
                            onLoadEnd={() => {
                                this.sendPostMessage()
                            }}
                            startInLoadingState
                            renderLoading={this.renderLoading}
                            scalesPageToFit={true}
                            zoomable={false}
                            scrollEnabled={false}
                        />
                    </View>
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    ipContent: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 8,
        paddingBottom: 8,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        height: 200,
        textAlignVertical: 'top'
    },
    boxControl: {
        paddingTop: 10,
        alignItems: 'flex-end',
        justifyContent: 'center',
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    boxInsert: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#03a9f4',
        backgroundColor: '#03a9f4'
    },
    txtInsert: {
        color: '#FFFFFF',
        fontSize: 15,
        fontWeight: 'bold',
        paddingLeft: 5,
        paddingRight: 5
    },
    boxCancel: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        marginRight: 5,
    },
    txtCancel: {
        color: '#333333',
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: 5, paddingRight: 5
    },
});

const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        createAboutTeam: (slug, body, navigation) => {
            dispatch(actCreateAboutTeam(slug, body, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateIntroTeam);
