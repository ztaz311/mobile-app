import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { TextField } from 'react-native-material-textfield';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            submitted: false
        };
    }

    onSubmit = () => {
        var { username, password } = this.state;
        var body = {
            username,
            password
        }
        this.props.onSubmit(body);
    }

    onSubmitGG = () => {

    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "white",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: 15,
                }}
            >
                <TextField
                    containerStyle={{ width: '100%', marginTop: 4 }}
                    label='Username'
                    onChangeText={(username) => this.setState({ username })}
                    value={this.state.username}
                    autoCapitalize='none'
                />

                <TextField
                    containerStyle={{ width: '100%', marginTop: 4 }}
                    label='Password'
                    onChangeText={(password) => this.setState({ password })}
                    value={this.state.password}
                    secureTextEntry={true}
                    autoCapitalize='none'
                />
                <TouchableOpacity
                    style={{
                        paddingVertical: 15,
                        paddingHorizontal: 40,
                        backgroundColor: "indigo"
                    }}
                    onPress={this.onSubmit} >
                    <Text style={{ fontSize: 23, fontWeight: 'bold', color: "white" }}>
                        Register
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        paddingVertical: 15,
                        paddingHorizontal: 40,
                        backgroundColor: "indigo",
                        marginTop: 16
                    }}
                    onPress={this.onSubmitGG} >
                    <Text style={{ fontSize: 23, fontWeight: 'bold', color: "white" }}>
                        LOGIN WITH GOOGLE
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Login;