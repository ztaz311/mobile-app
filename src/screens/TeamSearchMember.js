import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native';
import SafeView from '../screens_view/SafeView'
import MemberTeamItem from '../components/member/MemberInviteItem'
import * as Colors from '../constants/Colors'

import Toast from 'react-native-simple-toast';

import { actInvitedMember } from '../actions/team';
import { actSearchMember } from '../actions/user';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
class TeamSearchMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: ''
        };
    }

    submitInvitedMember(memberId) {
        this.props.invitedMember(this.props.team.team.Id, memberId, this.props.navigation);
    }

    renderMember() {
        var members = this.props.user.searchMembers;
        return members.map((member, index) => {
            return <MemberTeamItem key={index} member={member}
                        invitedMember={() => {this.submitInvitedMember(member.Id)}}
                    />
        });
    }

    submitSearch() {
        this.props.searchMember(this.state.keyword, this.props.team.team.Id);
    }

    render() {
        let user = this.props.user;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1, marginTop: 20, marginBottom: 20 }}>
                <View style={styles.rowSearch}>
                    <TextInput
                        style={styles.inputSearch}
                        placeholder={convertLanguage(language, 'search_name_or_id')}
                        placeholderTextColor={"#bdbdbd"}
                        value={this.state.keyword}
                        onChangeText={(keyword) => this.setState({ keyword: keyword })}
                    />
                    <TouchableOpacity disabled={this.state.keyword.length === 0} style={styles.btnSearch} onPress={() => {this.submitSearch()}}>
                        <Image style={styles.imgSearch} source={require('../assets/search.png')} />
                    </TouchableOpacity>
                </View>
                <ScrollView style={styles.content} keyboardShouldPersistTaps='always' >
                    <Text style={styles.txtTotal}>{convertLanguage(language, 'found')} {user.searchMembers.length} {convertLanguage(language, 'people')}</Text>
                    {
                            user.loadingSearch ? 
                                <ActivityIndicator size="large" />
                            : null
                        }
                    {
                        this.renderMember()
                    }
                </ScrollView>
            </SafeView>
        );
    }
}
  const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    rowSearch: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: 'flex-end'
    },
    inputSearch: {
        flex: 1,
        paddingBottom: 10,
        borderBottomWidth: 1,
        fontSize: 15,
        borderColor: '#bcbcbc',
        color: '#333333'
    },
    imgSearch: {
        marginLeft: 20,
        marginBottom: 10,
        width: 32,
        height: 32
    },
    txtTotal: {
        fontSize: 15,
        fontWeight: 'bold',
        marginBottom: 15,
        color: '#333333'
    },
    content: {
        flex: 1,
        paddingTop: 25,
        paddingLeft: 20,
        paddingRight: 20
    },
  });
const mapStateToProps = state => {
    return {
        team: state.team,
        user: state.user,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        searchMember: (keyword, teamId) => {
            dispatch(actSearchMember(keyword, teamId))
        },
        invitedMember: (teamId, memberId, navigation) => {
            dispatch(actInvitedMember(teamId, memberId, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamSearchMember);
