import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity, KeyboardAvoidingView, ScrollView, Platform, ActivityIndicator } from 'react-native';

import Touchable from '../screens_view/Touchable'
import SafeView from '../screens_view/SafeView'


import * as Colors from '../constants/Colors'

import { actDeleteTeam } from '../actions/team';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
class TeamDelete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            err_passwd: '',
        };
    }

    submitDelete() {
        if (this.state.password.length >= 6 && !this.props.team.loadingDelete) {
            this.props.deleteTeam(this.props.team.team.Id, this.state.password, this.props.navigation);
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.team.errors.Password) {
            this.setState({
                err_passwd: nextProps.team.errors.Password,
            })
        }
    }

    render() {
        let team = this.props.team.team;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                </View>
                <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 0} style={{ flex: 1 }} >
                    <ScrollView style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.colThumb}>
                                <Image
                                    resizeMode='cover'
                                    style={styles.imgLogo}
                                    source={team.Logos ? { uri: team.Logos.Medium } : null} />
                            </View>
                            <View style={styles.colTeamInfo}>
                                <View style={styles.rowTeamName}>
                                    <Text style={styles.txtTeamName}>
                                        { team.Name }
                                    </Text>
                                    {
                                        team.Verify == 1 &&
                                        <Image style={styles.imgCertified} source={require('../assets/team_certified.png')} />
                                    }
                                </View>
                                <Text style={styles.txtTeamId}>ID: { team.Code }</Text>
                            </View>
                        </View>
                        <View style={styles.content}>
                            <Text style={styles.txtTitleConfirm}>{convertLanguage(language, 'sure_to_delete_this_team')}</Text>
                            <Text style={styles.txtDes}>{convertLanguage(language, 'text_after_delete_team')}</Text>
                            <Text style={styles.txtDes}>{convertLanguage(language, 'input_text_delete_team')}</Text>
                            <View style={styles.rowPassword}>
                                <Text style={styles.txtPassword}>{convertLanguage(language, 'password')}</Text>
                                <TextInput
                                    style={styles.inputPassword}
                                    placeholder=''
                                    secureTextEntry={true}
                                    value={this.state.password}
                                    onChangeText={(password) => this.setState({ password: password, err_passwd: '' })}
                                    selectionColor='#bcbcbc'
                                />
                                {
                                    this.state.err_passwd !== '' &&
                                    <Text style={styles.txtError}>{this.state.err_passwd}</Text>
                                }
                            </View>
                            <View style={styles.rowForgot}>
                                <TouchableOpacity style={styles.btnForgot}
                                    onPress={() => {
                                        this.props.navigation.navigate('ForgotPw')
                                    }}
                                >
                                    <Text style={styles.txtForgot}
                                        textDecorationLine='underline'
                                    >Forgot Password?</Text>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity style={[styles.btnConfirm, this.state.password.length < 6 ? {} : styles.btnReportActive ]}
                                onPress={() => { this.submitDelete() }}>
                                <Text style={[styles.txtConfirm, this.state.password.length < 6 ? {} : styles.txtBtnReportActive]}>{convertLanguage(language, 'confirm')}</Text>
                                {
                                    this.props.team.loadingDelete ? 
                                        <ActivityIndicator size="small" />
                                    : null
                                }
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeView>
        );
    }
}
  const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1
    },
    content: {
        flex: 1,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 25
    },
    header: {
        flexDirection: 'row',
        paddingBottom: 20
    },
    colThumb: {
        flex: 0.3,
        justifyContent: 'flex-start'
    },
    colTeamInfo: {
        flex: 0.7,
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10

    },
    imgLogo: {
        // alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 40
    },
    rowTeamName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtTeamName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtTeamId: {
        fontSize: 18,
        color: '#333333',
        fontWeight: '400'
    },
    imgCertified: {
        maxWidth: 30,
        marginLeft: 10
    },
    txtTitleConfirm: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5,
        color: '#333333'
    },
    txtDes: {
        fontSize: 16,
        color: '#333333'
    },
    rowPassword: {
        paddingTop: 15
    },
    inputPassword: {
        borderBottomWidth: 1,
        borderBottomColor: '#bcbcbc',
        paddingBottom: 10,
        paddingTop: 10
    },
    errorPassword: {
        color: Colors.RED,
        marginTop: 10
    },
    // rowConfirm: {
    //     alignItems: 'center',
    //     borderWidth: 1,
    //     borderColor: '#bcbcbc',
    //     borderRadius: 5
    // },
    btnConfirm: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#bcbcbc',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
    },
    txtConfirm: {
        color: '#bcbcbc',
        fontWeight: 'bold'
    },
    rowForgot: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginBottom: 30
    },
    btnForgot: {
        borderBottomWidth: 1,
        color: '#03a9f4',
        borderBottomColor: '#03a9f4'
    },
    txtForgot: {
        color: '#03a9f4',
    },
    btnReportActive : {
        borderColor: Colors.PRIMARY,
        backgroundColor: Colors.PRIMARY,
    },
    txtBtnReportActive: {
        color: '#FFFFFF'
    },
    txtPassword: {
        color: '#333333',
        fontWeight: 'bold',
        fontSize: 18,
    },
    txtError: {
        color: '#ff4081',
        fontSize: 12,
        marginTop: 2
    }
  });
const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        deleteTeam: (id, password, navigation) => {
            dispatch(actDeleteTeam(id, password, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamDelete);
