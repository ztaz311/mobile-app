import React, { Component } from 'react';
import {
    Dimensions, Platform,
    View, ScrollView, RefreshControl, Image,
    Text, StyleSheet,
    Alert,
    Linking,
    StatusBar,
    Vibration
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Dots from 'react-native-dots-pagination';
import Touchable from '../screens_view/Touchable'
import Line from '../screens_view/Line'
import ItemTeam from '../components/team/ItemTeam'
import AsyncStorage from '@react-native-community/async-storage';
import * as Colors from '../constants/Colors'
import ModalLogin from '../components/global/ModalLogin'
import SafeView from '../screens_view/SafeView'
import { actHomeTeamLists, actTeamFollow } from '../actions/team';
import { actEventLists, actLikeEvent } from '../actions/event';
import { actVenueLists } from '../actions/venue';
import { actBannerLists } from '../actions/banner';
import { actGlobalCheckLogin, actLoadDataConfig, setChangeCity, actUseLinking } from '../actions/global';
import { actSelectCountryByEvent } from '../actions/city';
import { actUpdateLanguage } from '../actions/language';
import { actUpdateDeviceToken, actUpdateNewNotification, setCheckTeamKR, hideModalSettingTeam } from '../actions/user';
import ModalCity from '../components/ModalCity';
import ItemEventHalf from '../components/event/ItemEventHalf';
import ItemVenueHalf from '../components/venue/ItemVenueHalf';
import ModalRegisterVenue from '../components/home/ModalRegisterVenue';
import ModalRegisterSuccess from '../components/home/ModalRegisterSuccess';
import ModalRegisterTeam from '../components/home/ModalRegisterTeam';
import { convertLanguage } from '../services/Helper';
import LottieView from 'lottie-react-native';
import firebase from 'react-native-firebase';
const ConstantSystem = require('../services/ConstantSystem');
const { width } = Dimensions.get('window');
import { connect } from "react-redux";
import FastImage from 'react-native-fast-image';
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            loading: false,
            position: 'se',
            // isLoadTeamList: false,
            // city: this.props.global.city.Id ? this.props.global.city : this.props.global.country,
            modalCity: false,
            checkUpdateApp: true,
            modalRegisterVenue: false,
            modalRegisterSuccess: false,
            modalRegisterTeam: false,
            activeSlide: 0,
            m_city_id: ''
        };
    }

    componentDidMount() {
        var { city, country, isUseLinking } = this.props.global;
        var { profile } = this.props.user;
        this.props.navigation.setParams({
            scrollToTop: this._scrollToTop,
        });
        this.props.getBanners(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
        this.props.getEvents(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
        this.props.getVenues(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
        this.props.getTeams(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
        var { event_countries } = this.props.city;
        if (event_countries.length === 0) {
            this.props.onLoadDataCountry();
        }
        this.checkPermission();
        this.createNotificationListeners();
        if (!this.props.global.config.version_ios) {
            this.props.onLoadDataConfig();
        }
        {
            profile.Id &&
                this.props.onCheckTeamKR();
        }
        // if (Platform.OS === 'android') {
        //     Linking.getInitialURL().then(url => {
        //         this.navigate(url);
        //     });
        // } else {
        //     Linking.addEventListener('url', this.handleOpenURL);
        // }
        Linking.getInitialURL()
            .then(url => {
                if (isUseLinking) {
                    this.props.onUseLinking()
                    this._handleDeeplink(url)
                }
            })
            .catch(err => {
                console.warn('Deeplinking error')
            })

        // Linking.addListener('url', e => {
        //     this._handleDeeplink(e.url)
        // })
    }

    _scrollToTop = () => {
        // Scroll to top, in this case I am using FlatList
        if (!!this.scrollView) {
            this.scrollView.scrollTo({ x: 0, y: 0, animated: true });
        }
    }

    _handleDeeplink = (url) => {
        Linking.openURL(url)
    }

    componentWillUnmount() {
        this.removeNotificationDisplayedListener();
        this.removeNotificationListener();
        this.removeNotificationOpenedListener();
        // Linking.removeEventListener('url', this.handleOpenURL);
    }

    // handleOpenURL = (event) => { // D
    //     Linking.openURL(event.url)
    // }

    onRefresh = () => {
        var { city, country } = this.props.global;
        this.props.getBanners(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
        this.props.getEvents(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
        this.props.getVenues(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
        this.props.getTeams(city.Id ? city.Id : country.Code, city.Id ? 'city' : 'country');
    }

    itemBanner = ({ item, index }) => {
        return <Touchable onPress={() => Linking.openURL(item.LinkApp)} disabled={item.LinkApp === '#'}><FastImage style={{ backgroundColor: '#bcbcbc', aspectRatio: 2.4, }} source={{ uri: item.Images ? item.Images.Medium : item.Image }} /></Touchable>
    }

    followTeam = (teamId) => {
        this.props.followTeam(teamId)
    }
    renderTeamList() {
        var { city } = this.state;
        let teams = this.props.team.home_teams;
        var { language } = this.props.language;
        if (!teams) {
            return null;
        }
        return teams.map((team, index, arr) => {
            var disabled = this.props.team.teamIdAction == team.Id ? this.props.team.loadingFollowers : false;
            if (index < 11) {
                return <ItemTeam
                    key={'team_' + team.Id}
                    data={team}
                    followTeam={this.followTeam}
                    disabled={disabled}
                    navigation={this.props.navigation}
                />
            } else if (index == 11) {
                return (
                    <>
                        <ItemTeam
                            key={'team_' + team.Id}
                            data={team}
                            followTeam={this.followTeam}
                            disabled={disabled}
                            navigation={this.props.navigation}
                        />
                        <Touchable
                            onPress={() => this.props.navigation.navigate('TeamList', { city })}
                            key={'btn_more_team'}
                            style={{
                                flex: 1,
                                margin: 16, minHeight: 46,
                                marginTop: 24,
                                marginBottom: 32,
                                borderWidth: 1, borderColor: '#4F4F4F', borderRadius: 4,
                                justifyContent: 'center', alignItems: 'center'
                            }}>
                            <Text style={{ fontSize: 16, color: '#4F4F4F' }}>{convertLanguage(language, 'see_more')}</Text>
                        </Touchable>
                    </>
                )

            } else {
                return null
            }
        })
    }

    pressEvent = () => {
        this.props.navigation.navigate('RegisterOpt')
    }

    onSelectCity(city, country) {
        this.setState({ modalCity: false })
        AsyncStorage.setItem('city', JSON.stringify(city));
        AsyncStorage.setItem('country', JSON.stringify(country));
        this.props.onChangeCity(city, country)
        this.props.getBanners(city.Id, 'city');
        this.props.getEvents(city.Id, 'city');
        this.props.getVenues(city.Id, 'city');
        this.props.getTeams(city.Id, 'city');
    }

    async getToken() {
        var { profile } = this.props.user;
        var fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            AsyncStorage.setItem('fcmToken', fcmToken);
            if (profile.Id) {
                if (profile.NotiPushKeys.indexOf(fcmToken) == -1) {
                    var user_id = profile.Id ? profile.Id : '';
                    var body = {
                        NotiPushKey: fcmToken,
                        UserId: user_id,
                        AppType: Platform.OS
                    }
                    this.props.onUpdateDeviceToken(body);
                }
            } else {
                var body = {
                    NotiPushKey: fcmToken,
                    UserId: '',
                    AppType: Platform.OS
                }
                this.props.onUpdateDeviceToken(body);
            }
        }
    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
        const channel = new firebase.notifications.Android.Channel('default', 'Notification', firebase.notifications.Android.Importance.Max).setDescription('You will receive notification');
        firebase.notifications().android.createChannel(channel);
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            this.getToken();
        } catch (error) {
            console.log('permission rejected');
        }
    }

    async createNotificationListeners() {
        // firebase.notifications().onNotification(notification => {
        //   alert(JSON.stringify(notification.data))
        //   console.log(notification)
        //   // firebase.notifications().displayNotification(notification)
        // });
        this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
            // this.saveNotification(notification, 'displayed')
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        });
        this.removeNotificationListener = firebase.notifications().onNotification((notification) => {
            this.saveNotification(notification, 'foreground')
            // Process your notification as required
        });
        this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            this.saveNotification(notificationOpen.notification, '')
            // Get the action triggered by the notification being opened
            // const action = notificationOpen.action;
            // Get information about the notification that was opened
            // const notification: Notification = notificationOpen.notification;
        });
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            this.saveNotification(notificationOpen.notification, '')
            // console.log(4)
            //   // App was opened by a notification
            //   // Get the action triggered by the notification being opened
            //   const action = notificationOpen.action;
            //   // Get information about the notification that was opened
            //   const notification: Notification = notificationOpen.notification;
        }
        firebase.messaging().subscribeToTopic(Platform.OS == 'android' ? 'comeupAndroid' : 'comeupIos');
    }

    saveNotification(notification, type) {
        // const badgeCount = await firebase.notifications().getBadge();
        try {
            let mes = notification.data
            if (!mes) return
            if (type !== 'foreground') {
                if (mes.Url) {
                    var url = 'comeup:/' + mes.Url;
                    Linking.openURL(url)
                }
            } else {
                if (mes.Vibration == 1) {
                    Vibration.vibrate(1000);
                }
                if (mes.StatusBar == 1) {
                    const viewNotification = new firebase.notifications.Notification()
                    if (Platform.OS === 'android') {
                        viewNotification.android.setSmallIcon("ic_stat_ic_notification")
                        viewNotification.android.setColor("#00A9F4")
                        viewNotification.android.setAutoCancel(true)
                        viewNotification.android.setChannelId(notification.data.channelId || 'default');
                        viewNotification.android.setVibrate([0])
                    } else {
                        viewNotification.ios.setBadge(0)
                    }
                    viewNotification.setNotificationId(notification.notificationId)
                    viewNotification.setTitle(notification.title)
                    viewNotification.setBody(notification.body)
                    viewNotification.setData(notification.data)
                    firebase.notifications().displayNotification(viewNotification)
                        .then(e => {
                            console.log("then", e)
                        })
                        .catch(e => {
                            console.log("catch", e)
                        })
                }
                this.props.onUpdateNewNotification()
            }

        } catch (err) {
            console.log(err)
        }
    }

    showAlert(force_update) {
        var { language } = this.props.language;
        var data_alert = [
            {
                text: convertLanguage(language, 'update'), onPress: () => {
                    force_update && this.showAlert(force_update)
                    Linking.openURL(Platform.OS === 'ios' ? 'https://apps.apple.com/app/comeup/id973360108' : 'https://play.google.com/store/apps/details?id=com.starindex.comeup')
                },
                style: 'cancel'
            }
        ]
        !force_update && data_alert.push({ text: convertLanguage(language, 'no') })
        Alert.alert(
            convertLanguage(language, 'notification'),
            force_update ? convertLanguage(language, 'noti_update_force') : convertLanguage(language, 'noti_update'),
            data_alert
        );
    }

    versionCompare(v1, v2) {
        //  vnum stores each numeric part of version 
        var vnum1 = 0, vnum2 = 0;

        //  loop untill both string are processed 
        for (var i = 0, j = 0; (i < v1.length || j < v2.length);) {
            //  storing numeric part of version 1 in vnum1 
            while (i < v1.length && v1[i] != '.') {
                vnum1 = vnum1 * 10 + (v1[i] - '0');
                i++;
            }

            //  storing numeric part of version 2 in vnum2 
            while (j < v2.length && v2[j] != '.') {
                vnum2 = vnum2 * 10 + (v2[j] - '0');
                j++;
            }

            if (vnum1 > vnum2)
                return 1;
            if (vnum2 > vnum1)
                return -1;

            //  if equal, reset variables and go for next numeric 
            // part 
            vnum1 = vnum2 = 0;
            i++;
            j++;
        }
        return 0;
    }

    componentWillReceiveProps(nextProps) {
        // const isFocused = this.props.navigation.isFocused();
        // console.log(isFocused)
        if (this.state.checkUpdateApp) {
            if (Platform.OS === 'ios') {
                if (nextProps.global.config.version_ios && this.versionCompare(nextProps.global.config.version_ios, ConstantSystem.VERSION) > 0) {
                    this.setState({
                        checkUpdateApp: false
                    })
                    this.showAlert(nextProps.global.config.force_update)
                }
            } else {
                if (nextProps.global.config.version_android && this.versionCompare(nextProps.global.config.version_android, ConstantSystem.VERSION) > 0) {
                    this.setState({
                        checkUpdateApp: false
                    })
                    this.showAlert(nextProps.global.config.force_update)
                }
            }

        }
    }

    render() {
        var { city, country } = this.props.global;
        StatusBar.setBarStyle(Platform.OS === 'android' ? 'light-content' : 'dark-content', true);
        var { modalRegisterVenue, activeSlide, modalRegisterSuccess } = this.state;
        var { language } = this.props.language;
        var { profile, modalRegisterTeam, team } = this.props.user;
        var count = this.props.venue.venues.length > 0 ? 4 : 10;
        var hide = true
        return (
            <>
                {/* <SafeAreaView style={{ flex: 0, backgroundColor: 'red' }} /> */}
                <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Touchable style={styles.boxItemFilter} onPress={() => this.setState({ modalCity: true })}>
                            <Text style={styles.txtFilter}>{city.Id ? city['Name_' + language] : country['Name_' + language]}</Text>
                            <Image source={require('../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                        </Touchable>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Touchable style={{ minHeight: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.globalCheckLogin('News')}>
                                <Image source={profile.isNewNotification ? require('../assets/notification_active.png') : require('../assets/notification.png')} style={{ width: 32, height: 32 }} />
                            </Touchable>
                            <Touchable style={{ minHeight: 50, minWidth: 50, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')}>
                                <Image source={require('../assets/search.png')} style={{ width: 32, height: 32 }} />
                            </Touchable>
                        </View>
                    </View>

                    <Line />

                    <ScrollView
                        ref={(c) => { this.scrollView = c }}
                        style={{ flex: 1 }}
                        // onScrollBeginDrag={() => { this.getTeams() }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                            />
                        }>
                        <View>
                            {
                                this.props.banner.loading ?
                                    <LottieView source={require('../assets/banner_loading.json')} autoPlay loop style={{ width: '100%' }} resizeMode='cover' />
                                    :
                                    <View>
                                        <Carousel
                                            data={this.props.banner.banners}
                                            renderItem={this.itemBanner}
                                            sliderWidth={width}
                                            itemWidth={width}
                                            inactiveSlideScale={1}
                                            onSnapToItem={(index) => this.setState({ activeSlide: index })}
                                            autoplay={true}
                                            autoplayInterval={3000}
                                            loop
                                        />
                                        {
                                            this.props.banner.banners.length > 1 &&
                                            <View style={{ position: 'absolute', bottom: 0, alignSelf: 'center' }}>
                                                <Dots length={this.props.banner.banners.length} active={activeSlide} activeColor={'#FFFFFF'} width={100} passiveDotWidth={6} passiveDotHeight={6} activeDotWidth={9} activeDotHeight={9} paddingVertical={6} passiveColor={'rgba(255, 255, 255, 0.5)'} marginHorizontal={3} />
                                            </View>
                                        }
                                    </View>
                            }

                            <View style={{ flexDirection: 'row', margin: 16, marginTop: 25, flex: 1, justifyContent: 'space-between' }}>
                                <Touchable
                                    onPress={() => this.props.navigation.navigate('EventList', { refresh: this.onRefresh, city })}
                                    style={{
                                        paddingTop: 10, paddingBottom: 10,
                                        flex: 0.31,
                                        alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 8,
                                        shadowOffset: { width: 0, height: 0 },
                                        shadowColor: 'black',
                                        shadowRadius: 4,
                                        shadowOpacity: 0.2,
                                        backgroundColor: '#FFFFFF',
                                        elevation: 5,
                                    }}>
                                    <Image source={require('../assets/event_home.png')} style={styles.icon_tab} />
                                    <Text style={{ color: Colors.PRIMARY, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'event')}</Text>
                                </Touchable>
                                <Touchable
                                    onPress={() => this.setState({ modalRegisterVenue: true })}
                                    style={{
                                        paddingTop: 10, paddingBottom: 10,
                                        flex: 0.31,
                                        alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 8,
                                        shadowOffset: { width: 0, height: 0 },
                                        shadowColor: 'black',
                                        shadowRadius: 4,
                                        shadowOpacity: 0.2,
                                        backgroundColor: '#FFFFFF',
                                        elevation: 5,
                                    }}>
                                    <Image source={require('../assets/venue_home.png')} style={styles.icon_tab} />
                                    <Text style={{ color: Colors.PRIMARY, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'venue')}</Text>
                                </Touchable>
                                <Touchable
                                    onPress={() => this.props.navigation.navigate('TeamList', { city })}
                                    style={{
                                        paddingTop: 10, paddingBottom: 10,
                                        flex: 0.31,
                                        alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 8,
                                        shadowOffset: { width: 0, height: 0 },
                                        shadowColor: 'black',
                                        shadowRadius: 4,
                                        shadowOpacity: 0.2,
                                        backgroundColor: '#FFFFFF',
                                        elevation: 5,
                                    }}>
                                    <Image source={require('../assets/team_home.png')} style={styles.icon_tab} />
                                    <Text style={{ color: Colors.PRIMARY, fontSize: 16, fontWeight: 'bold' }}>{convertLanguage(language, 'team')}</Text>
                                </Touchable>
                            </View>

                            {/* event */}
                            <View style={{ margin: 16, marginTop: 15, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ color: '#333333', fontSize: 24, fontWeight: 'bold' }}>{convertLanguage(language, 'event')}</Text>
                                <Touchable
                                    onPress={() => this.props.globalCheckLogin('EventHostList')}
                                    style={styles.boxAdd}>
                                    <Image source={require('../assets/calenda_add.png')} style={styles.calenda_add} />
                                    <Text style={styles.txtAdd}>{convertLanguage(language, 'create_event')}</Text>
                                </Touchable>
                            </View>

                            {
                                this.props.event.loadingHome ?
                                    <View style={styles.boxListEvent}>
                                        <View style={{ width: '48%' }}>
                                            <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                        </View>
                                        <View style={{ width: '48%' }}>
                                            <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                        </View>
                                        <View style={{ width: '48%' }}>
                                            <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                        </View>
                                        <View style={{ width: '48%' }}>
                                            <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                        </View>
                                    </View>
                                    :
                                    <View style={styles.boxListEvent}>
                                        {
                                            this.props.event.events.map((item, index) => {
                                                return (index < count) && <ItemEventHalf language={language} cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} key={index} onLikeEvent={(id) => this.props.onLikeEvent(id)} />
                                            })
                                        }
                                    </View>
                            }
                            <Touchable style={{ margin: 16, minHeight: 46, borderWidth: 1, borderColor: '#4F4F4F', borderRadius: 4, flex: 1, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('EventList', { city })}>
                                <Text style={{ fontSize: 16, color: '#4F4F4F' }}>{convertLanguage(language, 'see_more')}</Text>
                            </Touchable>
                            {
                                (this.props.venue.loading || this.props.venue.venues.length > 0) && !hide &&
                                <>
                                    <View style={{ margin: 16, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ color: '#333333', fontSize: 24, fontWeight: 'bold' }}>{convertLanguage(language, 'venue')}</Text>
                                        <Touchable
                                            onPress={() => null}
                                            style={styles.boxAdd}>
                                            <Image source={require('../assets/calenda_add.png')} style={styles.calenda_add} />
                                            <Text style={styles.txtAdd}>{convertLanguage(language, 'create_venue')}</Text>
                                        </Touchable>
                                    </View>

                                    {
                                        this.props.venue.loading ?
                                            <View style={styles.boxListEvent}>
                                                <View style={{ width: '48%' }}>
                                                    <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                                </View>
                                                <View style={{ width: '48%' }}>
                                                    <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                                </View>
                                                <View style={{ width: '48%' }}>
                                                    <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                                </View>
                                                <View style={{ width: '48%' }}>
                                                    <LottieView source={require('../assets/event-loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 16, aspectRatio: 1 }} resizeMode='cover' />
                                                </View>
                                            </View>
                                            :
                                            <View style={styles.boxListEvent}>
                                                {
                                                    this.props.venue.venues.map((item, index) => {
                                                        return index <= 3 && <ItemVenueHalf data={item} navigation={this.props.navigation} key={index} />
                                                    })
                                                }
                                            </View>
                                    }

                                    <Touchable style={{ margin: 16, minHeight: 46, borderWidth: 1, borderColor: '#333333', borderRadius: 4, flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#333333' }}>{convertLanguage(language, 'more_venue')}</Text>
                                    </Touchable>
                                </>
                            }
                            <View style={{ margin: 16, marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ color: '#333333', fontSize: 24, fontWeight: 'bold' }}>{convertLanguage(language, 'team')}</Text>
                                <Touchable
                                    onPress={() => this.props.globalCheckLogin('RegisterTeam')}
                                    style={styles.boxAdd}>
                                    <Image source={require('../assets/team_add.png')} style={styles.calenda_add} />
                                    <Text style={styles.txtAdd}>{convertLanguage(language, 'create_team')}</Text>
                                </Touchable>
                            </View>
                            {
                                this.props.team.loadingHome ?
                                    <View style={{ marginHorizontal: 16 }}>
                                        <LottieView source={require('../assets/team_loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 20 }} resizeMode='cover' />
                                        <LottieView source={require('../assets/team_loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 20 }} resizeMode='cover' />
                                        <LottieView source={require('../assets/team_loading.json')} autoPlay loop style={{ width: '100%', marginBottom: 20 }} resizeMode='cover' />
                                    </View>
                                    :
                                    this.renderTeamList()
                            }
                            <View style={{ backgroundColor: 'rgba(0,169,244,0.25)', height: 1, marginHorizontal: 16, alignSelf: 'stretch' }} />
                            <Text style={{ color: Colors.TEXT_P, fontSize: 17, fontWeight: 'bold', alignSelf: 'center', marginTop: 16 }}>{convertLanguage(language, 'let_register')}</Text>
                            <Touchable
                                onPress={() => this.props.globalCheckLogin('RegisterOpt')}
                                style={{ alignSelf: 'center', borderRadius: 4, marginTop: 16, marginBottom: 16, width: '56%', paddingTop: 10, paddingBottom: 10, backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>+ {convertLanguage(language, 'register')}</Text>
                                <Text style={{ color: 'white', fontSize: 14, fontWeight: 'bold', textAlign: 'center' }}>{convertLanguage(language, 'event') + ' / ' + convertLanguage(language, 'venue') + ' / ' + convertLanguage(language, 'team')}</Text>
                            </Touchable>
                            <View style={styles.boxDetail}>
                                <Text style={styles.txtDetail}>{'STARINDEX Inc. | CEO : Wook Kim | EIN : 105-87-75277 | \n' +
                                    '502 20 Jandari-ro 2-gil Mapo-gu Seoul | \n' +
                                    'Email : comeup@starindex.com | Tel. : +82.2.6243.0771 | \n' +
                                    'Mail-order Sales Registration No : Mapo-1355 \n\n'}</Text>
                                <View style={{ paddingHorizontal: 40 }}>
                                    <Text style={styles.txtDetail}>{convertLanguage(language, 'about_comeup_1')}</Text>
                                    <Text style={styles.txtDetail}>{convertLanguage(language, 'about_comeup_2')}</Text>
                                    <Text style={[styles.txtDetail, { marginTop: 15 }]}>{convertLanguage(language, 'copyright_index')}</Text>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                    {
                        modalRegisterVenue
                        &&
                        <ModalRegisterVenue
                            modalVisible={this.state.modalRegisterVenue}
                            closeModal={() => this.setState({ modalRegisterVenue: false })}
                            showModalRegisterSuccess={() => this.setState({ modalRegisterVenue: false, modalRegisterSuccess: true })}
                        />
                    }
                    {
                        modalRegisterSuccess
                        &&
                        <ModalRegisterSuccess
                            modalVisible={this.state.modalRegisterSuccess}
                            closeModal={() => this.setState({ modalRegisterSuccess: false })}
                        />
                    }
                    {
                        modalRegisterTeam
                        &&
                        <ModalRegisterTeam
                            modalVisible={modalRegisterTeam}
                            closeModal={() => this.props.onHideModalSettingTeam()}
                            onGoToSetting={() => { this.props.onHideModalSettingTeam(); this.props.navigation.navigate('RegisterTeam', { data: team, from: 'home' }) }}
                        />
                    }
                    <ModalLogin navigation={this.props.navigation} />
                    {
                        this.state.modalCity && this.props.city.event_countries.length > 0
                            ?
                            <ModalCity
                                modalVisible={this.state.modalCity}
                                closeModal={() => this.setState({ modalCity: false })}
                                countries={this.props.city.event_countries}
                                selectCity={(city, country) => this.onSelectCity(city, country)}
                            />
                            : null
                    }
                </SafeView>
            </>
        );
    }
}

const styles = StyleSheet.create({
    icon_tab: {
        width: 48,
        height: 48,
        marginBottom: 5
    },
    loading: {
        padding: 16
    },
    boxItemFilter: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginLeft: 16,
        paddingHorizontal: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#4F4F4F',
        paddingBottom: 3
    },
    txtFilter: {
        fontSize: 14,
        color: '#4F4F4F',
        fontWeight: 'bold',
        marginRight: 10
    },
    ic_dropdown: {
        width: 16,
        height: 16
    },
    boxListEvent: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        flexGrow: 2,
        justifyContent: 'space-between',
        marginLeft: 16,
        marginRight: 16
    },
    boxDetail: {
        fontSize: 12, color: '#bdbdbd', textAlign: 'center', margin: 16, marginBottom: 36, marginTop: 30
    },
    txtDetail: {
        fontSize: 10,
        color: '#bdbdbd',
        textAlign: 'center'
    },
    boxAdd: {
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    calenda_add: {
        width: 20,
        height: 20,
        marginRight: 4
    },
    txtAdd: {
        fontSize: 16,
        color: '#00A9F4'
    },
});

const mapStateToProps = state => {
    return {
        team: state.team,
        event: state.event,
        venue: state.venue,
        banner: state.banner,
        city: state.city,
        language: state.language,
        user: state.user,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getTeams: (id, type) => {
            dispatch(actHomeTeamLists(id, type))
        },
        followTeam: (teamId) => {
            dispatch(actTeamFollow(teamId))
        },
        getEvents: (id, type) => {
            dispatch(actEventLists(id, type))
        },
        getVenues: (id, type) => {
            dispatch(actVenueLists(id, type))
        },
        getBanners: (id, type) => {
            dispatch(actBannerLists(id, type))
        },
        globalCheckLogin: (nextPage) => {
            dispatch(actGlobalCheckLogin(nextPage))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onLoadDataCountry: () => {
            dispatch(actSelectCountryByEvent())
        },
        onUpdateLanguage: (language) => {
            dispatch(actUpdateLanguage(language))
        },
        onUpdateDeviceToken: (body) => {
            dispatch(actUpdateDeviceToken(body))
        },
        onLoadDataConfig: () => {
            dispatch(actLoadDataConfig())
        },
        onUpdateNewNotification: () => {
            dispatch(actUpdateNewNotification())
        },
        onChangeCity: (city, country) => {
            dispatch(setChangeCity(city, country))
        },
        onCheckTeamKR: () => {
            dispatch(setCheckTeamKR())
        },
        onHideModalSettingTeam: () => {
            dispatch(hideModalSettingTeam())
        },
        onUseLinking: () => {
            dispatch(actUseLinking())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);