import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, Alert } from 'react-native';
// import { TextField } from 'react-native-material-textfield';
// import Toast from 'react-native-simple-toast';
import SafeView from '../screens_view/SafeView'
import ScrView from '../screens_view/ScrView'
import Touchable from '../screens_view/Touchable'
import { connect } from "react-redux";
import * as Colors from '../constants/Colors'
import { convertLanguage } from '../services/Helper'
import * as Config from '../constants/Config';
class ForgotPw extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            //
            id: '',
            err_id: ''
        };
    }

    should = () => {
        return (!this.state.loading && this.state.id)
    }

    go = () => {
        var { language } = this.props.language;
        this.setState({ loading: true, err_id: '' })

        let formdata = new FormData();
        formdata.append("IdOrEmail", this.state.id)

        fetch(Config.API_URL+'/users/forgot-password', {
            method: 'post',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: formdata
        }).then(async (response) => {
            if (response.status == 200) {
                response = await response.json()
                // if (response.status == 200) {
                    this.props.navigation.goBack()
                    this.setState({ loading: false })
                    Alert.alert(
                        convertLanguage(language, 'confirmation'),
                        convertLanguage(language, 'confirmation_forgot_password'),
                        [
                            {
                                text: 'OK',
                                style: 'cancel',
                            },
                        ],
                        { cancelable: false },
                    );
                // } else {
                //     if (response.errors) {
                //         // this.setState(state => {
                //         //     state.err_id = response.errors.IdOrEmail
                //         // })
                //         Toast.showWithGravity(response.errors.IdOrEmail, Toast.SHORT, Toast.TOP)
                //     } else {
                //         Toast.showWithGravity('errors', Toast.SHORT, Toast.TOP)
                //     }
                //     this.setState({ loading: false })
                // }
            } else {
                alert('Cannot connect to server.')
                this.setState({ loading: false })
            }
        }).catch(err => {
            console.log(err)
            this.setState({ loading: false })
        })
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <View style={{ width: 50 }} />
                    <Touchable
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <ScrView>
                    <Text style={{
                        marginLeft: 20, marginTop: 20,
                        fontWeight: 'bold', fontSize: 30, color: Colors.TEXT_P
                    }}>{convertLanguage(language, 'no_worry')}</Text>

                    <Text style={{
                        marginLeft: 20, marginTop: 20,
                        fontSize: 16, color: Colors.TEXT_P
                    }}>{convertLanguage(language, 'just_input_your_comup')}</Text>
                    <View style={styles.boxInput}>
                        <Text style={styles.txtInput}>{convertLanguage(language, 'id_or_email')}</Text>
                        <TextInput
                            style={styles.ipContent}
                            selectionColor="#bcbcbc"
                            value={this.state.id}
                            onChangeText={(id) => this.setState({ id })}
                            keyboardType={'email-address'}
                            autoCapitalize='none'
                        />
                    </View>


                    <Touchable
                        disabled={!this.should()}
                        onPress={this.go}
                        style={{
                            minHeight: 56, borderColor: this.should() ? Colors.PRIMARY : Colors.DISABLE, borderWidth: 1, borderRadius: 4,
                            marginLeft: 20, marginRight: 20, marginTop: 32, justifyContent: 'center', alignItems: 'center',
                            backgroundColor: this.should() ? Colors.PRIMARY : '#FFFFFF'
                        }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', color: this.should() ? '#FFFFFF' : Colors.DISABLE }}>{this.state.loading ? convertLanguage(language, 'waiting') : convertLanguage(language, 'send')}</Text>
                    </Touchable>
                </ScrView>

            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    boxInput: {
        paddingBottom: 24,
        margin: 20,
        marginTop: 10
    },
    txtInput: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    ipContent: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 8,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
})

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ForgotPw);