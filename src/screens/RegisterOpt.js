import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';

import SafeView from '../screens_view/SafeView'
import Touchable from '../screens_view/Touchable'
import * as Colors from '../constants/Colors'
import { convertLanguage } from '../services/Helper'
import ModalRegisterVenue from '../components/home/ModalRegisterVenue';
import ModalRegisterSuccess from '../components/home/ModalRegisterSuccess';
import { connect } from "react-redux";
class RegisterOpt extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalRegisterVenue: false,
            modalRegisterSuccess: false,
        };
    }

    register = () => {
        this.props.navigation.navigate('RegisterTeam')
    }

    render() {
        var { language } = this.props.language;
        var { modalRegisterVenue, modalRegisterSuccess } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1, alignItems: 'center' }}>
                <Touchable
                    onPress={() => this.props.navigation.goBack()}
                    style={{
                        minHeight: 40, minWidth: 40, marginTop: 20, marginRight: 20, alignSelf: 'flex-end'
                    }}>
                    <Image source={require('../assets/X_icon.png')} style={{ alignSelf: 'flex-end' }} />
                </Touchable>

                <Text style={{ marginTop: 24, fontSize: 20, color: Colors.TEXT_P, fontWeight: 'bold' }}>{convertLanguage(language, 'let_register')}</Text>
                <Text style={{ marginTop: 16, fontSize: 17, color: Colors.TEXT_P, textAlign: 'center' }}>{convertLanguage(language, 'free_create')}</Text>

                <Touchable
                    style={{
                        alignSelf: 'stretch',
                        minHeight: 56, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                        marginLeft: 20, marginRight: 20, marginTop: 30, justifyContent: 'center', alignItems: 'center'
                    }}
                    onPress={() => this.props.navigation.navigate('EventHostList')}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={require('../assets/event_home.png')} style={{ width: 48, height: 48 }} />
                        <Text style={{ marginLeft: 20, fontSize: 20, fontWeight: 'bold', color: Colors.PRIMARY }}>{convertLanguage(language, 'register_event')}</Text>
                    </View>
                </Touchable>

                <Touchable
                    onPress={() => this.setState({ modalRegisterVenue: true })}
                    style={{
                        alignSelf: 'stretch',
                        minHeight: 56, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                        marginLeft: 20, marginRight: 20, marginTop: 20, justifyContent: 'center', alignItems: 'center'
                    }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={require('../assets/venue_home.png')} style={{ width: 48, height: 48 }} />
                        <Text style={{ marginLeft: 20, fontSize: 20, fontWeight: 'bold', color: Colors.PRIMARY }}>{convertLanguage(language, 'register_venue')}</Text>
                    </View>
                </Touchable>


                <Touchable
                    onPress={this.register}
                    style={{
                        alignSelf: 'stretch',
                        minHeight: 56, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                        marginLeft: 20, marginRight: 20, marginTop: 20, justifyContent: 'center', alignItems: 'center'
                    }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={require('../assets/team_home.png')} style={{ width: 48, height: 48 }} />
                        <Text style={{ marginLeft: 20, fontSize: 20, fontWeight: 'bold', color: Colors.PRIMARY }}>{convertLanguage(language, 'register_team')}</Text>
                    </View>
                </Touchable>


                <Touchable
                    style={{ alignSelf: 'center', position: 'absolute', bottom: 30 }}>
                    <Text
                        style={{ fontSize: 18, textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', color: Colors.PRIMARY }}>{convertLanguage(language, 'guide')}</Text>
                </Touchable>
                {
                    modalRegisterVenue
                    &&
                    <ModalRegisterVenue
                        modalVisible={this.state.modalRegisterVenue}
                        closeModal={() => this.setState({ modalRegisterVenue: false })}
                        showModalRegisterSuccess={() => this.setState({ modalRegisterVenue: false, modalRegisterSuccess: true })}
                    />
                }
                {
                    modalRegisterSuccess
                    &&
                    <ModalRegisterSuccess
                        modalVisible={this.state.modalRegisterSuccess}
                        closeModal={() => this.setState({ modalRegisterSuccess: false })}
                    />
                }
            </SafeView>
        );
    }
}

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(RegisterOpt);
