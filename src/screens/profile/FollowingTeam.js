import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actFollowingTeamLists, actClearTeamListFollowingTeam } from '../../actions/user';
import ItemTeam from '../../components/team/ItemTeam';
import { actTeamFollow } from '../../actions/team';

class FollowingTeam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            per_page: 12,
            page: 1
        };
    }

    componentDidMount() {

        this.props.getFollowingTeamList(this.state.page, this.state.per_page)
    }

    componentWillUnmount() {
        this.props.onClearFollowTeam()
    }
    _renderFooter() {
        var { followingTeam } = this.props.user
        if (followingTeam.loadMore || followingTeam.loading) {
            return <ActivityIndicator size="large" color="#000000" style={styles.loading} />
        } else return null
    }
    renderItem = (item, index, prevItem) => {
        var { language } = this.props.language;
        var x = false
        if (item.Role == "Guest" &&  (index === 0 || (prevItem && prevItem.Role !== "Guest"))) {
            x = true
        }


        var { followingTeam } = this.props.user
        var disabled = followingTeam.teamIdAction == item.Id ? followingTeam.loadingFollowers : false;
        if (x) return <>
            <View style={[styles.boxTitle]}>
                <Text style={[styles.txtTitle]}>{convertLanguage(language, 'following_team')}</Text>
            </View>
            <ItemTeam
                style={{ marginTop: 20 }}
                key={'teams_' + item.Id}
                data={item}
                followTeam={this.followTeam}
                disabled={disabled}
                navigation={this.props.navigation}
            />
        </>
        else
            return <ItemTeam
                style={{ marginTop: 20 }
                }
                key={'teams_' + item.Id}
                data={item}
                followTeam={this.followTeam}
                disabled={disabled}
                navigation={this.props.navigation}
            />

    }
    followTeam = (teamId) => {
        this.props.followTeam(teamId)
    }
    refresh() {
        this.props.getFollowingTeamList(1, this.state.per_page)
        this.setState({ page: 1 });
    }

    loadMore() {
        var { followingTeam } = this.props.user
        var { page } = this.state

        if (followingTeam.loadMore) {
            this.props.getFollowingTeamList(page + 1, this.state.per_page)

            this.setState({
                page: page + 1
            })
        }

    }
    renderTeam() {
        var { language } = this.props.language;
        var { followingTeam } = this.props.user
        return (<>
        {
            followingTeam.teams.length > 0 && followingTeam.teams[0].Role !== 'Guest' &&
            <View style={styles.boxTitle}>
                <Text style={[styles.txtTitle]}>{convertLanguage(language, 'my_team')}</Text>
            </View>
        }
            {followingTeam.is_empty ? <Text style={{ paddingHorizontal: 20, textAlign: 'center' }} >{convertLanguage(language, 'data_empty')}</Text> : null}
        </>
        )

    }

    renderFollowTeam() {
        var { followingTeam } = this.props.user
        var { language } = this.props.language;
        return (<>
            <View style={[styles.boxTitle, { paddingTop: 0 }]}>
                <Text style={[styles.txtTitle]}>{convertLanguage(language, 'following_team')}</Text>
            </View>

            {followingTeam.is_empty ? <Text style={{ paddingHorizontal: 20, textAlign: 'center' }} >{convertLanguage(language, 'data_empty')}</Text> :
                <View>

                    {followingTeam.teams.map((item, index) => {
                        return index > 4 && this.renderItem(item, index)
                    })
                    }
                </View>
            }
        </>
        )

    }

    render() {
        var { followingTeam } = this.props.user
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'following_team')}</Text>
                    <Touchable style={{ minHeight: 48, minWidth: 48, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')}>
                        <Image source={require('../../assets/search.png')} style={{ width: 32, height: 32 }} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    {/* <ScrollView contentContainerStyle={{ paddingBottom: 20 }}>
                        {this.renderTeam()}
                        <View style={{ marginHorizontal: 20, marginVertical: 20, height: 1, backgroundColor: '#bdbdbd' }}></View>
                        {this.renderFollowTeam()}
                    </ScrollView> */}
                    <FlatList
                        // style={{ flex: 1, width: '100%' }}
                        contentContainerStyle={{ paddingBottom: 20 }}
                        data={followingTeam.teams}
                        renderItem={({ item, index }) => {
                            return this.renderItem(item, index, followingTeam.teams[index - 1])
                        }}
                        onEndReached={() => { !followingTeam.loading ? this.loadMore() : null }}
                        ListHeaderComponent={this.renderTeam()}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        refreshing={false}
                        onRefresh={() => { this.refresh() }}
                        ListFooterComponent={() => this._renderFooter()}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    boxTitle: {
        paddingTop: 20,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    txtTitle: {
        color: Colors.TEXT_P,
        fontSize: 34 / 2,
        fontWeight: 'bold',
        // marginTop: 20,
    },
    content: {
        flex: 1,
    },
    loading: {
        padding: 20
    },
});

const mapStateToProps = state => {
    return {
        user: state.user,
        language: state.language,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getFollowingTeamList: (page, per_page) => {
            dispatch(actFollowingTeamLists(page, per_page))
        },
        followTeam: (teamId) => {
            dispatch(actTeamFollow(teamId))
        },
        onClearFollowTeam: () => {
            dispatch(actClearTeamListFollowingTeam())
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FollowingTeam);
// export default EventHostList;
