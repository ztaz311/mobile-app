import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Alert, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { convertLanguage } from '../../services/Helper';
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import ModalPageWebview from '../../components/ModalPageWebView';
import * as Config from '../../constants/Config';

class Setting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uri: '',
            modalPageWebview: false,
            title: ''
        };
    }

    componentDidMount() {
        // this.getTeams();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    logout = () => {
        var { language } = this.props.language;
        Alert.alert(
            convertLanguage(language, 'warning'),
            convertLanguage(language, 'logout_question'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => this.props.navigation.navigate('Logout'), style: 'cancel' },
            ],
            { cancelable: false },
        );
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'setting')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <ScrollView style={styles.content} disableScrollViewPanResponder={true}>
                    <Touchable style={styles.row} onPress={() => this.props.navigation.navigate('EditProfile')}>
                        <Image source={require('../../assets/ic_profile.png')} style={styles.icon} />
                        <Text style={styles.text}>{convertLanguage(language, 'edit_profile')}</Text>
                    </Touchable>
                    <Touchable style={styles.row} onPress={() => this.props.navigation.navigate('UpdatePassword')}>
                        <Image source={require('../../assets/ic_security.png')} style={styles.icon} />
                        <Text style={styles.text}>{convertLanguage(language, 'security_setting')}</Text>
                    </Touchable>
                    <Touchable style={styles.row} onPress={() => this.props.navigation.navigate('NotificationSetting')}>
                        <Image source={require('../../assets/generalsetting.png')} style={styles.icon} />
                        <Text style={styles.text}>{convertLanguage(language, 'general_setting')}</Text>
                    </Touchable>
                    <Touchable style={styles.row} onPress={() => this.props.navigation.navigate('ReceivedRequest')}>
                        <Image source={require('../../assets/inbox.png')} style={styles.icon} />
                        <Text style={styles.text}>{convertLanguage(language, 'received_request')}</Text>
                    </Touchable>
                    <Touchable style={styles.row} onPress={() => this.props.navigation.navigate('PurchaseHistory')}>
                        <Image source={require('../../assets/purchase_history.png')} style={styles.icon} />
                        <Text style={styles.text}>{convertLanguage(language, 'purchase_history')}</Text>
                    </Touchable>
                    <Touchable style={styles.row} onPress={() => this.props.navigation.navigate('UserSaleReport')}>
                        <Image source={require('../../assets/sale_report.png')} style={styles.icon} />
                        <Text style={styles.text}>{convertLanguage(language, 'sales_report')}</Text>
                    </Touchable>
                    <Touchable style={styles.row} onPress={this.logout}>
                        <Image source={require('../../assets/logout.png')} style={styles.icon} />
                        <Text style={[styles.text, {color: '#828282'}]}>{convertLanguage(language, 'logout')}</Text>
                    </Touchable>
                    <Text style={styles.txtTitle} onPress={() => this.setState({modalPageWebview: true, uri: `${Config.API_URL}/pages/terms-of-service`, title: convertLanguage(language, 'terms_of_service')})}>{convertLanguage(language, 'terms_of_service')}</Text>
                    <Text style={styles.txtTitle} onPress={() => this.setState({modalPageWebview: true, uri: `${Config.API_URL}/pages/privacy-policy`, title: convertLanguage(language, 'privacy_policy')})}>{convertLanguage(language, 'privacy_policy')}</Text>
                    <Text style={styles.txtTitle} onPress={() => this.setState({modalPageWebview: true, uri: `${Config.API_URL}/pages/introduce`, title: convertLanguage(language, 'about_comeup')})}>{convertLanguage(language, 'about_comeup')}</Text>
                </ScrollView>
                {
                    this.state.modalPageWebview &&
                    <ModalPageWebview
                        closeModal={() => this.setState({modalPageWebview: false})}
                        uri={this.state.uri}
                        navigation={this.props.navigation}
                        title={this.state.title}
                    />
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 48,
        marginTop: 16
    },
    icon: {
        width: 32,
        height: 32,
        marginRight: 8
    },
    text: {
        flex: 1,
        color: '#4F4F4F',
        fontSize: 16,
        fontWeight: '600'
    },
    txtTitle: {
        paddingVertical: 15,
        color: '#4F4F4F',
        fontSize: 16,
    }
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getLogout: (navigation) => {
            dispatch(actLogout(navigation))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Setting);
// export default EventHostList;
