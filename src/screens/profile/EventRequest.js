import React, { useEffect } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'

import * as Colors from '../../constants/Colors'
import { useSelector, useDispatch } from "react-redux";
import { actLoadDataEventRequest, actAcceptEvent, actDenyEvent } from '../../actions/user';
import ItemEventRequest from '../../components/event/ItemEventRequest';
import { convertLanguage } from '../../services/Helper';

function EventRequest({navigation}) {
    const [page, setpage] = React.useState(1);

    const user = useSelector(state => state.user)

    const language = useSelector(state => state.language)

    const dispatch = useDispatch()

    const onAcceptEvent = (Id) => dispatch(actAcceptEvent(Id))

    const onDenyEvent = (Id) => dispatch(actDenyEvent(Id))

    const onLoadDataEventRequest = (page) => dispatch(actLoadDataEventRequest(page))

    const renderFooter = () => {
        var { loadMore, is_empty } = user;
        // var { language } = language;
        if (loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', }}>{convertLanguage(language.language, 'no_data')}</Text>
            } else {
                return null
            }
        }
    }

    const loadMore = () => {
        var { loadMore } = user;
        if (loadMore) {
            onLoadDataEventRequest(page+1)
            setpage(page+1)
        }
    }

    // useEffect(() => {
    //     onLoadDataEventRequest(page)
    // }, [])
    
    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                <Touchable
                    onPress={() => navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                </Touchable>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>Event Request</Text>
                <View style={{ width: 48 }} />
            </View>

            <Line />
            <FlatList
                data={user.listEventRequest}
                contentContainerStyle={styles.content}
                renderItem={({ item }) => {
                    return <ItemEventRequest data={item} onAcceptEvent={onAcceptEvent} onDenyEvent={onDenyEvent} navigation={navigation} />
                }}
                onEndReached={loadMore}
                // ListHeaderComponent={renderHeader}
                onEndReachedThreshold={0.1}
                keyExtractor={(item, index) => index.toString()}
                ListFooterComponent={renderFooter}
            />
        </SafeView>
    );
}

const styles = StyleSheet.create({
    content: {
        // flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20
    },
});

export default EventRequest;
