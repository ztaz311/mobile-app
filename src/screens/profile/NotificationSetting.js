import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import ToggleSwitch from 'toggle-switch-react-native'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actUpdateSetting } from '../../actions/user';

class NotificationSetting extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        // this.getTeams();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    onUpdateSetting(body) {
        this.props.onUpdateSetting(body)
    }

    render() {
        var { language } = this.props.language;
        var { GeneralSetting } = this.props.user.profile;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'general_setting')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <ScrollView style={styles.content}>
                    <View style={styles.boxNoti}>
                        <Text style={styles.txtLabel}>{convertLanguage(language, 'push_notifications')}</Text>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'promotion_push')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.PromotionPush === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({PromotionPush: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'hour_24_reminder_push')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.Hour24ReminderPush === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({Hour24ReminderPush: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'received_collaborating_request_push')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.ReceivedCollaboratingRequestPush === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({ReceivedCollaboratingRequestPush: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'received_joining_team_request_push')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.ReceivedJoiningTeamRequestPush === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({ReceivedJoiningTeamRequestPush: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'received_refund_push')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.ReceivedRefundPush === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({ReceivedRefundPush: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'team_new_follower_push')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.TeamNewFollowerPush === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({TeamNewFollowerPush: isOn ? 1 : 0})}
                            />
                        </View>
                    </View>
                    <View style={styles.boxNoti}>
                        <Text style={styles.txtLabel}>{convertLanguage(language, 'email_sms_notifications')}</Text>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'promotion_email')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.PromotionEmail === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({PromotionEmail: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'hour_24_reminder_email')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.Hour24ReminderEmail === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({Hour24ReminderEmail: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'received_collaborating_request_email')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.ReceivedCollaboratingRequestEmail === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({ReceivedCollaboratingRequestEmail: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'received_joining_team_request_email')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.ReceivedJoiningTeamRequestEmail === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({ReceivedJoiningTeamRequestEmail: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'received_refund_email')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.ReceivedRefundEmail === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({ReceivedRefundEmail: isOn ? 1 : 0})}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.boxContent}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'weekly_news_letter_email')}</Text>
                                {/* <Text style={styles.txtContent}>This might be some description of the function</Text> */}
                            </View>
                            <ToggleSwitch
                                isOn={GeneralSetting.weeklyNewsLetterEmail === 1}
                                onColor="#00A9F4"
                                offColor="#F2F2F2"
                                size="small"
                                onToggle={isOn => this.onUpdateSetting({weeklyNewsLetterEmail: isOn ? 1 : 0})}
                            />
                        </View>
                    </View>
                </ScrollView>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16,
    },
    boxNoti: {
        marginTop: 16
    },
    txtLabel: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#333333',
        marginBottom: 24
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 16,
        paddingHorizontal: 12
    },
    boxContent: {
        flex: 1,
        paddingRight: 16
    },
    txtTitle: {
        fontSize: 16,
        color: '#4F4F4F',
        marginBottom: 5
    },
    txtContent: {
        fontSize: 12,
        color: '#828282'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        user: state.user
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onUpdateSetting: (body) => {
            dispatch(actUpdateSetting(body))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NotificationSetting);
