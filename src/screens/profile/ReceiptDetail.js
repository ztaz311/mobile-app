import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Linking, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { useSelector } from "react-redux";
import * as Colors from '../../constants/Colors';
import { convertDate, convertLanguage, formatNumber } from '../../services/Helper';

function ReceiptDetail({ navigation, route }) {
    const language = useSelector(state => state.language)
    const { data } = route.params;
    const { payment } = data;

    const getTotal = () => {
        var result = 0;
        switch (data.Type) {
            case 'payment':
                result = payment.TotalMoney;
                break;
            case 'refund':
                result = data.paymentRefund.Ticket.Price;
                break;
            default:
                break;
        }
        return result;
    }

    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                <Touchable
                    onPress={() => navigation.goBack()}
                    style={{ minHeight: 48, alignItems: 'center', flexDirection: 'row', marginLeft: 12 }}>
                    <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24, marginRight: 5}} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>E-Receipt</Text>
                </Touchable>
                {
                    (payment.LinkPaymentGate !== "" && payment.LinkPaymentGate) &&
                    <Text style={{ fontSize: 16, color: '#00A9F4', marginRight: 16 }} onPress={() => Linking.openURL(payment.LinkPaymentGate)}>{convertLanguage(language.language, 'text_print_receipt')}</Text>
                }
            </View>

            <Line />

            <ScrollView style={styles.content}>
                <View style={styles.boxInfo}>
                    {
                        payment.Team &&
                        <Touchable style={[styles.rowBetween, { borderBottomColor: 'rgba(51, 51, 51, 0.25)', borderBottomWidth: 1, paddingBottom: 8 }]} onPress={() => navigation.navigate({name: 'DetailTeam', params: { id: payment.Team.Id }, key: payment.Team.Id})}>
                            <View style={[styles.row, { flex: 1 }]}>
                                <Image source={{ uri: payment.Team.Logos.Small }} style={styles.avatar} />
                                <Text style={[styles.txtTitle, { flex: 1 }]}>{payment.Team.Name}</Text>
                            </View>
                            <Image source={require('../../assets/right.png')} style={styles.ic_right} />
                        </Touchable>
                    }
                    <Touchable style={styles.boxEventDetail} onPress={() => navigation.navigate({ name: 'EventDetail', params: { Slug: payment.Event.Slug }, key: payment.Event.Slug })}>
                        <Image source={{ uri: payment.Event.Posters ? payment.Event.Posters.Small : payment.Event.Poster }} style={styles.image_event} />
                        <View style={styles.boxEventInfo}>
                            <Text style={styles.txtEventName} numberOfLines={2}>{payment.Event.Title}</Text>
                            <Text style={[styles.txtStatus, { marginBottom: 3 }]} numberOfLines={2}>{convertDate(payment.Event.TimeStart, payment.Event.TimeFinish)}</Text>
                            <Text style={[styles.txtStatus, { marginBottom: 3 }]} numberOfLines={2}>{payment.Event.VenueName}</Text>
                        </View>
                    </Touchable>
                </View>
                <View style={styles.boxThank}>
                    <Text style={styles.txtThank}>{convertLanguage(language.language, 'thank_for_using_service')}</Text>
                    <View style={[styles.rowBetween, { marginVertical: 16 }]}>
                        <Text style={styles.txtStatus}>{payment.CreatedAt}</Text>
                        <Text style={styles.txtStatus}>Purchase ID: {payment.TransactionCode}</Text>
                    </View>
                    <View style={styles.boxBankInfo}>
                        <View style={[styles.rowBetween, { paddingHorizontal: 8, paddingBottom: 8 }]}>
                            <Text style={styles.txtTicket}>{convertLanguage(language.language, 'payment_method')}: </Text>
                            <Text style={styles.txtTicket}>{payment.Type == 1 ? convertLanguage(language.language, 'domestic_foreign_card') : convertLanguage(language.language, 'cash_on_delivery')}</Text>
                        </View>
                        {
                            payment.Type == 1 &&
                            <>
                                <View style={[styles.rowBetween, { paddingHorizontal: 8, paddingBottom: 8 }]}>
                                    <Text style={styles.txtTicket}>{convertLanguage(language.language, 'bank_name')}:</Text>
                                    <Text style={styles.txtTicket}>{payment.BankName}</Text>
                                </View>
                                <View style={[styles.rowBetween, { paddingHorizontal: 8, paddingBottom: 8 }]}>
                                    <Text style={styles.txtTicket}>{convertLanguage(language.language, 'card_number')}:</Text>
                                    <Text style={styles.txtTicket}>{payment.CardNo}</Text>
                                </View>
                                <View style={[styles.rowBetween, { paddingHorizontal: 8, paddingBottom: 8 }]}>
                                    <Text style={styles.txtTicket}>{convertLanguage(language.language, 'account_holder_name')}:</Text>
                                    <Text style={styles.txtTicket}>{payment.AcountHolderName}</Text>
                                </View>
                            </>
                        }
                    </View>
                </View>
                {
                data.Type === 'refund' ?
                    data.paymentRefund &&
                    <View style={styles.boxPurchaseDetail}>
                        <View style={[styles.rowBetween, { paddingHorizontal: 8, paddingBottom: 8 }]}>
                            <Text style={[styles.txtStatus, { flex: 1 }]}>{1} x {data.paymentRefund.Ticket.Name}</Text>
                            <Text style={styles.txtStatus}>{data.paymentRefund.Ticket.Price == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(data.paymentRefund.Ticket.Price) + ' ' + payment.Unit.toUpperCase()}</Text>
                        </View>
                        <View style={styles.hr}></View>
                        <View style={styles.boxTotal}>
                            <Text style={styles.txtTotal}>{convertLanguage(language.language, 'total')}: {getTotal() == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(getTotal()) + ' ' + payment.paymentDetails[0].Ticket.Unit.toUpperCase()}</Text>
                        </View>
                    </View>
                    :
                    payment.paymentDetails.length > 0 &&
                    <View style={styles.boxPurchaseDetail}>
                        {
                            payment.paymentDetails.map((item, index) => {
                                return <View key={index} style={[styles.rowBetween, { paddingHorizontal: 8, paddingBottom: 8 }, index === 0 ? { paddingTop: 0 } : {}]}>
                                    <Text style={[styles.txtStatus, { flex: 1 }]}>{item.Quantity} x {item.Ticket.Name}</Text>
                                    <Text style={styles.txtStatus}>{item.Price == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(item.Price) + ' ' + payment.Unit.toUpperCase()}</Text>
                                </View>
                            })
                        }
                        <View style={styles.hr}></View>
                        <View style={styles.boxTotal}>
                        <Text style={styles.txtTotal}>{convertLanguage(language.language, 'total')}: {getTotal() == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(getTotal()) + ' ' + payment.paymentDetails[0].Ticket.Unit.toUpperCase()}</Text>
                        </View>
                    </View>
}
                {
                    // payment.paymentDetails.length > 0 &&
                    // <View style={styles.boxPurchaseDetail}>
                    //     {
                    //         payment.paymentDetails.map((item, index) => {
                    //             return <View key={index} style={[styles.rowBetween, { paddingHorizontal: 8, paddingBottom: 8 }]}>
                    //                 <Text style={styles.txtTicket}>{item.Quantity} x {item.Ticket.Name}</Text>
                    //                 <Text style={styles.txtTicket}>{item.Price == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(item.Price) + ' ' + item.Ticket.Unit.toUpperCase()}</Text>
                    //             </View>
                    //         })
                    //     }
                    //     <View style={styles.hr}></View>
                    //     <View style={styles.boxTotal}>
                    //         {/* <Text style={[styles.txtTitle, { marginTop: 8 }]}>Delivery fee: 26.000 VNĐ</Text>
                    //         <Text style={[styles.txtTitle, { marginTop: 8 }]}>VAT: 26.000 VNĐ</Text> */}
                    //         <Text style={styles.txtTotal}>{convertLanguage(language.language, 'total')}: {getTotal() == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(getTotal()) + ' ' + payment.paymentDetails[0].Ticket.Unit.toUpperCase()}</Text>
                    //     </View>
                    // </View>
                }
                <View style={styles.boxAmount}>
                    {
                        payment.Vat &&
                        <View style={[styles.rowBetween, { marginBottom: 8 }]}>
                            <Text style={styles.txtTitle}>VAT:</Text>
                            <Text style={styles.txtTitle}>{getTotal() == 0 ? 'Free Ticket' : formatNumber(payment.Vat) + ' ' + payment.paymentDetails[0].Ticket.Unit.toUpperCase()}</Text>
                        </View>
                    }
                    {
                        payment.DeliveryFee &&
                        <View style={[styles.rowBetween, { marginBottom: 8 }]}>
                            <Text style={styles.txtTitle}>Delivery fee:</Text>
                            <Text style={styles.txtTitle}>{getTotal() == 0 ? 'Free Ticket' : formatNumber(payment.DeliveryFee) + ' ' + payment.paymentDetails[0].Ticket.Unit.toUpperCase()}</Text>
                        </View>
                    }
                    <View style={[styles.rowBetween, { marginBottom: 8 }]}>
                        <Text style={styles.txtTitle}>{convertLanguage(language.language, 'total_amount')}:</Text>
                        <Text style={styles.txtTitle}>{getTotal() == 0 ? 'Free Ticket' : formatNumber(getTotal()) + ' ' + payment.paymentDetails[0].Ticket.Unit.toUpperCase()}</Text>
                    </View>
                </View>
            </ScrollView>
        </SafeView>
    );
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxInfo: {
        padding: 16,
        backgroundColor: '#F2F2F2'
    },
    rowBetween: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    avatar: {
        width: 32,
        height: 32,
        borderRadius: 16,
        marginRight: 8
    },
    txtTitle: {
        fontSize: 12,
        color: '#333333'
    },
    ic_right: {
        width: 18,
        height: 18,
        marginLeft: 5
    },
    txtStatus: {
        fontSize: 12,
        color: '#828282'
    },
    boxEventDetail: {
        flexDirection: 'row',
        paddingVertical: 8
    },
    image_event: {
        width: 70,
        height: 70,
        borderRadius: 4,
        marginRight: 8
    },
    boxEventInfo: {
        flex: 1
    },
    txtEventName: {
        fontSize: 14,
        color: '#333333',
        marginBottom: 4
    },
    boxThank: {
        paddingHorizontal: 16
    },
    txtThank: {
        textAlign: 'center',
        marginTop: 16,
        color: '#4F4F4F',
        fontSize: 16,
        marginBottom: 4
    },
    boxPurchaseDetail: {
        backgroundColor: '#F2F2F2',
        padding: 16
    },
    txtTicket: {
        fontSize: 12,
        color: '#4F4F4F'
    },
    hr: {
        height: 1,
        backgroundColor: '#00A9F4',
        width: '50%',
        alignSelf: 'center',
        marginVertical: 8
    },
    boxTotal: {
        alignItems: 'flex-end',
        marginTop: 16
    },
    txtTotal: {
        fontSize: 12,
        color: '#00A9F4'
    },
    boxAmount: {
        paddingHorizontal: 24,
        paddingVertical: 16
    },
    boxBankInfo: {
        paddingBottom: 16
    }
});

export default ReceiptDetail;
