import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actPastHostingEvent, actClearEventPastEventHostEvent } from '../../actions/user';
import ItemEvent from '../../components/event/ItemEvent';
import { actLikeEvent } from '../../actions/event';

class HostingEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            per_page: 12,
            page: 1
        };
    }

    componentDidMount() {

        this.props.onActSearchEvent(this.state.page, this.state.per_page)
    }
    componentWillUnmount() {
        this.props.onClearEventPastEventHostEvent()
    }

    _renderFooter() {
        var { hostingEvent } = this.props.user
        var { language } = this.props.language;
        if (hostingEvent.past_loading) { return <ActivityIndicator size="large" color="#000000" style={styles.loading} /> } else {
            if (hostingEvent.past_loadMore) { return <ActivityIndicator size="large" color="#000000" style={styles.loading} /> }
            else if (hostingEvent.past_is_empty) {
                return <Text style={{ paddingHorizontal: 20, textAlign: 'center' }} >{convertLanguage(language, 'data_empty')}</Text>
            } else { return <></> }
        }
    }
    loadMore() {
        var { hostingEvent } = this.props.user
        var { page } = this.state
        if (hostingEvent.past_loadMore) {
            this.props.onActSearchEvent(this.state.page + 1, this.state.per_page)

            this.setState({
                page: page + 1
            })
        }

    }

    refresh() {
        this.props.onActSearchEvent(1, this.state.per_page)
        this.setState({ page: 1 });
    }


    render() {
        var { language } = this.props.language;
        var { hostingEvent } = this.props.user;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'past_events')}</Text>
                    {/* <View style={{ width: 48 }} /> */}
                    <Touchable style={{ minHeight: 48, minWidth: 48, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')}>
                        <Image source={require('../../assets/search.png')} style={{ width: 32, height: 32 }} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    <FlatList
                        style={{ paddingTop: 20 }}
                        contentContainerStyle={{ paddingBottom: 20 }}
                        data={hostingEvent.past_events}
                        renderItem={({ item }) => {
                            return <ItemEvent cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} />
                        }}
                        onEndReached={() => { this.loadMore() }}

                        onEndReachedThreshold={0.5}
                        refreshing={false}
                        onRefresh={() => { this.refresh() }}
                        ListFooterComponent={() => this._renderFooter()}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    loading: {
        padding: 20
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        user: state.user
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onActSearchEvent: (page, per_page) => {
            dispatch(actPastHostingEvent(page, per_page))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onClearEventPastEventHostEvent: () => {
            dispatch(actClearEventPastEventHostEvent())
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HostingEvent);
