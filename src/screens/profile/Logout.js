import React, { Component } from 'react';
import { ActivityIndicator, View, Text, StyleSheet } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import AsyncStorage from '@react-native-community/async-storage';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { actLogout, actRemoveDeviceToken } from '../../actions/user';

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";

class Logout extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        LoginManager.logOut()
        GoogleSignin.revokeAccess();
        GoogleSignin.signOut()
        let arr = []
        arr.push(['auKey', ''])
        arr.push(['auValue', ''])
        AsyncStorage.multiSet(arr)
        this.props.getLogout(this.props.navigation);
        this.removeToken()
    }

    async removeToken() {
        var { profile } = this.props.user;
        const fcmToken = await AsyncStorage.getItem('fcmToken');
        AsyncStorage.removeItem('fcmToken')
        var body = {
            NotiPushKey: fcmToken,
            UserId: profile.Id,
        }
        this.props.onRemoveDeviceToken(body);
    }

    render() {
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <ActivityIndicator size="large" color="#000000" />
                <Text style={{fontSize: 14, color: '#333333', marginTop: 5}}>Logging out</Text>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
});

const mapStateToProps = state => {
    return {
        user: state.user
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getLogout: (navigation) => {
            dispatch(actLogout(navigation))
        },
        onRemoveDeviceToken: (body) => {
            dispatch(actRemoveDeviceToken(body))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Logout);
// export default EventHostList;
