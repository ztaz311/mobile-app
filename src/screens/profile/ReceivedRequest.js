import React, { useEffect } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { actLoadDataEventRequest, actLoadDataTeamRequest, actAcceptEvent, actDenyEvent, actAcceptTeam, actDenyTeam } from '../../actions/user';
import * as Colors from '../../constants/Colors'
import { useSelector, useDispatch } from "react-redux";
import ItemEventRequest from '../../components/event/ItemEventRequest';
import ItemTeamRequest from '../../components/team/ItemTeamRequest';

function ReceivedRequest({ navigation, route }) {

    console.log(route)

    const dispatch = useDispatch()

    const onLoadDataEventRequest = (page, notification) => dispatch(actLoadDataEventRequest(page, notification))

    const onAcceptEvent = (Id) => dispatch(actAcceptEvent(Id))

    const onDenyEvent = (Id) => dispatch(actDenyEvent(Id))

    const onAcceptTeam = (Id) => dispatch(actAcceptTeam(Id))

    const onDenyTeam = (Id) => dispatch(actDenyTeam(Id))

    const user = useSelector(state => state.user)

    const language = useSelector(state => state.language)

    const onLoadDataTeamRequest = (page) => dispatch(actLoadDataTeamRequest(page))

    useEffect(() => {
        const notification = route.params && route.params['notification'] ? route.params['notification'] : '';
        onLoadDataEventRequest(1, notification)
    }, [])

    useEffect(() => {
        onLoadDataTeamRequest(1)
    }, [])
    
    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                <Touchable
                    onPress={() => navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                </Touchable>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language.language, 'received_request')}</Text>
                <View style={{ width: 48 }} />
            </View>

            <Line />

            <ScrollView style={styles.content}>
                <View style={styles.boxNoti}>
                    <Text style={styles.txtLabel}>{convertLanguage(language.language, 'team_invitations')}</Text>
                    {
                        user.loadMore ?
                            <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
                            :
                            user.listTeamRequest.length == 0 ?
                                <Text style={{ textAlign: 'center' }}>{convertLanguage(language.language, 'no_data')}</Text>
                                :
                                <>
                                    {
                                        user.listTeamRequest.map((item, index) => {
                                            if (index < 2) {
                                                return <ItemTeamRequest key={index} onAcceptTeam={onAcceptTeam} onDenyTeam={onDenyTeam} data={item} navigation={navigation} />
                                            } else {
                                                return null;
                                            }
                                        })
                                    }
                                    {
                                        user.listTeamRequest.length > 2 &&
                                        <Touchable style={styles.btnSeeMore} onPress={() => navigation.navigate('TeamRequest')}>
                                            <Text style={styles.txtDeny}>{convertLanguage(language.language, 'see_more')}</Text>
                                        </Touchable>
                                    }
                                </>
                    }
                </View>
                <View style={styles.boxNoti}>
                    <Text style={styles.txtLabel}>{convertLanguage(language.language, 'event_invitations')}</Text>
                    {
                        user.loadMore ?
                            <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
                            :
                            user.listEventRequest.length == 0 ?
                                <Text style={{ textAlign: 'center' }}>{convertLanguage(language.language, 'no_data')}</Text>
                                :
                                <>
                                    {
                                        user.listEventRequest.map((item, index) => {
                                            if (index < 2) {
                                                return <ItemEventRequest key={index} data={item} onAcceptEvent={onAcceptEvent} onDenyEvent={onDenyEvent} navigation={navigation} />
                                            } else {
                                                return null;
                                            }
                                        })
                                    }
                                    {
                                        user.listEventRequest.length > 2 &&
                                        <Touchable style={styles.btnSeeMore} onPress={() => navigation.navigate('EventRequest')}>
                                            <Text style={styles.txtDeny}>{convertLanguage(language.language, 'see_more')}</Text>
                                        </Touchable>
                                    }
                                </>
                    }
                </View>
            </ScrollView>
        </SafeView>
    );
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16,
    },
    boxNoti: {
        marginTop: 16
    },
    txtLabel: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#333333',
        marginBottom: 16
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    boxContent: {
        flex: 1,
        paddingRight: 16
    },
    txtTitle: {
        fontSize: 16,
        color: '#333333',
        marginBottom: 5,
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 12,
        color: '#828282'
    },
    boxRequest: {
        marginBottom: 16
    },
    txtInvite: {
        color: '#00A9F4',
        fontSize: 12,
        marginBottom: 8
    },
    avatar_team: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginRight: 16
    },
    txtCategory: {
        fontSize: 12,
        color: '#4F4F4F',
        marginBottom: 5
    },
    btnAccept: {
        height: 32,
        paddingHorizontal: 16,
        borderRadius: 4,
        backgroundColor: '#00A9F4',
        marginRight: 10,
        justifyContent: 'center'
    },
    txtAccept: {
        color: '#FFFFFF',
        fontSize: 16
    },
    btnDeny: {
        height: 32,
        paddingHorizontal: 16,
        borderRadius: 4,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#4F4F4F',
        justifyContent: 'center'
    },
    txtDeny: {
        color: '#4F4F4F',
        fontSize: 16
    },
    btnSeeMore: {
        height: 36,
        borderRadius: 4,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#4F4F4F',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 16
    },
    avatar_event: {
        width: 100,
        height: 176,
        borderRadius: 4,
        marginRight: 16
    },
});

export default ReceivedRequest;
