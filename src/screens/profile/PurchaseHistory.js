import React, { useEffect } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { convertDate3, convertLanguage, formatNumber, convertDate2 } from '../../services/Helper';
import * as Colors from '../../constants/Colors';
import FastImage from 'react-native-fast-image';
import { useSelector, useDispatch } from "react-redux";
import { actLoadDataPurchaseHistory } from '../../actions/user';

function PurchaseItem({ data, navigation }) {
    const { payment } = data;
    const language = useSelector(state => state.language)
    const showType = (Type) => {
        var result = '';
        switch (Type) {
            case 'payment':
                result = convertLanguage(language.language, 'payment_success');
                break;
            case 'refund':
                result = convertLanguage(language.language, 'refunded');
                break;
            default:
                break;
        }
        return result;
    }
    const getTotal = () => {
        var result = 0;
        switch (data.Type) {
            case 'payment':
                result = payment.TotalMoney;
                break;
            case 'refund':
                result = data.paymentRefund.Ticket.Price;
                break;
            default:
                break;
        }
        return result;
    }
    return <View style={styles.boxPurchase}>
        <View style={styles.rowBetween}>
            <Text style={[styles.txtStatus, data.Type == 'refund' ? { color: '#EB5757' } : {}]}>{showType(data.Type)}</Text>
            <Text style={styles.txtStatus}>{convertDate2(payment.CreatedAt)} · {payment.TransactionCode}</Text>
        </View>
        <View style={styles.boxContent}>
            <Touchable style={styles.boxEventDetail} onPress={() => navigation.navigate({ name: 'EventDetail', params: { Slug: payment.Event.Slug }, key: payment.Event.Slug })}>
                <FastImage source={{ uri: payment.Event.Posters ? payment.Event.Posters.Small : payment.Event.Poster }} style={styles.image_event} />
                <View style={styles.boxEventInfo}>
                    <Text style={styles.txtEventName} numberOfLines={2}>{payment.Event.Title}</Text>
                    {/* <Text style={[styles.txtStatus, { marginBottom: 2 }]} numberOfLines={2}>{convertDate(payment.Event.TimeStart, payment.Event.TimeFinish)}</Text> */}
                    <Text style={[styles.txtStatus, { marginBottom: 2 }]} numberOfLines={2}>{convertDate3(payment.Event.TimeStart, payment.Event.TimeFinish, language)}</Text>
                    <Text style={[styles.txtStatus, { marginBottom: 2 }]} numberOfLines={2}>{payment.Event.VenueName}</Text>
                </View>
            </Touchable>
            {
                data.Type === 'refund' ?
                    data.paymentRefund &&
                    <View style={styles.boxPurchaseDetail}>
                        <View style={[styles.rowBetween, { borderBottomWidth: 0, paddingHorizontal: 4 }]}>
                            <Text style={[styles.txtStatus, { flex: 1 }]}>{1} x {data.paymentRefund.Ticket.Name}</Text>
                            <Text style={styles.txtStatus}>{data.paymentRefund.Ticket.Price == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(data.paymentRefund.Ticket.Price) + ' ' + payment.Unit.toUpperCase()}</Text>
                        </View>
                        <View style={[styles.rowBetween, { borderBottomWidth: 0, paddingHorizontal: 4 }]}>
                            <Text style={[styles.txtStatus, { color: '#000000' }]}>{convertLanguage(language.language, 'total_amount')}:</Text>
                            <Text style={[styles.txtStatus, { color: '#00A9F4' }]}>{getTotal() == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(getTotal()) + ' ' + payment.Unit.toUpperCase()}</Text>
                        </View>
                    </View>
                    :
                    payment.paymentDetails.length > 0 &&
                    <View style={styles.boxPurchaseDetail}>
                        {
                            payment.paymentDetails.map((item, index) => {
                                return <View key={index} style={[styles.rowBetween, { borderBottomWidth: 0, paddingHorizontal: 4 }, index === 0 ? { paddingTop: 0 } : {}]}>
                                    <Text style={[styles.txtStatus, { flex: 1 }]}>{item.Quantity} x {item.Ticket.Name}</Text>
                                    <Text style={styles.txtStatus}>{item.Price == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(item.Price) + ' ' + payment.Unit.toUpperCase()}</Text>
                                </View>
                            })
                        }
                        <View style={[styles.rowBetween, { borderBottomWidth: 0, paddingHorizontal: 4 }]}>
                            <Text style={[styles.txtStatus, { color: '#000000' }]}>{convertLanguage(language.language, 'total_amount')}:</Text>
                            <Text style={[styles.txtStatus, { color: '#00A9F4' }]}>{getTotal() == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(getTotal()) + ' ' + payment.Unit.toUpperCase()}</Text>
                        </View>
                    </View>
            }
            {
                // payment.paymentDetails.length > 0 &&
                // <View style={styles.boxPurchaseDetail}>
                //     {
                //         payment.paymentDetails.map((item, index) => {
                //             return <View key={index} style={[styles.rowBetween, { borderBottomWidth: 0, paddingHorizontal: 4 }, index === 0 ? { paddingTop: 0 } : {}]}>
                //                 <Text style={[styles.txtStatus, { flex: 1 }]}>{item.Quantity} x {item.Ticket.Name}</Text>
                //                 <Text style={styles.txtStatus}>{item.Price == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(item.Price) + ' ' + payment.Unit.toUpperCase()}</Text>
                //             </View>
                //         })
                //     }
                //     <View style={[styles.rowBetween, { borderBottomWidth: 0, paddingHorizontal: 4 }]}>
                //         <Text style={[styles.txtStatus, { color: '#000000' }]}>{convertLanguage(language.language, 'total_amount')}:</Text>
                //         <Text style={[styles.txtStatus, { color: '#000000' }]}>{getTotal() == 0 ? convertLanguage(language.language, 'free_ticket') : formatNumber(getTotal()) + ' ' + payment.Unit.toUpperCase()}</Text>
                //     </View>
                // </View>
            }
        </View>
        <View style={styles.boxDetail}>
            <Touchable style={styles.btnReceipt} onPress={() => navigation.navigate('ReceiptDetail', { data })}>
                <Text style={styles.txtReceipt}>{convertLanguage(language.language, 'view_e_receipt')}</Text>
            </Touchable>
        </View>
    </View>
}

function PurchaseHistory({ navigation }) {
    const [page, setpage] = React.useState(1);

    const user = useSelector(state => state.user)

    const language = useSelector(state => state.language)

    const iso_code = useSelector(state => state.global.iso_code)

    const dispatch = useDispatch()

    const onLoadDataPurchaseHistory = (page) => dispatch(actLoadDataPurchaseHistory(page, iso_code))

    const renderFooter = () => {
        var { loadMore, is_empty } = user;
        if (loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', padding: 20 }}>{convertLanguage(language.language, 'no_data')}</Text>
            } else {
                return null
            }
        }
    }

    const loadMore = () => {
        var { loadMore } = user;
        if (loadMore) {
            onLoadDataPurchaseHistory(page + 1)
            setpage(page + 1)
        }
    }

    useEffect(() => {
        onLoadDataPurchaseHistory(page)
    }, [])
    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                <Touchable
                    onPress={() => navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                </Touchable>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language.language, 'purchase_history')}</Text>
                <View style={{ width: 48 }} />
            </View>

            <Line />
            <FlatList
                data={user.listPurchaseHistory}
                contentContainerStyle={styles.content}
                renderItem={({ item }) => {
                    return <PurchaseItem data={item} navigation={navigation} />
                }}
                onEndReached={loadMore}
                // ListHeaderComponent={renderHeader}
                onEndReachedThreshold={0.1}
                keyExtractor={(item, index) => index.toString()}
                ListFooterComponent={renderFooter}
            />
        </SafeView>
    );

}

const styles = StyleSheet.create({
    content: {
        // paddingLeft: 20,
        // paddingRight: 20,
        // paddingTop: 20
    },
    boxPurchase: {
        margin: 8,
        borderRadius: 4,
        shadowOffset: { width: 0, height: 3 },
        shadowColor: 'black',
        shadowRadius: 4,
        shadowOpacity: 0.2,
        backgroundColor: '#FFFFFF',
        elevation: 5,
    },
    rowBetween: {
        padding: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: 'rgba(51, 51, 51, 0.3)',
        borderBottomWidth: 0.5
    },
    txtStatus: {
        fontSize: 12,
        color: '#828282'
    },
    boxContent: {
        paddingHorizontal: 8,
        borderBottomColor: 'rgba(51, 51, 51, 0.3)',
        borderBottomWidth: 0.5,
    },
    boxEventDetail: {
        flexDirection: 'row',
        paddingHorizontal: 8,
        borderBottomColor: 'rgba(51, 51, 51, 0.3)',
        borderBottomWidth: 0.5,
        paddingVertical: 12
    },
    image_event: {
        width: 70,
        height: 70,
        borderRadius: 4,
        marginRight: 8
    },
    boxEventInfo: {
        flex: 1
    },
    txtEventName: {
        fontSize: 20,
        color: '#333333',
        marginBottom: 4,
        fontWeight: '600'
    },
    boxPurchaseDetail: {
        paddingHorizontal: 10,
        paddingVertical: 12
    },
    boxDetail: {
        padding: 16,
        alignItems: 'flex-end',
        paddingTop: 12
    },
    btnReceipt: {
        height: 36,
        paddingHorizontal: 16,
        borderWidth: 1,
        borderColor: '#4F4F4F',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4
    },
    txtReceipt: {
        fontSize: 16,
        color: '#4F4F4F'
    },
});

export default PurchaseHistory;
