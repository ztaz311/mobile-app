import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions, Alert } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import ItemEventNews from '../../components/event/ItemEventNews';
import ModalEventNews from '../../components/event/ModalEventNews';
import ModalZoomImage from '../../components/event/ModalZoomImage';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { getTag, convertLanguage } from '../../services/Helper';
import Share from 'react-native-share';
import { actLoadDataEventNews, actDeleteEventNews, actLikeEventNews } from '../../actions/event';
const { width, height } = Dimensions.get('window');
import * as Config from '../../constants/Config';
import ModalEventNewsDetail from '../../components/event/ModalEventNewsDetail';
class EventList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            modelEventNews: false,
            EventNewsId: '',
            zoomeImageIndex: -1,
            modalZoom: false,
            event_news: null,
            modalEventNewsDetail: false,
            eventnews: {},
            Role: ''
        };
    }

    componentDidMount() {
        var { page } = this.state;
        var { iso_code } = this.props.global;
        this.props.onLoadDataEventNews(this.props.route.params['Slug'], page, iso_code);
    }

    _renderFooter() {
        var { loadMore, is_empty } = this.props.event;
        var { language } = this.props.language;
        if (loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'event_news_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { loadMore } = this.props.event;
        var { iso_code } = this.props.global;
        var { page } = this.state;
        if (loadMore) {
            this.props.onLoadDataEventNews(this.props.route.params['Slug'], page + 1, iso_code);
            this.setState({ page: page + 1 })
        }
    }

    onEditEventNews() {
        var { EventNewsId } = this.state;
        this.setState({
            modelEventNews: false,
        })
        this.props.navigation.navigate('WriteEventNews', { Id: EventNewsId })
    }

    onReportEventNews() {
        var { EventNewsId } = this.state;
        this.setState({
            modelEventNews: false,
        })
        this.props.navigation.navigate('EventNewsReport', { Id: EventNewsId })
    }

    onDeleteEventNews() {
        var { EventNewsId } = this.state;
        var { language } = this.props.language;
        setTimeout(() => {
            Alert.alert(
                convertLanguage(language, 'delete_event_news'),
                convertLanguage(language, 'do_you_want_to_delete_this_news'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    { text: convertLanguage(language, 'ok'), onPress: () => { this.setState({ modalEventNewsDetail: false, modelEventNews: false, eventnews: {} }); this.props.onDeleteEventNews(EventNewsId) } },
                ],
                { cancelable: false },
            );
        }, 100)
    }

    onLikeEventNews(Id) {
        this.props.onLikeEventNews(Id);
    }

    onOpenModelEventNews(Id) {
        this.setState({
            modelEventNews: true,
            EventNewsId: Id,
        })
    }

    share() {
        let options = {
            title: this.props.route.params['Title'],
            message: this.props.route.params['Title'] + "\nFind and join event in Comeup\n" + getTag(this.props.route.params['HashTag']),
            url: Config.MAIN_URL + 'events/' + this.props.route.params['Slug'],
            subject: "Jois with us" //  for email
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    onOpenModelEventNewsDetail(item, Role) {
        item.Event = this.props.route.params['Event'];
        this.setState({
            modalEventNewsDetail: true,
            eventnews: item,
            Role: Role
        })
    }

    closeModalEventNewsDetail = () => {
        this.setState({ modalEventNewsDetail: false, eventnews: {} })
    }

    render() {
        var { event_news } = this.props.event;
        var { language } = this.props.language;
        var { Event } = this.props.route.params;
        return (
            <>
                <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                        <Touchable
                            onPress={() => this.props.navigation.goBack()}
                            style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                        </Touchable>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'event_news')}</Text>
                        <Touchable onPress={() => this.props.navigation.navigate('SearchResult')} style={{ minHeight: 48, minWidth: 48, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../assets/search.png')} style={{ width: 32, height: 32 }} />
                        </Touchable>
                    </View>

                    <Line />

                    <View style={styles.content}>
                        <FlatList
                            style={{
                                paddingLeft: 16,
                                paddingRight: 16
                            }}
                            data={event_news}
                            renderItem={({ item }) => {
                                return <ItemEventNews
                                    cancel_text={convertLanguage(language, 'canceled_event')}
                                    data={item}
                                    zoomImage={(index, dataZoom) => this.setState({ zoomeImageIndex: index, modalZoom: true, dataZoom: dataZoom })}
                                    Role={Event.Role}
                                    onDeleteEventNews={(Id) => this.props.onDeleteEventNews(Id)}
                                    Poster={Event.Poster}
                                    Title={Event.Title}
                                    navigation={this.props.navigation}
                                    onLikeEventNews={(Id) => this.onLikeEventNews(Id)}
                                    share={() => this.share()}
                                    onOpenModelEventNews={(Id) => this.onOpenModelEventNews(Id)}
                                    onOpenModelEventNewsDetail={() => this.onOpenModelEventNewsDetail(item, Event.Role)} />
                            }}
                            onEndReached={() => { this.loadMore() }}
                            // ListHeaderComponent={() => this.renderHeader()}
                            // ItemSeparatorComponent={this.renderSep}
                            onEndReachedThreshold={0.5}
                            // refreshing={this.props.team.loading}
                            // onRefresh={() => {this.refresh()}}
                            ListFooterComponent={() => this._renderFooter()}
                        />
                    </View>
                    {
                        this.state.modelEventNews &&
                        <ModalEventNews
                            closeModal={() => this.setState({ modelEventNews: false, EventNewsId: '' })}
                            onEditEventNews={() => this.onEditEventNews()}
                            onDeleteEventNews={() => this.onDeleteEventNews()}
                            Role={Event.Role}
                            onReportEventNews={() => this.onReportEventNews()}
                        />
                    }
                    {
                        this.state.modalZoom
                            ?
                            <ModalZoomImage
                                modalVisible={this.state.modalZoom}
                                closeModal={() => this.setState({ modalZoom: false })}
                                event={this.props.route.params['event']}
                                dataZoom={this.state.dataZoom}
                                index={this.state.zoomeImageIndex}
                            />
                            : null
                    }
                </SafeView>
                {
                    this.state.modalEventNewsDetail &&
                    <ModalEventNewsDetail
                        closeModal={this.closeModalEventNewsDetail}
                        data={this.state.eventnews}
                        onLikeEventNews={(Id) => this.onLikeEventNews(Id)}
                        onOpenModelEventNews={(Id) => this.onOpenModelEventNews(Id, this.state.Role)}
                        navigation={this.props.navigation}
                        onDeleteEventNews={(Id) => this.props.onDeleteEventNews(Id)}
                    />
                }
            </>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    imgBanner: {
        width: width,
        height: width * 9 / 16,
    },
    boxFilter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataEventNews: (slug, page, iso_code) => {
            dispatch(actLoadDataEventNews(slug, page, iso_code))
        },
        onDeleteEventNews: (EventNewsId) => {
            dispatch(actDeleteEventNews(EventNewsId))
        },
        onLikeEventNews: (EventNewsId) => {
            dispatch(actLikeEventNews(EventNewsId))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventList);
