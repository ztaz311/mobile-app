import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView, TextInput, Keyboard } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import { convertLanguage } from '../../services/Helper'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actDeleteEvent } from '../../actions/event';
import Loading from '../../screens_view/Loading';
class DeleteEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Password: ''
        };
    }

    componentDidMount() {
        // this.getTeams();
    }

    onDeleteEvent() {
        Keyboard.dismiss();
        var { event } = this.props.route.params;
        var { Password } = this.state;
        var body = {
            Password
        }
        this.props.onDeleteEvent(event.Id, body, this.props.navigation)
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    render() {
        var { language } = this.props.language;
        var { isFetching } = this.props.event;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />
                <View style={{ flex: 1 }}>
                    {
                        isFetching &&
                        <Loading />
                    }
                    <ScrollView style={styles.content} keyboardShouldPersistTaps='always'>
                        <Text style={styles.txtWarning}>{convertLanguage(language, 'warning')}</Text>
                        <Text style={styles.txtQuestion}>{convertLanguage(language, 'delete_event_question')}</Text>
                        <View style={styles.boxPassword}>
                            <Text style={styles.txtPhone}>{convertLanguage(language, 'password')}</Text>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                value={this.state.Password}
                                onChangeText={(Password) => this.setState({ Password })}
                                secureTextEntry={true}
                            />
                        </View>
                        <Touchable
                            style={{ marginTop: 0, alignSelf: 'flex-end' }}
                            onPress={() => this.props.navigation.navigate('ForgotPw')}>
                            <Text
                                style={{ textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: Colors.PRIMARY }}>
                                {convertLanguage(language, 'forgot_password')}?</Text>
                        </Touchable>
                        <Touchable style={[styles.btnConfirm, { backgroundColor: this.state.Password.length < 6 ? '#e8e8e8' : '#ff4081' }]} onPress={() => this.onDeleteEvent()}>
                            <Text style={[styles.txtConfirm, { color: this.state.Password.length < 6 ? '#bdbdbd' : '#FFFFFF' }]}>{convertLanguage(language, 'confirm')}</Text>
                        </Touchable>
                    </ScrollView>
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    txtWarning: {
        textAlign: 'center',
        fontSize: 26,
        color: '#ff4081',
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 20
    },
    txtQuestion: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 45
    },
    boxInfo: {
        marginTop: 25,
        marginBottom: 25
    },
    txtInfo: {
        fontSize: 15,
        color: '#333333',
        marginBottom: 5
    },
    ipMessage: {
        height: 160,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        fontSize: 13,
        color: '#333333',
        textAlignVertical: 'top',
        padding: 10,
        marginBottom: 20
    },
    boxPassword: {
        marginBottom: 15
    },
    txtPhone: {
        fontSize: 13,
        color: '#757575'
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 8,
        paddingTop: 8,
        fontWeight: 'bold'
    },
    btnConfirm: {
        alignSelf: 'center',
        width: 160,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        marginTop: 20
    },
    txtConfirm: {
        fontSize: 17,
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        event: state.event
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onDeleteEvent: (id, body, navigation) => {
            dispatch(actDeleteEvent(id, body, navigation))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DeleteEvent);
// export default EventHostList;
