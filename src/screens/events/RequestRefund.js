import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, ScrollView, StyleSheet, Alert, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import Popover from 'react-native-popover-view';
import SrcView from '../../screens_view/ScrView';
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { formatNumber, convertLanguage } from '../../services/Helper';
import * as Colors from '../../constants/Colors';
import ModalPageWebView from '../../components/ModalPageWebView';
import ModalRefundInfo from '../../components/ticket/ModalRefundInfo';
import ModalDataRefundInfo from '../../components/ticket/ModalDataRefundInfo';
import { connect } from "react-redux";
import Loading from '../../screens_view/Loading';
import { TextField } from '../../components/react-native-material-textfield';
import * as Config from '../../constants/Config';
import { actLoadDataHistoryRefund, actRefund, actChangeComment, actCancelRefund, actOpenModalRefundInfo, actCloseModalRefundInfo, actSendCommentRefund, actAcceptRefund } from '../../actions/ticket'
class RequestRefund extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Zipcode: '+82',
            Comment: '',
            modalRefundInfo: false,
            Phone: '',
            modalDataRefundInfo: false,
            CountryCode: 'VN',
            isVisible: false,
            modalPageWebView: false,
            uri: '',
            title: ''
        };
    }

    componentDidMount() {
        var { ticketDetailId, indexTicket } = this.props.route.params;
        var { iso_code } = this.props.global;
        this.props.onLoadDataHistoryRefund(ticketDetailId, iso_code)
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.ticket.itemRefund.refund && nextProps.ticket.itemRefund.refund.Phone && nextProps.ticket.itemRefund.refund.Phone != nextProps.ticket.itemRefund.user.Phone) {
            this.setState({ Phone: nextProps.ticket.itemRefund.refund.Phone.toString() })
        } else {
            this.setState({ Phone: nextProps.ticket.itemRefund.user.Phone ? nextProps.ticket.itemRefund.user.Phone.toString() : this.state.Phone })
        }
        if (nextProps.ticket.itemRefund.refund && nextProps.ticket.itemRefund.refund.Zipcode && nextProps.ticket.itemRefund.refund.Zipcode != nextProps.ticket.itemRefund.user.Zipcode) {
            this.setState({ Zipcode: nextProps.ticket.itemRefund.refund.Zipcode.toString() })
        } else {
            this.setState({ Zipcode: nextProps.ticket.itemRefund.user.Zipcode ? nextProps.ticket.itemRefund.user.Zipcode.toString() : this.state.Zipcode })
        }
    }

    onSendCommentRefund() {
        var { Id } = this.props.ticket.itemRefund.ticketDetail;
        var { Comment } = this.props.ticket;
        var { iso_code } = this.props.global;
        var body = {
            TicketDetailId: Id,
            Comment: Comment,
            Phone: this.state.Phone,
            Zipcode: this.state.Zipcode,
            iso_code
        }
        this.props.onSendCommentRefund(body, Id)
    }

    onRefund() {
        var { Id, Status } = this.props.ticket.itemRefund.ticketDetail;
        var { indexTicket, type } = this.props.route.params;
        var { Comment } = this.props.ticket;
        var { iso_code } = this.props.global;
        var body = {
            TicketDetailId: Id,
            Comment: Comment.replace(/\s/g, '').length === 0 ? 'I want to get a refund' : Comment,
            Phone: this.state.Phone,
            Zipcode: this.state.Zipcode,
            iso_code: iso_code
        }
        this.props.onRefund(Status, body, Id, indexTicket, type)
    }

    onCancelRefund(ticketDetailId) {
        var { language } = this.props.language;
        var { eventSlug, indexTicket, type } = this.props.route.params;
        var body = {
            eventSlug: eventSlug,
            TicketDetailId: ticketDetailId,
        }
        Alert.alert(
            convertLanguage(language, 'cancel_request'),
            convertLanguage(language, 'are_you_sure_cancel_request'),
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.props.onCancelRefund(ticketDetailId, body, indexTicket, type) },
            ],
            { cancelable: false },
        );
    }

    validate() {
        var { itemRefund } = this.props.ticket;
        var { Phone } = this.state;
        if (!Phone || (Phone && '' + Phone.replace(/\s/g, '').length === 0)) {
            return false;
        }
        if (![0, -2, -3, -4].includes(itemRefund.ticketDetail.Status)) {
            return false;
        }
        return true;
    }

    validateComment() {
        var { Comment } = this.props.ticket;
        var { Phone } = this.state;
        if (!Phone || (Phone && '' + Phone.replace(/\s/g, '').length === 0)) {
            return false;
        }
        if (Comment.replace(/\s/g, '').length === 0) {
            return false;
        }
        return true;
    }

    onAcceptRefund(ticketDetailId) {
        var { language } = this.props.language;
        var { eventSlug, indexTicket, type } = this.props.route.params;
        var { iso_code } = this.props.global;
        var body = {
            Slug: eventSlug,
            TicketDetailId: ticketDetailId,
            iso_code
        }
        Alert.alert(
            convertLanguage(language, 'confirm_refund'),
            convertLanguage(language, 'do_you_want_confirm_refund'),
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.props.onAcceptRefund(ticketDetailId, body, indexTicket, type) },
            ],
            { cancelable: false },
        );
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    render() {
        var { language } = this.props.language;
        var { loadingRefund, itemRefund, loadingPostRefund, loadingCancelRefund, loadingPostComment, loadingAcceptRefund } = this.props.ticket;
        var { indexTicket } = this.props.route.params;
        var { Zipcode } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', paddingHorizontal: 16 }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'request_a_refund')}</Text>
                    </Touchable>
                    <Touchable onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/refund-policy`, title: convertLanguage(language, 'refund_policy') })} style={{ paddingHorizontal: 16 }}>
                        <Text style={styles.txtRefundPolicy}>{convertLanguage(language, 'refund_policy')}...</Text>
                    </Touchable>
                </View>

                <Line />
                <>
                    {
                        loadingCancelRefund &&
                        <Loading />
                    }
                    {
                        loadingRefund ?
                            <ActivityIndicator size="large" color="#000000" style={styles.loading} />
                            :
                            <KeyboardAvoidingView
                                behavior={Platform.OS === "ios" ? "padding" : null}
                                style={{ flex: 1 }}>
                                <ScrollView style={{ flex: 1 }} containerStyle={{ paddingBottom: 20 }} keyboardShouldPersistTaps='handled' ref={ref => this.scrollView = ref}>
                                    <View style={styles.content}>
                                        <View style={styles.boxTicket}>
                                            <View style={[styles.boxTicketType, { marginBottom: 15, }]}>
                                                <Image source={require('../../assets/ticket_active.png')} style={styles.icon} />
                                                <Text style={styles.txtTicketName}>{itemRefund.ticket.Name}</Text>
                                            </View>
                                            <View style={[styles.boxTicketType, { justifyContent: 'space-between', marginBottom: 25 }]}>
                                                <Text style={styles.txtTicketPrice}>{formatNumber(itemRefund.ticket.Price)} {itemRefund.ticket.Unit && itemRefund.ticket.Unit.toUpperCase()}</Text>
                                                <View style={styles.boxTicketType}>
                                                    {
                                                        itemRefund.ticket.Type === 'd-p-ticket'
                                                            ?
                                                            <Image source={require('../../assets/ic_delivery_brown.png')} style={styles.ic_delivery} />
                                                            :
                                                            <Image source={require('../../assets/ic_eticket.png')} style={styles.ic_eticket} />
                                                    }
                                                    <Text style={styles.txtDelivery}>{itemRefund.ticket.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket') : convertLanguage(language, 'e_ticket')}</Text>
                                                </View>
                                            </View>
                                            {
                                                itemRefund.ticket.Description != ''
                                                &&
                                                <Text style={styles.txtTicketDes}>{itemRefund.ticket.Description}</Text>
                                            }
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingTop: 16, borderBottomWidth: 1, borderBottomColor: '#BDBDBD', paddingBottom: 32 }}>
                                            <TouchableOpacity disabled={![1, -6, -2, 4, 5, 6, -7].includes(itemRefund.ticketDetail.Status) ? false : true} onPress={() => this.showPopover()}
                                                ref={ref => this.touchable = ref}
                                                style={{
                                                    marginRight: 10,
                                                    height: 57,
                                                    borderRadius: 4,
                                                    paddingHorizontal: 12,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    backgroundColor: '#F2F2F2',
                                                    width: 130
                                                }}
                                            >
                                                <View style={{ justifyContent: 'space-between', }}>
                                                    <Text style={{ color: '#828282', fontSize: 12, }}>{convertLanguage(language, 'country')}</Text>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={Zipcode === '+84' ? require('../../assets/flag_vn.png') : Zipcode === '+66' ? require('../../assets/flag_th.png') : require('../../assets/flag_kr.png')} style={styles.ic_country} />
                                                        <Text style={styles.txtCountry}>{Zipcode}</Text>
                                                    </View>
                                                </View>
                                                <Image source={require('../../assets/down_arrow_active.png')} style={{
                                                    width: 13,
                                                    height: 11,
                                                }} />
                                            </TouchableOpacity>
                                            <TextField
                                                disabled={(itemRefund.ticketDetail.Status != 1 && itemRefund.ticketDetail.Status != -6 && itemRefund.ticketDetail.Status != -2 && itemRefund.ticketDetail.Status != 4 && itemRefund.ticketDetail.Status != 5 && itemRefund.ticketDetail.Status != 6 && itemRefund.ticketDetail.Status != -7) ? false : true}
                                                autoCorrect={false}
                                                enablesReturnKeyAutomatically={true}
                                                value={this.state.Phone}
                                                onChangeText={(Phone) => this.setState({ Phone: Phone == 0 ? '' : Phone, isChange: true })}
                                                // error={errors.Phone}
                                                disabledLineWidth={0}
                                                returnKeyType='next'
                                                label={convertLanguage(language, 'phone')}
                                                baseColor={this.state.Phone.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                                tintColor={'#828282'}
                                                errorColor={'#EB5757'}
                                                inputContainerStyle={[styles.inputContainerStyle]}
                                                containerStyle={[styles.containerStyle, { paddingHorizontal: 0 }]}
                                                labelHeight={25}
                                                labelTextStyle={{ paddingBottom: 15 }}
                                                lineWidth={2}
                                                selectionColor={'#3a3a3a'}
                                                style={styles.input}
                                                labelTextStyle={{ paddingLeft: 12 }}
                                                errorImage={require('../../assets/error.png')}
                                                keyboardType="numeric"
                                            />
                                        </View>
                                        {
                                            itemRefund.refundComments.length > 0 &&
                                            <View style={styles.boxChat}>
                                                {
                                                    itemRefund.refundComments.map((item, index) => {
                                                        if (item.Role == "guest") {
                                                            if (item.Status == "-1") {
                                                                return <View style={[styles.boxOtherChat, { alignSelf: 'center' }]} key={index}>
                                                                    <View style={[styles.boxContentOtherChat, { alignItems: 'center' }]}>
                                                                        {
                                                                            itemRefund.eventPoster &&
                                                                            <Image source={{ uri: itemRefund.eventPoster.Small }} style={styles.avatarEvent} />
                                                                        }
                                                                        <Text style={[styles.txtCancelRefund, { marginRight: 48 }]}>{convertLanguage(language, 'you_have_canceled_the_refund_request')}</Text>
                                                                    </View>
                                                                    <Text style={[styles.txtTimeCancel, { textAlign: 'right' }]}>{item.CreatedAt}</Text>
                                                                </View>
                                                            } else {
                                                                return <View style={styles.boxMyChat} key={index}>
                                                                    <View style={styles.boxContentMyChat}>
                                                                        <Text style={styles.txtChat}>{item.Comment}</Text>
                                                                        {
                                                                            itemRefund.user && itemRefund.user.Avatars &&
                                                                            <Image source={{ uri: itemRefund.user.Avatars.Small }} style={styles.avatar} />
                                                                        }
                                                                    </View>
                                                                    <Text style={styles.txtTime}>{item.CreatedAt}</Text>
                                                                </View>
                                                            }
                                                        } else {
                                                            if (item.Status == "-2") {
                                                                return <View style={[styles.boxOtherChat, { alignSelf: 'center', paddingRight: 8 }]} key={index}>
                                                                    <View style={styles.boxContentOtherChat}>
                                                                        <Image source={{ uri: itemRefund.eventPoster ? itemRefund.eventPoster.Small : '' }} style={styles.avatarEvent} />
                                                                        <Text style={[styles.txtCancelRefund, { marginRight: 48 }]}>{item.Comment}</Text>
                                                                    </View>
                                                                    <Text style={[styles.txtTimeCancel, { textAlign: 'right' }]}>{item.CreatedAt}</Text>
                                                                </View>
                                                            } else {
                                                                return <View style={styles.boxOtherChat} key={index}>
                                                                    <View style={styles.boxContentOtherChat}>
                                                                        <Image source={{ uri: itemRefund.eventPoster ? itemRefund.eventPoster.Small : '' }} style={styles.avatarEvent} />
                                                                        <Text style={[styles.txtChat, { color: '#333333' }]}>{item.Comment}</Text>
                                                                    </View>
                                                                    <Text style={styles.txtTime}>{item.CreatedAt}</Text>
                                                                </View>
                                                            }
                                                        }
                                                    })
                                                }
                                            </View>
                                        }
                                        {
                                            (itemRefund.ticketDetail.Status == -2 || itemRefund.refund.Rate) &&
                                            <View style={styles.boxAccess}>
                                                <Text style={styles.txtRequestAccepted}>{convertLanguage(language, 'time_accepted_refund')} {itemRefund.ticketDetail.CreatedAtRefund}</Text>
                                                <Text style={[styles.txtAcceptedTitle, { color: '#4F4F4F' }]}>{itemRefund.ticketDetail.Team && itemRefund.ticketDetail.Team.Name.toUpperCase()} {convertLanguage(language, 'team_accepted_refund')} <Text style={{ color: '#00A9F4' }}>{itemRefund.refund.Rate}%</Text> {convertLanguage(language, 'total_bill')}</Text>
                                                <Text style={styles.txtAcceptedLable}>{convertLanguage(language, 'some_fee')} <Text style={{ color: '#00A9F4' }}>0 {itemRefund.ticketDetail.Ticket && itemRefund.ticketDetail.Ticket.Unit.toUpperCase()}</Text></Text>
                                                <Text style={styles.txtAcceptedLable}>{convertLanguage(language, 'total_amount_get')} <Text style={{ color: '#00A9F4' }}>{formatNumber(itemRefund.refund.PriceRefund)} {itemRefund.ticketDetail.Ticket && itemRefund.ticketDetail.Ticket.Unit.toUpperCase()}</Text></Text>
                                                {
                                                    itemRefund.ticketDetail.Status == -2 &&
                                                    <Text style={[styles.txtAcceptedTitle, { textAlign: 'center', marginBottom: 16, marginTop: 5, fontSize: 14 }]}>
                                                        {
                                                            itemRefund.ticketDetail.Payment.Type === 2 ?
                                                                convertLanguage(language, 'status_refund_1')
                                                                :
                                                                convertLanguage(language, 'status_refund_3')
                                                        }
                                                    </Text>

                                                }
                                                {
                                                    [-6, -7].includes(itemRefund.ticketDetail.Status) &&
                                                    <Text style={[styles.txtAcceptedTitle, { textAlign: 'center', marginBottom: 16, marginTop: 5, fontSize: 14 }]}>
                                                        {
                                                            itemRefund.ticketDetail.Payment.Type === 2 ?
                                                                convertLanguage(language, 'status_refund_2')
                                                                :
                                                                convertLanguage(language, 'status_refund_4')
                                                        }
                                                    </Text>
                                                }
                                                {
                                                    [5].includes(itemRefund.ticketDetail.Status) &&
                                                    <Text style={[styles.txtAcceptedTitle, { textAlign: 'center', marginBottom: 16, marginTop: 5, fontSize: 14 }]}>
                                                        {convertLanguage(language, 'refund_error')}
                                                    </Text>
                                                }
                                                {
                                                    [6].includes(itemRefund.ticketDetail.Status) &&
                                                    <Text style={[styles.txtAcceptedTitle, { textAlign: 'center', marginBottom: 16, marginTop: 5, fontSize: 14 }]}>
                                                        {convertLanguage(language, 'refund_succeed')}
                                                    </Text>
                                                }
                                                {
                                                    ![-2, -6, -7, 5, 6, -3].includes(itemRefund.ticketDetail.Status) &&
                                                    <Text style={[styles.txtAcceptedTitle, { textAlign: 'center', marginBottom: 16, marginTop: 5, fontSize: 14 }]}>
                                                        {convertLanguage(language, 'denie_refund1')}{itemRefund.ticketDetail.Status != 0 ? ('\n\n' + convertLanguage(language, 'status_refund_2') + itemRefund.ticketDetail.UpdateAtRefund) : ''}
                                                    </Text>
                                                }
                                                {
                                                    [-3].includes(itemRefund.ticketDetail.Status) &&
                                                    <Text style={[styles.txtAcceptedTitle, { textAlign: 'center', marginBottom: 16, marginTop: 5, fontSize: 14 }]}>
                                                        {convertLanguage(language, 'the_host_refuse')}
                                                    </Text>
                                                }
                                                {
                                                    itemRefund.ticketDetail.Status == -2 ?
                                                        itemRefund.ticketDetail.Payment.Type === 2 ?
                                                            <Touchable style={styles.btnAccept} onPress={() => this.props.onOpenModalRefundInfo()}>
                                                                <Text style={styles.txtAccepted}>{convertLanguage(language, 'accept_proceed_refund')}</Text>
                                                                <Image source={require('../../assets/arrow_right.png')} style={styles.arrow_right} />
                                                            </Touchable>
                                                            :
                                                            <Touchable style={styles.btnAccept} onPress={() => this.onAcceptRefund(itemRefund.ticketDetail.Id)}>
                                                                {
                                                                    loadingAcceptRefund
                                                                        ?
                                                                        <Text style={styles.txtAccepted}>{convertLanguage(language, 'loading')}...</Text>
                                                                        :
                                                                        <>
                                                                            <Text style={styles.txtAccepted}>{convertLanguage(language, 'accept_proceed_refund')}</Text>
                                                                            <Image source={require('../../assets/arrow_right.png')} style={styles.arrow_right} />
                                                                        </>
                                                                }
                                                            </Touchable>
                                                        : null
                                                }
                                            </View>
                                        }
                                        {
                                            ![-6, 6, 5, -7].includes(itemRefund.ticketDetail.Status) &&
                                            <>
                                                {
                                                    itemRefund.ticketDetail.Status != 0 && itemRefund.ticketDetail.CheckRefund &&
                                                    <View style={styles.boxInput}>
                                                        <TextInput
                                                            style={styles.ipMessage}
                                                            selectionColor="#4F4F4F"
                                                            name="Comment"
                                                            placeholder={convertLanguage(language, 'write_a_message')}
                                                            multiline={true}
                                                            value={this.props.ticket.Comment}
                                                            onChangeText={(Comment) => this.props.onChangeComment(Comment)}
                                                            onFocus={() => this.scrollView.scrollToEnd({ animated: true })}
                                                            editable={!loadingPostComment}
                                                        />
                                                        <Touchable style={[styles.btnSend, { backgroundColor: !this.validateComment() ? '#E0E2E8' : '#00A9F4' }]} onPress={() => this.onSendCommentRefund()} disabled={!this.validateComment() || loadingPostComment}>
                                                            <Text style={[styles.txtSend, { color: !this.validateComment() ? '#bdbdbd' : '#FFFFFF' }]}>{loadingPostComment ? convertLanguage(language, 'loading') + '...' : convertLanguage(language, 'send')}</Text>
                                                        </Touchable>
                                                    </View>
                                                }
                                                <View style={styles.boxInfo}>
                                                    <Text style={styles.txtInfo}>{convertLanguage(language, 'text_in_refund1')}</Text>
                                                    <Text style={styles.txtInfo}>{convertLanguage(language, 'text_in_refund2')}</Text>
                                                </View>
                                                <View style={styles.boxAction}>
                                                    {
                                                        !(itemRefund.ticketDetail.CheckRefund && itemRefund.ticketDetail.Status != 0) &&
                                                        <View style={{ flex: 0.45 }}></View>
                                                    }
                                                    <Touchable onPress={() => this.onRefund()} disabled={!this.validate() || loadingPostRefund} style={[styles.btnRequestRefund, { backgroundColor: !this.validate() ? '#e8e8e8' : 'transparent', borderColor: !this.validate() ? '#e8e8e8' : '#4F4F4F' }]}>
                                                        <Text style={[styles.txtRequestRefund, { color: !this.validate() ? '#bdbdbd' : '#4F4F4F' }]}>{loadingPostRefund ? convertLanguage(language, 'loading') + '...' : itemRefund.ticketDetail.Status === -2 ? convertLanguage(language, 'request_again') : convertLanguage(language, 'request_refund')}</Text>
                                                    </Touchable>
                                                    {
                                                        (itemRefund.ticketDetail.CheckRefund && itemRefund.ticketDetail.Status != 0) &&
                                                        <Touchable style={[styles.btnCancel, [-1, -2].includes(itemRefund.ticketDetail.Status) ? {} : { backgroundColor: '#e8e8e8', borderWidth: 0 }]} disabled={loadingPostRefund || ![-1, -2].includes(itemRefund.ticketDetail.Status)} onPress={() => this.onCancelRefund(itemRefund.ticketDetail.Id)}>
                                                            <Text style={[styles.txtCancel, [-1, -2].includes(itemRefund.ticketDetail.Status) ? {} : { color: '#bdbdbd' }]}>{convertLanguage(language, 'cancel_this_request')}</Text>
                                                        </Touchable>
                                                    }
                                                </View>
                                            </>
                                        }
                                        {
                                            [-6, -7].includes(itemRefund.ticketDetail.Status) &&
                                            <Touchable style={styles.btnReview} onPress={() => this.setState({ modalDataRefundInfo: true })}>
                                                <Text style={styles.txtReview}>{convertLanguage(language, 'review_refund')}</Text>
                                            </Touchable>
                                        }
                                    </View>
                                </ScrollView>
                            </KeyboardAvoidingView>
                    }
                </>
                {
                    this.state.modalPageWebView
                        ?
                        <ModalPageWebView
                            closeModal={() => this.setState({ modalPageWebView: false })}
                            navigation={this.props.navigation}
                            uri={this.state.uri}
                            title={this.state.title}
                        />
                        : null
                }
                {
                    this.props.ticket.modalRefundInfo
                        ?
                        <ModalRefundInfo
                            modalVisible={this.props.ticket.modalRefundInfo}
                            closeModal={() => this.props.onCloseModalRefundInfo()}
                            navigation={this.props.navigation}
                            itemRefund={itemRefund}
                            indexTicket={indexTicket}
                        />
                        : null
                }
                {
                    this.state.modalDataRefundInfo
                        ?
                        <ModalDataRefundInfo
                            onOpenModalRefundInfo={() => { this.setState({ modalDataRefundInfo: false }); this.props.onOpenModalRefundInfo() }}
                            modalVisible={this.state.modalDataRefundInfo}
                            closeModal={() => this.setState({ modalDataRefundInfo: false })}
                            data={itemRefund}
                        />
                        : null
                }
                <Popover
                    isVisible={this.state.isVisible}
                    animationConfig={{ duration: 0 }}
                    fromView={this.touchable}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => this.closePopover()}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+82' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_kr.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+82</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+84' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_vn.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+84</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+66' }) }} style={styles.boxCountry}>
                            <Image source={require('../../assets/flag_th.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+66</Text>
                        </Touchable>
                    </View>
                </Popover>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20
    },
    boxTicket: {
        paddingTop: 25,
        borderBottomColor: 'rgba(51, 51, 51, 0.25)',
        borderBottomWidth: 1
    },
    txtTicketName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    txtTicketDes: {
        fontSize: 13,
        color: '#757575',
        marginBottom: 20,
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxTicketType: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ic_delivery: {
        width: 15,
        height: 15,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDelivery: {
        fontSize: 12,
        color: '#4F4F4F'
    },
    boxInput: {
        flexDirection: 'row',
        paddingTop: 16,
    },
    boxChat: {
        marginTop: 16,
        paddingBottom: 32,
        borderBottomColor: '#BDBDBD',
        borderBottomWidth: 1
    },
    boxMyChat: {
        // flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        marginTop: 10,
    },
    txtTime: {
        color: '#757575',
        fontSize: 12,
        marginTop: 5,
        marginBottom: 2,
        // marginLeft: 5,
    },
    boxContentMyChat: {
        // maxWidth: 245,
        padding: 10,
        backgroundColor: '#F2F2F2',
        borderRadius: 4,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    txtChat: {
        fontSize: 14,
        color: '#333333',
        maxWidth: 200
        // flex: 1
    },
    boxOtherChat: {
        // flexDirection: 'row',
        // alignItems: 'flex-end',
        alignSelf: 'flex-start',
        marginTop: 10
    },
    boxContentOtherChat: {
        maxWidth: 245,
        padding: 10,
        backgroundColor: '#f2f2f2',
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center'
    },
    ipMessage: {
        height: 76,
        fontSize: 13,
        color: '#4F4F4F',
        textAlignVertical: 'top',
        padding: 10,
        marginBottom: 30,
        flex: 1,
        backgroundColor: '#F2F2F2',
        borderRadius: 5
    },
    boxInfo: {
        marginBottom: 20,
        alignItems: 'center',
        marginTop: 16
    },
    txtInfo: {
        fontSize: 14,
        color: '#333333',
        marginBottom: 3
    },
    boxAction: {
        marginBottom: 20,
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btnCancel: {
        flex: 0.45,
        alignItems: 'center',
        justifyContent: 'center',
        height: 42,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#4F4F4F'
    },
    txtCancel: {
        color: '#4F4F4F',
        fontSize: 16,
    },
    btnRequestRefund: {
        flex: 0.45,
        alignItems: 'center',
        justifyContent: 'center',
        height: 42,
        borderRadius: 5,
        borderWidth: 1
    },
    txtRequestRefund: {
        color: '#bdbdbd',
        fontSize: 16,
    },
    loading: {
        padding: 20
    },
    boxStatus: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 10
    },
    txtCancelRefund: {
        fontSize: 14,
        color: 'red',
        marginRight: 5
    },
    txtTimeCancel: {
        color: '#EB5757',
        fontSize: 12,
    },
    avatar: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: '#bdbdbd',
        marginLeft: 5
    },
    avatarEvent: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: '#bdbdbd',
        marginRight: 5
    },
    boxAccess: {
        marginTop: 16,
        marginBottom: 24,
        borderBottomColor: '#bdbdbd',
        borderBottomWidth: 1
    },
    txtRequestAccepted: {
        textAlign: 'center',
        fontSize: 14,
        color: '#00A9F4',
        marginBottom: 16
    },
    txtAcceptedTitle: {
        fontSize: 16,
        // fontWeight: 'bold',
        color: '#00A9F4'
    },
    txtAcceptedLable: {
        fontSize: 16,
        color: '#4F4F4F',
        fontWeight: 'bold'
    },
    btnAccept: {
        // width: '100%',
        backgroundColor: '#00A9F4',
        borderRadius: 4,
        // alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 16,
        height: 36
    },
    txtAccepted: {
        fontSize: 16,
        color: '#FFFFFF',
    },
    arrow_right: {
        width: 18,
        height: 18,
        marginLeft: 10
    },
    btnReview: {
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 4,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 16
    },
    txtReview: {
        fontSize: 16,
        color: '#333333'
    },
    inputContainerStyle: {
        // borderWidth: 1,
        // borderColor: '#bdbdbd',
        borderRadius: 4,
        paddingHorizontal: 12,
        backgroundColor: '#F2F2F2',
        flex: 1,
    },
    containerStyle: {
        paddingHorizontal: 20,
        flex: 1
    },
    input: {
        color: '#333333',
    },
    icon: {
        width: 30,
        height: 30,
        marginRight: 7
    },
    txtRefundPolicy: {
        color: '#00A9F4',
        fontSize: 16
    },
    btnSend: {
        backgroundColor: '#00A9F4',
        height: 36,
        borderRadius: 5,
        marginLeft: 16,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 18
    },
    txtSend: {
        fontSize: 16,
        color: '#FFFFFF'
    },
    boxPopover: {
        width: 130,
        padding: 12,
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 12
    },
    txtCountry: {
        color: '#333333',
        fontSize: 18
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        ticket: state.ticket,
        user: state.user,
        global: state.global
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataHistoryRefund: (id, iso_code) => {
            dispatch(actLoadDataHistoryRefund(id, iso_code))
        },
        onRefund: (Status, body, Id, indexTicket, type) => {
            dispatch(actRefund(Status, body, Id, indexTicket, type))
        },
        onChangeComment: (Comment) => {
            dispatch(actChangeComment(Comment))
        },
        onCancelRefund: (Id, body, indexTicket, type) => {
            dispatch(actCancelRefund(Id, body, indexTicket, type))
        },
        onOpenModalRefundInfo: () => {
            dispatch(actOpenModalRefundInfo())
        },
        onCloseModalRefundInfo: () => {
            dispatch(actCloseModalRefundInfo())
        },
        onSendCommentRefund: (body, Id) => {
            dispatch(actSendCommentRefund(body, Id))
        },
        onAcceptRefund: (ticketDetailId, body, indexTicket, type) => {
            dispatch(actAcceptRefund(ticketDetailId, body, indexTicket, type))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(RequestRefund);
