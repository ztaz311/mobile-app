import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SimpleToast from 'react-native-simple-toast'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux"
import SrcView from '../../screens_view/ScrView'
import { actLoadDataHostInfo } from '../../actions/event'
import { convertLanguage } from '../../services/Helper'
import { getTag } from '../../services/Helper';
import FastImage from 'react-native-fast-image';
class EventHostList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            HostInfoId: '',
        };
    }

    componentDidMount() {
        this.props.onLoadDataHostInfo();
    }

    selectHostTeam() {
        var { HostInfoId } = this.state;
        if (HostInfoId === '') {
            SimpleToast.showWithGravity('Please select a host team!', SimpleToast.SHORT, SimpleToast.BOTTOM)
        } else {
            this.props.navigation.replace('CreateEvent', { HostInfoId: HostInfoId })
        }
    }

    render() {
        var { loading, hostsInfo } = this.props.event;
        var { HostInfoId } = this.state;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <View style={{ width: 50 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    {
                        loading ?
                            <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                            :
                            <React.Fragment>
                                <SrcView contentContainerStyle={styles.srcView}>
                                    <View style={styles.boxBanner}>
                                        <Text style={styles.txtBanner}>{convertLanguage(language, 'select_host_team')}</Text>
                                    </View>
                                    <View style={styles.boxList}>
                                        {
                                            hostsInfo.map((item, index) => {
                                                return <Touchable style={styles.boxItemList} key={index} onPress={() => this.props.navigation.replace('CreateEventStep1', { HostInfoId: item.Id })}>
                                                    <View style={styles.boxInfo}>
                                                        <FastImage source={{ uri: item.Logos.Medium }} style={[styles.image_thumb, { borderColor: HostInfoId === item.Id ? '#03a9f4' : '#d7d7d7', borderWidth: HostInfoId === item.Id ? 1 : 0, padding: 1 }]} />
                                                        <View style={styles.boxContent}>
                                                            <Text style={styles.txtName} numberOfLines={2}>{item.Name}</Text>
                                                            <Text style={styles.txtId} numberOfLines={1}>({item.Code})</Text>
                                                            <Text numberOfLines={2} style={styles.txtTag}>{getTag(item.HashTag)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.boxRadio}>
                                                        <Image source={require('../../assets/right.png')} style={styles.ic_radio} />
                                                    </View>
                                                </Touchable>
                                            })
                                        }
                                        <Touchable style={styles.boxItemList} onPress={() => this.props.navigation.navigate('RegisterTeam', { type: 'event' })}>
                                            <View style={styles.boxInfo}>
                                                <Image source={require('../../assets/plus_circle.png')} style={styles.image_thumb} />
                                                <View style={styles.boxContent}>
                                                    <Text style={styles.txtAddNew}>{convertLanguage(language, 'create_new_team')}</Text>
                                                </View>
                                            </View>
                                        </Touchable>
                                    </View>
                                </SrcView>
                                {/* <View style={styles.boxAction}>
                                <Touchable style={styles.btnNext} onPress={() => this.selectHostTeam()}>
                                    <Text style={styles.txtNext}>{convertLanguage(language, 'next')}</Text>
                                </Touchable>
                            </View> */}
                            </React.Fragment>
                    }
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    srcView: {
        // flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    content: {
        flex: 1
    },
    boxBanner: {
        paddingTop: 20,
        paddingBottom: 30,
        alignItems: 'center'
    },
    txtBanner: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333333'
    },
    boxList: {
        flex: 1
    },
    boxItemList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 15
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    image_thumb: {
        width: 70,
        height: 70,
        borderRadius: 35,
        // borderWidth: 2,
        marginRight: 15
    },
    ic_plus: {
        width: 70,
        height: 70,
        marginRight: 15
    },
    boxContent: {
        flex: 1
    },
    txtName: {
        fontSize: 16,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtId: {
        fontSize: 14,
        color: '#828282',
        // fontWeight: 'bold'
    },
    txtAddNew: {
        fontSize: 18,
        color: '#212529'
    },
    boxRadio: {
        // padding: 5
    },
    ic_radio: {
        width: 24,
        height: 24
    },
    boxAction: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        // paddingBottom: 20
    },
    btnPrevios: {
        flex: 0.45,
        height: 48,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtPrevios: {
        color: '#333333',
        fontSize: 17,
        fontWeight: 'bold'
    },
    btnNext: {
        flex: 1,
        height: 48,
        borderRadius: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#03a9f4'
    },
    txtNext: {
        color: '#FFFFFF',
        fontSize: 17,
        fontWeight: 'bold'
    },
    txtTag: {
        fontSize: 14,
        color: '#333333',
        marginTop: 5
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataHostInfo: () => {
            dispatch(actLoadDataHostInfo())
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventHostList);
// export default EventHostList;
