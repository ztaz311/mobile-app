import React, { Component } from 'react';
import { PermissionsAndroid, View, Text, Image, ScrollView, StyleSheet, Platform, Keyboard, ActivityIndicator } from 'react-native';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Config from '../../constants/Config';

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import MapView, { PROVIDER_GOOGLE, ProviderPropType } from 'react-native-maps';
import flagPinkImg from '../../assets/pin_1_icon_active.png';
import Autocomplete from "react-native-autocomplete-input";
import Toast from 'react-native-simple-toast';
import Geolocation from '@react-native-community/geolocation';
import { convertLanguage } from '../../services/Helper'
const ASPECT_RATIO = 2;
const LATITUDE_DELTA = 0.08;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class LocationSetting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Lat: this.props.route.params['Lat'],
            Long: this.props.route.params['Long'],
            Address: this.props.route.params['Address'],
            VenueName: this.props.route.params['VenueName'],
            VenueId: this.props.route.params['VenueId'],
            AreaId: this.props.route.params['AreaId'],
            AreaName: this.props.route.params['AreaName'],
            Unit: this.props.route.params['Unit'],
            hideResults: true,
            predictions: [],
            isEdit: false,
            showButtonMyLocation: true,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            loadingLocation: false,
            editAddress: false,
            showPink: this.props.route.params['Lat'] ? true : false,
            CountryCode: ''
        };
    }

    componentDidMount() {
        Geolocation.setRNConfiguration({ 'skipPermissionRequests': true });
        Geolocation.getCurrentPosition((position) => {
            this.setState({
                showButtonMyLocation: true
            })
        },
            (error) => {
                this.setState({
                    showButtonMyLocation: false,
                })
            })
    }

    randomColor() {
        return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
    }

    setLocation(location) {
        this.setState({
            Lat: location.Lat,
            Long: location.Long,
            Address: location.Address,
            VenueName: location.Name,
            AreaId: location.AreaId,
            AreaName: location.AreaName,
            editAddress: false,
            showPink: true
        })
        this.loadDataUnit(location.AreaId)
    }

    loadDataUnit(AreaId) {
        fetch(Config.API_URL + '/area/' + AreaId + '/find-country', {
        }).then((response) => {
            return response.json()
        }).then((response) => {
            if (response.status == 200) {
                this.setState({ Unit: response.data.Unit, CountryCode: response.data.Code })
            }
        })
    }

    setVenue(type, venue) {
        if (type === 1) {
            this.setState({
                Lat: parseFloat(venue.Lat),
                Long: parseFloat(venue.Long),
                Address: venue.Address,
                VenueName: venue.Name,
                VenueId: venue.Id,
                AreaId: venue.AreaId,
                AreaName: venue.AreaName,
                editAddress: false,
                showPink: true
            })
            this.region = {
                latitude: parseFloat(venue.Lat),
                longitude: parseFloat(venue.Long),
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta
            }
            setTimeout(() => this.map.animateToRegion(this.region), 10);
        } else {
            this.setState({
                VenueName: venue,
                VenueId: '',
                editAddress: false
            })
        }
        this.loadDataUnit(venue.AreaId)
    }

    async onChangeDestination(Address) {
        var { Lat, Long } = this.state;
        this.setState({
            loadingLocation: Address === '' ? false : true,
            Address,
            hideResults: Address === '' ? true : false,
            editAddress: true
        })
        const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${Config.GOOGLE_API_KEY}&input=${encodeURI(Address)}&types=establishment&strictbounds&radius=12000&components=country:kr|country:vn&location=${Lat},${Long}`;
        try {
            const result = await fetch(apiUrl);
            const json = await result.json();
            this.setState({
                loadingLocation: false,
                predictions: json.predictions,
            })
        } catch (err) {
            console.log(err)
        }
    }

    isCheckValidate() {
        var { AreaId, Address, Lat } = this.state;
        return (AreaId === '' || Lat === '' || Address === '');
    }

    onSaveLocation() {
        var { language } = this.props.language;
        if (this.state.AreaId === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_city'), Toast.SHORT, Toast.BOTTOM)
            return;
        }
        if (this.state.Address === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_address'), Toast.SHORT, Toast.BOTTOM)
            return;
        }
        if (this.state.Lat === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_location'), Toast.SHORT, Toast.BOTTOM)
            return;
        }
        this.props.route.params['callback'](this.state);
        this.props.navigation.goBack()
    }

    async onSelectLocation(data) {
        Keyboard.dismiss();
        this.setState({
            Address: data.item.description,
            hideResults: true,
            showPink: true
        })
        const apiUrl2 = `https://maps.googleapis.com/maps/api/place/details/json?key=${Config.GOOGLE_API_KEY}&placeid=${data.item.place_id}`;
        try {
            const result2 = await fetch(apiUrl2);
            const json2 = await result2.json();
            this.setState({
                Lat: json2.result.geometry.location.lat,
                Long: json2.result.geometry.location.lng,
            })
            this.region = {
                latitude: json2.result.geometry.location.lat,
                longitude: json2.result.geometry.location.lng,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta
            }
            setTimeout(() => this.map.animateToRegion(this.region), 10);
        } catch (err) {
            console.log(err)
        }
    }

    setCity(city) {
        console.log(city)
        if (city.Id) {
            this.setState({
                AreaId: city.Id,
                AreaName: city.Name,
                Lat: city.Lat,
                Long: city.Lng,
            })
            this.region = {
                latitude: city.Lat,
                longitude: city.Lng,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta
            }
            setTimeout(() => this.map.animateToRegion(this.region), 10);
            this.loadDataUnit(city.Id)
        }
    }

    onRegionChange(region) {
        this.setState({
            Lat: region.latitude,
            Long: region.longitude,
            latitudeDelta: region.latitudeDelta,
            longitudeDelta: region.latitudeDelta,
        })
    }

    onEditLocation() {
        dismissKeyboard();
        this.setState({ isEdit: true, showPink: true });
        this.scrollView.scrollToEnd();
    }

    onPressToMyLocation() {
        Geolocation.getCurrentPosition((position) => {
            this.setState({
                Lat: position.coords.latitude,
                Long: position.coords.longitude,
            })
            this.region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta
            }
            setTimeout(() => this.map.animateToRegion(this.region), 10);
        })
    }

    onPressZoomIn() {
        this.region = {
            latitude: parseFloat(this.state.Lat),
            longitude: parseFloat(this.state.Long),
            latitudeDelta: this.state.latitudeDelta / 2,
            longitudeDelta: this.state.longitudeDelta / 2
        }
        this.setState({
            latitudeDelta: this.state.latitudeDelta / 2,
            longitudeDelta: this.state.longitudeDelta / 2
        })
        // this.map.animateToRegion(this.region, 1000);
        setTimeout(() => this.map.animateToRegion(this.region), 10);
    }
    onPressZoomOut() {
        this.region = {
            latitude: parseFloat(this.state.Lat),
            longitude: parseFloat(this.state.Long),
            latitudeDelta: this.state.latitudeDelta * 2,
            longitudeDelta: this.state.longitudeDelta * 2
        }
        this.setState({
            latitudeDelta: this.state.latitudeDelta * 2,
            longitudeDelta: this.state.longitudeDelta * 2
        })
        // this.map.animateToRegion(this.region, 1000);
        setTimeout(() => this.map.animateToRegion(this.region), 10);
    }

    _onMapReady() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    // this.setState({ paddingTop: 0 });
                });
        }
    }

    onFocusAddress() {
        this.scrollView.scrollTo({ x: 0, y: 100, animated: true })
    }

    async onSavePin() {
        var { language } = this.props.language;
        this.setState({ loading: true })
        const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.state.Lat},${this.state.Long}&key=${Config.GOOGLE_API_KEY}&result_type=country`;
        try {
            const result = await fetch(apiUrl);
            const json = await result.json();
            this.setState({ loading: false })
            if (json.results.length > 0 && json.results[0].address_components.length > 0 && json.results[0].address_components[0].short_name === this.state.CountryCode.toUpperCase()) {
                this.setState({ isEdit: false })
            } else {
                alert(convertLanguage(language, 'select_location_fail'))
            }
        } catch (err) {
            this.setState({ isEdit: false, loading: false })
            console.log(err)
        }
    }

    render() {
        var { Lat, Long, Address, VenueName, AreaName, hideResults, predictions, isEdit, showButtonMyLocation, loadingLocation, editAddress, showPink, loading } = this.state;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text ref="address" style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable disabled={this.isCheckValidate()} style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: this.isCheckValidate() ? '#e8e8e8' : '#03a9f4', marginRight: 20 }} onPress={() => this.onSaveLocation()}>
                        <Text style={{ fontSize: 17, color: this.isCheckValidate() ? '#d7d7d7' : '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'save')}</Text>
                    </Touchable>
                </View>

                <Line />
                <ScrollView style={{ flex: 1 }} ref={(ref) => this.scrollView = ref} keyboardShouldPersistTaps='handled'>
                    <View style={styles.content}>
                        {
                            isEdit &&
                            <View style={{ position: 'absolute', backgroundColor: 'rgba(0,0,0,0.6)', left: 0, top: 0, width: '100%', height: '100%', zIndex: 9 }}></View>
                        }
                        <Touchable style={styles.btnPrevios} onPress={() => this.props.navigation.navigate('PreviousLocation', { callback: (location) => this.setLocation(location) })}>
                            <Text style={styles.txtPrevios}>{convertLanguage(language, 'previous_location')}</Text>
                        </Touchable>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'city')}</Text>
                            <Touchable style={styles.boxInputContent} onPress={() => this.props.navigation.navigate('SelectCity', { callback: (city) => this.setCity(city) })}>
                                <Text style={styles.txtContent}>{AreaName}</Text>
                            </Touchable>
                        </View>
                        <View style={styles.boxInput}>
                            <Text style={styles.txtInput}>{convertLanguage(language, 'venue_name')}</Text>
                            <Touchable disabled={AreaName == ''} style={styles.boxInputContent} onPress={() => this.props.navigation.navigate('SelectVenue', { callback: (type, venue) => this.setVenue(type, venue) })}>
                                <Text style={styles.txtContent}>{VenueName}</Text>
                            </Touchable>
                        </View>
                        <Text style={[styles.txtInput, { paddingLeft: 20, paddingRight: 20 }]}>{convertLanguage(language, 'address')}</Text>
                        <Autocomplete
                            setKeyboard={() => this.onFocusAddress()}
                            autoCapitalize="none"
                            editable={AreaName != ''}
                            autoCorrect={false}
                            hideResults={hideResults}
                            containerStyle={{ marginBottom: 24, marginLeft: 20, marginRight: 20 }}
                            inputContainerStyle={[styles.boxInputContent, { borderWidth: 0 }, { paddingTop: 8, paddingBottom: 8 }]}
                            style={[styles.txtContent, { paddingTop: 0, paddingBottom: 0 }]}
                            data={predictions}
                            value={Address}
                            onChangeText={Address => isEdit ? null : this.onChangeDestination(Address)}
                            underlineColorAndroid='transparent'
                            onFocus={() => this.setState({ focus: 'suggest' })}
                            renderItem={data => (
                                <Touchable onPress={() => this.onSelectLocation(data)} style={styles.btnSelect}>
                                    <Text style={styles.txtSelect}>{data.item.description}</Text>
                                </Touchable>
                            )}
                        />
                        {
                            loadingLocation &&
                            <ActivityIndicator size="large" color="#000000" />
                        }
                        {
                            (!loadingLocation && Address != '' && predictions.length === 0 && editAddress) &&
                            <Text style={{ fontSize: 16, color: '#333333', textAlign: 'center', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'search_location')}</Text>
                        }
                        {
                            Lat && Long ?
                                <View style={styles.boxEditMap}>
                                    <Text style={styles.txtEditMap}>{convertLanguage(language, 'please_check_the_pin_location_is_correct_on_map')}</Text>
                                    <Touchable style={styles.btnEdit} onPress={() => this.onEditLocation()}>
                                        <Text style={styles.txtEdit}>{convertLanguage(language, 'edit')}</Text>
                                    </Touchable>
                                </View>
                                :
                                null
                        }
                        <View style={[{ height: 400, marginTop: 20, backgroundColor: '#E7E7E7', position: 'relative' }, isEdit ? { zIndex: 10 } : {}]}>
                            {
                                Lat && Long ?
                                    <React.Fragment>
                                        <MapView
                                            provider={PROVIDER_GOOGLE}
                                            initialRegion={{
                                                latitude: parseFloat(Lat),
                                                longitude: parseFloat(Long),
                                                latitudeDelta: this.state.latitudeDelta,
                                                longitudeDelta: this.state.longitudeDelta,
                                            }}
                                            style={{ height: 400 }}
                                            onRegionChangeComplete={(region) => this.onRegionChange(region)}
                                            zoomEnabled={isEdit}
                                            pitchEnabled={isEdit}
                                            rotateEnabled={isEdit}
                                            scrollEnabled={isEdit}
                                            ref={ref => this.map = ref}
                                            onMapReady={this._onMapReady}
                                        />
                                        {
                                            showPink &&
                                            <View style={styles.markerFixed}>
                                                <Image style={styles.marker} source={flagPinkImg} />
                                            </View>
                                        }
                                        {
                                            isEdit &&
                                            <Touchable style={styles.boxPinLocation} onPress={() => this.onSavePin()} disabled={loading}>
                                                <Text style={styles.txtPinLocation}>{convertLanguage(language, 'save_pin_location')}</Text>
                                                {
                                                    loading &&
                                                    <ActivityIndicator size='small' color='#FFFFFF' style={{marginLeft: 5}} />
                                                }
                                            </Touchable>
                                        }
                                        {
                                            isEdit &&
                                            <View style={styles.boxAction}>
                                                {
                                                    showButtonMyLocation &&
                                                    <Touchable style={styles.btnMyLocation} onPress={() => this.onPressToMyLocation()}>
                                                        <Image source={require('../../assets/my_location.png')} style={styles.icMyLocation} />
                                                    </Touchable>
                                                }
                                                <Touchable style={styles.btnPlus} onPress={() => this.onPressZoomIn()}>
                                                    <Image source={require('../../assets/plus.png')} style={styles.icPlus} />
                                                </Touchable>
                                                <Touchable style={styles.btnMinus} onPress={() => this.onPressZoomOut()}>
                                                    <Image source={require('../../assets/minus.png')} style={styles.icMinus} />
                                                </Touchable>
                                            </View>
                                        }
                                    </React.Fragment>
                                    :
                                    null
                            }
                        </View>
                        {/* <Marker
                                     coordinate={{latitude: parseFloat(Lat), longitude: parseFloat(Long)}}
                                     image={flagPinkImg}
                                     onDragEnd={(e) => this.setState({ Lat: e.nativeEvent.coordinate.latitude, Long: e.nativeEvent.coordinate.longitude })}
                                     draggable
                                 />
                             </MapView> */}
                    </View>
                </ScrollView>
            </SafeView>
        );
    }
}

LocationSetting.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    content: {
        flex: 1,
        position: 'relative'
    },
    btnPrevios: {
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#333333',
        borderWidth: 1,
        borderRadius: 2,
        margin: 20,
    },
    txtPrevios: {
        fontSize: 15,
        color: '#333333'
    },
    boxInput: {
        paddingBottom: 24,
        paddingLeft: 20,
        paddingRight: 20,
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 7,
        fontWeight: 'bold'
    },
    boxInputContent: {
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        justifyContent: 'center',
        paddingTop: 8,
        paddingBottom: 8,
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxEditMap: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    txtEditMap: {
        fontSize: 14,
        color: '#757575',
        flex: 1,
        lineHeight: 20
    },
    btnEdit: {
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#bdbdbd',
        paddingLeft: 10,
        paddingRight: 10,
        marginLeft: 10
    },
    txtEdit: {
        fontSize: 13,
        color: '#757575',
    },
    containerStyle: {
        flex: 1,
        borderWidth: 0,
        borderRadius: 0,
    },
    btnSelect: {
        backgroundColor: '#FFFFFF',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomColor: '#bdbdbd',
        borderBottomWidth: 1,
        paddingLeft: 5,
        paddingRight: 5
    },
    txtSelect: {

    },
    markerFixed: {
        left: '50%',
        marginLeft: -16,
        marginTop: -45,
        position: 'absolute',
        top: '50%'
    },
    marker: {
        height: 45,
        width: 32
    },
    boxPinLocation: {
        position: 'absolute',
        alignSelf: 'center',
        width: 150,
        height: 36,
        backgroundColor: '#03a9f4',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        top: 20,
        flexDirection: 'row'
    },
    txtPinLocation: {
        color: '#FFFFFF',
        fontSize: 14,
        fontWeight: 'bold'
    },
    boxAction: {
        position: 'absolute',
        bottom: 15,
        right: 15
    },
    btnMyLocation: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icMyLocation: {
        width: 20,
        height: 20
    },
    btnPlus: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icPlus: {
        width: 18,
        height: 18
    },
    btnMinus: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icMinus: {
        width: 18,
        height: 18
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
// const mapDispatchToProps = (dispatch, props) => {
//     return {


//     }
// }
export default connect(mapStateToProps, null)(LocationSetting);
// export default EventHostList;
