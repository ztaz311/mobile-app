import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataCategory } from '../../actions/category'
const { width, height } = Dimensions.get('window');
import { convertLanguage } from '../../services/Helper'
class SelectCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.route.params['category_selected']
        };
    }

    componentDidMount() {
        this.props.onLoadDataCategory('event');
    }

    save() {
        const { navigation } = this.props
        this.props.route.params['callback'](this.state.selected)
        navigation.goBack()
    }

    selectCategory(data) {
        var { selected } = this.state;
        var j = selected.some(el => el.Id === data.Id);
        if (j) {
            selected = selected.filter(el => el.Id !== data.Id);
        } else {
            if (selected.length < 3) {
                selected.push(data)
            }
        }
        this.setState({ selected })
    }

    showListCategory() {
        var { selected } = this.state;
        var { categories } = this.props.category;
        var result = null;
        result = categories.map((data, index) => {
            var backgroundColor = '#FFFFFF';
            // '#e7e7e7';00acfb
            var color = "#333333";
            var fontWeight = 'normal';
            var marginRight = (index + 1) % 3 === 0 ? 0 : 10;
            var borderWidth = 1;
            var borderColor = '#4F4F4F';
            var i = selected.some(el => el.Id === data.Id);
            if (i) {
                if (selected[0].Id === data.Id) {
                    backgroundColor = '#00acfb';
                    color = "#FFFFFF";
                    fontWeight = 'bold';
                    borderColor = '#00acfb';
                } else {
                    backgroundColor = '#FFFFFF';
                    color = "#00acfb";
                    fontWeight = 'bold';
                    borderWidth = 1;
                    borderColor = '#00acfb';
                }
            }
            return <Touchable style={[styles.btnCategory, { borderColor, backgroundColor, marginRight, borderWidth }]} key={index} onPress={() => this.selectCategory(data)}>
                <Text style={[styles.txtCategory, { color, fontWeight }]}>#{data.HashTagName}</Text>
            </Touchable>
        })
        return result;
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        var { loading } = this.props.category;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: '#03a9f4', marginRight: 20 }} onPress={() => this.save()}>
                        <Text style={{ fontSize: 17, color: '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'save')}</Text>
                    </Touchable>
                </View>

                <Line />
                {
                    loading ?
                        <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                        :
                        <View style={styles.content}>
                            <View style={styles.boxTitle}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'select_category_event')}</Text>
                                <Text style={styles.txtTitle}>({convertLanguage(language, 'select_category_event_0')})</Text>
                            </View>
                            <View style={styles.boxListCategory}>
                                {
                                    this.showListCategory()
                                }
                            </View>
                        </View>
                }

            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxTitle: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    txtTitle: {
        fontSize: 14,
        paddingBottom: 10,
        textAlign: 'center'
    },
    boxListCategory: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    btnCategory: {
        width: (width - 60) / 3,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 15,
        // borderColor: '#00acfb'
    },
    txtCategory: {
        fontSize: 15
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        category: state.category,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataCategory: (type) => {
            dispatch(actLoadDataCategory(type))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SelectCategory);
// export default EventHostList;
