import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Keyboard, StyleSheet, Dimensions, TouchableOpacity, KeyboardAvoidingView, TextInput, TouchableWithoutFeedback } from 'react-native';

import Modal from "react-native-modal";
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { TabView } from 'react-native-tab-view';
import WithdrawCompany from '../../components/event/WithdrawCompany';
import WithdrawIndividual from '../../components/event/WithdrawIndividual';
import { actWithdraw, actGetDataWithdraw } from '../../actions/event';
import { convertLanguage, formatNumber } from '../../services/Helper'
const { width, height } = Dimensions.get('window')
class WithdrawRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            routes: [
                { key: 'WithdrawCompany', title: 'Company' },
                { key: 'WithdrawIndividual', title: 'Individual' },
            ],
            individual: {
                Name: '',
                Birthday: '',
                Gender: '',
                Phone: '',
                Zipcode: '+84',
                Email: '',
                Bank: '',
                AccountNo: '',
                AccountHolder: '',
            },
            company: {
                Name: '',
                CEO: '',
                RegistrationNo: '',
                Phone: '',
                Zipcode: '+84',
                Email: '',
                Address: '',
                Bank: '',
                AccountNo: '',
                AccountHolder: '',
            },
            isUpdate: false,
            Password: '',
            modalPassword: false
        };
    }

    _renderTabBar = props => {
        var { language } = this.props.language;
        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    return (
                        <TouchableOpacity
                            key={`tabbar${i}`}
                            style={[styles.tabItem, { borderBottomColor: this.state.index === i ? '#03a9f4' : '#e8e8e8' }]}
                            onPress={() => this.setState({ index: i })}>
                            <Text style={{ color: this.state.index === i ? '#03a9f4' : '#333333', fontWeight: this.state.index === i ? 'bold' : '400', fontSize: 15 }}>{route.title == 'Company' ? convertLanguage(language, 'company') : convertLanguage(language, 'individual')}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    componentDidMount() {
        var { language } = this.props.language;
        this.setState({ isUpdate: false })
        var { Slug } = this.props.route.params;
        this.props.onGetDataWithdraw(Slug, language);
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    updateData(data) {
        this.setState({
            [data.WithdrawType]: data,
            index: data.WithdrawType == 'individual' ? 1 : 0,
        })
    }

    onWithdraw(body) {
        Keyboard.dismiss();
        this.setState({
            modalPassword: true,
            body: body
        })
        // var { Slug } = this.props.route.params;
        // this.props.onWithdraw(Slug, body);
    }

    onSubmit() {
        this.setState({
            modalPassword: false,
        })
        var { Slug } = this.props.route.params;
        var { body, Password } = this.state;
        body.Password = Password;
        this.props.onWithdraw(Slug, body);
    }

    onChange(key, value, type) {
        var data = this.state[type];
        data[key] = value;
        this.setState({ [type]: data });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.event.dataWithdraw.withdraw && !this.state.isUpdate) {
            this.setState({
                [nextProps.event.dataWithdraw.withdraw.WithdrawType]: nextProps.event.dataWithdraw.withdraw,
                index: nextProps.event.dataWithdraw.withdraw.WithdrawType == 'individual' ? 1 : 0,
            })
        }
    }

    render() {
        var { loadingWithdraw, loadingDataWithdraw } = this.props.event;
        var { company, individual, Password, modalPassword } = this.state;
        var { language } = this.props.language;
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1 }}
            >
                <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                        <Touchable
                            onPress={() => this.props.navigation.goBack()}
                            style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                        </Touchable>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'withdraw')}</Text>
                        <View style={{ width: 48 }} />
                    </View>

                    <Line />
                    <View style={styles.content}>
                        {
                            loadingWithdraw &&
                            <View style={{ flex: 1, bottom: height / 2 - 30, width, position: 'absolute', backgroundColor: 'rgba(0,0,0,0.1)', left: 0, top: 0, height: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 999999 }}>
                                <ActivityIndicator size="large" color="#000000" />
                            </View>
                        }
                        {
                            loadingDataWithdraw &&
                            <View style={{ flex: 1, bottom: height / 2 - 30, width, position: 'absolute', backgroundColor: '#FFFFFF', left: 0, top: 0, height: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 999999 }}>
                                <ActivityIndicator size="large" color="#000000" />
                            </View>
                        }
                        <View style={styles.header}>
                            <View style={styles.boxTitle}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'tax_issue')}</Text>
                            </View>
                            <Touchable style={styles.btnImport} onPress={() => this.props.navigation.navigate('ImportWithdraw', { callback: (data) => this.updateData(data) })}>
                                <Text style={styles.txtImport}>{convertLanguage(language, 'import')}</Text>
                            </Touchable>
                        </View>
                        <TabView
                            style={styles.container}
                            navigationState={this.state}
                            labelStyle={{ backgroundColor: 'red' }}
                            renderScene={({ route }) => {
                                switch (route.key) {
                                    case 'WithdrawCompany':
                                        return <WithdrawCompany onWithdraw={(body) => this.onWithdraw(body)} company={company} onChange={(key, value, type) => this.onChange(key, value, type)} loadingWithdraw={loadingWithdraw} />
                                    case 'WithdrawIndividual':
                                        return <WithdrawIndividual onWithdraw={(body) => this.onWithdraw(body)} individual={individual} onChange={(key, value, type) => this.onChange(key, value, type)} loadingWithdraw={loadingWithdraw} />
                                }
                            }}
                            onIndexChange={index => this.setState({ index })}
                            renderTabBar={this._renderTabBar}
                        />
                    </View>
                    {
                        modalPassword &&
                        <Modal
                            isVisible={true}
                            backdropOpacity={0.3}
                            animationIn="zoomInDown"
                            animationOut="zoomOutUp"
                            animationInTiming={1}
                            animationOutTiming={1}
                            backdropTransitionInTiming={1}
                            backdropTransitionOutTiming={1}
                            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                        >
                            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                                <View style={[styles.modalContent]}>
                                    <View style={styles.rowClose}>
                                        <Touchable style={styles.btnCloseModal} onPress={() => { this.setState({ modalPassword: false }) }} >
                                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                                        </Touchable>
                                    </View>
                                    <View style={[styles.contentModal, { alignItems: 'flex-start' }]}>
                                        <Text style={styles.txtWarning}>{convertLanguage(language, 'warning')}</Text>
                                        <Text style={styles.txtQuestion}>{convertLanguage(language, 'delete_event_question')}</Text>
                                        <View style={styles.boxPassword}>
                                            <Text style={styles.txtPhone}>{convertLanguage(language, 'password')}</Text>
                                            <TextInput
                                                style={styles.ipContent}
                                                selectionColor="#bcbcbc"
                                                value={Password}
                                                onChangeText={(Password) => this.setState({ Password })}
                                                secureTextEntry={true}
                                            />
                                        </View>
                                        <Touchable
                                            style={{ marginTop: 0, alignSelf: 'flex-end' }}
                                            onPress={() => {
                                                this.setState({ modalPassword: false })
                                                this.props.navigation.navigate('ForgotPw')
                                            }}>
                                            <Text
                                                style={{ textDecorationLine: 'underline', alignSelf: 'center', fontWeight: 'normal', fontSize: 15, color: Colors.PRIMARY }}>
                                                {convertLanguage(language, 'forgot_password')}?</Text>
                                        </Touchable>
                                        <Touchable disabled={Password.length < 6} style={[styles.btnConfirm, { backgroundColor: Password.length < 6 ? '#e8e8e8' : '#ff4081' }]} onPress={() => this.onSubmit()}>
                                            <Text style={[styles.txtConfirm, { color: Password.length < 6 ? '#bdbdbd' : '#FFFFFF' }]}>{convertLanguage(language, 'confirm')}</Text>
                                        </Touchable>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    }
                </SafeView>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    header: {
        padding: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    boxTitle: {
        flex: 1
    },
    txtTitle: {
        fontSize: 13,
        color: '#333333',
    },
    btnImport: {
        width: 60,
        height: 31,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 2,
    },
    txtImport: {
        fontSize: 13,
        color: '#333333',
    },
    tabBar: {
        flexDirection: 'row',
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 16,
        paddingBottom: 16,
        borderBottomWidth: 2,
        borderBottomColor: '#000000'
    },
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5,
    },
    contentModal: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center',
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnCloseModal: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtWarning: {
        textAlign: 'center',
        fontSize: 26,
        color: '#ff4081',
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 20
    },
    txtQuestion: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 45
    },
    boxInfo: {
        marginTop: 25,
        marginBottom: 25
    },
    txtInfo: {
        fontSize: 15,
        color: '#333333',
        marginBottom: 5
    },
    boxPassword: {
        marginBottom: 15,
        width: '100%',
    },
    txtPhone: {
        fontSize: 13,
        color: '#757575'
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 8,
        paddingTop: 8,
        fontWeight: 'bold',
        // width: '100%',
    },
    btnConfirm: {
        alignSelf: 'center',
        width: 160,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        marginTop: 20
    },
    txtConfirm: {
        fontSize: 17,
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onWithdraw: (slug, body) => {
            dispatch(actWithdraw(slug, body))
        },
        onGetDataWithdraw: (slug, language) => {
            dispatch(actGetDataWithdraw(slug, language))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WithdrawRequest);
