import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { TabView } from 'react-native-tab-view';
import TeamMembers from '../../components/event/TeamMembers';
import Followers from '../../components/event/Followers';
import Search from '../../components/event/Search';
import { convertLanguage } from '../../services/Helper'
import { actInviteUser, actLoadDataMembers, actClearInviteSearch } from '../../actions/event';

class EventInviteStaff extends Component {
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            index: 0,
            routes: [
                { key: 'TeamMembers', title: convertLanguage(language, 'team_member') },
                { key: 'Followers', title: convertLanguage(language, 'followers') },
                { key: 'Search', title: convertLanguage(language, 'search') },
            ],
        };
    }

    _renderTabBar = props => {
        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    return (
                        <TouchableOpacity
                            key={`tanbar${i}`}
                            style={[styles.tabItem, { borderBottomColor: this.state.index === i ? '#03a9f4' : '#e8e8e8' }]}
                            onPress={() => this.setState({ index: i })}>
                            <Text style={{ color: this.state.index === i ? '#03a9f4' : '#333333', fontWeight: this.state.index === i ? 'bold' : '400', fontSize: 15 }}>{route.title}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    componentDidMount() {
        // this.getTeams();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    componentWillUnmount() {
        this.props.clearInviteSearch()
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <View style={styles.content}>
                    <View style={styles.header}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'invite_people_as_your_staff')}</Text>
                    </View>
                    <TabView
                        style={styles.container}
                        navigationState={this.state}
                        labelStyle={{ backgroundColor: 'red' }}
                        renderScene={({ route }) => {
                            switch (route.key) {
                                case 'TeamMembers':
                                    return <TeamMembers onLoadDataMembers={this.props.onLoadDataMembers} onInviteUser={this.props.onInviteUser} />
                                case 'Followers':
                                    return <Followers onLoadDataMembers={this.props.onLoadDataMembers} onInviteUser={this.props.onInviteUser}/>
                                case 'Search':
                                    return <Search onLoadDataMembers={this.props.onLoadDataMembers} onInviteUser={this.props.onInviteUser}/>
                            }
                        }}
                        onIndexChange={index => this.setState({ index })}
                        renderTabBar={this._renderTabBar}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    header: {
        padding: 15,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    txtTitle: {
        fontWeight: 'bold',
        fontSize: 17,
        color: '#333333'
    },
    tabBar: {
        flexDirection: 'row',
        // borderWidth: 1,
        // borderColor: '#000000'
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 16,
        paddingBottom: 16,
        borderBottomWidth: 2,
        borderBottomColor: '#000000'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataMembers: (slug) => {
            dispatch(actLoadDataMembers(slug))
        },
        onInviteUser: (id, userId,item) => {
            dispatch(actInviteUser(id, userId,item))
        },
        clearInviteSearch: () => {
            dispatch(actClearInviteSearch())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventInviteStaff);
// export default EventHostList;
