import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, TextInput } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SrcView from '../../screens_view/ScrView';
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataVenue } from '../../actions/event'
import { convertLanguage } from '../../services/Helper'

class SelectVenue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            selected: '',
            data: {}
        };
    }

    componentDidMount() {
        // this.getTeams();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    onChangeVenue(name) {
        this.setState({
            name
        })
        this.props.onLoadDataVenue(name)
    }

    selectVenue(item, index) {
        this.setState({ selected: index, data: item })
    }

    onGetVenue() {
        this.props.route.params['callback'](2, this.state.name);
        this.props.navigation.goBack()
    }

    onGetDataVenue() {
        var { data, selected } = this.state;
        if(selected === -1) {
            this.props.route.params['callback'](2, this.state.name);
            this.props.navigation.goBack()
        } else {
            this.props.route.params['callback'](1, data);
            this.props.navigation.goBack()
        }
    }

    render() {
        var { loading, venues } = this.props.event;
        var { language } = this.props.language;
        var { selected } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <View style={{ width: 50 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    <View style={styles.boxInput}>
                        <Text style={styles.txtInput}>{convertLanguage(language, 'venue_name')}</Text>
                        <TextInput
                            style={styles.ipContent}
                            selectionColor="#bcbcbc"
                            value={this.state.name}
                            onChangeText={(name) => this.onChangeVenue(name)}
                        />
                    </View>
                    <SrcView style={{ flex: 1, paddingLeft: 10, paddingRight: 10 }}>
                        {
                            this.state.name != '' &&
                            <Touchable style={styles.boxLocation} onPress={() => this.setState({selected: -1})}>
                                <View style={[styles.boxContentLocation, { flexDirection: 'row', alignItems: 'center' }]}>
                                    <Image source={require('../../assets/location_plus.png')} style={{ width: 25, height: 25, marginRight: 5 }} />
                                    <Text style={[styles.txtVenue, selected === -1 ? { color: '#03a9f4' } : {}]}>{convertLanguage(language, 'add')} <Text style={{ color: '#333333' }}>{this.state.name}</Text> {convertLanguage(language, 'as_new_venue')}</Text>
                                </View>
                                {
                                    // item.IsLinked
                                    // &&
                                    // <Image source={require('../../assets/linked_icon.png')} style={styles.linked_icon} />
                                }
                            </Touchable>
                        }
                        {
                            loading ?
                                <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                                :
                                venues.map((item, index) => {
                                    return <Touchable style={styles.boxLocation} key={index} onPress={() => this.selectVenue(item, index)}>
                                        <View style={styles.boxContentLocation}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../../assets/location_plus.png')} style={{ width: 25, height: 25, marginRight: 5 }} />
                                                <Text style={[styles.txtVenue, selected === index ? { color: '#03a9f4' } : {}]}>{item.Name}</Text>
                                            </View>
                                            <Text style={[styles.txtAddress, selected === index ? { color: '#03a9f4' } : {}]}>{item.Address}</Text>
                                        </View>
                                        {
                                            item.IsLinked
                                            &&
                                            <Image source={require('../../assets/linked_icon.png')} style={styles.linked_icon} />
                                        }
                                    </Touchable>
                                })
                        }
                    </SrcView>
                    <Touchable disabled={selected === ''} style={[styles.btnOk, { backgroundColor: selected !== '' ? '#03a9f4' : '#e8e8e8' }]} onPress={() => this.onGetDataVenue()}>
                        <Text style={[styles.txtOk, { color: selected !== '' ? '#FFFFFF' : '#BCBCBC' }]}>{convertLanguage(language, 'ok')}</Text>
                    </Touchable>
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxInput: {
        marginLeft: 20,
        marginRight: 20,
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 7,
        fontWeight: 'bold'
    },
    ipContent: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 8,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    boxLocation: {
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 2,
        borderBottomColor: '#e8e8e8',
        flexDirection: 'row',
        alignItems: 'center'
    },
    boxContentLocation: {
        flex: 1,
    },
    linked_icon: {
        marginLeft: 10,
        width: 27,
        height: 27
    },
    txtVenue: {
        fontSize: 15,
        color: '#757575',
        paddingBottom: 10,
        fontWeight: 'bold'
    },
    txtAddress: {
        fontSize: 15,
        color: '#757575'
    },
    btnOk: {
        height: 48,
        backgroundColor: '#03a9f4',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtOk: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#ffffff'
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataVenue: (name) => {
            dispatch(actLoadDataVenue(name))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SelectVenue);
// export default EventHostList;
