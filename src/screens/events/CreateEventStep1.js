import React from 'react';
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import { useSelector } from "react-redux";
import RNFetchBlob from "rn-fetch-blob";
import * as Colors from '../../constants/Colors';
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';
const { width, height } = Dimensions.get('window')
const fs = RNFetchBlob.fs;
let imagePath = null;

const scale = width / 360


const CreateEventStep1 = (props) => {
  const { navigation } = props
  var language = useSelector(state => state.language.language)
  return (
    <SafeView style={styles.container}>
      <View style={styles.header}>
        <Touchable
          onPress={() => navigation.goBack()}
          style={styles.touchAction}>
          <View style={styles.headerLeft}>
            <Image source={require('../../assets/icon_back.png')} style={{ width: 18 * scale, height: 18 * scale }} resizeMode='cover' />
            <Text style={styles.txtLeft}>{convertLanguage(language, 'create_event')}</Text>
          </View>
        </Touchable>

        <Touchable
          onPress={() => null}
          style={styles.touchAction}>
          <Text style={styles.headerRight}>{convertLanguage(language, 'import_event')}</Text>
        </Touchable>
      </View>
      <View style={styles.stepContainer}>
        <Touchable style={{ borderBottomWidth: 1, borderBottomColor: Colors.PRIMARY, width: '25%', flexDirection: 'row', justifyContent: 'center' }}>
          <View style={{ alignItems: 'center' }}>
            <Image source={require('../../assets/basic_info.png')} style={{ width: 32 * scale, height: 32 * scale }} resizeMode='cover' />
            <Text style={{ paddingTop: 4, fontSize: 14, lineHeight: 18, color: '#4F4F4F', }}>1. Basic info</Text>
          </View>
          <View style={{ top: 8 * scale, position: 'absolute', right: -5 * scale }} >
            <Image source={require('../../assets/right.png')} style={{ width: 16 * scale, height: 16 * scale, tintColor: '#BDBDBD' }} resizeMode='cover' />
          </View>
        </Touchable>

        <Touchable>
          <View style={{ alignItems: 'center', }}>
            <Image source={require('../../assets/detail_info.png')} style={{ width: 32 * scale, height: 32 * scale }} resizeMode='cover' />
            {/* <Text style={{ paddingTop: 4, fontSize: 14, lineHeight: 18, color: '#4F4F4F' }}>1. Basic info</Text> */}
          </View>
        </Touchable>
        <View style={{ paddingTop: 8 * scale }} >
          <Image source={require('../../assets/right.png')} style={{ width: 16 * scale, height: 16 * scale, tintColor: '#BDBDBD', alignSelf: 'center' }} resizeMode='cover' />
        </View>
        <Touchable>
          <View style={{ alignItems: 'center', }}>
            <Image source={require('../../assets/tickets_big.png')} style={{ width: 32 * scale, height: 32 * scale }} resizeMode='cover' />
            {/* <Text style={{ paddingTop: 4, fontSize: 14, lineHeight: 18, color: '#4F4F4F' }}>1. Basic info</Text> */}
          </View>
        </Touchable>
        <View style={{ paddingTop: 8 * scale }} >
          <Image source={require('../../assets/right.png')} style={{ width: 16 * scale, height: 16 * scale, tintColor: '#BDBDBD', alignSelf: 'center' }} resizeMode='cover' />
        </View>
        <Touchable>
          <View style={{ alignItems: 'center', }}>
            <Image source={require('../../assets/publish.png')} style={{ width: 32 * scale, height: 32 * scale }} resizeMode='cover' />
            {/* <Text style={{ paddingTop: 4, fontSize: 14, lineHeight: 18, color: '#4F4F4F' }}>1. Basic info</Text> */}
          </View>
        </Touchable>
      </View>
    </SafeView>
  )
}

const styles = StyleSheet.create({
  container: { backgroundColor: Colors.BG, flex: 1 },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 12 * scale,
    borderBottomWidth: 1 * scale,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOffset: {
      width: 0,
      height: 1 * scale,
    },
    shadowOpacity: 0.1 * scale,
    shadowRadius: 1 * scale,
  },
  touchAction: {
    minWidth: 48, minHeight: 48, alignSelf: 'flex-start',
    justifyContent: 'center', alignItems: 'center',
  },
  headerLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerRight: { color: Colors.PRIMARY, fontSize: 16 * scale },
  txtLeft: { paddingLeft: 4 * scale, fontSize: 18 * scale, fontWeight: '600', color: '#4f4f4f', lineHeight: 23 * scale },

  stepContainer: { flexDirection: 'row', justifyContent: 'space-between', paddingTop: 16, marginBottom: 24, borderBottomWidth: 1, borderBottomColor: '#F2F2F2', marginHorizontal: 12 }
})

export default CreateEventStep1

