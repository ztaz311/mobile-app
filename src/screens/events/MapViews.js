import React, { Component } from 'react';
import { View, Text, Image, Platform, StyleSheet, PermissionsAndroid, Alert } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import MapView, { Marker, ProviderPropType, PROVIDER_GOOGLE } from 'react-native-maps';
import * as Colors from '../../constants/Colors'
import flagPinkImg from '../../assets/pin_icon.png';
import Geolocation from '@react-native-community/geolocation';
import { check, PERMISSIONS, RESULTS, openSettings, request } from 'react-native-permissions';
import AndroidOpenSettings from 'react-native-android-open-settings';
import { connect } from "react-redux";
const ASPECT_RATIO = 2;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
class MapViews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lat: this.props.route.params['Lat'],
            lng: this.props.route.params['Long'],
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            showButtonMyLocation: true,
        };
    }

    onPressToMyLocation() {
        // Geolocation.getCurrentPosition((position) => {
        //     this.setState({
        //         lat: position.coords.latitude,
        //         lng: position.coords.longitude,
        //     })
        //     this.region = {
        //         latitude: position.coords.latitude,
        //         longitude: position.coords.longitude,
        //         latitudeDelta: this.state.latitudeDelta,
        //         longitudeDelta: this.state.longitudeDelta
        //     }
        //     setTimeout(() => this.map.animateToRegion(this.region), 10);
        // })
        var permission = Platform.OS === 'ios' ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
        check(permission).then(response => {
            if (response === RESULTS.BLOCKED) {
                this._alertForLocationPermission()
            } else if (response === RESULTS.GRANTED) {
                Geolocation.getCurrentPosition((position) => {
                    this.setState({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    })
                    this.region = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: this.state.latitudeDelta,
                        longitudeDelta: this.state.longitudeDelta
                    }
                    setTimeout(() => this.map.animateToRegion(this.region), 10);
                })
            } else if (response === RESULTS.DENIED) {
                request(permission).then(response2 => {
                    if (response2 === RESULTS.GRANTED) {
                        Geolocation.getCurrentPosition((position) => {
                            this.setState({
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            })
                            this.region = {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                                latitudeDelta: this.state.latitudeDelta,
                                longitudeDelta: this.state.longitudeDelta
                            }
                            setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
                        })
                    }
                })
            } else {
                this._alertForLocationPermission()
            }
        });
    }

    _alertForLocationPermission() {
        var { language } = this.props.language;
        setTimeout(() => {
            Alert.alert(
                convertLanguage(language, 'access_location_title'),
                convertLanguage(language, 'access_location_content'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        style: 'cancel',
                    },
                    { text: convertLanguage(language, 'open_setting'), onPress: () => this.openSettings() },
                ],
            );
        }, 100)
    }
    openSettings() {
        Platform.OS === 'ios' ? openSettings() : AndroidOpenSettings.appDetailsSettings();
        // this.props.navigation.goBack();
    }

    onPressZoomIn() {
        this.region = {
            latitude: this.state.lat,
            longitude: this.state.lng,
            latitudeDelta: this.state.latitudeDelta / 2,
            longitudeDelta: this.state.longitudeDelta / 2
        }
        this.setState({
            latitudeDelta: this.state.latitudeDelta / 2,
            longitudeDelta: this.state.longitudeDelta / 2
        })
        // this.map.animateToRegion(this.region, 1000);
        setTimeout(() => this.map.animateToRegion(this.region), 10);
    }
    onPressZoomOut() {
        this.region = {
            latitude: this.state.lat,
            longitude: this.state.lng,
            latitudeDelta: this.state.latitudeDelta * 2,
            longitudeDelta: this.state.longitudeDelta * 2
        }
        this.setState({
            latitudeDelta: this.state.latitudeDelta * 2,
            longitudeDelta: this.state.longitudeDelta * 2
        })
        // this.map.animateToRegion(this.region, 1000);
        setTimeout(() => this.map.animateToRegion(this.region), 10);
    }

    _onMapReady() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    // this.setState({ paddingTop: 0 });
                });
        }
    }

    render() {
        var { Lat, Long } = this.props.route.params;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: '500', color: '#333333' }}></Text>
                    <View style={{ minHeight: 48, minWidth: 48, marginRight: 5, justifyContent: 'center', alignItems: 'center' }}></View>
                </View>

                <Line />
                <View style={styles.content}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        initialRegion={{
                            latitude: this.state.lat,
                            longitude: this.state.lng,
                            latitudeDelta: this.state.latitudeDelta,
                            longitudeDelta: this.state.longitudeDelta,
                        }}
                        zoomEnabled={true}
                        enableZoomControl={true}
                        style={{ flex: 1 }}
                        showsUserLocation={true}
                        showsMyLocationButton={false}
                        onRegionChangeComplete={(center) => {
                            this.setState({
                                lat: center.latitude && center.latitude === 0 ? this.state.lat : center.latitude,
                                lng: center.longitude && center.longitude === 0 ? this.state.lng : center.longitude,
                                latitudeDelta: center.latitudeDelta,
                                longitudeDelta: center.longitudeDelta,
                            })
                        }}
                        onPress={(e) => console.log(e.nativeEvent.coordinate)}
                        ref={ref => this.map = ref}
                        onMapReady={this._onMapReady}
                    >
                        <Marker
                            coordinate={{latitude: Lat, longitude: Long}}
                            image={flagPinkImg}
                        />
                    </MapView>
                    <View style={styles.boxAction}>
                        <Touchable style={styles.btnMyLocation} onPress={() => this.onPressToMyLocation()}>
                            <Image source={require('../../assets/my_location.png')} style={styles.icMyLocation} />
                        </Touchable>
                        <Touchable style={styles.btnPlus} onPress={() => this.onPressZoomIn()}>
                            <Image source={require('../../assets/plus.png')} style={styles.icPlus} />
                        </Touchable>
                        <Touchable style={styles.btnMinus} onPress={() => this.onPressZoomOut()}>
                            <Image source={require('../../assets/minus.png')} style={styles.icMinus} />
                        </Touchable>
                    </View>
                </View>
            </SafeView>
        );
    }
}

MapViews.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxAction: {
        position: 'absolute',
        bottom: 15,
        right: 15
    },
    btnMyLocation: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icMyLocation: {
        width: 20,
        height: 20
    },
    btnPlus: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icPlus: {
        width: 18,
        height: 18
    },
    btnMinus: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icMinus: {
        width: 18,
        height: 18
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
export default connect(mapStateToProps, null)(MapViews);
