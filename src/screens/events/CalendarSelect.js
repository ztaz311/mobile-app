import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';
import moment from "moment";
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import { convertLanguage, findIndex } from '../../services/Helper'
import { connect } from "react-redux";
import * as Colors from '../../constants/Colors'
import { Calendar } from 'react-native-calendars';
class CalendarSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markedDates: this.setMarkedDates(),
            dates_repeat: this.props.route.params['TimeRepeat'],
            // days_between: this.setDayBetween()
        };
    }

    setDayBetween() {
        var TimeStart = this.props.route.params['TimeStart'];
        var TimeFinish = this.props.route.params['TimeFinish'];
        return moment(this.convertDateToMoment(TimeFinish)).diff(this.convertDateToMoment(TimeStart), 'days')
    }

    convertDateToMoment(dateString) {
        var date = dateString.split("-");
        return moment([date[0], date[1]-1, date[2]])
    }

    setMarkedDates() {
        // var days_between = this.setDayBetween()
        var markedDates = {};
        markedDates = {
            [this.props.route.params['TimeStart']]: {customStyles: {container: {backgroundColor: '#03a9f4'}}, selected: true, disableTouchEvent: true}
        }
        // if(days_between>0) {
        //     dateMoment = this.convertDateToMoment(this.props.route.params['TimeStart']);
        //     for(var i = 1; i <= days_between; i++) {
        //         var newDate = moment(dateMoment).add(i, 'days');
        //         markedDates[newDate.format('YYYY-MM-DD')] = {
        //             customStyles: {
        //                 container: {
        //                     borderWidth: 4,
        //                     borderColor: '#67938E',
        //                 },
        //                 text: {
        //                     color: 'white',
        //                     marginTop: 3
        //                 },
        //             },
        //             disabled: true,
        //             disableTouchEvent: true
        //         }
        //     }
        //     for(var j = 1; j <= days_between; j++) {
        //         var newDate = moment(dateMoment).subtract(j, 'days');
        //         if(!(newDate.format('YYYY-MM-DD') in markedDates)) {
        //             markedDates[newDate.format('YYYY-MM-DD')] = {
        //                 customStyles: {
        //                     text: {
        //                         color: '#bdbdbd',
        //                     },
        //                 },
        //                 disabled: true,
        //                 disableTouchEvent: true
        //             }
        //         }
        //     }
        // }
        this.props.route.params['TimeRepeat'].forEach((date_repeat, index) => {
            markedDates[date_repeat] = {
                customStyles: {
                    container: {
                        // backgroundColor: '#67938E',
                        borderWidth: 4,
                        borderColor: '#67938E',
                    },
                    text: {
                        color: 'white',
                        marginTop: 3
                    },
                }
            }

            // if(days_between>0) {
            //     dateRepeatMoment = this.convertDateToMoment(date_repeat);
            //     for(var m = 1; m <= days_between; m++) {
            //         var newDate = moment(dateRepeatMoment).add(m, 'days');
            //         markedDates[newDate.format('YYYY-MM-DD')] = {
            //             customStyles: {
            //                 container: {
            //                     borderWidth: 4,
            //                     borderColor: '#67938E',
            //                 },
            //                 text: {
            //                     color: 'white',
            //                     marginTop: 3
            //                 },
            //             },
            //             disabled: true,
            //             disableTouchEvent: true
            //         }
            //     }
            //     for(var n = 1; n <= days_between; n++) {
            //         var newDate = moment(dateRepeatMoment).subtract(n, 'days');
            //         if(!(newDate.format('YYYY-MM-DD') in markedDates)) {
            //             markedDates[newDate.format('YYYY-MM-DD')] = {
            //                 customStyles: {
            //                     text: {
            //                         color: '#bdbdbd',
            //                     },
            //                 },
            //                 disabled: true,
            //                 disableTouchEvent: true
            //             }
            //         }
            //     }
            // }
        });
        return markedDates;
    }
    
    onDayPress(day) {
        var { markedDates, dates_repeat } = this.state;
        var dateString = day.dateString;
        // var dateMoment = this.convertDateToMoment(dateString);
        // alert(dateMoment.format('YYYY-MM-DD'));
        if (dateString in markedDates) {
            // trường hợp bỏ chọn repeart
            var index = findIndex(dates_repeat, dateString);
            dates_repeat.splice(index, 1)
            delete markedDates[dateString];

            // if(days_between>0) {
            //     // Check hiển thị disable hoặc xóa diable
            //     for(var i = 1; i <= days_between; i++) {
            //         var newDate = moment(dateMoment).add(i, 'days');
            //         if(newDate.format('YYYY-MM-DD') in markedDates) {
            //             var checkDisable = false;
            //             for(var m = 1; m <= days_between; m++) {
            //                 if(findIndex(dates_repeat, moment(newDate).add(m, 'days').format('YYYY-MM-DD')) !== -1) {
            //                     checkDisable = true;
            //                     break;
            //                 }
            //             }
            //             if(checkDisable) {
            //                 markedDates[newDate.format('YYYY-MM-DD')] = {
            //                     customStyles: {
            //                         text: {
            //                             color: '#bdbdbd',
            //                         },
            //                     },
            //                     disabled: true,
            //                     disableTouchEvent: true
            //                 }
            //             } else {
            //                 delete markedDates[newDate.format('YYYY-MM-DD')];
            //             }
            //         }
            //     }

            //     // Check bỏ disable khi bỏ chọn ngày repeart.
            //     var countSubtractDate = 0;
            //     for(var j = 1; j <= days_between; j++) {
            //         var checkDate = moment(dateMoment).subtract(days_between+j, 'days');
            //         var index2 = findIndex(dates_repeat, moment(checkDate).format('YYYY-MM-DD'));
            //         if(index2 !== -1 || moment(checkDate).format('YYYY-MM-DD') === this.props.route.params['TimeStart']) {
            //             countSubtractDate = j;
            //             break;
            //         }
            //     }
            //     countSubtractDate = countSubtractDate === 0 ? days_between+1 : countSubtractDate;
            //     for(var k = 1; k < countSubtractDate; k++) {
            //         var newDate = moment(dateMoment).subtract(k, 'days');
            //         if(newDate.format('YYYY-MM-DD') in markedDates) {
            //             delete markedDates[newDate.format('YYYY-MM-DD')];
            //         }
            //     }
            // }

            this.setState({
                markedDates: markedDates,
                dates_repeat: dates_repeat
            })
        } else {
            // Trường hợp chọn repeart
            dates_repeat.push(dateString)
            markedDates[dateString] = {
                customStyles: {
                    container: {
                        // backgroundColor: '#67938E',
                        borderWidth: 4,
                        borderColor: '#67938E',
                    },
                    text: {
                        color: 'white',
                        marginTop: 3
                    },
                }
            }

            // if(days_between>0) {
            //     // Thêm những thằng phía bên phải
            //     for(var i = 1; i <= days_between; i++) {
            //         var newDate = moment(dateMoment).add(i, 'days');
            //         markedDates[newDate.format('YYYY-MM-DD')] = {
            //             customStyles: {
            //                 container: {
            //                     borderWidth: 4,
            //                     borderColor: '#67938E',
            //                 },
            //                 text: {
            //                     color: 'white',
            //                     marginTop: 3
            //                 },
            //             },
            //             disabled: true,
            //             disableTouchEvent: true
            //         }
            //     }
            //     // check disable phía bên trái
            //     for(var j = 1; j <= days_between; j++) {
            //         var newDate = moment(dateMoment).subtract(j, 'days');
            //         if(!(newDate.format('YYYY-MM-DD') in markedDates)) {
            //             markedDates[newDate.format('YYYY-MM-DD')] = {
            //                 customStyles: {
            //                     text: {
            //                         color: '#bdbdbd',
            //                     },
            //                 },
            //                 disabled: true,
            //                 disableTouchEvent: true
            //             }
            //         }
            //     }
            // }
            
            this.setState({
                markedDates: markedDates,
                dates_repeat: dates_repeat
            })
        }
    }

    showDateRepeat() {
        var { dates_repeat } = this.state;
        var dateString = '';
        dateString = dates_repeat.map((item, index) => {
            var arrDate = item.split("-");
            var newItem = arrDate[0]+'-'+arrDate[1]+'-'+arrDate[2];
            if(index+1 < dates_repeat.length) {
                return ' '+newItem+',';
            }else {
                return ' '+newItem;
            }
        });
        return dateString;
    }

    save() {
        var { dates_repeat } = this.state;
        this.props.route.params['callback'](dates_repeat);
        this.props.navigation.goBack();
    }

    goBack() {
        this.props.navigation.goBack();
    }

    render() {
        var markedDates = {...this.state.markedDates}
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable style={{height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: '#03a9f4', marginRight: 20}} onPress={() => this.save()}>
                        <Text style={{fontSize: 17, color: '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15}}>{convertLanguage(language, 'save')}</Text>
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    <View style={styles.boxTitle}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'repeat_title_1')}</Text>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'repeat_title_2')}</Text>
                    </View>
                    <Calendar
                        minDate={this.props.route.params['TimeStart']}
                        onDayPress={(day) => {this.onDayPress(day)}}
                        monthFormat={'MM / yyyy'}
                        hideArrows={false}
                        // current={this.props.route.params['TimeStart']}
                        // renderArrow={(direction) => (<Arrow />)}
                        disableMonthChange={true}
                        firstDay={1}
                        onPressArrowLeft={substractMonth => substractMonth()}
                        onPressArrowRight={addMonth => addMonth()}
                        markingType={'custom'}
                        markedDates={markedDates}
                        theme={{
                            backgroundColor: '#424242',
                            calendarBackground: '#424242',
                            textSectionTitleColor: '#b6c1cd',
                            selectedDayBackgroundColor: '#67938E',
                            selectedDayTextColor: '#424242',
                            // todayTextColor: 'red',
                            dayTextColor: '#ffffff',
                            textDisabledColor: '#bdbdbd',
                            selectedDotColor: '#ffff',
                            arrowColor: '#ffffff',
                            monthTextColor: '#ffffff',
                            textMonthFontWeight: 'bold',
                            textDayFontSize: 16,
                            textDayFontWeight: 'bold',
                            textMonthFontSize: 16,
                            textDayHeaderFontSize: 16
                        }}
                        />
                    <View style={styles.boxRepeat}>
                        <Text style={styles.txtRepeat}><Text style={styles.txtRepeatName}>{convertLanguage(language, 'repeat_date')}:</Text>{this.showDateRepeat()}</Text>
                    </View>
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxTitle: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    txtTitle: {
        fontSize: 13,
        paddingBottom: 10,
        textAlign: 'center'
    },
    boxRepeat: {
        paddingTop: 20
    },
    txtRepeat: {
        fontSize: 14,
        color: '#333333',
        lineHeight: 20
    },
    txtRepeatName: {
        color: '#8a8a8a'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(CalendarSelect);
