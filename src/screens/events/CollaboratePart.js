import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SrcView from '../../screens_view/ScrView';
import ItemTeamColla from '../../components/event/ItemTeamColla'
import Toast from 'react-native-simple-toast';
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actApplyCollaborate } from '../../actions/event'
import { actLoadDataCategory } from '../../actions/category'
const { width, height } = Dimensions.get('window');
class CollaboratePart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: [],
            data: this.props.route.params['data'],
            isOnpress: true,
        };
    }

    componentDidMount() {
        this.props.onLoadDataCategory('event');
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    save() {
        const { navigation } = this.props
        this.props.route.params['callback'](this.state.selected)
        navigation.goBack()
    }

    selectCategory(data) {
        var { selected } = this.state;
        var index = selected.indexOf(data);
        if (index === -1) {
            selected.push(data)
        } else {
            selected.splice(index, 1);
        }
        this.setState({ selected })
    }

    showListCategory() {
        var { selected } = this.state;
        var { categories } = this.props.category;
        var result = null;
        result = categories.map((data, index) => {
            var backgroundColor = '#FFFFFF';
            var color = "#333333";
            var fontWeight = 'normal';
            var marginRight = (index + 1) % 3 === 0 ? 0 : 10;
            var borderWidth = 1;
            var borderColor = '#4F4F4F'
            // '#00acfb'

            if (selected.indexOf(data) !== -1) {
                backgroundColor = '#FFFFFF';
                color = "#00acfb";
                fontWeight = 'bold';
                borderWidth = 1;
                borderColor = '#00acfb'
            }
            return <Touchable style={[styles.btnCategory, { borderColor, backgroundColor, marginRight, borderWidth }]} key={index} onPress={() => this.selectCategory(data)}>
                <Text style={[styles.txtCategory, { color, fontWeight }]}>{data.HashTagName}</Text>
            </Touchable>
        })
        return result;
    }

    onApplyCollaborate(TeamId) {
        var { language } = this.props.language;
        const { navigation } = this.props
        var { selected } = this.state;
        this.setState({
            isOnpress: false
        })
        if (selected.length === 0) {
            Toast.showWithGravity('You must select the part', Toast.SHORT, Toast.BOTTOM)
            this.setState({
                isOnpress: true
            })
            return;
        }
        var Tag = [];
        selected.forEach((select) => {
            Tag.push(select.Id);
        });
        var body = {
            EventId: this.props.route.params['Slug'],
            TeamId,
            Tag
        }
        this.props.onApplyCollaborate(body, navigation, language)
        setTimeout(() => {
            this.setState({
                isOnpress: true
            })
        }, 2000)
    }

    render() {
        var { data, isOnpress } = this.state;
        var { isFetching } = this.props.event;
        var { loading } = this.props.category;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />
                {
                    loading ?
                        <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                        :
                        <React.Fragment>
                            <SrcView style={{ flex: 1 }}>
                                {
                                    isFetching &&
                                    <View style={{ width, height: height - 100, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                                        <ActivityIndicator size="large" color="#000000" />
                                    </View>
                                }
                                <View style={[styles.content, { opacity: isFetching ? 0.2 : 1 }]}>
                                    <ItemTeamColla data={data} type={'show'} navigation={this.props.navigation} />
                                    <View style={styles.boxTitle}>
                                        <Text style={styles.txtTitle}>{convertLanguage(language, 'select_the_part_text')}</Text>
                                        <Text style={styles.txtContent}>{convertLanguage(language, 'if_the_host_confirm_it_text')}</Text>
                                    </View>
                                    <View style={styles.boxListCategory}>
                                        {
                                            this.showListCategory()
                                        }
                                    </View>
                                </View>
                            </SrcView>
                            <Touchable style={styles.btnApply} onPress={() => (isFetching || !isOnpress) ? null : this.onApplyCollaborate(data.Id)}>
                                <Text style={styles.txtApply}>{convertLanguage(language, 'apply')}</Text>
                            </Touchable>
                        </React.Fragment>
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
    },
    boxTitle: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    txtTitle: {
        fontSize: 15,
        paddingBottom: 10,
        textAlign: 'center',
        color: '#333333'
    },
    txtContent: {
        fontSize: 14,
        paddingBottom: 10,
        textAlign: 'center',
        color: '#757575'
    },
    boxListCategory: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    btnCategory: {
        width: (width - 60) / 3,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 15,
    },
    txtCategory: {
        fontSize: 15
    },
    btnApply: {
        height: 54,
        backgroundColor: '#03a9f4',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        borderRadius: 2
    },
    txtApply: {
        color: '#FFFFFF',
        fontSize: 17,
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        category: state.category,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataCategory: (type) => {
            dispatch(actLoadDataCategory(type))
        },
        onApplyCollaborate: (body, navigation, language) => {
            dispatch(actApplyCollaborate(body, navigation, language))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CollaboratePart);
