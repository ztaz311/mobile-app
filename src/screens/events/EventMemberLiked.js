import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import LikedMember from '../../components/event/LikedMember';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { actLoadDataLikeEvent, actLikeEvent } from '../../actions/event';
import { convertLanguage } from '../../services/Helper';
import { Modalize } from 'react-native-modalize';
class EventLiked extends Component {
    modal = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            isOnpressLike: true,
        };
    }

    componentDidMount() {
        var { page } = this.state;
        this.modal.current?.open();
        this.props.onLoadDataLikeEvent(this.props.Slug, page);
    }

    closeModal = () => {
        if (this.modal.current) {
            this.modal.current.close();
        }
    };

    _renderFooter = () => {
        var { loadMoreLiked, is_empty } = this.props.event;
        var { language } = this.props.language;
        if (loadMoreLiked) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'no_data')}</Text>
                // return null;
            } else {
                return null;
            }
        }
    }

    onLikeEvent(Id) {
        this.setState({
            isOnpressLike: false,
        })
        this.props.onLikeEvent(Id, this.props.user.profile);
        setTimeout(() => {
            this.setState({
                isOnpressLike: true
            })
        }, 2000)
    }

    loadMore = () => {
        var { loadMoreLiked } = this.props.event;
        var { page } = this.state;
        if (loadMoreLiked) {
            this.props.onLoadDataLikeEvent(this.props.Slug, page + 1);
            this.setState({ page: page + 1 })
        }
    }

    renderHeader = () => {
        var { list_likes_event, event } = this.props.event;
        var { language } = this.props.language;
        return <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 15, marginBottom: 15, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', paddingLeft: 20, paddingRight: 20 }}>
            <View style={styles.boxCountLike}>
                {
                    this.props.Role === 'Guest' &&
                    <Touchable onPress={() => this.state.isOnpressLike ? this.onLikeEvent(event.Id) : null} disabled={!this.state.isOnpressLike}>
                        <Image source={event.Like ? require('../../assets/liked_red.png') : require('../../assets/like_border_black.png')} style={styles.icLike} />
                    </Touchable>
                }
                <Text style={styles.txtLike}>{list_likes_event.total} {convertLanguage(language, 'liked')}</Text>
            </View>
            <Touchable style={styles.boxClose} onPress={this.closeModal}>
                <Image source={require('../../assets/close.png')} style={styles.icClose} />
            </Touchable>
        </View>
    }

    renderItem = ({ item }) => (
        <LikedMember data={item} />
    );

    render() {
        var { list_likes_event } = this.props.event;
        return (
            <Modalize
                ref={this.modal}
                // snapPoint={300}
                flatListProps={{
                    data: list_likes_event.data,
                    renderItem: this.renderItem,
                    showsVerticalScrollIndicator: false,
                    onEndReached: this.loadMore,
                    ListHeaderComponent: this.renderHeader,
                    onEndReachedThreshold: 0.1,
                    ListFooterComponent: this._renderFooter
                }}
                onClosed={() => this.props.closeModal()}
            />
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxCountLike: {
        // marginTop: 15,
        // marginBottom: 15,
        // paddingBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        // marginLeft: 20,
        // marginRight: 20,
        // borderBottomWidth: 1,
        // borderBottomColor: '#e8e8e8'
    },
    icLike: {
        width: 28,
        height: 28,
        marginRight: 10
    },
    txtLike: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxClose: {
        // width: 40,
        // height: 40,
    },
    icClose: {
        width: 30,
        height: 30
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        user: state.user,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataLikeEvent: (slug, page) => {
            dispatch(actLoadDataLikeEvent(slug, page))
        },
        onLikeEvent: (id, user) => {
            dispatch(actLikeEvent(id, user))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventLiked);
// export default EventHostList;
