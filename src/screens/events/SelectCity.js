import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import SrcView from '../../screens_view/ScrView';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { actSelectCity } from '../../actions/city';
import { convertLanguage } from '../../services/Helper';
import * as Config from '../../constants/Config';

class SelectCity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            country: '',
            city: null,
            cityId: '',
            location_loading: false
        };
    }

    componentDidMount() {
        // this.getTeams();
    }

    selectCity(Slug) {
        this.setState({country: Slug});
        this.props.onSelectCity(Slug);
    }

    async setCity(item) {
        this.setState({cityId: item.Id, city: null, location_loading: true})
        const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${item.Name}&key=${Config.GOOGLE_API_KEY}`;
        try {
            const result = await fetch(apiUrl);
            const json = await result.json();
            if(json.results.length > 0) {
                item.Lat = json.results[0].geometry.location.lat;
                item.Lng = json.results[0].geometry.location.lng;
            }
            this.setState({city: item, location_loading: false})
        } catch (err) {
            console.log(err)
        }
    }

    callBackCity() {
        var { city } = this.state;
        this.props.route.params['callback'](city);
        this.props.navigation.goBack()
    }

    render() {
        var { cities, loading } = this.props.city;
        var { language } = this.props.language;
        var { city, country, cityId, location_loading } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <View style={{ width: 50 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <SrcView style={{ flex: 1 }}>
                    <View style={styles.content}>
                        <View style={styles.boxCountry}>
                            <Text style={styles.txtCountry}>{convertLanguage(language, 'country')}</Text>
                            <Text style={country === 'korea' ? styles.txtCountryNameActive : styles.txtCountryName} onPress={() => this.selectCity('korea')}>Korea</Text>
                            <Text style={country === 'viet-nam' ? styles.txtCountryNameActive : styles.txtCountryName } onPress={() => this.selectCity('viet-nam')}>Vietnam</Text>
                        </View>
                        <View style={[styles.boxCountry, { borderBottomWidth: 0 }]}>
                            {
                                loading ?
                                <ActivityIndicator size="large" color="#000000" style={{padding: 20}}/>
                                :
                                <React.Fragment>
                                    <Text style={styles.txtCountry}>{convertLanguage(language, 'city')}</Text>
                                    {
                                        country === '' || cities.length > 0 ?
                                        cities.map((item, index) => {
                                            return <Text style={item.Id === cityId ? styles.txtCountryNameActive : styles.txtCountryName} key={index} onPress={() => this.setCity(item)}>{item.Name}</Text>
                                        })
                                        :
                                        <Text style={{padding: 20, textAlign: 'center', color: '#333333'}}>{convertLanguage(language, 'city_empty')}</Text>
                                    }
                                </React.Fragment>
                            }
                        </View>
                    </View>
                </SrcView>
                <Touchable disabled={!city} style={[styles.btnOk, {backgroundColor: city ? '#03a9f4' : '#E8E7E8', borderColor: city ? '#03a9f4' : '#E8E7E8'  }]} onPress={() => this.callBackCity()}>
                    <Text style={[styles.txtOk, { color: city ? '#FFFFFF' : '#BDBDBD' }]}>{!location_loading ? convertLanguage(language, 'ok') : convertLanguage(language, 'loading')+'...'}</Text>
                </Touchable>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxCountry: {
        paddingTop: 20,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8'
    },
    txtCountry: {
        fontSize: 14,
        color: '#757575',
        paddingBottom: 10
    },
    txtCountryNameActive: {
        paddingBottom: 10,
        paddingTop: 10,
        color: '#03a9f4',
        fontWeight: 'bold'
    },
    txtCountryName: {
        paddingBottom: 10,
        paddingTop: 10,
        color: '#333333',
        fontWeight: '400'
    },
    btnOk: {
        height: 48,
        backgroundColor: '#03a9f4',
        marginTop: 10,
        marginBottom: 30,
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtOk: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#ffffff'
    },
});

const mapStateToProps = state => {
    return {
        city: state.city,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onSelectCity: (slug) => {
            dispatch(actSelectCity(slug))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SelectCity);
// export default EventHostList;
