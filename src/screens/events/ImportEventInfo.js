import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataEventImport } from '../../actions/event'
import { convertLanguage } from '../../services/Helper'

class ImportEventInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1
        };
    }

    componentDidMount() {
        var { page } = this.state;
        this.props.onLoadDataEventImport(this.props.route.params['HostInfoId'], page);
    }

    _renderFooter() {
        var { loadMoreImport, is_empty } = this.props.event;
        var { language } = this.props.language;
        if(loadMoreImport) {
            return <ActivityIndicator size="large" color="#000000" style={{padding: 20, flex: 1}}/>
        } else {
            if(is_empty) {
                return <Text style = {{textAlign: 'center', paddingTop: 20}}>{convertLanguage(language, 'no_data')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { loadMoreImport } = this.props.event;
        var { page } = this.state;
        if(loadMoreImport) {
            this.props.onLoadDataEventImport(this.props.route.params['HostInfoId'], page+1);
            this.setState({page: page+1})
        }
    }

    importDataEvent(item) {
        this.props.route.params['callback'](item);
        this.props.navigation.goBack()
    }

    render() {
        var { list_event_import } = this.props.event;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                </View>

                <Line />
                <View style={styles.content}>
                    <FlatList
                        // style={{ flex: 1, width: '100%' }}
                        data={list_event_import.data}
                        renderItem={({ item }) => {
                            return <Touchable style={styles.boxEvent} onPress={() => this.importDataEvent(item)}>
                                <Image source={{uri: item.Posters.Small}} style={styles.imgThumb} />
                                <View style={styles.boxInfoEvent}>
                                    <Text style={styles.txtTitle} numberOfLines={1}>{item.Title}</Text>
                                    <Text style={styles.txtTime} numberOfLines={1}>{item.TimeStart} ~ {item.TimeFinish}</Text>
                                    <Text style={styles.txtVenue} numberOfLines={1}>{item.VenueName}</Text>
                                    <Text style={styles.txtAddress} numberOfLines={1}>{item.Address}</Text>
                                </View>
                            </Touchable>
                        }}
                        onEndReached={() => { this.loadMore()}}
                        // ListHeaderComponent={() => this.renderHeader()}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        // refreshing={this.props.team.loading}
                        // onRefresh={() => {this.refresh()}}
                        ListFooterComponent={() => this._renderFooter()}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxEvent: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8',
        paddingLeft: 20,
        paddingRight: 20
    },
    imgThumb: {
        width: 80,
        height: 80,
        marginRight: 15
    },
    boxInfoEvent: {
        flex: 1,
    },
    txtTitle: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        paddingTop: 5,
        paddingBottom: 5
    },
    txtTime: {
        fontSize: 13,
        color: '#333333',
        paddingBottom: 5
    },
    txtVenue: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 5
    },
    txtAddress: {
        fontSize: 13,
        color: '#333333',
        paddingBottom: 5
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataEventImport: (slug, page) => {
            dispatch(actLoadDataEventImport(slug, page))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ImportEventInfo);
// export default EventHostList;
