import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, ScrollView, StyleSheet, Alert, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { formatNumber } from '../../services/Helper';
import * as Colors from '../../constants/Colors';
import ModalPageWebView from '../../components/ModalPageWebView';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import Loading from '../../screens_view/Loading';
import ModalRefundRate from '../../components/event/ModalRefundRate';
import { actLoadDataHandleRefund, actRefuseRefund, actChangeComment, actHandleRefund, actHostSendCommentRefund } from '../../actions/ticket';
import { TextField } from '../../components/react-native-material-textfield';
import * as Config from '../../constants/Config';
class HandleRefund extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Comment: '',
            modalRate: false,
            modalPageWebView: false,
            uri: '',
            title: ''
        };
    }

    componentDidMount() {
        var { eventSlug, ticketDetailId } = this.props.route.params;
        var { iso_code } = this.props.global;
        this.props.onLoadDataHandleRefund(eventSlug, ticketDetailId, iso_code);
        // this.props.onLoadDataHandleRefund('cdfg', 498);
    }

    onRefuseRefund() {
        var { eventSlug, ticketDetailId } = this.props.route.params;
        var { iso_code } = this.props.global;
        var body = {
            Slug: eventSlug,
            TicketDetailId: ticketDetailId,
            iso_code
        }
        Alert.alert(
            'Refuse request',
            'Do you want to refuse this request?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.props.onRefuseRefund(body) },
            ],
            { cancelable: false },
        );
    }

    onRefund(TicketDetailId, Rate) {
        this.setState({ modalRate: false });
        var { slugEvent } = this.props.ticket.handleRefund;
        var { iso_code } = this.props.global;
        var { Comment } = this.props.ticket;
        var body = {
            Slug: slugEvent,
            TicketDetailId: TicketDetailId,
            Comment: Comment,
            Rate,
            iso_code
        }
        this.props.onHandleRefund(body);
    }

    validate() {
        var { loadingRefuseRefund, loadingPostHandleRefund } = this.props.ticket;
        if (loadingPostHandleRefund || loadingRefuseRefund) {
            return true;
        }
        return false
    }

    onHostSendCommentRefund() {
        var { Id } = this.props.ticket.handleRefund.ticketDetail;
        var { slugEvent } = this.props.ticket.handleRefund;
        var { Comment } = this.props.ticket;
        var { iso_code } = this.props.global;
        var body = {
            TicketDetailId: Id,
            Comment: Comment,
            Slug: slugEvent,
            iso_code
        }
        this.props.onHostSendCommentRefund(body, Id)
    }

    render() {
        var { language } = this.props.language;
        var { loadingHandleRefund, handleRefund, Comment, loadingRefuseRefund, loadingPostHandleRefund, loadingPostComment } = this.props.ticket;
        var { modalRate } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', paddingHorizontal: 16 }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ marginRight: 7, width: 24, height: 24 }} />
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'request_a_refund')}</Text>
                    </Touchable>
                    <Touchable onPress={() => this.setState({ modalPageWebView: true, uri: `${Config.API_URL}/pages/refund-policy`, title: convertLanguage(language, 'refund_policy') })} style={{ paddingHorizontal: 16 }}>
                        <Text style={styles.txtRefundPolicy}>{convertLanguage(language, 'refund_policy')}...</Text>
                    </Touchable>
                </View>

                <Line />
                {/* <View style={{ flex: 1 }}> */}
                {
                    (loadingRefuseRefund || loadingPostHandleRefund) &&
                    <Loading />
                }
                {
                    loadingHandleRefund ?
                        <ActivityIndicator size="large" color="#000000" style={styles.loading} />
                        :
                        <KeyboardAvoidingView
                            behavior={Platform.OS === "ios" ? "padding" : null}
                            style={{ flex: 1 }}>
                            <ScrollView style={{ flex: 1 }} containerStyle={{ paddingBottom: 20 }} keyboardShouldPersistTaps='handled' ref={ref => this.scrollView = ref}>
                                <View style={styles.content}>
                                    <View style={styles.boxTicket}>
                                        <View style={[styles.boxTicketType, { marginBottom: 15, }]}>
                                            <Image source={require('../../assets/ticket_active.png')} style={styles.icon} />
                                            <Text style={styles.txtTicketName}>{handleRefund.ticket.Name}</Text>
                                        </View>
                                        <View style={[styles.boxTicketType, { justifyContent: 'space-between', marginBottom: 25 }]}>
                                            <Text style={styles.txtTicketPrice}>{formatNumber(handleRefund.ticket.Price)} {handleRefund.ticket.Unit && handleRefund.ticket.Unit.toUpperCase()}</Text>
                                            <View style={styles.boxTicketType}>
                                                {
                                                    handleRefund.ticket.Type === 'd-p-ticket'
                                                        ?
                                                        <Image source={require('../../assets/ic_delivery_brown.png')} style={styles.ic_delivery} />
                                                        :
                                                        <Image source={require('../../assets/ic_eticket.png')} style={styles.ic_eticket} />
                                                }
                                                <Text style={styles.txtDelivery}>{handleRefund.ticket.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket') : convertLanguage(language, 'e_ticket')}</Text>
                                            </View>
                                        </View>
                                        {
                                            handleRefund.ticket.Description != ''
                                            &&
                                            <Text style={styles.txtTicketDes}>{handleRefund.ticket.Description}</Text>
                                        }
                                    </View>
                                    <View style={{ flexDirection: 'row', paddingTop: 16, borderBottomWidth: 1, borderBottomColor: '#BDBDBD', paddingBottom: 32 }}>
                                        <View
                                            style={{
                                                marginRight: 10,
                                                height: 57,
                                                borderRadius: 4,
                                                paddingHorizontal: 12,
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                justifyContent: 'space-between',
                                                backgroundColor: '#F2F2F2',
                                                width: 130
                                            }}>
                                            <View style={{ justifyContent: 'space-between', }}>
                                                <Text style={{ color: '#828282', fontSize: 12, }}>{convertLanguage(language, 'country')}</Text>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={handleRefund.refund.Zipcode === '+84' ? require('../../assets/flag_vn.png') : require('../../assets/flag_kr.png')} style={styles.ic_country} />
                                                    <Text style={styles.txtCountry}>{handleRefund.refund.Zipcode}</Text>
                                                </View>
                                            </View>
                                            <Image source={require('../../assets/down_arrow_active.png')} style={{
                                                width: 13,
                                                height: 11,
                                            }} />
                                        </View>
                                        <TextField
                                            disabled={true}
                                            autoCorrect={false}
                                            enablesReturnKeyAutomatically={true}
                                            value={handleRefund.refund.Phone}
                                            onChangeText={(Phone) => this.setState({})}
                                            // error={errors.Phone}
                                            disabledLineWidth={0}
                                            returnKeyType='next'
                                            label={convertLanguage(language, 'phone')}
                                            baseColor={'#828282'}
                                            tintColor={'#828282'}
                                            errorColor={'#EB5757'}
                                            inputContainerStyle={[styles.inputContainerStyle]}
                                            containerStyle={[styles.containerStyle, { paddingHorizontal: 0 }]}
                                            labelHeight={25}
                                            labelTextStyle={{ paddingBottom: 15 }}
                                            lineWidth={2}
                                            selectionColor={'#3a3a3a'}
                                            style={styles.input}
                                            labelTextStyle={{ paddingLeft: 12 }}
                                            errorImage={require('../../assets/error.png')}
                                            keyboardType="numeric"
                                        />
                                    </View>
                                    {
                                        handleRefund.refundComments.length > 0 &&
                                        <View style={styles.boxChat}>
                                            {
                                                handleRefund.refundComments.map((item, index) => {
                                                    if (item.Role == "host") {
                                                        if (item.Status == "-2") {
                                                            return <View style={[styles.boxOtherChat, { alignSelf: 'center', paddingRight: 8 }]} key={index}>
                                                                <View style={styles.boxContentOtherChat}>
                                                                    {
                                                                        handleRefund.eventPoster &&
                                                                        <Image source={{ uri: handleRefund.eventPoster.Small }} style={[styles.avatar, { marginRight: 8 }]} />
                                                                    }
                                                                    <Text style={[styles.txtCancelRefund, { marginRight: 48 }]}>{convertLanguage(language, 'the_host_refuse')}</Text>
                                                                </View>
                                                                <Text style={[styles.txtTimeCancel, { textAlign: 'right' }]}>{item.CreatedAt}</Text>
                                                            </View>
                                                        } else {
                                                            return <View style={styles.boxMyChat} key={index}>
                                                                <View style={styles.boxContentMyChat}>
                                                                    <Text style={styles.txtChat}>{item.Comment}</Text>
                                                                    {
                                                                        <Image source={{ uri: handleRefund.eventPoster ? handleRefund.eventPoster.Small : '' }} style={styles.avatar} />
                                                                    }
                                                                </View>
                                                                <Text style={styles.txtTime}>{item.CreatedAt}</Text>
                                                            </View>
                                                        }
                                                    } else {
                                                        if (item.Status == "-1") {
                                                            return <View style={[styles.boxOtherChat, { alignSelf: 'center', paddingRight: 8 }]} key={index}>
                                                                <View style={styles.boxContentOtherChat}>
                                                                    {
                                                                        handleRefund.clientUser && handleRefund.clientUser.Avatars &&
                                                                        <Image source={{ uri: handleRefund.clientUser.Avatars.Small }} style={styles.avatarEvent} />
                                                                    }
                                                                    <Text style={[styles.txtCancelRefund, { marginRight: 48 }]}>{convertLanguage(language, 'customer_have_canceled_the_refund_request')}</Text>
                                                                </View>
                                                                <Text style={[styles.txtTimeCancel, { textAlign: 'right' }]}>{item.CreatedAt}</Text>
                                                            </View>
                                                        } else {
                                                            return <View style={styles.boxOtherChat} key={index}>
                                                                <View style={styles.boxContentOtherChat}>
                                                                    {
                                                                        handleRefund.clientUser && handleRefund.clientUser.Avatars &&
                                                                        <Image source={{ uri: handleRefund.clientUser.Avatars.Small }} style={styles.avatarEvent} />
                                                                    }
                                                                    <Text style={[styles.txtChat, { color: '#333333' }]}>{item.Comment}</Text>
                                                                </View>
                                                                <Text style={styles.txtTime}>{item.CreatedAt}</Text>
                                                            </View>
                                                        }
                                                    }
                                                })
                                            }
                                        </View>
                                    }
                                    {
                                        (handleRefund.ticketDetail.Status == -2 || handleRefund.ticketDetail.Status == -6) &&
                                        <View style={styles.boxAccess}>
                                            <Text style={styles.txtAcceptedTitle}>{convertLanguage(language, 'title_accept_request')} <Text style={{ color: '#00A9F4' }}>{handleRefund.refund.Rate}%</Text> {convertLanguage(language, 'total_bill')}</Text>
                                            <Text style={styles.txtAcceptedLable}>{convertLanguage(language, 'some_fee')} <Text style={{ color: '#00A9F4' }}>0 {handleRefund.ticketDetail.Ticket && handleRefund.ticketDetail.Ticket.Unit.toUpperCase()}</Text></Text>
                                            <Text style={styles.txtAcceptedLable}>{convertLanguage(language, 'total_amount_refund')} <Text style={{ color: '#00A9F4' }}>{formatNumber(handleRefund.refund.PriceRefund)} {handleRefund.ticketDetail.Ticket && handleRefund.ticketDetail.Ticket.Unit.toUpperCase()}</Text></Text>
                                        </View>
                                    }
                                    <View style={styles.boxInput}>
                                        <TextInput
                                            style={styles.ipMessage}
                                            selectionColor="#4F4F4F"
                                            name="Comment"
                                            placeholder={convertLanguage(language, 'write_a_message')}
                                            multiline={true}
                                            value={this.props.ticket.Comment}
                                            onChangeText={(Comment) => this.props.onChangeComment(Comment)}
                                            onFocus={() => this.scrollView.scrollToEnd({ animated: true })}
                                            editable={!loadingPostComment}
                                        />
                                        <Touchable style={[styles.btnSend, { backgroundColor: !Comment ? '#E0E2E8' : '#00A9F4' }]} onPress={() => this.onHostSendCommentRefund()} disabled={!Comment || loadingPostComment}>
                                            <Text style={[styles.txtSend, { color: !Comment ? '#bdbdbd' : '#FFFFFF' }]}>{loadingPostComment ? convertLanguage(language, 'loading') + '...' : convertLanguage(language, 'send')}</Text>
                                        </Touchable>
                                    </View>
                                    <View style={styles.boxInfo}>
                                        <Text style={styles.txtInfo}>{convertLanguage(language, 'text_in_refund1')}</Text>
                                        <Text style={styles.txtInfo}>{convertLanguage(language, 'text_in_refund2')}</Text>
                                    </View>
                                    <View style={styles.boxAction}>
                                        <Touchable onPress={() => this.onRefuseRefund()} disabled={loadingRefuseRefund || ![-1].includes(handleRefund.ticketDetail.Status)} style={[styles.btnRequestRefund, { backgroundColor: ![-1].includes(handleRefund.ticketDetail.Status) ? '#e8e8e8' : 'transparent', borderColor: ![-1].includes(handleRefund.ticketDetail.Status) ? '#e8e8e8' : '#4F4F4F' }]}>
                                            <Text style={[styles.txtRequestRefund, { color: ![-1].includes(handleRefund.ticketDetail.Status) ? '#bdbdbd' : '#4F4F4F' }]}>{loadingRefuseRefund ? convertLanguage(language, 'loading') + '...' : convertLanguage(language, 'refuse')}</Text>
                                        </Touchable>
                                        <Touchable style={[styles.btnCancel, [-1].includes(handleRefund.ticketDetail.Status) ? {} : { backgroundColor: '#e8e8e8', borderWidth: 0 }]} disabled={loadingPostHandleRefund || ![-1].includes(handleRefund.ticketDetail.Status)} onPress={() => this.setState({ modalRate: true })}>
                                            <Text style={[styles.txtCancel, [-1].includes(handleRefund.ticketDetail.Status) ? {} : { color: '#bdbdbd' }]}>{convertLanguage(language, 'accept_refund')}</Text>
                                        </Touchable>
                                    </View>
                                </View>
                            </ScrollView>
                        </KeyboardAvoidingView>
                }
                {/* </View> */}
                {
                    this.state.modalPageWebView
                        ?
                        <ModalPageWebView
                            closeModal={() => this.setState({ modalPageWebView: false })}
                            navigation={this.props.navigation}
                            uri={this.state.uri}
                            title={this.state.title}
                        />
                        : null
                }
                {
                    modalRate
                        ?
                        <ModalRefundRate
                            modalVisible={modalRate}
                            closeModal={() => this.setState({ modalRate: false })}
                            onRefund={(Rate) => this.onRefund(handleRefund.ticketDetail.Id, Rate)}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20
    },
    boxTicket: {
        paddingTop: 25,
        borderBottomColor: 'rgba(51, 51, 51, 0.25)',
        borderBottomWidth: 1
    },
    txtTicketName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    txtTicketDes: {
        fontSize: 13,
        color: '#757575',
        marginBottom: 20,
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxTicketType: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ic_delivery: {
        width: 16,
        height: 16,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDelivery: {
        fontSize: 12,
        color: '#4F4F4F'
    },
    boxInput: {
        flexDirection: 'row',
        paddingTop: 16,
    },
    boxChat: {
        marginTop: 16,
        paddingBottom: 32,
        borderBottomColor: '#BDBDBD',
        borderBottomWidth: 1
    },
    boxMyChat: {
        // flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        marginTop: 10,
    },
    txtTime: {
        color: '#757575',
        fontSize: 8,
        marginTop: 5,
        marginBottom: 2,
        // marginLeft: 5,
    },
    boxContentMyChat: {
        // maxWidth: 245,
        padding: 10,
        backgroundColor: '#F2F2F2',
        borderRadius: 4,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    txtChat: {
        fontSize: 14,
        color: '#333333',
        maxWidth: 200
        // flex: 1
    },
    boxOtherChat: {
        // flexDirection: 'row',
        // alignItems: 'flex-end',
        alignSelf: 'flex-start',
        marginTop: 10
    },
    boxContentOtherChat: {
        maxWidth: 245,
        padding: 10,
        backgroundColor: '#f2f2f2',
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center'
    },
    ipMessage: {
        height: 76,
        fontSize: 13,
        color: '#4F4F4F',
        textAlignVertical: 'top',
        padding: 10,
        marginBottom: 30,
        flex: 1,
        backgroundColor: '#F2F2F2',
        borderRadius: 5
    },
    boxInfo: {
        marginBottom: 20,
        alignItems: 'center',
        marginTop: 16
    },
    txtInfo: {
        fontSize: 14,
        color: '#333333',
        marginBottom: 3
    },
    boxAction: {
        marginBottom: 20,
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btnCancel: {
        flex: 0.45,
        alignItems: 'center',
        justifyContent: 'center',
        height: 42,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#4F4F4F'
    },
    txtCancel: {
        color: '#4F4F4F',
        fontSize: 16,
    },
    btnRequestRefund: {
        flex: 0.45,
        alignItems: 'center',
        justifyContent: 'center',
        height: 42,
        borderRadius: 5,
        borderWidth: 1
    },
    txtRequestRefund: {
        color: '#bdbdbd',
        fontSize: 16,
    },
    loading: {
        padding: 20
    },
    boxStatus: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 10
    },
    txtCancelRefund: {
        fontSize: 14,
        color: 'red',
        marginRight: 5
    },
    txtTimeCancel: {
        color: '#EB5757',
        fontSize: 12,
    },
    avatar: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: '#bdbdbd',
        marginLeft: 5
    },
    avatarEvent: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: '#bdbdbd',
        marginRight: 5
    },
    boxAccess: {
        marginTop: 16,
        paddingBottom: 24,
        borderBottomColor: '#bdbdbd',
        borderBottomWidth: 1
    },
    txtRequestAccepted: {
        textAlign: 'center',
        fontSize: 14,
        color: '#00A9F4',
        marginBottom: 16
    },
    txtAcceptedTitle: {
        fontSize: 16,
        // fontWeight: 'bold',
        color: '#4F4F4F'
    },
    txtAcceptedLable: {
        fontSize: 16,
        color: '#4F4F4F',
        fontWeight: 'bold'
    },
    btnAccept: {
        // width: '100%',
        backgroundColor: '#00A9F4',
        borderRadius: 4,
        // alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 16,
        height: 36
    },
    txtAccepted: {
        fontSize: 16,
        color: '#FFFFFF',
    },
    arrow_right: {
        width: 18,
        height: 18,
        marginLeft: 10
    },
    btnReview: {
        borderWidth: 1,
        borderColor: '#00A9F4',
        borderRadius: 4,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 16
    },
    txtReview: {
        fontSize: 16,
        color: '#00A9F4'
    },
    inputContainerStyle: {
        // borderWidth: 1,
        // borderColor: '#bdbdbd',
        borderRadius: 4,
        paddingHorizontal: 12,
        backgroundColor: '#F2F2F2',
        flex: 1,
    },
    containerStyle: {
        paddingHorizontal: 20,
        flex: 1
    },
    input: {
        color: '#333333',
    },
    icon: {
        width: 30,
        height: 30,
        marginRight: 7
    },
    txtRefundPolicy: {
        color: '#00A9F4',
        fontSize: 16
    },
    btnSend: {
        backgroundColor: '#00A9F4',
        height: 36,
        borderRadius: 5,
        marginLeft: 16,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 18
    },
    txtSend: {
        fontSize: 16,
        color: '#FFFFFF'
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 12
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        ticket: state.ticket,
        user: state.user,
        event: state.event,
        global: state.global
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataHandleRefund: (EventSlug, TicketDetailId, iso_code) => {
            dispatch(actLoadDataHandleRefund(EventSlug, TicketDetailId, iso_code))
        },
        onRefuseRefund: (body) => {
            dispatch(actRefuseRefund(body))
        },
        onChangeComment: (Comment) => {
            dispatch(actChangeComment(Comment))
        },
        onHandleRefund: (body) => {
            dispatch(actHandleRefund(body))
        },
        onHostSendCommentRefund: (body, Id) => {
            dispatch(actHostSendCommentRefund(body, Id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HandleRefund);
