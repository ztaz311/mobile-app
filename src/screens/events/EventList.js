import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import ItemEvent from '../../components/event/ItemEvent'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import ModalCity from '../../components/ModalCity'
import ModalTime from '../../components/ModalTime'
import ModalTag from '../../components/ModalTag'
import { actLoadDataListEvent, actLikeEvent, actLoadBannerEvent, actClearEventList } from '../../actions/event'
import { actLoadDataCategory } from '../../actions/category'
import { actSelectCountryByEvent } from '../../actions/city'
const { width, height } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import Dots from 'react-native-dots-pagination';
import { convertLanguage } from '../../services/Helper';
import FastImage from 'react-native-fast-image';
class EventList extends Component {
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            scroll: true,
            page: 1,
            activeSlide: 0,
            data_times: [
                {
                    name_show: convertLanguage(language, 'all'),
                    name: convertLanguage(language, 'time'),
                    value: ''
                },
                {
                    name_show: convertLanguage(language, 'today'),
                    name: convertLanguage(language, 'today'),
                    value: 'today'
                },
                {
                    name_show: convertLanguage(language, 'tomorrow'),
                    name: convertLanguage(language, 'tomorrow'),
                    value: 'tomorrow'
                },
                {
                    name_show: convertLanguage(language, 'week'),
                    name: convertLanguage(language, 'week'),
                    value: 'week'
                },
                {
                    name_show: convertLanguage(language, 'weekend'),
                    name: convertLanguage(language, 'weekend'),
                    value: 'weekend'
                },
                {
                    name_show: convertLanguage(language, 'custom_period'),
                    name: convertLanguage(language, 'custom'),
                    value: 'custom',
                    start: '',
                    finish: '',
                }
            ],
            time_event: {
                name_show: convertLanguage(language, 'all'),
                name: convertLanguage(language, 'all'),
                value: ''
            },
            tag: this.props.route.params && this.props.route.params['tag'] ? this.props.route.params['tag'] : {
                name: convertLanguage(language, 'all'),
                value: ''
            },
            area: this.props.global.city.Id ? this.props.global.city : this.props.global.country,
            type: this.props.global.city.Id ? 'city' : 'country',
            modalCity: false,
            modalTime: false,
            modalTag: false,
        };
    }

    componentWillUnmount() {
        this.props.clearEventList()
    }

    componentDidMount() {
        var { page, time_event, tag, type, area } = this.state;
        this.props.getBannerEvent()
        this.props.onLoadDataListEvent(page, type === 'city' ? area.Id : area.Code, time_event.value, tag.value, '', '', type);
        this.props.onLoadDataCategory('event');
        var { event_countries } = this.props.city;
        if (event_countries.length === 0) {
            this.props.onLoadDataCountry();
        }
    }

    showDataCategory() {
        var { categories } = this.props.category;
        var result = [{
            name: 'All',
            value: ''
        }];
        categories.forEach(category => {
            result.push({
                name: category.HashTagName,
                value: category.Id
            })
        });
        return result;
    }

    renderHeader = () => {
        var { language } = this.props.language;
        var { area, time_event, tag, activeSlide } = this.state;
        var { banners } = this.props.event;
        return <View>
            {
                banners.length > 0 &&
                <View>
                    <Carousel
                        data={banners}
                        renderItem={({ item, index }) => {
                            return <Touchable onPress={() => Linking.openURL(item.LinkApp)} disabled={item.LinkApp === '#'}><FastImage style={{ backgroundColor: '#bcbcbc', aspectRatio: 2.4 }} source={{ uri: item.Images ? item.Images.Medium : item.Image }} /></Touchable>
                        }}
                        sliderWidth={width}
                        itemWidth={width}
                        inactiveSlideScale={1}
                        onSnapToItem={(index) => this.setState({ activeSlide: index })}
                        autoplay={true}
                        autoplayInterval={3000}
                        loop
                    />
                    {
                        banners.length > 1 &&
                        <View style={{ position: 'absolute', bottom: 0, alignSelf: 'center' }}>
                            <Dots length={banners.length} active={activeSlide} activeColor={'#FFFFFF'} width={100} passiveDotWidth={6} passiveDotHeight={6} activeDotWidth={9} activeDotHeight={9} paddingVertical={6} passiveColor={'rgba(255, 255, 255, 0.5)'} marginHorizontal={3} />
                        </View>
                    }
                </View>
            }
            <View style={styles.boxFilter}>
                <View style={styles.boxFilter1}>
                    <Touchable style={[styles.boxItemFilter, { flex: 0.47 }]} onPress={() => this.setState({ modalCity: true })}>
                        <Image source={require('../../assets/ic_location_blue.png')} style={styles.ic_location} />
                        <Text style={styles.txtFilter} numberOfLines={1}>{area['Name_' + language]}</Text>
                        <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                    </Touchable>
                    <Touchable style={[styles.boxItemFilter, { flex: 0.47 }]} onPress={() => this.setState({ modalTime: true })}>
                        <Image source={require('../../assets/ic_calenda_blue.png')} style={styles.ic_location} />
                        <Text style={styles.txtFilter} numberOfLines={1}>{time_event.name}</Text>
                        <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                    </Touchable>
                </View>
                <Touchable style={styles.boxItemFilter} onPress={() => this.setState({ modalTag: true })}>
                    <Image source={require('../../assets/ic_category_blue.png')} style={styles.ic_location} />
                    <Text style={styles.txtFilter} numberOfLines={1}>{tag.name}</Text>
                    <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                </Touchable>
            </View>
        </View>
    }

    _renderFooter = () => {
        var { loadMore, is_empty } = this.props.event;
        var { language } = this.props.language;
        if (loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'event_empty')}</Text>
            } else {
                return null
            }
        }
    }

    loadMore() {
        var { loadMore } = this.props.event;
        var { page, time_event, tag, type, area } = this.state;
        if (loadMore) {
            var start = time_event.value === 'custom' ? time_event.start : '';
            var finish = time_event.value === 'custom' ? time_event.finish : '';
            this.props.onLoadDataListEvent(page + 1, type === 'city' ? area.Id : area.Code, time_event.value, tag.value, start, finish, type);
            this.setState({ page: page + 1 })
        }
    }

    async onSelect(key, value) {
        await this.setState({
            [key]: value,
            page: 1,
            modalCity: false,
            modalTime: false,
            modalTag: false,
            type: key === 'area' ? 'city' : this.state.type
        })
        var { page, time_event, tag, type, area } = this.state;
        var start = time_event.value === 'custom' ? time_event.start : '';
        var finish = time_event.value === 'custom' ? time_event.finish : '';
        this.props.onLoadDataListEvent(page, type === 'city' ? area.Id : area.Code, time_event.value, tag.value, start, finish, type);
    }

    goBack = () => {
        var { index, routes } = this.props.navigation.dangerouslyGetState();
        if (routes.length > 1 && ['Login', 'LoginWithMail', 'ForgotPw', 'Splash'].includes(routes[index - 1].name)) {
            this.props.navigation.reset({ index: 0, routes: [{ name: 'Main' }], })
        } else {
            this.props.navigation.goBack();
        }
    }

    render() {
        var { list_events } = this.props.event;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={this.goBack}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: '500', color: '#333333' }}>{convertLanguage(language, 'event')}</Text>
                    <Touchable style={{ minHeight: 48, minWidth: 48, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')}>
                        <Image source={require('../../assets/search.png')} style={{ width: 32, height: 32 }} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    <FlatList
                        // style={{ flex: 1, width: '100%' }}
                        data={list_events}
                        renderItem={({ item }) => {
                            return <ItemEvent language={language} cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} />
                        }}
                        onEndReached={() => { this.loadMore() }}
                        ListHeaderComponent={this.renderHeader}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        // refreshing={this.props.team.loading}
                        // onRefresh={() => {this.refresh()}}
                        ListFooterComponent={this._renderFooter}
                        onScrollBeginDrag={(event) => { this.setState({ scroll: false }) }}
                        onScrollEndDrag={(event) => {
                            this.setState({ scroll: true })
                        }}
                        onMomentumScrollBegin={(event) => { this.setState({ scroll: false }) }}
                        onMomentumScrollEnd={(event) => {
                            this.setState({ scroll: true })
                        }}
                    />
                    {
                        list_events.length > 0 && this.state.scroll &&
                        <Touchable style={styles.boxViewMaps} onPress={() => this.props.navigation.navigate('EventMaps', { area: this.state.area, type: this.state.type, time_event: this.state.time_event, tag: this.state.tag })}>
                            <Image source={require('../../assets/pin_white.png')} style={styles.pinIcon} />
                            <Text style={styles.txtViewMap}>{convertLanguage(language, 'view_map')}</Text>
                        </Touchable>
                    }
                </View>
                {
                    this.state.modalCity && this.props.city.event_countries.length > 0
                        ?
                        <ModalCity
                            modalVisible={this.state.modalCity}
                            closeModal={() => this.setState({ modalCity: false })}
                            countries={this.props.city.event_countries}
                            selectCity={(city) => this.onSelect('area', city)}
                        />
                        : null
                }
                {
                    this.state.modalTime
                        ?
                        <ModalTime
                            modalVisible={this.state.modalTime}
                            closeModal={() => this.setState({ modalTime: false })}
                            times={this.state.data_times}
                            selectTime={(time) => this.onSelect('time_event', time)}
                        />
                        : null
                }
                {
                    this.state.modalTag
                        ?
                        <ModalTag
                            modalVisible={this.state.modalTag}
                            closeModal={() => this.setState({ modalTag: false })}
                            tags={this.showDataCategory()}
                            selectTag={(tag) => this.onSelect('tag', tag)}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    boxPastTicket: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        height: 30,
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        marginTop: 20,
        marginBottom: 20
    },
    content: {
        flex: 1,
    },
    imgBanner: {
        width: width,
        height: width * 9 / 16,
        marginBottom: 20,
    },
    boxFilter: {
        padding: 16,
    },
    boxFilter1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 10
    },
    boxViewMaps: {
        borderRadius: 32,
        shadowOffset: { width: 0, height: 6 },
        shadowColor: 'black',
        shadowRadius: 9,
        shadowOpacity: 0.3,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 5,
        position: 'absolute',
        bottom: 20,
        alignSelf: 'center',
        backgroundColor: '#00A9F4',
        flexDirection: 'row',
        height: 48,
        paddingHorizontal: 12
    },
    pinIcon: {
        width: 24,
        height: 24,
        marginRight: 8
    },
    txtViewMap: {
        color: '#FFFFFF',
        fontSize: 16
    },
    boxItemFilter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 36,
        borderRadius: 8,
        borderColor: '#BDBDBD',
        borderWidth: 1,
        paddingHorizontal: 10
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 16,
        height: 16
    },
    ic_location: {
        width: 20,
        height: 20
    }
});

const mapStateToProps = state => {
    return {
        event: state.event,
        category: state.category,
        city: state.city,
        language: state.language,
        global: state.global
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataListEvent: (page, area, time_event, tag, start, finish, type) => {
            dispatch(actLoadDataListEvent(page, area, time_event, tag, start, finish, type))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onLoadDataCategory: (type) => {
            dispatch(actLoadDataCategory(type))
        },
        getBannerEvent: () => {
            dispatch(actLoadBannerEvent())
        },
        onLoadDataCountry: () => {
            dispatch(actSelectCountryByEvent())
        },
        clearEventList: () => {
            dispatch(actClearEventList())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventList);
// export default EventHostList;
