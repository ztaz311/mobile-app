import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView, Dimensions, TouchableOpacity } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import ItemTeamColla from '../../components/event/ItemTeamColla'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataTeamApply } from '../../actions/event';
import { convertLanguage } from '../../services/Helper';
import Modal from "react-native-modal";
import ExtraDimensions from 'react-native-extra-dimensions-android';
const { width, height } = Dimensions.get('window')
class EventApplyColla extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1
        };
    }

    componentDidMount() {
        var { page } = this.state;
        this.props.onLoadDataTeamApply(this.props.Slug, page);
    }

    _renderFooter() {
        var { loadMoreEventApply, is_empty } = this.props.event;
        var { language } = this.props.language;
        if (loadMoreEventApply) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'team_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { loadMoreEventApply } = this.props.event;
        var { page } = this.state;
        if (loadMoreEventApply) {
            this.props.onLoadDataTeamApply(this.props.Slug, page + 1);
            this.setState({ page: page + 1 })
        }
    }

    renderHeader() {
        var { language } = this.props.language;
        return <Text style={{ color: '#4F4F4F', fontSize: 11, paddingHorizontal: 27, textAlign: 'center', paddingBottom: 10 }}>{convertLanguage(language, 'apply_for_collaborating')}</Text>

    }

    render() {
        var { list_team_apply } = this.props.event;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                style={{ marginTop: 100, marginBottom: 100, }}
            >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <Touchable style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </Touchable>
                    </View>
                    <FlatList
                        style={styles.content}
                        data={list_team_apply}
                        renderItem={({ item }) => {
                            return <ItemTeamColla data={item} navigation={this.props.navigation} Slug={this.props.Slug} closeModal={() => this.props.closeModal()} />
                        }}
                        onEndReached={() => { this.loadMore() }}
                        ListHeaderComponent={() => this.renderHeader()}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        // refreshing={this.props.team.loading}
                        // onRefresh={() => {this.refresh()}}
                        ListFooterComponent={() => this._renderFooter()}
                    />
                </SafeView>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 10
    },
    content: {
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 16,
        marginBottom: 16
    },
    boxCountLike: {
        marginTop: 15,
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    icLike: {
        width: 25,
        height: 23,
        marginRight: 10
    },
    txtLike: {
        fontSize: 14,
        color: '#757575',
        fontWeight: 'bold'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        zIndex: 999999
    },
    btnClose: {
        padding: 12,
    },
    iconClose: {
        width: 30,
        height: 30
    }
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataTeamApply: (slug, page) => {
            dispatch(actLoadDataTeamApply(slug, page))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventApplyColla);
// export default EventHostList;
