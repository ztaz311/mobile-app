import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Alert, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actDataWithdrawByTeam, actDeleteWithdrawImport } from '../../actions/event'
import { convertLanguage } from '../../services/Helper'

class ImportWithdraw extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            index: '',
            item: ''
        };
    }

    componentDidMount() {
        // this.props.getDataWithdraw(this.props.event.event.TeamId)
    }

    selectWithdraw(index, item) {
        this.setState({
            index,
            item
        })
    }

    apply() {
        var { item } = this.state;
        this.props.route.params['callback'](item);
        this.props.navigation.goBack()
    }

    onDeleteWithdrawImport(Id) {
        var { language } = this.props.language;
        Alert.alert(
            convertLanguage(language, 'warning'),
            convertLanguage(language, 'delete_warning'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => this.props.onDeleteWithdrawImport(Id) },
            ],
            { cancelable: false },
        );
    }

    render() {
        var { importWithdraws, loadingImportWithdraw, dataWithdraw } = this.props.event;
        var { language } = this.props.language;
        var { index } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: index == '' ? '#e8e8e8' : '#03a9f4', marginRight: 20 }} onPress={() => this.apply()}>
                        <Text style={{ fontSize: 17, color: index == '' ? '#bdbdbd' : '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'apply')}</Text>
                    </Touchable>
                </View>

                <Line />
                <View style={styles.content}>
                    {
                        loadingImportWithdraw ?
                            <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                            :
                            <ScrollView style={{ paddingLeft: 20, paddingRight: 20}}>
                                <View style={{ paddingTop: 20, paddingBottom: 20 }}>
                                    {
                                        dataWithdraw.import.length > 0
                                        ?
                                        dataWithdraw.import.map((item, i) => {
                                            return <Touchable style={[styles.boxInfo]} onPress={() => this.selectWithdraw(item.Id, item)} key={i}>
                                                <View style={[styles.boxDataInfo, { borderWidth: item.Id === index ? 2 : 1, borderColor: item.Id === index ? '#03a9f4' : '#bdbdbd' }]}>
                                                    <View style={{flex: 1}}>
                                                        <View style={styles.boxInput}>
                                                            <Text style={styles.txtInput}>{convertLanguage(language, 'name')}</Text>
                                                            <View style={styles.boxInputContent}>
                                                                <Text style={styles.txtContent}>{item.Name}</Text>
                                                            </View>
                                                        </View>
                                                        {
                                                            item.WithdrawType == 'company' &&
                                                            <View style={styles.boxInput}>
                                                                <Text style={styles.txtInput}>{convertLanguage(language, 'ceo')}</Text>
                                                                <View style={styles.boxInputContent}>
                                                                    <Text style={styles.txtContent}>{item.CEO}</Text>
                                                                </View>
                                                            </View>
                                                        }
                                                        {
                                                            item.WithdrawType == 'company' &&
                                                            <View style={styles.boxInput}>
                                                                <Text style={styles.txtInput}>{convertLanguage(language, 'registration_no')}</Text>
                                                                <View style={styles.boxInputContent}>
                                                                    <Text style={styles.txtContent}>{item.RegistrationNo}</Text>
                                                                </View>
                                                            </View>
                                                        }
                                                        {
                                                            item.WithdrawType == 'company' &&
                                                            <View style={styles.boxInput}>
                                                                <Text style={styles.txtInput}>{convertLanguage(language, 'address')}</Text>
                                                                <View style={styles.boxInputContent}>
                                                                    <Text style={styles.txtContent}>{item.Address}</Text>
                                                                </View>
                                                            </View>
                                                        }
                                                        <View style={styles.boxInput}>
                                                            <Text style={styles.txtInput}>{convertLanguage(language, 'phone')}</Text>
                                                            <View style={styles.boxInputContent}>
                                                                <Text style={styles.txtContent}>{item.Phone}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.boxInput}>
                                                            <Text style={styles.txtInput}>{convertLanguage(language, 'email')}</Text>
                                                            <View style={styles.boxInputContent}>
                                                                <Text style={styles.txtContent}>{item.Email}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.boxInput}>
                                                            <Text style={styles.txtInput}>{convertLanguage(language, 'bank')}</Text>
                                                            <View style={styles.boxInputContent}>
                                                                <Text style={styles.txtContent}>{item.Bank}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.boxInput}>
                                                            <Text style={styles.txtInput}>{convertLanguage(language, 'account')}</Text>
                                                            <View style={styles.boxInputContent}>
                                                                <Text style={styles.txtContent}>{item.AccountNo}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.boxInput}>
                                                            <Text style={styles.txtInput}>{convertLanguage(language, 'holder')}</Text>
                                                            <View style={styles.boxInputContent}>
                                                                <Text style={styles.txtContent}>{item.AccountHolder}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    <Touchable style={styles.btnClose} onPress={() => this.onDeleteWithdrawImport(item.Id)} >
                                                        <Image style={styles.iconClose} source={require('../../assets/X_icon.png')} />
                                                    </Touchable>
                                                </View>
                                            </Touchable>
                                        })
                                        :
                                        <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'host_info_empty')}</Text>
                                    }
                                </View>
                            </ScrollView>
                    }
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#e8e8e8'
    },
    boxInput: {
        paddingBottom: 20,
        flex: 1
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 7,
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxInputContent: {
        justifyContent: 'center',
    },
    boxInfo: {
        marginBottom: 15,
    },
    btnImport: {
        width: 60,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
    },
    boxDataInfo: {
        flex: 1,
        padding: 20,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    txtImport: {
        fontSize: 13,
        color: '#333333'
    },
    btnClose: {
        paddingLeft: 12,
        paddingBottom: 12
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getDataWithdraw: (slug, page) => {
            dispatch(actDataWithdrawByTeam(slug, page))
        },
        onDeleteWithdrawImport: (Id) => {
            dispatch(actDeleteWithdrawImport(Id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ImportWithdraw);
// export default EventHostList;
