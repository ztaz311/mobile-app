import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Alert, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataMembers, actUnInviteUser, actSetEventRole, actLeaveEvent } from '../../actions/event'
import ModalSetRole from '../../components/event/ModalSetRole1.';
class EventStaff extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalSetRole: false,
            item: {}
        };
    }


    componentDidMount() {
        this.props.onLoadDataMembers(this.props.route.params['Slug'], this.props.route.params['notification']);
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    submitLeaveEvent(member) {
        var { user } = this.props
        var title = ''
        var content = ''
        if (user.profile.Id == member.Id) {
            title = 'leave_event'
            content = 'are_you_sure_to_leave_event'
        } else {
            title = 'discharge'
            content = 'sure_to_kick_this_member_event'
        }
        var { language } = this.props.language;
        Alert.alert(
            convertLanguage(language, title),
            convertLanguage(language, content),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: convertLanguage(language, 'ok'), onPress: () => {
                        this.props.onLeaveEvent(this.props.event.event.Id, member)
                        this.toggleModal(false)
                    },
                }
            ],
            { cancelable: false },
        );

    }


    showModalSet = (member) => {
        this.setState({
            modalSetRole: true,
            item: member
        })
    }
    toggleModal = (value) => {
        this.setState({ modalSetRole: value })
    }

    render() {
        var { members, memberInvited, loadingMember, event } = this.props.event;
        var { profile } = this.props.user;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'staff')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                {
                    loadingMember
                        ?
                        <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                        :
                        <ScrollView style={styles.content}>
                            <View style={styles.boxMembers}>
                                <Text style={styles.txtMembers}>{members.length} {convertLanguage(language, 'member')}</Text>
                                <Touchable style={styles.btnInvite} onPress={() => this.props.navigation.navigate('EventInviteStaff')}>
                                    <Text style={styles.txtInvite}>{convertLanguage(language, 'invite')}</Text>
                                </Touchable>
                            </View>
                            <View style={styles.boxListMember}>
                                {
                                    members.length > 0
                                        ?
                                        members.map((member, index) => {
                                            if (member) return <View style={styles.boxItem} key={index}>
                                                <View style={styles.boxInfo}>
                                                    <Image source={{ uri: member.Avatars.Small }} style={styles.imgAvatar} />
                                                    <View style={styles.boxInfoDetail}>
                                                        <Text style={styles.txtName}>{member.Name}</Text>
                                                        <Text style={styles.txtId}>{member.UserId}</Text>
                                                        <Text style={styles.txtRole}>{convertLanguage(language, member.Role.toLowerCase())}</Text>
                                                    </View>
                                                </View>
                                                <Touchable style={styles.boxAction} onPress={profile.Id == member.Id ? () => this.submitLeaveEvent(member) : () => this.showModalSet(member)}>
                                                    <Image source={profile.Id == member.Id ? require('../../assets/leave_icon.png') : require('../../assets/set_role.png')} style={styles.imgClose} />
                                                </Touchable>

                                            </View>
                                        })
                                        :

                                        <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'no_data')}</Text>
                                }
                            </View>
                            <View style={styles.boxPending}>
                                <Text style={styles.txtMembers}>{convertLanguage(language, 'pending_invitation')}</Text>
                            </View>
                            <View style={styles.boxListMember}>
                                {
                                    memberInvited.length > 0
                                        ?
                                        memberInvited.map((member, index) => {
                                            if (member) return <View style={styles.boxItem} key={index}>
                                                <View style={styles.boxInfo}>
                                                    <Image source={{ uri: member.Avatars.Small }} style={styles.imgAvatar} />
                                                    <View style={styles.boxInfoDetail}>
                                                        <Text style={styles.txtName}>{member.Name}</Text>
                                                        <Text style={styles.txtId}>{member.UserId}</Text>
                                                        {/* <Text style={styles.txtRole}>{convertLanguage(language, member.Role.toLowerCase())}</Text> */}
                                                    </View>
                                                </View>
                                                <Touchable style={styles.boxAction} onPress={() => { this.props.onUnInviteUser(event.Id, member.Id) }}>
                                                    <Image source={require('../../assets/x_cancel.png')} style={styles.imgClose} />
                                                </Touchable>
                                            </View>
                                        })
                                        :
                                        <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'no_data')}</Text>
                                }
                            </View>
                            {this.state.modalSetRole ?
                                <ModalSetRole
                                    UserId={this.state.item.Id}
                                    eventId={this.props.event.event.Id}
                                    modalVisible={this.state.modalSetRole}
                                    toggleModal={() => this.toggleModal(false)}
                                    EventRole={this.props.event.event.Role}
                                    MemberRole={this.state.item.Role}
                                    onLeaveEvent={() => this.submitLeaveEvent(this.state.item)}
                                /> : null
                            }
                        </ScrollView>
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxMembers: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 10
    },
    txtMembers: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
    },
    btnInvite: {
        width: 72,
        height: 31,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#03a9f4',
        borderRadius: 4
    },
    txtInvite: {
        fontSize: 13,
        color: '#03a9f4'
    },
    boxListMember: {
        marginBottom: 20
    },
    boxItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 15
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    imgAvatar: {
        width: 60,
        height: 60,
        marginRight: 10,
        borderRadius: 30
    },
    boxInfoDetail: {
        flex: 1,
    },
    txtName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    txtId: {
        fontSize: 14,
        color: '#757575',
    },
    txtRole: {
        fontSize: 14,
        color: '#757575',
    },
    boxAction: {
        padding: 15,
        paddingRight: 0,
    },
    imgClose: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    boxPending: {
        marginBottom: 20
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        user: state.user,
        language: state.language,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataMembers: (slug, notification_id) => {
            dispatch(actLoadDataMembers(slug, notification_id))
        },
        onUnInviteUser: (id, userId) => {
            return dispatch(actUnInviteUser(id, userId))
        },
        onLeaveEvent: (eventId, member) => {
            return dispatch(actLeaveEvent(eventId, member))
        }


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventStaff);
// export default EventHostList;
