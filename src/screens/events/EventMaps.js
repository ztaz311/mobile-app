import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Alert, StyleSheet, Dimensions, Platform, PermissionsAndroid, FlatList, ScrollView } from 'react-native';
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import * as Colors from '../../constants/Colors';
import { Marker, ProviderPropType, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import Carousel from 'react-native-snap-carousel';
import { connect } from "react-redux";
import pin_1_icon from '../../assets/pin_1_icon.png';
import pin_1_icon_active from '../../assets/pin_1_icon_active.png';
import pin_menu_icon from '../../assets/pin_menu_icon.png';
import pin_menu_icon_active from '../../assets/pin_menu_icon_active.png';
import pin_icon_transparent from '../../assets/pin_icon_transparent.png';
import ItemEventSlide from '../../components/event/ItemEventSlide';
import ItemEventMapShow from '../../components/event/ItemEventMapShow';
const { width, height } = Dimensions.get('window');
import { actLoadDataListEvent, actLikeEvent, actLoadEventByMaps } from '../../actions/event';
import { check, PERMISSIONS, RESULTS, openSettings, request } from 'react-native-permissions';
import AndroidOpenSettings from 'react-native-android-open-settings';
import Geolocation from '@react-native-community/geolocation';
import { actLoadDataCategory } from '../../actions/category';
import { actSelectCountry } from '../../actions/city';
const ASPECT_RATIO = width / height;
import ModalCity from '../../components/ModalCity';
import ModalTime from '../../components/ModalTime';
import ModalTag from '../../components/ModalTag';
import { Modalize } from 'react-native-modalize';
import { convertLanguage } from '../../services/Helper';
import isEqual from 'lodash.isequal';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import ClusteredMapView from 'react-native-maps-super-cluster';
import * as Config from '../../constants/Config';
class EventMaps extends Component {
    modal = React.createRef();
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            page: 1,
            sliderActiveSlide: 0,
            data_times: [
                {
                    name_show: convertLanguage(language, 'all'),
                    name: convertLanguage(language, 'time'),
                    value: ''
                },
                {
                    name_show: convertLanguage(language, 'today'),
                    name: convertLanguage(language, 'today'),
                    value: 'today'
                },
                {
                    name_show: convertLanguage(language, 'tomorrow'),
                    name: convertLanguage(language, 'tomorrow'),
                    value: 'tomorrow'
                },
                {
                    name_show: convertLanguage(language, 'week'),
                    name: convertLanguage(language, 'week'),
                    value: 'week'
                },
                {
                    name_show: convertLanguage(language, 'weekend'),
                    name: convertLanguage(language, 'weekend'),
                    value: 'weekend'
                },
                {
                    name_show: convertLanguage(language, 'custom_period'),
                    name: convertLanguage(language, 'custom'),
                    value: 'custom',
                    start: '',
                    finish: '',
                }
            ],
            time_event: this.props.route.params['time_event'],
            tag: this.props.route.params['tag'],
            area: this.props.route.params['area'],
            lat: this.props.route.params['area'] && this.props.route.params['area'].Lat ? this.props.route.params['area'].Lat : 21.027765,
            lng: this.props.route.params['area'] && this.props.route.params['area'].Lng ? this.props.route.params['area'].Lng : 105.834160,
            latitudeDelta: 0.2,
            longitudeDelta: 0.2 * ASPECT_RATIO,
            north: '',
            east: '',
            south: '',
            west: '',
            events: [],
            modalCity: false,
            modalTime: false,
            modalTag: false,
            showButtonReSearch: false,
            isEdit: false,
            markers: [],
            modal_events: [],
            tracksViewChanges: true,
            panMap: true,
            type: this.props.route.params['type'],
        };
    }

    componentDidMount() {
        var { tag, time_event, area, type } = this.state;
        this.props.onLoadEventByMaps('', '', '', '', type === 'city' ? area.Id : area.Code, tag.value, time_event.value, '', '', type);
        this.props.onLoadDataCategory('event');
        var { event_countries } = this.props.city;
        if (event_countries.length === 0) {
            this.props.onLoadDataCountry();
        }
    }

    getDataMarkers(events_map) {
        var data_markers = [];
        var i = 0;
        events_map.forEach((event) => {
            if (typeof event.Lat != 'undefined' && typeof event.Long != 'undefined' && event.Lat !== "" && event.Long !== "") {
                let index = data_markers.findIndex(marker => marker.location.latitude == event.Lat && marker.location.longitude == event.Long);
                if (index !== -1) {
                    data_markers[index].events.push(event);
                    data_markers[index].keys.push(i)
                    data_markers.push({
                        location: {
                            latitude: parseFloat(event.Lat),
                            longitude: parseFloat(event.Long)
                        },
                        events: [],
                        keys: data_markers[index].keys,
                        showImage: false,
                        index: i
                    });
                    i++;
                } else {
                    data_markers.push({
                        location: {
                            latitude: parseFloat(event.Lat),
                            longitude: parseFloat(event.Long)
                        },
                        events: [event],
                        keys: [i],
                        showImage: true,
                        index: i
                    });
                    i++;
                }
            }
        })
        return data_markers;
    }

    showDataCountry() {
        var { event_countries } = this.props.city;
        var result = [
            {
                name: 'Map Area',
                value: ''
            }
        ]
        event_countries.forEach(country => {
            result.push({
                name: country.Name,
                value: country.Id
            })
        });
        return result;
    }

    showDataCategory() {
        var { categories } = this.props.category;
        var result = [{
            name: 'All',
            value: ''
        }];
        categories.forEach(category => {
            result.push({
                name: category.HashTagName,
                value: category.Id
            })
        });
        return result;
    }

    componentWillReceiveProps(nextProps) {
        var { events_map } = nextProps.event;
        if (events_map.length > 0 && this.state.events.length === 0) {
            var { sliderActiveSlide } = this.state;
            var markers = this.getDataMarkers(events_map)
            this.region = {
                latitude: markers[sliderActiveSlide].location.latitude,
                longitude: markers[sliderActiveSlide].location.longitude,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta
            }
            // console.log(markers)
            // console.log(this.region)
            // this.map.animateToRegion(this.region, 1000);
            if (this.state.type === 'country') {
                setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
                this.setState({
                    lat: markers[sliderActiveSlide].location.latitude,
                    lng: markers[sliderActiveSlide].location.longitude,
                })
            }
            // setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
            this.setState({
                // lat: markers[sliderActiveSlide].location.latitude,
                // lng: markers[sliderActiveSlide].location.longitude,
                events: events_map,
                markers: markers,
                showButtonReSearch: false
            })
        } else {
            if (!isEqual(events_map, this.state.events)) {
                this.setState({
                    events: events_map,
                    markers: this.getDataMarkers(events_map)
                })
            }
        }
        if (!isEqual(this.props, nextProps)) {
            this.setState({
                tracksViewChanges: true,
            })
        }
    }

    componentDidUpdate() {
        if (this.state.tracksViewChanges) {
            this.setState({
                tracksViewChanges: false,
            })
        }
    }

    async onSelect(key, value) {
        await this.setState({
            [key]: value,
            modalCity: false,
            modalTime: false,
            modalTag: false,
            events: [],
            modal_events: [],
            sliderActiveSlide: 0,
            type: key === 'area' ? 'city' : this.state.type
        })
        var { north, east, south, west, area, tag, time_event, type } = this.state;
        var start = time_event.value === 'customer' ? time_event.start : '';
        var finish = time_event.value === 'customer' ? time_event.finish : '';
        if (key === 'area') {
            const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${value.Name}&key=${Config.GOOGLE_API_KEY}`;
            try {
                const result = await fetch(apiUrl);
                const json = await result.json();
                console.log(this.state.events)
                if (json.results.length > 0 && this.state.events.length === 0) {
                    this.region = {
                        latitude: json.results[0].geometry.location.lat,
                        longitude: json.results[0].geometry.location.lng,
                        latitudeDelta: 0.2,
                        longitudeDelta: 0.2 * ASPECT_RATIO
                    }
                    setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
                }
            } catch (err) {
                console.log(err)
            }
            this.props.onLoadEventByMaps('', '', '', '', area.Id, tag.value, time_event.value, start, finish, type);
        } else if (key === 'time_event' || key === 'tag') {
            this.props.onLoadEventByMaps('', '', '', '', type === 'city' ? area.Id : area.Code, tag.value, time_event.value, start, finish, type);
        } else {
            this.props.onLoadEventByMaps(north, east, south, west, '', tag.value, time_event.value, start, finish, type);
        }
        // this.refresh()
    }

    onPressZoomIn = () => {
        this.region = {
            latitude: this.state.lat,
            longitude: this.state.lng,
            latitudeDelta: this.state.latitudeDelta / 2,
            longitudeDelta: this.state.longitudeDelta / 2
        }
        this.setState({
            latitudeDelta: this.state.latitudeDelta / 2,
            longitudeDelta: this.state.longitudeDelta / 2
        })
        // this.map.animateToRegion(this.region, 1000);
        setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
    }
    onPressZoomOut = () => {
        this.region = {
            latitude: this.state.lat,
            longitude: this.state.lng,
            latitudeDelta: this.state.latitudeDelta * 2,
            longitudeDelta: this.state.longitudeDelta * 2
        }
        this.setState({
            latitudeDelta: this.state.latitudeDelta * 2,
            longitudeDelta: this.state.longitudeDelta * 2
        })
        // this.map.animateToRegion(this.region, 1000);
        setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
    }
    _alertForLocationPermission() {
        var { language } = this.props.language;
        setTimeout(() => {
            Alert.alert(
                convertLanguage(language, 'access_location_title'),
                convertLanguage(language, 'access_location_content'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        style: 'cancel',
                    },
                    { text: convertLanguage(language, 'open_setting'), onPress: () => this.openSettings() },
                ],
            );
        }, 100)
    }
    openSettings() {
        Platform.OS === 'ios' ? openSettings() : AndroidOpenSettings.appDetailsSettings();
        this.props.navigation.goBack();
    }
    onPressToMyLocation = () => {
        var permission = Platform.OS === 'ios' ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
        check(permission).then(response => {
            if (response === RESULTS.BLOCKED) {
                this._alertForLocationPermission()
            } else if (response === RESULTS.GRANTED) {
                Geolocation.getCurrentPosition((position) => {
                    this.setState({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    })
                    this.region = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: this.state.latitudeDelta,
                        longitudeDelta: this.state.longitudeDelta
                    }
                    setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
                })
            } else if (response === RESULTS.DENIED) {
                request(permission).then(response2 => {
                    if (response2 === RESULTS.GRANTED) {
                        Geolocation.getCurrentPosition((position) => {
                            this.setState({
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            })
                            this.region = {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                                latitudeDelta: this.state.latitudeDelta,
                                longitudeDelta: this.state.longitudeDelta
                            }
                            setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
                        })
                    }
                })
            } else {
                this._alertForLocationPermission()
            }
        });
    }
    _onMapReady() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    // this.setState({ paddingTop: 0 });
                });
        }
    }

    searchEventByLocation = () => {
        var { lat, lng, latitudeDelta, longitudeDelta, tag, time_event, type } = this.state;
        this.setState({ sliderActiveSlide: 0, modal_events: [], showButtonReSearch: false })
        var north = lat + latitudeDelta / 2;
        var east = lng + longitudeDelta / 2;
        var south = lat - latitudeDelta / 2;
        var west = lng - longitudeDelta / 2;
        var start = time_event.value === 'customer' ? time_event.start : '';
        var finish = time_event.value === 'customer' ? time_event.finish : '';
        this.props.onLoadEventByMaps(north, east, south, west, '', tag.value, time_event.value, start, finish, type);
        this.setState({
            area: {
                Name: 'Map Area',
                Id: '',
                Name_vi: 'Khu vực',
                Name_en: 'Map Area',
                Name_ko: '지도 영역'
            },
        })
    }

    updateMarker(data) {
        if (data.keys.length > 1) {
            this.setState({ modal_events: data.events })
            // this.openModal();
            this.region = {
                latitude: data.location.latitude,
                longitude: data.location.longitude,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta
            }
            setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
            this.setState({
                lat: data.location.latitude,
                lng: data.location.longitude,
            })
        } else {
            var { markers } = this.state;
            this.setState({ modal_events: data.events })
            this.region = {
                latitude: markers[data.keys[0]].location.latitude,
                longitude: markers[data.keys[0]].location.longitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005 * ASPECT_RATIO
            }
            setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
            this.setState({
                // sliderActiveSlide: data.keys[0],
                lat: markers[data.keys[0]].location.latitude,
                lng: markers[data.keys[0]].location.longitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005 * ASPECT_RATIO
            })
        }
        if (this.modal.current) {
            this.modal.current?.open();
        }
    }

    onPanDrag = () => {
        if (this.state.showButtonReSearch) {
            this.setState({ showButtonReSearch: false })
            // this.showButtonReSearch = false;
        }
        if (!this.state.isEdit) {
            this.setState({ isEdit: true })
            // this.isEdit = true;
        }
    }

    renderCluster = (cluster, onPress) => {
        const pointCount = cluster.pointCount,
            coordinate = cluster.coordinate,
            clusterId = cluster.clusterId
        const clusteringEngine = this.map.getClusteringEngine(),
            clusteredPoints = clusteringEngine.getLeaves(clusterId, 100)

        return (
            <Marker coordinate={coordinate} onPress={(onPress)}>
                <View style={styles.myClusterStyle}>
                    <Text style={styles.myClusterTextStyle}>
                        {pointCount}
                    </Text>
                </View>
            </Marker>
        )
    }
    // onPress={() => this.updateMarker(data)}
    renderMarker = (data) => (
        <Marker key={data.index} coordinate={data.location} >
            {
                data.showImage ?
                    <View style={{ width: data.events.length === 1 ? 32 : 40, height: 45 }}>
                        <Image source={this.showImageMarker(data)} style={{ width: 32, height: 45 }} />
                        {
                            this.showNumberEvent(data)
                        }
                        {
                            data.keys.length === 1 ?
                                <Callout style={styles.plainView} >
                                    <Text>{data.events[0].Title}</Text>
                                </Callout>
                                :
                                <Callout style={styles.plainView} >
                                    <Text>{data.events[0].VenueName}</Text>
                                </Callout>
                        }
                    </View>
                    :
                    <View style={{ width: 0, height: 0, backgroundColor: '#000000' }}></View>
            }
        </Marker>
    )

    showImageMarker(data) {
        var { sliderActiveSlide } = this.state;
        if (data.showImage) {
            var index = data.keys.indexOf(sliderActiveSlide);
            if (index !== -1) {
                switch (data.events.length) {
                    case 1:
                        return pin_1_icon_active;
                    default:
                        return pin_menu_icon_active;
                }

            } else {
                switch (data.events.length) {
                    case 1:
                        return pin_1_icon;
                    default:
                        return pin_menu_icon;
                }
            }
        } else {
            return pin_icon_transparent;
        }
    }

    showNumberEvent(data) {
        if (data.showImage) {
            switch (data.events.length) {
                case 1:
                    return null;
                default:
                    return <View style={{ width: 16, height: 16, borderRadius: 8, backgroundColor: '#EB5757', position: 'absolute', top: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 11 }}>{data.events.length}</Text>
                    </View>
            }
        } else {
            return null;
        }
    }

    onRegionChangeComplete = (center) => {
        var { isEdit } = this.state;
        this.setState({
            lat: center.latitude && center.latitude === 0 ? this.state.lat : center.latitude,
            lng: center.longitude && center.longitude === 0 ? this.state.lng : center.longitude,
            latitudeDelta: center.latitudeDelta,
            longitudeDelta: center.longitudeDelta,
            showButtonReSearch: isEdit ? true : false,
            isEdit: false
        })
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    openModal = () => {
        if (this.modal.current) {
            this.modal.current?.open();
        }
    }

    onPressEvent = (index) => {
        var { events_map } = this.props.event;
        // var { markers } = this.state;
        var event = events_map[index];
        this.setState({ modal_events: [event] });
        this.region = {
            latitude: event.Lat,
            longitude: event.Long,
            latitudeDelta: 0.005,
            longitudeDelta: 0.005 * ASPECT_RATIO
        }
        setTimeout(() => this.map.getMapRef().animateToRegion(this.region), 10);
        if (!!this.scrollView) {
            this.scrollView.scrollTo({ x: 0, y: 0, animated: true });
        }
    }

    render() {
        var { markers, area, time_event, tag, sliderActiveSlide, modal_events, showButtonReSearch, panMap } = this.state;
        var { events_map, isFetching } = this.props.event;
        var { language } = this.props.language;
        var defaultHeight = Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() - 150 : height - 150;
        var initialRegion = {
            latitude: this.state.lat,
            longitude: this.state.lng,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
        }
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={this.goBack}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'event_map')}</Text>
                    <View style={{ width: 50 }} />
                </View>

                <Line />
                <ScrollView
                    style={{ flex: 1 }}
                    ref={(c) => { this.scrollView = c }}
                >
                    <View style={styles.content}>
                        <View style={styles.boxFilter}>
                            <View style={styles.boxFilter1}>
                                <Touchable style={[styles.boxItemFilter, { flex: 0.47 }]} onPress={() => this.setState({ modalCity: true })}>
                                    <Image source={require('../../assets/ic_location_blue.png')} style={styles.ic_location} />
                                    <Text style={styles.txtFilter} numberOfLines={1}>{area['Name_' + language]}</Text>
                                    <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                                </Touchable>
                                <Touchable style={[styles.boxItemFilter, { flex: 0.47 }]} onPress={() => this.setState({ modalTime: true })}>
                                    <Image source={require('../../assets/ic_calenda_blue.png')} style={styles.ic_location} />
                                    <Text style={styles.txtFilter} numberOfLines={1}>{time_event.name}</Text>
                                    <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                                </Touchable>
                            </View>
                            <Touchable style={styles.boxItemFilter} onPress={() => this.setState({ modalTag: true })}>
                                <Image source={require('../../assets/ic_category_blue.png')} style={styles.ic_location} />
                                <Text style={styles.txtFilter} numberOfLines={1}>{tag.name}</Text>
                                <Image source={require('../../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                            </Touchable>
                        </View>
                        <View style={{ marginBottom: 20, width: width, alignItems: 'center' }}>
                            {/* <View> */}
                            <ClusteredMapView
                                provider={PROVIDER_GOOGLE}
                                // style={{ flex: 1 }}
                                data={markers}
                                initialRegion={{
                                    latitude: this.state.lat,
                                    longitude: this.state.lng,
                                    latitudeDelta: this.state.latitudeDelta,
                                    longitudeDelta: this.state.longitudeDelta,
                                }}
                                zoomEnabled={true}
                                enableZoomControl={true}
                                // style={{ height: 400, width: width }}
                                height={width}
                                // showsUserLocation={true}
                                showsMyLocationButton={false}
                                onRegionChange={this.onPanDrag}
                                onRegionChangeComplete={this.onRegionChangeComplete}
                                // onPress={(e) => console.log(e.nativeEvent.coordinate)}
                                // onLongPress={(e) => this.setState({panMap: true})}
                                ref={ref => this.map = ref}
                                // onMapReady={this._onMapReady}
                                renderMarker={this.renderMarker}
                                renderCluster={this.renderCluster}
                            // scrollEnabled={panMap}
                            />
                            {/* <View style={{position: 'absolute', width, height: 400}}></View>
                            </View> */}
                            <View style={styles.boxAction}>
                                {
                                    <Touchable style={styles.btnMyLocation} onPress={this.onPressToMyLocation}>
                                        <Image source={require('../../assets/my_location.png')} style={styles.icMyLocation} />
                                    </Touchable>
                                }
                                <Touchable style={styles.btnPlus} onPress={this.onPressZoomIn}>
                                    <Image source={require('../../assets/plus.png')} style={styles.icPlus} />
                                </Touchable>
                                <Touchable style={styles.btnMinus} onPress={this.onPressZoomOut}>
                                    <Image source={require('../../assets/minus.png')} style={styles.icMinus} />
                                </Touchable>
                            </View>
                            {
                                showButtonReSearch &&
                                <Touchable style={styles.boxResearch} onPress={this.searchEventByLocation}>
                                    <Text style={styles.txtResearch}>Re-Search Here</Text>
                                </Touchable>
                            }
                        </View>
                        {
                            // modal_events.length > 0 &&
                            // <View style={styles.boxListEvent}>
                            //     <Carousel
                            //         data={modal_events}
                            //         renderItem={(item) => { return <ItemEventSlide cancel_text={convertLanguage(language, 'canceled_event')} isFromMap={true} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} /> }}
                            //         sliderWidth={width}
                            //         itemWidth={width - 30}
                            //         inactiveSlideScale={1}
                            //         firstItem={sliderActiveSlide}
                            //     // onSnapToItem={(index) => this.moveMarker(index)}
                            //     />
                            // </View>
                        }
                        {
                            isFetching
                                ?
                                <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
                                :
                                events_map.length > 0
                                    ?
                                    <FlatList
                                        // style={{ flex: 1, width: '100%' }}
                                        contentContainerStyle={{ paddingHorizontal: 16 }}
                                        data={events_map}
                                        renderItem={({ item, index }) => {
                                            return <ItemEventMapShow language={language} cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} onPress={() => this.onPressEvent(index)} />
                                        }}
                                        onEndReachedThreshold={0.5}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                    :
                                    <Text style={styles.txtEmpty}>{convertLanguage(language, 'event_empty')}</Text>
                        }
                    </View>
                </ScrollView>

                {
                    this.state.modalCity && this.props.city.event_countries.length > 0
                        ?
                        <ModalCity
                            modalVisible={this.state.modalCity}
                            closeModal={() => this.setState({ modalCity: false })}
                            countries={this.props.city.event_countries}
                            selectCity={(city) => this.onSelect('area', city)}
                        />
                        : null
                }
                {
                    this.state.modalTime
                        ?
                        <ModalTime
                            modalVisible={this.state.modalTime}
                            closeModal={() => this.setState({ modalTime: false })}
                            times={this.state.data_times}
                            selectTime={(time) => this.onSelect('time_event', time)}
                        />
                        : null
                }
                {
                    this.state.modalTag
                        ?
                        <ModalTag
                            modalVisible={this.state.modalTag}
                            closeModal={() => this.setState({ modalTag: false })}
                            tags={this.showDataCategory()}
                            selectTag={(tag) => this.onSelect('tag', tag)}
                        />
                        : null
                }
                <Modalize ref={this.modal} adjustToContentHeight={true} handlePosition="inside" handleStyle={styles.handleStyle} rootStyle={styles.rootStyle} overlayStyle={styles.overlayStyle}>
                    <View style={styles.boxListEvent}>
                        <Carousel
                            data={modal_events}
                            renderItem={(item) => { return <ItemEventSlide cancel_text={convertLanguage(language, 'canceled_event')} isFromMap={true} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} /> }}
                            sliderWidth={width}
                            itemWidth={width - 30}
                            inactiveSlideScale={1}
                            firstItem={sliderActiveSlide}
                        // onSnapToItem={(index) => this.moveMarker(index)}
                        />
                    </View>
                </Modalize>
            </SafeView>
        );
    }
}

EventMaps.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxFilter: {
        padding: 16,
    },
    boxFilter1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 10
    },
    boxListEvent: {
        marginBottom: 20,
        marginTop: 25
    },
    txtEmpty: {
        marginTop: 20,
        textAlign: 'center'
    },
    boxAction: {
        position: 'absolute',
        bottom: 15,
        right: 15
    },
    btnMyLocation: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icMyLocation: {
        width: 20,
        height: 20
    },
    btnPlus: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icPlus: {
        width: 18,
        height: 18
    },
    btnMinus: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10
    },
    icMinus: {
        width: 18,
        height: 18
    },
    boxResearch: {
        position: 'absolute',
        top: 20,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 10,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#03A9F4',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtResearch: {
        color: '#03A9F4',
        fontSize: 17,
    },
    boxItemFilter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 36,
        borderRadius: 8,
        borderColor: '#BDBDBD',
        borderWidth: 1,
        paddingHorizontal: 10
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 16,
        height: 16
    },
    ic_location: {
        width: 20,
        height: 20
    },
    myClusterStyle: {
        // position: 'absolute',
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#03a9f4',
        opacity: 0.5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    myClusterTextStyle: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 15
    },
    modalizeContent: {
        paddingHorizontal: 15,
        paddingTop: 15,
        flex: 1
    },
    plainView: {
        padding: 5,
        position: 'relative',
        minWidth: 100,
        maxWidth: 500,
        alignItems: 'center'
    },
    handleStyle: {
        width: 80,
        height: 4,
        backgroundColor: '#F6F4F4',
        borderRadius: 7.5
    },
    rootStyle: {
        top: 'auto',
    },
    overlayStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0)',
    }
});

const mapStateToProps = state => {
    return {
        event: state.event,
        city: state.city,
        category: state.category,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataListEvent: (page) => {
            dispatch(actLoadDataListEvent(page))
        },
        onLoadDataCategory: (type) => {
            dispatch(actLoadDataCategory(type))
        },
        onLoadDataCountry: () => {
            dispatch(actSelectCountry())
        },
        onLoadEventByMaps: (north, east, south, west, area, tag, time_event, start, finish, type) => {
            dispatch(actLoadEventByMaps(north, east, south, west, area, tag, time_event, start, finish, type))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventMaps);
