import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions, Platform, ActivityIndicator, ScrollView } from 'react-native';
const { width, height } = Dimensions.get('window')
import Touchable from '../../screens_view/Touchable';
import AutoHeightWebView from 'react-native-autoheight-webview';
import SafeView from '../../screens_view/SafeView';
import Line from '../../screens_view/Line';
import HTMLView from 'react-native-htmlview';
import YouTube from 'react-native-youtube';
import * as Colors from '../../constants/Colors';
import PhotoGrid from 'react-native-thumbnail-grid';
import ImageView from 'react-native-image-view';
import Share from 'react-native-share';
import * as Config from '../../constants/Config';
import { connect } from "react-redux";
import { actLoadDataEventNewsDetail, actLikeEventNews } from '../../actions/event';
import Error404 from '../../screens_view/404Error';
class EventNewsDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isImageViewVisible: false,
            modelEventNews: false,
        }
    }

    getId = (url) => {
        var ID = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        }
        else {
            ID = url;
        }
        return ID;
    }

    renderNode = (node, index, siblings, parent, defaultRenderer) => {
        if (node.name == 'iframe') {
            const a = node.attribs;
            const iframeHtml = `<iframe src="${a.src}" allowfullscreen></iframe>`;
            return (
                <View key={index} style={{ width: width - 40, height: (width - 40) * Number(a.height) / Number(a.width) }}>
                    {
                        Platform.OS === 'ios' ?
                            <YouTube
                                videoId={this.getId(a.src)} // The YouTube video ID
                                play={false} // control playback of video with true/false
                                fullscreen={false} // control whether the video should play in fullscreen or inline
                                style={{ alignSelf: 'stretch', height: (width - 40) * Number(a.height) / Number(a.width) }}
                            />
                            :
                            <AutoHeightWebView source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40) * 9 / 16}px}</style><body>` + iframeHtml + `</body></html>` }} />
                    }
                </View>
            );
        }
    }

    getImages(datas) {
        var images = [];
        datas.forEach(data => {
            images.push(data.Photos.Large)
        });
        return images;
    }

    getImagesZoom(datas) {
        var images = [];
        datas.forEach(data => {
            images.push({
                source: {
                    uri: data.Photos.Large
                },
            })
        });
        return images;
    }

    onZoomImage(uri, images) {
        this.setState({
            isImageViewVisible: true,
            index: images.indexOf(uri) === -1 ? 0 : images.indexOf(uri)
        })
    }

    componentDidMount() {
        var { iso_code } = this.props.global;
        this.props.onLoadDataEventNewsDetail(this.props.route.params['news'], this.props.route.params['notification'], iso_code);
        // this.props.onLoadDataEventNewsDetail(450);
    }

    share() {
        var { eventNewsDetail } = this.props.event;
        let options = {
            title: eventNewsDetail.Event.Title,
            message: eventNewsDetail.Event.Title + "\nFind and join event in Comeup\n",
            url: Config.MAIN_URL + 'event-news?news=' + eventNewsDetail.Id,
            // subject: "Jois with us" //  for email
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    onOpenModelEventNews(Id) {
        this.setState({
            modelEventNews: true,
        })
    }

    render() {
        var { loadingEventNewsDetail, eventNewsDetail } = this.props.event;
        // var { language } = this.props.language;
        // var images = this.getImages(eventNewsDetail.Images);
        // var imageZooms = this.getImagesZoom(eventNewsDetail.Images);
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    {/* <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'refund_request')}</Text> */}
                    <View style={{ width: 48 }} />
                </View>
                <Line />
                {
                    loadingEventNewsDetail &&
                    <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                }
                {
                    eventNewsDetail.status_code && eventNewsDetail.status_code === 404
                        ?
                        <Error404 onPress={() => this.props.navigation.goBack()} />
                        :
                        eventNewsDetail.Id &&
                        <>
                            <ScrollView style={styles.content}>
                                <View style={styles.boxInfo}>
                                    <Touchable style={styles.boxInfoChild} onPress={() => this.props.navigation.navigate({ name: 'EventDetail', params: { Slug: eventNewsDetail.Event.Slug }, key: eventNewsDetail.Event.Slug })}>
                                        <Image source={{ uri: eventNewsDetail.Event.Posters ? eventNewsDetail.Event.Posters.Medium : eventNewsDetail.Event.Poster }} style={styles.avatar} />
                                        <View style={styles.boxInfoRight}>
                                            <Text numberOfLines={2} style={styles.txtNameInfo}>{eventNewsDetail.Event.Title}</Text>
                                            <Text style={styles.txtTimeAgo}>{eventNewsDetail.CreatedAt}</Text>
                                        </View>
                                    </Touchable>
                                    {/* <Touchable style={styles.boxMore} onPress={() => this.onOpenModelEventNews(data.Id)}>
                                    <Image source={require('../../assets/more_horizontal.png')} style={styles.icMore} />
                                </Touchable> */}
                                </View>
                                <View style={styles.boxNews}>
                                    <HTMLView
                                        value={eventNewsDetail.Contents.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com')}
                                        renderNode={this.renderNode}
                                        paragraphBreak={'\n'}
                                    />
                                    <PhotoGrid source={this.getImages(eventNewsDetail.Images)} onPressImage={(data, uri) => this.onZoomImage(uri, this.getImages(eventNewsDetail.Images))} width={width - 40} styles={{ marginTop: 15 }} height={eventNewsDetail.Images.length === 1 ? (width - 40)*9/16 : 300} />
                                </View>
                                <View style={styles.boxEventMore}>
                                    <View style={styles.boxEventMoreLeft}>
                                        <Touchable onPress={() => this.props.onLikeEventNews(eventNewsDetail.Id)}>
                                            <Image source={eventNewsDetail.IsLike ? require('../../assets/liked_red.png') : require('../../assets/like_border_black.png')} style={styles.icLike} />
                                        </Touchable>
                                        <Touchable onPress={() => this.share()}>
                                            <Image source={require('../../assets/share_2.png')} style={styles.icLike} />
                                        </Touchable>
                                    </View>
                                </View>
                                <ImageView
                                    images={this.getImagesZoom(eventNewsDetail.Images)}
                                    imageIndex={this.state.index}
                                    isVisible={this.state.isImageViewVisible}
                                    onClose={() => this.setState({ isImageViewVisible: false })}
                                />
                            </ScrollView>
                        </>
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: "white",
        paddingLeft: 15,
        paddingRight: 15,
        flex: 1
    },
    boxNews: {
        flex: 1,
        paddingTop: 15
    },
    boxInfo: {
        flexDirection: 'row',
        paddingTop: 15
    },
    boxRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    boxClose: {
        width: 40,
        height: 40,
        alignItems: 'flex-end'
    },
    icClose: {
        width: 30,
        height: 30
    },
    boxInfoChild: {
        flexDirection: 'row',
        // alignItems: 'center',
        flex: 1
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 23,
        marginRight: 8
    },
    boxInfoRight: {
        flex: 1,
    },
    txtNameInfo: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 5
    },
    txtTimeAgo: {
        fontSize: 14,
        color: '#757575',
    },
    boxEventMore: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 15
    },
    boxEventMoreLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icLike: {
        width: 30,
        height: 30,
        marginRight: 10
    },
    boxImages: {
        marginBottom: 15
    },
    boxItem: {
        marginRight: 15,
    },
    image: {
        width: 100,
        height: 100
    },
    modalizeContent: {
        // paddingHorizontal: 15,
        paddingTop: 15,
        flex: 1
    },
    boxMore: {
        width: 30,
        height: 24,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    icMore: {
        width: 24, height: 24
    }
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataEventNewsDetail: (id, notification_id, iso_code) => {
            dispatch(actLoadDataEventNewsDetail(id, notification_id, iso_code))
        },
        onLikeEventNews: (EventNewsId) => {
            dispatch(actLikeEventNews(EventNewsId))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventNewsDetail);