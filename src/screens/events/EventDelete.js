import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity, KeyboardAvoidingView, ScrollView, Platform, ActivityIndicator } from 'react-native';

import Touchable from '../../screens_view/Touchable'
import SafeView from '../../screens_view/SafeView'


import * as Colors from '../../constants/Colors'

import { actDeleteEvent } from '../../actions/event';
import { connect } from "react-redux";

class EventDelete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            event: this.props.route.params['event'],
            err_passwd: '',
        };
    }

    submitDelete() {
        if (this.state.password.length >= 6 && !this.props.event.isFetching) {
            this.props.deleteEvent(this.state.event.Id, this.state.password, this.props.navigation);
        }
    }

    render() {
        let { event } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ margin: 20, marginBottom: 0 }}>
                    <Touchable
                        onPress={() => 
                            this.props.navigation.goBack()
                        }
                        style={{
                            minWidth: 40, minHeight: 40, alignSelf: 'flex-start',
                            justifyContent: 'center',
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                </View>
                <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 0} style={{ flex: 1 }} >
                    <ScrollView style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.colThumb}>
                                <Image
                                    resizeMode='cover'
                                    style={styles.imgLogo}
                                    source={{ uri: event.Posters.Small }} />
                            </View>
                            <View style={styles.colTeamInfo}>
                                <View style={styles.rowTeamName}>
                                    <Text style={styles.txtTeamName}>
                                        { event.Title }
                                    </Text>
                                    {/* <Image style={styles.imgCertified} source={require('../../assets/team_certified.png')} /> */}
                                </View>
                                <Text style={styles.txtTeamId}>ID: { event.Id }</Text>
                            </View>
                        </View>
                        <View style={styles.content}>
                            <Text style={styles.txtTitleConfirm}>Sure to delete this event?</Text>
                            <Text style={styles.txtDes}>After delete this event, all users can not see your event page, all the event news and venues that this event.</Text>
                            <Text style={styles.txtDes}>Input your password to confirm to delete</Text>
                            <View style={styles.rowPassword}>
                                <Text style={styles.txtPassword}>Password</Text>
                                <TextInput
                                    style={styles.inputPassword}
                                    placeholder=''
                                    value={this.state.password}
                                    secureTextEntry={true}
                                    onChangeText={(password) => this.setState({ password: password })}
                                />
                                {
                                    this.state.err_passwd !== '' &&
                                    <Text style={styles.txtError}>{this.state.err_passwd}</Text>
                                }
                            </View>
                            <View style={styles.rowForgot}>
                                <TouchableOpacity style={styles.btnForgot}
                                    onPress={() => {
                                        this.props.navigation.navigate('ForgotPw')
                                    }}
                                >
                                    <Text style={styles.txtForgot}
                                        textDecorationLine='underline'
                                    >Forgot Password?</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.rowConfirm}>
                                <TouchableOpacity style={[styles.btnConfirm, this.state.password.length < 6 || this.props.event.isFetching ? {} : styles.btnReportActive ]}
                                    onPress={() => { this.submitDelete() }}>
                                    <Text style={[styles.txtConfirm, this.state.password.length < 6 || this.props.event.isFetching ? {} : styles.txtBtnReportActive]}>Confirm</Text>
                                    {
                                        this.props.event.isFetching ? 
                                            <ActivityIndicator size="small" />
                                        : null
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeView>
        );
    }
}
  const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1
    },
    content: {
        flex: 1,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 25
    },
    header: {
        flexDirection: 'row',
        paddingBottom: 20
    },
    colThumb: {
        flex: 0.3,
        justifyContent: 'flex-start'
    },
    colTeamInfo: {
        flex: 0.7,
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10

    },
    imgLogo: {
        // alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 40
    },
    rowTeamName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtTeamName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtTeamId: {
        fontSize: 18,
        color: '#333333'
    },
    imgCertified: {
        maxWidth: 30,
        marginLeft: 10
    },
    txtTitleConfirm: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5,
        color: '#333333'
    },
    txtDes: {
        lineHeight: 30,
        fontSize: 16,
        color: '#333333'
    },
    rowPassword: {
        paddingTop: 15
    },
    inputPassword: {
        borderBottomWidth: 1,
        borderBottomColor: '#bcbcbc'
    },
    errorPassword: {
        color: Colors.RED,
        marginTop: 10
    },
    rowConfirm: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#bcbcbc',
        borderRadius: 5
    },
    btnConfirm: {
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
    },
    txtConfirm: {
        color: '#bcbcbc',
        fontWeight: 'bold'
    },
    rowForgot: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginBottom: 30
    },
    btnForgot: {
        borderBottomWidth: 1,
        color: '#03a9f4',
        borderBottomColor: '#03a9f4'
    },
    txtForgot: {
        color: '#03a9f4',
    },
    btnReportActive : {
        borderColor: Colors.PRIMARY
    },
    txtBtnReportActive: {
        color: Colors.PRIMARY
    },
    txtPassword: {
        color: '#333333',
        fontWeight: 'bold',
        fontSize: 18,
    },
    txtError: {
        color: '#ff4081',
        fontSize: 12,
        marginTop: 2
    }
  });
const mapStateToProps = state => {
    return {
        event: state.event,
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        deleteEvent: (id, password, navigation) => {
            dispatch(actDeleteEvent(id, password, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventDelete);
