import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, StyleSheet, Dimensions, TextInput, ScrollView, Platform } from 'react-native';
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import SrcView from '../../screens_view/ScrView';
import DatePicker from 'react-native-datepicker';
const { width, height } = Dimensions.get('window')
import ImagePicker from 'react-native-image-crop-picker';
// import ImgToBase64 from 'react-native-image-base64';
import Toast from 'react-native-simple-toast';
import RNFetchBlob from "rn-fetch-blob";
// import HTMLView from 'react-native-htmlview'
import AutoHeightWebView from 'react-native-autoheight-webview';
import LinearGradient from 'react-native-linear-gradient';
import Modal from "react-native-modal";
import moment from "moment";
import ModalContactInfoComeup from '../../components/event/ModalContactInfoComeup';
import { datetimeNow, formatNumber, convertLanguage } from '../../services/Helper';
const fs = RNFetchBlob.fs;
let imagePath = null;
import { actSaveEvent, actEditEvent, actUpdateEvent } from '../../actions/event';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import Loading from '../../screens_view/Loading';
class CreateEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            PosterBaseCrop: '',
            PosterBase: '',
            Title: '',
            TimeStart: '',
            TimeFinish: '',
            TimeRepeat: [],
            category_selected: [],
            Lat: '',
            Long: '',
            Address: '',
            VenueName: '',
            VenueId: '',
            AreaId: '',
            AreaName: '',
            Tickets: [],
            Detail: '',
            FAQ: [[], [], [], []],
            NameHost: '',
            Ceo: '',
            BusinessRegistrationNumber: '',
            AddressHost: '',
            ThePersonInCharge: '',
            PhoneHost: '',
            EmailHost: '',
            HostInfoId: '',
            IdHost: '',
            isOnpress: true,
            modelNoti: false,
            Unit: '',
            isUpdate: false,
            DraftId: '',
            modalContact: false,
            isChange: false,
        };
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }

    componentDidMount() {
        this.setState({ isUpdate: false })
        var EventId = this.props.route.params['EventId'];
        if (EventId) {
            this.props.onEditEvent(EventId)
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.event.itemEditing && this.props.route.params['EventId'] && !this.state.isUpdate) {
            var event = nextProps.event.itemEditing;
            RNFetchBlob.config({
                fileCache: true
            })
                .fetch("GET", event.Posters.Full)
                .then(resp => {
                    imagePath = resp.path();
                    return resp.readFile("base64");
                })
                .then(base64Data => {
                    this.setState({
                        PosterBase: 'data:image/png;base64,' + base64Data,
                    })
                    return fs.unlink(imagePath);
                });
            RNFetchBlob.config({
                fileCache: true
            })
                .fetch("GET", event.Posters.Medium)
                .then(resp => {
                    imagePath = resp.path();
                    return resp.readFile("base64");
                })
                .then(base64Data => {
                    this.setState({
                        PosterBaseCrop: 'data:image/png;base64,' + base64Data,
                    })
                    return fs.unlink(imagePath);
                });
            this.setState({
                isUpdate: true,
                Title: event.Title,
                TimeStart: event.TimeStart,
                TimeFinish: event.TimeFinish,
                // TimeRepeat: event.TimeRepeat ? event.TimeRepeat.split(',') : [],
                category_selected: event.HashTag,
                Lat: event.Lat,
                Long: event.Long,
                Address: event.Address,
                VenueName: event.VenueName,
                VenueId: event.VenueId,
                AreaId: event.AreaId,
                AreaName: event.AreaName,
                Tickets: this.convertTicket(event.Tickets, true, event.UnitTicket),
                Detail: event.Detail,
                FAQ: this.convertFAQ(event.FAQ),
                NameHost: event.HostInfo.Name,
                Ceo: event.HostInfo.Ceo,
                BusinessRegistrationNumber: event.HostInfo.BusinessRegistrationNumber,
                AddressHost: event.HostInfo.Address,
                ThePersonInCharge: event.HostInfo.ThePersonInCharge,
                PhoneHost: event.HostInfo.Phone,
                EmailHost: event.HostInfo.Email,
                HostInfoId: event.TeamId,
                IdHost: event.HostInfo.Id,
                Unit: event.UnitTicket
            })
        }
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    convertRepeat(date) {
        var arr = date.split(" ");
        var arrDate = arr[0].split("-");
        return arrDate[0] + '-' + arrDate[1] + '-' + arrDate[2];
    }

    showDateRepeat() {
        var { TimeRepeat } = this.state;
        var dateString = '';
        dateString = TimeRepeat.map((item, index) => {
            if (index > 3) {
                return;
            }
            var arrDate = item.split("-");
            var newItem = arrDate[1] + '.' + arrDate[2];
            return <View style={styles.boxTimeRepeat} key={index}>
                <Text style={styles.txtTimeRepeat}>{newItem}</Text>
            </View>
        });
        return dateString;
    }

    showListCategory() {
        var { category_selected } = this.state;
        var result = null;
        result = category_selected.map((category, index) => {
            return index + 1 < category_selected.length ? '#' + category.HashTagName + ' ' : '#' + category.HashTagName
        })
        return result
    }

    pickThumb = () => {
        ImagePicker.openPicker({
            mediaType: 'photo',
            includeBase64: true,
        }).then(image => {
            ImagePicker.openCropper({
                path: image.path,
                width: image.width,
                height: image.width * 9 / 16,
                includeBase64: true,
            }).then(image2 => {
                this.setState({
                    PosterBaseCrop: 'data:image/png;base64,' + image2.data,
                    PosterBase: 'data:image/png;base64,' + image.data,
                    isChange: true,
                })
            });
        }).catch(e => { });
    }

    getDateRepeat() {
        var { TimeRepeat } = this.state;
        var dateString = '';
        TimeRepeat.forEach((item, index) => {
            dateString = index + 1 === TimeRepeat.length ? dateString + item : dateString + item + ', ';
        });
        return dateString;
    }

    onChangeLocation(location) {
        this.setState({
            Lat: location.Lat,
            Long: location.Long,
            Address: location.Address,
            VenueName: location.VenueName,
            VenueId: location.VenueId,
            AreaId: location.AreaId,
            AreaName: location.AreaName,
            Unit: location.Unit,
            isChange: true
        })
    }

    getDataTag() {
        var { category_selected } = this.state;
        var result = [];
        category_selected.forEach((category) => {
            result.push(category.Id)
        })
        return result
    }

    checkTicket() {
        var { Tickets } = this.state;
        var result = [];
        Tickets.forEach((item) => {
            result.push(item.NameTicket)
        })
        return result
    }

    onChangeDataDetail(data) {
        this.setState({
            Detail: data.Detail,
            FAQ: data.Data,
            NameHost: data.NameHost,
            Ceo: data.Ceo,
            BusinessRegistrationNumber: data.BusinessRegistrationNumber,
            AddressHost: data.AddressHost,
            ThePersonInCharge: data.ThePersonInCharge,
            PhoneHost: data.PhoneHost,
            EmailHost: data.EmailHost,
            isChange: true,
        })
    }

    isCheckValidate() {
        var { PosterBaseCrop, Title, TimeStart, TimeFinish, Address, category_selected, isChange } = this.state;
        return (PosterBaseCrop === '' || Title === '' || TimeStart === '' || TimeFinish === '' || category_selected.length === 0 || Address === '' || !isChange)
    }

    validate() {
        var { language } = this.props.language;
        var { PosterBaseCrop, PosterBase, Title, TimeStart, TimeFinish, Address, Tickets, category_selected, Unit, FAQ, NameHost, PhoneHost, EmailHost } = this.state;
        if (PosterBaseCrop === '' || PosterBase === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_poster_required'), Toast.SHORT, Toast.CENTER)
            return true;
        }
        if (Title === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_title_required'), Toast.SHORT, Toast.CENTER)
            return true;
        }
        if (TimeStart === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_time_start_required'), Toast.SHORT, Toast.CENTER)
            return true;
        }
        if (TimeFinish === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_time_finish_required'), Toast.SHORT, Toast.CENTER)
            return true;
        }
        if (category_selected.length === 0) {
            Toast.showWithGravity("convertLanguage(language, 'validate_category_required')", Toast.SHORT, Toast.CENTER)
            return true;
        }
        if (Address === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_address_required'), Toast.SHORT, Toast.CENTER)
            return true;
        }
        if (Unit === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_Unit_required'), Toast.SHORT, Toast.CENTER)
            result = true;
            return;
        }
        if (Tickets.length > 0) {
            var result = false;
            Tickets.forEach((Ticket) => {
                if (Ticket.NameTicket === '') {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_name'), Toast.SHORT, Toast.CENTER)
                    result = true;
                    return;
                }
                if (Ticket.Price === '') {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_price'), Toast.SHORT, Toast.CENTER)
                    result = true;
                    return;
                }
                if (Ticket.Quantity === '') {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_quantity'), Toast.SHORT, Toast.CENTER)
                    result = true;
                    return;
                }
                if (Ticket.LimitQuantity === '') {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_limit'), Toast.SHORT, Toast.CENTER)
                    result = true;
                    return;
                }
                if (Ticket.TimeStartTicket === '') {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_time_start'), Toast.SHORT, Toast.CENTER)
                    result = true;
                    return;
                }
                if (Ticket.TimeEndTicket === '') {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_time_end'), Toast.SHORT, Toast.CENTER)
                    result = true;
                    return;
                }
            })
            return result;
        }
        if(FAQ[2].length === 0 || FAQ[3].length === 0 || NameHost === '' || PhoneHost === '' || EmailHost === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_detail'), Toast.SHORT, Toast.CENTER)
            return true;
        }
        return false;
    }

    getFAQ() {
        var { FAQ } = this.state;
        var result = {};
        var a = [];
        var b = [];
        var c = [];
        var d = [];
        FAQ[0].forEach((item) => {
            a.push(item.key)
        })
        FAQ[1].forEach((item) => {
            b.push(item.key)
        })
        FAQ[2].forEach((item) => {
            c.push(item.key)
        })
        FAQ[3].forEach((item) => {
            d.push(item.key)
        })
        result['A'] = a;
        result['B'] = b;
        result['C'] = c;
        result['D'] = d;
        return result;
    }

    convertFAQ(FAQ) {
        var a = [];
        var b = [];
        var c = [];
        var d = [];
        if (FAQ) {
            FAQ['A'] && FAQ['A'].forEach((item) => {
                a.push({
                    key: item,
                    value: this.showDataFAQ(item)
                })
            })
            FAQ['B'] && FAQ['B'].forEach((item) => {
                b.push({
                    key: item,
                    value: this.showDataFAQ(item)
                })
            })
            FAQ['C'] && FAQ['C'].forEach((item) => {
                c.push({
                    key: item,
                    value: this.showDataFAQ(item)
                })
            })
            FAQ['D'] && FAQ['D'].forEach((item) => {
                d.push({
                    key: item,
                    value: this.showDataFAQ(item)
                })
            })
        }
        return [a, b, c, d];
    }

    showDataFAQ(item) {
        switch (item) {
            case 'comeup-ticket':
                return 'ComeUp ticket';
            case 'printed-comeup-e-ticket':
                return 'Printed ComeUp e-ticket';
            case 'ticket-holder-name-&-phone-number':
                return 'Ticket holder name & phone number';
            case 'delivered-ticket-from-this-event-host':
                return 'Delivered ticket from this event host';
            case 'no-tickyet-check-in':
                return 'No ticket check-in';
            case 'id-card-check':
                return 'ID card check';
            case 'no-minor':
                return 'No minor';
            case 'no-carrying-food-and-beverage':
                return 'No carrying food and beverage';
            case 'no-pet':
                return 'No pet';
            case 'custom-restriction':
                return 'Custom Restriction';
            case 'no-car-park':
                return 'No car park';
            case 'free':
                return 'Free';
            case 'paid':
                return 'Paid';
            case 'comeup-refund-policy':
                return 'ComeUp refund policy';
            case 'full-refund-before-event-start':
                return 'Full refund before event start';
            case 'no-refund':
                return 'No refund';
            case 'host-refund-policy':
                return 'Host refund policy';
            default:
                return '';
        }
    }

    onRegister(type = '') {
        this.setState({
            isOnpress: false
        })
        if (type != 'draft') {
            if (this.validate()) {
                this.setState({
                    isOnpress: true
                })
                return;
            }
        }
        var { PosterBaseCrop, PosterBase, Title, TimeStart, TimeFinish, Lat, Long, Address, VenueId, VenueName, AreaId, AreaName, Tickets, Detail, NameHost, Ceo, BusinessRegistrationNumber, AddressHost, ThePersonInCharge, PhoneHost, EmailHost, IdHost, DraftId, category_selected } = this.state;
        var Tags = [];
        category_selected.forEach(category => {
            Tags.push(category.Id);
        });
        var TagString = JSON.stringify(Tags);
        var body = {
            PosterBaseCrop,
            PosterBase,
            Title,
            TimeStart,
            TimeFinish,
            Lat,
            Long,
            Address,
            VenueName,
            VenueId,
            AreaId,
            AreaName,
            tickets: Tickets,
            TimeRepeat: this.getDateRepeat(),
            Tag: this.getDataTag(),
            Ticket: this.checkTicket(),
            Detail,
            FAQ: this.getFAQ(),
            NameHost,
            Ceo,
            BusinessRegistrationNumber,
            AddressHost,
            ThePersonInCharge,
            PhoneHost,
            EmailHost,
            IdHost,
            TagString
        }
        var EventId = this.props.route.params['EventId'];
        if (type === 'draft') {
            body.Draft = 'draft';
            body.DraftId = DraftId;
        }
        if (EventId) {
            this.props.onUpdateEvent(EventId, body, this.props.navigation)
        } else {
            this.props.onSaveEvent(this.props.route.params['HostInfoId'], body, this.props.navigation);
        }
        setTimeout(() => {
            this.setState({
                isOnpress: true
            })
        }, 2000)
    }

    convertTicket(eventTickets, isEdit, Unit) {
        var Tickets = [];
        eventTickets.forEach((Ticket) => {
            var data = {
                TicketId: Ticket.Id,
                NameTicket: Ticket.Name,
                Unit: Unit,
                Price: Ticket.Price.toString(),
                Quantity: Ticket.TotalQuantity.toString(),
                LimitQuantity: Ticket.LimitQuantity.toString(),
                TimeStartTicket: isEdit ? Ticket.TimeStart : '',
                TimeEndTicket: isEdit ? Ticket.TimeEnd : '',
                Description: Ticket.Description,
                Option: Ticket.Option,
                Status: Ticket.Status,
                Question2: Ticket.Question2,
                Type: Ticket.Type,
                TotalTicketDetail: Ticket.total_ticketDetail,
                StatusCOD: Ticket.StatusCOD
            }
            Tickets.push(data)
        })
        return Tickets;
    }

    importDataEvent(event) {
        RNFetchBlob.config({
            fileCache: true
        })
            .fetch("GET", event.Posters.Full)
            .then(resp => {
                imagePath = resp.path();
                return resp.readFile("base64");
            })
            .then(base64Data => {
                this.setState({
                    PosterBase: 'data:image/png;base64,' + base64Data,
                })
                return fs.unlink(imagePath);
            });
        RNFetchBlob.config({
            fileCache: true
        })
            .fetch("GET", event.Posters.Medium)
            .then(resp => {
                imagePath = resp.path();
                return resp.readFile("base64");
            })
            .then(base64Data => {
                this.setState({
                    PosterBaseCrop: 'data:image/png;base64,' + base64Data,
                })
                return fs.unlink(imagePath);
            });
        this.setState({
            Title: event.Title,
            category_selected: event.HashTag,
            Lat: event.Lat,
            Long: event.Long,
            Address: event.Address,
            VenueName: event.VenueName,
            VenueId: event.VenueId,
            AreaId: event.AreaId,
            AreaName: event.AreaName,
            Tickets: this.convertTicket(event.Tickets, false, event.UnitTicket),
            Detail: event.Detail,
            FAQ: this.convertFAQ(event.FAQ),
            NameHost: event.HostInfo.Name,
            Ceo: event.HostInfo.Ceo,
            BusinessRegistrationNumber: event.HostInfo.BusinessRegistrationNumber,
            AddressHost: event.HostInfo.Address,
            ThePersonInCharge: event.HostInfo.ThePersonInCharge,
            PhoneHost: event.HostInfo.Phone,
            EmailHost: event.HostInfo.Email,
            Unit: event.UnitTicket,
            DraftId: event.Id
        })
    }

    onChangeTimeFinish(TimeFinish) {
        this.setState({ TimeFinish, TimeRepeat: [], isChange: true })
        var { Tickets } = this.state;
        if (Tickets.length > 0) {
            var newTickets = [];
            Tickets.forEach((ticket) => {
                ticket.TimeStartTicket = datetimeNow();
                ticket.TimeEndTicket = TimeFinish;
                newTickets.push(ticket)
            })
            this.setState({ Tickets: newTickets })
        }
    }

    validateEdit() {
        var datetimenow = moment().format('YYYY-MM-DD HH:mm');
        var { TimeFinish } = this.state;
        var EventId = this.props.route.params['EventId'];
        if (EventId && moment(datetimenow).isSameOrAfter(TimeFinish)) {
            return true;
        } else {
            return false;
        }
    }

    checkTicketCOD() {
        var { Tickets } = this.state;
        const result = Tickets.filter(ticket => ticket.Type === 'd-p-ticket' && ticket.StatusCOD === 0);
        return result.length > 0 ? true : false;
    }

    onShowModalContactInfo = () => {
        this.setState({
            modalContact: true
        })
    }

    render() {
        var datetimenow = moment().format('YYYY-MM-DD HH:mm')
        var { isFetching, loading } = this.props.event;
        var EventId = this.props.route.params['EventId'];
        var { language } = this.props.language;
        var { PosterBaseCrop, TimeStart, TimeFinish, TimeRepeat, category_selected, Title, Address, Tickets, Detail, NameHost, Ceo, BusinessRegistrationNumber, AddressHost, ThePersonInCharge, PhoneHost, EmailHost, FAQ, HostInfoId, isOnpress, Lat, Long, VenueName, VenueId, AreaId, AreaName, modelNoti, Unit, isChange } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'event_registration')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <SrcView style={{ flex: 1 }}  keyboardShouldPersistTaps='handled'>
                    <View style={[styles.content, { opacity: (isFetching || loading) ? 0.2 : 1 }]}>
                        <View style={styles.boxImageThumb}>
                            {
                                PosterBaseCrop !== '' ?
                                    <Touchable onPress={() => this.pickThumb()}>
                                        <Image source={{ uri: PosterBaseCrop }} style={styles.imgThumb} />
                                    </Touchable>
                                    :
                                    <Touchable style={styles.boxAddImage} onPress={() => this.pickThumb()}>
                                        <Image source={require('../../assets/photo_add.png')} style={{width: 32, height: 32, marginRight: 5}} />
                                        <Text style={styles.txtAddImage}>ADD POSTER</Text>
                                    </Touchable>
                            }
                        </View>
                        <View style={styles.boxDataInput}>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'title')} *</Text>
                                <TextInput
                                    style={styles.ipContent}
                                    selectionColor="#bcbcbc"
                                    value={Title}
                                    onChangeText={(Title) => this.setState({ Title, isChange: true })}
                                />
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'time')} *</Text>
                                <View style={styles.boxDate}>
                                    <DatePicker
                                        style={{ flex: 0.45 }}
                                        date={TimeStart}
                                        mode="datetime"
                                        placeholder={convertLanguage(language, 'start')}
                                        format="YYYY-MM-DD HH:mm"
                                        // minDate={datetimeNow()}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        iconComponent={<Image style={{ position: 'absolute', right: 0 }} source={require('../../assets/calendar_icon.png')} />}
                                        disabled={this.validateEdit()}
                                        customStyles={{
                                            dateInput: {
                                                borderBottomColor: '#bdbdbd',
                                                borderBottomWidth: 1,
                                                borderLeftWidth: 0,
                                                borderRightWidth: 0,
                                                borderTopWidth: 0,
                                            },
                                            dateText: {
                                                color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                            },
                                            btnTextConfirm: {
                                                color: Colors.PRIMARY, fontSize: 17
                                            },
                                            btnTextCancel: {
                                                color: Colors.TEXT_P, fontSize: 16
                                            },
                                            placeholderText: {
                                                alignSelf: 'flex-start',
                                                fontSize: 15,
                                                color: '#bdbdbd'
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(TimeStart) => this.setState({ TimeStart, TimeFinish: '', TimeRepeat: [] })}
                                    />
                                    <Text style={styles.txtCenter}>~</Text>
                                    <DatePicker
                                        style={{ flex: 0.45 }}
                                        date={TimeFinish}
                                        mode="datetime"
                                        placeholder={convertLanguage(language, 'finish')}
                                        format="YYYY-MM-DD HH:mm"
                                        minDate={TimeStart === '' ? datetimeNow() : moment(TimeStart).isSameOrAfter(datetimenow) ? TimeStart : datetimeNow()}
                                        disabled={this.validateEdit() || TimeStart === '' ? true : false}
                                        // maxDate="01-01-2010"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        iconComponent={<Image style={{ position: 'absolute', right: 0 }} source={require('../../assets/calendar_icon.png')} />}
                                        customStyles={{
                                            dateInput: {
                                                borderBottomColor: '#bdbdbd',
                                                borderBottomWidth: 1,
                                                borderLeftWidth: 0,
                                                borderRightWidth: 0,
                                                borderTopWidth: 0,
                                            },
                                            dateText: {
                                                color: Colors.TEXT_P, fontSize: 13, alignSelf: 'flex-start',
                                            },
                                            btnTextConfirm: {
                                                color: Colors.PRIMARY, fontSize: 17
                                            },
                                            btnTextCancel: {
                                                color: Colors.TEXT_P, fontSize: 16
                                            },
                                            placeholderText: {
                                                alignSelf: 'flex-start',
                                                fontSize: 15,
                                                color: '#bdbdbd'
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(TimeFinish) => this.onChangeTimeFinish(TimeFinish)}
                                    />
                                </View>
                            </View>
                            <View style={styles.boxRepeat}>
                                {
                                    TimeStart === '' || TimeFinish === '' || this.validateEdit()
                                        ?
                                        <View style={[styles.btnRepeat, { backgroundColor: '#e8e8e8', borderWidth: 0 }]}>
                                            <Text style={[styles.txtRepeat, { color: '#bdbdbd' }]}>{convertLanguage(language, 'repeat')}</Text>
                                        </View>
                                        :
                                        <Touchable style={styles.btnRepeat} onPress={() => this.props.navigation.navigate('CalendarSelect', { TimeStart: this.convertRepeat(TimeStart), TimeFinish: this.convertRepeat(TimeFinish), callback: (TimeRepeat) => this.setState({ TimeRepeat, isChange: true }), TimeRepeat })}>
                                            <Text style={styles.txtRepeat}>{convertLanguage(language, 'repeat')}</Text>
                                        </Touchable>
                                }
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{ flex: 1 }}>
                                    {
                                        this.showDateRepeat()
                                    }
                                    {
                                        TimeRepeat.length > 4 &&
                                        <Touchable disabled={TimeStart === '' || TimeFinish === '' || this.validateEdit()} style={[styles.boxTimeRepeat, { flex: 0.1 }]} onPress={() => this.props.navigation.navigate('CalendarSelect', { TimeStart: this.convertRepeat(TimeStart), TimeFinish: this.convertRepeat(TimeFinish), callback: (TimeRepeat) => this.setState({ TimeRepeat, isChange: true }), TimeRepeat })}>
                                            <Text style={styles.txtTimeRepeat}>...</Text>
                                        </Touchable>
                                    }
                                </ScrollView>
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'location')} *</Text>
                                <Touchable disabled={this.validateEdit()} style={[styles.boxInputContent, this.validateEdit() ? { backgroundColor: '#e8e8e8' } : {}]} onPress={() => this.props.navigation.navigate('LocationSetting', { Lat, Long, Address, VenueName, VenueId, AreaId, AreaName, Unit, callback: (location) => this.onChangeLocation(location) })}>
                                    {
                                        VenueName !== '' &&
                                        <Text style={[styles.txtContent, { fontSize: 15, marginBottom: 5 }]}>{VenueName.toUpperCase()}</Text>
                                    }
                                    <Text style={styles.txtContent}>{Address}</Text>
                                </Touchable>
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'category')} *</Text>
                                <Touchable disabled={this.validateEdit()} style={[styles.boxInputContent, this.validateEdit() ? { backgroundColor: '#e8e8e8' } : {}]} onPress={() => this.props.navigation.navigate('SelectCategory', { category_selected, callback: (category_selected) => this.setState({ category_selected, isChange: true }) })}>
                                    <Text style={styles.txtContent}>{this.showListCategory()}</Text>
                                </Touchable>
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'ticket')}</Text>
                                {
                                    Tickets.length > 0
                                        ?
                                        <>
                                            <Touchable disabled={this.validateEdit()} style={[styles.boxInputContent, this.validateEdit() ? { backgroundColor: '#e8e8e8' } : {}]} onPress={() => (TimeStart && TimeFinish && Address) ? this.props.navigation.navigate('EventTicket', { Tickets, TimeFinish, Unit, EventId, callback: (Tickets) => this.setState({ Tickets, isChange: true }) }) : this.setState({ modelNoti: true })}>
                                                {
                                                    Tickets.map((item, index) => {
                                                        return <View style={styles.boxItemTicket} key={index}>
                                                            <Text style={styles.txtNameTicket} numberOfLines={1}>{item.NameTicket}</Text>
                                                            <Text style={styles.txtProceTicket}>{formatNumber(item.Price) + ' ' + Unit}</Text>
                                                        </View>
                                                    })
                                                }
                                            </Touchable>
                                            {
                                                this.checkTicketCOD() &&
                                                <Text style={styles.txtInfoCod}>{convertLanguage(language, 'text_this_physical_ticket')} <Text style={{ color: '#00A9F4' }} onPress={() => this.onShowModalContactInfo()}>{convertLanguage(language, 'see_more')}.</Text></Text>
                                            }
                                        </>
                                        :
                                        <Touchable disabled={this.validateEdit()} style={[styles.boxCreateTicket, this.validateEdit() || (TimeStart && TimeFinish && Address) ? {} : { backgroundColor: '#e8e8e8', borderWidth: 0 }]} onPress={() => (TimeStart && TimeFinish && Address) ? this.props.navigation.navigate('EventTicket', { Tickets, TimeFinish, Unit, EventId, callback: (Tickets) => this.setState({ Tickets, isChange: true }) }) : this.setState({ modelNoti: true })}>
                                            <Text style={[styles.txtCreateTicket, this.validateEdit() || (TimeStart && TimeFinish && Address) ? { color: '#333333' } : {}]}>+ {convertLanguage(language, 'create_tickets')}</Text>
                                        </Touchable>
                                }

                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'detail')} *</Text>
                                {
                                    Detail.length > 0
                                        ?
                                        <View style={{}}>
                                            <View style={styles.boxEditDetail}>
                                                <AutoHeightWebView startInLoadingState={true} scalesPageToFit={false} zoomable={false} scrollEnabled={false} renderLoading={this.renderLoading} source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40) * 9 / 16}px} img {width: ${width - 40} !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }} originWhitelist={['*']} />
                                                <LinearGradient colors={['rgba(255,255,255,0)', '#FFFFFF']} style={{ height: 50, width: '100%', position: 'absolute', bottom: 0, left: 0 }}></LinearGradient>
                                            </View>
                                            <Text style={styles.txtEdit} onPress={() => this.props.navigation.navigate('EventDetailSetting', { Detail, NameHost, Ceo, BusinessRegistrationNumber, AddressHost, ThePersonInCharge, PhoneHost, EmailHost, FAQ, HostInfoId: this.props.route.params['HostInfoId'] ? this.props.route.params['HostInfoId'] : HostInfoId, callback: (DataDetail) => this.onChangeDataDetail(DataDetail) })}>+ {convertLanguage(language, 'edit')}</Text>
                                        </View>
                                        :
                                        <Touchable style={styles.boxInputDetail} onPress={() => this.props.navigation.navigate('EventDetailSetting', { Detail, NameHost, Ceo, BusinessRegistrationNumber, AddressHost, ThePersonInCharge, PhoneHost, EmailHost, FAQ, HostInfoId: this.props.route.params['HostInfoId'] ? this.props.route.params['HostInfoId'] : HostInfoId, callback: (DataDetail) => this.onChangeDataDetail(DataDetail) })}>
                                            <Text style={styles.txtDetail}>{convertLanguage(language, 'introduce_how_this_event_is_awesome')}</Text>
                                        </Touchable>
                                }
                            </View>
                        </View>
                        <View style={styles.boxAction}>
                            {
                                EventId
                                    ?
                                    <Touchable style={[styles.btnSave, { borderColor: this.isCheckValidate() ? '#E8E7E8' : '#03a9f4', backgroundColor: this.isCheckValidate() ? '#E8E7E8' : '#03a9f4' }]} disabled={this.isCheckValidate()} onPress={() => (isFetching || !isOnpress) ? null : this.onRegister()}>
                                        <Text style={[styles.txtSave, { color: this.isCheckValidate() ? '#BDBDBD' : '#FFFFFF' }]}>{convertLanguage(language, 'save')}</Text>
                                    </Touchable>
                                    :
                                    <>
                                        <Touchable disabled={PosterBaseCrop === '' || Title === '' || !isChange} onPress={() => this.onRegister('draft')} style={[styles.btnRegister, { borderColor: PosterBaseCrop === '' || Title === '' || !isChange ? '#E8E7E8' : '#333333', backgroundColor: PosterBaseCrop === '' || Title === '' || !isChange ? '#E8E7E8' : '#FFFFFF' }, { flex: 0.25 }]} >
                                            <Text style={[styles.txtRegister, { color: PosterBaseCrop === '' || Title === '' ? '#BDBDBD' : '#333333' }]}>{convertLanguage(language, 'save')}</Text>
                                        </Touchable>
                                        <Touchable style={[styles.btnImport, { flex: 0.25 }]} onPress={() => this.props.navigation.navigate('ImportEventInfo', { HostInfoId: this.props.route.params['HostInfoId'] ? this.props.route.params['HostInfoId'] : HostInfoId, callback: (event) => this.importDataEvent(event) })}>
                                            <Text style={styles.txtImport}>{convertLanguage(language, 'import')}</Text>
                                        </Touchable>
                                        <Touchable disabled={this.isCheckValidate()} style={[styles.btnRegister, { borderColor: this.isCheckValidate() ? '#E8E7E8' : '#03a9f4', backgroundColor: this.isCheckValidate() ? '#E8E7E8' : '#03a9f4' }]} onPress={() => (isFetching || !isOnpress) ? null : this.onRegister()}>
                                            <Text style={[styles.txtRegister, { color: this.isCheckValidate() ? '#BDBDBD' : '#FFFFFF' }]}>{convertLanguage(language, 'register')}</Text>
                                        </Touchable>
                                    </>
                            }
                        </View>
                    </View>
                </SrcView>

                {
                    (isFetching || loading) &&
                    // <View style={{ flex: 1, height: height, width, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                    //     <ActivityIndicator size="large" color="#000000" />
                    // </View>
                    <Loading />
                }
                <Modal
                    isVisible={modelNoti}
                    backdropOpacity={0.3}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={1}
                    animationOutTiming={1}
                    backdropTransitionInTiming={1}
                    backdropTransitionOutTiming={1}
                    deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                // style={{margin: 0}}
                >
                    <View style={styles.modalContent}>
                        <View style={styles.rowClose}>
                            <Touchable style={styles.btnClose} onPress={() => { this.setState({ modelNoti: false }) }} >
                                <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                            </Touchable>
                        </View>
                        <Text style={styles.txtNoti}>
                            {
                                (!TimeStart || !TimeFinish) ?
                                    convertLanguage(language, 'set_the_time_first')
                                    :
                                    !Address &&
                                    convertLanguage(language, 'set_the_location_info_first')
                            }
                        </Text>
                        <Touchable style={styles.btnOk} onPress={() => { this.setState({ modelNoti: false }) }}>
                            <Text style={styles.txtOk}>{convertLanguage(language, 'ok')}</Text>
                        </Touchable>
                    </View>
                </Modal>
                {
                    this.state.modalContact &&
                    <ModalContactInfoComeup
                        modalVisible={this.state.modalContact}
                        closeModal={() => this.setState({ modalContact: false })}
                    />
                }
            </SafeView>
        );
    }
}
const stylesHtml = StyleSheet.create({
    a: {
        fontWeight: '300',
        color: '#FF3366', // make links coloured pink
    },
    p: {
        marginTop: 3,
        marginBottom: 3
    }
});
const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxImageThumb: {
        marginBottom: 30
    },
    imgThumb: {
        width: width,
        resizeMode: 'cover',
        height: 210,
    },
    boxAddImage: {
        height: 210,
        backgroundColor: '#f2f2f2',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    txtAddImage: {
        fontSize: 16,
        color: '#B3B8BC',
        fontWeight: 'bold'
    },
    boxDataInput: {
        paddingLeft: 20,
        paddingRight: 20,
        flex: 1
    },
    boxInput: {
        paddingBottom: 24,
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 7,
        fontWeight: 'bold'
    },
    ipContent: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 8,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    boxInputContent: {
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        justifyContent: 'center',
        paddingTop: 8,
        paddingBottom: 8,
    },
    boxDate: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtCenter: {
        flex: 0.1,
        color: '#757575',
        fontSize: 28,
        textAlign: 'center'
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxItemTicket: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15
    },
    txtNameTicket: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        flex: 1,
        marginRight: 5
    },
    txtProceTicket: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        // flex: 1
    },
    boxRepeat: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 25
    },
    btnRepeat: {
        padding: 7,
        borderWidth: 1,
        borderColor: '#bcbcbc',
        borderRadius: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtRepeat: {
        color: '#757575',
        fontSize: 13,
    },
    boxTimeRepeat: {
        backgroundColor: '#f2f2f2',
        paddingTop: 7,
        paddingBottom: 7,
        marginLeft: 10,
        flex: 0.225,
        alignItems: 'center'
    },
    txtTimeRepeat: {
        fontSize: 13,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxCreateTicket: {
        height: 35,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtCreateTicket: {
        fontSize: 14,
        color: '#bdbdbd'
    },
    boxInputDetail: {
        height: 170,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        overflow: 'hidden',
        padding: 20
    },
    txtDetail: {
        fontSize: 14,
        color: '#bdbdbd',
    },
    boxAction: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 15,
        marginBottom: 15,
        paddingLeft: 20,
        paddingRight: 20
    },
    btnSave: {
        flex: 1,
        // height: 48,
        // backgroundColor: '#e8e8e8',
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderRadius: 1,
        // marginRight: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        height: 48,
        marginRight: 5,
    },
    txtSave: {
        fontWeight: 'bold',
        // color: '#bdbdbd',
        color: '#333333',
        fontSize: 17
    },
    btnImport: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        height: 48,
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 6
    },
    txtImport: {
        fontWeight: 'bold',
        color: '#333333',
        fontSize: 17,
    },
    btnRegister: {
        flex: 0.5,
        // height: 48,
        // backgroundColor: '#e8e8e8',
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderRadius: 1,
        // marginLeft: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        height: 48,
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 6
    },
    txtRegister: {
        fontWeight: 'bold',
        // color: '#bdbdbd',
        color: '#333333',
        fontSize: 17
    },
    modalContent: {
        backgroundColor: "white",
        height: 189,
        width: 261,
        alignSelf: 'center',
        justifyContent: 'space-around',
        paddingBottom: 20,
        borderRadius: 4
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    content: {

    },
    txtNoti: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 14
    },
    btnOk: {
        width: 211,
        height: 50,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 2
    },
    txtOk: {
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold'
    },
    boxEditDetail: {
        height: 200,
        paddingTop: 20,
        marginBottom: 20,
        overflow: 'hidden'
    },
    txtEdit: {
        textAlign: 'center',
        color: '#03a9f4',
        fontSize: 14,
        fontWeight: 'bold'
    },
    txtInfoCod: {
        fontSize: 12,
        color: '#F2994A',
        marginTop: 5
    }
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onSaveEvent: (Slug, body, navigation) => {
            dispatch(actSaveEvent(Slug, body, navigation))
        },
        onEditEvent: (EventId) => {
            dispatch(actEditEvent(EventId))
        },
        onUpdateEvent: (Id, body, navigation) => {
            dispatch(actUpdateEvent(Id, body, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateEvent);
