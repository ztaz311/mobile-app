import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView, TextInput } from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import PickerItem from '../../screens_view/PickerItem';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { actLoadDataRefundList, actRefundAll, actRefuseAll } from '../../actions/event';
import ModalRefundRate from '../../components/event/ModalRefundRate';
import ModalCommentRefuse from '../../components/event/ModalCommentRefuse';
import Loading from '../../screens_view/Loading';
class RefundList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Status: 'All',
            Keyword: '',
            Page: 1,
            Type: 'time',
            modalRate: false,
            modalCommentRefuse: false,
        };
    }

    componentDidMount() {
        var Slug = this.props.route.params['Slug'];
        var { Keyword, Page, Type, Status } = this.state;
        this.props.onLoadDataRefundList(Slug, Keyword, Page, Type, Status);
    }

    _renderFooter() {
        var { loadMoreRefund, is_empty } = this.props.event;
        var { language } = this.props.language;
        if (loadMoreRefund) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20, paddingBottom: 20 }}>{convertLanguage(language, 'data_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var Slug = this.props.route.params['Slug'];
        var { loadMoreRefund } = this.props.event;
        var { Keyword, Page, Type, Status } = this.state;
        if (loadMoreRefund) {
            this.props.onLoadDataRefundList(Slug, Keyword, Page + 1, Type, Status);
            this.setState({ Page: Page + 1 })
        }
    }

    onChange(key, value) {
        this.setState({ [key]: value })
        var Slug = this.props.route.params['Slug'];
        this.setState({ [key]: value, Page: 1 })
        var { Keyword, Type, Status } = this.state;
        if (key === 'Keyword') {
            this.props.onLoadDataRefundList(Slug, value, 1, Type, Status);
        } else if (key === 'Status') {
            this.props.onLoadDataRefundList(Slug, Keyword, 1, Type, value);
        } else {
            this.props.onLoadDataRefundList(Slug, Keyword, 1, value, Status);
        }
    }

    showStatus(item) {
        var { language } = this.props.language;
        var { event } = this.props.event;
        var result = '';
        switch (item.status) {
            case -2:
            case "-2":
                result = <Touchable onPress={() => this.props.navigation.navigate('HandleRefund', { eventSlug: event.Slug, ticketDetailId: item.idTicketDetail })} style={[styles.boxAction, { backgroundColor: '#FFFFFF', borderWidth: 1, borderColor: '#333333' }]}>
                    <Text style={[styles.txtAction, { color: '#333333' }]}>{convertLanguage(language, 'refused')}</Text>
                </Touchable>
                break;
            case 3:
            case "3":
                result = <Touchable onPress={() => this.props.navigation.navigate('HandleRefund', { eventSlug: event.Slug, ticketDetailId: item.idTicketDetail })} style={[styles.boxAction, { backgroundColor: '#e8e8e8' }]}>
                    <Text style={[styles.txtAction, { color: '#bdbdbd' }]}>{convertLanguage(language, 'refunded')}</Text>
                </Touchable>
                break;
            case 0:
            case "0":
                result = <Touchable onPress={() => this.props.navigation.navigate('HandleRefund', { eventSlug: event.Slug, ticketDetailId: item.idTicketDetail })} style={[styles.boxAction, { backgroundColor: '#FFFFFF', borderWidth: 1, borderColor: '#03a9f4' }]}>
                <Text style={[styles.txtAction, { color: '#03a9f4' }]}>{convertLanguage(language, 'updated')}</Text>
            </Touchable>
                break;
            default:
                result = <Touchable onPress={() => this.props.navigation.navigate('HandleRefund', { eventSlug: event.Slug, ticketDetailId: item.idTicketDetail })} style={[styles.boxAction, { backgroundColor: '#e8e8e8' }]}>
                <Text style={[styles.txtAction, { color: '#bdbdbd' }]}>{convertLanguage(language, 'pending')}</Text>
            </Touchable>
                break;
        }
        return result;
    }
    showSelect(status) {
        var { language } = this.props.language;
        var result = '';
        switch (status) {
            case "All":
                result = convertLanguage(language, 'all')
                break;
            case 'Updated':
                result = convertLanguage(language, 'updated')
                break;
            case "Refused":
                result = convertLanguage(language, 'refused')
                break;
            case "Refunded":
                result = convertLanguage(language, 'refunded')
                break;
        }
        return result;
    }

    onRefund(Rate) {
        this.setState({ modalRate: false });
        var { event } = this.props.event;
        var body = {
            Rate,
            Slug: event.Slug,
            EventId: event.Id
        }
        this.props.onRefundAll(body);
    }

    onRefuse(Comment) {
        this.setState({ modalCommentRefuse: false });
        var { event } = this.props.event;
        var body = {
            Comment,
            Slug: event.Slug,
            EventId: event.Id
        }
        this.props.onRefuseAll(body);
    }

    render() {
        var { Keyword, Status, Type, modalRate, modalCommentRefuse } = this.state;
        var { totalRefunds, refunds, loadingRefundAll } = this.props.event;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'refund_request')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />

                <View style={styles.content}>
                    {
                        loadingRefundAll &&
                        <Loading />
                    }
                    <View style={styles.boxHeader}>
                        {
                            refunds.length > 0 &&
                            <View style={styles.boxButton}>
                                <Touchable style={styles.btnRefuse} onPress={() => this.setState({ modalCommentRefuse: true })}>
                                    <Text style={styles.txtRefuse}>{convertLanguage(language, 'refuse_all')}</Text>
                                </Touchable>
                                <Touchable style={styles.btnRefuse} onPress={() => this.setState({ modalRate: true })}>
                                    <Text style={styles.txtRefuse}>{convertLanguage(language, 'refund_all')}</Text>
                                </Touchable>
                            </View>
                        }
                        <TextInput
                            style={styles.ipContent}
                            selectionColor="#bdbdbd"
                            placeholder={convertLanguage(language, 'search_name_or_id_by_bolder_list')}
                            name="Keyword"
                            value={Keyword}
                            onChangeText={(Keyword) => this.onChange('Keyword', Keyword)}
                        />
                        <View style={styles.boxFilter}>
                            <View style={styles.boxLeft}>
                                <View style={styles.boxTotal}>
                                    <Image source={require('../../assets/icon_user.png')} style={styles.icon_user} />
                                    <Text style={styles.txtTotal}>{totalRefunds}</Text>
                                </View>
                                <PickerItem
                                    boxDisplayStyles={styles.boxPicker}
                                    options={[
                                        {
                                            name: convertLanguage(language, 'all'),
                                            value: 'All'
                                        },
                                        {
                                            name: convertLanguage(language, 'updated'),
                                            value: 'Updated'
                                        },
                                        {
                                            name: convertLanguage(language, 'refused'),
                                            value: 'Refused'
                                        },
                                        {
                                            name: convertLanguage(language, 'refunded'),
                                            value: 'Refunded'
                                        },
                                    ]}
                                    selectedOption={Status}
                                    onSubmit={(Status) => this.onChange('Status', Status)}
                                />
                            </View>
                            <View style={styles.boxRight}>
                                <Touchable style={[styles.boxTime, Type === 'name' ? { borderBottomWidth: 1, borderBottomColor: '#e8e8e8' } : {}]} onPress={() => this.onChange('Type', 'time')} disabled={Type === 'time' ? true : false}>
                                    <Text style={[styles.txtTime, Type === 'name' ? { fontWeight: 'normal', color: '#333333' } : {}]}>{convertLanguage(language, 'time')}</Text>
                                </Touchable>
                                <Touchable style={[styles.boxTime, Type === 'time' ? { borderBottomWidth: 1, borderBottomColor: '#e8e8e8' } : {}]} onPress={() => this.onChange('Type', 'name')} disabled={Type === 'name' ? true : false}>
                                    <Text style={[styles.txtTime, Type === 'time' ? { fontWeight: 'normal', color: '#333333' } : {}]}>{convertLanguage(language, 'name')}</Text>
                                </Touchable>
                            </View>
                        </View>
                    </View>
                    <FlatList
                        data={refunds}
                        contentContainerStyle={{ padding: 20 }}
                        renderItem={({ item }) => {
                            return <View style={styles.boxDataItem}>
                                <View style={styles.boxItem}>
                                    <View style={styles.boxInfo}>
                                        <Image source={{ uri: item.user.Avatars.Small }} style={styles.imgAvatar} />
                                        <View style={styles.boxInfoDetail}>
                                            <Text style={styles.txtName}>{item.NameHolder}</Text>
                                            <Text style={styles.txtId}>{item.user.Name}{item.user.UserId && ' / ID:' + item.user.UserId}</Text>
                                        </View>
                                    </View>
                                    {
                                        this.showStatus(item)
                                    }
                                </View>
                                {
                                    item.commentHolder &&
                                    <Text style={styles.txtReason}><Text style={{ color: '#03a9f4' }}>*</Text> {item.commentHolder}</Text>
                                }
                            </View>
                        }}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => this._renderFooter()}
                        onEndReached={() => { this.loadMore() }}
                    // onRefresh={() => {this.refresh()}}
                    // refreshing={false}
                    />
                </View>
                {
                    modalRate
                        ?
                        <ModalRefundRate
                            modalVisible={modalRate}
                            closeModal={() => this.setState({ modalRate: false })}
                            onRefund={(Rate) => this.onRefund(Rate)}
                        />
                        : null
                }
                {
                    modalCommentRefuse
                        ?
                        <ModalCommentRefuse
                            modalVisible={modalCommentRefuse}
                            closeModal={() => this.setState({ modalCommentRefuse: false })}
                            onRefuse={(Comment) => this.onRefuse(Comment)}
                        />
                        : null
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxHeader: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxDataItem: {
        marginBottom: 15
    },
    txtReason: {
        fontSize: 14,
        color: '#333333',
        marginTop: 10
    },
    boxButton: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
    },
    btnRefuse: {
        // width: 84,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 6,
        marginLeft: 15,
        paddingLeft: 10,
        paddingRight: 10
    },
    txtRefuse: {
        fontSize: 13,
        color: '#333333',
    },
    ipContent: {
        fontSize: 17,
        color: '#333333',
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        paddingBottom: 8,
        paddingTop: 8,
        paddingLeft: 0,
        marginBottom: 25,
        marginTop: 15
    },
    boxPicker: {
        borderWidth: 1,
        borderColor: '#bdbdbd',
        width: 100,
        height: 30,
        marginRight: 20,
        borderRadius: 1,
        paddingLeft: 8,
        paddingRight: 8,
        borderRadius: 5
    },
    txtPicker: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5,
        marginLeft: 5
    },
    boxFilter: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    boxLeft: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#e8e8e8',
        borderBottomWidth: 1,
        paddingBottom: 5
    },
    boxTotal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    icon_user: {
        width: 15,
        height: 17,
        marginRight: 5
    },
    txtTotal: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxRight: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    boxTime: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomWidth: 2,
        borderBottomColor: '#03a9f4'
    },
    txtTime: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#03a9f4'
    },
    boxData: {
        paddingLeft: 20,
        paddingRight: 20
    },
    boxItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    imgAvatar: {
        width: 60,
        height: 60,
        marginRight: 10,
        borderRadius: 30
    },
    boxInfoDetail: {
        flex: 1,
    },
    txtName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    txtId: {
        fontSize: 14,
        color: '#757575',
    },
    txtRole: {
        fontSize: 14,
        color: '#757575',
    },
    boxAction: {
        width: 75,
        height: 31,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6
    },
    txtAction: {
        fontSize: 13,
        color: '#FFFFFF'
    }
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataRefundList: (EventId, Keyword, Page, Type, Status) => {
            dispatch(actLoadDataRefundList(EventId, Keyword, Page, Type, Status))
        },
        onRefundAll: (body) => {
            dispatch(actRefundAll(body))
        },
        onRefuseAll: (body) => {
            dispatch(actRefuseAll(body))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(RefundList);
