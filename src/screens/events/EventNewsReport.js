import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity, ActivityIndicator, Platform, Keyboard, ScrollView } from 'react-native';

import Touchable from '../../screens_view/Touchable'
import SafeView from '../../screens_view/SafeView'

import * as Colors from '../../constants/Colors'

import { actReportEventNews } from '../../actions/event';
import { connect } from "react-redux";
import Toast from 'react-native-simple-toast';
import { convertLanguage } from '../../services/Helper';
import Loading from '../../screens_view/Loading';
class EventNewsReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            content: '',
        };
    }

    submitReport() {
        Keyboard.dismiss();
        var { language } = this.props.language;
        if (this.state.content.replace(/\s/g, '').length === 0) {
            Toast.showWithGravity(convertLanguage(language, 'validate_content'), Toast.SHORT, Toast.TOP)
            return;
        }
        if (!this.props.event.loadingReport) {
            this.props.reportEvent(this.props.route.params['Id'], this.state.content, this.props.navigation);
        }
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                {
                    this.props.event.loadingReport &&
                    <Loading />
                }
                <View style={{ margin: 20, marginBottom: 0, alignItems: 'center' }}>
                    <Touchable
                        onPress={() =>
                            this.props.navigation.goBack()
                        }
                        style={{
                            minWidth: 40, minHeight: 40, alignSelf: 'flex-start',
                            justifyContent: 'center',
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                    <Touchable
                        onPress={() => { this.submitReport() }}
                        style={[{
                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 4,
                            justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', right: 0, top: 0, borderWidth: 1,
                            borderColor: Colors.DISABLE,
                        }, this.state.content.replace(/\s/g, '').length == 0 || this.props.event.loadingReport ? {} : styles.btnReportActive]}>
                        <Text style={[styles.txtBtnReport, this.state.content.replace(/\s/g, '').length == 0 || this.props.event.loadingReport ? {} : styles.txtBtnReportActive]}>{convertLanguage(language, 'report')}</Text>
                    </Touchable>
                </View>
                <View style={{ flex: 1 }}>
                    <View style={styles.rowTitle}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'report')}</Text>
                    </View>
                    <ScrollView style={styles.rowContent}>

                        <TextInput
                            style={styles.ipContent}
                            multiline={true}
                            placeholder={convertLanguage(language, 'write_your_report')}
                            value={this.state.content}
                            onChangeText={(content) => this.setState({ content })}
                        // onEndEditing={(e) => this.setState({content: e.nativeEvent.text})}
                        />
                    </ScrollView>
                </View>
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    rowTitle: {
        alignItems: "center",
        marginBottom: 10
    },
    txtTitle: {
        fontSize: 18,
        color: '#333333',
        fontWeight: 'bold'
    },
    rowContent: {
        flex: 0.7,
        padding: 20,
    },
    ipContent: {
        flex: 1,
        padding: 10,
        fontSize: 18,
        textAlignVertical: 'top',
        borderWidth: 1,
        borderColor: '#bdbdbd',
        height: 300
    },
    rowAction: {
        flex: 0.3,
        alignItems: "center",
        marginTop: 10
    },
    btnReport: {
        flexDirection: 'row',
        borderWidth: 1,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 4,
        borderColor: Colors.DISABLE
    },
    txtBtnReport: {
        fontSize: 18,
        color: Colors.DISABLE,
        paddingRight: 3
    },
    btnReportActive: {
        backgroundColor: Colors.PRIMARY
    },
    txtBtnReportActive: {
        color: '#FFFFFF'
    }
});
const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        reportEvent: (id, content, navigation) => {
            dispatch(actReportEventNews(id, content, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventNewsReport);
