import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Alert, StyleSheet, ScrollView, PermissionsAndroid } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { convertLanguage, formatNumber } from '../../services/Helper';
import * as Colors from '../../constants/Colors';
import { connect } from "react-redux";
import { actLoadDataTicketSales } from '../../actions/event';
import { openSettings } from 'react-native-permissions';
import RNFetchBlob from 'rn-fetch-blob';
import * as Config from '../../constants/Config';
import moment from "moment";
class EventSaleReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingDownload: false
        };
    }

    componentDidMount() {
        var { Slug } = this.props.route.params;
        this.props.onLoadDataTicketSales(Slug);
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    async downLoadPDF() {
        var d = new Date()
        var { language } = this.props.language;
        this.setState({ loadingDownload: true })
        var { Slug } = this.props.route.params;
        var url = Config.API_URL + '/events/' + Slug + '/ticket-sales-pdf';
        const auKey = await AsyncStorage.getItem('auKey');
        const auValue = await AsyncStorage.getItem('auValue');
        var header = {
            [auKey]: auValue
        }
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.setState({ loading: true });
                    const android = RNFetchBlob.android
                    RNFetchBlob.config({
                        addAndroidDownloads: {
                            useDownloadManager: true,
                            title: Slug + d.getTime() + "-ticket-sale.pdf",
                            description: 'An PDF that will be installed',
                            mime: 'application/pdf',
                            mediaScannable: true,
                            notification: true,
                            path: RNFetchBlob.fs.dirs.DownloadDir + "/" + Slug + "-ticket-sale" + d.getTime() + ".pdf",
                        }
                    })
                        .fetch('GET', url, header)
                        .then((res) => {
                            this.setState({ loadingDownload: false });
                            Alert.alert(
                                convertLanguage(language, 'notification'),
                                convertLanguage(language, 'download_pdf_success'),
                                [
                                    { text: convertLanguage(language, 'cancel'), onPress: () => { } }
                                ]
                            );
                        }).catch((err) => {
                            console.log(err)
                            this.setState({ loadingDownload: false });
                            Alert.alert(
                                convertLanguage(language, 'notification'),
                                convertLanguage(language, 'download_pdf_fail'),
                                [
                                    { text: convertLanguage(language, 'cancel'), onPress: () => { } }
                                ]
                            );
                        })
                } else {
                    this._alertForStoragePermission()
                }
            } catch (err) {
                console.warn(err);
            }
        } else {
            const { dirs: { DocumentDir } } = RNFetchBlob.fs;
            const fPath = `${DocumentDir}/${Slug}-ticket-sale${d.getTime()}.pdf`;
            console.log(header)
            RNFetchBlob.config({
                fileCache: true,
                path: fPath,
                appendExt: 'pdf',
            })
                .fetch('GET', url, header)
                .then((resp) => {
                    this.setState({ loadingDownload: false });
                    RNFetchBlob.fs.writeFile(fPath, resp.data, 'base64')
                    RNFetchBlob.ios.previewDocument(fPath);
                }).catch((err) => {
                    // alert(JSON.stringify(err));
                    this.setState({ loadingDownload: false });
                    Alert.alert(
                        convertLanguage(language, 'notification'),
                        convertLanguage(language, 'download_pdf_fail'),
                        [
                            { text: convertLanguage(language, 'cancel'), onPress: () => { } }
                        ]
                    );
                });
        }
    }

    _alertForStoragePermission() {
        var { language } = this.props.language;
        Alert.alert(
            '',
            convertLanguage(language, 'storage_need'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                { text: convertLanguage(language, 'open_setting'), onPress: () => { openSettings() } },
            ],
        );
    }

    checkWithdraw() {
        var { SaleReport } = this.props.event;
        if (SaleReport.Event.TimeFinish) {
            var datetime = SaleReport.Event.TimeFinish.split(" ");
            var date = datetime[0].split("-");
            var time = datetime[1].split(":");
            var count_days = moment().diff(moment([date[0], date[1] - 1, date[2], time[0], time[1]]), 'days');
            return count_days;
        } else {
            return 0;
        }

    }

    showStatusWithdraw() {
        var { language } = this.props.language;
        var { SaleReport } = this.props.event;
        if (SaleReport.SaleReportMain.TotalRevenue > 0) {
            if (SaleReport.Event.StatusBtnWithDraw) {
                if (SaleReport.Event.Withdraw && Object.keys(SaleReport.Event.Withdraw).length > 0 && SaleReport.Event.Withdraw.Have > 0 && SaleReport.SaleReportMain.TotalRevenue > 0) {
                    if (SaleReport.Event.Withdraw.Status == 1) {
                        return <View style={[styles.btnWithdraw, { borderWidth: 0, borderBottomWidth: 1, borderBottomColor: '#03a9f4', backgroundColor: 'transparent' }]}>
                            <Text style={[styles.txtWithdraw, { color: '#333333' }]}>{convertLanguage(language, 'payment_code')}:  <Text style={{ color: '#03a9f4' }}>{SaleReport.Event.Withdraw.CodeWithDraw}</Text></Text>
                        </View>
                    } else if (SaleReport.Event.Withdraw.Status == 0) {
                        return <Touchable style={[styles.btnWithdraw, { borderWidth: 0, borderBottomWidth: 1, borderBottomColor: '#03a9f4', backgroundColor: 'transparent' }]} onPress={() => this.props.navigation.navigate('WithdrawRequest', { Slug: SaleReport.Event.Slug })}>
                            <Text style={[styles.txtWithdraw, { color: '#03a9f4' }]}>{convertLanguage(language, 'pending')}({convertLanguage(language, 'edit')})</Text>
                        </Touchable>
                    } else if (SaleReport.Event.Withdraw.Status == -1) {
                        return <Touchable style={[styles.btnWithdraw, { borderWidth: 0, borderBottomWidth: 1, borderBottomColor: '#03a9f4', backgroundColor: 'transparent' }]} onPress={() => this.props.navigation.navigate('WithdrawRequest', { Slug: SaleReport.Event.Slug })}>
                            <Text style={[styles.txtWithdraw, { color: '#03a9f4' }]}>{convertLanguage(language, 'error')}({convertLanguage(language, 'edit')})</Text>
                        </Touchable>
                    } else if (SaleReport.Event.Withdraw.Status == 2) {
                        return <View style={[styles.btnWithdraw, { borderWidth: 0, borderBottomWidth: 1, borderBottomColor: '#03a9f4', backgroundColor: 'transparent' }]}>
                            <Text style={[styles.txtWithdraw, { color: '#03a9f4' }]}>{convertLanguage(language, 'pending')}</Text>
                        </View>
                    } else {
                        return <Touchable disabled={!SaleReport.IsWithdraw} style={[styles.btnWithdraw, !SaleReport.IsWithdraw ? { backgroundColor: '#e8e8e8', borderWidth: 0 } : {}]} onPress={() => this.props.navigation.navigate('WithdrawRequest', { Slug: SaleReport.Event.Slug })}>
                            <Text style={[styles.txtWithdraw, !SaleReport.IsWithdraw ? { color: '#bdbdbd' } : {}]}>{convertLanguage(language, 'withdraw')}</Text>
                        </Touchable>
                    }
                } else {
                    return <Touchable style={styles.btnWithdraw} onPress={() => this.props.navigation.navigate('WithdrawRequest', { Slug: SaleReport.Event.Slug })}>
                        <Text style={[styles.txtWithdraw]}>{convertLanguage(language, 'withdraw')}</Text>
                    </Touchable>
                }
            } else {
                if (SaleReport.Event.Withdraw && Object.keys(SaleReport.Event.Withdraw).length > 0 && SaleReport.Event.Withdraw.Have > 0 && SaleReport.SaleReportMain.TotalRevenue > 0 && SaleReport.Event.Withdraw.Status == 1) {
                    return <View style={[styles.btnWithdraw, { borderWidth: 0, borderBottomWidth: 1, borderBottomColor: '#03a9f4', backgroundColor: 'transparent' }]}>
                        <Text style={[styles.txtWithdraw, { color: '#333333' }]}>{convertLanguage(language, 'payment_code')}: <Text style={{ color: '#03a9f4' }}>{SaleReport.Event.Withdraw.CodeWithDraw}</Text></Text>
                    </View>
                } else {
                    return <Touchable disabled={!SaleReport.IsWithdraw} style={[styles.btnWithdraw, { backgroundColor: '#e8e8e8', borderWidth: 0 }]} onPress={() => this.props.navigation.navigate('WithdrawRequest', { Slug: SaleReport.Event.Slug })}>
                        <Text style={[styles.txtWithdraw, { color: '#bdbdbd' }]}>{convertLanguage(language, 'withdraw')}</Text>
                    </Touchable>
                }
            }
        } else {
            return <Touchable disabled={true} style={[styles.btnWithdraw, { backgroundColor: '#e8e8e8', borderWidth: 0 }]} onPress={() => this.props.navigation.navigate('WithdrawRequest', { Slug: SaleReport.Event.Slug })}>
                <Text style={[styles.txtWithdraw, { color: '#bdbdbd' }]}>{convertLanguage(language, 'withdraw')}</Text>
            </Touchable>
        }
    }

    checkDisableDownload() {
        var { SaleReport } = this.props.event;
        var { loadingDownload } = this.state;
        // !SaleReport.IsFinish || loadingDownload
        if (loadingDownload) {
            return true;
        }
        if (SaleReport.Event.StatusDownload) {
            return false
        } else {
            return true;
        }
    }

    render() {
        var { language } = this.props.language;
        var { loadingTicketSales, SaleReport } = this.props.event;
        // console.log(SaleReport)
        var { loadingDownload } = this.state;
        console.log(this.props.event)

        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'sales_report')}</Text>
                    <View style={{ width: 48 }} />
                </View>

                <Line />
                {
                    loadingTicketSales ?
                        <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                        :
                        <ScrollView style={styles.content}>
                            <View style={styles.boxCurrentSale}>
                                <Text style={styles.txtTitle}>{SaleReport.IsFinish ? convertLanguage(language, 'final_sales') : convertLanguage(language, 'current_sales')}<Text style={styles.txtMore}>({convertLanguage(language, 'include_vat')})</Text></Text>
                                <View style={styles.boxRevenue}>
                                    <View style={styles.boxSoldRefund}>
                                        <View style={styles.boxSold}>
                                            <Text style={styles.txtSold}>{convertLanguage(language, 'sold')}</Text>
                                            <Text style={styles.txtSold}>{SaleReport.Event.Unit} {formatNumber(SaleReport.SaleReportMain.TotalMoney)}</Text>
                                        </View>
                                        <View style={styles.boxSold}>
                                            <Text style={styles.txtSold}>{convertLanguage(language, 'refunded')}</Text>
                                            <Text style={styles.txtSold}>- {SaleReport.Event.Unit} {formatNumber(SaleReport.SaleReportMain.TotalRefundMoney)}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.boxSoldRefund}>
                                        <View style={styles.boxSold}>
                                            <Text style={styles.txtTotalSale}>{convertLanguage(language, 'total_sales')}</Text>
                                            <Text style={styles.txtValueTotalSale}>{SaleReport.Event.Unit} {formatNumber(SaleReport.SaleReportMain.TotalSales)}</Text>
                                        </View>
                                        <View style={styles.boxSold}>
                                            <Text style={styles.txtSold}>{convertLanguage(language, 'fee')}</Text>
                                            <Text style={styles.txtSold}>- {SaleReport.Event.Unit} {formatNumber(SaleReport.SaleReportMain.Fee)}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.boxSold}>
                                        <Text style={styles.txtRevenue}>{convertLanguage(language, 'revenue')}</Text>
                                        <Text style={styles.txtRevenue}>{SaleReport.Event.Unit} {formatNumber(SaleReport.SaleReportMain.TotalRevenue)}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.boxTicketDetail}>
                                {
                                    SaleReport.detailSaleReports.length > 0
                                    &&
                                    <>
                                        {
                                            SaleReport.detailSaleReports.map((item, index) => {
                                                return <View style={[styles.boxTicketItem, index === SaleReport.detailSaleReports.length - 1 ? { borderBottomWidth: 0 } : {}]} key={index}>
                                                    <Text style={styles.txtRevenue}>{item.Name}</Text>
                                                    <Text style={styles.txtSold}>{SaleReport.Event.Unit} {formatNumber(item.Price)}</Text>
                                                    <View style={styles.boxRevenue}>
                                                        <View style={styles.boxSoldRefund}>
                                                            <View style={styles.boxSold}>
                                                                <Text style={styles.txtSold}>{convertLanguage(language, 'sold')} {item.QuantitySold}</Text>
                                                                <Text style={styles.txtSold}>{SaleReport.Event.Unit} {formatNumber(item.TotalMoneySold)}</Text>
                                                            </View>
                                                            {
                                                                item.RefundSold.length > 0
                                                                &&
                                                                <View style={styles.boxRefundDetail}>
                                                                    <Text style={styles.txtSold}>{convertLanguage(language, 'refunded')}</Text>
                                                                    <View style={styles.boxRefundTicket}>
                                                                        {
                                                                            item.RefundSold.map((refund, i) => {
                                                                                return <View style={styles.boxRefundPrice} key={i}>
                                                                                    <Text style={styles.txtSold}>{refund.Quantity} x {refund.Rate}%</Text>
                                                                                    <Text style={styles.txtSold}>- {SaleReport.Event.Unit} {formatNumber(refund.Total)}</Text>
                                                                                </View>
                                                                            })
                                                                        }
                                                                    </View>
                                                                </View>
                                                            }
                                                            <View style={styles.boxSold}>
                                                                <Text style={styles.txtSold}>{convertLanguage(language, 'comeup_fee')} {item.ComeupFeeRate}%</Text>
                                                                <Text style={styles.txtSold}>- {SaleReport.Event.Unit} {formatNumber((item.TotalMoneySold - item.TotalMoneyRefund) * item.ComeupFeeRate / 100)}</Text>
                                                            </View>
                                                            <View style={styles.boxSold}>
                                                                <Text style={styles.txtSold}>{convertLanguage(language, 'payment_fee')} {item.PGFeeRate}%</Text>
                                                                <Text style={styles.txtSold}>- {SaleReport.Event.Unit} {formatNumber((item.TotalMoneySold - item.TotalMoneyRefund) * item.PGFeeRate / 100)}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.boxSold}>
                                                            <Text style={styles.txtRevenue}></Text>
                                                            <Text style={styles.txtRevenue}>{SaleReport.Event.Unit} {formatNumber(item.TotalMoneyRevenue)}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            })
                                        }
                                    </>
                                }

                                <Text style={styles.txtToturial}>{convertLanguage(language, 'can_dowload_sales_report')} {convertLanguage(language, 'can_withdraw_sales_revenue')}</Text>
                                <View style={styles.boxAction}>
                                    <Touchable disabled={this.checkDisableDownload()} style={[styles.btnDownload, SaleReport.Event.StatusDownload ? { backgroundColor: 'transparent', borderColor: '#00A9F4' } : { backgroundColor: '#e8e8e8', borderWidth: 0 }]} onPress={() => this.downLoadPDF()}>
                                        {
                                            loadingDownload ?
                                                <ActivityIndicator size="small" color="#000000" />
                                                :
                                                <>
                                                    {
                                                        SaleReport.Event.StatusDownload &&
                                                        <Image source={require('../../assets/ic_download_small.png')} style={{ width: 18, height: 18, marginRight: 8 }} />
                                                    }
                                                    <Text style={[styles.txtDownload, SaleReport.Event.StatusDownload ? { color: '#00A9F4' } : { color: '#bdbdbd' }]}>{convertLanguage(language, 'download')}</Text>
                                                </>
                                        }
                                    </Touchable>
                                    {
                                        this.showStatusWithdraw()
                                    }
                                </View>
                            </View>
                        </ScrollView>
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    boxCurrentSale: {
        padding: 20,
        backgroundColor: '#FFFFFF',
        borderBottomColor: '#333333',
        borderBottomWidth: 1
    },
    txtTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#03a9f4'
    },
    txtMore: {
        fontSize: 13,
        color: '#757575'
    },
    boxRevenue: {
        marginLeft: 55
    },
    boxSoldRefund: {
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    boxSold: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        paddingTop: 3,
        paddingBottom: 3
    },
    txtSold: {
        fontSize: 14,
        color: '#333333'
    },
    txtTotalSale: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    txtValueTotalSale: {
        fontSize: 15,
        color: '#03a9f4',
        fontWeight: 'bold'
    },
    txtRevenue: {
        fontSize: 15,
        color: '#333333',
        fontWeight: '700'
    },
    boxTicketDetail: {
        paddingTop: 15,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 15
    },
    boxTicketItem: {
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: '#757575',
        borderBottomWidth: 1
    },
    txtTicketName: {
        fontSize: 15,
        color: '#03a9f4',
        fontWeight: '700'
    },
    boxRefundDetail: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    boxRefundTicket: {
        flex: 1,
        marginLeft: 5
    },
    boxRefundPrice: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        paddingBottom: 5
    },
    txtToturial: {
        fontSize: 11,
        color: '#757575',
        textAlign: 'justify'
    },
    boxAction: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 25,
        marginBottom: 20
    },
    btnDownload: {
        flex: 0.3,
        height: 48,
        backgroundColor: '#e8e8e8',
        borderRadius: 1,
        borderWidth: 0.5,
        borderColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        flexDirection: 'row'
    },
    txtDownload: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'normal'
    },
    btnWithdraw: {
        flex: 0.6,
        height: 48,
        backgroundColor: '#03a9f4',
        borderRadius: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4
    },
    txtWithdraw: {
        fontSize: 18,
        color: '#FFFFFF',
        // fontWeight: '500'
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataTicketSales: (slug) => {
            dispatch(actLoadDataTicketSales(slug))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventSaleReport);
// export default EventHostList;
