import React, { Component } from 'react';
import { Platform, View, Text, Image, ScrollView, StyleSheet, KeyboardAvoidingView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import FormTicket from '../../components/event/FormTicket'
import * as Colors from '../../constants/Colors'
import Toast from 'react-native-simple-toast';
import { convertLanguage, datetimeNow } from '../../services/Helper';
import ModalContactInfoComeup from '../../components/event/ModalContactInfoComeup';
import { connect } from "react-redux";
class EventTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_save: false,
            is_delete: false,
            is_sort: false,
            is_update: false,
            index_delete: -1,
            tickets: (this.props.route.params['Tickets'] && this.props.route.params['Tickets'].length > 0) ? this.props.route.params['Tickets'] : [
                {
                    TicketId: '',
                    NameTicket: '',
                    Unit: this.props.route.params['Unit'],
                    Price: '',
                    Quantity: '',
                    LimitQuantity: '10',
                    TimeStartTicket: datetimeNow(),
                    TimeEndTicket: this.props.route.params['TimeFinish'],
                    Description: '',
                    Option: [],
                    Question2: '',
                    Type: 'e-ticket',
                    Status: 1,
                    TotalTicketDetail: 0,
                    StatusCOD: 0
                },
            ],
            ticket_count: (this.props.route.params['Tickets'] && this.props.route.params['Tickets'].length > 0) ? this.props.route.params['Tickets'].length : 1,
            modalContact: false,
            isShowModalContact: false,
        };
    }

    componentDidMount() {
        // this.getTeams();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    onDelete(index) {
        var { tickets, ticket_count } = this.state;
        tickets.splice(index, 1);
        this.setState({ tickets, ticket_count: ticket_count - 1 })
    }

    save() {
        var { tickets } = this.state;
        if (this.validate(tickets)) return;
        this.props.route.params['callback'](tickets);
        this.props.navigation.goBack()
    }

    addMoreTicket() {
        var { tickets, ticket_count } = this.state;
        tickets.push({
            TicketId: '',
            NameTicket: '',
            Unit: this.props.route.params['Unit'],
            Price: '',
            Quantity: '',
            LimitQuantity: '10',
            TimeStartTicket: datetimeNow(),
            TimeEndTicket: this.props.route.params['TimeFinish'],
            Description: '',
            Option: [],
            Question2: '',
            Type: 'e-ticket',
            Status: 1,
            TotalTicketDetail: 0,
            StatusCOD: 0
        })
        this.setState({ tickets, ticket_count: ticket_count + 1 });
    }

    validate(Tickets) {
        var { language } = this.props.language;
        var result = false;
        Tickets.forEach((Ticket) => {
            if (Ticket.NameTicket === '') {
                Toast.showWithGravity(convertLanguage(language, 'validate_ticket_name'), Toast.SHORT, Toast.TOP)
                result = true;
                return;
            }
            if (Ticket.Price === '') {
                Toast.showWithGravity(convertLanguage(language, 'validate_ticket_price'), Toast.SHORT, Toast.TOP)
                result = true;
                return;
            }
            if(Ticket.Unit.toUpperCase() === 'VND') {
                if(Ticket.Price < 10000) {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_price_min', {zero: '0', min: '10,000'}), Toast.SHORT, Toast.TOP)
                    result = true;
                    return;
                } else if(Ticket.Price > 10000000000) {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_price_max', {max: '10,000,000,000'}), Toast.SHORT, Toast.TOP)
                    result = true;
                    return;
                }
            } else {
                if(Ticket.Price < 1000) {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_price_min', {zero: '0', min: '1,000'}), Toast.SHORT, Toast.TOP)
                    result = true;
                    return;
                } else if(Ticket.Price > 1000000000) {
                    Toast.showWithGravity(convertLanguage(language, 'validate_ticket_price_max', {max: '1,000,000,000'}), Toast.SHORT, Toast.TOP)
                    result = true;
                    return;
                }
            }
            if (Ticket.Quantity === '') {
                Toast.showWithGravity(convertLanguage(language, 'validate_ticket_quantity'), Toast.SHORT, Toast.TOP)
                result = true;
                return;
            }
            if (Ticket.LimitQuantity === '') {
                Toast.showWithGravity(convertLanguage(language, 'validate_ticket_limit'), Toast.SHORT, Toast.TOP)
                result = true;
                return;
            }
            if (Ticket.TimeStartTicket === '') {
                Toast.showWithGravity(convertLanguage(language, 'validate_ticket_time_start'), Toast.SHORT, Toast.TOP)
                result = true;
                return;
            }
            if (Ticket.TimeEndTicket === '') {
                Toast.showWithGravity(convertLanguage(language, 'validate_ticket_time_end'), Toast.SHORT, Toast.TOP)
                result = true;
                return;
            }
        })
        return result;
    }

    onUpTicket(index) {
        var { tickets } = this.state;
        if (index > 0) {
            var data1 = tickets[index];
            var data2 = tickets[index - 1];
            tickets[index - 1] = data1;
            tickets[index] = data2;
        }
        this.setState({ tickets });
    }
    onDownTicket(index) {
        var { tickets, ticket_count } = this.state;
        if (index < ticket_count - 1) {
            var data1 = tickets[index];
            var data2 = tickets[index + 1];
            tickets[index + 1] = data1;
            tickets[index] = data2;
        }
        this.setState({ tickets });
    }

    onChange(index, key, value) {
        var { tickets } = this.state;
        if (key === 'Price' || key === 'Quantity' || key === 'LimitQuantity') {
            value = value.replace(/[^0-9]/ig, '');
            if(value.length > 1) {
                if(value.charAt(0) == 0) {
                    value = value.slice(1);
                }
            }
        }
        if (key === 'Option') {
            // tickets[index][key] = '';
            var Option = tickets[index][key];
            if (Option.indexOf(value) === -1) {
                Option.push(value);
            } else {
                Option.splice(Option.indexOf(value), 1)
            }
            tickets[index][key] = Option;
        } else {
            tickets[index][key] = value;
        }
        if (key === 'TimeStartTicket') {
            tickets[index]['TimeEndTicket'] = '';
        }
        if (key === 'Type' && value === 'd-p-ticket') {
            var Option = tickets[index]['Option'];
            if (Option.indexOf(1) === -1) {
                Option.push(1);
                tickets[index]['Option'] = Option;
            }
            if (tickets[index].StatusCOD === 0) {
                this.setState({
                    modalContact: true
                })
            }
        }
        this.setState({ tickets });
    }

    onShowModalContactInfo = () => {
        this.setState({
            modalContact: true
        })
    }

    render() {
        var { tickets, ticket_count } = this.state;
        var { language } = this.props.language;
        var { EventId } = this.props.route.params;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{this.state.data}</Text>
                    <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: '#03a9f4', marginRight: 20 }} onPress={() => this.save()}>
                        <Text style={{ fontSize: 17, color: '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'save')}</Text>
                    </Touchable>
                </View>

                <Line />
                <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 0} style={{flex: 1}} >
                    <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps='always' contentContainerStyle={{ paddingBottom: 16 }}>
                        <View style={styles.content}>
                            <View style={styles.boxTitle}>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'we_are_happy_with_free')}</Text>
                                <Text style={styles.txtTitle2}>{convertLanguage(language, 'terms_of_selling_tickets')}</Text>
                                <Text style={styles.txtTitle3}>{convertLanguage(language, 'by_creating_tickets_you_agree_to_the_terms_of_selling_tickets')}</Text>
                            </View>
                            {
                                // tickets.length === 0 ?
                                // <FormTicket key={index} index={index} is_save={this.state.is_save} onUpdateData={() => this.onUpdateData()} />
                                // :
                                tickets.map((ticket, index) => {
                                    return <FormTicket onShowModalContactInfo={() => this.onShowModalContactInfo()} key={index} index={index} ticket={ticket} EventId={EventId} TimeFinish={this.props.route.params['TimeFinish']} onUpdateData={(index, data) => this.onUpdateData(index, data)} onDelete={(index) => this.onDelete(index)} ticket_count={ticket_count} onUpTicket={(index) => this.onUpTicket(index)} onDownTicket={(index) => this.onDownTicket(index)} onChange={(index, key, value) => this.onChange(index, key, value)} />
                                })
                            }
                            <Touchable style={styles.btnAddMore} onPress={() => this.addMoreTicket()}>
                                <Text style={styles.txtAddMore}>+ {convertLanguage(language, 'add_a_ticket')}</Text>
                            </Touchable>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
                {
                    this.state.modalContact &&
                    <ModalContactInfoComeup
                        modalVisible={this.state.modalContact}
                        closeModal={() => this.setState({ modalContact: false })}
                    />
                }
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxTitle: {
        paddingTop: 20,
        paddingBottom: 20,
    },
    txtTitle: {
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold',
        marginBottom: 20,
        textAlign: 'center'
    },
    txtTitle2: {
        color: '#03a9f4',
        fontSize: 15,
        marginBottom: 20,
        textDecorationLine: 'underline',
        textAlign: 'center'
    },
    txtTitle3: {
        fontSize: 14,
        color: '#757575',
        textAlign: 'center'
    },
    btnAddMore: {
        height: 35,
        borderWidth: 1,
        borderColor: '#03a9f4',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtAddMore: {
        color: '#03a9f4',
        fontSize: 15
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
// const mapDispatchToProps = (dispatch, props) => {
//     return {


//     }
// }
export default connect(mapStateToProps, null)(EventTicket);
// export default EventHostList;
