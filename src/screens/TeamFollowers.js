import React, { Component } from 'react';
import { Text, StyleSheet, ActivityIndicator, ScrollView, View, TouchableOpacity, Image } from 'react-native';
import { actListMemberFollowers, actInvitedMember } from '../actions/team';
import { connect } from "react-redux";
import SafeView from '../screens_view/SafeView'
import MemberFollowItem from '../components/member/MemberFollowItem'
import * as Colors from '../constants/Colors'
import { convertLanguage } from '../services/Helper'
import Touchable from '../screens_view/Touchable'
import Line from '../screens_view/Line'
class TeamFollowers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: ''
        };
    }

    componentWillMount() {
        this.props.listMemberFollowers(this.props.team.team.Id);
    }

    submitInvitedMember(memberId) {
        this.props.invitedMember(this.props.team.team.Id, memberId, this.props.navigation);
    }

    renderMember() {
        var members = this.props.team.team.Followers;
        if (!members) {
            return null;
        }
        return members.map((member, index) => {
            return <MemberFollowItem key={index} member={member}
                invitedMember={() => { this.submitInvitedMember(member.Id) }}
            />
        });
    }

    render() {
        let team = this.props.team.team;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{width: 24, height: 24}} />
                    </Touchable>
                </View>
                <Line />
                <ScrollView contentContainerStyle={styles.content}>
                    <View style={styles.header}>
                        <View style={styles.colThumb}>
                            <Image
                                resizeMode='cover'
                                style={styles.imgLogo}
                                source={team.Logos ? { uri: team.Logos.Medium } : null} />
                        </View>
                        <View style={styles.colTeamInfo}>
                            <View style={styles.rowTeamName}>
                                <Text style={styles.txtTeamName}>
                                    {team.Name}
                                </Text>
                                {
                                    team.Verify == 1 &&
                                    <Image style={styles.imgCertified} source={require('../assets/team_certified.png')} />
                                }
                            </View>
                            <Text style={styles.txtTeamId}>ID: {team.Code}</Text>
                        </View>
                    </View>
                    <Text style={styles.txtTotal}> {team.Followers ? team.Followers.length : 0} {convertLanguage(language, 'followers')}</Text>
                    {
                        this.props.team.loadingFollowers ?
                            <ActivityIndicator size="large" />
                            : null
                    }
                    {
                        this.renderMember()
                    }
                </ScrollView>
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    rowSearch: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    inputSearch: {
        flex: 1,
        paddingBottom: 10,
        borderBottomWidth: 1,
        fontSize: 16,
        borderColor: '#383838'
    },
    imgSearch: {
        marginLeft: 20
    },
    txtTotal: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 15,
        color: '#333333'
    },
    content: {
        margin: 20,
        marginTop: 0
    },
    header: {
        flexDirection: 'row',
        paddingBottom: 20,
        paddingTop: 20
    },
    colThumb: {
        flex: 0.3,
        justifyContent: 'flex-start'
    },
    colTeamInfo: {
        flex: 0.7,
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10

    },
    imgLogo: {
        // alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 40
    },
    rowTeamName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtTeamName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtTeamId: {
        fontSize: 18,
        color: '#333333'
    },
    imgCertified: {
        maxWidth: 30,
        marginLeft: 10
    },
});
const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        listMemberFollowers: (teamId) => {
            dispatch(actListMemberFollowers(teamId))
        },
        invitedMember: (teamId, memberId, navigation) => {
            dispatch(actInvitedMember(teamId, memberId, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamFollowers);
