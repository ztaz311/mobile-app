/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import appReducers from './src/reducers/index';
import Route from './src/router';
import thunk from 'redux-thunk';
// import { navigationRef } from './src/router/NavigationService';
import DropdownAlert from 'react-native-dropdownalert';
import NetInfo from "@react-native-community/netinfo";

const store = createStore(
  appReducers,
  applyMiddleware(thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstApp: true
    }
  }
  componentDidMount() {
    NetInfo.addEventListener(state => {
      if (!state.isConnected) {
        this.setState({ firstApp: false })
        this.dropDownAlertRef.alertWithType('error', '', 'Không có kết nối internet');
      }
      if (state.isConnected && !this.state.firstApp) {
        this.setState({ firstApp: true })
        this.dropDownAlertRef.closeAction()
      }
    });
  }
  render() {
    return (
      <Provider store={store}>
        <Route />
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} errorImageSrc={require('./src/assets/wifi_disconnect.png')} closeInterval={10000000000} tapToCloseEnabled={false} panResponderEnabled={true} contentContainerStyle={{ flexDirection: 'row', flex: 1, alignItem: 'center' }} containerStyle={{ padding: 0, flexDirection: 'row', alignItem: 'center' }} defaultTextContainer={{ flex: 1 }} messageStyle={{ fontSize: 14, textAlign: 'left', fontWeight: 'normal', color: 'white', backgroundColor: 'transparent', paddingLeft: 10 }} imageStyle={{ padding: 4, width: 20, height: 20, alignSelf: 'center' }} errorColor={'#4c4545'} />
      </Provider>
    );
  }
}